*# Christian 21/04/2020
*#-------------Groundwater (flujo subterraneo)---------------
*if(Strcmp(GenData(Groundwater_flow),"Off")==0)
*if(Strcmp(GenData(Groundwater__flow),"Off")==0)
0 0 *GenData(khs_multiplier)
Groundwater flow disabled
*elseif(Strcmp(GenData(Groundwater__flow),"Lumped")==0)
1 1 *GenData(khs_multiplier)
*tcl(Groundwater::WriteCalculationFile)
*set cond Assign_exfiltration_area *elems
*loop elems *OnlyInCond
*ElemsNum *tcl(Groundwater::WriteCatchmentNumber *cond(Reservoir_name))
*end elems
Reservoir asignation
*set cond Assign_infiltration_area *elems 
*loop elems *OnlyInCond
*ElemsNum *tcl(Groundwater::WriteCatchmentNumber *cond(Reservoir_name))
*end elems
*endif
*#DESARROLLO DEL BUCLE SI ESTAMOS EN GREEN AMPT
*elseif(Strcmp(GenData(Groundwater_flow),"Lumped")==0)
1 1 *GenData(khs_multiplier)
*tcl(Groundwater::WriteCalculationFile)
*set cond Assign_exfiltration_area *elems
*loop elems *OnlyInCond
*ElemsNum *tcl(Groundwater::WriteCatchmentNumber *cond(Reservoir_name))
*end elems
Reservoir asignation
*set cond Assign_infiltration_area *elems 
*loop elems *OnlyInCond
*ElemsNum *tcl(Groundwater::WriteCatchmentNumber *cond(Reservoir_name))
*end elems
*elseif(Strcmp(GenData(Groundwater_flow),"Distributed")==0)
*if(Strcmp(GenData(Losses_model),"Green_Ampt_")==0)
1 2 *GenData(khs_multiplier)
*elseif(Strcmp(GenData(Zonal_losses_model),"Green_Ampt_")==0)
1 2 *GenData(khs_multiplier)
*else
*messagebox - Losses model is disabled, you must activate Green&Ampt it if you want to calculate groundwater flow - Process finished with error -
*endif
*#------Horizontal permeability-------
Horizontal permeability
*set cond Distributed_model_parameters *elems
*if(CondNumEntities==0)
*messagebox - No elements with Distributed Groundwater Horizontal permeability were found, please check and run again - Process finished with error -
*endif
*loop elems *OnlyInCond
*if(strcmp(cond(Khs_definition),"Value")==0)
*ElemsNum *cond(Khs_[mm/h])
*else
*ElemsNum -*cond(Multiplier_Kga_[-])
*endif
*end elems
End Horizontal permeability
*#------ Boundary Condition --------------------
Boundary condition
*set cond Distributed_model_boundary_conditions *elems 
*loop elems *OnlyInCond
*if(strcmp(cond(Groundwater_BC),"Hydraulic_head")==0)
*ElemsNum *CondElemFace 0. 1 *cond(Hydraulic_head_[m])
*elseif(strcmp(cond(Groundwater_BC),"Unit_discharge")==0)
*ElemsNum *CondElemFace 0. 3 *cond(Unit_discharge_[m2/s])
*elseif(strcmp(cond(Groundwater_BC),"Saturated_thickness")==0)
*ElemsNum *CondElemFace 0. 2 *cond(Saturated_thickness_[m])
*endif
*end elems
*endif
End Condition