*# WQ 2.0 (Luis-Marcos 08/2019)
*#----------------Phytoplankton---------------------
*if(Strcmp(GenData(Phytoplankton),"On")==0)
*#------ Initial Condition --------------------
Initial condition Phytoplankton 
*set cond Phytoplankton_Initial_Cond *elems 
*if(CondNumEntities==0)
*messagebox - An element without Phytoplankton Initial Condition was found, please check and run again - Process finished with error -
*endif
*loop elems *OnlyInCond
*ElemsNum *cond(Chl-A_[mg/l])
*end elems
End Initial condition Phytoplankton 
*#------ Boundary Condition --------------------
Boundary condition Phytoplankton 
*set cond Phytoplankton_Bound_Cond *elems 
*loop elems *OnlyInCond
*set var INS(int)=cond(Phytoplankton_inlet,int)
*for(i=1;i<=INS(int);i=i+2)
*ElemsNum *CondElemFace *cond(Phytoplankton_inlet,*i,int) *cond(Phytoplankton_inlet,*Operation(i+1),real) 
*end for
*end elems
End condition Phytoplankton 
*endif
