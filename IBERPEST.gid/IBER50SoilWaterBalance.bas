*#------ Archivo Iber_SoilWaterBalance.bas ------------------------
*#CHRISTIAN 26/03/2020
*#------
*#Evapotranspiracion y percolacion
*#------
*if(Strcmp(GenData(Infiltration_losses),"Manual_/_By_parameters")==0)
*if(Strcmp(GenData(Losses_model),"Green_Ampt_")==0)
*if(Strcmp(GenData(Evapotranspiration__process),"On")==0)
1 *\
*elseif(Strcmp(GenData(Evapotranspiration__process),"Off")==0)
0 *\
*endif
*if(Strcmp(GenData(Percolation_process),"On")==0)
1 *\
*elseif(Strcmp(GenData(Percolation_process),"Off")==0)
0 *\
*endif *\
*GenData(Correction_coef_for_potential_ETP) *GenData(Pore_distribution_index) *GenData(Ks_for_percolation_[mm/h])
*GenData(Soil_water_stress)
*tcl(SoilWaterStress::WriteCalculationFile)
*set cond Crop *elems 
*loop elems *OnlyInCond
*ElemsNum *tcl(SoilWaterStress::WriteCropNumber *cond(Crop_name))
*end elems
*elseif(Strcmp(GenData(Losses_model),"SCS_continuous")==0)
*if(Strcmp(GenData(Evapotranspiration_process),"On")==0)
1 *\
*elseif(Strcmp(GenData(Evapotranspiration_process),"Off")==0)
0 *\
*endif
*if(Strcmp(GenData(Percolation_process),"On")==0)
1 *\
*elseif(Strcmp(GenData(Percolation_process),"Off")==0)
0 *\
*endif *\
*GenData(Correction_coef_for_potential_ETP) *GenData(Pore_distribution_index) *GenData(Ks_for_percolation_[mm/h])
0
*tcl(SoilWaterStress::WriteCalculationFile)
*set cond Crop *elems 
*loop elems *OnlyInCond
*ElemsNum *tcl(SoilWaterStress::WriteCropNumber *cond(Crop_name))
*end elems
*else
*if(Strcmp(GenData(Groundwater__flow),"Lumped")==0)
*if(Strcmp(GenData(Evapotranspiration_process),"On")==0)
1 *\
*elseif(Strcmp(GenData(Evapotranspiration_process),"Off")==0)
0 *\
*endif
*if(Strcmp(GenData(Percolation_process),"On")==0)
1 *\
*elseif(Strcmp(GenData(Percolation_process),"Off")==0)
0 *\
*endif *\
*GenData(Correction_coef_for_potential_ETP) *GenData(Pore_distribution_index) *GenData(Ks_for_percolation_[mm/h])
0
*elseif(Strcmp(GenData(Groundwater__flow),"Off")==0)
0 0 0 0 0
*endif
*endif
*endif
*if(Strcmp(GenData(Infiltration_losses),"By_zones")==0)
*if(Strcmp(GenData(Zonal_losses_model),"Green_Ampt_")==0)
*if(Strcmp(GenData(Evapotranspiration__process),"On")==0)
1 *\
*elseif(Strcmp(GenData(Evapotranspiration__process),"Off")==0)
0 *\
*endif
*if(Strcmp(GenData(Percolation_process),"On")==0)
1 *\
*elseif(Strcmp(GenData(Percolation_process),"Off")==0)
0 *\
*endif *\
*GenData(Correction_coef_for_potential_ETP) *GenData(Pore_distribution_index) *GenData(Ks_for_percolation_[mm/h])
*GenData(Soil_water_stress)
*tcl(SoilWaterStress::WriteCalculationFile)
*set cond Crop *elems 
*loop elems *OnlyInCond
*ElemsNum *tcl(SoilWaterStress::WriteCropNumber *cond(Crop_name))
*end elems
*elseif(Strcmp(GenData(Zonal_losses_model),"SCS_continuous")==0)
*if(Strcmp(GenData(Evapotranspiration_process),"On")==0)
1 *\
*elseif(Strcmp(GenData(Evapotranspiration_process),"Off")==0)
0 *\
*endif
*if(Strcmp(GenData(Percolation_process),"On")==0)
1 *\
*elseif(Strcmp(GenData(Percolation_process),"Off")==0)
0 *\
*endif *\
*GenData(Correction_coef_for_potential_ETP) *GenData(Pore_distribution_index) *GenData(Ks_for_percolation_[mm/h])
0
*tcl(SoilWaterStress::WriteCalculationFile)
*set cond Crop *elems 
*loop elems *OnlyInCond
*ElemsNum *tcl(SoilWaterStress::WriteCropNumber *cond(Crop_name))
*end elems
*else
*if(Strcmp(GenData(Groundwater__flow),"Lumped")==0)
*if(Strcmp(GenData(Evapotranspiration_process),"On")==0)
1 *\
*elseif(Strcmp(GenData(Evapotranspiration_process),"Off")==0)
0 *\
*endif
*if(Strcmp(GenData(Percolation_process),"On")==0)
1 *\
*elseif(Strcmp(GenData(Percolation_process),"Off")==0)
0 *\
*endif *\
*GenData(Correction_coef_for_potential_ETP) *GenData(Pore_distribution_index) *GenData(Ks_for_percolation_[mm/h])
0
*elseif(Strcmp(GenData(Groundwater__flow),"Off")==0)
0 0 0 0 0
*endif
*endif
*endif
*if(Strcmp(GenData(Infiltration_losses),"Off")==0)
0 0 0 0 0
*endif