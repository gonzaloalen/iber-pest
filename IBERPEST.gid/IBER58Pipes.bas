*#---Escribe el archivo de datos para los colectores
*# Jos� Luis Jul/2011 Ernest Jun/20
*if(strcmp(GenData(IberUrbanDrainagePlugin),"Disabled")==0)
*tcl(ModelName)
0    
CONDICIONS INICIALS
CC: CONDICIONS CONTORN
*endif
*if(strcmp(GenData(IberUrbanDrainagePlugin),"Enabled")==0)
*tcl(ModelName)
*if(strcmp(GenData(Flow),"Surface")==0)
-1      !Solo superficie pero Urban Drainage Activado (se calculan rejas)
CONDICIONS INICIALS           
CC: CONDICIONS CONTORN
*else
*set cond Pipes 
*set var MeshElem1=0
*loop elems *OnlyInCond
*set var MeshElem1=Operation(MeshElem1+1)
*end elems
*MeshElem1
*set elems(linear) 
*loop elems *OnlyInCond
*if(strcmp(cond(Shape),"Rectangular")==0)
*ElemsConec *Cond(Pipe_ID,int) 2 0 0 0 *Cond(Manning,real) *Cond(Width_[m],real) *Cond(Height_[m],real)  *Cond(Presurized_flow_celerity_[m/s],real) *ElemsNum 
*else
*ElemsConec *Cond(Pipe_ID,int) 3 0 0 0 *Cond(Manning,real) *Cond(Diameter_[m]) *Cond(Diameter_[m])  *Cond(Presurized_flow_celerity_[m/s],real) *ElemsNum 
*endif
*end elems
CONDICIONS INICIALS
*set cond Pipes_Initial_Condition
*set elems(linear) 
*loop elems *OnlyInCond
*if(strcmp(cond(Type_of_condition),"User_defined")==0)
*ElemsNum 1 *Cond(Discharge_[m3/s],real) *Cond(Depth_[m],real)  0   :Assigned Values by the user
*endif
*if(strcmp(cond(Type_of_condition),"Normal_depth")==0)
*ElemsNum 2 *Cond(Discharge_[m3/s],real) *Cond(Depth_[m],real)  0   :Normal depth
*endif
*#if(strcmp(cond(Type_of_condition),"Backwater_profile")==0)
*#ElemsNum 3 *Cond(Discharge_[m3/s],real) *Cond(Depth_[m],real)  0   :Backwater profile
*#endif
*end elems
CC: CONDICIONS CONTORN
*set cond 1D_Inlet
*loop nodes *OnlyInCond
*if(strcmp(cond(2D_Connection),"1")==0)
*NodesNum 0 0.0 0.0 -81 0 0 0 -1 -Entrada Conexi�n 2D-
*else
*if(strcmp(cond(Flow_Condition),"SUBCRITICAL/CRITICAL")==0)
*set var INS(int)=cond(Hydrograph,int)
*set var PASO=2
*for(i=1;i<=INS(int);i=i+PASO)
*NodesNum  *cond(Hydrograph,*i,int) *Operation(cond(Hydrograph,*Operation(i+1),real)) 0  -21 0 0 0 -1   -Hidrograma Q CriticoLento-
*end for
*endif
*if(strcmp(cond(Flow_Condition),"SUPERCRITICAL")==0)
*if(strcmp(cond(Water),"Depth")==0)
*set var INS(int)=cond(Discharge-Depth_Hydrograph,int)
*set var PASO=3
*for(i=1;i<=INS(int);i=i+PASO)
*NodesNum   *cond(Discharge-Depth_Hydrograph,*i,int) *Operation(cond(Discharge-Depth_Hydrograph,*Operation(i+1),real)) *cond(Discharge-Depth_Hydrograph,*Operation(i+2),real)  -11 0 0 0 -1  -Hidrograma t-Q-y Rapido-
*end for
*endif
*if(strcmp(cond(Water),"Elevation")==0)
*set var INS(int)=cond(Discharge-Elevation_Hydrograph,int)
*set var PASO=3
*for(i=1;i<=INS(int);i=i+PASO)
*NodesNum   *cond(Discharge-Elevation_Hydrograph,*i,int) *Operation(cond(Discharge-Elevation_Hydrograph,*Operation(i+1),real)) *cond(Discharge-Elevation_Hydrograph,*Operation(i+2),real)  -11 1 0 0 -1  -Hidrograma t-Q-z Rapido-
*end for
*endif
*endif
*endif
*end nodes
*set cond 1D_Outlet
*loop nodes *OnlyInCond
*if(strcmp(cond(2D_Connection),"1")==0)
*NodesNum 0 0.0 0.0 -82 0 0 0 -2 - S�lida Conexi�n 2D-
*else
*if(strcmp(cond(Exit),"Supercritical/Critical_Flow")==0)
*NodesNum  0.0 0.0 0.0  -40 0 0 0 -2   -S�lida Cr�tico/R�pido-
*endif
*if(strcmp(cond(Exit),"Subcritical_Flow")==0)
*if(strcmp(cond(Type),"Given_level")==0)
*set var INS(int)=cond(Given_level_[m],int)
*set var PASO=2
*for(i=1;i<=INS(int);i=i+PASO)
*NodesNum   *cond(Given_level_[m],*i,int)  0 *Operation(cond(Given_level_[m],*Operation(i+1),real))  -23 0 0 0 -2  -Salida t-z--
*end for
*endif
*if(strcmp(cond(Type),"Weir")==0)
*if(strcmp(cond(Weir_by),"Height")==0)
*NodesNum 0 *cond(Weir_height_[m]) *cond(Weir_crest_length_[m],real) *cond(Weir_Coefficient,real) 0 0 0 -2 -Salida Vertedero-altura-
*endif
*if(strcmp(cond(Weir_by),"Elevation")==0)
*NodesNum   0 *cond(Weir_elevation_[m]) *cond(Weir_crest_length_[m]),real)  *cond(Weir_Coefficient,real) 1 0 0 -2  -Salida Vertedero-cota-
*endif
*endif
*endif
*endif
*end nodes
*endif
*endif
123456789








