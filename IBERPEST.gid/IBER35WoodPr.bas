*if(strcmp(GenData(IberWoodPlugin),"Enabled")==0)
*if(strcmp(GenData(Method_W),"Kinematic")==0)
*if(strcmp(GenData(Drag_Coefficient),"Constant")==0)
1   *GenData(Drag_Coefficient_Value)   *GenData(Friction_coefficient)    *GenData(Wall_bouncing_angle_[rad])    *GenData(Restitution_coef) *GenData(Max_Root_Diam_[m]) *GenData(Max_Root_Width_[m])        Wood Transport (0:NO, 1:Kinematic, 2: Dynamic), Cd (if -888 Gippel) , friction, Bouncing Angle, Restitution coef, Max Root Diam, Max Root Width
*elseif(strcmp(GenData(Drag_Coefficient),"Gippel")==0)
1   -888   *GenData(Friction_coefficient)    *GenData(Wall_bouncing_angle_[rad])   *GenData(Restitution_coef) *GenData(Max_Root_Diam_[m]) *GenData(Max_Root_Width_[m])               		              Wood Transport (0:NO, 1:Kinematic, 2: Dynamic), Cd (if -888 Gippel) , friction, Bouncing Angle, Restitution coef, Max Root Diam, Max Root Width
*endif
*elseif(strcmp(GenData(Method_W),"Dynamic")==0)
*if(strcmp(GenData(Drag_Coefficient),"Constant")==0)
2   *GenData(Drag_Coefficient_Value)   *GenData(Friction_coefficient)    *GenData(Wall_bouncing_angle_[rad])   *GenData(Restitution_coef) *GenData(Max_Root_Diam_[m]) *GenData(Max_Root_Width_[m])         Wood Transport (0:NO, 1:Kinematic, 2: Dynamic), Cd (if -888 Gippel) , friction, Bouncing Angle, Restitution coef, Max Root Diam, Max Root Width
*elseif(strcmp(GenData(Drag_Coefficient),"Gippel")==0)
2   -888   *GenData(Friction_coefficient)    *GenData(Wall_bouncing_angle_[rad])   *GenData(Restitution_coef) *GenData(Max_Root_Diam_[m]) *GenData(Max_Root_Width_[m])           		                  Wood Transport (0:NO, 1:Kinematic, 2: Dynamic), Cd (if -888 Gippel) , friction, Bouncing Angle, Restitution coef, Max Root Diam, Max Root Width
*endif
*endif
*elseif(strcmp(GenData(IberWoodPlugin),"Disabled")==0)
0        No Wood Transport
*endif