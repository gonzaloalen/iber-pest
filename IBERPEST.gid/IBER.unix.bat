#!/bin/sh -f

# OutputFile: "$2/proceso.rep"

# erase fils with informtion on solver tu sue
mv "$2/$1.dat" "$2/Iber2D.dat"
mv "$2/$1-1.dat" "$2/Iber1D.dat"
mv "$2/$1-2.dat" "$2/Iber_Rain.dat"
mv "$2/$1-3.dat" "$2/Iber_Losses.dat"
mv "$2/$1-4.dat" "$2/Iber_Bedload.dat"
mv "$2/$1-5.dat" "$2/Iber_Bedload1D.dat"
mv "$2/$1-6.dat" "$2/Iber_Results.dat"
mv "$2/$1-7.dat" "$2/Iber_Wind.dat"
mv "$2/$1-8.dat" "$2/Iber_Imbornales.dat"
mv "$2/$1-9.dat" "$2/Iber_Internal_cond.dat"
mv "$2/$1-10.dat" "$2/Iber_Susp_sed.dat"
mv "$2/$1-11.dat" "$2/Iber_Pollutants.dat"
mv "$2/$1-12.dat" "$2/Iber_Problemdata.dat"
mv "$2/$1-13.dat" "$2/Iber_Turbulence.dat"
mv "$2/$1-14.dat" "$2/Iber_Encroachment.dat"
mv "$2/$1-15.dat" "$2/Iber_Breach.dat"
mv "$2/$1-16.dat" "$2/Iber_Ratingcurves.dat"
mv "$2/$1-17.dat" "$2/Iber_Variable_n.dat"
mv "$2/$1-18.dat" "$2/Iber_Roof.dat"
mv "$2/$1-19.dat" "$2/Iber_Culverts.dat"
mv "$2/$1-20.dat" "$2/Iber_Discharges.dat"
mv "$2/$1-21.dat" "$2/Iber_SedimentRelation.dat"
mv "$2/$1-22.dat" "$2/Iber_RatingcurveQ.dat"
mv "$2/$1-23.dat" "$2/Iber_Salinity.dat"
mv "$2/$1-24.dat" "$2/Iber_Temperature.dat"
mv "$2/$1-25.dat" "$2/Iber_Coliforms.dat"
mv "$2/$1-26.dat" "$2/Iber_DischargesWQ.dat"
mv "$2/$1-27.dat" "$2/Iber_OD.dat"
mv "$2/$1-28.dat" "$2/Iber_DBO.dat"
mv "$2/$1-29.dat" "$2/Iber_Nitro.dat"
mv "$2/$1-30.dat" "$2/Iber_Hazard.dat"
mv "$2/$1-31.dat" "$2/Iber_Hyetograph.dat"
mv "$2/$1-32.dat" "$2/Iber_DiffuR.dat"
mv "$2/$1-33.dat" "$2/Iber_Wood.dat"
mv "$2/$1-34.dat" "$2/Iber_Woodgage.dat"
mv "$2/$1-35.dat" "$2/Iber_WoodPr.dat"
mv "$2/$1-36.dat" "$2/Iber_AH.dat"
mv "$2/$1-37.dat" "$2/Iber_AH_c.dat"
mv "$2/$1-38.dat" "$2/Iber_AH_subs.dat"
mv "$2/$1-39.dat" "$2/Iber_Aval.dat"
mv "$2/$1-40.dat" "$2/Iber_Fosforo.dat"
mv "$2/$1-41.dat" "$2/Iber_Phyto.dat"
mv "$2/$1-42.dat" "$2/Iber_PH.dat"
mv "$2/$1-43.dat" "$2/Iber_Metal.dat"
mv "$2/$1-44.dat" "$2/Iber_AtmosphericVars.dat"
mv "$2/$1-45.dat" "$2/Iber_AtmosphericLoc.dat"
mv "$2/$1-46.dat" "$2/Iber_SedMix_ErodibleZones.dat"
mv "$2/$1-47.dat" "$2/Iber_SedMix_IniCond.dat"
mv "$2/$1-48.dat" "$2/Iber_SedMix_BC.dat"
mv "$2/$1-49.dat" "$2/Iber_SedMix.dat"
mv "$2/$1-50.dat" "$2/Iber_SoilWaterBalance.dat"
mv "$2/$1-51.dat" "$2/Iber_Gauges.dat"
mv "$2/$1-52.dat" "$2/Iber_Groundwater.dat"
mv "$2/$1-53.dat" "$2/Iber_SedMix_QsB.dat"
mv "$2/$1-54.dat" "$2/Iber_Bedmixtures.dat"
mv "$2/$1-55.dat" "$2/Iber_Bedmixturesprop.dat"
mv "$2/$1-56.dat" "$2/Iber_Sink&Sources.dat"
mv "$2/$1-57.dat" "$2/Iber_Junctions.dat"
mv "$2/$1-58.dat" "$2/Iber_Pipes.dat"
mv "$2/$1-59.dat" "$2/Iber_UD_Param.dat"
mv "$2/$1-60.dat" "$2/Iber_FloodDamage.dat"
mv "$2/$1-61.dat" "$2/Iber_FloodDamageCurves.dat"
mv "$2/$1-62.dat" "$2/Iber_SWMM_roof.dat"
mv "$2/$1-63.dat" "$2/Iber_SWMM_greenroof.dat"
mv "$2/$1-64.dat" "$2/Iber_SWMM.dat"
mv "$2/$1-65.dat" "$2/Iber_H_IC.dat"


# erase results of very old models using carpa
rm -f "$2/$1.flavia.res"
rm -f "$2/$1.flavia.msh"
# not erase results because could use restart or additional calculations
# rm -f "$2/$1.post.res"

if [ ! -e "$2/Iber_GPUSolver.dat" -o ! -s "$2/Iber_GPUSolver.dat" ]
then
  "$3/bin/linux/iber.exe"
else
  "$3/bin/linux/IberPlus.exe"
fi

# "$3/bin/linux/results_1D.exe"
# mv "$2/salida.post.res" "$2/1D.post.res"
