*#---Escribe el archivo de datos para los colectores, sin mallar capa Subsurface--
*# Jos� Luis Jul/2011
*if(strcmp(GenData(IberUrbanDrainagePlugin),"Disabled")==0)

*else
*GenData(tvd)           -Flux limiter
*GenData(CFL)        -CFL
*GenData(Flow_through_manhole_covers)           -Flow through manhole
*GenData(In_loss_coef.)        -In loss coeffcient
*GenData(Out_loss_coef.)        -Out loss coeffcieint
*GenData(Cover_thickness_[m])       -Cover thikness
*GenData(Cover_density_[kg/m3])     -Cover density
*if(strcmp(GenData(Exchange_flow_method),"UPC")==0)
1           - UPC Efficency method
*else
2           - Grates as orifices
*endif
*GenData(mfm)           0:L�mina Libre, 1: Pressmann, 2: Preissmann modificado, 3: Densidad constante, 4: Densidad variable
*if(strcmp(GenData(Friction_Loss_Formula),"Manning")==0)    
0           - Rougness as Manning
*else
1           -Raugness as Darcy-Weisbach
*endif
0           -No air phase
*endif
