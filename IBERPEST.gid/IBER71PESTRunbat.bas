*#GONZALOALEN Nov/2022
*#------ Batchfile con el codigo para ejecutar PEST ----
@echo off

rem check for incompatibilities with the timestep
if *GenData(Timeseries_time_interval_[s]) neq *tcl(Pest::WriteTimestep) (
echo ERROR: Timeseries time interval [s] is different to the timestep of the PEST observations
echo        Change one of them in:
echo               Timeseries time interval [s]: Data - Problem Data - Time Parameters - Timeseries time interval
echo               PEST observations: Iber_Tools - Control file - Observed data - Time [column]
pause
exit
)

rem check for incompatibilities with the number of observations
if *tcl(Pest::WriteIberRowsbatchfile *GenData(Max_simulation_time_[s]) *GenData(Timeseries_time_interval_[s])) neq *tcl(Pest::WriteObsbatchfile) (
echo ERROR: The number of observations defined in PEST is not correct.
echo        Number of PEST observations = *tcl(Pest::WriteObsbatchfile)
echo        Number of Iber outputs defined to compare with PEST observations = *tcl(Pest::WriteIberRowsbatchfile *GenData(Max_simulation_time_[s]) *GenData(Timeseries_time_interval_[s]))
pause
exit
)

*# check if there are variable to calibrate
*tcl(Pest::CheckVar)

*# Si se calibran datos hidrologicos, compruego si el modelo seleccionado en el
*# Problem Data coincide con el seleccionado en PEST
*if(strcmp(GenData(Infiltration_losses),"Off")==0)
*tcl(Pest::CheckHydrologicalModels 0)
*endif
*if(strcmp(GenData(Infiltration_losses),"Manual_/_By_parameters")==0)
*if(strcmp(GenData(Losses_model),"Green_Ampt_")==0)
*tcl(Pest::CheckHydrologicalModels 1)
*elseif(strcmp(GenData(Losses_model),"Horton_")==0)
*tcl(Pest::CheckHydrologicalModels 2)
*elseif(strcmp(GenData(Losses_model),"SCS_")==0)
*tcl(Pest::CheckHydrologicalModels 3)
*elseif(strcmp(GenData(Losses_model),"SCS_continuous")==0)
*tcl(Pest::CheckHydrologicalModels 4)
*elseif(strcmp(GenData(Losses_model),"Lineal_")==0)
*tcl(Pest::CheckHydrologicalModels 5)
*elseif(strcmp(GenData(Losses_model),"Depth_dependent_")==0)
*tcl(Pest::CheckHydrologicalModels 6)
*endif
*endif

rem run pestchek function
echo Iber-PEST: Run pestchek
*tcl(Pest::WriteRunPestchek)

rem Start optimisation of the Iber model with PEST
echo. & echo. & echo. & echo Iber-PEST: Run pest
*tcl(Pest::WriteRunPest)
pause
exit
