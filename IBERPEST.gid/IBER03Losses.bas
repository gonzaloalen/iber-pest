*#------ Archivo Iber_losses.bas ------------------------
*#Geor 1
*#Christian 26/03/2020
*#------ Escribe el archivo de Perdidas ----
*if(strcmp(GenData(Infiltration_losses),"Off")==0)
0
*elseif(strcmp(GenData(Infiltration_losses),"Manual_/_By_parameters")==0)
*if(strcmp(GenData(Losses_model),"Green_Ampt_")==0)
*#if(CondNumEntities==nelem)
4 *GenData(Suction_multiplier) *GenData(T_porosity_multip) *GenData(Ini_saturation_multip) *GenData(Ks_multiplier) *GenData(Initial_loss_multiplier) *GenData(Soil_depth_multiplier) *GenData(Start_time_infiltration)                       
*set cond Green&Ampt *elems
*loop elems *OnlyInCond
*ElemsNum  *cond(Suction_[mm])  *cond(Total_porosity)  *cond(Initial_saturation)  *cond(Ks_[mm/h])  *cond(Initial_loss_[mm])  *cond(Soil_depth_[m])
*end elems
*#else
*#messagebox - An element without Losses was found, please check and run again - Process finished with error -
*#end if
*elseif(strcmp(GenData(Losses_model),"Horton_")==0)
*#if(CondNumEntities==nelem)
3 *GenData(f0_multiplier) *GenData(fc_multiplier) *GenData(K_multiplier) *GenData(Start_time_infiltration) 
*set cond Horton *elems
*loop elems *OnlyInCond
*ElemsNum  *cond(f0_[mm/h])  *cond(fc_[mm/h])  *cond(K)
*end elems
*#else
*#messagebox - An element without Losses was found, please check and run again - Process finished with error -
*#end if
*elseif(strcmp(GenData(Losses_model),"SCS_")==0)
*#if(CondNumEntities==nelem)
2 *GenData(CN_multiplier) *GenData(Ia_coefficient) *GenData(Start_time_infiltration) 
*set cond SCS *elems
*loop elems *OnlyInCond
*ElemsNum  *cond(CN)
*end elems
*#else
*#messagebox - An element without Losses was found, please check and run again - Process finished with error -
*#end if
*elseif(strcmp(GenData(Losses_model),"SCS_continuous")==0)
*#if(CondNumEntities==nelem)
5 *GenData(CN_multiplier) *GenData(Ia_coefficient) *GenData(B_depletion) *GenData(Initial_S_coef) *GenData(Start_time_infiltration) 
*set cond SCS *elems
*loop elems *OnlyInCond
*ElemsNum  *cond(CN)
*end elems
*#else
*#messagebox - An element without Losses was found, please check and run again - Process finished with error -
*#end if
*elseif(strcmp(GenData(Losses_model),"Lineal_")==0)
*#if(CondNumEntities==nelem)
1 *GenData(Ia_multiplier) *GenData(Fi_multiplier) *GenData(Start_time_infiltration) 
*set cond Lineal_model *elems
*loop elems *OnlyInCond
*ElemsNum  *cond(Ia_[mm])  *cond(Fi_[mm/h])
*end elems
*elseif(strcmp(GenData(Losses_model),"Depth_dependent_")==0)
6 *GenData(A_multiplier) *GenData(B_multiplier) *GenData(C_multiplier) *GenData(Start_time_infiltration) 
*set cond Depth_dependent *elems
*loop elems *OnlyInCond
*if(strcmp(cond(Infiltration_model),"Linear")==0)
*ElemsNum 1 *cond(A) *cond(B) 0
*elseif(strcmp(cond(Infiltration_model),"Quadratic")==0)
*ElemsNum 2 *cond(A) *cond(B) *cond(C)
*elseif(strcmp(cond(Infiltration_model),"Logarithmic")==0)
*ElemsNum 3 *cond(A) *cond(B) 0
*elseif(strcmp(cond(Infiltration_model),"Potential")==0)
*ElemsNum 4 *cond(A) *cond(B) 0
*elseif(strcmp(cond(Infiltration_model),"h-I_table")==0)
*ElemsNum 5 *cond(h-I_table_ID) 0 0
*endif
*end elems
Tables
*set var tableid(int)=0
*set var tableids_max(int)=0
*set cond Depth_dependent *elems
*loop elems *OnlyInCond
*set var tableid(int)=cond(h-I_table_ID,int)
*if(tableid(int)>tableids_max(int))
*set var tableids_max(int)=tableid(int)
*endif
*end elems
*operation(tableids_max(int))
*for(i=1;i<=tableids_max(int);i=i+1)
*set var count=0
*set cond Depth_dependent *elems
*loop elems *OnlyInCond
*set var tableid(int)=cond(h-I_table_ID,int)
*if(tableid(int)==i(int))
*if(count==0)
*set var INS(int)=cond(Infiltration_rate,int)
*cond(h-I_table_ID,int) *INS(int)
*set var PASO=2
*for(ii=1;ii<=INS(int);ii=ii+PASO)
*cond(Infiltration_rate,*ii,real) *cond(Infiltration_rate,*Operation(ii+1),real)
*end for
*set var count=1
*endif
*endif
*end elems
*end for
Endtables
*endif
*elseif(strcmp(GenData(Infiltration_losses),"By_zones")==0)
*#else
*#messagebox - An element without Losses was found, please check And run again - Process finished with error -
*#end if
*tcl(LossesModel::WriteCalculationFile)
*set cond Infiltration_Zones *elems
*loop elems *OnlyInCond
*ElemsNum *cond(infzone_id)
*end elems
*endif