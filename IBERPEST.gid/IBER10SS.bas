*# Ernest 1
*#-----Escribe el archivo de sedimento en suspension entrada puntual-----
*set cond Suspended_Source *elems
Source
*loop elems *OnlyInCond
*set var ntm(int)=cond(Discharge_and_Concentration,int)
*for(i=1;i<=ntm(int);i=i+3) 
*ElemsNum *\
*format "%13.0f%13.3f%13.6f"
*cond(Discharge_and_Concentration,*i,real) *cond(Discharge_and_Concentration,*Operation(i+1),real) *cond(Discharge_and_Concentration,*Operation(i+2),real)
*end for 
*end elems
End Source
*#------Escribe el archivo de sedimento en las entradas---
Inlet
*set cond Suspended_Inlet *elems 
*loop elems *OnlyInCond
*set var ntm(int)=cond(Concentration,int)
*for(i=1;i<=ntm(int);i=i+2) 
*ElemsNum *CondElemFace *\
*format "%13.0f%13.6f"
*cond(Concentration,*i,real) *cond(Concentration,*Operation(i+1),real) 
*end for
*end elems
End Inlet
*#------Escribe condiciones iniciales de suspension---
Initial condition
*set cond Suspended_Initial_Condition *elems
*#add cond Rock_Position *elems
*loop elems *OnlyInCond
*format "%8i%20.6f"
*ElemsNum *cond(Sediment_Concentration_[g/l])
*end elems
End Initial condition
Rock
*set cond Rock_Position *elems
*loop elems *OnlyInCond
*format "%8i%12.3f%5i"
*if(strcmp(cond(Position_by),"Depth")==0)
     *ElemsNum*cond(Rock) 1
*else
     *ElemsNum*cond(Rock) 0
*endif
*end elems
End Rock
