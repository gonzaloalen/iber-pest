*if(strcmp(GenData(Wall_friction),"Roughness_Height")==0)
1     0     *GenData(Wall_Roughness)     *GenData(Wall_Manning)                 Friction on walls and Wall Roughness
*elseif(strcmp(GenData(Wall_friction),"Manning_Coefficient")==0)
1     1     *GenData(Wall_Roughness)     *GenData(Wall_Manning)                 Friction on walls and Manning
*elseif(strcmp(GenData(Wall_friction),"No_Friction")==0)
0     0     *GenData(Wall_Roughness)     *GenData(Wall_Manning)                 No Friction on walls
*endif
*if(strcmp(GenData(Turbulence_model),"No_turbulence")==0)
0 0 0 *GenData(Molecular_viscosity_[m2/s]) 0          No turbulence
*elseif(strcmp(GenData(Turbulence_model),"Parabolic")==0)
1 0 0 *GenData(Molecular_viscosity_[m2/s]) 0          Parabolic
*elseif(strcmp(GenData(Turbulence_model),"Mixing_length")==0)
2 0 0 *GenData(Molecular_viscosity_[m2/s])  0         Mixing_length
*elseif(strcmp(GenData(Turbulence_model),"k-eps")==0)
*#if(strcmp(GenData(Wall_functions),"On")==0)
3   0 *GenData(Limit_depth_[m])    *GenData(Molecular_viscosity_[m2/s])    *GenData(ischemeke)   k-epsilon
*elseif(strcmp(GenData(Turbulence_model),"Constant_v")==0)
4 *GenData(Viscosity_coef_[m2/s]) 0 *GenData(Molecular_viscosity_[m2/s])  0     Constant turbulent viscosity
*endif
*if(strcmp(GenData(Suspended_Transport),"Off")==0)
0 0 0 0 0 0 0 0 *GenData(Avalanche_Model_ST) *GenData(Friction_Angle_ST_[rad])  No suspended sediment
*elseif(strcmp(GenData(Suspended_Transport),"On")==0)
*if(strcmp(GenData(Method_),"Van_Rijn")==0)
1   *GenData(Diffusion_coefficient_[m2/s])  *GenData(Schmidt_num_)   *GenData(Diameter_Susp_Sed_[m])    *GenData(Start_time_ST_[s])  *GenData(Erosion_Rate_[m/s]) *GenData(Critical_Erosion_Stress_[N/m2])   *GenData(Bed_conc/Average_conc)  *GenData(Avalanche_Model_ST) *GenData(Friction_Angle_ST_[rad]) *GenData(Critical_Deposition_Stress_[N/m2]) *GenData(Porosity_ST) *GenData(Relative_Density_ST)  Suspended Sediment -Van Rijn-
*elseif(strcmp(GenData(Method_),"Smith_McLean")==0)
2   *GenData(Diffusion_coefficient_[m2/s])  *GenData(Schmidt_num_)   *GenData(Diameter_Susp_Sed_[m])    *GenData(Start_time_ST_[s])  *GenData(Erosion_Rate_[m/s]) *GenData(Critical_Erosion_Stress_[N/m2])   *GenData(Bed_conc/Average_conc)  *GenData(Avalanche_Model_ST) *GenData(Friction_Angle_ST_[rad]) *GenData(Critical_Deposition_Stress_[N/m2]) *GenData(Porosity_ST) *GenData(Relative_Density_ST)  Suspended Sediment -Smith McLean-
*elseif(strcmp(GenData(Method_),"Ariathurai")==0)
3   *GenData(Diffusion_coefficient_[m2/s])  *GenData(Schmidt_num_)   *GenData(Diameter_Susp_Sed_[m])    *GenData(Start_time_ST_[s])  *GenData(Erosion_Rate_[m/s]) *GenData(Critical_Erosion_Stress_[N/m2])   *GenData(Bed_conc/Average_conc)  *GenData(Avalanche_Model_ST) *GenData(Friction_Angle_ST_[rad]) *GenData(Critical_Deposition_Stress_[N/m2]) *GenData(Porosity_ST) *GenData(Relative_Density_ST)  Suspended Sediment -Ariathurai-
*elseif(strcmp(GenData(Method_),"Ariathurai-Lopez")==0)
5   *GenData(Diffusion_coefficient_[m2/s])  *GenData(Schmidt_num_)   *GenData(Diameter_Susp_Sed_[m])    *GenData(Start_time_ST_[s])  *GenData(Erosion_Rate_[m/s]) *GenData(Critical_Erosion_Stress_[N/m2])   *GenData(Bed_conc/Average_conc)  *GenData(Avalanche_Model_ST) *GenData(Friction_Angle_ST_[rad]) *GenData(Critical_Deposition_Stress_[N/m2]) *GenData(Porosity_ST) *GenData(Relative_Density_ST)  *GenData(Max_Erosion_Rate_[m/s]) *GenData(Min_Erosion_Rate_[m/s]) *GenData(Max_Erosion_Stress_[N/m2]) *GenData(Min_Erosion_Stress_[N/m2]) Suspended Sediment -Ariathurai Lopez-
*endif
*endif
*if(strcmp(GenData(Fill_Sinks),"Yes")==0)
*if(strcmp(GenData(Smooth_DTM),"Yes")==0)
*GenData(Dry_elements_depth_buffer_[m])    *GenData(Hydrological_Drying)   1    *GenData(Number_of_passes)   Drying method -*GenData(Drying_Method)- Dry elements negative depth buffer
*elseif(strcmp(GenData(Smooth_DTM),"No")==0)
*GenData(Dry_elements_depth_buffer_[m])    *GenData(Hydrological_Drying)   1    0   Drying method -*GenData(Drying_Method)- Dry elements negative depth buffer
*endif
*elseif(strcmp(GenData(Fill_Sinks),"No")==0)
*if(strcmp(GenData(Smooth_DTM),"Yes")==0)
*GenData(Dry_elements_depth_buffer_[m])    *GenData(Hydrological_Drying)   0    *GenData(Number_of_passes)   Drying method -*GenData(Drying_Method)- Dry elements negative depth buffer
*elseif(strcmp(GenData(Smooth_DTM),"No")==0)
*GenData(Dry_elements_depth_buffer_[m])    *GenData(Hydrological_Drying)   0    0   Drying method -*GenData(Drying_Method)- Dry elements negative depth buffer
*endif
*endif
*if(strcmp(GenData(Bed_Transport),"Off")==0)
0   *GenData(Avalanche_Model_BT)  *GenData(Einstein_Partition)    No Bed Load
*elseif(strcmp(GenData(Bed_Transport),"On")==0)
1   *GenData(Avalanche_Model_BT)   *GenData(Einstein_Partition)      *GenData(Number_M-P&M=1/VR=2)  *GenData(Number_Hiding_Exposure) *GenData(A_Hid) *GenData(B_Hid) *GenData(deltaH) *GenData(deltaR)    BedLoad  Method Hiddening_Function
*endif
*if(strcmp(GenData(Encroachment),"On")==0)
*if(strcmp(GenData(Based_on),"Line_and_width")==0)
1        Line And Width Encroachment
*elseif(strcmp(GenData(Based_on),"Polygon")==0)
2        Polygon Encroachment
*elseif(strcmp(GenData(Based_on),"Hydraulic_criteria")==0)
3        Specific Discharge
*endif
*elseif(strcmp(GenData(Encroachment),"Off")==0)
0        No  Encroachment
*endif
*if(strcmp(Gendata(Strict_Courant_Condition),"Off")==0)
0        Courant Condition Standard
*elseif(strcmp(GenData(Strict_Courant_Condition),"On")==0)
1        Courant Condition Strict
*endif
*GenData(Number_of_Threads)        Number of threads
*if(strcmp(GenData(Force_Results_to_Vertex),"1")==0)
1        Force Results to Vertex
*elseif(strcmp(GenData(Force_Results_to_Vertex),"0")==0)
0        Do not Force Results to Vertex
*endif
*if(strcmp(GenData(No_Results_on_Dry_elem),"1")==0)
1        No results on dry elements
*elseif(strcmp(GenData(No_Results_on_Dry_elem),"0")==0)
0        Results on dry elements
*endif
*if(strcmp(GenData(Write_ASCII_Results),"1")==0)
1        Writes ASCII results in .rep files
*elseif(strcmp(GenData(Write_ASCII_Results),"0")==0)
0        Does not write ASCII results in .rep files
*endif
*if(strcmp(GenData(Salinity),"Off")==0)
0 0 0 0 0 0 0   No Salinity
*elseif(strcmp(GenData(Salinity),"On")==0)
1   *GenData(Dispersion_coef_[m2/s])  *GenData(Schmidt_Number)  0  0  *GenData(sc)  *GenData(Beta_coef)   Salinity
*elseif(strcmp(GenData(Salinity),"Read")==0)
2 0 0 0 0 0 0   Read Salinity
*endif
*if(strcmp(GenData(Temperature),"Off")==0)
0 0 0 0 0 0 0   No Temperature
*elseif(strcmp(GenData(Temperature),"On")==0)
1   *GenData(Dispersion_coef_[m2/s])  *GenData(Schmidt_Number)  0  0  *GenData(sc)  *GenData(Beta_coef)   Temperature
*elseif(strcmp(GenData(Temperature),"Read")==0)
2 0 0 0 0 0 0   Read Temperature
*endif
*if(strcmp(GenData(Coliforms),"Off")==0)
0 0 0 0 0 0 0 0   No Coliforms
*elseif(strcmp(GenData(Coliforms),"On")==0)
1   *GenData(Dispersion_coef_[m2/s])  *GenData(Schmidt_Number)  *GenData(dmc)  0  0  *GenData(sc)  *GenData(Beta_coef)  *GenData(Keluz_[1/m])  *GenData(Hmax_[m])   Coliforms
*endif
*if(strcmp(GenData(Dissolved_Oxygen),"Off")==0)
0 0 0 0 0 0 0   No Dissolved Oxygen
*elseif(strcmp(GenData(Dissolved_Oxygen),"On")==0)
1   *GenData(Dispersion_coef_[m2/s])  *GenData(Schmidt_Number)  0  0  *GenData(sc)  *GenData(Beta_coef)   Dissolved Oxygen
*endif
*if(strcmp(GenData(CBOD),"Off")==0)
0 0 0 0 0 0 0 0 0   No CBOD
*elseif(strcmp(GenData(CBOD),"On")==0)
1   *GenData(Dispersion_coef_[m2/s])  *GenData(Schmidt_Number)  0  0  *GenData(sc)  *GenData(Beta_coef)   *GenData(Degradation_Rate_[1/d])  *GenData(Settling_Velocity_[m/d])     CBOD
*endif
*if(strcmp(GenData(Nitrogen),"Off")==0)
0 0 0 0 0 0 0 0 0 0 0   No Nitrogen
*elseif(strcmp(GenData(Nitrogen),"On")==0)
1   *GenData(Dispersion_coef_[m2/s])  *GenData(Schmidt_Number)  0  0  *GenData(sc)  *GenData(Beta_coef)   *GenData(Ammonification_Rate_[1/d])  *GenData(Nitrification_Rate_[1/d])  *GenData(Denitrification_Rate_[1/d])  *GenData(Organic_Nitrogen_Settling_Vel_[m/d])       Nitrogen
*endif
*# WQ 2.0 (Luis-Marcos 08/2019) - INICIO
*if(strcmp(GenData(Phosphorus),"Off")==0)
0 0 0 0 0 0 0 0 0 0   No Phosphorus
*elseif(strcmp(GenData(Phosphorus),"On")==0)
1   *GenData(Dispersion_coef_[m2/s])  *GenData(Schmidt_Number)  0  0  *GenData(sc)  *GenData(Beta_coef)   *GenData(Hydrolisis_Rate_[1/d])  *GenData(Organic_P_settling_velocity_[m/d])  *GenData(Inorganic_P_settling_velocity_[m/d])     Phosphorus
*endif
*if(strcmp(GenData(Phytoplankton),"Off")==0)
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  No Phytoplankton
*elseif(strcmp(GenData(Phytoplankton),"On")==0)
1   *GenData(Dispersion_coef_[m2/s])  *GenData(Schmidt_Number)  0  0  *GenData(sc)  *GenData(Beta_coef)  *GenData(Phyto_death_rate_[1/d])  *GenData(Phytoplankton_respiration_rate_[1/d])  *GenData(Maximum_photosynthesis_rate_[1/d])  *GenData(Phyto_settling_velocity_[m/d])  *GenData(Phyto_preference_for_amonia)  *GenData(Ratio_Nitrogen_to_ChlA_[mg/mg])  *GenData(Ratio_Phosphorus_to_ChlA_[mg/mg])  *GenData(Ratio_Oxigen_to_ChlA_[mg/mg])  *GenData(Ratio_Carbon_to_ChlA_[mg/mg])  *GenData(Nitrogen_half_saturation_[mg/l])  *GenData(Phosphorus_half_saturation_[mg/l])  *GenData(Carbon_half_saturation_[mg/l])  *GenData(Light_half_saturation_[W/m2])  *GenData(Oxygen_half_saturation_[mg/l])  *GenData(Keluz_photosynthesis_attenuation_[1/m])    Phytoplankton
*endif
*if(strcmp(GenData(pH),"Off")==0)
0 0 0 0 0 0 0 0   No pH
*elseif(strcmp(GenData(pH),"On")==0)
1   *GenData(Dispersion_coef_[m2/s])  *GenData(Schmidt_Number)  0  0  *GenData(sc)  *GenData(Beta_coef)   *GenData(Ratio_DO_consumed_C-org_oxided_C-inorg_[mg/mg])   pH
*endif
*if(strcmp(GenData(Metals),"Off")==0)
0 0 0 0 0 0 0 0   No Metals
*elseif(strcmp(GenData(Metals),"On")==0)
1   *GenData(Dispersion_coef_[m2/s])  *GenData(Schmidt_Number)  0  0  *GenData(sc)  *GenData(Beta_coef)   *GenData(Reaction_rate_[1/d])    Metals
*endif
*# WQ 2.0 (Luis-Marcos 08/2019) - FIN
*GenData(iradwave) *GenData(idispwave) *GenData(epswdwave_[m])  Waves variables
*if(strcmp(Gendata(Baroclinic_Pressure),"No")==0)
0        Baroclinic Pressure Not Activated
*elseif(strcmp(GenData(Baroclinic_Pressure),"Yes")==0)
1        Baroclinic Pressure Activated
*endif
*if(strcmp(GenData(Coriolis),"No")==0)
0  *GenData(Latitude_[degrees])      No Coriolis Forces
*elseif(strcmp(GenData(Coriolis),"Yes")==0)
1   *GenData(Latitude_[degrees])     Coriolis Activated
*endif
*GenData(Results_only_in_steps_of_stepped_discharge,int) *GenData(Tolerance_stepped_discharge_[%])      Stepped discharge
