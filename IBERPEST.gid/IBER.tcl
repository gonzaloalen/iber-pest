#----------- GiD Tcl-Event procedures (its names can't be changed, are predefined by GiD) -------------

proc GiD_Event_InitProblemtype { dir } {
    global IberPriv

    set ::IberPriv(IberGifIco) "TB_Iber.png"
    set ::IberPriv(dir) $dir
    set ::IberPriv(savedir) ""
    set ::IberPriv(Splash) "PT_Splash_Ibersplash.png"

    set data [ReadProblemtypeXml [file join $dir IBER.xml] Infoproblemtype {Version MinimumGiDVersion}]
    if { $data == "" } {
        WarnWinText [= "Configuration file %s not found" [file join $dir IBER.xml]]
        return 1
    }

    array set problemtype_local $data
    set Iber::VersionNumber $problemtype_local(Version)
    foreach varname {AutoCollapseAfterImport AutoImportTolerance LegendsBorder Legend_Height_Percent \
        LegendsOpaque CreateAlwaysNewPoint ImportTolerance SplashWindow UseMoreWindows} {
        set ::IberPriv(prev_$varname) [GiD_Set $varname]
    }

    set ::GID_LOGIN_HIDE 1
    StdBitmaps ;#to refresh toolbar hidding login button

    GiD_Set AutoCollapseAfterImport 0
    GiD_Set AutoImportTolerance 0
    GiD_Set CreateAlwaysNewPoint 0
    GiD_Set ImportTolerance 0.01
    GiD_Set LegendsBorder 1
    GiD_Set Legend_Height_Percent 33
    GiD_Set LegendsOpaque 1
    GiD_Set SplashWindow 0
    GiD_Set UseMoreWindows 1

    foreach varname {InfoGiDBitmapName InfoGiDProc HideVolumeLevel PostSpaceDimension} {
        if { [info exists ::GidPriv($varname)] } {
            set ::IberPriv(prev_GidPriv_$varname) $::GidPriv($varname)
        }
    }

    # To avoid re-build menus several times (because of trace), and avoid crash loading the problemtype in GiD 14 developer
    GiDMenu::DisableUpdateMenus

    if { $::GidPriv(HideVolumeLevel) != 1 } {
        set ::GidPriv(HideVolumeLevel) 1 ;#to disable all volume tools
    }

    # Declare that post is really 2D to do in XY calculation of integrals and streamlines
    if { ![info exists ::GidPriv(PostSpaceDimension)] || $::GidPriv(PostSpaceDimension) != "2D" } {
        set ::GidPriv(PostSpaceDimension) 2D
    }

    GiDMenu::EnableUpdateMenus

    set ::GidPriv(ProgName) $::Iber::ProgramName

    set scriptspath [file join $dir scripts]

    # It is not necessary to source TitRes.tcl, it's only a trick to set some translation for ramtranslator
    # MARCOS2020 - Deberíamos repensar el nombre de los scripts... Asociarlos a sus módulos
    foreach name {        Atmospheric AtmosphericVars \
        Breach Bridge \
        Culvert Custom Rock\
            Discharge DrainageInlets DTM \
        Encroachment ENCcriteria \
            GridData GridRes Groundwater \
                IberswmmRoofs \
                Greenroofs \
        Hyetograph InitialCND \
            IberPreMenu IberUtils \
        Junctions \
            LandUse LIDAR LossesModel Damage Pest\
        Mixtures MixturesLC MixturesC MoveNode \
            Pipes Prop \
        ResultsReports ResultsToRaster ResultsToShape Roughness RTIN \
            Section SelRes SoilErosion SoilWaterStress SourceAndSink Source&Sinks_Aux1 Stratum \
        UDshape \
            Woodgage WUA WUAcustom XSeccAndProf }  {

        set filename [file join $scriptspath $name.tcl]
        if { [file exists $filename] } {
            source $filename
        }
    }

    set myinfo [file join $dir icos PT_Img_infoiber.png]
    set ::GidPriv(InfoGiDBitmapName) $myinfo
    set ::GidPriv(InfoGiDProc) "Iber::About"

    if { ![GidUtils::IsTkDisabled] } {
        if {$::tcl_platform(platform) eq "windows"} {
            set myico [file join $dir icos Iber.ico]
            wm iconbitmap .gid -default $myico
            wm iconbitmap .gid  $myico
        }
        ShowGiDVersionType ;#it is a GiD procedure that refresh the toolbar right logo
        Iber::Splash
        ChangeWindowTitle
        Iber::ChangeMenus
        Iber::AddPostToolbar
        Iber::AddPreToolbar
    }

    ### Recovering variables stored in PRB
    Culvert::FillTclDataFromProblemData
    DrainageInlet::FillTclDataFromProblemData
    SourceSink::FillTclDataFromProblemData
    Discharge::FillTclDataFromProblemData
    Breach::FillTclDataFromProblemData
    Woodgage::FillTclDataFromProblemData
    Hyetograph::FillTclDataFromProblemData
    MixturesC::FillTclDataFromProblemData
    MixturesLC::FillTclDataFromProblemData
    Mixtures::FillTclDataFromProblemData
    WUAcustom::FillTclDataFromProblemData
    SoilErosion::FillTclDataFromProblemData
    Groundwater::FillTclDataFromProblemData
    LossesModel::FillTclDataFromProblemData
    SoilWaterStress::FillTclDataFromProblemData
    AtmosphericVars::FillTclDataFromProblemData
    Damage::FillTclDataFromProblemData
    Pest::FillTclDataFromProblemData
    IberswmmRoofs::FillTclDataFromProblemData
	GreenRoofs::FillTclDataFromProblemData

    set ::IberPriv(drawopengl) [GiD_OpenGL register Iber::ReDraw]

    #cath because old models doesn't have this IberDefault general data field, instead catch will be better to check if exists...
    catch {
        GiD_AccessValue set gendata IberDefault State Enabled
        GiD_AccessValue set gendata IberDefault Enabled
    }
    #Iber::CustomEnableDisable
    #Iber::ModifyDefaultDataMenuCustomPlugins

    #register the proc to be automatically called when re-creating all menus (e.g. when doing files new)
    GiD_RegisterPluginAddedMenuProc Iber::ChangeMenus

    #register iber procedures to be called when a GiD event happen
    Iber::RegisterEvents
}

proc GiD_Event_BeforeMeshGeneration { element_size } {
    if { [GiD_Info Geometry NumSurfaces] } {
        # SwapNormals Surfaces fail in mesh view
        if { [GiD_Info Project ViewMode] != "GEOMETRYUSE" } {
            GiD_Process MEscape Geometry escape
        }
        GiD_Process Mescape Utilities SwapNormals Surfaces SelByNormal 1:end escape 0.0 0.0 1.0 Yes escape
    }
    return 0
}

proc GiD_Event_AfterChangeBackground { } {
    if { [GiD_BackgroundImage get filename] != "" } {
        if { [GiD_BackgroundImage get show] } {
            GiD_Process 'Zoom Frame
        } else {
            GidUtils::SetWarnLine [= "Background image not visible because is set hidden" ]
        }
    }
}

proc GiD_Event_AfterSaveImage { filename type } {
    GidUtils:SaveGeoreferenceFile $filename $type
}

# GiD_Event_BeforeCalculate is raised from 15.1.0d to allow process commands (like renumber)
# before previus queued commands that write calculation files
# is a tricky alternative to GiD_Event_BeforeRunCalculation or GiD_Event_BeforeWriteCalculationFile
proc GiD_Event_BeforeCalculate { isremote } {
    Iber::RenumberMeshIfNecessary
}

proc GiD_Event_BeforeRunCalculation { batfilename basename dir problemtypedir gidexe args } {
    set directory [GiD_Info project ModelName].gid
    set postmesh [file join $directory [ModelName].post.msh]
    if {[file exists $postmesh]} {
        set value3 [GiD_AccessValue get gendata IberWoodPlugin]
        if {$value3 == "Disabled"} {
            file delete $postmesh
        }
    }
}


proc GiD_Event_BeforeWriteCalculationFile { file } {

    ######### CHECKS #########
    ### Encroachment ###
    set ENConoff [GiD_AccessValue get gendata Encroachment]
    set ENCcriteria [GiD_AccessValue get gendata Based_on]
    if { $ENCcriteria == "On" } {
        if { $ENCcriteria == "Hydraulic_criteria" } {
            set directory [GiD_Info project ModelName].gid
            set name "Iber_encroachment_q.dat"
            set file_name [file join $directory $name]
            if { ![file exists $file_name] } {
                W [= "Wrong encroachment criteria selected"]
                W [= "Computation aborted"]
                set value -cancel-
                return $value
            }
            set plugactivated [GiD_AccessValue get gendata IberPlusPlugin]
            if { $plugactivated == "Enabled" } {
                W [= "The encroachment criteria selected is not available. Please select another"]
                W [= "Computation aborted"]
                set value -cancel-
                return $value
            }
        }
    }

    ### BedLoad Mixtures ###
    set BLsedtype [GiD_AccessValue get gendata Sediment_type]
    if { $BLsedtype == "Mixtures" } {

        #Num Class CC <= Num Class CI
        set nclass [MixturesLC::GetNclasses]
        set nclassci [lindex [lindex [lindex [GiD_Info conditions Sediments_mixt_Inlet mesh] 0] 5] 1]
        if { $nclassci != "" } {
            if { [expr $nclassci/2] > $nclass } {
                W [= "Num. classes of Inlet condition (Mixtures) must be equal or lower than Num. classes defined"]
                W [= "Computation aborted"]
                set value -cancel-
                return $value
            }
        }
    }

    ### MESH MATERIALS BY DEFAULT ###
    set element_ids_material_zero [GiD_Mesh list -element_type {triangle quadrilateral} -material 0 Element]
    if { [objarray length $element_ids_material_zero]} {
        # GiD_AssignData material unclassified elements $element_ids_material_zero
        # GidUtils::SetWarnLine [= "Automatic Land Use has been assigned to some elements"]
        W [= "Land Use has not been defined to the following elements: %s" $element_ids_material_zero]
        W [= "Computation aborted"]
        set value -cancel-
        return $value
    }

    ### Hydrology - Rain raster folder ###
    set raintype [GiD_AccessValue get gendata Precipitation]
    if { $raintype == "Rasters" || $raintype == "Rasters_interpolation" } {
        set dirgrids [GiD_AccessValue get gendata Rasters_folder]
        if { $dirgrids == "" } {
            W [= "The directory of the rain raster does not exist. Please, define it (Data >> Problem data)"]
            W [= "Computation aborted"]
            set value -cancel-
            return $value
        }
    }
    ### Check if Plan_ID is enabled. Then warning. Do NOT erase the proc below (future versions) ###
    #proc BeforeMeshGeneration {} {
        #        set gen_data [GiD_Info gendata]
        #        WarnWinText $gen_data
        #        set planONOFF [GiD_Info gendata Simulation_plan]
        #        if {$planONOFF != 0} {
            #                tk_dialogRAM .gid.tmpwin Info \
            #                        "Simulation plan was enabled, the mesh must be the same for all plans" \
                #                        error 0 OK
            #        }
        #}
}

proc GiD_Event_InitGIDPostProcess {} {
    set directory [GiD_Info project ModelName].gid
    set model_name [file tail [GiD_Info project ModelName]]

    set files_to_add ""
    set 1Dfile [file join $directory 1D.post.res]
    if {[file exists $1Dfile]} {
        lappend files_to_add $1Dfile
    }
    set Encfile [file join $directory Int_Desague_${model_name}.post.res]
    if {[file exists $Encfile]} {
        lappend files_to_add $Encfile
    }

    #-----------------------Add the "Plan ID" post files--------------------------------
    #set plannum [GiD_Info GenData]
    #set Planfile [file join $directory Plan_${plannum}_${model_name}.post.res]
    #if {[file exists $Planfile]} {
        #        lappend files_to_add $Planfile
        #}
    #for {set planid 0} {$planid < 1000} {incr 1} {
        #        set Planfile [file join $directory Plan_${planid}_${model_name}.post.res]
        #        if {[file exists $Planfile]} {
            #                lappend files_to_add $Planfile
            #        }
        #}

    if { $files_to_add != "" } {
        GiD_Process MEscape Files Add $files_to_add
    }

    set resfile [file join $directory ${model_name}.post.res]
    if {[file exists $resfile]} {
        #not try to set Hydraylic result if it exists a .post.vv file that will restore the last used result
        set result [Iber::GetResultFromVV  [file join $directory ${model_name}.post.vv]]
        if { $result == "" } {
            after idle ::Iber::SetResultHydraulic
        }
    } else {
        set value [GiD_AccessValue get gendata Simulation_plan]
        if {$value == "Enabled"} {
            tk_dialogRAM .gid.tmpwin Info \
                "Simulation plan was enabled. Please, load the results (Files >> Merge >> Plan_ID_modelname.post.res)" \
                error 0 OK
            return
        }
    }
}

proc GiD_Event_AfterLoadResults { filename } {
    after idle ::Iber::SetResultHydraulic
}

proc GiD_Event_EndProblemtype {} {
    global IberPriv
    GiD_UnRegisterPluginAddedMenuProc Iber::ChangeMenus
    Iber::UnRegisterEvents

    if { [info exists ::IberPriv(drawopengl)] } {
        #catch because there is an error in some gid versions unregistering
        catch { GiD_OpenGL unregister $::IberPriv(drawopengl) }
        unset ::IberPriv(drawopengl)
    }

    #avoid windows that only belongs to this problemtype
    #and also reset variable to avoid errors when creating a new model or reading another one
    if { ![GidUtils::IsTkDisabled] } {
        Culvert::CloseWindow
        DrainageInlet::CloseWindow
        SourceSink::CloseWindow
        Discharge::CloseWindow
        Breach::CloseWindow
        Woodgage::CloseWindow
        Hyetograph::CloseWindow
        Mixtures::CloseWindow
        MixturesC::CloseWindow
        MixturesLC::CloseWindow
        WUAcustom::CloseWindow
        LossesModel::CloseWindow
        Groundwater::CloseWindow
        SoilErosion::CloseWindow
        SoilWaterStress::CloseWindow
        Damage::CloseWindow
        IberswmmRoofs::CloseWindow
        GreenRoofs::CloseWindow
        Pest::CloseWindow
    }
    Culvert::UnsetVariables
    DrainageInlet::UnsetVariables
    SourceSink::UnsetVariables
    Discharge::UnsetVariables
    Breach::UnsetVariables
    Woodgage::UnsetVariables
    Hyetograph::UnsetVariables
    Mixtures::UnsetVariables
    MixturesC::UnsetVariables
    MixturesLC::UnsetVariables
    WUAcustom::UnsetVariables
    LossesModel::UnsetVariables
    Groundwater::UnsetVariables
    SoilErosion::UnsetVariables
    SoilWaterStress::UnsetVariables
    Damage::UnsetVariables
    Pest::UnsetVariables
        IberswmmRoofs::UnsetVariables
        GreenRoofs::UnsetVariables

    if { [info exists ::IberPriv(condfuns)] } {
        foreach i $::IberPriv(condfuns) {
            rename $i {}
            rename $i-base $i
        }
    }
    if { ![GidUtils::IsTkDisabled] } {
        if {$::tcl_platform(platform) eq "windows"} {
            set gidico [file join [gid_filesystem::get_folder_standard themes] gid.ico]
            wm iconbitmap .gid -default $gidico
            wm iconbitmap .gid  $gidico
        }
        ChangeWindowTitle ""
    }
    foreach varname {InfoGiDBitmapName InfoGiDProc HideVolumeLevel PostSpaceDimension} {
        if { [info exists ::IberPriv(prev_GidPriv_$varname)] } {
            if { $::GidPriv($varname) != $::IberPriv(prev_GidPriv_$varname) } {
                set ::GidPriv($varname) $::IberPriv(prev_GidPriv_$varname)
            }
        } else {
            unset -nocomplain ::GidPriv($varname)
        }
    }
    foreach varname {AutoCollapseAfterImport AutoImportTolerance ImportTolerance \
        CreateAlwaysNewPoint UseMoreWindows SplashWindow} {
        if { [GiD_Set $varname] != $::IberPriv(prev_$varname) } {
            GiD_Set $varname $::IberPriv(prev_$varname)
        }
    }

    if { ![GidUtils::IsTkDisabled] } {
        Iber::RemoveToolbar
        ShowGiDVersionType ;#it is a GiD procedure that refresh the toolbar right logo
    }
    unset -nocomplain ::IberPriv
}

proc GiD_Event_SaveModelSPD { filespd } {
    set filename [file rootname $filespd].xml
    Iber::SaveXml $filename
}

proc GiD_Event_LoadModelSPD { filespd } {
    #this event is raised after read the .prb, then could set tcl variables from problem data saved field
    #maybe a better alternative could be to read/save this data in the xml file, not in the prb

    AtmosphericVars::FillTclDataFromProblemData
    Breach::FillTclDataFromProblemData
    Culvert::FillTclDataFromProblemData
    Pest::FillTclDataFromProblemData
    Discharge::FillTclDataFromProblemData
    DrainageInlet::FillTclDataFromProblemData
    Groundwater::FillTclDataFromProblemData
    Hyetograph::FillTclDataFromProblemData
    LossesModel::FillTclDataFromProblemData
    MixturesC::FillTclDataFromProblemData
    MixturesLC::FillTclDataFromProblemData
    Mixtures::FillTclDataFromProblemData
    SoilErosion::FillTclDataFromProblemData
    SoilWaterStress::FillTclDataFromProblemData
    SourceSink::FillTclDataFromProblemData
    Woodgage::FillTclDataFromProblemData
    WUAcustom::FillTclDataFromProblemData
    Damage::FillTclDataFromProblemData
    Pest::FillTclDataFromProblemData
    IberswmmRoofs::FillTclDataFromProblemData
    GreenRoofs::FillTclDataFromProblemData

    set filename [file rootname $filespd].xml
    set model_iber_version_number [Iber::ReadXml $filename]
    if { $model_iber_version_number != -1 && $model_iber_version_number < $::Iber::VersionNumber } {
        set must_transform 1
    } else {
        set must_transform 0
    }
    if { $must_transform } {
        after idle [list Iber::Transform $model_iber_version_number $::Iber::VersionNumber]
    }
}

#------------------------- Iber procedures ------------------------------------
namespace eval Iber {
    variable ProgramName IBER
    variable VersionNumber ;#interface version, get it from xml to avoid duplication
    variable Web http://www.iberaula.es
}

proc Iber::RegisterEvents { } {
    #instead of directly define the event proc name can register another proc to be called
    #except the initial event GiD_Event_InitProblemtype because Iber::RegisterEvents is called after this event happen
    #GiD_Event_InitProblemtype
    #GiD_Event_BeforeMeshGeneration
    #GiD_Event_AfterSaveImage
    #GiD_Event_BeforeCalculate
    #GiD_Event_BeforeRunCalculation
    #GiD_Event_BeforeWriteCalculationFile
    #GiD_Event_InitGIDPostProcess
    #GiD_Event_AfterLoadResults
    #GiD_Event_EndProblemtype
    #GiD_Event_SaveModelSPD
    #GiD_Event_LoadModelSPD
        GiD_RegisterEvent GiD_Event_AfterOpenFile Iber::ShapefileReadDBF PROBLEMTYPE IBER
}

proc Iber::ShapefileReadDBF { filename format fail } {
        Iber::Shapefile_convert_gid_conditions
}

proc Iber::UnRegisterEvents { } {
    GiD_UnRegisterEvents PROBLEMTYPE IBER
}

proc Iber::SaveXml { filename } {
    set fp [open $filename w]
    if { $fp != "" } {
        puts $fp {<?xml version='1.0' encoding='utf-8'?><!-- -*- coding: utf-8;-*- -->}
        puts $fp "<$::Iber::ProgramName version='$::Iber::VersionNumber'/>"
        close $fp
    }
}

#return -1 if the xml is not an IBER one
#        0 if the xml not exists
#        the IBER version of the model of the xml file
proc Iber::ReadXml { filename } {
    set model_iber_version_number 0
    if { [file exists $filename] } {
        set fp [open $filename r]
        if { $fp != "" } {
            set line ""
            gets $fp header
            gets $fp line ;#something like: <IBER version='1.0'/>
            close $fp
            set line [string range $line 1 end-2]
            set model_program [lindex $line 0]
            if { $model_program == $::Iber::ProgramName } {
                set model_version [lindex $line 1]
                if { [lindex [split $model_version =] 0] == "version" } {
                    set model_iber_version_number [string range [lindex [split $model_version =] 1] 1 end-1]
                }
            } else {
                set model_iber_version_number -1
            }
        }
    }
    return $model_iber_version_number
}

proc Iber::Transform { from_version to_version } {
    variable ProgramName
    GiD_Process escape escape escape escape Data Defaults TransfProblem $ProgramName escape
}

#get the result name to be restored in the .post.vv file if any
proc Iber::GetResultFromVV { vvpostfile } {
    set result ""
    if { [file exists $vvpostfile] } {
        set fp [open $vvpostfile]
        if { $fp != "" } {
            set content [read $fp]
            foreach line [split $content \n] {
                if { [string range [string trim $line] 0 10] == "ResultView:" } {
                    set result [lindex $line 3]
                    break
                }
            }
            close $fp
        }
    }
    return $result
}

proc Iber::ReDraw { } {
    set view_mode [GiD_Info Project ViewMode]
    if { $view_mode == "GEOMETRYUSE" || $view_mode == "MESHUSE"} {
        Atmospheric::ReDraw
        Breach::ReDraw
        Culvert::ReDraw
        Discharge::ReDraw
        DrainageInlet::ReDraw
        Hyetograph::ReDraw
        LossesModel::ReDraw
        SourceSink::ReDraw
        Woodgage::ReDraw
    } elseif { $view_mode == "POSTUSE" } {
        #draw culverts also in postprocess
        Culvert::ReDraw
        Discharge::ReDraw
        IberswmmRoofs::ReDraw
        GreenRoofs::ReDraw
    }
}

proc Iber::SetResultHydraulic {} {
    set allstep ""
    if { [catch { set allstep [GiD_Info postprocess get all_steps Hydraulic] }] } {
        #do nothing
    } else {
        if { [llength $allstep] } {
            GiD_Process Mescape results analysissel Hydraulic [lindex $allstep end]
        }
    }
}

proc Iber::Splash { } {
    global IberPriv

    set prev_splash_state [GiD_Set SplashWindow]
    GiD_Set SplashWindow 1 ;#set temporary to 1 to force show splash without take care of the GiD splash preference
    ::GidUtils::Splash [file join $::IberPriv(dir) icos $::IberPriv(Splash)] .splash 1 \
        [list "$::Iber::ProgramName Version $::Iber::VersionNumber" 370 500]
    GiD_Set SplashWindow $prev_splash_state
}

proc Iber::About { } {
    global IberPriv

    set prev_splash_state [GiD_Set SplashWindow]
    GiD_Set SplashWindow 1
    ::GidUtils::Splash [file join $::IberPriv(dir) icos $::IberPriv(Splash)] .splash 0 \
        [list "$::Iber::ProgramName Version $::Iber::VersionNumber" 370 500]
    GiD_Set SplashWindow $prev_splash_state
}

proc Iber::RemoveMenu { } {
    GiDMenu::RemoveOption "Help" [list "Register GiD"] PREPOST _
    GiDMenu::RemoveOption "Help" [list "Register problem type"] PREPOST _
    GiDMenu::RemoveOption "Help" [list "Register from file"] PREPOST _
    GiDMenu::RemoveOption "Help" [list "---"] PREPOST _
}

proc Iber::SetDataMenu { } {
    global IberPriv

    set dir $::IberPriv(dir)
    GidChangeDataLabel "Data units" ""
    GidChangeDataLabel "Interval" ""
    GidChangeDataLabel "Conditions" ""
    GidChangeDataLabel "Materials" ""
    GidChangeDataLabel "Interval Data" ""
    GidChangeDataLabel "Problem Data" ""
    GidChangeDataLabel "Local axes" ""
    Iber::SetDefaultDataMenu
    #to re-add the custom enabled plugins to menus when rebuild menus (e.g. change current languaje)
    #Iber::CustomEnableDisable
    Iber::ModifyDefaultDataMenuCustomPlugins
}

proc Iber::SetDefaultDataMenu { } {
    global IberPriv
    GidClearUserDataOptions

    GidAddUserDataOptions "---" "" end
    GidAddUserDataOptions [= "Problem Data"]... "GidOpenProblemData Data" end
    GidAddUserDataOptions "---" "" end
    GidAddUserDataOptionsMenu [= "Hydrodynamics"] [list Iber::DataMenu boundary %W] end
    GidAddUserDataOptions "---" "" end
    GidAddUserDataOptionsMenu [= "Roughness"] [list Iber::DataMenu properties %W] end
    GidAddUserDataOptionsMenu [= "Turbulence"] [list Iber::DataMenu turbulencia %W] end
    GidAddUserDataOptionsMenu [= "Wind"] [list Iber::DataMenu wind %W] end

    ### Hidden menus, visible once it corresponding plugin is enabled
    # GidAddUserDataOptions "---" "" end
    # GidAddUserDataOptionsMenu [= "Atmospheric"] [list Iber::DataMenu atmospheric %W] end
    # GidAddUserDataOptions "---" "" end
    # GidAddUserDataOptionsMenu [= "Breach"] [list Iber::DataMenu breach %W] end
    # GidAddUserDataOptions "---" "" end
    # GidAddUserDataOptionsMenu [= "Habitat"] [list Iber::DataMenu aquatichabitat %W] end
    # GidAddUserDataOptions "---" "" end
    # GidAddUserDataOptionsMenu [= "Hydrological Processes"] [list Iber::DataMenu hms %W] end
    # GidAddUserDataOptions "---" "" end
    # GidAddUserDataOptionsMenu [= "Non Newtonian"] [list Iber::DataMenu nonnewtonian %W] end
    # GidAddUserDataOptions "---" "" end
    # GidAddUserDataOptionsMenu [= "Sediment Transport"] [list Iber::DataMenu seds %W] end
    # GidAddUserDataOptions "---" "" end
    # GidAddUserDataOptionsMenu [= "Soil Erosion"] [list Iber::DataMenu soilerosion %W] end
    # GidAddUserDataOptions "---" "" end
    # GidAddUserDataOptionsMenu [= "Urban Drainage"] [list Iber::DataMenu urbandrainage %W] end
    # GidAddUserDataOptions "---" "" end
    # GidAddUserDataOptionsMenu [= "Water Quality"] [list Iber::DataMenu waterquality %W] end
    # GidAddUserDataOptions "---" "" end
    # GidAddUserDataOptionsMenu [= "Wood"] [list Iber::DataMenu wood %W] end
}

proc Iber::DataMenu { what menu } {
    if { [$menu index end] ne "none" } { return }

    switch $what {
        boundary {
            set IberUrbanDrainagePlugin [GiD_AccessValue get gendata IberUrbanDrainagePlugin]
            if { $IberUrbanDrainagePlugin == "Enabled" } {
                #Generate the menus corresponding to IberUD
                $menu add command -label [= "Boundary Conditions Network"]... -command [list GidOpenConditions 1D_Analysis]
                $menu add command -label [= "Initial Conditions Network"]... -command [list GidOpenConditions Initial_Condition_1D]
                $menu add separator
                $menu add command -label [= "Boundary Conditions"]... -command [list GidOpenConditions 2D_Analysis]
                $menu add command -label [= "Initial Conditions"]... -command [list GidOpenConditions Initial_Condition_2D]
                $menu add command -label [= "Internal Conditions"]... -command [list GidOpenConditions Internal_Condition]
                $menu add command -label [= "Source and Sink"]... -command [list SourceSink::Window]
                $menu add cascade -label [= "Structures"] -menu $menu.structures
                menu $menu.structures -tearoff 0
                $menu.structures add command -label [= "Bridge"]... -command [list GidOpenConditions Bridge]
                $menu.structures add command -label [= "Culvert"]... -command [list Culvert::Window]
                $menu.structures add command -label [= "Lid"]... -command [list GidOpenConditions Lid]
            } else {
                #Generate the menus corresponding to other Ibers
                $menu add command -label [= "Boundary Conditions"]... -command [list GidOpenConditions 2D_Analysis]
                $menu add command -label [= "Initial Conditions"]... -command [list GidOpenConditions Initial_Condition_2D]
                $menu add command -label [= "Internal Conditions"]... -command [list GidOpenConditions Internal_Condition]
                $menu add command -label [= "Source and Sink"]... -command [list SourceSink::Window]
                $menu add cascade -label [= "Structures"] -menu $menu.structures
                menu $menu.structures -tearoff 0
                $menu.structures add command -label [= "Bridge"]... -command [list GidOpenConditions Bridge]
                $menu.structures add command -label [= "Culvert"]... -command [list Culvert::Window]
                $menu.structures add command -label [= "Lid"]... -command [list GidOpenConditions Lid]
            }
        }
        hms {
            $menu add cascade -label [= "Rainfall"] -menu $menu.rain
            menu $menu.rain -tearoff 0
            $menu.rain add command -label [= "Hyetograph definition"]... -command [list Hyetograph::Window]
            $menu.rain add command -label [= "Hyetograph assignation"]... -command [list GidOpenConditions Rainfall]
            $menu add cascade -label [= "Infiltration losses"] -menu $menu.losses
            menu $menu.losses -tearoff 0
            $menu.losses add command -label [= "Manual assignation"]... -command [list GidOpenConditions Infiltration_manual_assignation]
            $menu.losses add separator
            $menu.losses add cascade -label [= "Automatic assignation by parameters"] -menu $menu.automlosses
            menu $menu.automlosses -tearoff 0
            $menu.automlosses add command -label [= "SCS"]... -command [list Iber::AssignLossesModelWin SCS [list CN]]
            $menu.automlosses add command -label [= "Lineal model"]... -command [list Iber::AssignLossesModelWin Lineal_model [list Ia Fi]]
            $menu.automlosses add command -label [= "Green&Ampt"]... -command [list Iber::AssignLossesModelWin Green&Ampt [list Suction Total_porosity Initial_saturation Ks Initial_loss Soil_depth]]
            $menu.losses add separator
            $menu.losses add cascade -label [= "Automatic assignation by zones"] -menu $menu.infbyzones
            menu $menu.infbyzones -tearoff 0
            $menu.infbyzones add command -label [= "Infiltration zones definition"]... -command [list LossesModel::ZonaDistModelWindow]
            $menu.infbyzones add command -label [= "Infiltration zones assignation"]... -command [list Iber::AssignInfiltrationZoneWin infzone_id]
            $menu add cascade -label [= "Groundwater flow"] -menu $menu.groundwater_flow
            menu $menu.groundwater_flow -tearoff 0
            $menu.groundwater_flow add cascade -label [= "Distributed model"] -menu $menu.distributed
            menu $menu.distributed -tearoff 0
            $menu.distributed add command -label [= "Boundary conditions"]... -command [list GidOpenConditions Distributed_model_boundary_conditions]
            $menu.distributed add command -label [= "Parameters"]... -command [list GidOpenConditions Distributed_model_parameters]
            $menu.groundwater_flow add separator
            $menu.groundwater_flow add cascade -label [= "Lumped model"] -menu $menu.lumped
            menu $menu.lumped -tearoff 0
            $menu.lumped add command -label [= "Reservoir definition"]... -command [list Groundwater::Window]
            $menu.lumped add command -label [= "Reservoir assignation"]... -command [list GidOpenConditions Reservoir_assignation]
            $menu add cascade -label [= "Soil water stress"] -menu $menu.soilwaterstress
            menu $menu.soilwaterstress -tearoff 0
            $menu.soilwaterstress add command -label [= "Crop definition"]... -command [list SoilWaterStress::Window]
            $menu.soilwaterstress add command -label [= "Crop assignation"]... -command [list GidOpenConditions Crop_assignation]
        }
        wind {
            $menu add command -label [= "Wind"]... -command [list GidOpenConditions Wind]
        }
        iberswmm {
            $menu add cascade -label [= "Roofs"] -menu $menu.roofswmm
            menu $menu.roofswmm -tearoff 0
            $menu.roofswmm add command -label [= "Roofs"] -command [list GidOpenConditions Roofs_IberSWMM]
            $menu.roofswmm add command -label [= "Import roofs"]... -command [list DoFilesImport Shapefile Shapefile {.shp} [list [_ "Import options"] BrowserExtraCreateImportShapefile BrowserExtraGetImportShapefile]]
            #$menu.roofswmm add command -label [= "Import roofs"] -command [list UDshape::ImportSHP] 
        }
        suds {
            $menu add cascade -label [= "Green roofs"] -menu $menu.greenroofs
            menu $menu.greenroofs -tearoff 0
            $menu.greenroofs add command -label [= "Green roof definition"] -command [list GreenRoofs::Window]
            $menu.greenroofs add command -label [= "Green roof assignation"] -command [list GidOpenConditions GreenRoof_assignation]
        }
        seds {
            $menu add cascade -label [= "Rock Layer Position"]... -menu $menu.rock
            menu $menu.rock -tearoff 0
            $menu.rock add command -label [= "Manual"]... -command [list GidOpenConditions Rock_Position]
            $menu.rock add cascade -label [= "Automatic"] -menu $menu.autorock
            menu $menu.autorock -tearoff 0
            $menu.autorock add command -label [= "Depth"]... -command [list Iber::AssignRockWin Rock_Position [list Rock] Depth]
            $menu.autorock add command -label [= "Elevation"]... -command [list Iber::AssignRockWin Rock_Position [list Rock] Elevation]
            $menu add separator
            $menu add cascade -label [= "BedLoad Uniform"] -menu $menu.bedloaduni
            menu $menu.bedloaduni -tearoff 0
            $menu.bedloaduni add command -label [= "Boundary Cond Uniform"]... -command [list GidOpenConditions Sediments]
            $menu add cascade -label [= "BedLoad Mixtures"] -menu $menu.bedloadmixt
            menu $menu.bedloadmixt -tearoff 0
            $menu.bedloadmixt add command -label [= "Boundary Cond Mixtures"]... -command [list GidOpenConditions Sediment_mixtures]
            $menu.bedloadmixt add separator
            $menu.bedloadmixt add command -label [= "Stratums&Classes"]... -command [list MixturesLC::Window]
            $menu.bedloadmixt add command -label [= "Classes definition"]... -command [list MixturesC::Window]
            $menu.bedloadmixt add command -label [= "Mixtures properties"]... -command [list Mixtures::Window]
            $menu.bedloadmixt add command -label [= "Mixtures assignation"]... -command [list GidOpenConditions Mixtures_assignation]
            $menu add separator
            $menu add command -label [= "Susp Sed at inlet"]... -command [list GidOpenConditions Suspended_Inlet]
            $menu add command -label [= "Susp Sed Source"]... -command [list GidOpenConditions Suspended_Source]
            $menu add command -label [= "Susp Sed Initial Cond"]... -command [list GidOpenConditions Suspended_Initial_Condition]
        }
        contam {
            $menu add command -label [= "Susp Pollutant inlet"]... -command [list GidOpenConditions Pollutant_Inlet]
            $menu add command -label [= "Susp Pollutant Source"]... -command [list GidOpenConditions Pollutant_Source]
        }
        turbulencia {
            $menu add command -label [= "K-epsilon Model"]... -command [list GidOpenConditions K-epsilon_model]
        }
        properties {
            $menu add command -label [= "Land Use"]... -command [list GidOpenMaterials Land_Use]
                GiD_DataBehaviour materials Land_Use geomlist {surfaces}
            $menu add command -label [= "Variable Manning"]... -command [list GidOpenMaterials Variable_Manning]
                GiD_DataBehaviour materials Variable_Manning geomlist {surfaces}
            $menu add separator
            $menu add command -label [= "Automatic Assignation"]... -command [list Iber::AssignLandUseWin]
        }
        waterquality {
            $menu add command -label [= "Boundary Conditions"]... -command [list GidOpenConditions Water_Quality_Boundary_Condition]
            $menu add command -label [= "Initial Conditions"]... -command [list GidOpenConditions Water_Quality_Initial_Condition]
            $menu add command -label [= "Discharges"]... -command [list Discharge::Window]
            $menu add command -label [= "Sediment Oxygen Demand"]... -command [list GidOpenConditions Sediment_Oxygen_Demand_Cond]
        }
        atmospheric {
            $menu add command -label [= "Atmospheric Vars"]... -command [list AtmosphericVars::Window]
            $menu add command -label [= "Atmospherics Assignation"]... -command [list GidOpenConditions Atmospherics]
        }
        soilerosion {
            $menu add command -label [= "Boundary Conditions"]... -command [list GidOpenConditions SoilErosion_Boundary_Condition]
            $menu add command -label [= "Erodible Zones"]... -command [list SoilErosion::Window]
            $menu add command -label [= "Manual Erodible Zone Assignation"]... -command [list GidOpenConditions Erosion_Zone]
            $menu add separator
            $menu add command -label [= "Automatic Erodible Zone Assignation"]... -command [list Iber::AssignErosionZoneWin zone_id]
        }
        breach {
            $menu add command -label [= "Breach Definition"]... -command [list Breach::Window]
        }
        urbandrainage {
            $menu add command -label [= "Import Network"]... -command [list DoFilesImport Shapefile Shapefile {.shp} [list [_ "Import options"] BrowserExtraCreateImportShapefile BrowserExtraGetImportShapefile]]
            $menu add command -label [= "Drainage network"]... -command [list GidOpenConditions Drainage_network]
            $menu add separator
            $menu add command -label [= "Import Inlets file"]... -command [list rejasWin]
            $menu add command -label [= "Drainage Inlets Definition"]... -command [list DrainageInlet::Window]
            # $menu add command -label [= "Roofs drainage"]... -command [list GidOpenConditions Roofs_drainage]
            # $menu add command -label [= "Create nodes 2D"]... -command Prueba3
        }
        wood {
            $menu add command -label [= "Wood Boundary Conditions"]... -command [list GidOpenConditions Wood_boundary]
            $menu add command -label [= "Wood gage"]... -command [list Woodgage::Window]
        }
        aquatichabitat {
            $menu add command -label [= "Custom Suit. curves"]... -command [list GidOpenMaterials {Custom_Suitability}]
            GiD_DataBehaviour materials Custom_Suitability hide {assign draw unassign}
        }
        nonnewtonian {
            $menu add command -label [= "Rock Layer Position"]... -command [list GidOpenConditions Rock_Position]
            $menu add separator
            $menu add cascade -label [= "Automatic Rock Elevation Assign"]... -menu $menu.rock
            menu $menu.rock -tearoff 0
            $menu.rock add command -label [= "Elevation"]... -command [list Iber::AssignRockWin Elevation [list Rock]]
        }
        default {
            WarnWinText "Iber::DataMenu: unexpected case $what"
        }
    }
}

# [= "Iber_Tools"]
# [= "DTM"]
# [= "Merge-Split DTM"]
# [= "Import DTM"]
# [= "Lines"]
# [= "Points"]
# [= "Surface"]
# [= "LAZ to DTM"]
# [= "RTIN"]
# [= "Create RTIN"]
# [= "Geometry"]
# [= "Triangles from Points"]
# [= "Mesh"]
# [= "Edit"]
# [= "Mesh Connecting Points"]
# [= "Set elevation from file"]
# [= "Set elevation constant"]
# [= "Set elevation offset"]
# [= "One element by surface"]
# [= "Structure in Mesh"]
# [= "Bridge"]
# [= "Levee"]
# [= "Breach"]
# [= "Encroachment"]
# [= "Select Line"]]
# [= "Select Polygon"]
# [= "Plug-ins"]
# [= "Results to Raster"]
# [= "XSections and Profiles"]
# [= "Create"]
# [= "View Results"]
# [= "Maximum"]
# [= "Instant"]
# [= "Hydrograph"]
# [= "VID criteria"]

proc Iber::InsertToolsMenu { } {
    set bitmaps_path [file join $::IberPriv(dir) icos]
    GiDMenu::Create "Iber_Tools" PREPOST -1 =

    ### PRE ###
    GiDMenu::InsertOption "Iber_Tools" [list "DTM"] 0 PRE "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "DTM" "Merge-Split DTM..."] 0 PRE CreateDTMWin "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "DTM" "Import DTM"] 1 PRE "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "DTM" "Import DTM" "Points..."] 0 PRE [list DTMWindow [= Points]] "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "DTM" "Import DTM" "Lines..."] 1 PRE [list DTMWindow [= Lines]] "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "DTM" "Import DTM" "Surface..."] 2 PRE [list DTMWindow [= Surface]] "" "" replace =
    if { [LIDAR::IsAvailable] } {
        GiDMenu::InsertOption "Iber_Tools" [list "LIDAR"] 1 PRE "" "" "" replace =
    }
    if { [LIDAR::IsAvailable] } {
        GiDMenu::InsertOption "Iber_Tools" [list "LIDAR" "LAZ to DTM..."] 0 PRE LIDAR::CreateLIDARWin "" "" replace =
    }
    GiDMenu::InsertOption "Iber_Tools" [list "RTIN"] 2 PRE "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "RTIN" "Create RTIN..."] 0 PRE CreateRTINWin "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" "---" 10 PRE "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Geometry"] 11 PRE "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Geometry" "Triangles from Points"] 0 PRE ConnectPointsGeom "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Mesh"] 12 PRE "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Mesh" "Edit"] 0 PRE "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Mesh" "Mesh Connecting Points"] 1 PRE ConnectPointsMesh "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Mesh" "Edit" "Set elevation from file..."] 0 PRE MoveNode::LeeGrid "" [file join $bitmaps_path "TB_Pre_DTM.png"] replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Mesh" "Edit" "Set elevation constant..."] 1 PRE MoveNode::SetElevationConstantWin "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Mesh" "Edit" "Set elevation offset..."] 2 PRE MoveNode::SetHeightConstantWin "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Mesh" "One element by surface"] 2 PRE OneElementMesh "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Mesh" "Structure in Mesh"] 3 PRE "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Mesh" "Structure in Mesh" "Bridge..."] 0 PRE {Bridge::Window bridge} "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Mesh" "Structure in Mesh" "Levee..."] 1 PRE {Bridge::Window levee} "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" "---" 20 PRE "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Breach"] 21 PRE Breach::Window "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Encroachment"] 22 PRE "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Encroachment" "Select Line"] 0 PRE [list Encroachment::Win 2] "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Encroachment" "Select Polygon"] 1 PRE [list Encroachment::Win 1] "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" "---" 30 PRE "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Flood damage"] 31 PRE "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Flood damage" "Max damage"] 0 PRE "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Flood damage" "Max damage" "Manual Assignation..."] 0 PRE [list GidOpenConditions Flood_damage] "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Flood damage" "Max damage" "Automatic Assignation..."] 1 PRE [list Damage::AssignMaxDaRasterWin Max_Damage_\[Euro/m2\]] "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Flood damage" "Damage curves"] 1 PRE "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Flood damage" "Damage curves" "Definition..."] 0 PRE [list Damage::Window] "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Flood damage" "Damage curves" "Manual Assignation..."] 1 PRE [list GidOpenConditions Damage_curves_assignation] "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Flood damage" "Damage curves" "Automatic Assignation..."] 2 PRE [list Damage::AssignDamageCurveRasterWin Damage_Curve] "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" "---" 40 PRE "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "PEST"] 41 PRE "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "PEST" "PEST path"] 0 PRE {Pest::Window set1} "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "PEST" "Parameters to calibrate"] 1 PRE {Pest::Window par} "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "PEST" "Observation data"] 2 PRE {Pest::Window output} "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "PEST" "PEST Settings"] 3 PRE {Pest::Window set2} "" "" replace =
   
    GiDMenu::InsertOption "Iber_Tools" "---" 100 PRE "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Plug-ins..."] 101 PRE Iber::PluginsWin "" [file join $bitmaps_path "TB_Pre_Plugins"] replace =

    ### POST ###
    GiDMenu::InsertOption "Iber_Tools" [list "Results to Raster..."] 0 POST ::PostRaster::ResultsToRasterWin  "" [file join $bitmaps_path "TB_Post_ResToRaster"] replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Results to Shapefile..."] 1 POST ResultsToShapefile::WritePostW "" [file join $bitmaps_path "TB_Post_ResToShape"] replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Reports"] 2 POST "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Reports" "Basic..."] 0 POST Reports::GaugesReportBasicWin "" "" replace =
    #GiDMenu::InsertOption "Iber_Tools" [list "Reports" "Extended..."] 1 POST Reports::GaugesReportExtended "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" "---" 10 POST "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "XSections and Profiles"] 20 POST "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "XSections and Profiles" "Create"] 0 POST XsAndProf::CreateProfiles "" [file join $bitmaps_path "TB_Post_sections"] replace =
    GiDMenu::InsertOption "Iber_Tools" [list "XSections and Profiles" "View Results"] 1 POST "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "XSections and Profiles" "View Results" "Maximum"] 0 POST XsAndProf::ViewProfileMaxRes "" [file join $bitmaps_path "TB_Post_secmax"] replace =
    GiDMenu::InsertOption "Iber_Tools" [list "XSections and Profiles" "View Results" "Instant"] 1 POST XsAndProf::ViewProfileResWin "" [file join $bitmaps_path "TB_Post_secc"] replace =
    GiDMenu::InsertOption "Iber_Tools" [list "XSections and Profiles" "View Results" "Hydrograph"] 2 POST XsAndProf::ResultHydrograph "" [file join $bitmaps_path "TB_Post_hydro"] replace =
    GiDMenu::InsertOption "Iber_Tools" "---" 30 POST "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Encroachment criteria"] 40 POST ENCcriteria "" [file join $bitmaps_path "TB_Post_ENCcriteria"] replace =
}

proc Iber::InsertHelpMenu { } {
    GiDMenu::InsertOption "Help" [list [_ "Visit %s web" $::Iber::ProgramName]...] 7 PREPOST [list VisitWeb $::Iber::Web] "" "" insertafter _
    GiDMenu::InsertOption "Help" [list [concat [_ "About"] " " $::Iber::ProgramName ...]] end PREPOST Iber::About "" "" insertafter _
}

proc Iber::ChangeMenus {} {
    Iber::RemoveMenu
    #must call Iber::InsertToolsMenu before Iber::SetDataMenu (that can also add plugin sub-options to Iber_Tools menu)
    Iber::InsertToolsMenu
    #Iber::SetDataMenu ;#dark trick: to it after 500 because other after idle of GiD is calling FillMenuData and is changing this menu again (and changing theme or theme size remain the  GiD data menu)!!
    Iber::InsertHelpMenu
    after 500 "Iber::SetDataMenu ; GiDMenu::UpdateMenus"
}

proc Iber::Help { } {
    Iber::About
}

# Barra Iber PRE-proceso
proc Iber::AddPreToolbar { { type "DEFAULT INSIDELEFT" } } {
    global IberPreToolbarNames IberPreToolbarCommands IberPreToolbarHelp IberPrePriv

    set IberPreToolbarNames(0) [list TB_Pre_Plugins.png \
            --- \
            TB_Pre_BGImage.png \
            TB_Pre_BGImageSH.png \
            --- \
            TB_Pre_mesh.png \
            TB_Pre_DTM.png \
            --- \
            TB_Pre_calc.png \
            TB_Pre_info.png \
            TB_Pre_cancelcalc.png \
            --- \
            $::IberPriv(IberGifIco)]
    set IberPreToolbarCommands(0) [list  "-np- Iber::PluginsWin" \
            "" \
            "-np- IberPreMenu::BackgroundImage" \
            "-np- IberPreMenu::SwapShowBackgroundImage" \
            "" \
            "-np- IberPreMenu::CreateMesh" \
            "-np- MoveNode::LeeGrid" \
            "" \
            "-np- IberPreMenu::CalculateIber" \
            "-np- PWViewOutput" \
            "-np- IberPreMenu::CancelCalculateIber" \
            "" \
            "-np- Iber::About"]
    set IberPreToolbarHelp(0) [list [= "Plug-ins"] \
            "" \
            [= "Background Image"] \
            [= "Show/Hide Background Image"] \
            "" \
            [= "Generate mesh"] \
            [= "Set elevation from file"] \
            "" \
            [= "Calculate"] \
            [= "View process info"] \
            [= "Cancel process"] \
            "" \
            "Iber $::Iber::VersionNumber"]

    # prefix values:
    #          Pre        Only active in the preprocessor
    #          Post       Only active in the postprocessor
    #          PrePost    Active Always
    set prefix Pre

    set bitmaps_path [file join $::IberPriv(dir) icos]
    set IberPrePriv(toolbarwin) [CreateOtherBitmaps IberPreBar "Iber Pre- toolbar" IberPreToolbarNames IberPreToolbarCommands \
            IberPreToolbarHelp $bitmaps_path Iber::AddPreToolbar $type $prefix]
    AddNewToolbar "Iber Pre- toolbar" ${prefix}IberWindowGeom Iber::AddPreToolbar
}

proc Iber::RemovePreToolbar { } {
    global IberPrePriv

    # ReleasePreToolbar "Iber Pre- toolbar"
    #rename Iber::AddPreToolbar ""

    if { [winfo exists $IberPrePriv(toolbarwin)]  } {
        destroy $IberPrePriv(toolbarwin)
        set IberPrePriv(toolbarwin) ""
    }
}

# Barra Iber POST-proceso
proc Iber::AddPostToolbar { { type "DEFAULT INSIDELEFT" } } {
    global IberToolbarNames IberToolbarCommands IberToolbarHelp IberPriv

    set IberToolbarNames(0) [list TB_Pre_BGImage.png \
            TB_Pre_BGImageSH.png \
            --- \
            TB_Post_resultonview.png \
            TB_Post_resultonviewoff.png \
            --- \
            TB_Post_sections.png \
            TB_Post_secmax.png \
            TB_Post_secc.png \
            TB_Post_hydro.png \
            --- \
            TB_Post_ResToRaster.png  \
            TB_Post_ResToShape.png  \
            --- \
            TB_Iber.png]
    set IberToolbarCommands(0) [list "-np- IberPreMenu::BackgroundImage" \
            "-np- IberPreMenu::SwapShowBackgroundImage" \
            "" \
            "-np- SelRes::ResOnElem" \
            "-np- SelRes::ResOnElemOff" \
            "" \
            "-np- XsAndProf::CreateProfiles" \
            "-np- XsAndProf::ViewProfileMaxRes" \
            "-np- XsAndProf::ViewProfileResWin" \
            "-np- XsAndProf::ResultHydrograph" \
            "" \
            "-np- ::PostRaster::ResultsToRasterWin" \
            "-np- ResultsToShapefile::WritePostW" \
            "" \
            "-np- Iber::About"]
    set IberToolbarHelp(0) [list [= "Background Image"] \
            [= "Show/Hide Background Image"] \
            "" \
            [= "Label results on elements"] \
            [= "Hide all results on labels"] \
            "" \
            [= "Create profiles"] \
            [= "Draw maximum water profiles"] \
            [= "Draw instant water profile"] \
            [= "Do Hydrograph"] \
            "" \
            [= "Export results to ASCII Raster"] \
            [= "Export results to Shapefile"] \
            "" \
            "Iber $::Iber::VersionNumber"]

    # prefix values:
    #          Pre        Only active in the preprocessor
    #          Post       Only active in the postprocessor
    #          PrePost    Active Always
    set prefix Post
    set bitmaps_path [file join $::IberPriv(dir) icos]
    set ::IberPriv(toolbarwin) [CreateOtherBitmaps IberBar "Iber toolbar" IberToolbarNames IberToolbarCommands \
            IberToolbarHelp $bitmaps_path Iber::AddPostToolbar $type $prefix]
    AddNewToolbar "Iber toolbar" ${prefix}IberWindowGeom Iber::AddPostToolbar
}

proc Iber::RemovePostToolbar { } {
    global IberPriv

    #Iber::RemovePreToolbar

    # ReleaseToolbar "Iber toolbar"
    # rename Iber::AddPostToolbar ""

    if { [winfo exists $::IberPriv(toolbarwin)]  } {
        destroy $::IberPriv(toolbarwin)
        set ::IberPriv(toolbarwin) ""
    }
}

# Barra Iber POST-proceso-WUA
proc Iber::AddPostToolbarWUA { { type "DEFAULT INSIDELEFT" } } {
    global IberToolbarNames IberToolbarCommands IberToolbarHelp IberPriv

    set IberToolbarNames(0) [list TB_Pre_BGImage.png \
            TB_Pre_BGImageSH.png \
            --- \
            TB_Post_resultonview.png \
            TB_Post_resultonviewoff.png \
            --- \
            TB_Post_sections.png \
            TB_Post_secmax.png \
            TB_Post_secc.png \
            TB_Post_hydro.png \
            --- \
            TB_Post_wua.png \
            --- \
            TB_Post_ResToRaster.png  \
            --- \
            $::IberPriv(IberGifIco)]
    set IberToolbarCommands(0) [list "-np- IberPreMenu::BackgroundImage" \
            "-np- IberPreMenu::SwapShowBackgroundImage" \
            "" \
            "-np- SelRes::ResOnElem" \
            "-np- SelRes::ResOnElemOff" \
            "" \
            "-np- XsAndProf::CreateProfiles" \
            "-np- XsAndProf::ViewProfileMaxRes" \
            "-np- XsAndProf::ViewProfileResWin" \
            "-np- XsAndProf::ResultHydrograph" \
            "" \
            "-np- Wua" \
            "" \
            "-np- ::PostRaster::ResultsToRasterWin" \
            "" \
            "-np- Iber::About"]
    set IberToolbarHelp(0) [list [= "Background Image"] \
            [= "Show/Hide Background Image"] \
            "" \
            [= "Label results on elements"] \
            [= "Hide all results on labels"] \
            "" \
            [= "Create profiles"] \
            [= "Draw maximum water profiles"] \
            [= "Draw instant water profile"] \
            [= "Do Hydrograph"] \
            "" \
            [= "Plot WUA vs Q"] \
            "" \
            [= "Export results to Raster or XYZ"] \
            "" \
            "Iber $::Iber::VersionNumber"]

    # prefix values:
    #          Pre        Only active in the preprocessor
    #          Post       Only active in the postprocessor
    #          PrePost    Active Always
    set prefix Post
    set bitmaps_path [file join $::IberPriv(dir) icos]
    set ::IberPriv(toolbarwin) [CreateOtherBitmaps IberBar "Iber toolbar" IberToolbarNames IberToolbarCommands \
            IberToolbarHelp $bitmaps_path Iber::AddPostToolbarWUA $type $prefix]
    AddNewToolbar "Iber toolbar" ${prefix}IberWindowGeom Iber::AddPostToolbarWUA
}

proc Iber::RemovePostToolbarWUA { } {
    global IberPriv

    # ReleaseToolbar "Iber toolbar"
    # rename Iber::AddPostToolbarWUA ""

    if { [winfo exists $::IberPriv(toolbarwin)]  } {
        destroy $::IberPriv(toolbarwin)
        set ::IberPriv(toolbarwin) ""
    }
}

proc Iber::RemoveToolbar { } {
    Iber::RemovePreToolbar
    Iber::RemovePostToolbar
}

proc Iber::StartCalculation { } {
    # Determina el nombre del archivo
    set ProjectName [GiD_Info project ModelName]
    if { [file extension $ProjectName] == ".gid" } {
        set ProjectName [file root $ProjectName]
    }
    set basename [file tail $ProjectName]

    #   Check if results file exists
    if { [GiD_Set UseMoreWindows] != 0 } {
        set directory $ProjectName.gid
        if { [file pathtype $directory] == "relative" } {
            set directory [file join [pwd] $directory]
        }
        set file [file join $directory $basename.flavia.res]
        set file_dat [file join $directory $basename.dat]

        if { [file exists $file] } {
            set answer [tk_messageBox -message "[= {Results file exists}].\n[= {Do you want to overwrite it?}]" \
                    -title [= {Results file exists}] -type okcancel -icon question]
            switch $answer {
                ok {
                    GiD_Process Mescape Utilities Calculate escape
                }
                cancel return
            }
        } else {
            GiD_Process Mescape Utilities Calculate escape
        }
    } else {
        GiD_Process Mescape Utilities Calculate escape
    }
}

proc WriteResults { } {
    global IberPriv
    set basename [GiD_Info project ModelName]
    set directory $basename.gid

    set file [file join $directory salida.flavia.res]
    if { ![file exists $file] } {
        set fileN [file join $directory [file tail $basename].flavia.res]
        #        open $fileN w+
        #     WarnWin "$file $fileN"
        set program [file join $::IberPriv(dir) codes results.exe]
        #cambiar con la nueva version de results
        cd $directory
        if { [catch { exec $program } err] } {
            WarnWinText "Error $err"
        }
        #set fil [open |\"$program\" r+]

        while { 1 } {
            if { [file exists $file] } {
                file copy -force -- $file $fileN
                break
            }
        }
    }
}

proc Iber::GetFullPathExe { file_tail_name } {
    if { $::tcl_platform(platform) == "windows" } {
        set full_name [file join $::IberPriv(dir) bin windows $file_tail_name]
    } else {
        set full_name [file join $::IberPriv(dir) bin linux $file_tail_name]
    }
    return $full_name
}

proc Iber::RenumberMeshIfNecessary { } {
    if { [GiD_Info Mesh NumNodes]!=[GiD_Info Mesh MaxNumNodes] || [GiD_Info Mesh NumElements]!=[GiD_Info Mesh MaxNumElements]} {
        #there are holes in node or element numeration unsupported by Iber, renumber it
        GiD_Process Mescape Utilities Renumber Yes escape
    }
}

#return an objarray with the integer ids of the nodes on layers not frozen (and ids sorted, not grouped by layers)
proc Iber::GetNodeIdsLayersNoFrozen { } {
    if { [GidUtils::VersionSatisfy 16.0.4 16.1.3d] } {
        set node_ids [GiD_Mesh list -avoid_frozen_layers node]
    } else {
        set node_ids [objarray new intarray 0]
        foreach layer_name [GiD_Layers list] {
            if { ![GiD_Layers get frozen $layer_name] } {
                set node_ids [objarray concat $node_ids [GiD_Mesh list -layer $layer_name node]]
            }
        }
        set node_ids [objarray sort $node_ids]
    }
    return $node_ids
}

proc Iber::GetElementIdsLayersNoFrozen { } {
    if { [GidUtils::VersionSatisfy 16.0.4 16.1.3d] } {
        set element_ids [GiD_Mesh list -avoid_frozen_layers element]
    } else {
        set element_ids [objarray new intarray 0]
        foreach layer_name [GiD_Layers list] {
            if { ![GiD_Layers get frozen $layer_name] } {
                set element_ids [objarray concat $element_ids [GiD_Mesh list -layer $layer_name element]]
            }
        }
        set element_ids [objarray sort $element_ids]
    }
    return $element_ids
}

###########################################
### GiD Events/Proc customized for Iber ###

# Button/list in CND and PRB => Disable for Iber # MARCOS-2020
proc CreateJumpButton { GDN w col {dir right}} {
    upvar #0 $GDN GidData

    set ::GidPriv(ProgName) $::Iber::ProgramName
    set ptype ::GidPriv(ProgName)
    if {$::GidPriv(ProgName) == "IBER"} {
        set hp $w.hp
        set jb $w.jb
        set hi $w.hi
        set m  $jb.m
        ttk::button $hp -image [gid_themes::GetImage questionarrow.png small_icons] \
            -takefocus 0 -command [list PickHelp $hp]
        ttk::menubutton $jb -image [gid_themes::GetImage note.png small_icons] \
            -menu $m -underline 0 -direction $dir
        #-style Horizontal.IconButtonWithMenu
        menu $m -postcommand [list FillJumpMenu $GDN] -borderwidth 1 -activeborderwidth 1

        grid $hp -row 0 -column $col -sticky "snew"
        incr col
        #grid $jb -row 0 -column $col -sticky "snew" ### Line commented

        set GidData(JUMPBUTTON) $jb
        set GidData(HELPBUTTON) $hp

        GidHelp $jb [_ "Open other data option using this window"]

        list $hp $jb
    } else {
        #Do nothing
    }
}
