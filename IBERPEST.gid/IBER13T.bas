*# Ernest 1
*#------Escribe condiciones iniciales de turbulencia---
Initial condition
*set cond Turbulence_initial_cond *elems
*loop elems *OnlyInCond
*format "%8i%20.6f%12.6f"
*ElemsNum*cond(k_[m2/s2])*cond(Epsilon_[m2/s3])
*end elems
End Initial condition
*#-----Escribe entradas de turbulencia-----
Inlet
*set cond K-epsilon_inlet *elems 
*loop elems *OnlyInCond
*set var ntm(int)=cond(Kinetic_energy_and_dissipation,int)
*for(i=1;i<=ntm(int);i=i+3) 
*ElemsNum *CondElemFace *\
*format "%13.0f%13.7f%13.7f"
*cond(Kinetic_energy_and_dissipation,*i,real) *cond(Kinetic_energy_and_dissipation,*Operation(i+1),real)  *cond(Kinetic_energy_and_dissipation,*Operation(i+2),real)
*end for
*end elems
End Inlet
