*#GONZALOALEN Apr/2020
*#------ Escribe el archivo de la plantilla 1 de datos de entrada de PEST ------
*# Multiplicadores hidrología
ptf #
*if(strcmp(GenData(Infiltration_losses),"Off")==0)
0
*elseif(strcmp(GenData(Infiltration_losses),"Manual_/_By_parameters")==0)
*if(strcmp(GenData(Losses_model),"Green_Ampt_")==0)
*#if(CondNumEntities==nelem)
*tcl(Pest::WriteInput1GA *GenData(Suction_multiplier) *GenData(T_porosity_multip) *GenData(Ini_saturation_multip) *GenData(Ks_multiplier) *GenData(Initial_loss_multiplier) *GenData(Soil_depth_multiplier))
*set cond Green&Ampt *elems
*loop elems *OnlyInCond
*ElemsNum  *cond(Suction_[mm])  *cond(Total_porosity)  *cond(Initial_saturation)  *cond(Ks_[mm/h])  *cond(Initial_loss_[mm])  *cond(Soil_depth_[m])
*end elems
*#else
*#messagebox - An element without Losses was found, please check and run again - Process finished with error -
*#end if
*elseif(strcmp(GenData(Losses_model),"Horton_")==0)
3
*elseif(strcmp(GenData(Losses_model),"SCS_")==0)
*#if(CondNumEntities==nelem)
*tcl(Pest::WriteInput1CN *GenData(CN_multiplier) *GenData(Ia_coefficient)) *GenData(Start_time_infiltration)
*set cond SCS *elems
*loop elems *OnlyInCond
*ElemsNum  *cond(CN)
*end elems
*#else
*#messagebox - An element without Losses was found, please check and run again - Process finished with error -
*#end if
*elseif(strcmp(GenData(Losses_model),"SCS_continuous")==0)
5
*elseif(strcmp(GenData(Losses_model),"Lineal_")==0)
*#if(CondNumEntities==nelem)
1
*endif
*endif
