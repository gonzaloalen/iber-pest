rem OutputFile: %2\Proceso.rep


rem erase files with information on solver tu use
copy %1.dat Iber2D.dat
copy %1-1.dat Iber1D.dat
copy %1-2.dat Iber_Rain.dat
copy %1-3.dat Iber_Losses.dat
copy %1-4.dat Iber_Bedload.dat
copy %1-5.dat Iber_Bedload1D.dat
copy %1-6.dat Iber_Results.dat
copy %1-7.dat Iber_Wind.dat
copy %1-8.dat Iber_Imbornales.dat
copy %1-9.dat Iber_Internal_cond.dat
copy %1-10.dat Iber_Susp_sed.dat
copy %1-11.dat Iber_Pollutants.dat
copy %1-12.dat Iber_Problemdata.dat
copy %1-13.dat Iber_Turbulence.dat
copy %1-14.dat Iber_Encroachment.dat
copy %1-15.dat Iber_Breach.dat
copy %1-16.dat Iber_Ratingcurves.dat
copy %1-17.dat Iber_Variable_n.dat
copy %1-18.dat Iber_Roof.dat
copy %1-19.dat Iber_Culverts.dat
copy %1-20.dat Iber_Discharges.dat
copy %1-21.dat Iber_SedimentRelation.dat
copy %1-22.dat Iber_RatingcurveQ.dat
copy %1-23.dat Iber_Salinity.dat
copy %1-24.dat Iber_Temperature.dat
copy %1-25.dat Iber_Coliforms.dat
copy %1-26.dat Iber_DischargesWQ.dat
copy %1-27.dat Iber_OD.dat
copy %1-28.dat Iber_DBO.dat
copy %1-29.dat Iber_Nitro.dat
copy %1-30.dat Iber_Hazard.dat
copy %1-31.dat Iber_Hyetograph.dat
copy %1-32.dat Iber_DiffuR.dat
copy %1-33.dat Iber_Wood.dat
copy %1-34.dat Iber_Woodgage.dat
copy %1-35.dat Iber_WoodPr.dat
copy %1-36.dat Iber_AH.dat
copy %1-37.dat Iber_AH_c.dat
copy %1-38.dat Iber_AH_subs.dat
copy %1-39.dat Iber_Aval.dat
copy %1-40.dat Iber_Fosforo.dat
copy %1-41.dat Iber_Phyto.dat
copy %1-42.dat Iber_PH.dat
copy %1-43.dat Iber_Metal.dat
copy %1-44.dat Iber_AtmosphericVars.dat
copy %1-45.dat Iber_AtmosphericLoc.dat
copy %1-46.dat Iber_SedMix_ErodibleZones.dat
copy %1-47.dat Iber_SedMix_IniCond.dat
copy %1-48.dat Iber_SedMix_BC.dat
copy %1-49.dat Iber_SedMix.dat
copy %1-50.dat Iber_SoilWaterBalance.dat
copy %1-51.dat Iber_Gauges.dat
copy %1-52.dat Iber_Groundwater.dat
copy %1-53.dat Iber_SedMix_QsB.dat
copy %1-54.dat Iber_Bedmixtures.dat
copy %1-55.dat Iber_Bedmixturesprop.dat
copy %1-56.dat Iber_Sink&Sources.dat
copy %1-57.dat Iber_Junctions.dat
copy %1-58.dat Iber_Pipes.dat
copy %1-59.dat Iber_UD_Param.dat
copy %1-60.dat Iber_FloodDamage.dat
copy %1-61.dat Iber_FloodDamageCurves.dat
copy %1-62.dat Iber_SWMM_roof.dat
copy %1-63.dat Iber_SWMM_greenroof.dat
copy %1-64.dat Iber_SWMM.dat
copy %1-65.dat Iber_H_IC.dat
copy %1-66.dat Iber_PEST_input1.tpl
copy %1-67.dat Iber_PEST_input2.tpl
copy %1-68.dat Iber_PEST_output.ins
copy %1-69.dat iber_PEST_case.pst
copy %1-70.dat Iber_PEST_par2par.tpl
copy %1-71.dat Iber_PEST_runPEST.bat
copy %1-72.dat Iber_PEST_batchfile.bat

erase %1.dat
erase %1-1.dat
erase %1-2.dat
erase %1-3.dat
erase %1-4.dat
erase %1-5.dat
erase %1-6.dat
erase %1-7.dat
erase %1-8.dat
erase %1-9.dat
erase %1-10.dat
erase %1-11.dat
erase %1-12.dat
erase %1-13.dat
erase %1-14.dat
erase %1-15.dat
erase %1-16.dat
erase %1-17.dat
erase %1-18.dat
erase %1-19.dat
erase %1-20.dat
erase %1-21.dat
erase %1-22.dat
erase %1-23.dat
erase %1-24.dat
erase %1-25.dat
erase %1-26.dat
erase %1-27.dat
erase %1-28.dat
erase %1-29.dat
erase %1-30.dat
erase %1-31.dat
erase %1-32.dat
erase %1-33.dat
erase %1-34.dat
erase %1-35.dat
erase %1-36.dat
erase %1-37.dat
erase %1-38.dat
erase %1-39.dat
erase %1-40.dat
erase %1-41.dat
erase %1-42.dat
erase %1-43.dat
erase %1-44.dat
erase %1-45.dat
erase %1-46.dat
erase %1-47.dat
erase %1-48.dat
erase %1-49.dat
erase %1-50.dat
erase %1-51.dat
erase %1-52.dat
erase %1-53.dat
erase %1-54.dat
erase %1-55.dat
erase %1-56.dat
erase %1-57.dat
erase %1-58.dat
erase %1-59.dat
erase %1-60.dat
erase %1-61.dat
erase %1-62.dat
erase %1-63.dat
erase %1-64.dat
erase %1-65.dat
erase %1-66.dat
erase %1-67.dat
erase %1-68.dat
erase %1-69.dat
erase %1-70.dat
erase %1-71.dat
erase %1-72.dat

rem erase results of very old models using carpa
erase %1.flavia.res
erase %1.flavia.msh
rem not erase results because could use restart or additional calculations
rem erase %1.post.res

if exist Iber_CPUSolver.dat %3\bin\windows\Iber.exe
if exist Iber_GPUSolver.dat %3\bin\windows\IberPlus.exe
if exist Iber_RIberSolver.dat %3\bin\windows\RIber.exe

rem %3\bin\windows\results_1D.exe
rem rename salida.post.res 1D.post.res
