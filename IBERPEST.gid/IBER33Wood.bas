*# virginia
*#-----------------Wood Transport-------------------------------
Inlet     element, side, time, logs/min, Dmin, Dmax, Lmin, Lmax, ro_min_i, ro_max_i, Inlet_Num, ro_min_f, ro_max_f,  K  , Min_Root_Coef, Max_Root_Coef
*if(strcmp(GenData(IberWoodPlugin),"Enabled")==0)
*set cond Wood_boundary *elems *canrepeat
*loop elems *OnlyInCond
*if(strcmp(cond(Density),"Constant")==0)
*set var INS(int)=cond(Wood_Rate,int)
*set var PASO=2
*for(i=1;i<=INS(int);i=i+PASO)
*ElemsNum *CondElemFace   *cond(Wood_Rate,*i,int) *Operation(cond(Wood_Rate,*Operation(i+1),real)) *cond(Min_Diameter_[m]) *cond(Max_Diameter_[m]) *\
*cond(Min_Length_[m]) *cond(Max_Length_[m]) *cond(Min_Density_[kg/m3]) *cond(Max_Density_[kg/m3]) *cond(Inlet_Number) *cond(Min_Density_[kg/m3]) *\
 *cond(Max_Density_[kg/m3]) 0  *cond(Min_Root_Coef) *cond(Max_Root_Coef)    Constant Density 
*end for
*endif
*if(strcmp(cond(Density),"Variable")==0)
*set var INS(int)=cond(Wood_Rate,int)
*set var PASO=2
*for(i=1;i<=INS(int);i=i+PASO)
*ElemsNum *CondElemFace   *cond(Wood_Rate,*i,int) *Operation(cond(Wood_Rate,*Operation(i+1),real)) *cond(Min_Diameter_[m]) *cond(Max_Diameter_[m]) *\
*cond(Min_Length_[m]) *cond(Max_Length_[m]) *cond(Min_Density_[kg/m3]) *cond(Max_Density_[kg/m3]) *cond(Inlet_Number) *cond(Min_Final_Density_[kg/m3]) *\
 *cond(Max_Final_Density_[kg/m3]) *cond(K_[1/h])   *cond(Min_Root_Coef) *cond(Max_Root_Coef)    Variable Density
*end for
*endif
*end elems
*endif
End Inlet
Initial Condition   Log_num, Diameter, Length, Density_i, X, Y, Angle[rad], Time,  Density_f, K, Root_Coefficient
*if(strcmp(GenData(IberWoodPlugin),"Enabled")==0)
*if(strcmp(GenData(Density),"Constant")==0)
*set var INS(int)=GenData(Initial_Condition,int)
*set var PASO=9
*for(i=1;i<=INS(int);i=i+PASO)
*GenData(Initial_Condition,*Operation(i+0))    *GenData(Initial_Condition,*Operation(i+1))  *GenData(Initial_Condition,*Operation(i+2))  *GenData(Initial_Condition,*Operation(i+3))  *\
*GenData(Initial_Condition,*Operation(i+4))  *GenData(Initial_Condition,*Operation(i+5))  *GenData(Initial_Condition,*Operation(i+6))  *GenData(Initial_Condition,*Operation(i+7))  *\
*GenData(Initial_Condition,*Operation(i+3))   0 *GenData(Initial_Condition,*Operation(i+8))   Initial Condition - Constant Density
*end for
*endif
*if(strcmp(GenData(Density),"Variable")==0)
*set var INS(int)=GenData(Initial_Conditions,int)
*set var PASO=11
*for(i=1;i<=INS(int);i=i+PASO)
*GenData(Initial_Conditions,*Operation(i+0))    *GenData(Initial_Conditions,*Operation(i+1))  *GenData(Initial_Conditions,*Operation(i+2))  *GenData(Initial_Conditions,*Operation(i+3))  *\
*GenData(Initial_Conditions,*Operation(i+4))  *GenData(Initial_Conditions,*Operation(i+5))  *GenData(Initial_Conditions,*Operation(i+6))  *GenData(Initial_Conditions,*Operation(i+7))  *\
*GenData(Initial_Conditions,*Operation(i+8))  *GenData(Initial_Conditions,*Operation(i+9))  *GenData(Initial_Conditions,*Operation(i+10))     Initial Condition - Variable Density
*end for
*endif
*endif