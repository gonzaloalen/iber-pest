*# WQ 2.0 (Luis-Marcos 08/2019)
*#----------------pH---------------------
*if(Strcmp(GenData(pH),"On")==0)
*#------ Initial Condition --------------------
Initial condition pH 
*set cond pH_Initial_Cond *elems 
*if(CondNumEntities==0)
*messagebox - An element without pH Initial Condition was found, please check and run again - Process finished with error -
*endif
*loop elems *OnlyInCond
*ElemsNum *cond(C-inorg_[mg/l]) *cond(Alkalinity_CaCO3_[mg/l])
*end elems
End Initial condition pH 
*#------ Boundary Condition --------------------
Boundary condition pH 
*set cond pH_Bound_Cond *elems 
*loop elems *OnlyInCond
*set var INS(int)=cond(pH_inlet,int)
*for(i=1;i<=INS(int);i=i+3)
*ElemsNum *CondElemFace *cond(pH_inlet,*i,int) *cond(pH_inlet,*Operation(i+1),real) *cond(pH_inlet,*Operation(i+2),real)
*end for
*end elems
End condition pH 
*endif

