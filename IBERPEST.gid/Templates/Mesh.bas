*# Write the mesh to import mannings in preprocess
*# To export mesh triangles and quadrilaterals
*# Written by Georgina - december 2003
*# modified - march 2004
*# Geor 1
*if(nelem(Triangle)==0 && nelem(Quadrilateral)==0)
*MessageBox Error: Must create a mesh triangles or quadrilaterals.
*endif
*#*if(isquadratic)
*#*MessageBox Error: elements are Quadratic.
*#*endif
*if(nelem(Tetrahedra) || nelem(Hexahedra))
*WarningBox Warning: Exist Bad elements. Only allow the lines, triangles and quadrilaterals.
*endif
*#set cond 2D_Mesh
*#CondNumEntities
*nelem *npoin
*Set elems(All)
*loop elems 
*ElemsNum *Elemstype
*ElemsConec
*end elems
*loop nodes 
*format "%6i%20.5f%20.5f%20.5f"
*NodesNum*NodesCoord(1)*NodesCoord(2)*NodesCoord(3)
*end nodes
 
