*# Write the mesh to create the cross sections in preprocess
*# To export mesh triangles and quadrilaterals
*# Written by Georgina - march 2004
*# Editted by Georgina - may 2007
*# Geor 1
*if(nelem(Triangle)==0 && nelem(Quadrilateral)==0)
*MessageBox Error: Must create a mesh triangles or quadrilaterals.
*endif
*if(nelem(Tetrahedra) || nelem(Hexahedra))
*MessageBox Warning: Exist Bad elements. Only allow the lines, triangles and quadrilaterals.
*endif
*Set elems(linear)
*nelem *npoin
*loop elems
*ElemsNum *Elemstype   *ElemsMatProp(Manning)
*ElemsConec
*end elems
*loop nodes
*format "%6i%20.5f%20.5f%20.5f"
*NodesNum*NodesCoord(1)*NodesCoord(2)*NodesCoord(3)
*end nodes
 