*#-----------------Non Newtonian Fluids-------------------------------
*if(strcmp(GenData(IberNonNewtonianPlugin),"Disabled")==0)
0               0:Off; 1: Manning; 2:Voellmy; 3: Bingham
*else
*if(strcmp(GenData(Rehological_Model),"Manning")==0)
1               0:Off; 1: MANNING; 2:Voellmy; 3: Bingham
*endif
*if(strcmp(GenData(Rehological_Model),"Voellmy")==0)
2               0:Off; 1: Manning; 2:VOELLMY; 3: Bingham
*endif
*if(strcmp(GenData(Rehological_Model),"Simplified_Bingham")==0)
3               0:Off; 1: Manning; 2:Voellmy; 3: BINGHAM
*endif
*if(strcmp(GenData(Voellmy_Parameters),"From_Land_Use")==0)
0               0:Voellmy from Land Use
*else
1               1:Voellmy from Elevation Table
*endif
*if(strcmp(GenData(Russian_Yield_Height),"Off")==0)
999999               Critical Height for Russian Modification
*else
*GenData(Critical_Yield_Height)               Critical Height for Russian Modification
*endif
*GenData(Pressure_Factor)  Pressure Factor Kp
*GenData(Fluid_Density)  Fluid Density
*if(strcmp(GenData(Entrainment),"Off")==0)
0               0:No Entrainment
*else
*if(strcmp(GenData(Entrainment_Criteria),"Shear_Strength")==0)
1               1: Entrainment by Shear Strength
*endif
*if(strcmp(GenData(Entrainment_Criteria),"Velocity")==0)
2               2: Entrainment by Velocity
*endif
*if(strcmp(GenData(Entrainment_Criteria),"Snow_Depth")==0)
3               3: Snow Depth
*endif
*if(strcmp(GenData(Entrainment_Criteria),"Squared_Velocity")==0)
4               3: Entrainment by Squared Velocity
*endif
*endif
*GenData(Entrainment_Rate)  Entrainment Rate
*GenData(Entrainment_Threshold)  Entrainment Threshold
*GenData(Bottom_density)  Bottom_density
*GenData(Momentum_Percentage_Stop)   Momentum percentage Stop
ELEVATION-VOELLMY PARAMETERS TABLE
*set var INS(int)=GenData(Voellmy_Table,int)
*set var PASO=4
*for(i=1;i<=INS(int);i=i+PASO)
*GenData(Voellmy_Table,*Operation(i+0))    *GenData(Voellmy_Table,*Operation(i+1))  *GenData(Voellmy_Table,*Operation(i+2))  *GenData(Voellmy_Table,*Operation(i+3))
*end for
*endif
ELEMENT VOELLMY-BINGHAM PARAMETERS TABLE
*if(strcmp(GenData(Rehological_Model),"Voellmy")==0)
*if(strcmp(GenData(Voellmy_Parameters),"From_Land_Use")==0)
*set elems(quadrilateral) 
*add elems(triangle)
*loop elems
 *ElemsNum *ElemsMatProp(6,real) *ElemsMatProp(7,real) *ElemsMatProp(8,real) *ElemsMatProp(10,real) *ElemsMatProp(11,real)
*end elems
*endif
*endif
BINGHAM PARAMETERS

*if(strcmp(GenData(Rehological_Model),"Simplified_Bingham")==0)
*set elems(quadrilateral) 
*add elems(triangle)
*loop elems
*ElemsNum *ElemsMatProp(10,real) *ElemsMatProp(11,real)
*end elems
*endif
