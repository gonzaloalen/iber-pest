*#*# Geor 1
*#*#------Escribe el archivo de sedimentos para 1D---
*#*set cond 1D_Bed_Load *nodes
*#*if(CondNumEntities!=0)
*#*format "%13.3f"
*#*GenData(t_sediments_[s])
*#*endif
*#*loop nodes *OnlyInCond
*#*format "%8i%20.6f%12.5f%12.5f%12.5f%8i%8i"
*#*Tcl(TramAndSeccNumber *NodesNum) *cond(d50_[m])*cond(Rock[m])*cond(Porosity) *cond(Friction_angle_BT_[rad])*cond(param1)*cond(Method)
*#*end nodes
*#CC
*#*set cond 1D_Sediments_Inlet *nodes
*#*#-----
*#*loop nodes *OnlyInCond
*#*#-----
*#*if(strcmp(cond(Water_Condition),"Clear")==0)
*#*elseif(strcmp(cond(Water_Condition),"With_Seds")==0)
*#*Tcl(TramAndSeccNumber *NodesNum)     0      0 0 0 -1 0 0 0
*#*endif
*#*end nodes