*#GONZALOALEN Nov/2022
*#------ Escribe el archivo de la plantilla 2 de datos de entrada de PEST ------
ptf #
*#
*intformat "%10i"
*realformat "%10.4f"
*#----- Poner si esta activo o no Urban Drainaje -----
*#if(strcmp(GenData(Flow),"Subsurface")==0)
*#----- Escribe solo 1D -----
*#tcl(ModelName)
*#if(strcmp(GenData(Simulation),"Resume")==0)
*#format "%13.3f%13.3f%13.3f%6i%8.3f%8.5f%10.5f%12.3f%6i%6i"
*#GenData(Max_time_increment_[s]) *GenData(Max_simulation_time_[s]) *GenData(Results_2D_time_interval_[s]) *GenData(tvd) *GenData(CFL) *GenData(Wet-Dry_limit_[m]) *GenData(Viscosity_coef) *GenData(Initial_time_[s]) *GenData(Simulation_Details) 1 *GenData(Timeseries_time_interval_[s])
*#elseif(strcmp(GenData(Simulation),"New")==0)
*#format "%13.3f%13.3f%13.3f%6i%8.3f%8.5f%10.5f%12.3f%6i%6i"
*#GenData(Max_time_increment_[s]) *GenData(Max_simulation_time_[s]) *GenData(Results_2D_time_interval_[s]) *GenData(tvd) *GenData(CFL) *GenData(Wet-Dry_limit_[m]) *GenData(Viscosity_coef) *GenData(Initial_time_[s]) *GenData(Simulation_Details) 0 *GenData(Timeseries_time_interval_[s])
*#endif
*# MATRIU
*#   0
*# VERTEXS
*#   0
*# CONDICIONS INICIALS
*# CC: CONDICIONS CONTORN
*# 123456789
*# *else
*#----- Si es solo 1D -----
*if(strcmp(GenData(Analysis),"1D")==0)
*tcl(ModelName)
*if(strcmp(GenData(Simulation),"Resume")==0)
*#format "%13.3f%13.3f%13.3f%6i%8.3f%8.5f%10.5f%12.3f%6i%6i"
*GenData(Max_time_increment_[s]) *GenData(Max_simulation_time_[s]) *GenData(Results_2D_time_interval_[s]) *GenData(tvd) *GenData(CFL) *GenData(Wet-Dry_limit_[m]) *GenData(Viscosity_coef) *GenData(Initial_time_[s]) *GenData(Simulation_Details) 1 *GenData(Timeseries_time_interval_[s])
*elseif(strcmp(GenData(Simulation),"New")==0)
*#format "%13.3f%13.3f%13.3f%6i%8.3f%8.5f%10.5f%12.3f%6i%6i"
*GenData(Max_time_increment_[s]) *GenData(Max_simulation_time_[s]) *GenData(Results_2D_time_interval_[s]) *GenData(tvd) *GenData(CFL) *GenData(Wet-Dry_limit_[m]) *GenData(Viscosity_coef) *GenData(Initial_time_[s]) *GenData(Simulation_Details) 0 *GenData(Timeseries_time_interval_[s])
*endif
MATRIU
   0
VERTEXS
   0
CONDICIONS INICIALS
CC: CONDICIONS CONTORN
   0  0
123456789
*else
*#------ Escribe Iber_2D ------
*tcl(ModelName)
*#MARCOS 08/2018. Se anade PLAN ID al final
*if(strcmp(GenData(Simulation),"Resume")==0)
*#format "%13.3f%13.3f%13.3f%6i%8.3f%8.5f%12.3f%6i%6i"
*GenData(Max_time_increment_[s]) *GenData(Max_simulation_time_[s]) *GenData(Results_2D_time_interval_[s]) *GenData(tvd) *GenData(CFL) *GenData(Wet-Dry_limit_[m]) *GenData(Viscosity_coef) *GenData(Initial_time_[s]) *GenData(Simulation_Details) 1 *GenData(Plan_ID) *GenData(Simulation_plan) *GenData(Timeseries_time_interval_[s])
*elseif(strcmp(GenData(Simulation),"New")==0)
*#format "%13.3f%13.3f%13.3f%6i%8.3f%8.5f%10.5f%12.3f%6i%6i"
*GenData(Max_time_increment_[s]) *GenData(Max_simulation_time_[s]) *GenData(Results_2D_time_interval_[s]) *GenData(tvd) *GenData(CFL) *GenData(Wet-Dry_limit_[m]) *GenData(Viscosity_coef) *GenData(Initial_time_[s]) *GenData(Simulation_Details) 0 *GenData(Plan_ID) *GenData(Simulation_plan) *GenData(Timeseries_time_interval_[s])
*endif
MATRIU
*#set elems(all)
*set elems(quadrilateral)
*add elems(triangle)
*nelem
*loop elems
*if(Elemsnnode==4)
*if(ElemsMatProp(1,int)!=333)
*ElemsConec *tcl(Pest::WriteIber2DPest *ElemsMatProp(Manning,real)) *ElemsNum
*else
*ElemsConec *Operation(-1*ElemsMatProp(2,real)) *ElemsNum
*endif
*elseif(elemsnnode==3)
*if(ElemsMatProp(1,int)!=333)
*ElemsConec *ElemsConec(1) *tcl(Pest::WriteIber2DPest *ElemsMatProp(Manning,real)) *ElemsNum
*else
*ElemsConec *ElemsConec(1) *Operation(-1*ElemsMatProp(2,real)) *ElemsNum
*endif
*endif
*end elems
VERTEXS
*npoin
*realformat "%20.5f"
*loop nodes
*NodesCoord(1) *NodesCoord(2) *NodesCoord(3) *NodesNum
*end nodes
*realformat "%f"
*intformat "%d"
CONDICIONS INICIALS
*set elems(quadrilateral)
*add elems(triangle)
*set cond IC_manual_assignation *elems
*#if(CondNumEntities==0)
*#WarningBox - Warning: No elements 2D Initial Condition -
*#messagebox - An element without Initial Condition was found, please check and run again - Process finished with error -
*#endif
*loop elems *OnlyInCond
*if(strcmp(cond(Condition_by),"Depth")==0)
*Cond(Vx_[m/s],real) *Cond(Vy_[m/s],real) *Cond(Value_[m],real) 0 *ElemsNum
*else
*Cond(Vx_[m/s],real) *Cond(Vy_[m/s],real) *Cond(Value_[m],real) 1 *ElemsNum
*endif
*end elems
*#else
*#messagebox - An element without Initial Condition was found, please check and run again - Process finished with error -
*#endif
CC: CONDICIONS CONTORN
*set cond 2D_Inlet *elems *canrepeat
*loop elems *OnlyInCond
*if(strcmp(cond(1D_connection),"1")==0)
*ElemsNum *CondElemFace     0      *cond(Point_of_Inlet) -8 0 2 0        -1D Connection Inlet-
*else
*if(strcmp(cond(Inlet),"Specific_Discharge")==0)
*if(strcmp(cond(Inlet_Condition_q),"_Subcritical")==0)
*set var INS(int)=cond(Hydrograph,int)
*set var PASO=2
*for(i=1;i<=INS(int);i=i+PASO)
*ElemsNum *CondElemFace *cond(Hydrograph,*i,int) *Operation(cond(Hydrograph,*Operation(i+1),real)*(-1)) 0 1 -21 1 1 1   -Hidrograma q RLento-
*end for
*elseif(strcmp(cond(Inlet_Condition_q),"_Supercritical")==0)
*if(strcmp(cond(Water),"Elevation")==0)
*set var INS(int)=cond(Discharge-Elevation_Hydrograph,int)
*set var PASO=3
*for(i=1;i<=INS(int);i=i+PASO)
*ElemsNum *CondElemFace *cond(Discharge-Elevation_Hydrograph,*i,int) *Operation(-1*cond(Discharge-Elevation_Hydrograph,*Operation(i+1),real)) 0 *cond(Discharge-Elevation_Hydrograph,*Operation(i+2),real) -11 1 1 1   -Hidrograma q-cota RRapido-
*end for
*elseif(strcasecmp(cond(Water),"Depth")==0)
*set var INS(int)=cond(Discharge-Depth_Hydrograph,int)
*set var PASO=3
*for(i=1;i<=INS(int);i=i+PASO)
*ElemsNum *CondElemFace  *cond(Discharge-Depth_Hydrograph,*i,int) *Operation(-1*cond(Discharge-Depth_Hydrograph,*Operation(i+1),real)) 0 *cond(Discharge-Depth_Hydrograph,*Operation(i+2),real) -11 2 1 1   -Hidrograma q-calado RRapido-
*end for
*endif
*endif
*elseif(strcmp(cond(Inlet),"Total_Discharge")==0)
*if(strcmp(cond(Inlet_Condition),"Critical/Subcritical")==0)
*set var INS(int)=cond(Total_Discharge_sub,int)
*set var PASO=2
*for(i=1;i<=INS(int);i=i+PASO)
*ElemsNum *CondElemFace   *cond(Total_Discharge_sub,*i,int) *Operation(-1*cond(Total_Discharge_sub,*Operation(i+1),real)) -1 1  -41 2 1 *cond(Inlet_Num,int)   -Hidrograma Q CriticoLento-
*end for
*elseif(strcmp(cond(Inlet_Condition),"Supercritical")==0)
*set var INS(int)=cond(Total_Discharge_super,int)
*set var PASO=3
*for(i=1;i<=INS(int);i=i+PASO)
*ElemsNum *CondElemFace   *cond(Total_Discharge_super,*i,int) *Operation(-1*cond(Total_Discharge_super,*Operation(i+1),real)) *cond(Total_Discharge_super,*Operation(i+2),real) 1  -41 2 1 *cond(Inlet_Num,int)   -Hidrograma Q Rapido-
*end for
*endif
*elseif(strcmp(cond(Inlet),"Water_Elevation")==0)
*set var INS(int)=cond(Table,int)
*set var PASO=2
*for(i=1;i<=INS(int);i=i+PASO)
*ElemsNum   *CondElemFace *cond(Table,*i,int) 0 0 *Operation(cond(Table,*Operation(i+1),real)) -34 1 1 3   -Nivel dado-
*end for
*elseif(strcmp(cond(Inlet),"Stepped_discharge")==0)
*set var INS(int)=cond(Discharge_value,int)
*set var PASO=1
*for(i=1;i<=INS(int);i=i+PASO)
*ElemsNum   *CondElemFace   *i   *Operation(-1*cond(Discharge_value,*i,real)) -1 1  -42 0 0 *cond(Inlet_Num,int)   -Caudal escalonado-
*end for
*#elseif(strcmp(cond(1D_connection),"1")==0)
*#ElemsNum *CondElemFace     0      0 0 0 -8 *cond(REACH_Number,int) 1 0
*endif
*endif
*end elems
*set cond 2D_Outlet *elems *canrepeat
*loop elems *OnlyInCond
*if(strcmp(cond(1D_connection),"1")==0)
*ElemsNum *CondElemFace     0      *cond(Outlet_Point) -8 0 1 0               -1D Connection Outlet-
*else
*if(strcmp(cond(Flow_condition),"SUPERCRITICAL/CRITICAL")==0)
*ElemsNum *CondElemFace     0      0 0 0 -40 1 1 0 *cond(Outlet_Num,int)  -Salida en Critico/Rapido -
*elseif(strcmp(cond(Flow_condition),"SUBCRITICAL")==0)
*if(strcmp(cond(Type),"Weir")==0)
*if(strcmp(cond(Crest_Definition),"Height")==0)
*ElemsNum *CondElemFace   0        0 0 *cond(Weir_height_[m],real) *cond(Weir_Coefficient,real) 1 1 0 *cond(Outlet_Num,int)   -Salida Vertedero Altura-
*elseif(strcmp(cond(Crest_Definition),"Elevation")==0)
*ElemsNum *CondElemFace   0        0 0 *cond(Weir_elevation_[m],real) *cond(Weir_Coefficient,real) 3 1 0 *cond(Outlet_Num,int)   -Salida Vertedero Cota-
*endif
*elseif(strcmp(cond(Type),"Given_level")==0)
*set var INS(int)=cond(Given_level_[m],int)
*set var INST(int)=Operation(cond(Given_level_[m],int)/2)
*set var PASO=2
*for(i=1;i<=INS(int);i=i+PASO)
*ElemsNum *CondElemFace   *cond(Given_level_[m],*i,int) 0 0 *Operation(cond(Given_level_[m],*Operation(i+1),real)) -34 1 1 0 *cond(Outlet_Num,int)   -Salida Nivel Dado-
*end for
*elseif(strcmp(cond(Type),"Rating_curve")==0)
*ElemsNum *CondElemFace   0    *cond(Table_number,int) 0 0 -111 1 1 0 *cond(Outlet_Num,int)   -Salida RatingCurve q-
*elseif(strcmp(cond(Type),"Rating_curve_Q")==0)
*ElemsNum *CondElemFace   0    *cond(Table_number_Q,int) 0 0 -112 1 1 0 *cond(Outlet_Num,int)   -Salida RatingCurve Q-
*endif
*endif
*endif
*end elems
*#------------
123456789
*endif
*if(strcmp(GenData(IberPlusPlugin),"Enabled")==0)
*tcl(SelectGPUPlus)
*elseif(strcmp(GenData(IberCDXPlugin),"Enabled")==0)
*tcl(SelectGPUCDX)
*elseif(strcmp(GenData(IberPlusPlugin),"Disabled")==0)
*tcl(SelectCPU)
*endif
