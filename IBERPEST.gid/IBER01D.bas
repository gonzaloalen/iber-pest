*#------ Archivo Iber_1D.bas ------------------------
*# iniciado  Georgina 05/2003
*# modificado/actualizado Georgina 01/08
*# Geor 4
*#------
*if(strcmp(GenData(Analysis),"2D")==0)
*tcl(ModelName)
0   0    1  
CONDICIONS INICIALS           
CC: CONDICIONS CONTORN        
123456789
*else
*#------ Escribe Iber 1D ------
*if(strcmp(GenData(IberUrbanDrainagePlugin),"Enabled")==0)
*tcl(ModelName)
0   0    1
CONDICIONS INICIALS           
CC: CONDICIONS CONTORN        
123456789
*else
*tcl(ModelName)
*#Tcl(1DSection) 
CONDICIONS INICIALS
*set cond Pipes_Initial_Condition *nodes
*loop nodes *OnlyInCond
*if(strcmp(cond(Water),"Depth")==0)
*cond(Q_[m3/s],real) *cond(Depth_[m],real)   1
*elseif(strcmp(cond(Water),"Elevation")==0)
*cond(Q_[m3/s],real) *cond(Elevation_[m],real)   0 
*endif 
*#break   
*end nodes
CC: CONDICIONS CONTORN
*set cond 1D_Inlet *nodes 
*#add cond 1D_Outlet *nodes
*#-----
*loop nodes *OnlyInCond
*#-----
*if(cond(2D_Connection,int))
*Tcl(TramAndSeccNumber *NodesNum) 0            0 0 -8 1 1 *cond(Base_Discharge) *NodesNum
*elseif(strcasecmp(cond(Flow_Condition),"SUPERCRITICAL")==0)
*if(strcasecmp(cond(Water),"Elevation")==0)
*set var INS(int)=cond(5,int)
*set var PASO=3
*for(i=1;i<=INS(int);i=i+PASO)
*Tcl(TramAndSeccNumber *NodesNum) *cond(5,*i,int) *Operation(cond(5,*Operation(i+1),real)) *cond(5,*Operation(i+2),real) -11 1 1 0 *NodesNum
*end for
*elseif(strcasecmp(cond(Water),"Depth")==0)
*set var INS(int)=cond(6,int)
*set var PASO=3
*for(i=1;i<=INS(int);i=i+PASO)
*Tcl(TramAndSeccNumber *NodesNum) *cond(6,*i,int) *Operation(cond(6,*Operation(i+1),real)) *cond(6,*Operation(i+2),real) -11 2 1 0 *NodesNum
*end for
*endif
*elseif(strcasecmp(cond(Flow_Condition),"SUBCRITICAL")==0)
*set var INS(int)=cond(Hydrograph,int)
*set var PASO=2
*for(i=1;i<=INS(int);i=i+PASO)
*Tcl(TramAndSeccNumber *NodesNum) *cond(Hydrograph,*i,int) *Operation(cond(Hydrograph,*Operation(i+1),real)) 1 -21 1 1 1 *NodesNum
*end for
*elseif(strcasecmp(cond(Flow_Condition),"LATERAL_INLET")==0)
*set var INS(int)=cond(8,int)
*set var PASO=2
*for(i=1;i<=INS(int);i=i+PASO)
*Tcl(TramAndSeccNumber *NodesNum) *cond(8,*i,int) *Operation(cond(8,*Operation(i+1),real)) 1 -90 1 1 1 *NodesNum
*end for
*endif
*end nodes
*#-----
*set cond 1D_Outlet *nodes
*loop nodes *OnlyInCond
*if(strcasecmp(cond(2D_Connection),"YES")==0)
*Tcl(TramAndSeccNumber *NodesNum) 0            0 0 -8 1 1 0 *NodesNum
*elseif(strcasecmp(cond(Exit),"Supercritical/Critical_Flow")==0)
*Tcl(TramAndSeccNumber *NodesNum) 0            0 0 -40 1 1 1 *NodesNum
*elseif(strcasecmp(cond(Exit),"Subcritical_Flow")==0)
*if(strcasecmp(cond(Type),"Weir")==0)
*if(strcasecmp(cond(Weir),"Height")==0)
*Tcl(TramAndSeccNumber *NodesNum) 0 *cond(Weir_crest_length_[m],real) *cond(Weir_height_[m],real) *cond(Weir_Coefficient,real) 1 1 1 *NodesNum
*elseif(strcasecmp(cond(Weir),"Elevation")==0)
*Tcl(TramAndSeccNumber *NodesNum) 0 *cond(Weir_crest_length_[m],real) *cond(Weir_elev_[m],real) *cond(Weir_Coefficient,real) 3 1 1 *NodesNum
*endif
*else
*set var INS(int)=cond(Given_level_[m],int)
*set var PASO=2
*for(i=1;i<=INS(int);i=i+PASO)
*Tcl(TramAndSeccNumber *NodesNum) *cond(Given_level_[m],*i,int) 0 *Operation(cond(Given_level_[m],*Operation(i+1),real)) -34 1 1 1 *NodesNum
*end for
*endif
*endif
*end nodes
*#------------
123456789
*endif
*endif	  
