*# Ernest1
*# Enrique 07/13
*# Gonzaloalen 05/2021
*#------Escribe el archivo de condiciones internas---
*set var maxnumtablas(int)=0
*set cond 2D_Internal_Condition *elems 
*loop elems *OnlyInCond
*if(strcmp(cond(Condition_type),"Weir-Gate")==0)
*if(strcmp(cond(Gate_Type),"Mobile")==0)
*if(cond(Num_gate_table,int)>maxnumtablas(int))
*set var maxnumtablas(int)=cond(Num_gate_table,int)
*endif
*else
*set var cond(Num_gate_table,int)=0
*endif
*if(strcmp(cond(Ref_Points_Definition),"Standard")==0)  *# Si se asigna "Standard" escribe directamente ceros en las coordenadas de los puntos de referencia
*ElemsNum *CondElemFace 1 *cond(Bottom_elevation_[m],real) *cond(Gate_elevation_[m],real) *cond(Gate_opening_width_[%],real) *cond(Free_Gate_Cd,real) *cond(Weir_elevation_[m],real) *cond(Weir_opening_length_[%],real) *cond(Weir_Cd,real) *cond(Submerged_Gate_Cd,real) *cond(Gauge_Number,int) *cond(Num_gate_table,int) *\
0 0 0 0
*elseif(strcmp(cond(Ref_Points_Definition),"User_defined")==0)
*ElemsNum *CondElemFace 1 *cond(Bottom_elevation_[m],real) *cond(Gate_elevation_[m],real) *cond(Gate_opening_width_[%],real) *cond(Free_Gate_Cd,real) *cond(Weir_elevation_[m],real) *cond(Weir_opening_length_[%],real) *cond(Weir_Cd,real) *cond(Submerged_Gate_Cd,real) *cond(Gauge_Number,int) *cond(Num_gate_table,int) *\
*tcl(IberUtils:WriteCalculationFileCoord * cond(Ref_Point_1) 1) *tcl(IberUtils:WriteCalculationFileCoord * cond(Ref_Point_1) 2) *tcl(IberUtils:WriteCalculationFileCoord * cond(Ref_Point_2) 1) *tcl(IberUtils:WriteCalculationFileCoord * cond(Ref_Point_2) 2)
*endif
*elseif(strcmp(cond(Condition_type),"Gate")==0)
*if(strcmp(cond(Gate_Type),"Mobile")==0)
*if(cond(Num_gate_table,int)>maxnumtablas(int))
*set var maxnumtablas(int)=cond(Num_gate_table,int)
*endif
*else
*set var cond(Num_gate_table,int)=0
*endif
*if(strcmp(cond(Ref_Points_Definition),"Standard")==0)  *# Si se asigna "Standard" escribe directamente ceros en las coordenadas de los puntos de referencia
*ElemsNum *CondElemFace 1 *cond(Bottom_elevation_[m],real) *cond(Gate_elevation_[m],real) *cond(Gate_opening_width_[%],real) *cond(Free_Gate_Cd,real)  0 0 0 *cond(Submerged_Gate_Cd,real) *cond(Gauge_Number,int) *cond(Num_gate_table,int) *\
0 0 0 0
*elseif(strcmp(cond(Ref_Points_Definition),"User_defined")==0)
*ElemsNum *CondElemFace 1 *cond(Bottom_elevation_[m],real) *cond(Gate_elevation_[m],real) *cond(Gate_opening_width_[%],real) *cond(Free_Gate_Cd,real)  0 0 0 *cond(Submerged_Gate_Cd,real) *cond(Gauge_Number,int) *cond(Num_gate_table,int) *\
*tcl(IberUtils::WriteCalculationFileCoord *cond(Ref_Point_1) 1) *tcl(IberUtils::WriteCalculationFileCoord *cond(Ref_Point_1) 2) *tcl(IberUtils::WriteCalculationFileCoord *cond(Ref_Point_2) 1) *tcl(IberUtils::WriteCalculationFileCoord *cond(Ref_Point_2) 2)
*endif
*elseif(strcmp(cond(Condition_type),"Weir")==0)
*if(strcmp(cond(Ref_Points_Definition),"Standard")==0)  *# Si se asigna "Standard" escribe directamente ceros en las coordenadas de los puntos de referencia
*ElemsNum *CondElemFace 1 0 0 0 0 *cond(Weir_elevation_[m],real) *cond(Weir_opening_length_[%],real) *cond(Weir_Cd,real) 0 *cond(Gauge_Number,int) 0 *\
0 0 0 0
*elseif(strcmp(cond(Ref_Points_Definition),"User_defined")==0)
*ElemsNum *CondElemFace 1 0 0 0 0 *cond(Weir_elevation_[m],real) *cond(Weir_opening_length_[%],real) *cond(Weir_Cd,real) 0 *cond(Gauge_Number,int) 0 *\
*tcl(IberUtils::WriteCalculationFileCoord *cond(Ref_Point_1) 1) *tcl(IberUtils::WriteCalculationFileCoord *cond(Ref_Point_1) 2) *tcl(IberUtils::WriteCalculationFileCoord *cond(Ref_Point_2) 1) *tcl(IberUtils::WriteCalculationFileCoord *cond(Ref_Point_2) 2)
*endif
*elseif(strcmp(cond(Condition_type),"Local_loss")==0)
*ElemsNum *CondElemFace 2 *cond(Lambda,real) 0 0 0 0 0 0 0 *cond(Gauge_Number,int) 0 0 0 0
*elseif(strcmp(cond(Condition_type),"Stream_gauge")==0)
*ElemsNum *CondElemFace 3 0 0 0 0 0 0 0 0 *cond(Gauge_Number,int) 0 0 0 0
*endif
*end elems
*# ahora hacemos un bucle nuevo por elementos para escribir 1 vez solo la tabla
*for(ii=1;ii<=maxnumtablas(int);ii=ii+1)
*set var ckeck(int)=0
*set var id(int)=ii
*set cond 2D_Internal_Condition *elems 
*loop elems *OnlyInCond
*if(strcmp(cond(Gate_Type),"Mobile")==0)
*if(cond(Num_gate_table,int)==id(int))
*if(ckeck(int)==0)
Start Gate Table
*id(int)
*set var ntm(int)=cond(Gate_Opening,int)
*for(i=1;i<=ntm(int);i=i+2)
*cond(Gate_Opening,*i,real) *cond(Gate_Opening,*Operation(i+1),real)
*end for
Ends Gate Table
*set var ckeck(int)=1
*endif
*endif
*endif
*end elems
*end for
*set cond Line_Bridge *elems
*loop elems *OnlyInCond
*if(strcmp(cond(Ref_Points_Definition),"Standard")==0)  *# Si se asigna "Standard" escribe directamente ceros en las coordenadas de los puntos de referencia
*ElemsNum *CondElemFace 1 -999 *cond(Lower_Deck_Elevation,real) *cond(Bridge_Opening_Percent,real) *cond(Free_Pressured_Flow_Cd,real) *cond(Deck_Elevation,real) *cond(Deck_Length_Percent,real) *cond(Deck_Cd,real) *cond(Submerged_Pressured_Flow_Cd,real)  *cond(Gauge_Number,int) 0 *\
0 0 0 0
*elseif(strcmp(cond(Ref_Points_Definition),"User_defined")==0)
*ElemsNum *CondElemFace 1 -999 *cond(Lower_Deck_Elevation,real) *cond(Bridge_Opening_Percent,real) *cond(Free_Pressured_Flow_Cd,real) *cond(Deck_Elevation,real) *cond(Deck_Length_Percent,real) *cond(Deck_Cd,real) *cond(Submerged_Pressured_Flow_Cd,real)  *cond(Gauge_Number,int) 0 *\
*tcl(IberUtils::WriteCalculationFileCoord *cond(Ref_Point_1) 1) *tcl(IberUtils::WriteCalculationFileCoord *cond(Ref_Point_1) 2) *tcl(IberUtils::WriteCalculationFileCoord *cond(Ref_Point_2) 1) *tcl(IberUtils::WriteCalculationFileCoord *cond(Ref_Point_2) 2)
*endif
*end elems
