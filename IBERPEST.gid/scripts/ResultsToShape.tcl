### Export mesh + results as a shapefile ###
## Marcos 2022 - Adapted from Shapefile.tcl of Enrique ##
# Shapefile write results plugin #

namespace eval ResultsToShapefile {
    variable info_plugin ;#information read from .xml plugin file          
   
    variable stop
    variable percent_done

    variable _all_result_steps 1 ;#export options:one file by result step or only the current
    variable _all_results 1;#export options: only current result or all results
    variable _all_analysis 1 ;#export options: only current analysis or all analysis
    variable _nodal_to_elemental 0 ;#interpolate result nodal to element, to export as polygons
    
    #set info_plugin(path) [file dirname [info script]] ;#to know where to find the files
    #if { [info exists info_plugin(Version)] } {
    #    set other_plugin_version $info_plugin(Version)
    #}
    #array set info_plugin [ReadProblemtypeXml [file join $info_plugin(path) [file tail $info_plugin(path)].xml] InfoPlugin {Name Version MinimumGiDVersion}]  
    #if { [info exists other_plugin_version] && $other_plugin_version != $info_plugin(Version)} {
    #    WarnWinText [_ "There are two versions of %s: the %s and %s inside the plugins folder. One must be deleted" $info_plugin(Name) $other_plugin_version $info_plugin(Version)]
    #}    
    #if { [GidUtils::VersionCmp $info_plugin(MinimumGiDVersion)] < 0 } {  
    #    WarnWinText [_ "This plugin requires GiD %s or later" $info_plugin(MinimumGiDVersion)]
    #}
}

proc ResultsToShapefile::PostResultFillResultsByEntities { analysis_name result_name time_step } {
    variable results_by_nodes
    variable results_by_elements
        
    set gidpost_type ""
    set gidpost_over ""
    set result_info [GiD_Result get -array [list $result_name $analysis_name $time_step]]
    foreach item $result_info {
        set first_subitem [lindex $item 0]
        set first_subitem_type [objarray type $first_subitem]
        if { $first_subitem_type != "string" && $first_subitem_type != ""} {
            #check objarray type for efficiency, to avoid objarray conversion to string
            #set result_data $item 
            continue
        }
        if { $first_subitem == "Result"} {  
            set gidpost_type [lindex $item 4]
            set gidpost_over [lindex $item 5]
            break
        }
    }
    set result_data [lindex $result_info end]
    set gidpost_ids [lindex $result_data 0]        
    set num_values [objarray length $gidpost_ids]
    
    set entities ""
    if { $gidpost_over == "OnNodes" } {
        upvar results_by_nodes results_by_entities
    } elseif { $gidpost_over == "OnGaussPoints" } {
        upvar results_by_elements results_by_entities
    } else {
        error "Unexpected gidpost_over $gidpost_over"
    }
           
    set i_component 0
    foreach component_values [lindex $result_data 1] {
        set num_values_i [objarray length $component_values]
        if { $num_values_i != $num_values } {
            if { $gidpost_over == "OnNodes" } {
                W "Error result [list $result_name $analysis_name $time_step]. Result is defined for $num_values entities but are returned $num_values_i values. Used only firsts"
                set component_values [objarray range $component_values 0 [expr {$num_values-1}]]
            } elseif { $gidpost_over == "OnGaussPoints" } {
                #multiple gauss points, convert to one averaging values
                set num_gauss_points [expr {int($num_values_i)/int($num_values) }]
                set type [objarray type $component_values]
                set averaged_component_values [objarray new $type $num_values]
                set pos_ini 0
                set pos_end [expr {$num_gauss_points-1}]
                for {set i 0} {$i<$num_values} {incr i} {
                    set gauss_values [objarray range $component_values $pos_ini $pos_end]                    
                    set sum [objarray sum $gauss_values]
                    set mean [expr {double($sum)/double($num_gauss_points)}]                   
                    objarray set $averaged_component_values $i $mean
                    incr pos_ini $num_gauss_points             
                    incr pos_end $num_gauss_points
                }
                set component_values $averaged_component_values
            }
        }
        set data($i_component) $component_values
        incr i_component
    }    
    if { $gidpost_type == "Scalar" } {                         
        for {set i_entity 0 } {$i_entity < $num_values} {incr i_entity} {
            set id [objarray get $gidpost_ids $i_entity]
            set value [objarray get $data(0) $i_entity]
            #lappend results_by_entities($id) $value 
            if { [IsResultNotDefined $value] } {
                #do nothing
            } else {
                lappend results_by_entities($id) $value
            }     
        }                            
    } elseif { $gidpost_type == "Vector" } {     
        #use the modulus, shapefile attributtes doesn't support vector
        for {set i_entity 0 } {$i_entity < $num_values} {incr i_entity} {
            set id [objarray get $gidpost_ids $i_entity]
            set vx [objarray get $data(0) $i_entity]
            set vy [objarray get $data(1) $i_entity]
            set vz [objarray get $data(2) $i_entity]            
            set value [MathUtils::VectorModulus [list $vx $vy $vz]]
            lappend results_by_entities($id) $value                
        }     
    } else {
        #it is a tensor, what to use being representative as a single scalar?.  e.g. S-I, or von misses for stress,...
        set num_components [llength [array names data]]
        for {set i_entity 0 } {$i_entity < $num_values} {incr i_entity} {
            set id [objarray get $gidpost_ids $i_entity]
            set vs [list]
            for {set i_component 0} {$i_component<$num_components} {incr i_component} {                
                set vi [objarray get $data($i_component) $i_entity]
                lappend vs $vi
            }
            if { $num_components == 9 } {
                #eigenvalues provided, use only c-I as scalar
                set value [lindex $vs 6]
            } else {
                #mean of all values, but doesn't has much physical sense!!
                set value 0.0
                foreach vi $vs {
                    set value [expr $value+$vi]
                }
                set value [expr $value/double($num_components)]
            }                
            lappend results_by_entities($id) $value
        }    
    }    
    return $gidpost_over
}

proc ResultsToShapefile::GetValidResultNameModifyToBeUnique { result_name previous_result_names } {
    set result_name_valid $result_name
    if { [lsearch $previous_result_names $result_name_valid] != -1 } {            
        set n [string length $result_name]
        if { $n<10 } {
            #complete with 0 until 10 chars
            set result_name_valid ${result_name}[string repeat 0 [expr 10-$n]]
        }
        for {set index 9} {$index>=0} {incr index -1} {
            foreach letter {0 1 2 3 4 5 6 7 8 9 a b c d e f g h i j k l m n o p q r s t u v w x y z} {
                set result_name_valid [string replace $result_name_valid $index $index $letter]
                if { [lsearch $previous_result_names $result_name_valid] == -1 } {
                    return $result_name_valid
                }
            }
        }
    }
    return $result_name_valid
}

#result_name must be string <=10 chars, and letters, numbers and _
#fields are the current fields, be check that truncated name is not repeating other name
proc ResultsToShapefile::GetValidResultName { result_name previous_result_names } {
    set result_name_valid ""
    set result_name [string map {// _} $result_name] ;#typical GiD results representing a tree
    set n [string length $result_name]
    if { $n>10 } {
        set n 10
    }
    for {set i 0} {$i<$n} {incr i} {
        set letter [string index $result_name $i]
        if { [string is space $letter] } {
            set letter _
        } elseif { ![string is alnum $letter] } {
            set letter _
        }
        append result_name_valid $letter
    }
    if { [lsearch $previous_result_names $result_name_valid] != -1 } {
        set result_name_valid [ResultsToShapefile::GetValidResultNameModifyToBeUnique $result_name_valid $previous_result_names]        
    }
    return $result_name_valid
}

#avoid use by now, it is very expensive the implementation until GiD 15.1.1d, to be improved for next 15.1.2d...
proc ResultsToShapefile::GetElementNodesEXPENSIVE { element_id } {    
    return [lindex [lindex [GiD_Info Mesh -post elements Any $element_id $element_id -array] 0] 2]
}

proc ResultsToShapefile::GetNodeCoordinates2DEXPENSIVE { node_id } {    
  return [lrange [lindex [GiD_Info Mesh -post nodes $node_id $node_id -array2] 1] 0 1]
}

#for efficiency ask only once and store data in tcl array for ResultsToShapefile::GetElementNodes
proc ResultsToShapefile::CacheMeshPost { cache_elements } {    
    variable _node_coordinates
    variable _element_connectivities            
    lassign [GiD_Info Mesh -post nodes -array2] node_ids coordinates
    set num_nodes [objarray length $node_ids]
    set pos 0
    for {set i_node 0} {$i_node<$num_nodes} {incr i_node} {        
        set _node_coordinates([objarray get $node_ids $i_node]) [objarray range $coordinates $pos [expr $pos+1]]
        incr pos 3
    }    
    if { $cache_elements } {
        foreach mesh [GiD_Info Mesh -post elements Any -array2] {
            lassign $mesh element_type element_ids connectivities materials
            if { $element_type != "Triangle" && $element_type != "Quadrilateral" } {
                continue
            }
            set num_elements [objarray length $element_ids]
            set num_nodes_element [expr [objarray length $connectivities]/$num_elements]        
            set pos 0
            for {set i_element 0} {$i_element<$num_elements} {incr i_element} {            
                set _element_connectivities([objarray get $element_ids $i_element]) [objarray range $connectivities $pos [expr $pos+$num_nodes_element-1]]
                incr pos $num_nodes_element
            }
        }
    }
    return 0
}

#temporary dark trick to avoid calls to ResultsToShapefile::GetElementNodesEXPENSIVE
proc ResultsToShapefile::GetElementNodes { element_id } {
    variable _element_connectivities
    return $_element_connectivities($element_id)
}

proc ResultsToShapefile::GetElementIds { } {
    variable _element_connectivities
    return [lsort -integer [array names _element_connectivities]]
}

proc ResultsToShapefile::GetNodeCoordinates2D { node_id } {
    variable _node_coordinates
    return $_node_coordinates($node_id)
}

proc ResultsToShapefile::GetNodeIds { } {
    variable _node_coordinates
    return [lsort -integer [array names _node_coordinates]]
}

proc ResultsToShapefile::ReleaseCacheMeshPost { } {
    variable _node_coordinates
    variable _element_connectivities    
    array unset _node_coordinates
    array unset _element_connectivities
}

proc ResultsToShapefile::GetElementVerticesCoordinates2D { element_id } {        
    set coordinates_outer [list]
    foreach node_id [ResultsToShapefile::GetElementNodes $element_id] {        
        lappend coordinates_outer {*}[ResultsToShapefile::GetNodeCoordinates2D $node_id]
    }
    return $coordinates_outer
}

proc ResultsToShapefile::vector_mean_objarray { vectors_list } {    
    set result [objarray vector_sum -type floatarray {*}$vectors_list]
    objarray scale $result [expr 1.0/[llength $vectors_list]]
    return $result
}

proc ResultsToShapefile::vector_mean { vectors_list } {
    set result [list]
    set num_vectors [llength $vectors_list]
    set num_values [llength [lindex $vectors_list 0]] ;#assumed all vectors same num_values    
    for {set j 0} {$j<$num_values} {incr j} {
        set num_vectors_used $num_vectors
        set sum 0.0
        for {set i 0} {$i<$num_vectors} {incr i} {                   
            set value [lindex [lindex $vectors_list $i] $j]
            if { [IsResultNotDefined $value] } {
                incr num_vectors_used -1
            } else {
                set sum [expr {$sum+$value}]
            }
        }
        if { $num_vectors_used } {
            lappend result [expr {$sum*[expr 1.0/$num_vectors_used]}]
        } else {
            lappend result [ResultsToShapefile::GetResultNotDefinedForShapefile]
        }
    }            
    return $result
}

#format 19 9 (%19.9f) seems that -9e7 is the minimum allowed value
#the NO_RESULT special GiD values -3.402823e+38 is not allowed!!
proc ResultsToShapefile::GetResultNotDefinedForShapefile { } {
    #return -3.402823e+38 
    return -9e7
}

proc ResultsToShapefile::WritePostMeshAndResults { filename } {
    variable results_by_nodes
    variable results_by_elements
    
    variable stop
    variable percent_done
    variable _all_result_steps
    variable _all_results
    variable _all_analysis
    variable _nodal_to_elemental
    set fail 0
    #GidUtils::Crono::Start
    if { ![GiD_Info Mesh -post NumNodes] } {
        return 1
    }
    #calculate num_files to write based of results (each time step must be in a different file)    
    set num_files 0
    if { $_all_analysis } {
        set analysis_names [GiD_Info postprocess get all_analysis]
    } else {
        set analysis_names [list [GiD_Info postprocess get cur_analysis]]
    }
    foreach analysis_name $analysis_names {     
        if { $_all_result_steps } {
            set time_steps [GiD_Info postprocess get all_steps $analysis_name]
        } else {
            set time_steps [GiD_Info postprocess get cur_step]
        }
        incr num_files [llength $time_steps]           
    }        
    if { $num_files == 0 } {
        W "No result to be exported"
        return 1
    }    
    set i_file 0
    set ::ResultsToShapefile::stop 0
    set ::ResultsToShapefile::percent_done 0    
    ::GidUtils::CreateAdvanceBar [_ "Writting file"] [_ "Process"] ::ResultsToShapefile::percent_done 100 {set ::ResultsToShapefile::stop 1}
    #a Shapefile file can't store several time steps (without tricks like encode in the dataset name)    
    if { $_all_analysis } {
        set analysis_names [GiD_Info postprocess get all_analysis]
    } else {
        set analysis_names [list [GiD_Info postprocess get cur_analysis]]
    }
    foreach analysis_name $analysis_names {
        if { $_all_result_steps } {
            set time_steps [GiD_Info postprocess get all_steps $analysis_name]
        } else {
            set time_steps [list [GiD_Info postprocess get cur_step]]
        }        
        foreach time_step $time_steps {            
            if { $::ResultsToShapefile::stop } {
                break
            }
            if { $_all_results } {
                set result_names [GiD_Info postprocess get results_list CONTOUR_FILL $analysis_name $time_step]                
                if { ![llength $result_names] } {
                    #there are no results defined for this analysis and the current time step
                    continue
                }
            } else {
                set result_names [list [GiD_Info postprocess get cur_result]]
                if { ![llength $result_names] } {
                    W "Must set a current result to be exported"
                    continue
                }
            }            
            
            array unset results_by_nodes
            array unset results_by_elements
            set fields_nodes [list]
            set fields_elements [list]
            set result_names_used [list] ;#to avoid repeat names when truncated
            set num_results [llength $result_names]
            foreach result_name $result_names {        
                set gidpost_over [ResultsToShapefile::PostResultFillResultsByEntities $analysis_name $result_name $time_step]
                set valid_result_name [ResultsToShapefile::GetValidResultName $result_name $result_names_used]
                if { $gidpost_over == "OnNodes" } {
                    lappend fields_nodes double $valid_result_name 19 9
                } elseif { $gidpost_over == "OnGaussPoints" } {
                    lappend fields_elements double $valid_result_name 19 9
                } else {
                    W "Unexpected gidpost_over $gidpost_over, result $result_name"
                    continue
                }                                    
                lappend result_names_used $valid_result_name
            }
            set cache_elements 0
            if { [llength $fields_elements] || $_nodal_to_elemental } {
                set cache_elements 1
            }
            ResultsToShapefile::CacheMeshPost $cache_elements
            
            if { [llength $fields_nodes] && $_nodal_to_elemental } {
                #assumed num_results equal for all nodes of the element, but maybe not...
                foreach element_id [ResultsToShapefile::GetElementIds] {                                    
                    set vectors [list]
                    foreach node_id [ResultsToShapefile::GetElementNodes $element_id] {
                        lappend vectors $results_by_nodes($node_id)                        
                    }
                    lappend results_by_elements($element_id) {*}[ResultsToShapefile::vector_mean $vectors]
                    #lappend results_by_elements($element_id) {*}[ResultsToShapefile::vector_mean_objarray $vectors]
                }
                array unset results_by_nodes
                lappend fields_elements {*}$fields_nodes
                set fields_nodes [list]
            }
            
            if { $num_files>1 } {
                set filename_i [file rootname $filename]_${analysis_name}_[format "%04d" $i_file][file extension $filename]
            } else {
                set filename_i $filename
            }
                                           
            if { [llength $fields_nodes] } {
                set base_path [file rootname $filename_i]
                if { [llength $fields_elements] } {
                    #if both, must be written in two different files, then need two names, add a suffix
                    set base_path ${base_path}_nodes
                }
                set type point    
                set fields [list integer id 10 0]
                lappend fields {*}$fields_nodes                
                set shp [shapetcl::shapefile $base_path $type $fields]
                foreach node_id [lsort -integer [array names results_by_nodes]] {                    
                    set attributes [list $node_id]
                    foreach value $results_by_nodes($node_id) {
                        if { [IsResultNotDefined $value] } {
                            set value [ResultsToShapefile::GetResultNotDefinedForShapefile]
                        }
                        lappend attributes $value
                    }
                    set entity_id [$shp write [list [ResultsToShapefile::GetNodeCoordinates2D $node_id]] $attributes]                          
                }
                $shp close
            } 
            if { [llength $fields_elements] } {
                set base_path [file rootname $filename_i]
                if { [llength $fields_nodes] } {
                    #if both, must be written in two different files, then need two names, add a suffix
                    set base_path ${base_path}_elements
                }            
                #on gauss points, if more that 1 gauss points the results have been averaged to 1                
                set type polygon
                set fields [list integer id 10 0]
                lappend fields {*}$fields_elements                
                set shp [shapetcl::shapefile $base_path $type $fields]
                $shp configure autoClosePolygons 1                
                foreach element_id [lsort -integer [array names results_by_elements]] {                                                                                
                    set attributes [list $element_id]
                    foreach value $results_by_elements($element_id) {
                        if { [IsResultNotDefined $value] } {
                            set value [ResultsToShapefile::GetResultNotDefinedForShapefile]
                        }
                        lappend attributes $value
                    }
                    set entity_id [$shp write [list [ResultsToShapefile::GetElementVerticesCoordinates2D $element_id]] $attributes]                    
                }
                $shp close
            }
            ResultsToShapefile::ReleaseCacheMeshPost
            array unset results_by_nodes
            array unset results_by_elements
            incr i_file                
            set ::ResultsToShapefile::percent_done [expr {int(($i_file*100.0)/$num_files)}]
            GiD_RaiseEvent GiD_Event_AfterSaveFile $filename_i SHAPEFILE_FORMAT 0
        }    
    }
    GidUtils::DeleteAdvanceBar   
    #W [GidUtils::Crono::End] 
    return $fail
}

proc ResultsToShapefile::WritePost0 { filename } {
    package require shapetcl
    
    set fail [ResultsToShapefile::WritePostMeshAndResults $filename]
    return $fail
}

proc ResultsToShapefile::WritePost { filename } {        
    variable percent_done
    GidUtils::WaitState .gid
    GidUtils::DisableGraphics      
    set fail 0        
    set err [catch { set fail [ResultsToShapefile::WritePost0 $filename] } msg]   
    if { $err } {
        set ::ResultsToShapefile::percent_done 100.0
        set fail 1
        W $msg   
    }
    GidUtils::EnableGraphics
    GidUtils::EndWaitState .gid
    if { $fail } {
        GidUtils::SetWarnLine [_ "Problems writting"]
    } else {
        GidUtils::SetWarnLine [_ "File '%s' written" $filename]
    }
    GiD_RaiseEvent GiD_Event_AfterSaveFile $filename SHAPEFILE_FORMAT $fail
    return $fail
}

#to add more options to the standar file browser
proc ResultsToShapefile::MoreExportOptions { f } {    
    variable _all_analysis
    variable _all_results
    variable _all_result_steps    
    variable _nodal_to_elemental 
    ttk::checkbutton $f.chkAllAnalysis -text [_ "All analysis"] -variable ResultsToShapefile::_all_analysis
    ttk::checkbutton $f.chkAllResults -text [_ "All results"] -variable ResultsToShapefile::_all_results       
    ttk::checkbutton $f.chkAllResultSteps -text [_ "All steps"] -variable ResultsToShapefile::_all_result_steps
    ttk::checkbutton $f.chkNodalToElemental -text [_ "Results on nodes to elements"] -variable ResultsToShapefile::_nodal_to_elemental
    grid columnconfigure $f 3 -weight 1
    grid $f.chkAllAnalysis $f.chkAllResults $f.chkAllResultSteps -sticky w
    grid $f.chkNodalToElemental -columnspan 3
    variable export_dir
    if { ![info exists export_dir] } {
        #trick to show the more options
        set export_dir open
    }
    return ResultsToShapefile::export_dir
}

proc ResultsToShapefile::WritePostW { } {        
    set multiple 0
    set filename [MessageBoxGetFilename file write [_ "Write Shapefile file"] \
            {} {{{Shapefile} {.shp}}} {.shp} $multiple \
            [list [_ "Export options"] ResultsToShapefile::MoreExportOptions]]
    if {$filename != ""} {        
        GidUtils::WaitState .gid
        GidUtils::DisableGraphics              
        set fail 0
        if { [catch { set res [GidUtils::EvalAndEnterInBatch ResultsToShapefile::WritePost $filename] } err options] } {
            set fail 1
        }        
        GidUtils::EnableGraphics
        GidUtils::EndWaitState .gid
        if { $fail } {
            return -options $options $err
        } else {
            GidUtils::SetWarnLine [_ "Shapefile file '%s' written" $filename]
        }
    }
}


#####################################################################################
##################################### OTHER #########################################
#####################################################################################


#proc ResultsToShapefile::AddToMenuPOST { } {    
#    if { [GidUtils::IsTkDisabled] } {  
#        return
#    }
#    if { [GiDMenu::GetOptionIndex Files [list Export "Shapefile..."] POST] != -1 } {
#        return
#    }
#    #try to insert this menu after the word "Files->Export->Visible surfaces to PLY"
#    set position [GiDMenu::GetOptionIndex Files [list Export "Visible surfaces to PLY"] POST]
#    if { $position == -1 } {
#        set position end
#    }
#    GiDMenu::InsertOption Files [list Export "Shapefile..."] $position POST ResultsToShapefile:: "" "" insertafter _ 
#}

#proc ResultsToShapefile::AddToMenu { } {   
#    ResultsToShapefile::AddToMenuPOST
#}

#invoke this menus changes
#Shapefile::AddToMenu
#GiDMenu::UpdateMenus

#register the proc to be automatically called when re-creating all menus (e.g. when doing files new)
#GiD_RegisterPluginAddedMenuProc Shapefile::AddToMenu



