
#########################
##### common procs ######

namespace eval Iber {
    variable plugins { Plus CDX \
	Habitat HydrologicalProcesses SedimentTransport SoilErosion UrbanDrainage IberSWMM WaterQuality NonNewtonian Wood  }
    variable plugins_gendatas { }
    foreach plugin $plugins {
        lappend plugins_gendatas Iber${plugin}Plugin
    }
    variable plugins_labels [list [= "IberPlus"] [= "R-Iber"] \
	    [= "Habitat"] [= "Hydrological processes"] [= "Sediment transport"] [= "Soil erosion"] [= "Urban drainage"] [= "Iber-SWMM"]  \
	    [= "Water quality"] [= "NonNewtonian fluid"] [= "Wood"]]
    variable m_incompatible_plugins
    
    set m_incompatible_plugins(Plus) [list CDX Habitat SedimentTransport UrbanDrainage IberSWMM]
    set m_incompatible_plugins(CDX) [list Plus HydrologicalProcesses SoilErosion UrbanDrainage IberSWMM WaterQuality]
    
    set m_incompatible_plugins(Habitat) [list Plus SoilErosion UrbanDrainage IberSWMM]
    set m_incompatible_plugins(HydrologicalProcesses) [list CDX]
    set m_incompatible_plugins(SedimentTransport) [list Plus SoilErosion UrbanDrainage IberSWMM]
    set m_incompatible_plugins(SoilErosion) [list CDX Habitat SedimentTransport UrbanDrainage IberSWMM]
    set m_incompatible_plugins(UrbanDrainage) [list Plus CDX Habitat SedimentTransport SoilErosion IberSWMM WaterQuality]
    set m_incompatible_plugins(IberSWMM) [list Plus CDX Habitat SedimentTransport SoilErosion UrbanDrainage WaterQuality]
    set m_incompatible_plugins(WaterQuality) [list CDX UrbanDrainage IberSWMM]
    set m_incompatible_plugins(NonNewtonian) [list Plus CDX]
    set m_incompatible_plugins(Wood) [list Plus CDX]
    variable m_plugin_enabled_tmp ;#is an array that store plugin state of widgets, temporary until press "Apply"
}

proc Iber::ModifyDefaultDataMenuCustomPlugins { } {    
    variable plugins    
    foreach plugin $plugins {
        if { [Iber::PluginGetEnabled $plugin] } {            
            Iber::${plugin}AddMenus
        }
    }    
    return 0
}

# first time loading problemtype, invoked by GiD_Event_InitProblemtype
proc Iber::CustomEnableDisable { } {    
    variable plugins    
    foreach plugin $plugins {
        if { [Iber::PluginGetEnabled $plugin] } {
            Iber::${plugin}Enable                 
        }
    }    
    return 0
}

#set the variable, now stored as gendata (and then implicitly saved/read with the model)
proc Iber::PluginSetEnabled { plugin value } {
    variable plugins
    variable m_plugin_enabled       
    
    if { [lsearch $plugins $plugin] == -1 } {
        error "plugin $plugin not valid"
    }    
    if { $value } {
        set gendata_value "Enabled"
        if { $plugin == "Plus" || $plugin == "CDX" } {
            GiD_AccessValue set gendata Iber${plugin}Plugin State ENABLED
        }
    } else {
        set gendata_value "Disabled"
    }
    
    GiD_AccessValue set gendata Iber${plugin}Plugin $gendata_value        
    return $value
}

#get the variable
proc Iber::PluginGetEnabled { plugin } {
    variable plugins    
    
    set enabled 0
    if { [lsearch $plugins $plugin] == -1 } {
        error "plugin $plugin not valid"
    }        
    
    #catch because reading a model saved by other IBER version maybe doesn't has this question
    set gendata_value "Disabled"
    catch { set gendata_value [GiD_AccessValue get gendata Iber${plugin}Plugin] }
    
    if { $gendata_value == "Enabled" } {
        set enabled 1
        if { $plugin == "NonNewtonian" } {
            set enabled 0
            set gendata_value Disabled
            GiD_AccessValue set gendata Rehological_Model Manning
            GiD_AccessValue set gendata Entrainment Off
            GiD_AccessValue set gendata Iber${plugin}Plugin $gendata_value
            Iber::PluginSetEnabled NonNewtonian 0
            GiD_AccessValue set gendata IberDefault Enabled
            
            set materials [GiD_Info materials]
            foreach item $materials {        
                set book [GiD_Info materials $item book]
                if { $book == "Land_Use" } {
                    GiD_AccessValue set material $item IberNonNewtonianPlugin Disabled
                    GiD_AccessValue set material $item IberNonNewtonianPlugin STATE NORMAL 
                }
            }
        }
    } elseif { $gendata_value == "Disabled" } {
        set enabled 0
    } else {
        set enabled 0
        W "gendata Iber${plugin}Plugin value $gendata_value not valid. Consider as Disabled"
    }        
    return $enabled
}

proc Iber::DisableAllPlugins { } {
    variable plugins        
    
    foreach plugin $plugins {
        Iber::PluginSetEnabled $plugin 0
        GiD_AccessValue set gendata Iber${plugin}Plugin Disabled
    }
    
    GiD_AccessValue set gendata IberDefault Enabled        
    
    ### Hidden CND - Hide ###
    GiD_AccessValue set condition 2D_Inlet  1D_connection STATE HIDDEN
    GiD_AccessValue set condition 2D_Outlet 1D_connection STATE HIDDEN 
    
    set materials [GiD_Info materials]
    foreach item $materials {        
        set book [GiD_Info materials $item book]
        if { $book == "Land_Use" } {
            GiD_AccessValue set material $item IberHabitatPlugin Disabled
            GiD_AccessValue set material $item IberHabitatPlugin STATE NORMAL
            GiD_AccessValue set material $item IberNonNewtonianPlugin Disabled
            GiD_AccessValue set material $item IberNonNewtonianPlugin STATE NORMAL 
        }
    }  
    
    ### Hide // Unable PRB options ###
    # Habitat #
    GiD_AccessValue set gendata IberHabitatPlugin Disabled
    GiD_AccessValue set gendata Habitat Off
    # HydrologicalProcesses #
    GiD_AccessValue set gendata Precipitation No_rain
    GiD_AccessValue set gendata Infiltration_losses Off
    # SedimentTransport #
    GiD_AccessValue set gendata Suspended_Transport Off
    GiD_AccessValue set gendata Bed_transport Off
    # SoilErosion #
    GiD_AccessValue set gendata Soil_Erosion Off
    # UrbanDrainage #
    GiD_AccessValue set gendata Flow Surface
	# Iber-SWMM #
    GiD_AccessValue set gendata Urban_Drainage_network Complete_UD_network
    # WaterQuality #
    GiD_AccessValue set gendata Salinity Off
    GiD_AccessValue set gendata Temperature Off
    GiD_AccessValue set gendata Coliforms Off
    GiD_AccessValue set gendata Dissolved_Oxygen Off
    GiD_AccessValue set gendata CBOD Off
    GiD_AccessValue set gendata Nitrogen Off
    GiD_AccessValue set gendata Phosphorus Off
    GiD_AccessValue set gendata Phytoplankton Off
    GiD_AccessValue set gendata pH Off
    GiD_AccessValue set gendata Metals Off
    # NonNewtonian #
    GiD_AccessValue set gendata Rehological_Model Manning
    GiD_AccessValue set gendata Entrainment Off
    
    Iber::RemoveToolbar
    Iber::RemovePostToolbarWUA
    Iber::AddPreToolbar
    Iber::AddPostToolbar    
    Iber::DisableAllPluginsMenus        
}

# [= "Iber_Tools"]
proc Iber::DisableAllPluginsMenus { } {
    GiDMenu::Delete "Iber_Tools" PREPOST =
    Iber::InsertToolsMenu
    Iber::SetDataMenu
    GiDMenu::UpdateMenus
}

# Enable all "oficial" plugins
proc Iber::EnableAllPlugins { } {
    variable plugins
    
    foreach plugin $plugins {
        if { $plugin != "NonNewtonian" && $plugin != "Wood" && $plugin != "Plus"} {
            Iber::PluginSetEnabled $plugin 1
            GiD_AccessValue set gendata Iber${plugin}Plugin Enabled
        } else {
            Iber::PluginSetEnabled $plugin 0
            GiD_AccessValue set gendata Iber${plugin}Plugin Disabled
        }
    }        
    
    Iber::RemoveToolbar
    Iber::RemovePostToolbarWUA
    Iber::AddPreToolbar
    Iber::AddPostToolbarWUA
    
    Iber::EnableAllPluginsMenus
    Iber::CustomEnableDisable
    
    GidUtils::SetWarnLine [= "All plugins have been enabled!"]    
}

# trick, do not delete this commend to be find by translator:
# [= "Plug-ins"]
# [= "Disable Plug-ins"]

proc Iber::EnableAllPluginsMenus { } {
    variable Plug
    
    ### Data menu ###
    GiDMenu::RemoveOption "Iber_Tools" [list "Plug-ins"] PRE =
    GiDMenu::InsertOption "Iber_Tools" [list "Disable Plug-ins"] 100 PRE Iber::DisableAllPlugins "" "" replace =
    
    GiDMenu::UpdateMenus 
}

# trace invoked when change m_plugin_enabled_tmp, clicking its checkbox, plugin is the array name changed
proc Iber::OnChangePluginEnabledTmp { w name1 plugin op } {
    #no not set disable only this changed $plugin, do it for all to restore normal state to others   
    Iber::WindowDisableAllPluginsIncompatible $w
}

proc Iber::PluginsWin { } {
    variable Plug
    variable m_plugin_enabled_tmp
    variable plugins
    variable plugins_labels        
    
    Iber::PluginFillTmpValuesFromDefinitives        
    set w .gid.iberplugins
    
    ### build the window
    InitWindow $w [= "Plug-ins of Iber"] PreIberPluginsWindowGeom Iber::PluginsWin
    
        #ttk::frame $w.fhead2 -style ridge.TFrame -borderwidth 2
    #ttk::label $w.fhead2.label -text "Select calculation module:"
    #grid $w.fhead2.label -sticky w
    #grid columnconfigure $w.fhead2 0 -weight 1
    #grid $w.fhead2 -sticky nsew
    
    #ttk::frame $w.f2 -style ridge.TFrame -borderwidth 2
    ttk::labelframe $w.f2 -text [= " Select calculation module "]
        foreach plugin $plugins label $plugins_labels {
        # do not show hidden plugins
        if { $plugin == "Plus" || $plugin == "CDX" } {
            #do nothing
        } elseif { $plugin != "NonNewtonian" && $plugin != "Wood" } {
            ttk::checkbutton $w.f2.cb$plugin -text $label -variable Iber::m_plugin_enabled_tmp($plugin) -state normal
            grid $w.f2.cb$plugin -sticky w
        }      
    }
    grid columnconfigure $w.f2 0 -weight 1 
    grid $w.f2 -sticky nsew             

    #ttk::frame $w.fhead1 -style ridge.TFrame -borderwidth 2
    #ttk::label $w.fhead1.label -text "Select GPU parallelization:"
    #grid $w.fhead1.label -sticky w
    #grid columnconfigure $w.fhead1 0 -weight 1
    #grid $w.fhead1 -sticky nsew
    
    #ttk::frame $w.f1 -style ridge.TFrame -borderwidth 2
    ttk::labelframe $w.f1 -text [= " Select GPU parallelization "]
        foreach plugin $plugins label $plugins_labels {
        # do not show hidden plugins
        if { $plugin == "Plus" || $plugin == "CDX" } {
            ttk::checkbutton $w.f1.cb$plugin -text $label -variable Iber::m_plugin_enabled_tmp($plugin) -state normal
            #grid $w.f1.cb$plugin -sticky w
        }
    }
    grid $w.f1.cbPlus $w.f1.cbCDX -sticky w
    grid columnconfigure $w.f1 0 -weight 1 
    grid $w.f1 -sticky nsew
        
    ### lower buttons
    ttk::frame $w.buts -style BottomFrame.TFrame  
    ttk::button $w.buts.apply -text [_ "Apply"] -command [list Iber::PluginFillDefinitivesFromTmpValues] -style BottomFrame.TButton
    ttk::button $w.buts.close -text [_ "Close"] -command [list destroy $w] -style BottomFrame.TButton    
    grid $w.buts.apply $w.buts.close -sticky ew -padx 5 -pady 6
    grid $w.buts -sticky sew
    grid anchor $w.buts center    
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 2 -weight 1
    bind $w <Alt-c> [list $w.buts.close invoke]
    bind $w <Escape> [list $w.buts.close invoke]
    bind $w <Destroy> [list +Iber::DestroyPluginsWin %W $w] ;# + to add to previous script        
    
    #set as disable the incompatible ones
    Iber::WindowDisableAllPluginsIncompatible $w        
    trace add variable Iber::m_plugin_enabled_tmp write [list Iber::OnChangePluginEnabledTmp $w]
}

proc Iber::PluginFillTmpValuesFromDefinitives { } {
    variable m_plugin_enabled_tmp
    variable plugins    
    
    foreach plugin $plugins {
        set m_plugin_enabled_tmp($plugin) [Iber::PluginGetEnabled $plugin]            
    }    
}

proc Iber::PluginFillDefinitivesFromTmpValues { } {
    variable plugins     
    variable m_plugin_enabled_tmp               
    
    #first do disable all and then do enable the enabled ones
    Iber::DisableAllPlugins    
    
    set num_enabled 0
    foreach plugin $plugins {                        
        set enabled $m_plugin_enabled_tmp($plugin)
        if { $enabled } {
            Iber::${plugin}Enable
            GiD_AccessValue set gendata Iber${plugin}Plugin Enabled
            incr num_enabled
        }
        Iber::PluginSetEnabled $plugin $enabled
    }  
    
    #better GidUtils::SetWarnLine than WarnWin, to avoid require user interaction (batch mode, etc.)
    GidUtils::SetWarnLine [= "%s Plug-ins have been enabled" $num_enabled]    
}

proc Iber::DestroyPluginsWin { W w } {     
    if { $W != $w } {
        #reenter multiple times, one by toplevel child, only interest w
        return
    }    
    trace remove variable Iber::m_plugin_enabled_tmp write [list Iber::OnChangePluginEnabledTmp $w]  
}

#set window checkbox states disabled based on temporary m_plugin_enabled_tmp value, not definitive value
proc Iber::WindowDisableAllPluginsIncompatible { w } {
    variable plugins
    variable m_plugin_enabled_tmp
    variable m_incompatible_plugins
    
    if { ![winfo exists $w] } {
        #avoid error if Plug-ins window is open and closed and then open a model
        #invoked by unwanted? traces
        return
    }
    
    #initialize all checkbox to normal state and then disable incompatible ones
    foreach plugin $plugins {
        if { $plugin == "Plus" || $plugin == "CDX"  } {        
            $w.f1.cb$plugin configure -state normal      
        } elseif { $plugin != "NonNewtonian" && $plugin != "Wood" } {        
            $w.f2.cb$plugin configure -state normal      
        }  
    }
    
    #disable checkbox widgets of incompatible plugins
    foreach plugin $plugins {
        #set enabled [Iber::PluginGetEnabled $plugin]
        set enabled $m_plugin_enabled_tmp($plugin)
        if { $enabled } {
            foreach incompatible_plugin $m_incompatible_plugins($plugin) {
                if { $incompatible_plugin == "Plus" || $incompatible_plugin == "CDX"  } {        
                    $w.f1.cb$incompatible_plugin configure -state disable      
                } elseif { $incompatible_plugin != "NonNewtonian" && $incompatible_plugin != "Wood" } {        
                    $w.f2.cb$incompatible_plugin configure -state disable      
                }  
                if { $m_plugin_enabled_tmp($incompatible_plugin) != 0 } {
                    set m_plugin_enabled_tmp($incompatible_plugin) 0
                }
            }
        }
    }          
}


### Calculation engines ####
#########################
######### Plus ##########

proc Iber::PlusEnable { } {        
    GiD_AccessValue set gendata IberDefault Disabled
    Iber::PluginSetEnabled Plus 1    
    Iber::PlusAddMenus

    GiDMenu::UpdateMenus
    GidUtils::SetWarnLine [= "IberPlus has been enabled. Only use IberPlus if you know exactly about its capabilities and limitations. Misuse can lead to abnormal results."]
}

proc Iber::PlusAddMenus { } {
    #Do nothing
}


#########################
######### CDX ##########

proc Iber::CDXEnable { } {
    ### Enable main tabs of PRB ###
    GiD_AccessValue set gendata IberDefault Disabled
    Iber::PluginSetEnabled CDX 1

    set scheme {1st_Order_[Fastest]}
    GiD_AccessValue set gendata Numerical_Scheme $scheme

    #Iber::CDXAddMenus
    GiDMenu::UpdateMenus
    GidUtils::SetWarnLine [= "R-Iber has been enabled. Only use R-Iber if you know exactly about its capabilities and limitations. Misuse can lead to abnormal results."]
}

proc Iber::CDXAddMenus { } {
    #Do nothing
}


### Calculation modules ###
#########################
####### Habitat #########

proc Iber::HabitatEnable { } {    
    ### Enable main tabs of PRB ###
    GiD_AccessValue set gendata IberDefault Disabled
    Iber::PluginSetEnabled Habitat 1
    
    set materials [GiD_Info materials]
    foreach item $materials {        
        set book  [GiD_Info materials $item book] 
        if { $book == "Land_Use" } {
            GiD_AccessValue set material $item IberHabitatPlugin Enabled 
            GiD_AccessValue set material $item IberHabitatPlugin STATE HIDDEN
        }
    }
    
    Iber::HabitatAddMenus
    GiDMenu::UpdateMenus 
    Iber::RemovePostToolbar
    Iber::AddPostToolbarWUA
}

# [= "Habitat"]
proc Iber::HabitatAddMenus { } {
    if { [Iber::PluginGetEnabled HydrologicalProcesses] } {
        GidAddUserDataOptions "---" "" end
        GidAddUserDataOptionsMenu [= "Atmospheric"] [list Iber::DataMenu atmospheric %W] end
    } elseif { [Iber::PluginGetEnabled WaterQuality] } {
        GidAddUserDataOptions "---" "" end
        GidAddUserDataOptionsMenu [= "Atmospheric"] [list Iber::DataMenu atmospheric %W] end
    } else {
        #don't load Atmospheric menu
    }
    
    GidAddUserDataOptions "---" "" end
    GidAddUserDataOptionsMenu [= "Habitat"] [list Iber::DataMenu aquatichabitat %W] end    
    
    GiDMenu::InsertOption "Iber_Tools" "---" 30 POST "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Habitat"] 31 POST "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Habitat" [= "WUA"]] 0 POST Wua "" "" replace =
}


#######################################
####### HydrologicalProcesses #########

proc Iber::HydrologicalProcessesEnable { } {
    ### Enable main tabs of PRB ###
    GiD_AccessValue set gendata IberDefault Disabled
    Iber::PluginSetEnabled HydrologicalProcesses 1     
    Iber::HydrologicalProcessesAddMenus
    GiDMenu::UpdateMenus
}

proc Iber::HydrologicalProcessesAddMenus { } {
    if { [Iber::PluginGetEnabled UrbanDrainage] } {
        #don't load Atmospheric menu
    } elseif { [Iber::PluginGetEnabled Habitat] } {
        #don't load Atmospheric menu again (loaded by Habitat)
    } else {
        GidAddUserDataOptions "---" "" end
        GidAddUserDataOptionsMenu [= "Atmospheric"] [list Iber::DataMenu atmospheric %W] end
    }
    GidAddUserDataOptions "---" "" end
    GidAddUserDataOptionsMenu [= "Hydrological Processes"] [list Iber::DataMenu hms %W] end    
}


###################################
####### SedimentTransport #########

proc Iber::SedimentTransportEnable { } {
    ### Enable main tabs of PRB ###
    GiD_AccessValue set gendata IberDefault Disabled
    Iber::PluginSetEnabled SedimentTransport 1   
    Iber::SedimentTransportAddMenus
    GiDMenu::UpdateMenus 
}

# [= "Sediment transport"]
# [= "Granulometry"]
proc Iber::SedimentTransportAddMenus { } {
    GidAddUserDataOptions "---" "" end
    GidAddUserDataOptionsMenu [= "Sediment Transport"] [list Iber::DataMenu seds %W] end
    
    GiDMenu::InsertOption "Iber_Tools" "---" 60 POST "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Sediment transport"] 61 POST "" "" "" replace =
    GiDMenu::InsertOption "Iber_Tools" [list "Sediment transport" "Granulometry"] 0 POST Stratum::Window "" "" replace =    
}


#############################
####### SoilErosion #########

proc Iber::SoilErosionEnable { } {
    ### Enable main tabs of PRB ###
    GiD_AccessValue set gendata IberDefault Disabled
    Iber::PluginSetEnabled SoilErosion 1   
    Iber::SoilErosionAddMenus
    GiDMenu::UpdateMenus  
}


proc Iber::SoilErosionAddMenus { } {    
    GidAddUserDataOptions "---" "" end
    GidAddUserDataOptionsMenu [= "Soil Erosion"] [list Iber::DataMenu soilerosion %W] end
}


##################################
######### UrbanDrainage ##########

proc Iber::UrbanDrainageEnable { } {        
    GiD_AccessValue set gendata IberDefault Disabled
    Iber::PluginSetEnabled UrbanDrainage 1   
    ### Hidden CND - Show ###
    GiD_AccessValue set condition 2D_Inlet  1D_connection STATE SHOW
    GiD_AccessValue set condition 2D_Outlet 1D_connection STATE SHOW    
    Iber::UrbanDrainageAddMenus
    GiDMenu::UpdateMenus
}

proc Iber::UrbanDrainageAddMenus { } {
    GidAddUserDataOptions "---" "" end
    GidAddUserDataOptionsMenu [= "Urban Drainage"] [list Iber::DataMenu urbandrainage %W] end
}

##################################
######### Iber-SWMM ##########
proc Iber::IberSWMMEnable { } {        
    GiD_AccessValue set gendata IberDefault Disabled
    Iber::PluginSetEnabled IberSWMM 1   
    Iber::IberSWMMAddMenus
    GiDMenu::UpdateMenus
}

proc Iber::IberSWMMAddMenus { } {
    GidAddUserDataOptions "---" "" end
    GidAddUserDataOptionsMenu [= "Iber SWMM"] [list Iber::DataMenu iberswmm %W] end
	#GidAddUserDataOptionsMenu [= "SUDS"] [list Iber::DataMenu suds %W] end
}


##############################
####### WaterQuality #########

proc Iber::WaterQualityEnable { } {
    ### Enable main tabs of PRB ###
    GiD_AccessValue set gendata IberDefault Disabled
    Iber::PluginSetEnabled WaterQuality 1
    Iber::WaterQualityAddMenus
    GiDMenu::UpdateMenus
}

proc Iber::WaterQualityAddMenus { } {
    if { [Iber::PluginGetEnabled HydrologicalProcesses] } {
        #don't load Atmospheric menu again
    } elseif { [Iber::PluginGetEnabled SedimentTransport] } {
        #don't load Atmospheric menu again (loaded by SedimentTransport)
    } elseif { [Iber::PluginGetEnabled Habitat] } {
        #don't load Atmospheric menu again (loaded by Habitat)
    } else {
        GidAddUserDataOptions "---" "" end
        GidAddUserDataOptionsMenu [= "Atmospheric"] [list Iber::DataMenu atmospheric %W] end
    }
    GidAddUserDataOptions "---" "" end
    GidAddUserDataOptionsMenu [= "Water Quality"] [list Iber::DataMenu waterquality %W] end
}


#########################
##### NonNewtonian ######

proc Iber::NonNewtonianEnable { } {        
    ### Enable main tabs of PRB ###
    GiD_AccessValue set gendata IberDefault Disabled
    Iber::PluginSetEnabled NonNewtonian 1    
    
    set materials [GiD_Info materials]
    foreach item $materials {        
        set book  [GiD_Info materials $item book] 
        if { $book == "Land_Use" } {
            GiD_AccessValue set material $item IberNonNewtonianPlugin Enabled
            GiD_AccessValue set material $item IberNonNewtonianPlugin STATE HIDDEN
        }
    }
    
    Iber::NonNewtonianAddMenus
    GiDMenu::UpdateMenus
    WarnWin [= "Only use this module if you know about Non-Newtonian Fluids and about this software capabilities and limitations."]
}


proc Iber::NonNewtonianAddMenus { } {
    GidAddUserDataOptions "---" "" end
    GidAddUserDataOptionsMenu [= "Non Newtonian"] [list Iber::DataMenu nonnewtonian %W] end
}


#########################
######### Wood ##########

proc Iber::WoodEnable { } {
    ### Enable main tabs of PRB ###
    GiD_AccessValue set gendata IberDefault Disabled
    Iber::PluginSetEnabled Wood 1
    Iber::WoodAddMenus
    GiDMenu::UpdateMenus
    WarnWin [= "Only use IberWood if you know about Large Wood Transport and about this software capabilities and limitations."]
}


proc Iber::WoodAddMenus { } {
    GidAddUserDataOptions "---" "" end
    GidAddUserDataOptionsMenu [= "Wood"] [list Iber::DataMenu wood %W] end
}
