#### create by Georgina 07/2014
#Geor 1
#
###################################################################################


namespace eval ::PostUtils {
    
}
#over: nodes or elements
#results: list with this \
    possible options: nodesnum, nodescoord, elemsnum,
#         elemsconec, \
    elemstype, elemslayer, <result name>
#selection: list of id's (can be used \
    a:b)
#presentation: TEXT or LIST
proc ::PostUtils::SelectResults { results over selection {presentation TEXT} } {
    set info [eval GiD_Info list_entities $over $selection]
    set info [split $info \n]
    set res ""
    if { $over == "nodes" } {
        if { [GiD_Info postprocess get cur_result] != "" } {
            set infoitems {numinfo usedbyinfo currentinfo moreinfo}
        } else {
            set infoitems {numinfo usedbyinfo moreinfo}
        }
        foreach $infoitems [lrange $info 1 end-1] {
            set res2 ""
            
            if { [lsearch $results "nodesnum"] != -1 } {
                
                lappend res2 [lindex $numinfo 1]
            }
            
            if { [lsearch $results "nodescoord"] != -1 } {
                lappend res2 \
                    [string trim [lindex $numinfo 2]]
            }
            
            foreach resultinfo [lindex $moreinfo 2] {
                set name [lindex \
                        $resultinfo 0]
                if { [lsearch $results $name] != -1 } {
                    
                    if { [string trim [lindex $resultinfo 1]] != "" } {
                        
                        set resultnames($name) ""
                        
                        foreach item [string trim [lindex $resultinfo 1]] {
                            
                            lappend resultnames($name) $item
                        }
                        
                    }
                    lappend res2 [string trim [lindex [lindex $resultinfo 2] 0]]
                }
            }
            
            lappend res $res2
        }
    } elseif { $over == "elements" } {
        if { [GiD_Info postprocess get cur_result] != "" } {
            set infoitems {numinfo currentinfo gausspoints moreinfo}
        } else {
            set infoitems {numinfo gausspoints moreinfo}
            
        }
        foreach $infoitems [lrange $info 1 end-1] {
            set res2 ""
            if { [lsearch $results "elemsnum"] != -1 } {
                
                lappend res2 [lindex $numinfo 1]
            }
            
            if { [lsearch $results "elemsconec"] != -1 } {
                lappend res2 [string trim [lindex $numinfo 2]]
            }
            
            if { [lsearch $results "elemstype"] != -1 } {
                
                lappend res2 [string trim [lindex $numinfo 3]]
            }
            
            if { [lsearch $results "elemslayer"] != -1 } {
                
                lappend res2 [string trim [lindex $numinfo 4]]
            }
            
            foreach resultinfo [lindex $moreinfo 2] {
                set name [lindex $resultinfo 0]
                if { [lsearch $results $name] != -1 } {
                    if { [string trim [lindex $resultinfo 2]] != "" } {
                        set resultnames($name) ""
                        
                        foreach item [string trim [lindex $resultinfo 2]] {
                            lappend resultnames($name) $item
                        }
                        
                    }
                    lappend res2 [string trim [lindex $resultinfo 3]]
                }
            }
            
            lappend res $res2
        }
    }
    if { $presentation == "TEXT" } {
        set newres ""
        foreach item $results {
            
            if { [info exists resultnames($item)] && $resultnames($item) != "" } {
                foreach component $resultnames($item) {
                    
                    set txt $item:$component
                    regsub -all { } $txt _ txt
                    append newres $txt " "
                }
                
            } else {
                if { $item == "nodesnum" || $item == "elemsnum" } {
                    set txt [_ "Number"]
                    
                    regsub -all { } $txt _ txt
                } elseif { $item == "nodescoord" } {
                    set txt "X Y Z"
                } elseif { $item == "elemsconec" } {
                    set txt [_ "connectivities"]
                } else {
                    set txt $item
                    regsub -all { } $txt _ txt
                }
                
                append newres $txt " "
            }
        }
        
        set newres [string trim $newres]\n
        foreach item $res {
            
            #append newres [eval concat [eval concat $item]]\n
            
            append newres [eval concat $item]\n
        }
        set res $newres
    }
    WarnWinText $res
    return $res
    
}
proc ::PostUtils::ApplySelectResults { w } {
    global PostUtils
    set results ""
    if { $PostUtils(over) == "nodes" } {
        if { $PostUtils(output,number) } {
            lappend results nodesnum
        }
        if { $PostUtils(output,coordinates) } {
            lappend results nodescoord
        }
    }  else {
        if { $PostUtils(output,number) } {
            
            lappend results elemsnum
        }
        if { $PostUtils(output,connectivities) } {
            lappend results elemsconec
        }
    }
    foreach item [GiD_Info postprocess get cur_results_list contour_fill] {
        regsub -all _ $item { } item
        if { $PostUtils(over,$item) == $PostUtils(over) && $PostUtils(output,$item) } {
            lappend results $item
        }
    }
    if { [llength $results] > 0 } {
        set SmallWinSelecting [GiD_Set SmallWinSelecting]
        FinishButton $w $w.buts [_ "Press 'Finish' to end selection"] "" disableall $SmallWinSelecting
        set entities [GidUtils::PickEntities $PostUtils(over) multiple]
        
        EndFinishButton $w "" $SmallWinSelecting
        if { $entities != "" } {
            ::PostUtils::SelectResults $results $PostUtils(over) $entities]
        } else {
            GidUtils::SetWarnLine [_ "Must select some entity"]
        }
    } else {
        GidUtils::SetWarnLine [_ "Must select some result"]
    }
}

proc ::PostUtils::SelectResultsW { { w .gid.selectresults } } {
    global PostUtils
    InitWindow $w [_ "Select Results"] PostSelectResultsWindowGeom ::PostUtils::SelectResultsW
    ttk::frame $w.f -style ridge.TFrame -borderwidth 2
    ttk::frame $w.f.f0
    ttk::radiobutton $w.f.f0.rb0 -text [_ "Nodes"] -value nodes -variable PostUtils(over)
    ttk::radiobutton $w.f.f0.rb1 -text [_ "Elements"] -value elements -variable PostUtils(over)
    if { ![info exists PostUtils(over)] || ($PostUtils(over) != "nodes" && $PostUtils(over) != "elements") } {
        set PostUtils(over) nodes
    }
    if { ![info exists PostUtils(output,number)] } {
        set PostUtils(output,number) 1
    }
    if { ![info exists PostUtils(output,coordinates)] } { set PostUtils(output,coordinates) 0 }
    
    ttk::checkbutton $w.f.f0.cbcoordinates -text [_ "Coordinates"] -variable PostUtils(output,coordinates)
    if { $PostUtils(over) == "nodes" } {
        
        $w.f.f0.cbcoordinates configure -state normal
    } else {
        
        $w.f.f0.cbcoordinates configure -state disable
    }
    if { ![info exists PostUtils(output,connectivities)] } { set PostUtils(output,connectivities) 0 }
    
    ttk::checkbutton $w.f.f0.cbconnectivities -text [_ "Connectivities"] -variable PostUtils(output,connectivities)
    if { $PostUtils(over) == "nodes" } {
        $w.f.f0.cbconnectivities configure -state disable
    } else {
        $w.f.f0.cbconnectivities configure -state normal
    }
    if { ![info exists PostUtils(output,number)] } { set PostUtils(output,number) 0 }
    
    ttk::checkbutton $w.f.number -text [_ "Number"] -variable PostUtils(output,number)
    #array unset PostUtils output,*
    set results [GiD_Info postprocess get cur_results_list contour_fill]
    set cont 0
    
    foreach item $results {
        set header [GiD_Result get -info [list $item \
                    [GiD_Info postprocess get cur_analysis] [GiD_Info postprocess get cur_step]]]
        regsub -all _ $item { } item
        if { [lindex [lindex $header 0] 5] == "OnNodes" } {
            set PostUtils(over,$item) nodes
        } else {
            
            set PostUtils(over,$item) elements
        }
        if { ![info exists PostUtils(output,$item)] } { set PostUtils(output,$item) 0 }
        ttk::checkbutton $w.f.cb$cont -text [TranslateResultName $item] -variable PostUtils(output,$item)
        if { $PostUtils(over,$item) == $PostUtils(over) \
        } {
            $w.f.cb$cont configure -state normal
        } else {
            $w.f.cb$cont configure -state disable
        }
        incr cont
    }
    grid $w.f.f0.rb0 $w.f.f0.rb1 -sticky w
    grid $w.f.f0.cbcoordinates $w.f.f0.cbconnectivities -sticky w
    grid $w.f.f0 -sticky ew
    grid $w.f.number -sticky w
    for {set i 0} {$i < $cont} {incr i} {
        grid $w.f.cb$i -sticky w
    }
    grid rowconfigure $w.f [expr {$cont+2}] -weight 1
    grid columnconfigure $w.f 0 -weight 1
    grid $w.f -sticky nsew    
    ttk::frame $w.buts -style BottomFrame.TFrame  
    ttk::button $w.buts.apply -text [_ "Select"] -command "::PostUtils::ApplySelectResults $w" -style BottomFrame.TButton
    ttk::button $w.buts.close -text [_ "Close"] -command "destroy $w" -style BottomFrame.TButton
    
    grid $w.buts.apply $w.buts.close -sticky ew -padx 5 -pady 6
    grid $w.buts -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.buts center }
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 0 -weight 1
    bind $w <Alt-c> "$w.buts.close invoke"
    bind $w <Escape> "$w.buts.close invoke"
    bind $w <Destroy> [list +::PostUtils::DestroySelectResults %W $w] ;# + to add to \
        previous script
    trace add variable PostUtils(over) write "::PostUtils::ChangeSelectResultsOver $w ;#"
}

proc ::PostUtils::DestroySelectResults { W w } {
    if { $W != $w } return
    
    #reenter multiple times, one by toplevel child, only interest w
    global PostUtils
    #catch { array unset PostUtils result,* } ;#remove also traces
    
    trace remove variable PostUtils(over) write "::PostUtils::ChangeSelectResultsOver $w ;#"
}

proc ::PostUtils::ChangeSelectResultsOver { w } {
    global PostUtils
    if { $PostUtils(over) == "nodes" } {
        $w.f.f0.cbcoordinates configure -state normal
        $w.f.f0.cbconnectivities configure -state disable
    } else {
        $w.f.f0.cbcoordinates configure -state disable
        $w.f.f0.cbconnectivities configure -state normal
    }
    set results [GiD_Info postprocess get cur_results_list contour_fill]
    set cont 0
    
    foreach item $results {
        regsub -all _ $item { } item
        if { $PostUtils(over,$item) == $PostUtils(over) } {
            $w.f.cb$cont configure -state normal
        } else {
            $w.f.cb$cont configure -state disable
        }
        incr cont
    }
}

