# Iber::Shapefile_convert_gid_conditions called by GiD_Event_AfterOpenFile to convert generic-imported-conditions to some of Iber
proc Iber::MapValues_point { values } {
	set values_new $values
	set num_fields [llength $values]
    set table_curr [lindex $values 6]
	#set inout [lindex $values 5]
	if { [lindex $table_curr 0] != "#N#" } {
		# If table of InOut is not defined, set it 0
		#lset values 5 0
		set num_fields 7
		set text "Table String C 50 0"
		lappend fields $text
		set table_default "#N# 2 0.0 0.0"
		lset values_new 6 $table_default
	} elseif { $num_fields < 6 || $num_fields > 7 } {
		error "Wrong format of the *.dbf file of $filename"
	}
	return $values_new
}

proc Iber::MapValues_line { values } {
	set values_new $values
	set num_fields [llength $values]
    set shape_curr [lindex $values 1]
	if { $num_fields == 5 && $shape_curr == "Circular" } {
		# If shape is Circular, insert Questions 'Height' and 'Widht'
		set oldList $values
		set midList [linsert $oldList 3 0.0]
		set values_new [linsert $midList 4 0.0]
	} elseif { $num_fields == 6 && $shape_curr == "Rectangular" } {
		# If shape is rectangular, insert Questions 'Diamater'
		set oldList $values
		set values_new [linsert $oldList 2 0.0]
	} elseif { $num_fields < 5 || $num_fields > 7 } {
		error "Wrong format of the *.dbf file of $filename"
	}
	return $values_new
}

proc Iber::MapValues_surface { values } {
	set values_new [list ""]
	#Insert the question Id_Surface
	set oldList $values
	set values_new [linsert $oldList 10 0]   

	return $values_new
}

proc Iber::Shapefile_convert_gid_conditions { } {
	set map_condition(point) Junctions
	set map_condition(line) Pipes
	#set map_condition(surface) Subsurface_Areas
	set map_condition(surface) Roofs_properties
	set map_condition_values_proc(point) Iber::MapValues_point
	set map_condition_values_proc(line) Iber::MapValues_line
	set map_condition_values_proc(surface) Iber::MapValues_surface
	foreach gid_entity_type {point line surface} {
		set condition_name ${gid_entity_type}_imported-1
		if { [lsearch [GiD_Info conditions over_$gid_entity_type] $condition_name] != -1 } {
			foreach item [GiD_Info conditions $condition_name geometry] {
				set entity_id [lindex $item 1]
				set values [lrange $item 3 end]
				set values_new [$map_condition_values_proc($gid_entity_type) $values]
				GiD_AssignData condition $map_condition($gid_entity_type) ${gid_entity_type}s $values_new [list $entity_id]
			}
			GiD_UnAssignData condition $condition_name ${gid_entity_type}s all
			GiD_CreateData delete condition $condition_name
		}
	}
    return 0
}
