#########################
### Reports generator ###
### v1.0 ################
### MSR 2023 ############

namespace eval Reports {

}


proc Reports::GaugesReportBasicWin { } {
    global ReportsPriv
    set w .gid.basicreport
    
    #creacion de la ventana principal
    InitWindow $w [= "Gauges basic report"] PostReportsWindowGeom Reports::GaugesReportBasicWin
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    
    #creacion de los witgets
    set f [ttk::frame $w.frmParams]
    
    #Label 1
    set ReportsPriv(GraphsCheck) 1
    ttk::label $f.graphstext -text [= "Point evolution"]
    ttk::checkbutton $f.graphs -variable ReportsPriv(GraphsCheck)
    grid $f.graphstext $f.graphs -sticky w
    grid columnconfigure $w.frmParams {0 1} -weight 1
    grid $w.frmParams -sticky new -padx 2 -pady 2
    
    #Label 2
    set ReportsPriv(MapsCheck) 1
    ttk::label $f.mapstext -text [= "Maps of maximums"]
    ttk::checkbutton $f.maps -variable ReportsPriv(MapsCheck)
    grid $f.mapstext $f.maps -sticky w
    grid columnconfigure $w.frmParams {0 1} -weight 1
    grid $w.frmParams -sticky new -padx 2 -pady 2
    
    #Label 2.1
    set ReportsPriv(zoomframe) 100
    ttk::labelframe $w.frmType1 -text [= " Maps options "]
    ttk::label $w.frmType1.zoomtext -text [= "Frame offset (m): "]
    ttk::entry $w.frmType1.zoom -textvariable ReportsPriv(zoomframe)
    #Label 2.2
    set ReportsPriv(SignalCheck) 0
    ttk::label $w.frmType1.signaltext -text [= "Signal location"]
    ttk::checkbutton $w.frmType1.signal -variable ReportsPriv(SignalCheck)
    #Label 2.3
    set ReportsPriv(BackImgCheck) [GiD_BackgroundImage get show]
    ttk::label $w.frmType1.backimgtext -text [= "Show/Hide image"]
    ttk::checkbutton $w.frmType1.backimg -variable ReportsPriv(BackImgCheck)
    
    grid $w.frmType1.zoomtext $w.frmType1.zoom -sticky ew
    grid $w.frmType1.signaltext $w.frmType1.signal -sticky ew
    grid $w.frmType1.backimgtext $w.frmType1.backimg -sticky ew
    grid columnconfigure $w.frmType1 {0 1} -weight 1
    grid $w.frmType1 -sticky new -padx 5 -pady 5
    
    #Label 3.1
    set ReportsPriv(zoomframe) 100
    ttk::labelframe $w.frmType2 -text [= " Display options "]
    #Label 3.2
    set ReportsPriv(TransCheck) 0
    ttk::label $w.frmType2.transtext -text [= "Transparency"]
    ttk::checkbutton $w.frmType2.trans -variable ReportsPriv(TransCheck)
    #Label 3.3
    set ReportsPriv(StyleCheck) "Body"
    ttk::label $w.frmType2.styletext -text [= "Display style: "]
    ttk::radiobutton $w.frmType2.body -text [= "Body"] -value Body -variable ReportsPriv(StyleCheck)
    ttk::radiobutton $w.frmType2.bodybound -text [= "Body Bound"] -value Body_Bound -variable ReportsPriv(StyleCheck)
    ttk::radiobutton $w.frmType2.bodylines -text [= "Body Lines"] -value Body_Lines -variable ReportsPriv(StyleCheck)
    #Label 3.4
    set ReportsPriv(ViewCheck) "view_cf"
    ttk::label $w.frmType2.viewtext -text [= "View: "]
    ttk::radiobutton $w.frmType2.cf -text [= "Contour Fill"] -value view_cf -variable ReportsPriv(ViewCheck)
    ttk::radiobutton $w.frmType2.scf -text [= "Smooth Contour Fill"] -value view_scf -variable ReportsPriv(ViewCheck)
    
    grid $w.frmType2.transtext $w.frmType2.trans -sticky new
    grid $w.frmType2.styletext -sticky new
    grid $w.frmType2.body -sticky new
    grid $w.frmType2.bodybound -sticky new
    grid $w.frmType2.bodylines -sticky new
    grid $w.frmType2.viewtext -sticky new
    grid $w.frmType2.cf -sticky new
    grid $w.frmType2.scf -sticky new
    grid columnconfigure $w.frmType1 {0 1} -weight 1
    grid $w.frmType2 -sticky new -padx 5 -pady 5
    
    #Buttons settins
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.btnRepBas -text [= "Execute"] -command "Reports::GaugesReportBasic $w" -underline 0 -width 10
    ttk::button $f.btncloseBas -text [= "Close"] -command "destroy $w" -underline 0 -width 10
    grid $f.btnRepBas $f.btncloseBas -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center } 
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 2 -weight 1
    
}

proc Reports::GaugesReportBasic { w } {
    global ReportsPriv
    variable Xcentre
    variable Ycentre
    #     variable elemID
    variable CoordinatesList
    variable NumGauges
    
    # working folders   
    set Project [GiD_Info project ModelName] 
    if { $Project == "UNNAMED" } {
        MessageBoxOk [= "Error" ] [= "Before writting results, a project title is needed. Save project to get it" ] error
        return
    } else {
        set directory $Project.gid
        if { [file pathtype $directory] == "relative" } {
            set directory [file join [pwd] $directory]
        }
        set workingdir [file join $directory Reports]
        file mkdir $workingdir
    }
    
    set initime [clock seconds]
    
    #check IF maps of MAX exists
    set all_anal [GiD_Info postprocess get all_analysis]
    if { ![info exists current_anal]  } {
        set current_anal [lindex $all_anal 0]
    }
    foreach item $all_anal {
        if { $item == "Maps of Maximums" } {
            set current_anal $item
        }
    }
    if { $current_anal != "Maps of Maximums" } {
        MessageBoxOk [= "Error" ] [= "Before generating report, maximums of the hydraulic variables (depth, velocity and specific discharge) are needed. Enable it and recalculate" ] error
        return
    } else {
        set allsteps [GiD_Info postprocess get all_steps $current_anal]
        set maxstep [lindex $allsteps end]
        set results [GiD_Info postprocess get results_list contour_fill $current_anal [lindex $allsteps end]]
        set current_result [lindex $results 0]
        
        GiD_Process Mescape Results AnalysisSel $current_anal $maxstep Geometry Original escape
        GiD_Process Mescape Results Geometry NoResults escape
        if { $ReportsPriv(ViewCheck) == "view_cf" } {
            GiD_Process Mescape Results ContourFill $current_result
        } elseif { $ReportsPriv(ViewCheck) == "view_scf" } {
            GiD_Process Mescape Results SmoothContourFill $current_result
        }
    }
    
    #check IF gauges ON
    set GaugesOnOff [GiD_AccessValue get gendata Gauges]
    if { $GaugesOnOff == On } {
        Reports::CheckIfInside
    } else {
        MessageBoxOk [= "Error in Gauges definition" ] [= "Gauges result is 'Off' (Data >> Problem data > Results)" ] error
        return
    }
       
    ##############################################################
    # Exporting maps of MAX of depth/velocity/specific discharge #
    #Define the Analysis => MAXIMUMS
    if { $ReportsPriv(MapsCheck) == 0 } {
        #do nothing
    } else {
        
        set all_anal [GiD_Info postprocess get all_analysis]
        if { ![info exists current_anal]  } {
            set current_anal [lindex $all_anal 0]
        }
        foreach item $all_anal {
            if { $item == "Maps of Maximums" } {
                set current_anal $item
            }
        }
        if { $current_anal != "Maps of Maximums" } {
            MessageBoxOk [= "Error" ] [= "Before generating report, maximums of the hydraulic variables (depth, velocity and specific discharge) are needed. Enable it and recalculate" ] error
            return
        }
        
        set allsteps [GiD_Info postprocess get all_steps $current_anal]
        set maxstep [lindex $allsteps end]
        set results [GiD_Info postprocess get results_list contour_fill $current_anal [lindex $allsteps end]]
        
        GiD_Process Mescape Utilities Comments Comments no escape MEscape 'Redraw
        GiD_Process Mescape
        #GiD_Process Mescape Utilities Variables PostLegendFormat %.2f escape escape
        
        if { $ReportsPriv(BackImgCheck) == 0 } {
            GiD_BackgroundImage set show 0
        } elseif { $ReportsPriv(BackImgCheck) == 1 } {
            GiD_BackgroundImage set show 1
        }
        
        if { $ReportsPriv(TransCheck) == 1 } {
            set numlayers [GiD_Layers list]
            foreach item $numlayers {
                GiD_Process Mescape Mescape Utilities ChangeProperty SurfaceSets $item transparent yes
            }
            GiD_Process 'Zoom Frame
        }
        GiD_Process Mescape DisplayStyle $ReportsPriv(StyleCheck)
        
        foreach result $results {
            if { $result == "Depth (m)" || $result == "Velocity (m/s)" || $result == "Specific Discharge (m2/s)" } {
                
                set name ""
                set name $result
                set name [string map {(m/s) ""} $name]
                set name [string map {(m) ""} $name]
                set name [string map {(m2/s) ""} $name]
                set name [string map {_m/s ""} $name]
                set name [string map {_m2/s ""} $name]
                set name [string map {/ ""} $name]
                set name [string map {| ""} $name]
                
                GiD_Process Mescape Results AnalysisSel $current_anal $maxstep Geometry Original escape
                GiD_Process Mescape Results Geometry NoResults escape
                if { $ReportsPriv(ViewCheck) == "view_cf" } {
                    GiD_Process Mescape Results ContourFill $result
                } elseif { $ReportsPriv(ViewCheck) == "view_scf" } {
                    GiD_Process Mescape Results SmoothContourFill $result
                }
                for {set i 1} {$i <= $NumGauges} {incr i 1} {
                    
                    set GaugeID $i
                    
                    #Creating the Zoom frame
                    GiD_Process 'Rotate Angle 270 90
                    set zoom $ReportsPriv(zoomframe)
                    set x0 [expr {$Xcentre($GaugeID)-$zoom}]
                    set y0 [expr {$Ycentre($GaugeID)+$zoom}]
                    set x1 [expr {$Xcentre($GaugeID)+$zoom}]
                    set y1 [expr {$Ycentre($GaugeID)-$zoom}]
                    
                    GiD_Process 'Zoom Points FNoJoin $x0,$y0,0 $x1,$y1,0
                    if { $ReportsPriv(SignalCheck) == 1 } {
                        GiD_Process Mescape Utilities SignalEntities Points FNoJoin $Xcentre($GaugeID),$Ycentre($GaugeID),0tol0.215277
                    }
                    
                    #Screenshots
                    set filenameSS "Max $name at ($Xcentre($GaugeID),$Ycentre($GaugeID)).png"       
                    set mapfile [file join $workingdir $filenameSS]
                    GiD_Process 'Hardcopy PNG $mapfile
                    GiD_Process Mescape
                }
            }   
        } 
        
        GiD_Process Mescape Utilities Comments Comments yes escape MEscape 'Redraw
        GiD_Process Mescape
        
    }
    
    GiD_Set UseMoreWindows 0
    
    GidUtils::DisableGraphics
    GidUtils::DisableWarnLine
    GidUtils::WaitState .gid
    
    #############################################
    # Creating graphs on CoordinatesList points #
    # Creating from *.post.res file
    if { $ReportsPriv(GraphsCheck) == 0 } {
        #do nothing
    } else {
        set all_anal [GiD_Info postprocess get all_analysis]
        foreach item $all_anal {
            if { $item == "Hydraulic" } {
                set current_anal $item
            }
        }
        if { $current_anal != "Hydraulic" } {
            MessageBoxOk [= "Error" ] [= "Before generating report, maximums of the hydraulic variables (depth, velocity and specific discharge) are needed. Enable it and recalculate" ] error
            return
        }
        
        set allsteps [GiD_Info postprocess get all_steps $current_anal]
        set maxstep [lindex $allsteps end]
        set results [GiD_Info postprocess get results_list contour_fill $current_anal [lindex $allsteps end]]
        
        GiD_Process Mescape Results AnalysisSel $current_anal 0 Geometry Original escape escape escape escape escape escape escape escape results Geometry NoResults
        
        foreach result $results {
            if { $result == "Depth (m)" || $result == "Velocity (m/s)" || $result == "Specific Discharge (m2/s)"} {
                #To obtain the variable name without units & spaces
                set name ""
                set name $result
                set name [string map {(m/s) ""} $name]
                set name [string map {(m) ""} $name]
                set name [string map {(m2/s) ""} $name]
                set name [string map {_m/s ""} $name]
                set name [string map {_m2/s ""} $name]
                set name [string map {/ ""} $name]
                set name [string map {| ""} $name]
                set name [string trim $name]
                
                if { [GiD_GraphSet exists $name] } {
                    set existinggraphset $name
                    GiD_GraphSet delete $existinggraphset
                }
                
                GiD_Process Mescape Results Graphs GraphSet Create $name Mescape Results Graphs GraphSet Current $name
                GiD_GraphSet current $name
                set numgraphs 0
                set numgraphsnew 0
                set CoordinatesListNew [list]
                foreach item $CoordinatesList {                   
                    if { $result == "Depth (m)"} {
                        GiD_Process MEscape Results Graphs PointEvolution $result
                        GiD_Process $item escape               
                    } else {
                        set resultaux "|$result|"
                        GiD_Process MEscape Results Graphs PointEvolution $result $resultaux
                        GiD_Process $item escape
                    }
                    set numgraphsnew [llength [GiD_Graph list $name]]
                    if { $numgraphsnew > $numgraphs } {
                        lappend CoordinatesListNew $item
                        incr numgraphs
                    } else {
                        #                         GidUtils::SetWarnLine [= "Point ($item) has no results in time. Point omitted." $item]
                    }
                }
                set auxvar [GiD_Graph list]
                set itemid 0
                foreach item $CoordinatesListNew {
                    set itemnamenew "($item)"
                    set itemnameold [lindex $auxvar $itemid]
                    GiD_Process Mescape Results Graphs OptionsGraph TitleGraphs $itemnameold $itemnamenew escape
                    incr itemid
                    
                    if { $result == "Depth (m)"} {
                        set Yunits "m"
                    } elseif { $result == "Velocity (m/s)"} {
                        set Yunits "m/s"
                    } elseif { $result == "Specific Discharge (m2/s)"} {
                        set Yunits "m2/s"
                    }
                }
                GiD_Process Mescape Results Graphs OptionsGraph X_axis SetLabel Time Mescape Results Graphs OptionsGraph X_axis SetUnit s escape
                GiD_Process Mescape Results Graphs OptionsGraph Y_axis SetLabel $name Mescape Results Graphs OptionsGraph Y_axis SetUnit $Yunits escape
            }
        }
        GiD_Process Mescape Mescape
        #Graphs
        set totgrafssets [GiD_GraphSet list]
        foreach grset $totgrafssets {
            set graphsetname [GiD_GraphSet current $grset]
            set totgrafs [GiD_Info postprocess get all_graphs]
            foreach item $totgrafs {
                set filenameG "Evolution of $graphsetname at $item.grf"
                set fileG [file join $workingdir $filenameG]
                set coord "$item"
                GiD_Process Mescape Results Graphs WriteGraphSet OneGraph $graphsetname $coord $fileG
                GiD_Process Mescape
            }
        }
    }
    
    GiD_Set UseMoreWindows 1
    GidUtils::EnableGraphics
    GidUtils::EnableWarnLine
    GidUtils::EndWaitState .gid   
    
    set endtime [clock seconds]
    set elapsedtime [expr {$endtime-$initime}]
    GidUtils::SetWarnLine [= "Basic report has been created in %s seconds in 'Reports' folder" $elapsedtime ]
}

proc Reports::GaugesReportExtended { } {
    global ReportsPriv
    variable Xcentre
    variable Ycentre
    variable CoordinatesList
    variable NumGauges
    variable NumFigures
    
    #working directory
    set Project [GiD_Info project ModelName] 
    if { $Project == "UNNAMED" } {
        MessageBoxOk [= "Error" ] [= "Before writting results, a project title is needed. Save project to get it" ] error
        return
    } else {
        set directory $Project.gid
    }
    
    #verify if 'Reports' folder exists
    set dirreport [file join $directory Reports]
    set ReportsPriv(dirreport) $dirreport
    set checkdirreport [file exists $dirreport]
    if { $checkdirreport == 0 } {
        MessageBoxOk [= "Error in working directory" ] [= "Folder of 'Reports' does not exist in the current working directoty" ] error
        set answ [MessageBoxOptionsButtons [= "Report" ] [= "Do you want to generate the basic report?" ] [list yes no] [list [_ "Yes"] [_ "No"]] question ""]
        if { $answ == "yes"} {
            Reports::GaugesReportBasicWin
        } elseif { $answ == "no"} {
            GidUtils::SetWarnLine [= "Extended report has not been generated"]
            return
        }
    }
    
    #     GidUtils::DisableGraphics
    #     GidUtils::DisableWarnLine
    #     GidUtils::WaitState .gid
    
    #Create Report
    GIDReport::Create
    
    #report header
    set title "Report of Iber. Gauges analysis"
    set GIDReport::_Titulo $title
    set GIDReport::_IncluirResumenProyecto 1
    #     
    set GaugesOnOff [GiD_AccessValue get gendata Gauges]
    if { $GaugesOnOff == On } {
        #         #Reading gauges XY
        #         set GaugeCoordinates [GiD_AccessValue get gendata Gauge_coordinates]
        #         set CoordinatesNum [lindex $GaugeCoordinates 1]
        #         set CoordinatesList [list]
        #         set NumGauges [expr {$CoordinatesNum/2}]
        #         
        #         set CoordinatesList ""
        #         for {set i 0} {$i< $CoordinatesNum} {incr i 2} {
            #             set X [lindex $GaugeCoordinates [expr {$i+2}]]
            #             set Y [lindex $GaugeCoordinates [expr {$i+3}]]
            #             lappend CoordinatesList "$X,$Y"
            #             
            #             set GaugeNum [expr {($i+2)/2}]
            #             set Xcentre($GaugeNum) $X
            #             set Ycentre($GaugeNum) $Y        
            #         }
    } else {
        MessageBoxOk [= "Error in Gauges definition" ] [= "Gauges result is 'Off'" ] error
        return
    }
    
    set all_anal [GiD_Info postprocess get all_analysis]
    if { ![info exists current_anal]  } {
        set current_anal [lindex $all_anal 0]
    }   
    foreach item $all_anal {
        if { $item == "Maps of Maximums" } {
            set current_anal $item
        }
    }
    if { $current_anal != "Maps of Maximums" } {
        MessageBoxOk [= "Error" ] [= "Before generating report, maximums of the hydraulic variables (depth, velocity and specific discharge) are needed. Enable it and recalculate" ] error
        #             tk_dialogRAM .gid.tmpwin error \
            #                 [= "Before writting results, maximus of the hydraulic variables (depth, velocity and specific discharge) are needed. Enable it and recalculate" ]\
            #                 error 0 OK
        return
    }
    
    set allsteps [GiD_Info postprocess get all_steps $current_anal]
    set maxstep [lindex $allsteps end]
    set results [GiD_Info postprocess get results_list contour_fill $current_anal [lindex $allsteps end]]
    
    foreach result $results {
        if { $result == "Depth (m)" || $result == "Velocity (m/s)" || $result == "Specific Discharge (m2/s)" } {
            
            set name ""
            set name $result
            set name [string map {(m/s) ""} $name]
            set name [string map {(m) ""} $name]
            set name [string map {(m2/s) ""} $name]
            set name [string map {_m/s ""} $name]
            set name [string map {_m2/s ""} $name]
            set name [string map {/ ""} $name]
            set name [string map {| ""} $name]
            
            #             set NumGauges [expr {$CoordinatesNum/2}]
            for {set i 0} {$i < $NumGauges} {incr i 1} {
                
                set GaugeID [expr {$i+1}]
                
                #Screenshots
                set filenameSS "Max $name at ($Xcentre($GaugeID),$Ycentre($GaugeID)).png"       
                set ReportsPriv($name,$i) [file join $dirreport $filenameSS]
                
                set control [file exists $ReportsPriv($name,$i)]
                if { $control == 0 } {
                    WarnWinText [= "File ID %s does not exists" $ReportsPriv($name,$i)]
                } else {
                    set GIDReport::_PieFoto $filenameSS
                    set caption $filenameSS
                    #add saved images to report
                    set nameid Gauge_${GaugeID}_${name}
                    set insertimghtml "<table border=0>\n"
                    append insertimghtml "<tr><td>"
                    append insertimghtml "<img src=\"$ReportsPriv($name,$i)\" border=0 align=\"center\" alt=\"$nameid\">"
                    append insertimghtml "</td></tr>\n"
                    append insertimghtml "<tr><td>"
                    append insertimghtml "<center>$caption</center>"
                    append insertimghtml "</td></tr>\n"
                    append insertimghtml "</table>"
                    
                    GIDReport::AnyadeElemento $nameid $insertimghtml
                    
                    GIDReport::ActualizaVistaPreviaInforme
                    
                }
            }
        }   
    }
    
    #     GidUtils::EnableGraphics
    #     GidUtils::EnableWarnLine
    #     GidUtils::EndWaitState .gid
    
}


proc Reports::CheckIfInside { } {
    #     variable conn
    #     variable nsides
    #     variable elemID
    variable CoordinatesList
    variable NumGauges
    variable Xcentre
    variable Ycentre
    
    set numelements [GiD_Info Mesh NumElements]   
    
    #Reading gauges XY - TOO SLOW
    set GaugeCoordinates [GiD_AccessValue get gendata Gauge_coordinates]
    set CoordinatesNum [lindex $GaugeCoordinates 1]
    set CoordinatesList [list]
    #     set aa [llength $CoordinatesList]
    
    set NumGauges 0
    set GaugeNum 0
    
    set itime [clock seconds]
    
    for { set k 0 } { $k < $CoordinatesNum } { incr k 2 } {
        set x [lindex $GaugeCoordinates [expr {$k+2}]]
        set y [lindex $GaugeCoordinates [expr {$k+3}]]
        
        lappend CoordinatesList "$x,$y"
        
        incr GaugeNum
        set Xcentre($GaugeNum) $x
        set Ycentre($GaugeNum) $y
        set NumGauges [expr {$NumGauges + 1}]
        
    }
    
    #     for {set i 1} {$i <= $numelements} {incr i 1} {
        #         set elemID -9999
        #         set conn($i) [GiD_Mesh get element $i connectivities]
        #         set nsides($i) [llength $conn($i)]
        #         
        #         if { $nsides($i) == 3 } {
            #             set ntimes 1            
            #         } elseif { $nsides($i) == 4 } {
            #             set ntimes 2
            #         }
        #         
        #         for { set j 1 } { $j <= $ntimes } { incr j 1 } {
            #             if { $j == 1 } {
                #                 set point1 [lindex $conn($i) 0]
                #                 set point2 [lindex $conn($i) 1]
                #                 set point3 [lindex $conn($i) 2]
                #             } elseif { $j == 2 } {
                #                 set point1 [lindex $conn($i) 2]
                #                 set point2 [lindex $conn($i) 3]
                #                 set point3 [lindex $conn($i) 0]
                #             }
            #             
            #             set a1 [lindex [lindex [GiD_Info Coordinates $point1] 0] 0]
            #             set a2 [lindex [lindex [GiD_Info Coordinates $point1] 0] 1]
            #             set b1 [lindex [lindex [GiD_Info Coordinates $point2] 0] 0]
            #             set b2 [lindex [lindex [GiD_Info Coordinates $point2] 0] 1]
            #             set c1 [lindex [lindex [GiD_Info Coordinates $point3] 0] 0]
            #             set c2 [lindex [lindex [GiD_Info Coordinates $point3] 0] 1]
            #             
            #             for {set k 0} {$k < $CoordinatesNum} {incr k 2} {
                #                 set x [lindex $GaugeCoordinates [expr {$k+2}]]
                #                 set y [lindex $GaugeCoordinates [expr {$k+3}]]
                #                 
                #                 #inn1
                #                 set cp1 [expr {($c1-$b1)*($y-$b2)-($c2-$b2)*($x-$b1)}]
                #                 set cp2 [expr {($c1-$b1)*($a2-$b2)-($c2-$b2)*($a1-$b1)}]
                #                 set prod [expr {($cp1*$cp2)}]
                #                 set inn1 0
                #                 if { $prod > 0 } {
                    #                     set inn1 1
                    #                 }
                #                 #inn2
                #                 set cp1 [expr {($c1-$a1)*($y-$a2)-($c2-$a2)*($x-$a1)}]
                #                 set cp2 [expr {($c1-$a1)*($b2-$a2)-($c2-$a2)*($b1-$a1)}]
                #                 set prod [expr {($cp1*$cp2)}]
                #                 set inn2 0
                #                 if { $prod > 0 } {
                    #                     set inn2 1
                    #                 }
                #                 #inn3
                #                 set cp1 [expr {($b1-$a1)*($y-$a2)-($b2-$a2)*($x-$a1)}]
                #                 set cp2 [expr {($b1-$a1)*($c2-$a2)-($b2-$a2)*($c1-$a1)}]
                #                 set prod [expr {($cp1*$cp2)}]
                #                 set inn3 0
                #                 if { $prod > 0 } {
                    #                     set inn3 1
                    #                 }
                #                 
                #                 if { $inn1 == 1 && $inn2 == 1 && $inn3 == 1 } {
                    #                     #Point is inside the element
                    #                     set checkresults0 [GiD_Info list_entities Elements $i]
                    #                     set checkresults1 [lindex $checkresults0 end]
                    #                     set checkresults2 [lindex $checkresults1 end]
                    #                     set checkresults3 [lindex $checkresults2 end]
                    #                     set checkresults4 [lindex $checkresults3 end]
                    #                     if { $checkresults4 == "n/a"} {
                        #                         set elemID 0  
                        #                         GidUtils::SetWarnLine [= "Point (%s,%s), which belongs to element %s, has no results. Point omitted." $x $y $i]
                        #                     } else {
                        #                         set elemID $i
                        #                         lappend CoordinatesList "$x,$y"
                        #                         
                        #                         incr GaugeNum
                        #                         set Xcentre($GaugeNum) $x
                        #                         set Ycentre($GaugeNum) $y
                        #                         incr NumGauges
                        #                     }
                    #                 }
                #             }
            #         }
        #         
        #     }
    
    
    set etime [clock seconds]
    set elapsedtime [expr {$etime-$itime}]
    GidUtils::SetWarnLine [= "Elapsed time: %s seconds" $elapsedtime]
    
    #     unset conn
    #     unset nsides
    
    return NumGauges CoordinatesList Xcentre Ycentre
    
}