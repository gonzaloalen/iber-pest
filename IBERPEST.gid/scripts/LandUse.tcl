# escolano: this file will replace (when GiD 14.1.8d is available) Prop.tcl and GridAssign.exe and its FORTRAN sources
#
#to assign to mesh elements the land use (GiD material with Manning coefficient value) from a pair or .asc .csv files:
#.asc (ARC/INFO ASCII) grid of integer key codes
# .csv (comma separated text) associating key with a name (the one used for material)
proc Iber::AssignLandUseWin { } {
    if { ![GiD_Info Mesh NumElements] } {
        WarnWin [_ "There are no mesh elements to assign them land use"]
        return 1
    }
    if { [GidUtils::VersionCmp 14.1.8d] < 0 } {
        #Iber::AssignLandUseDo require GiD 14.1.8d or higher (because use of GiD_Raster interpolate -closest)
        #until this version is available for Iber developers and next Iber packed use the old deprecated PropWin function
        return [PropWin]
    }
    
    set filename_raster [Browser-ramR file read .gid [= "Read ASCII RASTER file to assign land use"] \
            {} {{{} {.tif}} {{} {.txt}} {{} {.asc}} {{All files} {.*}}} ]
    if {$filename_raster != "" } {        
        GidUtils::WaitState
        set tstart [clock milliseconds]
        set show_advance_bar 1
        set fail [Iber::AssignLandUseDo $filename_raster $show_advance_bar]
        set tend [clock milliseconds]
        GidUtils::EndWaitState
        GidUtils::SetWarnLine [= "Land uses assigned to elements. time %s seconds" [expr ($tend-$tstart)/1000.0]]
        #ask the user if want to draw it
        set answer [MessageBoxOptionsButtons [= "Land use assigned"] [= "Land uses was succesfully assigned to mesh elements. Do you want to draw it?"] [list yes no] [list [_ "Yes"] [_ "No"]] question ""]
        if {$answer == "yes"} {
            #ensure mesh view mode
            MeshView 0
            GiD_Process Mescape Data Materials DrawMaterial -DrawAll-
        }
    }
}

#auxiliary proc to get an objarray of xys to be used after by GiD_Raster interpolate
proc Iber::GetElementCenters { element_ids } {    
    set num_elements [objarray length $element_ids]
    set xy_element_centers [objarray new doublearray [expr $num_elements*2]]
    set cont -1
    for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
        set element_id [objarray get $element_ids $i_element]
        lassign [GiD_Mesh get element $element_id center] x y z
        objarray set $xy_element_centers [incr cont] $x
        objarray set $xy_element_centers [incr cont] $y
    }
    return $xy_element_centers
}

#procedure that do the operation must be pure tcl, without tk GUI
proc Iber::AssignLandUseDo { filename_raster {show_advance_bar 1} } {
    set fail 0
    set materials [GiD_Info materials]
    set filename_csv [file rootname $filename_raster].csv
    set data [GidUtils::ReadFile $filename_csv]
    foreach line [split $data \n] {
        lassign [split $line {;}] key value
        if { [string is integer $key] } {
            set class_name($key) $value
            if { [lsearch $materials $value] != -1 } {
                #verify that the material exists
                set material_name($key) $value
            } else {
                W [= "Material '%s' doesn't exists. Verify file '%s'" $value $filename_csv]
            }
        } else {
            #e.g. the header line
            #id;class_Names
        }
    }
    set nodata_value [GDAL::GetNoDataValue $filename_raster]
    #if { [info exists class_name($nodata_value)] } {
        #    W [= "Warning: class %s defined as %s, but won't be assigned because is declared as 'nodata_value'" $nodata_value $class_name($nodata_value)]
        #}
    set raster_interpolation [GDAL::ReadRaster $filename_raster $show_advance_bar]
    set element_ids [Iber::GetElementIdsLayersNoFrozen]
    set num_elements [objarray length $element_ids]
    set xy_element_centers [Iber::GetElementCenters $element_ids]
    #-closest because values in this case are discrete integer codes, not a continous field!!
    set interpolated_values [GiD_Raster interpolate -closest $raster_interpolation [list nodes $xy_element_centers]]
    for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
        set element_id [objarray get $element_ids $i_element]
        set double_value [objarray get $interpolated_values $i_element]
        set key [expr int($double_value)]
        #         if { $key!=$nodata_value } {
        #             if { [info exists material_name($key)] } {
        #                 GiD_AssignData material $material_name($key) elements $element_id
        #             } else {
        #                 set undefined_class_name($key) 1
        #             }
        #         }         
        if { [info exists material_name($key)] } {
            GiD_AssignData material $material_name($key) elements $element_id
        } else {
            set undefined_class_name($key) 1
        }                        
    }
    set undefined_keys [lsort -integer [array names undefined_class_name]]
    if { [llength $undefined_keys] } {
        W [concat [= "Undefined uses" ]: $undefined_keys]  
    }
    return $fail 
}