# Marcos 12/2021 - Identical to LossesModel.tcl
# To import inicial conditions from ASCII raster file
# And apply in to each corresponding CND (Depth or Elevation)

namespace eval InitialCND {
    variable data ;#array of values
}


proc InitialCND::OnChangeSelectedFile { varname {tail 0}} {
    variable data
    
    if { [GiD_Info Mesh] == 0 } {
        tk_dialogRAM .gid.tmpwin [_ "Set Initial Conditions info"] \
            [_ "There is no mesh. Please generate a mesh before editing it."] \
            info 0 [_ "OK"]
        return
    }
    
    set filenames_and_questions ""
    if { $data == 0 } {
        set cnd_by "Depth"
    } else {
        set cnd_by "Elevation"
    }
    set questions [list $cnd_by Vx Vy]
    set loadedfiles 0
    foreach question $questions {
        set current_value [Browser-ramR file read .gid [= "Read ASCII RASTER file to assign the initial conditions by %s" $question] \
                {} {{{} {.txt}} {{} {.asc}} {{All files} {.*}}} ]
        if { $current_value != "" } {
            if { $question == "Depth" || $question == "Elevation" } {
                set question Value
            }
            lappend filenames_and_questions $current_value $question
            lappend filenameslist [file tail $current_value]
            
#             if { $current_value != "" } {
#                 $entry delete 0 end
#                 $entry insert end $filenameslist
#             }
            #set variable after change entry, else if variable is the own entry variable then delete 0 end will empty both
#             set ::$varname $filenameslist
            set show_advance_bar 1
            set tstart [clock milliseconds]
            InitialCND::AssignInitialDo IC_manual_assignation $filenames_and_questions $show_advance_bar $cnd_by
            set tend [clock milliseconds]
            GidUtils::SetWarnLine [= "Initial condition by %s assigned to elements in %s seconds" $question [expr ($tend-$tstart)/1000.0]]
            incr loadedfiles
        } else {
            GidUtils::SetWarnLine [= "Initial condition by %s omitted" $question]
#             if { $question == "Vy" } {
#                 return 1
#             } 
        }
    }
    
    if { $loadedfiles != 0 } {
        set question_labels [list {Value_[m]} {Vx_[m/s]} {Vy_[m/s]}]
        set answer [MessageBoxOptionsButtons [= "Initial Conditions 2D assigned"] [= "Initial Conditions 2D were succesfully assigned to mesh elements. Do you want to draw it?"] [list yes no] [list [_ "Yes"] [_ "No"]] question ""]
        if {$answer == "yes"} {
            #ensure mesh view mode
            MeshView 0
            foreach item $question_labels {
                GiD_Process MEscape Data Conditions DrawCond -ByColor- IC_manual_assignation $item
                GidUtils::SetWarnLine [= "Initial condition by %s drawn" $item]
                set xsecs 2
                after [expr {1000*$xsecs}]
                set response [MessageBoxOptionsButtons [= "Draw Initial Conditions 2D"] [= "Press 'Continue' to draw next Initial Condition 2D"] [list continue cancel] [list [_ "Continue"] [_ "Cancel"]] question ""]
                if { $response == "cancel" } {
                    after 1000 GiD_Process escape escape escape
                    break
                }
            }
        }
        return $filenameslist
    } else {
        GidUtils::SetWarnLine [= "No new initial conditions were assigned to the mesh"]
    }
}


proc InitialCND::TkwidgetGetFilenameButton { event args } {
    global tkwidgedprivfilenamebutton
    variable data
    
    switch $event {
        INIT {
            lassign $args PARENT current_row_variable GDN STRUCT QUESTION
            upvar $current_row_variable ROW
            #initialize variable to current field value
            set tkwidgedprivfilenamebutton($QUESTION,filename) [DWLocalGetValue $GDN $STRUCT $QUESTION]
            #set entry $PARENT.e$ROW
            set entry ""
            foreach item [grid slaves $PARENT -row [expr $ROW-1]] {
                if { [winfo class $item] == "Entry"  || [winfo class $item] == "TEntry" } {
                    #assumed that it is the only entry of this row
                    set entry $item
                    break
                }
            }
            #trick to fill in the values pressing transfer from an applied condition
            if { [lindex [info level 2] 0] == "DWUpdateConds" } {
                set values [lrange [lindex [info level 2] 2] 3 end]
                set index_field [LabelField $GDN $STRUCT $QUESTION]
                set value [lindex $values $index_field-1]
                set tkwidgedprivfilenamebutton($QUESTION,filename) $value
            }
            
            set CNDBY "Water_by: "
            
            set f [ttk::frame $PARENT.cfilenamebutton$CNDBY] ;#use a name depending on $QUESTION to allow more than one row changed
            set InitialCND::data 0
#             ttk::label $f.qtext -text $CNDBY
            ttk::radiobutton $f.qvalue1 -text [= "Depth"] -variable InitialCND::data -value 0
            ttk::radiobutton $f.qvalue2 -text [= "Elevation"] -variable InitialCND::data -value 1
            grid $f.qvalue1 $f.qvalue2 -sticky ew
#             grid $f.qtext $f.qvalue1 $f.qvalue2 -sticky ew
            grid columnconfigure $f {0} -weight 1
            grid $f -row [expr $ROW-2] -column 1 -sticky ew
            
            
            set w [ttk::frame $PARENT.cfilenamebutton$QUESTION]
#             ttk::entry $w.e1 -textvariable tkwidgedprivfilenamebutton($QUESTION,filename)
#             ttk::button $w.b1 \
#                 -command [list InitialCND::OnChangeSelectedFile tkwidgedprivfilenamebutton($QUESTION,filename) $w.e1 0]
            ttk::button $w.b1 -text [_ "Select"] -command [list InitialCND::OnChangeSelectedFile tkwidgedprivfilenamebutton($QUESTION,filename) 0]
            grid $w.b1 -sticky ew
            grid columnconfigure $w {0} -weight 1
            grid $w -row [expr $ROW-1] -column 1 -sticky ew
            
            
#             set w [ttk::frame $PARENT.cfilenamebutton$QUESTION]
#             ttk::entry $w.e1 -textvariable tkwidgedprivfilenamebutton($QUESTION,filename)
#             ttk::button $w.b1 -image [gid_themes::GetImage "folder.png"] \
#                 -command [list InitialCND::OnChangeSelectedFile tkwidgedprivfilenamebutton($QUESTION,filename) $w.e1 0]
#             set tkwidgedprivfilenamebutton($QUESTION,widget) $w
#             grid $w.e1 $w.b1 -sticky ew
#             grid columnconfigure $w {0} -weight 1
#             grid $w -row [expr $ROW-1] -column 1 -sticky ew
            if { $entry != "" } {
                grid remove $entry
            } else {
                #assumed that entry is hidden and then hide the usurpating frame
                grid remove $w
                grid remove $f
            }
        }
                SYNC {
            lassign $args GDN STRUCT QUESTION
            if { [info exists tkwidgedprivfilenamebutton($QUESTION,filename)] } {
                DWLocalSetValue $GDN $STRUCT $QUESTION $tkwidgedprivfilenamebutton($QUESTION,filename)
            }
        }
        DEPEND {
            lassign $args GDN STRUCT QUESTION ACTION VALUE
            if { [info exists tkwidgedprivfilenamebutton($QUESTION,widget)] && [winfo exists $tkwidgedprivfilenamebutton($QUESTION,widget)] } {
                if { $ACTION == "HIDE" } {
                    grid remove $tkwidgedprivfilenamebutton($QUESTION,widget)
                } else {
                    #RESTORE
                    grid $tkwidgedprivfilenamebutton($QUESTION,widget)
                }
            } else {

            }
        }
        CLOSE {
            array unset tkwidgedprivfilenamebutton
        }
        default {
            return [list ERROR [_ "Unexpected tkwidget event"]]
        }
    }
    #a tkwidget procedure must return "" if Ok or [list ERROR $description] or [list WARNING $description]
    return ""
}


proc InitialCND::AssignInitialDo { model_value filenames_and_questions {show_advance_bar 1} condition_by } {
    variable percent 0
    variable stop 0
    variable data
    set fail 0
    
    #assign percent of four steps to 10 10 40 40
    
    set condition_name $model_value
    set default_values [GidUtils::GetConditionDefaultValues $condition_name] ;#default values
    #set default_values [lindex $default_values 1]
    GiD_AccessValue set condition $condition_name Condition_by $condition_by ;#$condition_by is "Depth" or "Elevation"
                                                                                                                                                                                                                 
    if { $show_advance_bar } {
        GidUtils::CreateAdvanceBar [_ "Assign Initial condition values"] [_ "Percentage"]: ::InitialCND::percent 100  {set ::Iber::stop 1}
    }
    set percent 0    
    set nodata_value -9999 ;#assumed same value for all files
    set element_ids [Iber::GetElementIdsLayersNoFrozen]
    set num_elements [objarray length $element_ids]
    set xy_element_centers [Iber::GetElementCenters $element_ids]    
    set percent 10
    #initialize default values (except fixed model) for all elements
    for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
        set element_id [objarray get $element_ids $i_element]
        set element_values($element_id) $default_values
    }
    set n_raster [expr [llength $filenames_and_questions]/2]
    set n_refresh 1
    #fill a field by each raster
    set i_raster 0
    foreach {filename_raster condition_question} $filenames_and_questions {
        if { $show_advance_bar } {
            if { ![expr {$i_raster%$n_refresh}] } {
                set percent [expr int(double($i_raster)/$n_raster*100*0.4+20)]
            }
        }
        if { $stop } {
            set fail -1
            break
        }  
        set condition_question_index [GidUtils::GetConditionQuestionIndex $condition_name $condition_question]
        set values $default_values
        set raster_interpolation [GDAL::ReadRaster $filename_raster 0]
        set nodata_value [GDAL::GetNoDataValue $filename_raster]
        set nodata_value [list $condition_by $nodata_value]
        #set interpolatd_values [GiD_Raster interpolate $raster_interpolation [list nodes $xy_element_centers]]
        #-closest in this case also, because GiD_AssignData become a bottleneck in case of a each element a different value (and big strings), better use only a few discrete values
        set interpolated_values [GiD_Raster interpolate -closest $raster_interpolation [list nodes $xy_element_centers]]        
        for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
            set element_id [objarray get $element_ids $i_element]
            set value [objarray get $interpolated_values $i_element]
            lset element_values($element_id) $condition_question_index $value
        }
        incr i_raster
    }    
    #assign the final values
    #GiD_AssignData condition of a condition canrepeat=no an big mesh is a huge bottleneck 
    #(because it is moving every call the whole container to a temporary container sorted by it)
    #in future versions (>14.1.8d) we want to enhance the code storing entityvalue ordered by entity id and avoid the bottleneck
    #
    #to minimize GiD_AssignData calls join in a single assign the elements with same values
    for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
        set element_id [objarray get $element_ids $i_element]
        set values $element_values($element_id)
        lappend elements_same_value($values) $element_id
    }
    unset element_ids
    array unset element_values
    set list_values [array names elements_same_value]
    set num_different_values [llength $list_values]
    set n_refresh_different_values [expr int($num_different_values/4)]
    if { $n_refresh_different_values<1 } {
        set n_refresh_different_values 1
    }
    set i_different_values 0
    foreach values $list_values {
        if { [lindex $values 1] != [lindex $nodata_value 1] } {
            if { $show_advance_bar } {
                if { ![expr {$i_different_values%$n_refresh_different_values}] } {
                    set percent [expr int(double($i_different_values)/$num_different_values*100*0.4+60)]
                }
            }
            GiD_AssignData condition $condition_name body_elements $values $elements_same_value($values)
            incr i_different_values
        }
    }
    if { $show_advance_bar } {
        GidUtils::DeleteAdvanceBar
    }
    return $fail
}