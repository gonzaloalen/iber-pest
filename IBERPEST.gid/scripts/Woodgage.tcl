
namespace eval Woodgage {
    variable data ;#array of values
    variable woodgages ;#list of names of data
    variable current_woodgage ;#name of current selected woodgage
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {visible start end }
    variable fields_defaults {1 {} {} }   
    variable drawopengl
}

proc Woodgage::UnsetVariables { } {
    variable data
    variable woodgages      
    unset -nocomplain data
    unset -nocomplain woodgages
}

proc Woodgage::SetDefaultValues { } {    
    variable data
    variable woodgages
    variable current_woodgage
    variable current_value
    variable fields
    variable fields_defaults
    array unset data
    set woodgages {}
    foreach field $fields  {
        set current_value($field) ""
    }
    set current_woodgage {}
}

#store the woodgages information in a hiddend field of the problem data
#then this data is saved with the model without do nothing more
proc Woodgage::FillTclDataFromProblemData { } {
    variable data
    variable woodgages
    
    if { [catch {set x [GiD_AccessValue get gendata WoodgagesData]} msg] } {
        #reading a model created by an old version of the problemtype this gendata doesn't exists
        return
    }
    array set data $x
    
    set woodgages [list]
    foreach item [array names data *,start] {
        lappend woodgages [string range $item 0 end-6]
    }
    set woodgages [lsort -dictionary $woodgages]
    Woodgage::SetCurrentWoodgage [lindex $woodgages 0]
}

proc Woodgage::FillProblemDataFromTclData { } {
    variable data
    variable woodgages
    set x [array get data]
    GiD_AccessValue set gendata WoodgagesData $x
}

proc Woodgage::WriteCalculationFile { } {
    variable data
    set res ""       
    
    set i 0
    foreach woodgage [Woodgage::GetWoodgages] {
        incr i        
        set item [list]
        lappend item $i
        lappend item {*}[lrange $data($woodgage,start) 0 1]
        lappend item {*}[lrange $data($woodgage,end) 0 1]
        #lappend item [lindex $data($woodgage,start) 2]
        #lappend item [lindex $data($woodgage,end) 2]
        lappend item $woodgage                       
        append res [join $item ,]\n        
    }
    
    return $res
}

proc Woodgage::GetWoodgages { } {
    variable woodgages
    if { ![info exists woodgages] } {
        return ""
    }
    return $woodgages
}

proc Woodgage::GetCurrentWoodgage { } {
    variable current_woodgage
    return $current_woodgage
}

proc Woodgage::OnChangeSelectedWoodgage { cb } {   
    Woodgage::SetCurrentWoodgage [Woodgage::GetCurrentWoodgage]
}

proc Woodgage::SetCurrentWoodgage { woodgage } {
    variable data
    variable current_woodgage
    variable current_value
    variable fields   
    #fill in the current_value variables with data
    if { $woodgage != "" } {
        foreach field $fields {
            if { ![info exists data($woodgage,$field)] } {
                set data($woodgage,$field) 0
            }
            set current_value($field) $data($woodgage,$field)           
        }
    } else {
        foreach field $fields {
            set current_value($field) ""
        }
    }
    set current_woodgage $woodgage  
}

proc Woodgage::GetIndex { woodgage } {
    variable woodgages    
    return [lsearch $woodgages $woodgage]
}

proc Woodgage::Exists { woodgage } {    
    if { [Woodgage::GetIndex $woodgage] != -1 } {
        set exists 1
    } else {
        set exists 0
    }
    return $exists
}

proc Woodgage::GetWoodgageAutomaticName { } {
    set basename [_ "woodgage"]
    set i 1
    set woodgage $basename-$i
    while { [Woodgage::Exists $woodgage] } {        
        incr i
        set woodgage $basename-$i        
    }
    return $woodgage
}

proc Woodgage::NewWoodgage { cb } {
    variable woodgages
    variable data
    variable fields 
    variable fields_defaults
    set woodgage [Woodgage::GetWoodgageAutomaticName]     
    
    if { [Woodgage::Exists $woodgage] } {
        #already exists
        return 1
    }
    foreach field $fields value $fields_defaults {
        set data($woodgage,$field) $value
    }
    lappend woodgages $woodgage   
    if { [winfo exists $cb] } {
        $cb configure -values [Woodgage::GetWoodgages]
    }
    Woodgage::SetCurrentWoodgage $woodgage
    return 0
}

proc Woodgage::DeleteWoodgage { cb } {
    variable woodgages
    variable data
    variable fields 
    variable fields_defaults
    set woodgage [Woodgage::GetCurrentWoodgage] 
    if { ![Woodgage::Exists $woodgage] } {
        #not exists
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete woodgage '%s'" $woodgage] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {     
        set i [Woodgage::GetIndex $woodgage] 
        array unset data $woodgage,*        
        set woodgages [lreplace $woodgages $i $i]       
        if { [winfo exists $cb] } {
            $cb configure -values [Woodgage::GetWoodgages]
        }
        Woodgage::SetCurrentWoodgage [lindex $woodgages 0]  
        GiD_Redraw     
    }
    return 0
}

proc Woodgage::RenameWoodgage { cb } {
    variable data
    variable woodgages
    variable fields
    set woodgage [Woodgage::GetCurrentWoodgage]   
    if { ![Woodgage::Exists $woodgage] } {
        #not exists
        return 1
    } 
    set new_name [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $woodgage] \
            [= "Enter new name of %s '%s'" [= "woodgage"] $woodgage] gidquestionhead "any" ""]
    if { $new_name != "--CANCEL--" } {
        foreach field $fields {
            set data($new_name,$field) $data($woodgage,$field)
        }
        array unset data $woodgage,*
        set i [Woodgage::GetIndex $woodgage] 
        lset woodgages $i $new_name
        if { [winfo exists $cb] } {
            $cb configure -values [Woodgage::GetWoodgages]           
        }
        Woodgage::SetCurrentWoodgage $new_name       
        GiD_Redraw 
    }
    return 0
}

#apply the values of the current window
proc Woodgage::Apply { } {
    variable data
    variable current_value
    variable fields
    
    set woodgage [Woodgage::GetCurrentWoodgage]    
    foreach field $fields {
        set data($woodgage,$field) $current_value($field)
    }
    
    GiD_Redraw        
}

#not Woodgage::StartDraw and Woodgage::EndDraw are not used, the Draw button is hidden (because unregister fail in GiD 11.1.5d)
#and the redraw procedure is registered when loading the problemtype, and each woodgage is draw depending on its 'visible' variable value
proc Woodgage::StartDraw { bdraw} {
    variable drawopengl
    set drawopengl [GiD_OpenGL register Woodgage::ReDraw]
    GiD_Redraw
    $bdraw configure -text [= "Finish draw"] -command [list Woodgage::EndDraw $bdraw]
}

proc Woodgage::EndDraw { bdraw } {
    variable drawopengl
    if { [info exists drawopengl] } { 
        #unregister the callback tcl procedure to be called by GiD when redrawing
        GiD_OpenGL unregister $drawopengl
        catch {unset drawopengl}
        GiD_Redraw
        if { $bdraw != "" } {
            $bdraw configure -text [= "Draw"] -command [list Woodgage::StartDraw $bdraw]
        }
    }
}

proc Woodgage::ReDraw { } {
    foreach woodgage [Woodgage::GetWoodgages] {
        Woodgage::Draw $woodgage
    }
}

proc Woodgage::Draw { woodgage } {
    variable data
    if { $woodgage == "" || ![info exists data($woodgage,visible)] || !$data($woodgage,visible) } {
        return 1
    }
    foreach field {start end} {
        if { [llength $data($woodgage,$field)] != 3 } {
            return 1
        }
        foreach v $data($woodgage,$field) {
            if { ![string is double $v] } {
                return 1
            }
        }
    }    
    set blue {0 0 1}
    GiD_OpenGL draw -color $blue
    GiD_OpenGL draw -pointsize 3
    GiD_OpenGL draw -begin points
    GiD_OpenGL draw -vertex $data($woodgage,start)
    GiD_OpenGL draw -vertex $data($woodgage,end)
    GiD_OpenGL draw -end
    GiD_OpenGL draw -begin lines
    GiD_OpenGL draw -vertex $data($woodgage,start)
    GiD_OpenGL draw -vertex $data($woodgage,end)
    GiD_OpenGL draw -end
    #show label
    set p [MathUtils::VectorSum $data($woodgage,start) $data($woodgage,end)]
    set p [MathUtils::ScalarByVectorProd 0.5 $p]
    GiD_OpenGL draw -rasterpos $p
    GiD_OpenGL drawtext $woodgage
    return 0
}

proc Woodgage::DestroyWoodgageWindow { W w } {   
    if { $W == $w } {
        Woodgage::EndDraw ""
        Woodgage::FillProblemDataFromTclData             
    }
}

proc Woodgage::OnChangeType { f } {
    variable current_value
    if { $current_value(type) == 1 } {
        #Rectangular
        $f.lwidth configure -text [= "Width (m)"]        
        grid $f.lheight $f.eheight
    } else {
        #Circular
        $f.lwidth configure -text [= "Diameter (m)"]
        grid remove $f.lheight $f.eheight
    }
}

proc Woodgage::PickPointInSurfaceOrElementCmd { entry } {
    set view_mode [GiD_Info Project ViewMode]
    if { $view_mode == "GEOMETRYUSE" } {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an surface"] PointInSurface GEOMETRYUSE]
        #set surface_id $::GidPriv(selection)  
    } elseif { $view_mode == "MESHUSE"} {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an element"] PointInElement MESHUSE]
        #set element_id $::GidPriv(selection)    
    } else {
        set xyz ""
    }        
    if { $xyz != "" } {                    
        set res [format "%g %g %g" {*}$xyz]
        $entry delete 0 end
        $entry insert end $res        
    }
}

proc Woodgage::Window { } {
    variable data
    variable woodgages
    variable current_woodgage
    variable current_value
    
    if { ![info exists woodgages] } {
        Woodgage::SetDefaultValues
    }
    
    Woodgage::FillTclDataFromProblemData    
    
    set w .gid.woodgage
    InitWindow $w [= "Woodgage"] PreWoodgageWindowGeom Woodgage::Window
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    set f [ttk::frame $w.fwoodgages]
    
    ttk::combobox $f.cb1 -textvariable Woodgage::current_woodgage -values [Woodgage::GetWoodgages] -state readonly
    bind $f.cb1 <<ComboboxSelected>> [list Woodgage::OnChangeSelectedWoodgage %W]
    ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list Woodgage::NewWoodgage $f.cb1]
    GidHelp $f.bnew  [= "Create a new woodgage"]
    ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list Woodgage::DeleteWoodgage $f.cb1]
    GidHelp $f.bdel  [= "Delete a woodgage"]
    ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list Woodgage::RenameWoodgage $f.cb1]
    GidHelp $w.f.bren  [= "Rename a woodgage"]   
    grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
    grid $f -sticky new
    grid rowconfigure $f 0 -weight 1
    
    set f [ttk::frame $w.fwoodgage]        
    set field visible
    ttk::checkbutton $f.ch$field -text [= "Visible"] -variable Woodgage::current_value($field)
    grid $f.ch$field -sticky w
    
    foreach field {start end} text [list [= "Start"] [= "End"]] {
        ttk::label $f.l$field -text $text
        ttk::entry $f.e$field -textvariable Woodgage::current_value($field)
        ttk::button $f.b$field -image [gid_themes::GetImage "point.png" small_icons] -command [list Woodgage::PickPointInSurfaceOrElementCmd $f.e$field]
        grid $f.l$field $f.e$field $f.b$field -sticky ew
    }
    
    grid $f -sticky new
    grid columnconfigure $f {1} -weight 1
    
    
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.bapply -text [= "Apply"] -command [list Woodgage::Apply] -style BottomFrame.TButton
    #ttk::button $f.bdraw -text [= "Draw"] -command [list Woodgage::StartDraw $f.bdraw] -style BottomFrame.TButton
    ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    #grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
    grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }        
    
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    bind $w <Alt-c> "$f.bclose invoke"
    bind $w <Escape> "$f.bclose invoke"   
    bind $w <Destroy> [list +Woodgage::DestroyWoodgageWindow %W $w] ;# + to add to previous script  
}

proc Woodgage::CloseWindow { } {
    set w .gid.woodgage    
    if { [winfo exists $w] } {
        destroy $w
    }
}


