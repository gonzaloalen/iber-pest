###############################################
### version 2.0 of Iber Results2Raster tool ###
### created by Ernest/Marcos 201806 ###########
### updated by Marcos 202107 ##################
###############################################

namespace eval ::PostRaster {
    variable allzones { }
    for {set zone 1} {$zone <= 60 } {incr zone} {
        lappend allzones $zone
    }
    variable labelszone { }
    foreach item $allzones {
        lappend labelszone $item
    }
    variable allhemis { N S }
    variable labelshemis { N S }
    variable pi 3.14159265359
    variable radius_earth 6378137.0 ;#m (equatorial radius WGS-84)
    variable xyz        ;# array of values
}

proc ::PostRaster::SelectResults { results over selection selres rasteriter current_step } {
    global PostRaster
    global Project
    global dir
    global rastersdir
    global numres
    global cell
    global numel
    global numvert
    global numverts
    global maxnumverts
    global maxnumels
    global current_anal
    global Interpolation
    global Timeresults
    global areresults
    global Reproject
    package require objarray

    if { [info exists GidProcWin(w)] && [winfo exists $GidProcWin(w)]} {
        set w $GidProcWin(w)
    } else {
        set w .gid
    }

    set maxnumels [GiD_Info Mesh MaxNumElements]
    #set res_aux1 [expr $maxnumels-1]
    set maxnumvert [GiD_Info Mesh MaxNumNodes]
    #set res_aux2 [expr $maxnumvert-1]

    set filename1 "ResToRaster"
    append filename1 ".aux"
    set fitxer1 [file join $rastersdir $filename1]
    set aux [open $fitxer1 w]
    puts $aux $maxnumels
    puts $aux $maxnumvert
    puts $aux $Interpolation
    puts $aux $over
    puts $aux $cell
    puts $aux $current_step
    set projection $Reproject
    lappend projection $PostRaster(thiszone)
    if { $PostRaster(thishemis) == "N" } {
        set varhemis 1
    } elseif { $PostRaster(thishemis) == "S" } {
        set varhemis 2
    }
    lappend projection $varhemis
    puts $aux $projection

    close $aux

    set filename4 "elem_valor"
    append filename4 ".aux"
    set fitxer4 [file join $rastersdir $filename4]
    set fitxeraux [open $fitxer4 w]

    set numres [llength $areresults]
    puts $fitxeraux $numres

    for {set res 0} {$res < $numres} {incr res} {
        set name [lindex $areresults $res]
        set res_aux3 [expr $numres-1]
        set valors [lrange [GiD_Result get [list $name $current_anal $current_step]] 3 end]

        set num_comp [llength [string trim [lindex [lindex $valors 0] 1]]]
        set numval [llength $valors].

        set GidPriv(StopLoading) 0
        set res_up [expr $res+1]
        AdvanceBar 0 1 0 [= "($res_up/$numres) Reading results: $name " ] [= "Percentage"]:
        set dn [expr int($numval/20)]
        if { $dn < 1 } { set dn 1 }

        ### Changes characters to write the file ###
        # Delete IBER units #
        set name [string map {(m) ""} $name]
        set name [string map {(m/m) ""} $name]
        set name [string map {(º) ""} $name]
        set name [string map {(m/s) ""} $name]
        set name [string map {(m2/s) ""} $name]
        set name [string map {(s) ""} $name]
        set name [string map {(N/m2) ""} $name]
        set name [string map {(m2/s2) ""} $name]
        set name [string map {(m2/s3) ""} $name]
        set name [string map {(m2/s) ""} $name]
        set name [string map {(mm/h) ""} $name]
        set name [string map {(mm) ""} $name]
        set name [string map {(m/day) ""} $name]
        set name [string map {(0-1) ""} $name]
        set name [string map {(m3/m3) ""} $name]
        set name [string map {(rad) ""} $name]
        set name [string map {(cfu/100ml) ""} $name]
        set name [string map {(mg/l) ""} $name]
        set name [string map {(mgCO3Ca/l) ""} $name]
        set name [string map {(g/l) ""} $name]
        set name [string map {(ºC) ""} $name]
        set name [string map {(kg/m2) ""} $name]
        set name [string map {(kg/s/m) ""} $name]
        set name [string map {(%) ""} $name]
        set name [string map {(Pa) ""} $name]
        set name [string map {(m2) ""} $name]
        # Spaces - Separators #
        set name [string map {" " "_"} $name]
        set name [string map {// "_"} $name]
        set name [string map {, ""} $name]
        # Special characters not allowed by Windows #
        #set name [string map {\ ""} $name] #; not valid for TLC
        set name [string map {/ ""} $name]
        set name [string map {: ""} $name]
        set name [string map {* ""} $name]
        set name [string map {? ""} $name]
        #set name [string map {" ""} $name] #; not valid for TLC
        set name [string map {< ""} $name]
        set name [string map {> ""} $name]
        set name [string map {| ""} $name]


        set title $name
        set start "Begin results"
        puts $fitxeraux $start
        puts $fitxeraux $title
        puts $fitxeraux $num_comp
        for {set ent 0} {$ent <= $numval} {incr ent} {
            set numel [string trim [lindex [lindex $valors $ent] 0]]
            set valor_comp [string trim [lindex [lindex $valors $ent] 1]]
            set valor_malo [lindex $valor_comp 0]
            if { $valor_malo  != -3.4028234663852886e+38 } {
                set resu "$numel $valor_comp"
                puts $fitxeraux $resu

                if { ![expr $ent%$dn] } {
                    set percent [expr int(100.0*$ent/$numval)]
                    AdvanceBar $percent 100 $percent
                    if { $res == $numres } {
                        break
                        unset percent
                        unset dn
                    }
                }
            }
        }
    }
    close $fitxeraux
    AdvanceBar 100 100 100

    set GidPriv(StopLoading) 0
    AdvanceBar 0 1 0 [= "Creating raster files..." ]  [= "Percentage"]:
    set kgrid 0

    set prog [Iber::GetFullPathExe ResultsToRaster.exe]

    set pipe [open [list "|" $prog $rastersdir] r]
    while {[gets $pipe percent] != -1} {
        AdvanceBar $percent 100 $percent
        #if { $GidPriv(StopLoading) } "kills process"
    }
    close $pipe
    AdvanceBar 100 100 100

    file delete $fitxer1
    file delete $fitxer4

    if { $rasteriter == 1 } {
        MessageBoxOk [= "Raster Export Process"] [= "ASCII Rasters (*.asc) have been written in the model directory called 'Rasters'"] info
    } else {
        set maxstep [lindex [GiD_Info postprocess get all_steps $current_anal] end]
        if { $current_step != $maxstep } {
            GidUtils::SetWarnLine [= "ASCII Rasters (*.asc) for time %s s have been written" $current_step]
        } else {
            GidUtils::SetWarnLine [= "ASCII Rasters (*.asc) for time %s s have been written" $current_step]
            MessageBoxOk [= "Raster Export Process"] [= "ASCII Rasters (*.asc) have been written in the model directory called 'Rasters'"] info
        }
    }

    unset maxnumels
    unset maxnumvert
    unset filename1
    unset filename4
    unset fitxer1
    unset fitxer4
    unset fitxeraux
    unset valors
    unset num_comp
    unset numval
    unset title
    unset start
    unset numel
    unset valor_comp
    unset resu
    unset results
    unset selection

    return $res
}

proc ::PostRaster::CreateAuxiliaryFiles { fitxer2 fitxer3 } {
    global rastersdir
    global Reproject
    global PostRaster

    #Elements
    set maxnumels [GiD_Info Mesh MaxNumElements]
    set res_aux1 [expr $maxnumels-1]
    set elemsconn ""
    set elemsconn_q [GiD_Info Mesh Elements Quadrilateral -sublist]
    set elemsconn_t [GiD_Info Mesh Elements Triangle -sublist]

    set GidPriv(StopLoading) 0
    AdvanceBar 0 1 0 [= "Reading connectivities" ] [= "Percentage"]:
    set dn [expr int($res_aux1/20)]
    if { $dn < 1 } { set dn 1 }

    set filename2 "conn"
    append filename2 ".aux"
    set fitxer2 [file join $rastersdir $filename2]
    set fitxeraux [open $fitxer2 w]
    set num [llength $elemsconn]
    set elemsconn "$elemsconn_q $elemsconn_t"
    for {set elem 0} {$elem <= $res_aux1} {incr elem} {
        puts $fitxeraux [lindex $elemsconn $elem]
        if { ![expr $elem%$dn] } {
            set percent [expr int(100.0*$elem/$res_aux1)]
            AdvanceBar $percent 100 $percent
            if { $GidPriv(StopLoading) == 1 } {
                break
                unset percent
                unset dn
            }
        }
    }
    close $fitxeraux
    AdvanceBar 100 100 100

    #Vertexs
    set maxnumvert [GiD_Info Mesh MaxNumNodes]
    set res_aux2 [expr $maxnumvert-1]
    set vert_coor [GiD_Result get_nodes]

    set GidPriv(StopLoading) 0
    AdvanceBar 0 1 0 [= "Reading vertex and coordinates" ] [= "Percentage"]:
    set dn [expr int($res_aux2/20)]
    if { $dn < 1 } { set dn 1 }

    set filename3 "vert_coor"
    append filename3 ".aux"
    set fitxer3 [file join $rastersdir $filename3]
    set fitxeraux [open $fitxer3 w]
    set num [llength $vert_coor]
    for {set ent 0} {$ent <= $res_aux2} {incr ent} {
        set alfa 1 ;# auxiliary variable. It must be changed in window
        if { $Reproject == 0 } {
            puts $fitxeraux [lindex $vert_coor $ent]
        } elseif { $Reproject == 1 } {
            set easting [lindex [lindex $vert_coor $ent] 1]
            set northing [lindex [lindex $vert_coor $ent] 2]
            set zone $PostRaster(thiszone)
            set hemisfere $PostRaster(thishemis)
            set lonlat [::PostRaster::ConvertUTM2LonLat $easting $northing $zone $hemisfere]

            set values ""
            append values [expr {$ent+1}]
            append values " "
            append values $lonlat
            append values " "
            append values [lindex [lindex $vert_coor $ent] 3]

            puts $fitxeraux $values
        }

        if { ![expr $ent%$dn] } {
            set percent [expr int(100.0*$ent/$res_aux2)]
            AdvanceBar $percent 100 $percent
            if { $GidPriv(StopLoading) == 1 } {
                break
                unset percent
                unset dn
            }
        }
    }

    close $fitxeraux
    AdvanceBar 100 100 100

    #if { $Reproject == 1 } {
    #        ::PostRaster::GenerateXYZlonlatFile
    #}
}

proc ::PostRaster::GenerateXYZlonlatFile {  } {
    global rastersdir
    global PostRaster
    global cell

    #Reset variables
    set xlo_ant 90                ;# Degrees
    set xhi_ant 0                ;# Degrees
    set ylo_ant 90                ;# Degrees
    set yhi_ant 0                ;# Degrees
    set zlo_ant 9999        ;# Corresponding units
    set zhi_ant -9999        ;# Corresponding units

    set meshelems [GiD_Info Mesh MaxNumElements]
    #set xyz(in,elem) ""
    for {set elem 1} {$elem <= $meshelems} {incr elem} {
        #OJO aqui habria que poner un IF para verificar que el elemento existe. Tal como esta, los elementos van de 1 a meshelems
        #pero si hay huecos o siguen otra numeracion, hay problemas.
        #Creo que esto no hace falta porque GiD ya renumera antes de calcular, y ya no hay 'huecos' en la numeracion de la malla
        set x_let [lindex [GiD_Info listmassproperties Elements $elem] 9]
        set x [string trimleft $x_let center=]                                                                ;# For removing the first part of the string
        set y [lindex [GiD_Info listmassproperties Elements $elem] 10]
        set z [lindex [GiD_Info listmassproperties Elements $elem] 11]
        #set z_value ""        ;# Aqui pondremos el valor de la variable
        set zone $PostRaster(thiszone)
        set hemisfere $PostRaster(thishemis)
        set lonlat [::PostRaster::ConvertUTM2LonLat $x $y $zone $hemisfere]

        #Grid coordinates
        append xyz(in,$elem) [lindex $lonlat 0]
        lappend xyz(in,$elem) [lindex $lonlat 1]
        lappend xyz(in,$elem) $z

        # For LONGITUDE
        set xlo [lindex $lonlat 0]
        if { $xlo < $xlo_ant } {
            set xlo_ant $xlo
        } else {
            #Keep the previous value
        }
        set xhi [lindex $lonlat 0]
        if { $xhi > $xhi_ant } {
            set xhi_ant $xhi
        } else {
            #Keep the previous value
        }
        # For LATITUDE
        set ylo [lindex $lonlat 1]
        if { $ylo < $ylo_ant } {
            set ylo_ant $ylo
        } else {
            #Keep the previous value
        }
        set yhi [lindex $lonlat 1]
        if { $yhi > $yhi_ant } {
            set yhi_ant $yhi
        } else {
            #Keep the previous value
        }
        # For VALUE
        set zlo $z
        if { $zlo < $zlo_ant } {
            set zlo_ant $zlo
        } else {
            #Keep the previous value
        }
        set zhi $z
        if { $zhi > $zhi_ant } {
            set zhi_ant $zhi
        } else {
            #Keep the previous value
        }

    }

    #Header variables
    set xmin $xlo_ant
    set xmax $xhi_ant
    set ymin $ylo_ant
    set ymax $yhi_ant
    set zmin $zlo_ant
    set zmax $zhi_ant
    variable radius_earth
    variable pi

    #Writing GRD file
    set filenamegrd "test"
    append filenamegrd ".grd"
    set fitxergrd [open [file join $rastersdir $filenamegrd] w]
    #Start creating file
    puts $fitxergrd "DSAA"
    set distrad [expr {atan($cell/$radius_earth)}]
    set ncols [expr {int(($xmax-$xmin)*$pi/180/$distrad)}]
    set nrows [expr {int(($ymax-$ymin)*$pi/180/$distrad)}]
    set colrow [list $ncols $nrows]
    puts $fitxergrd $colrow
    set xs [list $xmin $xmax]
    puts $fitxergrd $xs
    set ys [list $ymin $ymax]
    puts $fitxergrd $ys
    set zs [list $zmin $zmax]
    puts $fitxergrd $zs

    #Creating 'void' grid values
    for {set row 0} {$row <= $nrows} {incr row} {
        set valor ""
        for {set col 0} {$col <= $ncols} {incr col} {
            set xlon [expr {$xmin+$distrad*$col*180/$pi}]
            set ylat [expr {$ymin+$distrad*$row*180/$pi}]
            append xyz($row,$col) $xlon
            lappend xyz($row,$col) $ylat
            lappend xyz($row,$col) 1.70141e38

            append valor $ylat
            append valor " "
        }
        puts $fitxergrd $valor
    }

    #Search closest xyz(in,elem) position to xyz(row,col)
    set dis_ant 10        ;# Esto son grados
    for {set elem 1} {$elem <= $meshelems} {incr elem} {
        for {set row 1} {$row <= $nrows} {incr row} {
            for {set col 1} {$col <= $ncols} {incr col} {
                #Creo que esto deberia ser con coordenadas UTM, pero lo hacemos con geograficas (lon,lat) en DEG
                #No seria la distancia en metros, mas bien en angulo, pero proporciona el mas cercano
                #set dist [expr {}]
            }
        }
    }

    close $fitxergrd
}

#proc based on gid_map::utm2ll from gid_map.tcl
proc ::PostRaster::ConvertUTM2LonLat { easting northing zone letter } {
    if { $zone == "" || $letter == "" } {
        W "gid_map::utm2ll. Must specify UTM zone and letter"
        return [list 0.0 0.0]
    }
    variable radius_earth
    variable pi
    set RAD_TO_DEGREE 57.2957795131
    set K0 0.9996
    # WGS-84
    set es2 0.00669438 ;# EccentricitySquared
    set es2x [expr {1.0-$es2}]
    set x [expr {$easting-500000.0}]
    set northernHemisphere [expr {$letter >= "N"}]
    set y [expr {$northing-($northernHemisphere? 0.0 : 10000000.0)}]
    set long_origin [expr {($zone-1.0)*6.0-180.0+3.0}] ;# +3 puts in middle
    set ep2 [expr {$es2/$es2x}]
    set e1 [expr {(1.0-sqrt($es2x))/(1.0+sqrt($es2x))}]
    set M [expr {$y/$K0}]
    set mu [expr {$M/($radius_earth*(1.0-$es2/4.0-3.0*$es2*$es2/64.0-5.0*$es2*$es2*$es2/256.0))}]
    set phi [expr {$mu+(3.0*$e1/2.0-27.0*$e1*$e1*$e1/32.0)*sin(2.0*$mu)+(21.0*$e1*$e1/16.0-55.0*$e1*$e1*$e1*$e1/32.0)*sin(4.0*$mu)+(151.0*$e1*$e1*$e1/96.0)*sin(6.0*$mu)}]
    set sinphi [expr {sin($phi)}]
    set cosphi [expr {cos($phi)}]
    set tanphi [expr {$sinphi/$cosphi}]
    set N1 [expr {$radius_earth/sqrt(1.0-$es2*$sinphi*$sinphi)}]
    set T1 [expr {$tanphi*$tanphi}]
    set C1 [expr {$ep2*$cosphi*$cosphi}]
    set R1 [expr {$radius_earth*$es2x/pow(1.0-$es2*$sinphi*$sinphi,1.5)}]
    set D [expr {$x/($N1*$K0)}]
    set latitude [expr {$phi-($N1*$tanphi/$R1)*($D*$D/2.0-(5.0+3.0*$T1+10.0*$C1-4.0*$C1*$C1-9.0*$ep2)*$D*$D*$D*$D/24.0+
        (61.0+90.0*$T1+298.0*$C1+45.0*$T1*$T1-252.0*$ep2-3.0*$C1*$C1)*$D*$D*$D*$D*$D*$D/720.0)}]
        set latitude [expr {$latitude*$RAD_TO_DEGREE}]
        set longitude [expr {($D-(1.0+2.0*$T1+$C1)*$D*$D*$D/6.0+(5.0-2.0*$C1+28.0*$T1-3.0*$C1*$C1+8.0*$ep2+24.0*$T1*$T1)*$D*$D*$D*$D*$D/120.0)/$cosphi}]
        set longitude [expr {$long_origin + $longitude * $RAD_TO_DEGREE}]
        return [list $longitude $latitude]
    }

    proc ::PostRaster::ApplyListEntities { w } {
    global PostRaster
    #global FileType
    global directory
    global dir
    global rastersdir
    global Project
    global numverts
    global maxnumels
    global maxnumverts
    global numres
    global numel
    global numvert
    global current_anal
    global current_step
    global areresults
    global Timeresults
    package require objarray

    set areresults ""
    set current_anal $PostRaster(thisanalysis)
    set steps [GiD_Info postprocess get all_steps $current_anal]
    set results_new [GiD_Info postprocess get results_list contour_fill $current_anal [lindex $steps end]]
    foreach item $results_new {
        regsub -all _ $item { } item
        if { $PostRaster(output,$item) } {
            lappend areresults $item
        }
    }
    if { $areresults == "" } {
        MessageBoxOk [_ "Warning"] [= "No results have been selected."] warning
        return
    }

    set selres $areresults

    # working folders
    set Project [GiD_Info project ModelName]
    if { $Project == "UNNAMED" } {
        MessageBoxOk [_ "Warning"] [= "Before writting results raster, a project title is needed. Save project to get it"] warning
        return
    }
    set directory $Project.gid
    if { [file pathtype $directory] == "relative" } {
        set directory [file join [pwd] $directory]
    }
    set workingdir [file join $directory Rasters]
    file mkdir $workingdir

    set results ""
    if { $PostRaster(over) == "nodes" } {
        lappend results nodescoord
    } else {
        lappend results elemsnum
    }

    #set current_anal [GiD_Info postprocess get cur_analysis]
    if { $Timeresults == "Currentstep" } {
        set rasteriter 1
        set current_step [GiD_Info postprocess get cur_step]
        if { $current_anal == "Topography"} {
            set current_step [lindex $steps end]
        }
    } elseif { $Timeresults == "Otherstep" } {
        set rasteriter 1
        set current_step $PostRaster(thisstep)
    } elseif { $Timeresults == "Allsteps" } {
        set allsteps [GiD_Info postprocess get all_steps $current_anal]
        set rasteriter [llength $allsteps]
        if { $current_anal == "Topography"} {
            set current_step [lindex $steps end]
        }
    }

    #Create auxiliary files
    set rastersdir [file join $workingdir $current_anal]
    file mkdir $rastersdir

    if { [llength $results] > 0 } {
        set selection ""
        set results $areresults
    } else {
        MessageBoxOk [_ "Warning"]  [= "No result selected"] warning
        return
    }

    #Auxiliary Files (Fitxer2 & Fitexer3)
    set filename2 "conn"
    append filename2 ".aux"
    set fitxer2 [file join $rastersdir $filename2]
    set filename3 "vert_coor"
    append filename3 ".aux"
    set fitxer3 [file join $rastersdir $filename3]
    ::PostRaster::CreateAuxiliaryFiles $fitxer2 $fitxer3

    if { $rasteriter == 1 } {
        ::PostRaster::SelectResults $results $PostRaster(over) $selection $selres $rasteriter $current_step
    } else {
        foreach item $allsteps {
            set current_step $item
            ::PostRaster::SelectResults $results $PostRaster(over) $selection $selres $rasteriter $current_step
        }
    }

    file delete $fitxer2
    file delete $fitxer3
}

proc ::PostRaster::ResultsToRasterWin { { w .gid.selectresults } } {
    global PostRaster
    #global FileType
    global RasterType
    global Interpolation
    global radius
    global cell
    global Timeresults
    global Reproject

    set all_analysis [GiD_Info postprocess get all_analysis]
    if { ![llength $all_analysis] } {
        MessageBoxOk [_ "Warning"] [= "Do not exists any result, probably the calculation has failed"] warning
        return
    }

    set Interpolation "linear"
    InitWindow $w [= "Select Results to Export"] PostSelectResultsWindowGeom ::PostRaster::ResultsToRasterWin
    ttk::frame $w.f -style ridge.TFrame -borderwidth 2
    if { ![info exists PostRaster(over)] || ($PostRaster(over) != "nodes" && $PostRaster(over) != "elements") } {
        set PostRaster(over) nodes
    }
    if { $PostRaster(over) == "nodes" } {
        set PostRaster(output,coordinates) 1
        set PostRaster(output,number) 1
    } else {
        set PostRaster(output,coordinates) 0
        set PostRaster(output,number) 1
    }
    set PostRaster(output,connectivities) 0

    #array unset PostRaster output,*
    set results [GiD_Info postprocess get cur_results_list contour_fill]
    set num_results [llength $results]
    set PostRaster(over) elements
    set cont 0
    foreach item $results {
        #set header [GiD_Result get -info [list $item [GiD_Info postprocess get cur_analysis] [GiD_Info postprocess get cur_step]]]
        set current_anal [GiD_Info postprocess get cur_analysis]
        set current_step [GiD_Info postprocess get cur_step]
        if {$current_anal == ""} {
            MessageBoxOk [_ "Warning"]  [= "Must select a results Analysis and Time Step"] warning
            return
        }
        if {$current_step == ""} {
            MessageBoxOk [_ "Warning"] [= "Must select a results Analysis and Time Step"] warning
            return
        }
        regsub -all _ $item { } item
        set PostRaster(output,$item) 0
        ttk::checkbutton $w.f.cb$cont -text [TranslateResultName $item] -variable PostRaster(output,$item)
        $w.f.cb$cont configure -state normal
        incr cont
    }

    grid rowconfigure $w.f [expr {$cont+2}] -weight 1
    grid columnconfigure $w.f 0 -weight 1
    grid $w.f -sticky nsew
    for {set i 0} {$i < $cont} {incr i} {
        grid $w.f.cb$i -sticky w
    }

    #Define the Analysis

    if { ![info exists current_anal]  } {
        set current_anal [lindex $all_analysis 0]
    }
    foreach item $all_analysis {
        lappend labelsanalysis $item
    }
    set PostRaster(thisanalysis) $current_anal
    ttk::frame $w.frmAnal  -borderwidth 2 -relief groove
    label $w.frmAnal.label0 -text [= "Analysis:"]
    TTKComboBox $w.frmAnal.reslist -state readonly -textvariable ::PostRaster(thisanalysis) -labels $labelsanalysis -values $all_analysis
    grid $w.frmAnal -sticky ew
    grid $w.frmAnal.label0 $w.frmAnal.reslist -sticky w

    #Define the time step(s) of the results
    if { ![info exists current_anal]  } {
        set all_analysis [GiD_Info postprocess get all_analysis]
        set current_anal [lindex $all_analysis 0]
    }
    set allsteps [GiD_Info postprocess get all_steps $current_anal]
    foreach item $allsteps {
        lappend labelstime $item
    }
    set Timeresults Currentstep
    set PostRaster(thisstep) ""
    ttk::frame $w.frmTime -borderwidth 2 -relief groove
    label $w.frmTime.label0 -text [= "Step:"]
    ttk::radiobutton $w.frmTime.rd0 -text [= "Current"] -value Currentstep -variable Timeresults
    ttk::radiobutton $w.frmTime.rd1 -text [= "All"] -value Allsteps -variable Timeresults
    ttk::radiobutton $w.frmTime.rd2 -text [= "Other"] -value Otherstep -variable Timeresults
    TTKComboBox $w.frmTime.reslist -state readonly -textvariable ::PostRaster(thisstep) -labels $labelstime -values $allsteps
    grid $w.frmTime -sticky ew
    grid $w.frmTime.label0 $w.frmTime.rd0 $w.frmTime.rd1 $w.frmTime.rd2 $w.frmTime.reslist -sticky w

    set Interpolation Linear
    ttk::frame $w.frmDry -borderwidth 2 -relief groove
    label $w.frmDry.label0 -text [= "Interpolation:"]
    ttk::radiobutton $w.frmDry.rd0 -text [= "Linear"] -value Linear -variable Interpolation
    ttk::radiobutton $w.frmDry.rd1 -text [= "Nearest"] -value Nearest -variable Interpolation
    grid $w.frmDry -sticky ew
    grid $w.frmDry.label0 $w.frmDry.rd0 $w.frmDry.rd1 -sticky w

    #Re-projection options ### Not available yet - Best opcion: GDAL library
    variable allzones
    variable labelszone
    variable labelshemis
    variable allhemis
    set Reproject 0
    set PostRaster(thiszone) 1
    set PostRaster(thishemis) N
    ttk::frame $w.frmPRJ -borderwidth 2 -relief groove
    label $w.frmPRJ.label0 -text [= "Re-project:"]
    label $w.frmPRJ.label1 -text [= "UTM zone:"]
    label $w.frmPRJ.label2 -text [= "Hemisfere:"]
    ttk::radiobutton $w.frmPRJ.rd0 -text [= "No"] -value 0 -variable Reproject
    ttk::radiobutton $w.frmPRJ.rd1 -text [= "Yes"] -value 1 -variable Reproject
    TTKComboBox $w.frmPRJ.reslist1 -state readonly -textvariable ::PostRaster(thiszone) -labels $labelszone -values $allzones -width 6
    TTKComboBox $w.frmPRJ.reslist2 -state readonly -textvariable ::PostRaster(thishemis) -labels $labelshemis -values $allhemis -width 6
    #grid $w.frmPRJ -sticky ew
    #grid $w.frmPRJ.label0 $w.frmPRJ.rd0 $w.frmPRJ.rd1 -sticky w
    #grid $w.frmPRJ.label1 $w.frmPRJ.reslist1 $w.frmPRJ.label2 $w.frmPRJ.reslist2 -sticky w

    set cell 10
    frame $w.frmData -bd 2 -relief groove
    label $w.frmData.lrow -text [= "Cell size (m)"]
    set e1 [entry $w.frmData.erow -relief sunken -width 15 -textvariable cell]
    #grid $w.frmData -padx 5 -pady 5 -sticky ew
    grid $w.frmData -sticky ew
    grid $w.frmData.lrow $w.frmData.erow -sticky news
    grid conf $w.frmData.erow -sticky ew -pady 5

    ttk::frame $w.buts -style BottomFrame.TFrame
    ttk::button $w.buts.apply -text [_ "Apply"] -command "::PostRaster::ApplyListEntities $w" -style BottomFrame.TButton
    ttk::button $w.buts.close -text [_ "Close"] -command "destroy $w" -style BottomFrame.TButton
    grid $w.buts.apply $w.buts.close -sticky ew -padx 5 -pady 6
    grid $w.buts -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.buts center }
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 0 -weight 1
    bind $w <Alt-c> "$w.buts.close invoke"
    bind $w <Escape> "$w.buts.close invoke"
    bind $w <Destroy> [list +::PostRaster::DestroySelectResults %W $w] ;# + to add to previous script
    #trace add variable FileType write "::PostRaster::ChangeSelectResultsOver $w ;#"
    trace add variable PostRaster(thisanalysis) write "::PostRaster::ChangeSelectResultsOver $w ;#"
    trace add variable labelstime write "::PostRaster::ChangeSelectResultsOver $w ;#"
    trace add variable allsteps write "::PostRaster::ChangeSelectResultsOver $w ;#"
    wm minsize $w 340 [expr 170+$num_results*20]
}

proc ::PostRaster::DestroySelectResults { W w } {
    if { $W != $w } return

    #reenter multiple times, one by toplevel child, only interest w
    global PostRaster
    global Interpolation
    global Timeresults
    #trace remove variable FileType write "::PostRaster::ChangeSelectResultsOver $w ;#"
    trace remove variable PostRaster(thisanalysis) write "::PostRaster::ChangeSelectResultsOver $w ;#"
    if { ![info exists PostRaster(thisanalysis_old)] } {
        #do nothing
    } else {
        unset PostRaster(thisanalysis_old)
        unset PostRaster(thisanalysis)
    }

}

proc ::PostRaster::ChangeSelectResultsOver { w } {
    global PostRaster
    global Interpolation
    global Timeresults
    global current_anal

    #Analysis
    if { ![info exists PostRaster(thisanalysis_old)] } {
        set current_anal_old [GiD_Info postprocess get cur_analysis]
    } else {
        set current_anal_old $PostRaster(thisanalysis_old)
    }

    if { $PostRaster(thisanalysis) != $current_anal_old } {
        set steps_old [GiD_Info postprocess get all_steps $current_anal_old]
        set results_old [GiD_Info postprocess get results_list contour_fill $current_anal_old [lindex $steps_old end]]
        set cont_old [llength $results_old]
        for {set i 0} {$i < $cont_old} {incr i} {
            grid remove $w.f.cb$i
            grid forget $w.f.cb$i
            destroy $w.f.cb$i
            #set w.f.cb$i ""
        }
        grid remove $w.f
        grid forget $w.f

        set cont 0

        set current_anal $PostRaster(thisanalysis)
        set steps [GiD_Info postprocess get all_steps $current_anal]
        set results_new [GiD_Info postprocess get results_list contour_fill $current_anal [lindex $steps end]]
        foreach item $results_new {
            regsub -all _ $item { } item
            set PostRaster(output,$item) 0
            ttk::checkbutton $w.f.cb$cont -text [TranslateResultName $item] -variable PostRaster(output,$item)
            $w.f.cb$cont configure -state normal
            incr cont
        }

        grid rowconfigure $w.f [expr {$cont+2}] -weight 1
        grid columnconfigure $w.f 0 -weight 1
        grid $w.f -sticky nsew
        for {set i 0} {$i < $cont} {incr i} {
            grid $w.f.cb$i -sticky w
        }

        set PostRaster(thisanalysis_old) $PostRaster(thisanalysis)
        set current_anal $PostRaster(thisanalysis)
    }

    #Steps list
    grid remove $w.frmTime
    grid remove $w.frmTime.label0 $w.frmTime.rd0 $w.frmTime.rd1 $w.frmTime.rd2 $w.frmTime.reslist
    destroy $w.frmTime.reslist

    set labelstime ""
    set allsteps ""
    set PostRaster(thisstep) ""
    set allsteps [GiD_Info postprocess get all_steps $current_anal]
    foreach item $allsteps {
        lappend labelstime $item
    }
    TTKComboBox $w.frmTime.reslist -state readonly -textvariable ::PostRaster(thisstep) -labels $labelstime -values $allsteps

    grid $w.frmTime -sticky ew
    grid $w.frmTime.label0 $w.frmTime.rd0 $w.frmTime.rd1 $w.frmTime.rd2 $w.frmTime.reslist -sticky w
}
