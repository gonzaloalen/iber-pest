
namespace eval AtmosphericVars {
    variable data ;#array of values
    variable atmospherics ;#list of names of data
    variable current_atmospheric ;#name of current selected atmospheric
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {table_value}
    variable fields_defaults {{0.0 0.0 0.0 0.0 0.0}}  
    variable drawopengl
    variable table ;#tktable widget
}

proc AtmosphericVars::UnsetVariables { } {
    variable data
    variable atmospherics      
    unset -nocomplain data
    unset -nocomplain atmospherics
}

proc AtmosphericVars::SetDefaultValues { } {    
    variable data
    variable atmospherics
    variable current_atmospheric
    variable current_value
    variable fields
    variable fields_defaults
    array unset data
    set atmospherics {}
    foreach field $fields  {
        set current_value($field) ""
    }
    set current_value(table_value) {0.0 0.0 0.0 0.0 0.0}
    set current_atmospheric {}
}

#store the atmospherics information in a hidden field of the problem data
#then this data is saved with the model without do nothing more
proc AtmosphericVars::FillTclDataFromProblemData { } {
    variable data
    variable atmospherics
    # OJOOOO revisar el HyetographsData y sustituirlo adecuadamente
    if { [catch {set x [GiD_AccessValue get gendata AtmosphericsData]} msg] } {
        #reading a model created by an old version of the problemtype this gendata doesn't exists
        return
    }    
    array set data $x
    
    set atmospherics [list]
    foreach item [array names data *,table_value] {
        lappend atmospherics [string range $item 0 end-12]
    }
    set atmospherics [lsort -dictionary $atmospherics]
    AtmosphericVars::SetCurrentAtmospheric [lindex $atmospherics 0]
}

proc AtmosphericVars::FillProblemDataFromTclData { } {
    variable data
    variable atmospherics
    set x [array get data]
    # OJOOOO revisar el HyetographsData y sustituirlo adecuadamente
    GiD_AccessValue set gendata AtmosphericsData $x
}

proc AtmosphericVars::WriteCalculationFile { } {
    variable data
    set res ""       
    
    set i 0
    append res "Atmospherics"  \n
    append res [llength [AtmosphericVars::GetAtmospherics]] \n
    foreach atmospheric [AtmosphericVars::GetAtmospherics] {
        incr i        
        append res $i  \n
        set lines [expr [llength $data($atmospheric,table_value)]/5]
        append res $lines  \n
        set l [expr $lines*5]
        for {set ii 0} {$ii < $l} {set ii [expr $ii+5]} {
            append res "[lrange $data($atmospheric,table_value) $ii [expr $ii+4]] " \n        
        }       
    }
    
    return $res
}

proc AtmosphericVars::WriteAtmosphericNumber { name } {
    variable number
    
    set number [expr [lsearch [AtmosphericVars::GetAtmospherics] $name]+1]
    
    return $number    
}


proc AtmosphericVars::GetAtmospherics { } {
    variable atmospherics
    if { ![info exists atmospherics] } {
        return ""
    }
    return $atmospherics
}

proc AtmosphericVars::GetCurrentAtmospheric { } {
    variable current_atmospheric
    return $current_atmospheric
}

proc AtmosphericVars::OnChangeSelectedAtmospheric { cb } {   
    AtmosphericVars::SetCurrentAtmospheric [AtmosphericVars::GetCurrentAtmospheric]
}

proc AtmosphericVars::SetCurrentAtmospheric { atmospheric } {
    variable data
    variable current_atmospheric
    variable current_value
    variable fields   
    variable table
    #fill in the current_value variables with data
    if { $atmospheric != "" } {
        foreach field $fields {
            if { ![info exists data($atmospheric,$field)] } {
                set data($atmospheric,$field) 0
            }
            set current_value($field) $data($atmospheric,$field)           
        }                
    } else {
        foreach field $fields {
            set current_value($field) ""
        }
    }
    
    #the other widgets have a linked variable that automatically update it but the table not
    if { [info exists table] && [winfo exists $table] } {
        GridData::SetData $table $current_value(table_value)
    }
    set current_atmospheric $atmospheric  
}

proc AtmosphericVars::GetIndex { atmospheric } {
    variable atmospherics    
    return [lsearch $atmospherics $atmospheric]
}

proc AtmosphericVars::Exists { atmospheric } {    
    if { [AtmosphericVars::GetIndex $atmospheric] != -1 } {
        set exists 1
    } else {
        set exists 0
    }
    return $exists
}

proc AtmosphericVars::GetAtmosphericAutomaticName { } {
    set basename [_ "atmospheric"]
    set i 1
    set atmospheric $basename-$i
    while { [AtmosphericVars::Exists $atmospheric] } {        
        incr i
        set atmospheric $basename-$i        
    }
    return $atmospheric
}

proc AtmosphericVars::NewAtmospherics { cb } {
    variable atmospherics
    variable data
    variable fields 
    variable fields_defaults
    set atmospheric [AtmosphericVars::GetAtmosphericAutomaticName]     
    
    if { [AtmosphericVars::Exists $atmospheric] } {
        #already exists
        return 1
    }
    foreach field $fields value $fields_defaults {
        set data($atmospheric,$field) $value
    }
    lappend atmospherics $atmospheric   
    if { [winfo exists $cb] } {
        $cb configure -values [AtmosphericVars::GetAtmospherics]
    }
    AtmosphericVars::SetCurrentAtmospheric $atmospheric
    return 0
}

proc AtmosphericVars::DeleteAtmospheric { cb } {
    variable atmospherics
    variable data
    variable fields 
    variable fields_defaults
    set atmospheric [AtmosphericVars::GetCurrentAtmospheric] 
    if { ![AtmosphericVars::Exists $atmospheric] } {
        #not exists
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete atmospheric '%s'" $atmospheric] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {     
        set i [AtmosphericVars::GetIndex $atmospheric] 
        array unset data $atmospheric,*        
        set atmospherics [lreplace $atmospherics $i $i]       
        if { [winfo exists $cb] } {
            $cb configure -values [AtmosphericVars::GetAtmospherics]
        }
        AtmosphericVars::SetCurrentAtmospheric [lindex $atmospherics 0]  
        GiD_Redraw     
    }
    return 0
}

proc AtmosphericVars::RenameAtmospheric { cb } {
    variable data
    variable atmospherics
    variable fields
    set atmospheric [AtmosphericVars::GetCurrentAtmospheric]   
    if { ![AtmosphericVars::Exists $atmospheric] } {
        #not exists
        return 1
    } 
    set new_name [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $atmospheric] \
            [= "Enter new name of %s '%s'" [= "atmospheric"] $atmospheric] gidquestionhead "any" ""]
    if { $new_name != "--CANCEL--" } {
        foreach field $fields {
            set data($new_name,$field) $data($atmospheric,$field)
        }
        array unset data $atmospheric,*
        set i [AtmosphericVars::GetIndex $atmospheric] 
        lset atmospherics $i $new_name
        if { [winfo exists $cb] } {
            $cb configure -values [AtmosphericVars::GetAtmospherics]           
        }
        AtmosphericVars::SetCurrentAtmospheric $new_name       
        GiD_Redraw 
    }
    return 0
}

#apply the values of the current window
proc AtmosphericVars::Apply { T } {
    variable data
    variable current_value
    variable fields
    
    set atmospheric [AtmosphericVars::GetCurrentAtmospheric]    
    foreach field $fields {
        set data($atmospheric,$field) $current_value($field)
    }
    
    set data($atmospheric,table_value) [GridData::GetDataAllItems $T]
    GiD_Redraw        
}

#not AtmosphericVars::StartDraw and AtmosphericVars::EndDraw are not used, the Draw button is hidden (because unregister fail in GiD 11.1.5d)
#and the redraw procedure is registered when loading the problemtype, and each atmospheric is draw depending on its 'visible' variable value
proc AtmosphericVars::StartDraw { bdraw} {
    variable drawopengl
    set drawopengl [GiD_OpenGL register AtmosphericVars::ReDraw]
    GiD_Redraw
    $bdraw configure -text [= "Finish draw"] -command [list AtmosphericVars::EndDraw $bdraw]
}

proc AtmosphericVars::EndDraw { bdraw } {
    variable drawopengl
    if { [info exists drawopengl] } { 
        #unregister the callback tcl procedure to be called by GiD when redrawing
        GiD_OpenGL unregister $drawopengl
        catch {unset drawopengl}
        GiD_Redraw
        if { $bdraw != "" } {
            $bdraw configure -text [= "Draw"] -command [list AtmosphericVars::StartDraw $bdraw]
        }
    }
}

proc AtmosphericVars::DestroyAtmosphericWindow { W w } {   
    if { $W == $w } {
        AtmosphericVars::EndDraw ""
        AtmosphericVars::FillProblemDataFromTclData             
    }
}

proc AtmosphericVars::Window { } {
    variable data
    variable atmospherics
    variable current_atmospheric
    variable current_value
    variable table
    
    if { ![info exists atmospherics] } {
        AtmosphericVars::SetDefaultValues
    }
    
    AtmosphericVars::FillTclDataFromProblemData    
    
    set w .gid.atmospheric
    InitWindow $w [= "Atmospheric variables"] PreAtmosphericWindowGeom AtmosphericVars::Window
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    set f [ttk::frame $w.fatmospherics]
    
    ttk::combobox $f.cb1 -textvariable AtmosphericVars::current_atmospheric -values [AtmosphericVars::GetAtmospherics] -state readonly
    bind $f.cb1 <<ComboboxSelected>> [list AtmosphericVars::OnChangeSelectedAtmospheric %W]
    ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list AtmosphericVars::NewAtmospherics $f.cb1]
    GidHelp $f.bnew  [= "Create a new atmospheric"]
    ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list AtmosphericVars::DeleteAtmospheric $f.cb1]
    GidHelp $f.bdel  [= "Delete a atmospheric"]
    ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list AtmosphericVars::RenameAtmospheric $f.cb1]
    GidHelp $w.f.bren  [= "Rename a atmospheric"]   
    grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
    grid $f -sticky new
    grid rowconfigure $f 0 -weight 1
    
    set f [ttk::frame $w.fatmospheric]        
    
    package require fulltktree
    set columns ""
    foreach name [list [= "Time (s)"] [= "Radiation (W/m2)"] [= "Wind_10 (m/s)"] [= "Air T (Celsius)"] [= "Hr (-)"]] {
        lappend columns [list 4 $name left text 1]
    }
    set T [fulltktree $f.t -columns $columns -showlines 0 -showheader 1 -expand 1]    
    set table $T
    $T configure -editbeginhandler GridData::EditBegin
    $T configure -editaccepthandler [list GridData::EditAccept]
    $T configure -deletehandler GridData::RemoveRows
    $T configure -contextualhandler_menu GridData::ContextualMenu    
    $T column configure all -button 0
    
    ttk::frame $f.fbuttons
    ttk::button $f.fbuttons.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $T 1]
    GidHelp $f.fbuttons.b1 [_ "Pressing this button, one new line will be added to this window"]    
    ttk::button $f.fbuttons.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $T 1]
    GidHelp $f.fbuttons.b2 [_ "Pressing this button, one line is deleted from this window"]        
    
    ttk::button $f.fbuttons.b3 -image [gid_themes::GetImage graphs.png small_icons] -text [_ "Graph"] \
        -command [list GridData::PlotCurveWin $T [= "atmospheric"] ""]
    GidHelp $f.fbuttons.b3 [_ "Pressing this button, a XY graph will be drawn"]
    
    grid $T -sticky nsew -columnspan 2
    grid $f.fbuttons.b1 $f.fbuttons.b2 $f.fbuttons.b3 -sticky w    
    grid $f.fbuttons -sticky ew
    
    
    
    grid columnconfigure $f {0} -weight 1
    grid rowconfigure $f {1 4} -weight 1
    grid $f -sticky nsew -padx 2 -pady 2
    GidHelp $f [= "Set atmospheric parameters"]
    
    bind $T <<Paste>> [list ::GridData::Paste $T CLIPBOARD]
    bind $T <<PasteSelection>> [list ::GridData::Paste $T PRIMARY]
    bind $T <<Cut>> [list GridData::Cut $T]
    bind $T <<Copy>> [list GridData::Copy $T]
    bind [$T givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header
    
    GridData::SetData $T $current_value(table_value)
    
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.bapply -text [= "Apply"] -command [list AtmosphericVars::Apply $T] -style BottomFrame.TButton
    #ttk::button $f.bdraw -text [= "Draw"] -command [list AtmosphericVars::StartDraw $f.bdraw] -style BottomFrame.TButton
    ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    #grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
    grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }        
    
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    bind $w <Alt-c> "$f.bclose invoke"
    bind $w <Escape> "$f.bclose invoke"   
    bind $w <Destroy> [list +AtmosphericVars::DestroyAtmosphericWindow %W $w] ;# + to add to previous script  
}

proc AtmosphericVars::CloseWindow { } {
    set w .gid.atmospheric
    if { [winfo exists $w] } {
        close $w
    }
}
