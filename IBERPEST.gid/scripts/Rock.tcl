

# for model_value and questions see the definitio of the condition Rock_Position in IBER.cnd
#
proc Iber::AssignRockWin { model_value questions type } {
    set filenames_and_questions [list]
	set position_by $type

    foreach question $questions {
        set filename_raster [Browser-ramR file read .gid [= "Read ASCII RASTER file to assign Rock %s parameter %s" $model_value $question] \
                {} {{{} {.txt}} {{} {.asc}} {{All files} {.*}}} ]
        if {$filename_raster != "" } {
            lappend filenames_and_questions $filename_raster $question
        } else {
            return 1
        }
    }
    
    if { [llength $filenames_and_questions] } {        
        GidUtils::WaitState
        set tstart [clock milliseconds]
        set show_advance_bar 1
        set fail [Iber::AssignRockDo $model_value $filenames_and_questions $show_advance_bar $position_by]
        set tend [clock milliseconds]
        GidUtils::EndWaitState        
        GidUtils::SetWarnLine [= "Rock %s assigned to elements. time %s seconds" $model_value [expr ($tend-$tstart)/1000.0]]
        #ask the user if want to draw it
        set answer [MessageBoxOptionsButtons [= "Rock assigned"] [= "Rock  was succesfully assigned to mesh elements. Do you want to draw it?"] [list yes no] [list [_ "Yes"] [_ "No"]] question ""]
        if {$answer == "yes"} {
            #ensure mesh view mode
            MeshView 0
            set i 0
            set n [llength $questions]
            foreach question $questions {
                GiD_Process Mescape Data Conditions DrawCond -ByColor- Rock_Position $question
                if { $i < [expr $n-1] } {
                    set answer [MessageBoxOptionsButtons [= "Draw Rock %s" $question] [= "Press continue to draw next parameter"] [list continue cancel] [list [_ "Continue"] [_ "Cancel"]] question ""]
                    if { $answer == "cancel" } {
                        after 1000 GiD_Process escape escape escape
                        break
                    }
                }
                incr i
            }
        }
    }
    return 0
}

#to assign to mesh elements rock (GiD condition Rock_Position with values from a list of ARC/Info .asc files, one by question to be set)
# 
proc Iber::AssignRockDo { model_value filenames_and_questions {show_advance_bar 1} position_by } {
    set fail 0
    variable percent 0
    variable stop 0
    
    #assign percent of four steps to 10 10 40 40
    
    set condition_name "Rock_Position"
    set default_values [GidUtils::GetConditionDefaultValues $condition_name] ;#default values    
    #lset default_values [GidUtils::GetConditionQuestionIndex $condition_name "Position_by"] $model_value
	GiD_AccessValue set condition $condition_name Position_by $position_by ;#$position_by is "Depth" or "elevation"

    if { $show_advance_bar } {
        GidUtils::CreateAdvanceBar [_ "Assign rock values"] [_ "Percentage"]: ::Iber::percent 100  {set ::Iber::stop 1}
    }
    set percent 0
    set nodata_value -9999 ;#assumed same value for all files
    set element_ids [Iber::GetElementIdsLayersNoFrozen]
    set num_elements [objarray length $element_ids]
    set xy_element_centers [Iber::GetElementCenters $element_ids]    
    set percent 10
    #initialize default values (except fixed model) for all elements
    for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
        set element_id [objarray get $element_ids $i_element]
        set element_values($element_id) $default_values        
    }    
    set n_raster [expr [llength $filenames_and_questions]/2]
    set n_refresh 1
    #fill a field by each raster
    set i_raster 0
    foreach {filename_raster condition_question} $filenames_and_questions {
        if { $show_advance_bar } {
            if { ![expr {$i_raster%$n_refresh}] } {
                set percent [expr int(double($i_raster)/$n_raster*100*0.4+20)]
            }
        }
        if { $stop } {
            set fail -1
            break
        }  
        set condition_question_index [GidUtils::GetConditionQuestionIndex $condition_name $condition_question]
        set values $default_values
        set raster_interpolation [GDAL::ReadRaster $filename_raster 0]
        set nodata_value [GDAL::GetNoDataValue $filename_raster]
		set nodata_value [list $position_by $nodata_value]
        #set interpolated_values [GiD_Raster interpolate $raster_interpolation [list nodes $xy_element_centers]]
        #-closest in this case also, because GiD_AssignData become a bottleneck in case of a each element a different value (and big strings), better use only a few discrete values
        set interpolated_values [GiD_Raster interpolate -closest $raster_interpolation [list nodes $xy_element_centers]]        
        for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
            set element_id [objarray get $element_ids $i_element]
            set value [objarray get $interpolated_values $i_element]
            lset element_values($element_id) $condition_question_index $value
        }
        incr i_raster
    }    
    #assign the final values
    #GiD_AssignData condition of a condition canrepeat=no an big mesh is a huge bottleneck 
    #(because it is moving every call the whole container to a temporary container sorted by it)
    #in future versions (>14.1.8d) we want to enhance the code storing entityvalue ordered by entity id and avoid the bottleneck
    #
    #to minimize GiD_AssignData calls join in a single assign the elements with same values
    for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
        set element_id [objarray get $element_ids $i_element]
        set values $element_values($element_id)
        lappend elements_same_value($values) $element_id
    }
    unset element_ids
    array unset element_values
    set list_values [array names elements_same_value]
    set num_different_values [llength $list_values]
    set n_refresh_different_values [expr int($num_different_values/4)]
    if { $n_refresh_different_values<1 } {
        set n_refresh_different_values 1
    }
    set i_different_values 0
    foreach values $list_values {
        if { [lindex $values 1] != [lindex $nodata_value 1] } {
            if { $show_advance_bar } {
                if { ![expr {$i_different_values%$n_refresh_different_values}] } {
                    set percent [expr int(double($i_different_values)/$num_different_values*100*0.4+60)]
                }
            }
            GiD_AssignData condition $condition_name body_elements $values $elements_same_value($values)
            incr i_different_values
        }
    }        
    if { $show_advance_bar } {
        GidUtils::DeleteAdvanceBar
    }
    return $fail
}
