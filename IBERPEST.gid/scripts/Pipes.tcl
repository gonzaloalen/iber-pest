#***Propiedades de los colectores (Iber_Pipes.dat)***
#***jaragonh -2009-***

#***Datos***
#***Agregado de UrbanDriange (Desde aqu�)********************************

#***No. y condiciones de colectores*******************************************
proc Col {i cont} {         
    set cols [GiD_Info conditions Pipes mesh]         
    if {$i==0} {
        set numcols [llength $cols]
        #numero de elementos con condici�n de pipe
        set resultado $numcols     
    } else {
        set col [lindex $cols $cont]      
        set resultado [lrange $col 1 1] 
        append resultado " "
        append resultado [lrange $col 3 3]
        append resultado " "
        append resultado [lrange $col 5 10]
    } 
    return $resultado
}



#*************Condiciones iniciales de los colectores 1D en todo el colector
proc ci1D {i cont} {    
    set cols [GiD_Info conditions Pipes_Initial_Condition mesh]         
    if {$i==0} {
        set numcols [llength $cols]
        #numero de elementos con condici�n de pipe
        set resultado $numcols     
    } else {
        set col [lindex $cols $cont]     
        set tipo [lrange $col 3 3]           
        if {$tipo=="Assign"} {  
            set resultado [lrange $col 1 1] 
            append resultado " "
            append resultado [lrange $col 4 4]
            append resultado " "
            append resultado [lrange $col 5 10]
        } else {           
            #calado normal, pone -88 a v y h
            set resultado [lrange $col 1 1] 
            append resultado " -88 -88"                                      
        }
    } 
    return $resultado
}



#***********************Condiciones de contorno 1D en la entrada************************************
proc cc1D1 {icol itiempo} {
    set cols [GiD_Info conditions Pipes mesh]
    set numcols [llength $cols]      
    set col [lindex $cols $icol]
    set idcol [lrange $col 1 1]   
    #Revisar en ccid1 y ccid2  cuando los coelctores inican en 1 va bien la anterior idcol, sino la siguiente
    set idcol1 [expr $icol+1]
    set p1 [lindex [GiD_Geometry get line $idcol] 2]
    set p1 [lrange $col 11 11]     
    set nodo1 [GiD_Info conditions 1D_Inlet mesh $p1]
    set nodo [lindex $nodo1 0]
    set aux [lrange $nodo 0 0]
    #    set ijunction [lrange $nodo 5 5]       
    if {$aux!=0} { 
        set p1 -1
        set er 0 
        set cflow [lrange $nodo 6 6]    
        if {$cflow == "SUPERCRITICAL"} {            
            set icc -11             
            set w [lrange $nodo 7 7] 
            if {$w=="Elevation"} {set er  1} 
            set cond [lrange $nodo 8 8]
            set cc1sb [lindex $cond 0]
            set numcc1 [lrange $cc1sb 1 1]
            set tq [lrange $cc1sb 2 end]          
            set pos [expr 3*$itiempo]            
            set ti [lrange $tq $pos $pos]
            set qi [lrange $tq $pos+1 $pos+1] 
            set hi [lrange $tq $pos+2 $pos+2]       
        } elseif {$cflow == "SUBCRITICAL"} {
            set icc -21  
            set cond [lrange $nodo 10 10]
            set cc1sb [lindex $cond 0]
            set numcc1 [lrange $cc1sb 1 1]
            set tq [lrange $cc1sb 2 end]          
            set pos [expr 2*$itiempo]            
            set ti [lrange $tq $pos $pos]
            set qi [lrange $tq $pos+1 $pos+1]
            set hi 0           
        }             
        #Entrada con flujo subcritico. Hidrograma        
        set res2 [format "%10s %10s %12.3f %12.3f %12.3f %10s %10s %10s %10s  %10s" $idcol1 1 $ti $qi $hi $icc $er 0 0 $p1]    
    } else {
        #Nodo 1D a la entrada del colector  
        set icc -91 
        #if {$ijunction==1} {set icc -81}     
        set res2 [format "%10s %10s %12.3f %12.3f %12.3f %10s %10s %10s %10s  %10s" $idcol1 1 0 0 0 $icc 0 0 0 $p1]  
    }         
    return $res2
}

#No. instantes de tiempo
proc cc1D01 {cont ifila} {
    set cols [GiD_Info conditions Pipes mesh]set nodos [GiD_Info conditions Junctions mesh]
    set nodo [lindex $nodos $cont]
    set numcols [llength $cols]      
    set col [lindex $cols $icol]
    set idcol [lrange $col 1 1] 
    set p1 [lrange $col 10 10]
    
    set nodo1 [GiD_Info conditions 1D_Inlet mesh $p1]
    set nodo [lindex $nodo1 0]
    set aux [lrange $nodo 0 0]
    if {$aux != 0} {
        set ijunction [lrange $nodo 5 5]
        set cond [lrange $nodo 10 10] 
        set cc1sb [lindex $cond 0]
        set numcc1 [lrange $cc1sb 1 1]        
        set res01 [expr $numcc1/2]    
    } else { set res01 1}
    return $res01
}

#Determina si la union de entrada al colector se modela con 1D o 2D
proc aux1cc1D1 {icol} {
    set cols [GiD_Info conditions Pipes mesh]
    set numcols [llength $cols]      
    set col [lindex $cols $icol]
    set idcol [lrange $col 1 1]       
    set p1 [lindex [GiD_Geometry get line $idcol] 2]
    set p1 [lrange $col 11 11]
    
    set datu [GiD_Info conditions Junctions mesh $p1]
    set u1d2d [lindex $datu 0]
    set m1d2d [lrange $u1d2d 8 8]
    if {$m1d2d=="1D"} {
        set res 1
    } elseif {$m1d2d=="2D"} { 
        set res 2
    } else  { 
        set res 0       
    }
    return $res
}

#Determina nodo del colector que tiene union 2D
proc aux2cc1D1 {icol} {
    set cols [GiD_Info conditions Pipes mesh]
    set numcols [llength $cols]
    for {set i 0} {$i<=$numcols-1} {incr i 1} {    
        set col [lindex $cols $i] 
        set idcol [lrange $col 1 1]  
        if {$idcol==$icol} {       
            set p1 [lindex [GiD_Geometry get line $idcol] 2]
            set p1 [lrange $col 11 11] 
            break  
        } 
    }   
    return $p1  
}


#***************Condiciones de contorno 1D en la salida**********************************************************************
proc cc1D2 {icol itiempo} {
    set cols [GiD_Info conditions Pipes mesh]
    set numcols [llength $cols]      
    set col [lindex $cols $icol]
    set idcol [lrange $col 1 1] 
    set idcol1 [expr $icol+1]
    set p2 [lindex [GiD_Geometry get line $idcol] 3]
    set p2 [lrange $col 13 13]  
    set nodo1 [GiD_Info conditions 1D_Outlet mesh $p2]
    set nodo [lindex $nodo1 0]
    set aux [lrange $nodo 0 0]    
    #    set ijunction [lrange $nodo 4 4]
    
    set datu [GiD_Info conditions Junctions mesh $p2]
    set u1d2d [lindex $datu 0]
    set m1d2d [lrange $u1d2d 8 8]
    
    if {$aux!=0} { 
        set p2 -2
        set er 0
        set cflow [lrange $nodo 5 5]                 
        if {$cflow == "Critical_depth"} {            
            set icc -99
            set ti 0
            set qi 0   
        } elseif {$cflow == "Supercritical/Critical_Flow"} {
            set icc -40            
        } elseif {$cflow == "Subcritical_Flow"} {
            set type  [lrange $nodo 6 6]
            if {$type=="Weir"} {
                set icc -60           
                set cv  [lrange $nodo 8 8]              
                set ti [lrange $nodo 12 12]                
                set w [lrange $nodo 9 9] 
                if {$w=="Height"} {
                    set qi [lrange $nodo 11 11]
                } elseif {$w=="Elevation"} {
                    set er 1
                    set qi [lrange $nodo 10 10] 
                }        
            } elseif {$type=="Given_level"} { 
                set icc -23           
                set cv 0
                set hcond [lrange $nodo 7 7] 
                set cc1sb [lindex $hcond 0]
                set numcc1 [lrange $cc1sb 1 1]
                set tq [lrange $cc1sb 2 end]          
                set pos [expr 2*$itiempo]            
                set ti [lrange $tq $pos $pos]
                set qi [lrange $tq $pos+1 $pos+1]                    
            }                 
        } 
        set res1 [format "%10s %10s %12.3f %12.3f %12.3f %10s %10s %10s %10s  %10s" $idcol1 2 0 $ti $qi $icc $er 0 0 $p2]    
    } else {
        #Nodo 1D a la salida del colector  
        set icc -92      
        #if {$ijunction==1} {set icc -82}    
        set res1 [format "%10s %10s %12.3f %12.3f %12.3f %10s %10s %10s %10s  %10s" $idcol1 2 0 0 0 $icc 0 0 0 $p2]   
    }
    return $res1
}
#No. instantes de tiempo
proc cc1D02 {icol} {
    set cols [GiD_Info conditions Pipes mesh]
    set numcols [llength $cols]      
    set col [lindex $cols $icol]
    set idcol [lrange $col 1 1] 
    set p2 [lindex [GiD_Geometry get line $idcol] 3]
    set p2 [lrange $col 13 13]
    
    set nodo1 [GiD_Info conditions 1D_Outlet mesh $p2]
    
    set nodo [lindex $nodo1 0]
    set aux [lrange $nodo 0 0]
    if {$aux != 0} {
        set ijunction [lrange $nodo 4 4]
        set cond [lrange $nodo 7 7] 
        set cc2sb [lindex $cond 0]
        set numcc1 [lrange $cc2sb 1 1]        
        set res02 [expr $numcc1/2]    
    } else { set res02 1}
    return $res02
}

#Determina si la union de salida al colector se modela con 1D o 2D
proc aux1cc1D2 {icol} {
    set cols [GiD_Info conditions Pipes mesh]
    set numcols [llength $cols]      
    set col [lindex $cols $icol]
    set idcol [lrange $col 1 1]       
    set p2 [lindex [GiD_Geometry get line $idcol] 3]
    set p2 [lrange $col 13 13]
    
    set datu [GiD_Info conditions Junctions mesh $p2]
    set u1d2d [lindex $datu 0]
    set m1d2d [lrange $u1d2d 8 8]
    if {$m1d2d=="1D"} {
        set res 1
    } elseif {$m1d2d=="2D"} { 
        set res 2
    } else  { 
        set res 0       
    }
    return $res
}

#Determina nodo del colector que tiene union 2D
proc aux2cc1D2 {icol} {
    set cols [GiD_Info conditions Pipes mesh]
    set numcols [llength $cols]  
    for {set i 0} {$i<=$numcols-1} {incr i 1} {
        set col [lindex $cols $i] 
        set idcol [lrange $col 1 1]        
        if {$idcol==$icol} {       
            set p2 [lindex [GiD_Geometry get line $idcol] 3]
            set p2 [lrange $col 13 13] 
            break  
        }  
    } 
    return $p2  
}

#***Agregado de UrbanDriange (Hasta aqu�)********************************
