#### create by Georgina 05/2009
#Geor 1
### Marcos 2022 ### Out-dated code
###################################################################################

proc CreateGridResWin { } {
    global GResPriv 
    
    set w .gid.gridresult
    
    set GResPriv(time) 10
    set GResPriv(size) 5
    set GResPriv(result) ""
    
    set values [list Depth Water_Elevation Velocity x_Velocity y_Velocity Discharge x_Discharge y_Discharge \
            Froude Critical_Diameter Hazard Manning Terrain Rain Erosion Bed_elevation Bedload \
            x_Bed_load y_Bed_load k Epsilon Turb_visc Suspended_Sed Maximums]
    set labels [list [= "Depth"] [= "Water_Elevation"] [= "Velocity"] [= "x_Velocity"] [= "y_Velocity"] [= "Discharge"] [= "x_Discharge"] [= "y_Discharge"] \
            [= "Froude"] [= "Critical_Diameter"] [= "Hazard"] [= "Manning"] [= "Terrain"] [= "Rain"] [= "Erosion"] [= "Bed_elevation"] [= "Bedload"] \
            [= "x_Bed_load"] [= "y_Bed_load"] k [= "Epsilon"] [= "Turb_visc"] [= "Suspended_Sed"] [= "Maximums"]]
    
    InitWindow $w [= "Export results raster (only if ASCII results 'ON')"] PostCreateGRIDWindowGeom CreateGridResWin
    
    ttk::frame $w.frmData    
    ttk::label $w.frmData.lnom -text [= "Result"]
    
    TTKComboBox $w.frmData.reslist -state readonly -textvariable ::GResPriv(result) -labels $labels -values $values
    ttk::label $w.frmData.lrow -text [= "Instant (s)"]
    ttk::entry $w.frmData.erow -textvariable GResPriv(time)
    ttk::label $w.frmData.lcol -text [= "Cell size (m)"]
    ttk::entry $w.frmData.ecol -textvariable GResPriv(size)
    
    ttk::frame $w.frmButton -style BottomFrame.TFrame
    ttk::button $w.frmButton.accept -text [= "Accept"] -command [list WriteGridResult $w]  -style BottomFrame.TButton
    ttk::button $w.frmButton.close -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    
    grid $w.frmData.lnom $w.frmData.reslist -sticky ew
    grid $w.frmData.lrow $w.frmData.erow -sticky ew
    grid $w.frmData.lcol $w.frmData.ecol -sticky ew
    grid configure $w.frmData.lnom $w.frmData.lrow $w.frmData.lcol -sticky w
    grid $w.frmData -padx 5 -pady 5 -sticky nsew
    grid columnconfigure $w.frmData 1 -weight 1
    
    grid $w.frmButton -sticky nsew
    grid $w.frmButton.accept $w.frmButton.close -padx 5 -pady 6
    if { $::tcl_version >= 8.5 } { grid anchor $w.frmButton center }
    
    grid columnconfigure $w "0" -weight 1
    grid rowconfigure $w "0" -weight 1
    
    bind $w <Alt-c> "$w.frmButton.close invoke"
    bind $w <Escape> "$w.frmButton.close invoke"
    bind $w <Return> "$w.frmButton.accept invoke"
    
    focus $w.frmButton.accept   
}

proc WriteGridResult {w} {    
    global GResPriv    
    
    # working folder   
    set Project [GiD_Info project ModelName]        
    
    if { $Project == "UNNAMED" } {
        tk_dialogRAM .gid.tmpwin error \
            [= "Before writting results raster, a project title is needed. Save project to get it" ]\
            error 0 OK
        return
    }
    set directory $Project.gid
    if { [file pathtype $directory] == "relative" } {
        set directory [file join [pwd] $directory]
    }
    
    set filename [file join $directory h.rep]
    if { ![file exists $filename] } {
        tk_dialogRAM .gid.tmpwin error \
            [= "There are not intermedia results files (*.rep). \n Please Calculate." ]\
            error 0 OK
        return 
    }
    
    if { ![info exists GResPriv(result)] || $GResPriv(result) == "" } {
        tk_dialogRAM .gid.tmpwin error [= "Must select the result to be exported" ] error 0 OK
        return
    }
    
    # parameters input file    
    set fdata [file join $directory resgrids.dat]
    set fd [open $fdata w]
    
    puts $fd $GResPriv(result)
    
    foreach item {time size} {
        set value $GResPriv($item)
        if { $value == "" } {
            tk_dialogRAM .gid.tmpwin error [= "Some entry(ies) are empty, please fill them" ] error 0 OK
            return
        } else {
            puts $fd $value
        }
    }
    close $fd 
    
    set program [Iber::GetFullPathExe resgrids.exe]
    set f [open |\"$program\" r+]
    puts $f \"$directory\"
    
    flush $f 
    destroy $w
    
    tk_dialogRAM .gid.tmpwin [= "Grid Result Created"] [= "New results raster file created in project folder"] info 0 OK
    
    unset GResPriv
}
