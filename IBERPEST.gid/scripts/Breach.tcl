
namespace eval Breach {
    variable data ;#array of values
    variable breachs ;#list of names of data
    variable current_breach ;#name of current selected breach
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {visible calculate start end type starting_at value TopElev BotElev TopWid BotWid Brtime}
    variable fields_defaults {1 1 {} {} 1 1 0.0 0.0 0.0 0.0 0.0 0.0}   
    variable drawopengl
    variable importbreaches
}

proc Breach::UnsetVariables { } {
    variable data
    variable breachs      
    unset -nocomplain data
    unset -nocomplain breachs
}

proc Breach::SetDefaultValues { } {    
    variable data
    variable breachs
    variable current_breach
    variable current_value
    variable fields
    variable fields_defaults
    array unset data
    set breachs {}
    foreach field $fields  {
        set current_value($field) ""
    }
    set current_breach {}
}

#store the breachs information in a hiddend field of the problem data
#then this data is saved with the model without do nothing more
proc Breach::FillTclDataFromProblemData { } {
    variable data
    variable breachs
    
    if { [catch {set x [GiD_AccessValue get gendata BreachsData]} msg] } {
        #reading a model created by an old version of the problemtype this gendata doesn't exists
        return
    }
    array set data $x
    
    set breachs [list]
    foreach item [array names data *,start] {
        lappend breachs [string range $item 0 end-6]
    }
    set breachs [lsort -dictionary $breachs]
    Breach::SetCurrentBreach [lindex $breachs 0]
}

proc Breach::FillProblemDataFromTclData { } {
    variable data
    variable breachs
    set x [array get data]
    GiD_AccessValue set gendata BreachsData $x
}

proc Breach::WriteCalculationFile { } {
    variable data
    set res ""       
    
    set i 0
    
    append res [llength [Breach::GetBreachs]] \n
    if { $res == 0 } {
        return $res
    } else {
        foreach breach [Breach::GetBreachs] {
            incr i        
            
            if { $data($breach,calculate) == 1 } {        
                set calc_breach "1 "
            } else {
                set calc_breach "0 "
            }
            
            if { $data($breach,type) == 1 } {
                append res "1"
                append res " "    
                append res $calc_breach
                append res " " 
                append res [lrange $data($breach,start) 0 1]
                append res " "    
                append res [lrange $data($breach,end) 0 1]
                append res " "
                append res $data($breach,TopElev)
                append res " "
                append res $data($breach,BotElev)
                append res " "
                append res $data($breach,value)
                append res " "
                append res $data($breach,Brtime)
                append res " "
                append res $data($breach,starting_at) \n
            } elseif { $data($breach,type) == 2 }  {
                append res "2"
                append res " "    
                append res $calc_breach
                append res " " 
                append res [lrange $data($breach,start) 0 1]
                append res " "    
                append res [lrange $data($breach,end) 0 1]
                append res " "
                append res $data($breach,TopElev)
                append res " "
                append res $data($breach,BotElev)
                append res " "
                append res $data($breach,value)
                append res " "
                append res $data($breach,Brtime)
                append res " "
                append res $data($breach,TopWid)
                append res " "
                append res $data($breach,BotWid) 
                append res " "
                append res $data($breach,starting_at) \n
            }  
        }  
        #    append res "STG x_1st_Point_Axis,y_1st_Point_Axis,x_2nd_Point_Axis,y_2nd_Point_Axis,  Dam_Top_Elevation,   Breach_bottom_elevation,  Breach_start,  Reservoir_Volume_Hm3,  Type(time=1,Elevation=2)" \n
        #    append res "Trap x_1st_Point_Axis, y_1st_Point_Axis, x_2nd_Point_Axis, y_2nd_Point_Axis, Dam_Top_Elevation, Breach_bottom_elevation, Breach_start_value,  Breaching_Time, Breach_Top_Width, Breach_Bottom_Width  Type(time=1,Elevation=2)" \n
        
        return $res
    }
}

proc Breach::GetBreachs { } {
    variable breachs
    if { ![info exists breachs] } {
        set breachs 0
    }
    return $breachs
}

proc Breach::GetCurrentBreach { } {
    variable current_breach
    return $current_breach
}

proc Breach::OnChangeSelectedBreach { cb } {   
    Breach::SetCurrentBreach [Breach::GetCurrentBreach]
}

proc Breach::SetCurrentBreach { breach } {
    variable data
    variable current_breach
    variable current_value
    variable fields   
    #fill in the current_value variables with data
    if { $breach != "" } {
        foreach field $fields {
            if { ![info exists data($breach,$field)] } {
                set data($breach,$field) 0
            }
            set current_value($field) $data($breach,$field)           
        }
    } else {
        foreach field $fields {
            set current_value($field) ""
        }
    }
    set current_breach $breach  
}

proc Breach::GetIndex { breach } {
    variable breachs    
    return [lsearch $breachs $breach]
}

proc Breach::Exists { breach } {    
    if { [Breach::GetIndex $breach] != -1 } {
        set exists 1
    } else {
        set exists 0
    }
    return $exists
}

proc Breach::GetBreachAutomaticName { } {
    set basename [_ "breach"]
    set i 1
    set breach $basename-$i
    while { [Breach::Exists $breach] } {        
        incr i
        set breach $basename-$i        
    }
    return $breach
}

proc Breach::NewBreach { cb } {
    variable breachs
    variable data
    variable fields 
    variable fields_defaults
    set breach [Breach::GetBreachAutomaticName]     
    
    if { [Breach::Exists $breach] } {
        #already exists
        return 1
    }
    foreach field $fields value $fields_defaults {
        set data($breach,$field) $value
    }
    lappend breachs $breach   
    if { [winfo exists $cb] } {
        $cb configure -values [Breach::GetBreachs]
    }
    Breach::SetCurrentBreach $breach
    return 0
}

proc Breach::DeleteBreach { cb } {
    variable breachs
    variable data
    variable fields 
    variable fields_defaults
    set breach [Breach::GetCurrentBreach] 
    if { ![Breach::Exists $breach] } {
        #not exists
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete breach '%s'" $breach] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {     
        set i [Breach::GetIndex $breach] 
        array unset data $breach,*        
        set breachs [lreplace $breachs $i $i]       
        if { [winfo exists $cb] } {
            $cb configure -values [Breach::GetBreachs]
        }
        Breach::SetCurrentBreach [lindex $breachs 0]  
        GiD_Redraw     
    }
    return 0
}

proc Breach::DeleteAllBreaches { cb } {
    variable breachs
    variable data
    variable fields 
    variable fields_defaults
    
    set breach [Breach::GetCurrentBreach] 
    if { ![Breach::Exists $breach] } {
        MessageBoxOk [= "Deleting process of breaches"] [= "There is no breaches to delete"] error
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete all breaches?"] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]

    if { $ret == 0 } {
        set i 1
        foreach breach $breachs { 
            set i [Breach::GetIndex $breach] 
            array unset data $breach,*        
            set breachs [lreplace $breachs $i $i]       
            if { [winfo exists $cb] } {
                $cb configure -values [Breach::GetBreachs]
            }
            Breach::SetCurrentBreach [lindex $breachs 0]  
            incr i
        }
    }
    GiD_Redraw
    GidUtils::SetWarnLine [= "All breaches have been deleted"]
    return 0
}

proc Breach::RenameBreach { cb } {
    variable data
    variable breachs
    variable fields
    set breach [Breach::GetCurrentBreach]   
    if { ![Breach::Exists $breach] } {
        #not exists
        return 1
    } 
    set new_name [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $breach] \
            [= "Enter new name of %s '%s'" [= "breach"] $breach] gidquestionhead "any" ""]
    if { $new_name != "--CANCEL--" } {
        foreach field $fields {
            set data($new_name,$field) $data($breach,$field)
        }
        array unset data $breach,*
        set i [Breach::GetIndex $breach] 
        lset breachs $i $new_name
        if { [winfo exists $cb] } {
            $cb configure -values [Breach::GetBreachs]           
        }
        Breach::SetCurrentBreach $new_name       
        GiD_Redraw 
    }
    return 0
}

#apply the values of the current window
proc Breach::Apply { } {
    variable data
    variable current_value
    variable fields
    
    set breach [Breach::GetCurrentBreach]    
    foreach field $fields {
        set data($breach,$field) $current_value($field)
    }
    
    GiD_Redraw        
}

#not Breach::StartDraw and Breach::EndDraw are not used, the Draw button is hidden (because unregister fail in GiD 11.1.5d)
#and the redraw procedure is registered when loading the problemtype, and each breach is draw depending on its 'visible' variable value
proc Breach::StartDraw { bdraw} {
    variable drawopengl
    set drawopengl [GiD_OpenGL register Breach::ReDraw]
    GiD_Redraw
    $bdraw configure -text [= "Finish draw"] -command [list Breach::EndDraw $bdraw]
}

proc Breach::EndDraw { bdraw } {
    variable drawopengl
    if { [info exists drawopengl] } { 
        #unregister the callback tcl procedure to be called by GiD when redrawing
        GiD_OpenGL unregister $drawopengl
        catch {unset drawopengl}
        GiD_Redraw
        if { $bdraw != "" } {
            $bdraw configure -text [= "Draw"] -command [list Breach::StartDraw $bdraw]
        }
    }
}

proc Breach::ReDraw { } {
    foreach breach [Breach::GetBreachs] {
        Breach::Draw $breach
    }
}

proc Breach::Draw { breach } {
    variable data
    
    if { $breach == "" || ![info exists data($breach,visible)] || !$data($breach,visible) } {
        return 1
    }
    
    foreach field {start end} {
        if { [llength $data($breach,$field)] != 3 } {
            return 1
        }
        foreach v $data($breach,$field) {
            if { ![string is double $v] } {
                return 1
            }
        }
    }  
    
    # Draw in "blue" when the breach is calculated, and in "grey" when the breach is not calculated
    if { $data($breach,calculate) == 1 } {
        set color {0 0 1}
    } else {
        set color {0.75 0.75 0.75}
    }
    GiD_OpenGL draw -color $color
    GiD_OpenGL draw -pointsize 3
    GiD_OpenGL draw -begin points
    GiD_OpenGL draw -vertex $data($breach,start)
    GiD_OpenGL draw -vertex $data($breach,end)
    GiD_OpenGL draw -end
    GiD_OpenGL draw -begin lines
    GiD_OpenGL draw -vertex $data($breach,start)
    GiD_OpenGL draw -vertex $data($breach,end)
    GiD_OpenGL draw -end
    #show label
    set p [MathUtils::VectorSum $data($breach,start) $data($breach,end)]
    set p [MathUtils::ScalarByVectorProd 0.5 $p]
    GiD_OpenGL draw -rasterpos $p
    GiD_OpenGL drawtext $breach
    return 0
}

proc Breach::DestroyBreachWindow { W w } {   
    if { $W == $w } {
        Breach::EndDraw ""
        Breach::FillProblemDataFromTclData             
    }
}

proc Breach::OnChangeType { f } {
    variable current_value
    if { $current_value(type) == 2 } {
        #Trapezoidal
        $f.lBrtime configure -text [= "Breaching time (s)"]        
        grid $f.lTopElev $f.eTopElev
        grid $f.lBotElev $f.eBotElev
        grid $f.lTopWid $f.eTopWid
        grid $f.lBotWid $f.eBotWid
        
    } else {
        #Spanish Technical Guide
        $f.lBrtime configure -text [= "Reservoir Vol (hm3)"]
        grid $f.lTopElev $f.eTopElev
        grid $f.lBotElev $f.eBotElev
        grid remove $f.lTopWid $f.eTopWid
        grid remove $f.lBotWid $f.eBotWid
    }
    #    TopElev BotElev TopWid BotWid Brtime
}

proc Breach::PickPointInSurfaceOrElementCmd { entry } {
    set view_mode [GiD_Info Project ViewMode]
    if { $view_mode == "GEOMETRYUSE" } {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an surface"] PointInSurface GEOMETRYUSE]
        #set surface_id $::GidPriv(selection)  
    } elseif { $view_mode == "MESHUSE"} {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an element"] PointInElement MESHUSE]
        #set element_id $::GidPriv(selection)    
    } else {
        set xyz ""
    }        
    if { $xyz != "" } {                    
        set res [format "%g %g %g" {*}$xyz]
        $entry delete 0 end
        $entry insert end $res        
    }
}

proc Breach::Window { } {
    variable data
    variable breachs
    variable current_breach
    variable current_value
    
    if { ![info exists breachs] } {
        Breach::SetDefaultValues
    }
    
    Breach::FillTclDataFromProblemData    
    
    set w .gid.breach
    InitWindow $w [= "Breach"] PreBreachWindowGeom Breach::Window
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    set f [ttk::frame $w.fbreachs]
    
    ttk::combobox $f.cb1 -textvariable Breach::current_breach -values [Breach::GetBreachs] -state readonly
    bind $f.cb1 <<ComboboxSelected>> [list Breach::OnChangeSelectedBreach %W]
    ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list Breach::NewBreach $f.cb1]
    GidHelp $f.bnew  [= "Create a new breach"]
    ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list Breach::DeleteBreach $f.cb1]
    GidHelp $f.bdel  [= "Delete a breach"]
    ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list Breach::RenameBreach $f.cb1]
    GidHelp $w.f.bren  [= "Rename a breach"]
    ttk::button $f.bdelall -image [gid_themes::GetImage erase.png small_icons] -command [list Breach::DeleteAllBreaches $f.cb1]
    GidHelp $f.bdelall  [= "Delete all breaches"]
    grid $f.cb1 $f.bnew $f.bdel $f.bren $f.bdelall -sticky w
    grid $f -sticky new
    grid rowconfigure $f 0 -weight 1
    
    set f [ttk::frame $w.fbreach]
    set field visible
    ttk::checkbutton $f.ch$field -text [= "Visible"] -variable Breach::current_value($field)
    grid $f.ch$field -sticky w
    
    set field calculate
    ttk::checkbutton $f.ch$field -text [= "Calculate"] -variable Breach::current_value($field)
    grid $f.ch$field -sticky w
    
    foreach field {start end} text [list [= "Start"] [= "End"]] {
        ttk::label $f.l$field -text $text
        ttk::entry $f.e$field -textvariable Breach::current_value($field)
        ttk::button $f.b$field -image [gid_themes::GetImage "point.png" small_icons] -command [list Breach::PickPointInSurfaceOrElementCmd $f.e$field]
        grid $f.l$field $f.e$field $f.b$field -sticky ew
    }
    
    foreach field {type} text [list [= "Type"]] values {{1 2}} labels [list [list [= "Spanish Technical Guide"] [= "Trapezoidal"]]] {
        ttk::label $f.l$field -text $text
        #ttk::combobox $f.c$field -values $values -state readonly -textvariable Breach::current_value($field)
        TTKComboBox $f.c$field -values $values -labels $labels -state readonly -textvariable Breach::current_value($field) \
            -modifycmd [list Breach::OnChangeType $f]
        grid $f.l$field $f.c$field -sticky ew
    }
    foreach field {starting_at} text [list [= "Starting at"]] values {{1 2}} labels [list [list [= "Time (s)"] [= "Elevation (m)"]]] {
        ttk::label $f.l$field -text $text
        #ttk::combobox $f.c$field -values $values -state readonly -textvariable Breach::current_value($field)
        TTKComboBox $f.c$field -values $values -labels $labels -state readonly -textvariable Breach::current_value($field) \
            -modifycmd [list Breach::OnChangeType $f]
        grid $f.l$field $f.c$field -sticky ew
    }
    
    foreach field {value} text [list [= "Value"]] {
        ttk::label $f.l$field -text $text
        ttk::entry $f.e$field -textvariable Breach::current_value($field)
        grid $f.l$field $f.e$field -sticky ew
    }
    foreach field {TopElev BotElev TopWid BotWid Brtime} text [list [= "Top elevation (m)"] [= "Bottom elevation (m)"] [= "Top width (m)"] [= "Bottom width (m)"] [= "Breaching time (s)"]] {
        ttk::label $f.l$field -text $text
        ttk::entry $f.e$field -textvariable Breach::current_value($field)
        grid $f.l$field $f.e$field -sticky ew
    }
    
    grid $f -sticky new
    grid columnconfigure $f {1} -weight 1
    
    
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.bapply -text [= "Apply"] -command [list Breach::Apply] -style BottomFrame.TButton
    ttk::button $f.bimport -text [= "Import"] -command [list Breach::Import] -style BottomFrame.TButton
    GidHelp $f.bimport  [= "Import the breach file from other model"]
    ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    grid $f.bapply $f.bimport $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }        
    
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    bind $w <Alt-c> "$f.bclose invoke"
    bind $w <Escape> "$f.bclose invoke"   
    bind $w <Destroy> [list +Breach::DestroyBreachWindow %W $w] ;# + to add to previous script  
}

proc Breach::CloseWindow { } {
    set w .gid.breach    
    if { [winfo exists $w] } {
        destroy $w
    }
}

proc Breach::Import { } { 
    variable importbreaches
    
    set types [list [list [= "File format"] {.dat}] [list [_ "All files"] {.*}]]
    set defaultextension .dat
    set importbreaches(file) ""
    
    set importbreaches(file) [Browser-ramR file read .gid [= "Choose Breach file"] $importbreaches(file) $types $defaultextension 0]      
    set importbreaches(filename) [file tail importbreaches(file)]
    set importbreaches(filedir) [file dirname importbreaches(file)]
    
    if { $importbreaches(file) == "" } {
        GidUtils::SetWarnLine [= "No breach file has been selected" ]
        return
    }
    set breachfile [open $importbreaches(file)]
    set importbreaches(numbreaches) [gets $breachfile]
    close $breachfile
    Breach::ReadBreachFile
}


proc Breach::ReadBreachFile { } {
    variable data
    variable breachs
    variable current_breach
    variable current_value
    variable importbreaches
    variable vars11 { type calculate s1 s2 e1 e2 TopElev BotElev value Brtime starting_at }
    variable vars13 { type calculate s1 s2 e1 e2 TopElev BotElev value Brtime TopWid BotWid starting_at }
    # {visible calculate start end type starting_at value TopElev BotElev TopWid BotWid Brtime}

#     GidUtils::DisableGraphics
    GidUtils::WaitState .gid
    set tstart [clock milliseconds]

    set breachfile [open $importbreaches(file) r]
    set importbreaches(numbreaches) [gets $breachfile]
    
    set check [file exists $importbreaches(file)]
    if { [file exists $importbreaches(file)] } {
        for {set i 1} {$i <= $importbreaches(numbreaches)} {incr i} {
            set breachdata ""
            set breachdata [gets $breachfile]
            set llarg [llength $breachdata]
            set typeofbreach [lrange $breachdata 0 0]
            
            Breach::GetBreachs
            set cb ""
            Breach::NewBreach $cb
            set breach [Breach::GetCurrentBreach]
            
            if { $llarg == 11 } {
                set current_value(type) 1
                for {set b 1} {$b <= $llarg} {incr b} {
                    set var [lrange $vars11 $b $b]
                    set current_value($var) [lrange $breachdata $b $b]
                }
                set current_value(start) [list $current_value(s1) $current_value(s2) 0]
                set current_value(end) [list $current_value(e1) $current_value(e2) 0]
                set current_value(visible) 1
            } elseif { $llarg == 13 } {
                set current_value(type) 2
                for {set b 1} {$b <= $llarg} {incr b} {
                    set var [lrange $vars13 $b $b]
                    set current_value($var) [lrange $breachdata $b $b]
                }
                set current_value(start) [list $current_value(s1) $current_value(s2) 0]
                set current_value(end) [list $current_value(e1) $current_value(e2) 0]
                set current_value(visible) 1
            } else {
                MessageBoxOk [= "Import process of breaches"] [= "Wrong breach file format"] error
                return
            }
            
            #close $filecheck
            
            Breach::Apply
            Breach::FillProblemDataFromTclData
        }
        #         set breachfile [open $importbreaches(file)]
    } else {
        MessageBoxOk [= "Import process of breaches"] [= "Breach file does not exists"] error
        return
    }
    
    Breach::CloseWindow
    
    
    set tend [clock milliseconds]
    set elapsedtime [expr ($tend-$tstart)/1000.0]
    GidUtils::EndWaitState .gid
#     GidUtils::EnableGraphics
    
    Breach::Window
    
    GidUtils::SetWarnLine [= "%s breaches imported successfully in %s seconds" $importbreaches(numbreaches) $elapsedtime]
    close $breachfile
}
