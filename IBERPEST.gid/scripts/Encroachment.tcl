
namespace eval Encroachment {
    
}

proc Encroachment::Win { type } {   
    #    set type [GidUtils::GetProblemDataValue Define]
    
    if { $type == "1"} {
        set index [tk_dialogRAM .gid.tmpwin [= "Encroachment Polygon"] \
                [= "Select the line(s) that represents the Polygon. \n The area inside the polygon will be \
                    used to calculate. \n Remember that points must be consecutives."] info 0 [= "Yes"] [= "No"]]
        
        set current_layer [GiD_Info Project LayerToUse]
        
        if {$index == 0} {       
            Encroachment::CreatePolyLine
            Encroachment::AssignCond
        }
        GiD_Process 'Layers ToUse $current_layer escape
    } elseif { $type == "2"} {      
        set index [tk_dialogRAM .gid.tmpwin [= "Encroachment Line"] \
                [= "Select the line(s) that represents the flow path. \n This line together with the defined width will be used \
                    to calculate."] info 0 [= "Yes"] [= "No"]]
        
        set current_layer [GiD_Info Project LayerToUse]
        
        if {$index == 0} {       
            Encroachment::CreateLine
            Encroachment::Width            
            Encroachment::EditLine
            Encroachment::AssignCond
        }
        GiD_Process 'Layers ToUse $current_layer escape
    }
    
}

proc Encroachment::CreatePolyLine { } {    
    set selected_lines [GidUtils::PickEntities "Lines" "multiple"]   
    if { $selected_lines != "" } {
        set special_layer *Encroachment*
        set ipos [lsearch -exact [GiD_Info Layers] $special_layer]
        if { $ipos == -1 } { 
            GiD_Process Mescape Layers New $special_layer escape
            GiD_Process Mescape Layers Color $special_layer 255255000 escape
        }
        GiD_Process 'Layers ToUse $special_layer escape  
        GiD_Process 'Layers Entities $special_layer LowerEntities Lines {*}$selected_lines Mescape
        GiD_Process Mescape Geometry Create PolyLine Layer:$special_layer escape        
        GiD_Process Mescape Meshing MeshCriteria NoMesh Lines Layer:$special_layer Mescape
    }
}

proc Encroachment::CreateLine { } {
    variable selected_lines
    
    set selected_lines [GidUtils::PickEntities "Lines" "multiple"]
    if { $selected_lines != "" } {
        set special_layer *Encroachment*        
        set ipos [lsearch -exact [GiD_Info Layers] $special_layer]
        if { $ipos == -1 } {
            GiD_Process 'Layers New $special_layer escape
            GiD_Process 'Layers Color $special_layer 255255000 escape
        }
        GiD_Process 'Layers ToUse $special_layer escape          
        GiD_Process 'Layers Entities $special_layer LowerEntities Lines {*}$selected_lines escape
        GiD_Process Mescape Meshing MeshCriteria NoMesh Lines Layer:$special_layer escape
    } else {
        tk_dialogRAM .gid.tmpwin error \
            [= "Please, select lines" ]\
            error 0 OK
        Encroachment::Win 2
    }
}

proc Encroachment::EditLine { } { 
    variable selected_lines
    
    set listofpoints ""
    set coordofpoints ""
    ###
    set aux_layer *Encroachment_aux*
    set ipos [lsearch -exact [GiD_Info Layers] $aux_layer]
    if { $ipos == -1 } { 
        GiD_Process Mescape Layers New $aux_layer escape
    }
    GiD_Process 'Layers ToUse $aux_layer escape
    
    
    set size [GiD_AccessValue get gendata {Width_[m]}]
    set size_neg [expr {-1*$size}]
    set uncompact_selected_lines [GidUtils::UnCompactNumberList $selected_lines]
    foreach item $uncompact_selected_lines {
        GiD_Process MEscape Utilities Copy Lines Duplicate Offset $size $item escape Mescape
        GiD_Process MEscape Utilities Copy Lines Duplicate Offset $size_neg $item escape Mescape
    }
    
    GiD_Process 'Layers Freeze $aux_layer escape
    GiD_Redraw
    
    set check [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you agree with this discretization?"] \
            gidquestionhead 0 [= "Yes"] [= "No"]]
    GiD_Process 'Layers UnFreeze $aux_layer escape
    GiD_Layers delete $aux_layer
    GiD_Redraw
    if { $check == 1 } {
        Encroachment::Win 2
    } else {   
        set special_layer *Encroachment*
        set divisionlength  [expr { $size/4 }]
        
        GiD_Project set disable_windows 1
        
        GiD_Process MEscape Geometry Edit DivideLine Multiple Measure $divisionlength Layer:$special_layer Mescape
        GiD_Process MEscape MEscape
        
        GiD_Project set disable_windows 0
    }    
}

proc Encroachment::Width { } {
    
    set value [GiD_AccessValue get gendata {Width_[m]}]
    set width  [tk_dialogEntryRAM   \
            .gid.width [= "Encroachment Width"] [= "Enter the width of the study area."] gidquestionhead \
            real+ $value ""]
    if { $width ==    "--CANCEL--" } {
        return  1
    } else {    
        GiD_AccessValue set gendata {Width_[m]} $width
    }       
}

proc Encroachment::AssignCond { } {
    
    set list [GiD_Info Layers -entities points *Encroachment*]
    foreach e $list {
        GiD_AssignData condition Encroachment points "" $e
    }    
}

proc Encroachment::ListNodeLine { GenData } {
    set res ""   
    foreach e [GiD_Info Layers -entities points *Encroachment*]  {
        set info [GiD_Geometry get point $e]
        append res "[lindex $info 1] [lindex $info 2] $GenData\n"
    }   
    return $res
}

proc Encroachment::ListNodePolygon { GenData } {
    set res ""     
    foreach e [GiD_Info Layers -entities points *Encroachment*] {
        set info [GiD_Geometry get point $e]
        append res "[lindex $info 1] [lindex $info 2] $GenData\n"
    }
    return $res
}
