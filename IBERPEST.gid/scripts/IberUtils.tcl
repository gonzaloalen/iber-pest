#IberUtils.tcl  -*- TCL -*-
#Geor 2

###################################################

namespace eval IberUtils {

}

#
#   Devuelve el nombre del proyecto actualmente cargado
#
proc ModelName {} {
    return [file tail [GiD_Info project ModelName]]
}

#
# Devuelve valores de la condición 1D_Section
#
proc TramAndSeccNumber { node } {
    return [lrange [lindex [GiD_Info conditions 1D_Section mesh $node] 0] 3 4]

}

proc OneElementMesh {} {

    GidUtils::DisableGraphics
    GidUtils::DisableWarnLine
    GidUtils::WaitState .gid

    GiD_Process Mescape Meshing CancelMesh PreserveFrozen Yes Mescape Meshing reset Yes Mescape \
        Meshing Structured Surfaces 1: escape 1 1: escape escape Mescape \
        Meshing Generate 0.6 MeshingParametersFrom=Preferences MEscape Mescape
    # Meshing MeshView

    GidUtils::EnableGraphics
    GidUtils::EnableWarnLine
    GidUtils::EndWaitState .gid
    GidUtils::SetWarnLine [_ "Structured meshing finished"]
    GiD_Process Mescape Meshing MeshView
}

proc ConnectPointsMesh {} {
    GiD_Process Mescape Meshing ConnectPoints CreateTriangles 1:End escape
}

proc ConnectPointsGeom {} {
    GiD_Process Mescape Meshing ConnectPoints CreateSurfaces 1:End escape
}

proc IberUtils::TkwidgetPickPointInSurfaceOrElement { event args } {
    global tkwidgedprivpicknodebuttons
    switch $event {
        INIT {
            set PARENT [lindex $args 0]
            upvar [lindex $args 1] ROW
            set GDN [lindex $args 2]
            set STRUCT [lindex $args 3]
            set QUESTION [lindex $args 4]
            set entry ""
            foreach item [grid slaves $PARENT -row [expr $ROW-1]] {
                if { [winfo class $item] == "Entry"  || [winfo class $item] == "TEntry" } {
                    #assumed that it is the only entry of this row
                    set entry $item
                    break
                }
            }
            set tkwidgedprivpicknodebuttons($QUESTION) [ttk::button $PARENT.bpicknode$QUESTION \
                    -image [gid_themes::GetImage "point.png" small_icons] \
                    -command [list IberUtils::PickPointInSurfaceOrElementCmd $entry]]
            grid $tkwidgedprivpicknodebuttons($QUESTION) -row [expr $ROW-1] -column 2 -sticky w
            grid configure $entry -sticky ew
            return ""
        }
        SYNC {
            #set GDN [lindex $args 0]
            #set STRUCT [lindex $args 1]
            #set QUESTION [lindex $args 2]
            #DWLocalSetValue $GDN $STRUCT $QUESTION $value
        }
        DEPEND {
            #set GDN [lindex $args 0]
            #set STRUCT [lindex $args 1]
            set QUESTION [lindex $args 2]
            set ACTION [lindex $args 3]
            #set value [lindex $args 4]
            if { [info exists tkwidgedprivpicknodebuttons($QUESTION)] && \
                [winfo exists $tkwidgedprivpicknodebuttons($QUESTION)] } {
                if { $ACTION == "HIDE" } {
                    grid remove $tkwidgedprivpicknodebuttons($QUESTION)
                } else {
                    #RESTORE
                    grid $tkwidgedprivpicknodebuttons($QUESTION)
                }
            }
        }
        CLOSE {
            array unset tkwidgedprivpicknodebuttons
        }
        default {
            return [list ERROR [_ "Unexpected tkwidget event"]]
        }
    }
}

proc IberUtils::PickPointInSurfaceOrElementCmd { entry } {
    set view_mode [GiD_Info Project ViewMode]
    if { $view_mode == "GEOMETRYUSE" } {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an surface"] PointInSurface GEOMETRYUSE]
        #set surface_id $::GidPriv(selection)
    } elseif { $view_mode == "MESHUSE"} {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an element"] PointInElement MESHUSE]
        #set element_id $::GidPriv(selection)
    } else {
        set xyz ""
    }
    if { $xyz != "" } {
        set res [format "%g %g %g" {*}$xyz]
        $entry delete 0 end
        $entry insert end $res
    }
}

proc IberUtils::TkwidgetGetHyetographname { event args } {
    global tkwidgedprivhyetograph
    switch $event {
        INIT {
            set PARENT [lindex $args 0]
            upvar [lindex $args 1] ROW
            set GDN [lindex $args 2]
            set STRUCT [lindex $args 3]
            set QUESTION [lindex $args 4]
            #initialize variable to current field value
            if { [DWLocalGetValue $GDN $STRUCT $QUESTION] != "" } {
                set tkwidgedprivhyetograph($QUESTION) [DWLocalGetValue $GDN $STRUCT $QUESTION]
            } else {
                set tkwidgedprivhyetograph($QUESTION) [lindex [Hyetograph::GetHyetograph] 0]
            }

            #set entry $PARENT.e$ROW
            set entry ""
            foreach item [grid slaves $PARENT -row [expr $ROW-1]] {
                if { [winfo class $item] == "Entry"  || [winfo class $item] == "TEntry" } {
                    #assumed that it is the only entry of this row
                    set entry $item
                    break
                }
            }
            if { $entry != "" } {
                #set width [$entry cget -width]
                set width 9
                set tkwidgedprivhyetograph($QUESTION) [ttk::combobox $PARENT.chyetograph$QUESTION \
                        -textvariable tkwidgedprivhyetograph($QUESTION,hyetographname) \
                        -values [Hyetograph::GetHyetograph] \
                        -width $width]

                grid remove $entry
                grid $tkwidgedprivhyetograph($QUESTION) -row [expr $ROW-1] -column 1 -sticky ew -columnspan 2
            }
        }
        SYNC {
            lassign $args GDN STRUCT QUESTION
            if { [info exists tkwidgedprivhyetograph($QUESTION,hyetographname)] } {
                DWLocalSetValue $GDN $STRUCT $QUESTION $tkwidgedprivhyetograph($QUESTION,hyetographname)
            }
        }
        DEPEND {
            #set GDN [lindex $args 0]
            #set STRUCT [lindex $args 1]
            set QUESTION [lindex $args 2]
            set ACTION [lindex $args 3]
            #set value [lindex $args 4]
            if { [info exists tkwidgedprivhyetograph($QUESTION)] && \
                [winfo exists $tkwidgedprivhyetograph($QUESTION)] } {
                if { $ACTION == "HIDE" } {
                    grid remove $tkwidgedprivhyetograph($QUESTION)
                } else {
                    #RESTORE
                    grid $tkwidgedprivhyetograph($QUESTION)
                }
            } else {

            }
        }
        CLOSE {
            #array unset tkwidgedprivhyetograph
        }
        default {
            return [list ERROR [_ "Unexpected tkwidget event"]]
        }
    }
    return ""
}

proc IberUtils::TkwidgetGetDamageCurvename { event args } {
    global tkwidgedprivdamagecurve
    switch $event {
        INIT {
            set PARENT [lindex $args 0]
            upvar [lindex $args 1] ROW
            set GDN [lindex $args 2]
            set STRUCT [lindex $args 3]
            set QUESTION [lindex $args 4]
            #initialize variable to current field value
            if { [DWLocalGetValue $GDN $STRUCT $QUESTION] != "" } {
                set tkwidgedprivdamagecurve($QUESTION) [DWLocalGetValue $GDN $STRUCT $QUESTION]
            } else {
                set tkwidgedprivdamagecurve($QUESTION) [lindex [Damage::GetDamageCurve] 0]
            }

            #set entry $PARENT.e$ROW
            set entry ""
            foreach item [grid slaves $PARENT -row [expr $ROW-1]] {
                if { [winfo class $item] == "Entry"  || [winfo class $item] == "TEntry" } {
                    #assumed that it is the only entry of this row
                    set entry $item
                    break
                }
            }
            if { $entry != "" } {
                #set width [$entry cget -width]
                set width 18
                set tkwidgedprivdamagecurve($QUESTION) [ttk::combobox $PARENT.cdamage_curve$QUESTION \
                        -textvariable tkwidgedprivdamagecurve($QUESTION,damagecurvename) \
                        -values [Damage::GetDamageCurve] \
                        -width $width]

                grid remove $entry
                grid $tkwidgedprivdamagecurve($QUESTION) -row [expr $ROW-1] -column 1 -sticky ew -columnspan 2
            }
        }
        SYNC {
            lassign $args GDN STRUCT QUESTION
            if { [info exists tkwidgedprivdamagecurve($QUESTION,damagecurvename)] } {
                set damage_curve_num  [Damage::GetDamageCurveNumber $tkwidgedprivdamagecurve($QUESTION,damagecurvename)]
                DWLocalSetValue $GDN $STRUCT $QUESTION $damage_curve_num
            }
        }
        DEPEND {
            #set GDN [lindex $args 0]
            #set STRUCT [lindex $args 1]
            set QUESTION [lindex $args 2]
            set ACTION [lindex $args 3]
            #set value [lindex $args 4]
            if { [info exists tkwidgedprivdamagecurve($QUESTION)] && \
                [winfo exists $tkwidgedprivdamagecurve($QUESTION)] } {
                if { $ACTION == "HIDE" } {
                    grid remove $tkwidgedprivdamagecurve($QUESTION)
                } else {
                    #RESTORE
                    grid $tkwidgedprivdamagecurve($QUESTION)
                }
            } else {

            }
        }
        CLOSE {
            #array unset
        }
        default {
            return [list ERROR [_ "Unexpected tkwidget event"]]
        }
    }
    return ""
}

# WQ 2.0 (Luis-Marcos 08/2019) -- Como TCL (2o BOOK)
proc IberUtils::TkwidgetGetAtmosphericname { event args } {
    global tkwidgedprivatmospheric
    switch $event {
        INIT {
            set PARENT [lindex $args 0]
            upvar [lindex $args 1] ROW
            set GDN [lindex $args 2]
            set STRUCT [lindex $args 3]
            set QUESTION [lindex $args 4]
            #initialize variable to current field value
            if { [DWLocalGetValue $GDN $STRUCT $QUESTION] != "" } {
                set tkwidgedprivatmospheric($QUESTION) [DWLocalGetValue $GDN $STRUCT $QUESTION]
            } else {
                set tkwidgedprivatmospheric($QUESTION) [lindex [AtmosphericVars::GetAtmospherics] 0]
            }

            #set entry $PARENT.e$ROW
            set entry ""
            foreach item [grid slaves $PARENT -row [expr $ROW-1]] {
                if { [winfo class $item] == "Entry"  || [winfo class $item] == "TEntry" } {
                    #assumed that it is the only entry of this row
                    set entry $item
                    break
                }
            }
            if { $entry != "" } {
                #set width [$entry cget -width]
                set width 9
                set tkwidgedprivatmospheric($QUESTION) [ttk::combobox $PARENT.catmospheric$QUESTION \
                        -textvariable tkwidgedprivatmospheric($QUESTION,atmosphericname) \
                        -values [AtmosphericVars::GetAtmospherics] \
                        -width $width]

                grid remove $entry
                grid $tkwidgedprivatmospheric($QUESTION) -row [expr $ROW-1] -column 1 -sticky ew -columnspan 2
            }
        }
        SYNC {
            lassign $args GDN STRUCT QUESTION
            if { [info exists tkwidgedprivatmospheric($QUESTION,atmosphericname)] } {
                DWLocalSetValue $GDN $STRUCT $QUESTION $tkwidgedprivatmospheric($QUESTION,atmosphericname)
            }
        }
        DEPEND {
            #set GDN [lindex $args 0]
            #set STRUCT [lindex $args 1]
            set QUESTION [lindex $args 2]
            set ACTION [lindex $args 3]
            #set value [lindex $args 4]
            if { [info exists tkwidgedprivatmospheric($QUESTION)] && \
                [winfo exists $tkwidgedprivatmospheric($QUESTION)] } {
                if { $ACTION == "HIDE" } {
                    grid remove $tkwidgedprivatmospheric($QUESTION)
                } else {
                    #RESTORE
                    grid $tkwidgedprivatmospheric($QUESTION)
                }
            } else {

            }
        }
        CLOSE {
            #array unset tkwidgedprivatmospheric
        }
        default {
            return [list ERROR [_ "Unexpected tkwidget event"]]
        }
    }
    return ""
}
# WQ 2.0 (Luis-Marcos 08/2019) -- Como CONDITIONS (1r BOOK) o como TCL (2o BOOK)

proc IberUtils::TkwidgetGetZonename { event args } {
    global tkwidgedprivzone
    switch $event {
        INIT {
            set PARENT [lindex $args 0]
            upvar [lindex $args 1] ROW
            set GDN [lindex $args 2]
            set STRUCT [lindex $args 3]
            set QUESTION [lindex $args 4]
            #initialize variable to current field value
            if { [DWLocalGetValue $GDN $STRUCT $QUESTION] != "" } {
                set tkwidgedprivzone($QUESTION) [DWLocalGetValue $GDN $STRUCT $QUESTION]
            } else {
                set tkwidgedprivzone($QUESTION) [lindex [SoilErosion::GetZones] 0]
            }

            #set entry $PARENT.e$ROW
            set entry ""
            foreach item [grid slaves $PARENT -row [expr $ROW-1]] {
                if { [winfo class $item] == "Entry"  || [winfo class $item] == "TEntry" } {
                    #assumed that it is the only entry of this row
                    set entry $item
                    break
                }
            }
            if { $entry != "" } {
                #set width [$entry cget -width]
                set width 9
                set tkwidgedprivzone($QUESTION) [ttk::combobox $PARENT.czone$QUESTION \
                        -textvariable tkwidgedprivzone($QUESTION,zonename) \
                        -values [SoilErosion::GetZones] \
                        -width $width]

                grid remove $entry
                grid $tkwidgedprivzone($QUESTION) -row [expr $ROW-1] -column 1 -sticky ew -columnspan 2
            }
        }
        SYNC {
            lassign $args GDN STRUCT QUESTION
            if { [info exists tkwidgedprivzone($QUESTION,zonename)] } {
                DWLocalSetValue $GDN $STRUCT $QUESTION $tkwidgedprivzone($QUESTION,zonename)
            }
        }
        DEPEND {
            #set GDN [lindex $args 0]
            #set STRUCT [lindex $args 1]
            set QUESTION [lindex $args 2]
            set ACTION [lindex $args 3]
            #set value [lindex $args 4]
            if { [info exists tkwidgedprivzone($QUESTION)] && \
                [winfo exists $tkwidgedprivzone($QUESTION)] } {
                if { $ACTION == "HIDE" } {
                    grid remove $tkwidgedprivzone($QUESTION)
                } else {
                    #RESTORE
                    grid $tkwidgedprivzone($QUESTION)
                }
            } else {

            }
        }
        CLOSE {
            #array unset tkwidgedprivzone
        }
        default {
            return [list ERROR [_ "Unexpected tkwidget event"]]
        }
    }
    return ""
}

proc IberUtils::TkwidgetGetCatchmentname { event args } {
    global tkwidgedprivzone
    switch $event {
        INIT {
            set PARENT [lindex $args 0]
            upvar [lindex $args 1] ROW
            set GDN [lindex $args 2]
            set STRUCT [lindex $args 3]
            set QUESTION [lindex $args 4]
            #initialize variable to current field value
            if { [DWLocalGetValue $GDN $STRUCT $QUESTION] != "" } {
                set tkwidgedprivzone($QUESTION) [DWLocalGetValue $GDN $STRUCT $QUESTION]
            } else {
                set tkwidgedprivzone($QUESTION) [lindex [Groundwater::GetCatchment] 0]
            }

            #set entry $PARENT.e$ROW
            set entry ""
            foreach item [grid slaves $PARENT -row [expr $ROW-1]] {
                if { [winfo class $item] == "Entry"  || [winfo class $item] == "TEntry" } {
                    #assumed that it is the only entry of this row
                    set entry $item
                    break
                }
            }
            if { $entry != "" } {
                #set width [$entry cget -width]
                set width 9
                set tkwidgedprivzone($QUESTION) [ttk::combobox $PARENT.ccatchment$QUESTION \
                        -textvariable tkwidgedprivzone($QUESTION,catchmentname) \
                        -values [Groundwater::GetCatchment] \
                        -width $width]

                grid remove $entry
                grid $tkwidgedprivzone($QUESTION) -row [expr $ROW-1] -column 1 -sticky ew -columnspan 2
            }
        }
        SYNC {
            lassign $args GDN STRUCT QUESTION
            if { [info exists tkwidgedprivzone($QUESTION,catchmentname)] } {
                DWLocalSetValue $GDN $STRUCT $QUESTION $tkwidgedprivzone($QUESTION,catchmentname)
            }
        }
        DEPEND {
            #set GDN [lindex $args 0]
            #set STRUCT [lindex $args 1]
            set QUESTION [lindex $args 2]
            set ACTION [lindex $args 3]
            #set value [lindex $args 4]
            if { [info exists tkwidgedprivzone($QUESTION)] && \
                [winfo exists $tkwidgedprivzone($QUESTION)] } {
                if { $ACTION == "HIDE" } {
                    grid remove $tkwidgedprivzone($QUESTION)
                } else {
                    #RESTORE
                    grid $tkwidgedprivzone($QUESTION)
                }
            } else {

            }
        }
        CLOSE {
            #array unset tkwidgedprivzone
        }
        default {
            return [list ERROR [_ "Unexpected tkwidget event"]]
        }
    }
    return ""
}


proc IberUtils::TkwidgetGetGreenRoofname { event args } {
    global tkwidgedprivzone
    switch $event {
        INIT {
            set PARENT [lindex $args 0]
            upvar [lindex $args 1] ROW
            set GDN [lindex $args 2]
            set STRUCT [lindex $args 3]
            set QUESTION [lindex $args 4]
            #initialize variable to current field value
            if { [DWLocalGetValue $GDN $STRUCT $QUESTION] != "" } {
                set tkwidgedprivzone($QUESTION) [DWLocalGetValue $GDN $STRUCT $QUESTION]
            } else {
                set tkwidgedprivzone($QUESTION) [lindex [
                                
                                
                                GetGreenRoof] 0]
            }
            
            #set entry $PARENT.e$ROW
            set entry ""
            foreach item [grid slaves $PARENT -row [expr $ROW-1]] {
                if { [winfo class $item] == "Entry"  || [winfo class $item] == "TEntry" } {
                    #assumed that it is the only entry of this row
                    set entry $item
                    break
                }
            }      
            if { $entry != "" } {                
                #set width [$entry cget -width]
                set width 9        
                set tkwidgedprivzone($QUESTION) [ttk::combobox $PARENT.cgreenroof$QUESTION \
                        -textvariable tkwidgedprivzone($QUESTION,greenroofname) \
                        -values [GreenRoofs::GetGreenRoof] \
                        -width $width]
                
                grid remove $entry
                grid $tkwidgedprivzone($QUESTION) -row [expr $ROW-1] -column 1 -sticky ew -columnspan 2
            }
        }
        SYNC {         
            lassign $args GDN STRUCT QUESTION            
            if { [info exists tkwidgedprivzone($QUESTION,greenroofname)] } {        
                DWLocalSetValue $GDN $STRUCT $QUESTION $tkwidgedprivzone($QUESTION,greenroofname)
            }
        }
        DEPEND {            
            #set GDN [lindex $args 0]
            #set STRUCT [lindex $args 1]
            set QUESTION [lindex $args 2]
            set ACTION [lindex $args 3]
            #set value [lindex $args 4]
            if { [info exists tkwidgedprivzone($QUESTION)] && \
                [winfo exists $tkwidgedprivzone($QUESTION)] } {
                if { $ACTION == "HIDE" } {
                    grid remove $tkwidgedprivzone($QUESTION)
                } else {
                    #RESTORE
                    grid $tkwidgedprivzone($QUESTION)
                }
            } else {
                
            }
        }
        CLOSE {
            #array unset tkwidgedprivzone
        }
        default {
            return [list ERROR [_ "Unexpected tkwidget event"]]
        }
    }
    return ""
}

proc IberUtils::TkwidgetGetCropname { event args } {
    global tkwidgedprivcrop
    switch $event {
        INIT {
            set PARENT [lindex $args 0]
            upvar [lindex $args 1] ROW
            set GDN [lindex $args 2]
            set STRUCT [lindex $args 3]
            set QUESTION [lindex $args 4]
            #initialize variable to current field value
            if { [DWLocalGetValue $GDN $STRUCT $QUESTION] != "" } {
                set tkwidgedprivcrop($QUESTION) [DWLocalGetValue $GDN $STRUCT $QUESTION]
            } else {
                set tkwidgedprivcrop($QUESTION) [lindex [SoilWaterStress::GetCrops] 0]
            }

            #set entry $PARENT.e$ROW
            set entry ""
            foreach item [grid slaves $PARENT -row [expr $ROW-1]] {
                if { [winfo class $item] == "Entry"  || [winfo class $item] == "TEntry" } {
                    #assumed that it is the only entry of this row
                    set entry $item
                    break
                }
            }
            if { $entry != "" } {
                #set width [$entry cget -width]
                set width 9
                set tkwidgedprivcrop($QUESTION) [ttk::combobox $PARENT.ccrop$QUESTION \
                        -textvariable tkwidgedprivcrop($QUESTION,cropname) \
                        -values [SoilWaterStress::GetCrops] \
                        -width $width]

                grid remove $entry
                grid $tkwidgedprivcrop($QUESTION) -row [expr $ROW-1] -column 1 -sticky ew -columnspan 2
            }
        }
        SYNC {
            lassign $args GDN STRUCT QUESTION
            if { [info exists tkwidgedprivcrop($QUESTION,cropname)] } {
                DWLocalSetValue $GDN $STRUCT $QUESTION $tkwidgedprivcrop($QUESTION,cropname)
            }
        }
        DEPEND {
            #set GDN [lindex $args 0]
            #set STRUCT [lindex $args 1]
            set QUESTION [lindex $args 2]
            set ACTION [lindex $args 3]
            #set value [lindex $args 4]
            if { [info exists tkwidgedprivcrop($QUESTION)] && \
                [winfo exists $tkwidgedprivcrop($QUESTION)] } {
                if { $ACTION == "HIDE" } {
                    grid remove $tkwidgedprivcrop($QUESTION)
                } else {
                    #RESTORE
                    grid $tkwidgedprivcrop($QUESTION)
                }
            } else {

            }
        }
        CLOSE {
            #array unset tkwidgedprivcrop
        }
        default {
            return [list ERROR [_ "Unexpected tkwidget event"]]
        }
    }
    return ""
}

proc IberUtils::TkwidgetGetMixturename { event args } {
    global tkwidgedprivmixture
    switch $event {
        INIT {
            set PARENT [lindex $args 0]
            upvar [lindex $args 1] ROW
            set GDN [lindex $args 2]
            set STRUCT [lindex $args 3]
            set QUESTION [lindex $args 4]
            #initialize variable to current field value
            if { [DWLocalGetValue $GDN $STRUCT $QUESTION] != "" } {
                set tkwidgedprivmixture($QUESTION) [DWLocalGetValue $GDN $STRUCT $QUESTION]
            } else {
                set tkwidgedprivmixture($QUESTION) [lindex [Mixtures::GetMixtures] 0]
            }

            #set entry $PARENT.e$ROW
            set entry ""
            foreach item [grid slaves $PARENT -row [expr $ROW-1]] {
                if { [winfo class $item] == "Entry"  || [winfo class $item] == "TEntry" } {
                    #assumed that it is the only entry of this row
                    set entry $item
                    break
                }
            }
            if { $entry != "" } {
                #set width [$entry cget -width]
                set width 9
                set tkwidgedprivmixture($QUESTION) [ttk::combobox $PARENT.cmixture$QUESTION \
                        -textvariable tkwidgedprivmixture($QUESTION,mixturename) \
                        -values [Mixtures::GetMixtures] \
                        -width $width]

                grid remove $entry
                grid $tkwidgedprivmixture($QUESTION) -row [expr $ROW-1] -column 1 -sticky ew -columnspan 2
            }
        }
        SYNC {
            lassign $args GDN STRUCT QUESTION
            if { [info exists tkwidgedprivmixture($QUESTION,mixturename)] } {
                DWLocalSetValue $GDN $STRUCT $QUESTION $tkwidgedprivmixture($QUESTION,mixturename)
            }
        }
        DEPEND {
            #set GDN [lindex $args 0]
            #set STRUCT [lindex $args 1]
            set QUESTION [lindex $args 2]
            set ACTION [lindex $args 3]
            #set value [lindex $args 4]
            if { [info exists tkwidgedprivmixture($QUESTION)] && \
                [winfo exists $tkwidgedprivmixture($QUESTION)] } {
                if { $ACTION == "HIDE" } {
                    grid remove $tkwidgedprivmixture($QUESTION)
                } else {
                    #RESTORE
                    grid $tkwidgedprivmixture($QUESTION)
                }
            } else {

            }
        }
        CLOSE {
            #array unset tkwidgedprivmixture
        }
        default {
            return [list ERROR [_ "Unexpected tkwidget event"]]
        }
    }
    return ""
}

proc IberUtils::TkwidgetGetMixturecname { event args } {
    global tkwidgedprivmixturec
    switch $event {
        INIT {
            set PARENT [lindex $args 0]
            upvar [lindex $args 1] ROW
            set GDN [lindex $args 2]
            set STRUCT [lindex $args 3]
            set QUESTION [lindex $args 4]
            #initialize variable to current field value
            if { [DWLocalGetValue $GDN $STRUCT $QUESTION] != "" } {
                set tkwidgedprivmixturec($QUESTION) [DWLocalGetValue $GDN $STRUCT $QUESTION]
            } else {
                set tkwidgedprivmixturec($QUESTION) [lindex [MixturesC::GetMixturesc] 0]
            }

            #set entry $PARENT.e$ROW
            set entry ""
            foreach item [grid slaves $PARENT -row [expr $ROW-1]] {
                if { [winfo class $item] == "Entry"  || [winfo class $item] == "TEntry" } {
                    #assumed that it is the only entry of this row
                    set entry $item
                    break
                }
            }
            if { $entry != "" } {
                #set width [$entry cget -width]
                set width 9
                set tkwidgedprivmixturec($QUESTION) [ttk::combobox $PARENT.cmixturec$QUESTION \
                        -textvariable tkwidgedprivmixturec($QUESTION,mixturecname) \
                        -values [MixturesC::GetMixturesc] \
                        -width $width]

                grid remove $entry
                grid $tkwidgedprivmixturec($QUESTION) -row [expr $ROW-1] -column 1 -sticky ew -columnspan 2
            }
        }
        SYNC {
            lassign $args GDN STRUCT QUESTION
            if { [info exists tkwidgedprivmixturec($QUESTION,mixturecname)] } {
                DWLocalSetValue $GDN $STRUCT $QUESTION $tkwidgedprivmixturec($QUESTION,mixturecname)
            }
        }
        DEPEND {
            #set GDN [lindex $args 0]
            #set STRUCT [lindex $args 1]
            set QUESTION [lindex $args 2]
            set ACTION [lindex $args 3]
            #set value [lindex $args 4]
            if { [info exists tkwidgedprivmixturec($QUESTION)] && \
                [winfo exists $tkwidgedprivmixturec($QUESTION)] } {
                if { $ACTION == "HIDE" } {
                    grid remove $tkwidgedprivmixturec($QUESTION)
                } else {
                    #RESTORE
                    grid $tkwidgedprivmixturec($QUESTION)
                }
            } else {

            }
        }
        CLOSE {
            #array unset tkwidgedprivmixturec
        }
        default {
            return [list ERROR [_ "Unexpected tkwidget event"]]
        }
    }
    return ""
}

proc DeleteSolvers { } {
    global PropPriv GidPriv
        variable solvers { Iber_CPUSolver Iber_GPUSolver Iber_RIberSolver }

        set Project [GiD_Info project ModelName]
    if { $Project == "UNNAMED" } {
        tk_dialogRAM .gid.tmpwin error \
            [= "Before simulating, a project title is needed. Save project to get it" ]\
            error 0 OK
        return
    }
    set directory $Project.gid
    if { [file pathtype $directory] == "relative" } {
        set directory [file join [pwd] $directory]
    }

        foreach item $solvers {
            set filename $item
                append filename ".dat"
                set fitxer [file join $directory $filename]
                file delete $fitxer
        }
}

proc SelectCPU {} {
    global PropPriv GidPriv
    set Project [GiD_Info project ModelName]
    if { $Project == "UNNAMED" } {
        tk_dialogRAM .gid.tmpwin error \
            [= "Before simulating, a project title is needed. Save project to get it" ]\
            error 0 OK
        return
    }
    set directory $Project.gid
    set Project [GiD_Info project ModelName]
    if { $Project == "UNNAMED" } {
        tk_dialogRAM .gid.tmpwin error \
            [= "Before simulating, a project title is needed. Save project to get it" ]\
            error 0 OK
        return
    }
    if { [file pathtype $directory] == "relative" } {
        set directory [file join [pwd] $directory]
    }

        DeleteSolvers

    set filename "Iber_CPUSolver"
    append filename ".dat"
    #set fitxer [file join $directory $filename]
    #file delete $fitxer

    set fitxer [file join $directory $filename]
    set felaux [open $fitxer w]
    set fila ""
    append fila "CPU"
    puts $felaux $fila
    close $felaux
}

proc SelectGPUPlus {} {
    global PropPriv GidPriv
    set Project [GiD_Info project ModelName]
    if { $Project == "UNNAMED" } {
        tk_dialogRAM .gid.tmpwin error \
            [= "Before simulating, a project title is needed. Save project to get it" ]\
            error 0 OK
        return
    }
    set directory $Project.gid
    set Project [GiD_Info project ModelName]
    if { $Project == "UNNAMED" } {
        tk_dialogRAM .gid.tmpwin error \
            [= "Before simulating, a project title is needed. Save project to get it" ]\
            error 0 OK
        return
    }
    if { [file pathtype $directory] == "relative" } {
        set directory [file join [pwd] $directory]
    }

        DeleteSolvers

    set filename "Iber_GPUSolver"
    append filename ".dat"
    #set fitxer [file join $directory $filename]
    #file delete $fitxer

    set fitxer [file join $directory $filename]
    set felaux [open $fitxer w]
    set fila ""
    append fila "GPU"
    puts $felaux $fila
    close $felaux
}

proc SelectGPUCDX {} {
    global PropPriv GidPriv
    set Project [GiD_Info project ModelName]
    if { $Project == "UNNAMED" } {
        tk_dialogRAM .gid.tmpwin error \
            [= "Before simulating, a project title is needed. Save project to get it" ]\
            error 0 OK
        return
    }
    set directory $Project.gid
    set Project [GiD_Info project ModelName]
    if { $Project == "UNNAMED" } {
        tk_dialogRAM .gid.tmpwin error \
            [= "Before simulating, a project title is needed. Save project to get it" ]\
            error 0 OK
        return
    }
    if { [file pathtype $directory] == "relative" } {
        set directory [file join [pwd] $directory]
    }

        DeleteSolvers

    set filename "Iber_RIberSolver"
    append filename ".dat"
    #set fitxer [file join $directory $filename]
    #file delete $fitxer

    set fitxer [file join $directory $filename]
    set felaux [open $fitxer w]
    set fila ""
        set GPUnumber [GiD_AccessValue get gendata GPU_id]
    append fila $GPUnumber
    puts $felaux $fila
    close $felaux
}

proc XYPointornodeCoords { i text } {
    variable p${i}
    set p [GidUtils::GetCoordinates $text]
    if { $p != "" } {
        foreach axis {0 1} {
            set p${i}($axis) [lindex $p $axis]
        }
    }
}

proc IberUtils::TkwidgetPickPointOrNodeCoord { event args } {
    global tkwidgedprivpicknodebuttons
    switch $event {
        INIT {
            lassign $args PARENT current_row_variable GDN STRUCT QUESTION
            upvar $current_row_variable ROW
            set entry ""
            set entry_gridded 0
            foreach item [grid slaves $PARENT -row [expr $ROW-1]] {
                if { [winfo class $item] == "Entry"  || [winfo class $item] == "TEntry" } {
                    #assumed that it is the only entry of this row
                    set entry $item
                    set entry_gridded 1
                    break
                }
            }
            if { $entry == "" } {
                set entry [GidUtils::DarkTrickTryFindUngriddedEntry $PARENT $QUESTION]
            }
            if { $entry != "" } {
                set tkwidgedprivpicknodebuttons($QUESTION) [ttk::button $PARENT.bpicknode$QUESTION \
                        -image [gid_themes::GetImage "point.png" small_icons] \
                        -command [list IberUtils::_PickCoordinates $entry]]
                grid $tkwidgedprivpicknodebuttons($QUESTION) -row [expr $ROW-1] -column 2 -sticky w
                grid configure $entry -sticky ew
                if { !$entry_gridded } {
                    grid remove $entry
                    grid remove $tkwidgedprivpicknodebuttons($QUESTION)
                }
            }
            return ""
        }
        SYNC {
            #lassign $args GDN STRUCT QUESTION
            #DWLocalSetValue $GDN $STRUCT $QUESTION $value
        }
        DEPEND {
            lassign $args GDN STRUCT QUESTION ACTION VALUE
            if { [info exists tkwidgedprivpicknodebuttons($QUESTION)] && \
                [winfo exists $tkwidgedprivpicknodebuttons($QUESTION)] } {
                if { $ACTION == "HIDE" } {
                    grid remove $tkwidgedprivpicknodebuttons($QUESTION)
                } else {
                    #RESTORE
                    grid $tkwidgedprivpicknodebuttons($QUESTION)
                }
            }
        }
        CLOSE {
            array unset tkwidgedprivpicknodebuttons
        }
        default {
            return [list ERROR [_ "Unexpected tkwidget event"]]
        }
    }
    #a tkwidget procedure must return "" if Ok or [list ERROR $description] or [list WARNING $description]
    return ""
}

# store global coordinates
proc IberUtils::_PickCoordinates { entry } {
    set xyz [GidUtils::GetCoordinates [_ "Pick point or node"] Join]
    if { $xyz != "" } {
        lassign $xyz x y z
        set res [list [format %f $x] [format %f $y] [format %f $z]]
        $entry delete 0 end
        $entry insert end $res
    }
}

proc IberUtils::TkwidgetPickPointOrNodeCoordCond { event args } {  ; # Gonzaloalen 05/2021
    global tkwidgedprivpicknodebuttons
    switch $event {
        INIT {
            lassign $args PARENT current_row_variable GDN STRUCT QUESTION
            upvar $current_row_variable ROW
            set entry ""
            set entry_gridded 0
            foreach item [grid slaves $PARENT -row [expr $ROW-1]] {
                if { [winfo class $item] == "Entry"  || [winfo class $item] == "TEntry" } {
                    #assumed that it is the only entry of this row
                    set entry $item
                    set entry_gridded 1
                    break
                }
            }
            if { $entry == "" } {
                set entry [GidUtils::DarkTrickTryFindUngriddedEntry $PARENT $QUESTION]
            }
            if { $entry != "" } {
                set tkwidgedprivpicknodebuttons($QUESTION) [ttk::button $PARENT.bpicknode$QUESTION \
                        -image [gid_themes::GetImage "point.png" small_icons] \
                        -command [list IberUtils::_PickCoordinatesCond $entry]]
                grid $tkwidgedprivpicknodebuttons($QUESTION) -row [expr $ROW-1] -column 2 -sticky w
                grid configure $entry -sticky ew
                if { !$entry_gridded } {
                    grid remove $entry
                    grid remove $tkwidgedprivpicknodebuttons($QUESTION)
                }
            }
            return ""
        }
        SYNC {
            #lassign $args GDN STRUCT QUESTION
            #DWLocalSetValue $GDN $STRUCT $QUESTION $value
        }
        DEPEND {
            lassign $args GDN STRUCT QUESTION ACTION VALUE
            if { [info exists tkwidgedprivpicknodebuttons($QUESTION)] && \
                [winfo exists $tkwidgedprivpicknodebuttons($QUESTION)] } {
                if { $ACTION == "HIDE" } {
                    grid remove $tkwidgedprivpicknodebuttons($QUESTION)
                } else {
                    #RESTORE
                    grid $tkwidgedprivpicknodebuttons($QUESTION)
                }
            }
        }
        CLOSE {
            array unset tkwidgedprivpicknodebuttons
        }
        default {
            return [list ERROR [_ "Unexpected tkwidget event"]]
        }
    }
    #a tkwidget procedure must return "" if Ok or [list ERROR $description] or [list WARNING $description]
    return ""
}

# store global coordinates
proc IberUtils::_PickCoordinatesCond { entry } { ; # Gonzaloalen 05/2021
  set view_mode [GiD_Info Project ViewMode]
  if { $view_mode == "GEOMETRYUSE" } {
      set xyz [GidUtils::GetCoordinates [_ "Pick point into an surface"] PointInSurface GEOMETRYUSE]
      #set surface_id $::GidPriv(selection)
  } elseif { $view_mode == "MESHUSE"} {
      set xyz [GidUtils::GetCoordinates [_ "Pick point into an element"] PointInElement MESHUSE]
      #set element_id $::GidPriv(selection)
  } else {
      set xyz ""
  }
    if { $xyz != "" } {
        lassign $xyz x y z
        set res [list [format %f $x] [format %f $y] [format %f $z]]
        $entry delete 0 end
        $entry insert end $res
    }
}


proc IberUtils::WriteCalculationFileCoord { coord number } { ; # Gonzaloalen 05/2021
    # Write each of the coordinates separately
    # Variable coord includes the coordinates values separated by white-spaces
    # Variable number selects the coordinate to write:
    #   1 -> Coordinate X
    #   2 -> Coordinate Y
    #   3 -> Coordinate Z

    set res ""
    set i 0
    set coordinates [split $coord]
    foreach line $coordinates {
      incr i
      if { $number == $i } { ; # number=1/2/3 -> Coordinate X/Y/Z
        append res $line
      }
    }
    return $res
}


proc IberUtils::SelectRainRastersDirectory { event args } {
    global tkwidgedprivfilenamebutton
    switch $event {
        INIT {
            lassign $args PARENT current_row_variable GDN STRUCT QUESTION
            upvar $current_row_variable ROW
            set tkwidgedprivfilenamebutton($QUESTION,filename) [DWLocalGetValue $GDN $STRUCT $QUESTION]
            set entry ""
            foreach item [grid slaves $PARENT -row [expr $ROW-1]] {
                if { [winfo class $item] == "Entry"  || [winfo class $item] == "TEntry" } {
                    set entry $item
                    break
                }
            }        
            if { [lindex [info level 2] 0] == "DWUpdateConds" } {
                set values [lrange [lindex [info level 2] 2] 3 end]
                set index_field [LabelField $GDN $STRUCT $QUESTION]
                set value [lindex $values $index_field-1]
                set tkwidgedprivfilenamebutton($QUESTION,filename) $value
            }
            set w [ttk::frame $PARENT.cfilenamebutton$QUESTION]
            ttk::entry $w.e1 -textvariable tkwidgedprivfilenamebutton($QUESTION,filename)
            ttk::button $w.b1 -image [gid_themes::GetImage "folder.png"] \
                -command [list IberUtils::WorkDirRasters tkwidgedprivfilenamebutton($QUESTION,filename) $w.e1 0]
            set tkwidgedprivfilenamebutton($QUESTION,widget) $w
            grid $w.e1 $w.b1 -sticky ew
            grid columnconfigure $w {0} -weight 1
            grid $w -row [expr $ROW-1] -column 1 -sticky ew
            if { $entry != "" } {
                grid remove $entry
            } else {
                grid remove $w
            }
        }
        SYNC {
            lassign $args GDN STRUCT QUESTION
            if { [info exists tkwidgedprivfilenamebutton($QUESTION,filename)] } {
                DWLocalSetValue $GDN $STRUCT $QUESTION $tkwidgedprivfilenamebutton($QUESTION,filename)
            }
        }
        DEPEND {
            lassign $args GDN STRUCT QUESTION ACTION VALUE
            if { [info exists tkwidgedprivfilenamebutton($QUESTION,widget)] && \
                [winfo exists $tkwidgedprivfilenamebutton($QUESTION,widget)] } {
                if { $ACTION == "HIDE" } {
                    grid remove $tkwidgedprivfilenamebutton($QUESTION,widget)
                } else {
                    grid $tkwidgedprivfilenamebutton($QUESTION,widget)
                }
            } else {

            }
        }
        CLOSE {
            array unset tkwidgedprivfilenamebutton
        }
        default {
            return [list ERROR [_ "Unexpected tkwidget event"]]
        }
    }
    return ""
}


proc IberUtils::WorkDirRasters { varname entry {tail 0}} {
    set types [list [list [_ "All files"] ".*"]]
    set defaultextension ""
    set title [_ "Select file"]
    set current_value [tk_chooseDirectory -title [= "Choose rain rasters directory"]]
    if { $tail } {
        set current_value [file tail $current_value]
    }
    if { $current_value != "" && $entry != "" && [winfo exists $entry] } {
        $entry delete 0 end
        $entry insert end $current_value
    }
    set ::$varname $current_value
    return $current_value
}


proc IberUtils::TkwidgetImportConditionFromRaster { } {
	#do nothing
}