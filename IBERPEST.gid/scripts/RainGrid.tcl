# RainGrid.tcl  -*- TCL -*-
# Geor 1
#---------------------------------------------------------------------------
#This file is written in TCL lenguage 
#For more information about TCL look at: http://www.sunlabs.com/research/tcl/
#
#For more information about GID internals, check the program scripts.
#---------------------------------------------------------------------------


proc PropWin { } {
    global PropPriv  
    
    
    set PropPriv(idata) ""
    set w .gid.properties
    set ImportPref 2
    
    # ventana princ
    InitWindow $w [= "Assign Properties"] PrePropertiesWindowGeom PropWin
    
    # widget
    frame $w.frmSelection -bd 2 -relief groove
    radiobutton $w.frmSelection.aut -text [= "Automatic Assignation"] -variable PropPriv(ImportPref) -value 1
    tk_optionMenu $w.frmSelection.file PropPriv(FileType) [= "ASCII Grid File"] [= "RAMFlood dbase File"]
    
    #        radiobutton $w.frmSelection.man -text "Manual Assignation" -variable PropPriv(ImportPref) -value 2
    
    
    set def_back [$w cget -background]
    frame $w.frmButtons -bg [CCColorActivo $def_back]
    button $w.frmButtons.btnAccept -text [= "Accept"] -command "PropSelec $w" -width 6
    button $w.frmButtons.btnClose -text [= "Close"] -command "destroy $w" -width 6
    
    
    # empaquetamiento
    grid $w.frmSelection -padx 5 -pady 5 -sticky news
    #        grid $w.frmSelection.man 
    grid $w.frmSelection.aut $w.frmSelection.file
    grid columnconf $w.frmSelection 1 -weight 1
    
    grid $w.frmButtons -sticky ews -columnspan 7
    grid $w.frmButtons.btnAccept $w.frmButtons.btnClose -padx 5 -pady 6
    
    grid columnconf $w "0" -weight 1
    grid rowconfigure $w "0" -weight 1
    
    focus $w.frmButtons.btnAccept
}

proc PropSelec {w } {
    global PropPriv
    
    if {$PropPriv(ImportPref) == 1 & $PropPriv(FileType) == [= "ASCII Grid File"] } {
        PropDefImp $w
    } elseif {$PropPriv(ImportPref) == 1 & $PropPriv(FileType) == [= "RAMFlood dbase File"] } {
        PropRAMImp $w
        #        } elseif {$PropPriv(ImportPref) == 2 } {
        #            GidOpenMaterials Properties
    }
    destroy $w
}

proc  PropRAMImp  { w } {
    global PropPriv GidPriv
    
    destroy $w
    
    # ubicaci�n de *.bas
    set basfile [file join $::IberPriv(dir) Templates Mesh.bas]
    
    # directorio de trabajo
    set Project [GiD_Info Project ModelName]        
    
    if { [file extension $Project] == ".gid" } {
        set Project [file root $Project]
    }
    if { $Project == "UNNAMED" } {
        tk_dialogRAM .gid.tmpwin error \
            [= "Before create section, a project title is needed. Save project to get it"] \
            error 0 OK
        return
    }
    set directory $Project.gid
    if { [file pathtype $directory] == "relative" } {
        set directory [file join [pwd] $directory]
    }
    set adress [file join $directory MESH.msh]
    
    # escribe el archivo con la informaci�n de la malla 
    GiD_Process Mescape Files WriteForBAS $basfile $adress escape
    
    # ubicaci�n de la base de datos
    set filename [Browser-ramR file read .gid [= "Read Properties file"] \
            {} {{{} {.txt}} {{} {.asc}} {{All files} {.*}}} ]
    if {$filename == "" } {
        return
    }
    
    # lee la base datos para enviarsela al "buscador"
    set dbf [open $filename r]
    gets $dbf bb
    lappend PropPriv(idata) [lindex $bb 0] [lindex $bb 0] "4.00"
    close $dbf
    
    set dbf [open $filename r]
    while { ![eof $dbf] } {
        gets $dbf aa
        lappend dbflist "$aa"
    }
    
    set PropPriv(clist) $dbflist
    set long [llength $dbflist]
    
    lappend PropPriv(idata) $long
    
    foreach e $PropPriv(clist) {
        bind $e {PropAssignInvoke}
    }
    foreach d $PropPriv(idata) { 
        bind $d {PropAssignInvoke} 
    }
    close $dbf
    
    #
    #Archivo que contiene las lista de clases
    # ID;Class_Name
    # n�int;name
    
    set bas [file root $filename]
    set dbas $bas.csv
    set dbfs [open $dbas r]
    
    gets $dbfs bb
    while {![eof $dbfs] } {
        gets $dbfs bb
        set clist [split $bb ";"]
        set indx [lindex $clist 0]
        set clas($indx) [lindex $clist 1]
        #             lappend PropPriv(class) [lindex $clist 1]
    }
    close $dbfs
    
    # escribe la ubicaci�n en el archivo de informaci�n
    set f [open $adress a]
    puts $f \"$filename\"
    puts $f \"$directory\"
    puts $f $PropPriv(idata)
    close $f
    
    # llama al proc que ejecuta el "buscador"
    ::GidUtils::DisableGraphics
    ::GidUtils::DisableWarnLine
    ::GidUtils::WaitState .gid
    
    PropAssignInvoke $directory
    
    # ubicaci�n del archivo con la lista elem vs. Manning        
    set adress2 [file join $directory clases.txt]
    
    set fin [open $adress2 r]
    set ncont [llength $fin]
    set GidPriv(StopLoading) 0
    AdvanceBar 0 1 0 [= "Assigning Properties"]  [= "Percentage"]:
    set dn [expr int($ncont/20)]
    if { $dn < 1 } { set dn 1 }
    
    # asigna el manning a cada elemento
    set icont 0
    while { ![eof $fin] } {
        incr icont
        gets $fin aa
        
        set idc [lindex $aa 1]
        
        set mat $clas($idc)
        GiD_AssignData material $mat elements [lindex $aa 0]
        
        if { ![expr $icont%$dn] } { 
            set percent [expr int(100.0*$icont/$ncont)]
            AdvanceBar $percent 100 $percent
            if { $GidPriv(StopLoading) } break
        }
        
    }
    
    close $fin
    
    ::GidUtils::EnableGraphics
    ::GidUtils::EnableWarnLine
    ::GidUtils::EndWaitState .gid
    
    #        if { $GidPriv(StopLoading) } {
        #            ::GidUtils::SetWarnLine [= "Stop at user demand"]
        #            WarnWin [= "Stop at user demand"]
        #        } else {
        #            ::GidUtils::SetWarnLine [= "File '%s' Read" $filename]
        #        }
    #        AdvanceBar 100 100 100
    
    # pregunta si quiere dibujar o no
    set index [tk_dialogRAM .gid.tmpwin [= "Manning Assigned"] \
            [= "Manning Coefficient was succesfully assigned to mesh elements. Do you want to draw it?"] question 0 yes no]
    
    if {$index == 0} {
        GidOpenMaterials Properties
    }
}        


proc PropDefImp { w } {
    global PropPriv GidPriv        
    destroy $w
    
    # ubicaci�n de *.bas
    set basfile [file join $::IberPriv(dir) Templates Mesh.bas]
    
    # directorio de trabajo
    set Project [GiD_Info Project ModelName]        
    if { [file extension $Project] == ".gid" } {
        set Project [file root $Project]
    }
    if { $Project == "UNNAMED" } {
        tk_dialogRAM .gid.tmpwin error \
            [= "Before create section, a project title is needed. Save project to get it"] \
            error 0 OK
        return
    }
    set directory $Project.gid
    if { [file pathtype $directory] == "relative" } {
        set directory [file join [pwd] $directory]
    }
    set adress [file join $directory MESH.msh]
    
    # escribe el archivo con la informaci�n de la malla
    
    GiD_Process Mescape Files WriteForBAS $basfile $adress escape
    
    # ubicaci�n de la base de datos
    set filename [Browser-ramR file read .gid [= "Read Definiens file"] \
            {} {{{} {.txt}} {{} {.asc}} {{All files} {.*}}} ]
    if {$filename == "" } {
        return
    }
    #
    #Archivo que contiene las lista de clases
    # ID;Class_Name
    # n�int;name
    
    set bas [file root $filename]
    set dbas $bas.csv
    set dbf [open $dbas r]
    
    gets $dbf bb
    while {![eof $dbf] } {
        gets $dbf bb
        set clist [split $bb ";"]
        set indx [lindex $clist 0]
        set clas($indx) [lindex $clist 1]
        #             lappend PropPriv(class) [lindex $clist 1]
    }
    close $dbf
    
    
    # controla que desde que tipo de archivo leer� la informaci�n de usos de suelo
    
    PropAscTranslator $filename $directory
    set filename [file join $directory Manlist.ram]
    
    
    # escribe la ubicaci�n en el archivo de informaci�n
    set f [open $adress a]
    puts $f \"$filename\"
    puts $f \"$directory\"
    puts $f $PropPriv(idata)
    close $f
    
    # llama al proc que ejecuta el "buscador"
    
    ::GidUtils::DisableGraphics
    ::GidUtils::DisableWarnLine
    ::GidUtils::WaitState .gid
    
    PropAssignInvoke $directory 
    
    # ubicaci�n del archivo con la lista elem vs. Manning        
    set adress2 [file join $directory clases.txt]
    
    # abre el archivo clases
    set fin [open $adress2 r]
    set ncont [llength $fin]
    set GidPriv(StopLoading) 0
    AdvanceBar 0 1 0 [= "Assigning Properties"]  [= "Percentage"]:
    set dn [expr int($ncont/20)]
    if { $dn < 1 } { set dn 1 }
    
    # asigna el manning a cada elemento
    set icont 0
    while { ![eof $fin] } {
        incr icont
        gets $fin aa
        #            set idc [expr int([lindex $aa 1]-1)]
        set idc [lindex $aa 1]
        #            set mat [lindex $PropPriv(class) $idc]
        set mat $clas($idc)
        GiD_AssignData material $mat elements [lindex $aa 0]
        
        if { ![expr $icont%$dn] } { 
            set percent [expr int(100.0*$icont/$ncont)]
            AdvanceBar $percent 100 $percent
            if { $GidPriv(StopLoading) } break
        }
        
    }
    
    close $fin
    ::GidUtils::EnableGraphics
    ::GidUtils::EnableWarnLine
    ::GidUtils::EndWaitState .gid
    
    if { $GidPriv(StopLoading) } {
        ::GidUtils::SetWarnLine [= "Stop at user demand"]
        WarnWin [= "Stop at user demand"]
    } else {
        ::GidUtils::SetWarnLine [= "File '%s' Read" $filename]
    }
    AdvanceBar 100 100 100
    
    # pregunta si quiere dibujar o no
    set index [tk_dialogRAM .gid.tmpwin [= "Manning Assigned"] \
            [= "Manning Coefficient was succesfully assigned to mesh elements. Do you want to draw it?"] question 0 yes no]
    
    if {$index == 0} {
        GidOpenMaterials Properties
    }
}        

proc PropAscTranslator { files directory } {
    global GidPriv PropPriv
    
    set filename [file join $directory Manlist.ram]
    set cfile [open $filename w]
    
    # Archivo que contiene la matriz de clasificaci�n de usos de suelo
    #NCOLS       <n�entero> - n� de columnas
    #NROWS       <n�entero> - n� de filas 
    #XLLCORNER   <n�real>   - coord x del v�rtice inferior izquierdo
    #YLLCORNER   <n�real>   - coord y del v�rtice inferior izquierdo
    #CELLSIZE    <n�real>   - paso
    #NODATA_VALUE <n�real>  - valor que indica la ausensia de datos
    #cada fila representa puntos con igual Y
    #cada columna representa puntos con igual X
    #<ID Class 0> <ID Class 1> ... <ID Class NCOLS-1> 
    # ...
    #<ID Class (NROWS-1)*NCOLS+0> ... <ID Class NCOLS*NROWS-1>
    
    set fin [open $files r]
    gets $fin aa
    set DTM(ncols) [lindex $aa 1]
    gets $fin aa
    set DTM(nrows) [lindex $aa 1]
    gets $fin aa
    set DTM(xllcorner) [lindex $aa 1]
    gets $fin aa
    set DTM(yllcorner) [lindex $aa 1]
    gets $fin aa
    set DTM(cellsize) [lindex $aa 1]
    gets $fin aa
    set DTM(nondatavalue) [lindex $aa 1]
    
    ::GidUtils::DisableGraphics
    ::GidUtils::DisableWarnLine
    ::GidUtils::WaitState .gid
    
    set GidPriv(StopLoading) 0
    AdvanceBar 0 1 0 [= "Reading Class Info"]  [= "Percentage"]:
    
    set njumpy 1
    set njumpx 1
    # cambios para que se dibuje en las coordenadas reales
    set xini [expr int($DTM(xllcorner))]                                
    set xfin [expr int($DTM(xllcorner)+(($DTM(ncols)-1)*$DTM(cellsize)))]  
    set yini [expr int($DTM(yllcorner))]                                
    set yfin [expr int($DTM(yllcorner)+(($DTM(nrows)-1)*$DTM(cellsize)))]  
    
    lappend PropPriv(idata) $xini $yini $DTM(cellsize) $DTM(ncols) $DTM(nrows)
    
    set pasoi [expr int($DTM(cellsize)*-1)]
    set ikont 0
    for {set i $yfin} {$i >= $yini} {incr i $pasoi} {                      
        incr ikont
        set percent [expr int($ikont/($DTM(nrows)-1.0)*100)]
        AdvanceBar $percent 100 $percent
        if { $GidPriv(StopLoading) } break
        
        gets $fin aa        
        #########
        #not jump the first and last curves
        if { $i != $yini &&  $i != $yfin } {                                
            incr contlocaly
            #jump njumpx internal curves
            if { $contlocaly < $njumpy } continue
        }
        set contlocaly 0
        # ############
        set y $i                                        
        
        set pasoj [expr int($DTM(cellsize))]
        set jkont 0
        
        for {set j $xini} {$j <= $xfin} {incr j $pasoj} {
            set clas [lindex $aa $jkont]
            incr jkont
            # ###########
            #not jump the first and last points
            if { $j != $xini &&  $j != $xfin } {
                incr contlocalx
                #jump njumpy internal points
                if { $contlocalx < $njumpx } continue
            }
            set contlocalx 0                
            #############
            if { $clas == $DTM(nondatavalue) } continue
            set x $j
            #                lappend cclist [list $x $y $clas]
            puts $cfile "$x $y $clas"
        }
    }
    
    ::GidUtils::EnableGraphics
    ::GidUtils::EnableWarnLine
    ::GidUtils::EndWaitState .gid
    
    if { $GidPriv(StopLoading) } {
        ::GidUtils::EnableGraphics
        ::GidUtils::EnableWarnLine
        ::GidUtils::EndWaitState .gid
        ::GidUtils::SetWarnLine [= "Stop at user demand"]
        WarnWin [= "Stop at user demand"]
    } else {
        ::GidUtils::EnableGraphics
        ::GidUtils::EnableWarnLine
        ::GidUtils::EndWaitState .gid
        ::GidUtils::SetWarnLine [= "File '%s' Read" $files]
    }
    AdvanceBar 100 100 100
    
    close $cfile
    close $fin
    
}

proc PropAssignInvoke {dir} {
    global PropPriv
    
    set program [Iber::GetFullPathExe MatImp.exe]
    set fil [open |\"$program\" r+]
    puts $fil \"$dir\"
    
    
    flush $fil
    gets $fil fin
    
    tk_dialogRAM .gid.tmpwin [= "Soil Ocupation"] \
        [= "Ocupation Coefficient read"] \
        info 0 OK
    
}
