
namespace eval SourceSink {
    variable data ;#array of values
    variable sourcesinks ;#list of names of data
    variable current_sourcesink ;#name of current selected sourcesink
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {visible location type table_value}
    variable fields_defaults {1 {} 1 {0.0 0.0}}  
    variable drawopengl
    variable table ;#tktable widget
}

proc SourceSink::UnsetVariables { } {
    variable data
    variable sourcesinks      
    unset -nocomplain data
    unset -nocomplain sourcesinks
}

proc SourceSink::SetDefaultValues { } {    
    variable data
    variable sourcesinks
    variable current_sourcesink
    variable current_value
    variable fields
    variable fields_defaults
    array unset data
    set sourcesinks {}
    foreach field $fields  {
        set current_value($field) ""
    }
    set current_value(table_value) {0.0 0.0}
    set current_sourcesink {}
}

#store the sourcesinks information in a hidden field of the problem data
#then this data is saved with the model without do nothing more
proc SourceSink::FillTclDataFromProblemData { } {
    variable data
    variable sourcesinks
    if { [catch {set x [GiD_AccessValue get gendata SourceSinksData]} msg] } {
        #reading a model created by an old version of the problemtype this gendata doesn't exists
        return
    }    
    array set data $x
    
    set sourcesinks [list]
    foreach item [array names data *,location] {
        lappend sourcesinks [string range $item 0 end-9]
    }
    set sourcesinks [lsort -dictionary $sourcesinks]
    SourceSink::SetCurrentSourceSink [lindex $sourcesinks 0]
}

proc SourceSink::FillProblemDataFromTclData { } {
    variable data
    variable sourcesinks
    set x [array get data]
    GiD_AccessValue set gendata SourceSinksData $x
}

proc SourceSink::WriteCalculationFile { } {
    variable data
    set res ""       
    
    set i 0
    append res "Source or Sink"  \n
    append res [llength [SourceSink::GetSourceSinks]] \n
    foreach sourcesink [SourceSink::GetSourceSinks] {
        incr i        
        if { $data($sourcesink,type) == 1 } {
            append res [lindex $data($sourcesink,location) 0]
            append res " "
            append res [lindex $data($sourcesink,location) 1] 
            
            set lines [expr [llength $data($sourcesink,table_value)]/2]
            append res "  $lines" 
            append res "  1"  \n
        } else {
            append res [lindex $data($sourcesink,location) 0]
            append res " "
            append res [lindex $data($sourcesink,location) 1] 
            
            set lines [expr [llength $data($sourcesink,table_value)]/2]
            append res "  $lines" 
            append res "  -1"  \n
        }
        set l [expr $lines*2]
        for {set ii 0} {$ii < $l} {set ii [expr $ii+2]} {
            append res "[lrange $data($sourcesink,table_value) $ii [expr $ii+1]] 0.0 0.0 0.0 20 0.0 0.0 0.0 0.0 0.0 0.0" \n        
        }
        
    }
    append res "End" 
    return $res
}

proc SourceSink::GetSourceSinks { } {
    variable sourcesinks
    if { ![info exists sourcesinks] } {
        return ""
    }
    return $sourcesinks
}

proc SourceSink::GetCurrentSourceSink { } {
    variable current_sourcesink
    return $current_sourcesink
}

proc SourceSink::OnChangeSelectedSourceSink { cb } {   
    SourceSink::SetCurrentSourceSink [SourceSink::GetCurrentSourceSink]
}

proc SourceSink::SetCurrentSourceSink { sourcesink } {
    variable data
    variable current_sourcesink
    variable current_value
    variable fields   
    variable table
    #fill in the current_value variables with data
    if { $sourcesink != "" } {
        foreach field $fields {
            if { ![info exists data($sourcesink,$field)] } {
                set data($sourcesink,$field) 0
            }
            set current_value($field) $data($sourcesink,$field)           
        }                
    } else {
        foreach field $fields {
            set current_value($field) ""
        }
    }
    
    #the other widgets have a linked variable that automatically update it but the table not
    if { [info exists table] && [winfo exists $table] } {
        GridData::SetData $table $current_value(table_value)
    }
    set current_sourcesink $sourcesink  
}

proc SourceSink::GetIndex { sourcesink } {
    variable sourcesinks    
    return [lsearch $sourcesinks $sourcesink]
}

proc SourceSink::Exists { sourcesink } {    
    if { [SourceSink::GetIndex $sourcesink] != -1 } {
        set exists 1
    } else {
        set exists 0
    }
    return $exists
}

proc SourceSink::GetSourceSinkAutomaticName { } {
    set basename [_ "sourcesink"]
    set i 1
    set sourcesink $basename-$i
    while { [SourceSink::Exists $sourcesink] } {        
        incr i
        set sourcesink $basename-$i        
    }
    return $sourcesink
}

proc SourceSink::NewSourceSink { cb } {
    variable sourcesinks
    variable data
    variable fields 
    variable fields_defaults
    set sourcesink [SourceSink::GetSourceSinkAutomaticName]     
    
    if { [SourceSink::Exists $sourcesink] } {
        #already exists
        return 1
    }
    foreach field $fields value $fields_defaults {
        set data($sourcesink,$field) $value
    }
    lappend sourcesinks $sourcesink   
    if { [winfo exists $cb] } {
        $cb configure -values [SourceSink::GetSourceSinks]
    }
    SourceSink::SetCurrentSourceSink $sourcesink
    return 0
}

proc SourceSink::DeleteSourceSink { cb } {
    variable sourcesinks
    variable data
    variable fields 
    variable fields_defaults
    set sourcesink [SourceSink::GetCurrentSourceSink] 
    if { ![SourceSink::Exists $sourcesink] } {
        #not exists
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete source or sink '%s'" $sourcesink] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {     
        set i [SourceSink::GetIndex $sourcesink] 
        array unset data $sourcesink,*        
        set sourcesinks [lreplace $sourcesinks $i $i]       
        if { [winfo exists $cb] } {
            $cb configure -values [SourceSink::GetSourceSinks]
        }
        SourceSink::SetCurrentSourceSink [lindex $sourcesinks 0]  
        GiD_Redraw     
    }
    return 0
}

proc SourceSink::RenameSourceSink { cb } {
    variable data
    variable sourcesinks
    variable fields
    set sourcesink [SourceSink::GetCurrentSourceSink]   
    if { ![SourceSink::Exists $sourcesink] } {
        #not exists
        return 1
    } 
    set new_name [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $sourcesink] \
            [= "Enter new name of %s '%s'" [= "source or sink"] $sourcesink] gidquestionhead "any" ""]
    if { $new_name != "--CANCEL--" } {
        foreach field $fields {
            set data($new_name,$field) $data($sourcesink,$field)
        }
        array unset data $sourcesink,*
        set i [SourceSink::GetIndex $sourcesink] 
        lset sourcesinks $i $new_name
        if { [winfo exists $cb] } {
            $cb configure -values [SourceSink::GetSourceSinks]           
        }
        SourceSink::SetCurrentSourceSink $new_name       
        GiD_Redraw 
    }
    return 0
}

#apply the values of the current window
proc SourceSink::Apply { T } {
    variable data
    variable current_value
    variable fields
    
    set sourcesink [SourceSink::GetCurrentSourceSink]    
    foreach field $fields {
        set data($sourcesink,$field) $current_value($field)
    }
    
    set data($sourcesink,table_value) [GridData::GetDataAllItems $T]
    GiD_Redraw        
}

#not SourceSink::StartDraw and SourceSink::EndDraw are not used, the Draw button is hidden (because unregister fail in GiD 11.1.5d)
#and the redraw procedure is registered when loading the problemtype, and each sourcesink is draw depending on its 'visible' variable value
proc SourceSink::StartDraw { bdraw} {
    variable drawopengl
    set drawopengl [GiD_OpenGL register SourceSink::ReDraw]
    GiD_Redraw
    $bdraw configure -text [= "Finish draw"] -command [list SourceSink::EndDraw $bdraw]
}

proc SourceSink::EndDraw { bdraw } {
    variable drawopengl
    if { [info exists drawopengl] } { 
        #unregister the callback tcl procedure to be called by GiD when redrawing
        GiD_OpenGL unregister $drawopengl
        catch {unset drawopengl}
        GiD_Redraw
        if { $bdraw != "" } {
            $bdraw configure -text [= "Draw"] -command [list SourceSink::StartDraw $bdraw]
        }
    }
}

proc SourceSink::ReDraw { } {
    foreach sourcesink [SourceSink::GetSourceSinks] {
        SourceSink::Draw $sourcesink
    }
}

proc SourceSink::Draw { sourcesink } {
    variable data
    if { $sourcesink == "" || ![info exists data($sourcesink,visible)] || !$data($sourcesink,visible) } {
        return 1
    }
    foreach field {location} {
        if { [llength $data($sourcesink,$field)] != 3 } {
            return 1
        }
        foreach v $data($sourcesink,$field) {
            if { ![string is double $v] } {
                return 1
            }
        }
    }    
    set blue {0 0 1}
    GiD_OpenGL draw -color $blue
    GiD_OpenGL draw -pointsize 5
    GiD_OpenGL draw -begin points
    GiD_OpenGL draw -vertex $data($sourcesink,location)    
    GiD_OpenGL draw -end
    
    #show label
    #increase a little z to try to draw the label over the mesh
    set p [MathUtils::VectorSum $data($sourcesink,location) {0.0 0.0 0.0001}]
    GiD_OpenGL draw -rasterpos $p
    if { $data($sourcesink,type) == 1 } {
        set txt [concat $sourcesink ([= "source"])]
    } elseif { $data($sourcesink,type) == 2 } {
        set txt [concat $sourcesink ([= "sink"])]
    } else {
        set txt [concat $sourcesink ([= "unexpected"])]
    }
    GiD_OpenGL drawtext $txt
    return 0
}

proc SourceSink::DestroySourceSinkWindow { W w } {   
    if { $W == $w } {
        SourceSink::EndDraw ""
        SourceSink::FillProblemDataFromTclData             
    }
}

proc SourceSink::OnChangeType { f } {
    variable current_value
    if { $current_value(type) == 1 } {
        #Source
        #         $f.lwidth configure -text [= "Width"]        
        #         grid $f.lheight $f.eheight
    } else {
        #Sink
        #         $f.lwidth configure -text [= "Diameter"]
        #         grid remove $f.lheight $f.eheight
    }
}

proc SourceSink::PickPointInSurfaceOrElementCmd { entry } {
    set view_mode [GiD_Info Project ViewMode]
    if { $view_mode == "GEOMETRYUSE" } {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an surface"] PointInSurface GEOMETRYUSE]
        #set surface_id $::GidPriv(selection)  
    } elseif { $view_mode == "MESHUSE"} {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an element"] PointInElement MESHUSE]
        #set element_id $::GidPriv(selection)    
    } else {
        set xyz ""
    }        
    if { $xyz != "" } {                    
        set res [format "%g %g %g" {*}$xyz]
        $entry delete 0 end
        $entry insert end $res        
    }
}

proc SourceSink::Window { } {
    variable data
    variable sourcesinks
    variable current_sourcesink
    variable current_value
    variable table
    
    if { ![info exists sourcesinks] } {
        SourceSink::SetDefaultValues
    }
    
    SourceSink::FillTclDataFromProblemData    
    
    set w .gid.sourcesink
    InitWindow $w [= "Source and Sink"] PreSourceSinkWindowGeom SourceSink::Window
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    set f [ttk::frame $w.fsourcesinks]
    
    ttk::combobox $f.cb1 -textvariable SourceSink::current_sourcesink -values [SourceSink::GetSourceSinks] -state readonly
    bind $f.cb1 <<ComboboxSelected>> [list SourceSink::OnChangeSelectedSourceSink %W]
    ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list SourceSink::NewSourceSink $f.cb1]
    GidHelp $f.bnew  [= "Create a new source or sink"]
    ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list SourceSink::DeleteSourceSink $f.cb1]
    GidHelp $f.bdel  [= "Delete a source or sink"]
    ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list SourceSink::RenameSourceSink $f.cb1]
    GidHelp $w.f.bren  [= "Rename a source or sink"]   
    grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
    grid $f -sticky new
    grid rowconfigure $f 0 -weight 1
    
    set f [ttk::frame $w.fsourcesink]        
    set field visible
    ttk::checkbutton $f.ch$field -text [= "Visible"] -variable SourceSink::current_value($field)
    grid $f.ch$field -sticky w
    
    foreach field {location} text [list [= "Location"]] {
        ttk::label $f.l$field -text $text
        ttk::entry $f.e$field -textvariable SourceSink::current_value($field)
        ttk::button $f.b$field -image [gid_themes::GetImage "point.png" small_icons] -command [list SourceSink::PickPointInSurfaceOrElementCmd $f.e$field]
        grid $f.l$field $f.e$field $f.b$field -sticky ew
    }
    
    foreach field {type} text [list [= "Type"]] values {{1 2}} labels [list [list [= "Source"] [= "Sink"]]] {
        ttk::label $f.l$field -text $text
        #ttk::combobox $f.c$field -values $values -state readonly -textvariable SourceSink::current_value($field)
        TTKComboBox $f.c$field -values $values -labels $labels -state readonly -textvariable SourceSink::current_value($field) \
            -modifycmd [list SourceSink::OnChangeType $f]
        grid $f.l$field $f.c$field -sticky ew
    }
    #     foreach field {Table} text [list [= "Table"]] {
        #         ttk::label $f.l$field -text $text
        #         ttk::entry $f.e$field -textvariable SourceSink::current_value($field)
        #         grid $f.l$field $f.e$field -sticky ew
        #     }
    
    grid $f -sticky new
    grid columnconfigure $f {1} -weight 1
    
    package require fulltktree
    set columns ""
    foreach name [list [= "Time (s)"] [= "Q (m3/s)"]] {
        lappend columns [list 4 $name left text 1]
    }
    set T [fulltktree $f.t -columns $columns -showlines 0 -showheader 1 -expand 1]    
    set table $T
    $T configure -editbeginhandler GridData::EditBegin
    $T configure -editaccepthandler [list GridData::EditAccept]
    $T configure -deletehandler GridData::RemoveRows
    $T configure -contextualhandler_menu GridData::ContextualMenu    
    $T column configure all -button 0
    
    ttk::frame $f.fbuttons
    ttk::button $f.fbuttons.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $T 1]
    GidHelp $f.fbuttons.b1 [_ "Pressing this button, one new line will be added to this window"]    
    ttk::button $f.fbuttons.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $T 1]
    GidHelp $f.fbuttons.b2 [_ "Pressing this button, one line is deleted from this window"]        
    
    ttk::button $f.fbuttons.b3 -image [gid_themes::GetImage graphs.png small_icons] -text [_ "Graph"] \
        -command [list GridData::PlotCurveWin $T [= "Source and Sink"] ""]
    GidHelp $f.fbuttons.b3 [_ "Pressing this button, a XY graph will be drawn"]
    
    grid $T -sticky nsew -columnspan 2
    grid $f.fbuttons.b1 $f.fbuttons.b2 $f.fbuttons.b3 -sticky w    
    grid $f.fbuttons -sticky ew
    
    
    
    grid columnconfigure $f {0} -weight 1
    grid rowconfigure $f {1 4} -weight 1
    grid $f -sticky nsew -padx 2 -pady 2
    GidHelp $f [= "Set Source and Sink parameters"]
    
    bind $T <<Paste>> [list ::GridData::Paste $T CLIPBOARD]
    bind $T <<PasteSelection>> [list ::GridData::Paste $T PRIMARY]
    bind $T <<Cut>> [list GridData::Cut $T]
    bind $T <<Copy>> [list GridData::Copy $T]
    bind [$T givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header
    
    GridData::SetData $T $current_value(table_value)
    
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.bapply -text [= "Apply"] -command [list SourceSink::Apply $T] -style BottomFrame.TButton
    #ttk::button $f.bdraw -text [= "Draw"] -command [list SourceSink::StartDraw $f.bdraw] -style BottomFrame.TButton
    ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    #grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
    grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }        
    
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    bind $w <Alt-c> "$f.bclose invoke"
    bind $w <Escape> "$f.bclose invoke"   
    bind $w <Destroy> [list +SourceSink::DestroySourceSinkWindow %W $w] ;# + to add to previous script  
}

proc SourceSink::CloseWindow { } {
    set w .gid.sourcesink
    if { [winfo exists $w] } {
        close $w
    }
}
