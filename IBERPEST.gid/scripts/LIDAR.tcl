################################
### create by Marcos 03/2020 ###
################################


namespace eval LIDAR {
    global fileDTMname
}

# to check it to now offer its menu (e.g linux if these third part exes are not available)
proc LIDAR::IsAvailable { } {
    set available 1
    foreach exe { laszip64.exe LAStoDEM.exe } {
        set program [Iber::GetFullPathExe $exe]
        if { ![file exists $program] } {
            set available 0
            break
        }
    }
    return $available
}

proc LIDAR::CreateLIDARWin { } {
    global LIDARPriv
    
    set w .gid.createLIDAR
    set LIDARPriv(file) ""
    set LIDARPriv(mode) "1"
    set LIDARPriv(cellsize) 10
    set LIDARPriv(type) 1    
    
    InitWindow $w [= "Read LIDAR file"] PreCreateLIDARWindowGeom LIDAR::CreateLIDARWin
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    
    #creación de los witgets
    set f [ttk::frame $w.frmParams]
    
    #Label 0.1
    ttk::label $f.ldir -text [= "LIDAR file: "]
    set e1 [ttk::entry $f.edir -textvariable LIDARPriv(file)]
    ttk::button $f.bdir -text [= "Select"] -command LIDAR::LIDARfile
    grid $f.ldir $f.edir $f.bdir -sticky w
    
    #Label 0.2
    ttk::label $f.ttrans -text [= "Transformation: "]
    set e5 [ttk::radiobutton $f.egeo -text [= "LAZ to LAS"] -variable LIDARPriv(type) -value 1]
    set e6 [ttk::radiobutton $f.emesh -text [= "LAS to DTM"] -variable LIDARPriv(type) -value 2]
    grid $f.ttrans $f.egeo $f.emesh -sticky w
    grid columnconfigure $w.frmParams {0 1 2} -weight 1
    grid $w.frmParams -sticky new -padx 2 -pady 2
    
    #Label 1.0
    ttk::labelframe $w.foptions -text [= " DTM options "]
    ttk::label $w.foptions.ttype -text [= "File type: "]
    set e2 [ttk::entry $w.foptions.enom -textvariable LIDARPriv(mode)]
    ttk::label $w.foptions.toptions -text [= "(Terrain: 1; Terrain+buildings: 2)"]
    ttk::label $w.foptions.toptionsv -text " "
    ttk::label $w.foptions.tsize -text [= "Cell size (m): "]
    set e3 [ttk::entry $w.foptions.vsize -textvariable LIDARPriv(cellsize)]
    grid $w.foptions.ttype $w.foptions.enom -sticky ew
    grid $w.foptions.toptions $w.foptions.toptionsv -sticky ew
    grid $w.foptions.tsize $w.foptions.vsize -sticky ew
    grid columnconfigure $w.foptions {0 1 2} -weight 1
    grid $w.foptions -sticky new -padx 5 -pady 5
    
    #Buttons settins
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.baccept -text [= "Execute"] -command "LIDAR::CreateLIDAR $w" -underline 0 -width 10
    ttk::button $f.bclose -text [= "Close"] -command "destroy $w" -underline 0 -width 10
    grid $f.baccept $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center } 
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    
}

proc LIDAR::LIDARfile { } {
    global LIDARPriv    
    #use of tk_getOpenFile crash on Windows 8 x64!!
    #set types {        {{Text Files} {.txt}} {{Asc Files} {.asc}} {{All Files} *} }
    #set LIDARPriv(fileD) [tk_getOpenFile -filetypes $types -title [= "Choose Original DTM file"]]   
    set types [list [list [= "LIDAR file"] ".laz .las"] [list [_ "All files"] ".*"]]
    set defaultextension .las
    if { ![info exists LIDARPriv(fileD)] } {
        set LIDARPriv(fileD) ""
    }
    set LIDARPriv(fileD) [Browser-ramR file read .gid [= "Choose LIDAR file"] $LIDARPriv(fileD) $types $defaultextension 0]       
    set LIDARPriv(file) [file tail $LIDARPriv(fileD)]
    set LIDARPriv(fDir) [file dirname $LIDARPriv(fileD)]
}

proc LIDAR::CreateLIDAR { w } {
    global LIDARPriv
    global fileDTMname
    
    # directorio de trabajo
    
    set Project [GiD_Info project ModelName]        
    #   set Project [file normalize $Project]
    
    #     if { [file extension $Project] == ".gid" } {
        #         set Project [file root $Project]
        #     }
    if { $LIDARPriv(type)== "1" } {
        
        if { $Project == "UNNAMED" } {
            tk_dialogRAM .gid.tmpwin error \
                [= "Before transform LIDARs, a project title is needed. Save project to get it"] \
                error 0 OK
            return
        }
        set directory $Project.gid
        if { [file pathtype $directory] == "relative" } {
            set directory [file join [pwd] $directory]
        }
        
        # archivo de parámetros 
        
        set fdata1 [file join $directory LIDAR1.bat]
        set fd1 [open $fdata1 w]
        
        set program1 [Iber::GetFullPathExe laszip64.exe]
        
        set title1 \"$program1\"
        append title1 " "
        append title1 \"$LIDARPriv(fileD)\"
        puts $fd1 $title1
        
        close $fd1
        exec $fdata1
        
        #destroy $w
        
        if {[file exists [file join $directory LIDAR_ERROR.log]]==1} {
            
            tk_dialogRAM .gid.tmpwin [= "LIDAR Error"] \
                [= "There is not enough memory. New LIDAR file was not created."] \
                error 0 OK
            
        } else {
            #   tkwait var $fin 
            tk_dialogRAM .gid.tmpwin [= "LIDAR info"] \
                [= "LIDAR (*.laz) has been uncompressed"] \
                info 0 OK
            
            #para que importe directamente
            set filename [file rootname $LIDARPriv(fileD)]
            append filename ".las"
            
            set LIDARPriv(fileD) [file join $filename]
            
            set LIDARPriv(file) [file tail $LIDARPriv(fileD)]
            set LIDARPriv(fDir) [file dirname $LIDARPriv(fileD)]
        }
        
        file delete $fdata1
        unset fdata1
        
    } elseif {$LIDARPriv(type)== "2"} {
        
        if { $Project == "UNNAMED" } {
            tk_dialogRAM .gid.tmpwin error \
                [= "Before transform LIDARs, a project title is needed. Save project to get it"] \
                error 0 OK
            return
        }
        set directory $Project.gid
        if { [file pathtype $directory] == "relative" } {
            set directory [file join [pwd] $directory]
        }
        
        # archivo de parámetros 
        
        set fdata2 [file join $directory LIDAR2.bat]
        set fd2 [open $fdata2 w]
        
        set program2 [Iber::GetFullPathExe LAStoDEM.exe]
        
        set title2 \"$program2\"
        append title2 " "
        append title2 \"$LIDARPriv(fileD)\"
        append title2 " "
        append title2 \"$LIDARPriv(fDir)\"
        append title2 " "
        append title2 \"$LIDARPriv(cellsize)\"
        append title2 " "
        append title2 \"$LIDARPriv(mode)\"
        puts $fd2 $title2
        
        close $fd2 
        
        exec $fdata2
        
        #destroy $w
        
        if {[file exists [file join $directory LIDAR_ERROR.log]]==1} {
            
            tk_dialogRAM .gid.tmpwin [= "LIDAR Error"] \
                [= "There is not enough memory. New LIDAR mesh file was not created."] \
                error 0 OK
            
        } else {
            
            #   tkwait var $fin 
            tk_dialogRAM .gid.tmpwin [= "LIDAR info"] \
                [= "DTM from LIDAR created"] \
                info 0 OK
            
            set index [tk_dialogRAM .gid.tmpwin [= "Mesh edition"] \
                    [= "DTM was successfully created. Do you want to set the elevation from DTM file?"] question 0 [= "Yes"] [= "No"]]
            #destroy $w
            if {$index == 0} {
                set fileDTMname [file rootname $LIDARPriv(fileD)]
                append fileDTMname ".las.txt"
                LIDARimport $fileDTMname
            }    
        }
        
        file delete $fdata2
        unset fdata2
        
    }
    
    #unset LIDARPriv   
}