###################################
### created by Georgina 05/2009 ###
### updated by Marcos 10/2021 ####
###################################

proc CreateRTINWin { } {
    global RTINPriv 
    
    set w .gid.creatertin
    
    set RTINPriv(file) " "
    set RTINPriv(tol) "0.1"
    set RTINPriv(max) 1
    set RTINPriv(min) 1
    set RTINPriv(type) 1
    
    InitWindow $w [= "Create RTIN File"] PreCreateRTINWindowGeom CreateRTINWin
    
    set f [ttk::frame $w.frmRTINParams]
    # Label 0.1
    ttk::label $f.workdirtext -text [= "Original DTM file: "]
    ttk::entry $f.workdir -textvariable RTINPriv(file)
    ttk::button $f.workdirret -text [= "Select"] -command RTINfile
    grid $f.workdirtext $f.workdir $f.workdirret -sticky w
    
    # Label 0.2
    ttk::label $f.importas -text [= "Import as: "]
    ttk::radiobutton $f.importasgeo -text [= "Geometry"] -variable RTINPriv(type) -value 1
    ttk::radiobutton $f.importasmesh -text [= "Mesh"] -variable RTINPriv(type) -value 2
    grid $f.importas $f.importasgeo $f.importasmesh -sticky w
    
    grid columnconfigure $w.frmRTINParams {0 1 2} -weight 1
    grid $w.frmRTINParams -sticky new -padx 2 -pady 2
    
    # Label 1.1
    ttk::labelframe $w.foptions -text [= " RTIN options "]
    ttk::label $w.foptions.tolerancetext -text [= "Tolerance (m) "]
    ttk::entry $w.foptions.tolerancevalue -textvariable RTINPriv(tol)
    ttk::label $w.foptions.maxsidetext -text [= "Maximum side (m) "]
    ttk::entry $w.foptions.maxsidevalue -textvariable RTINPriv(max)
    ttk::label $w.foptions.minsidetext -text [= "Minimum side (m) "]
    ttk::entry $w.foptions.minsidevalue -textvariable RTINPriv(min)
    grid $w.foptions.tolerancetext $w.foptions.tolerancevalue -sticky ew
    grid $w.foptions.maxsidetext $w.foptions.maxsidevalue -sticky ew
    grid $w.foptions.minsidetext $w.foptions.minsidevalue -sticky ew
    grid columnconfigure $w.foptions {0 1 2} -weight 1
    grid $w.foptions -sticky new -padx 5 -pady 5
    
    #grid $f -sticky new
    #grid columnconfigure $f {1} -weight 1
    
    # Buttons settins
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.btnRTIN -text [= "Accept"] -command "CreateRTIN $w" -underline 0 -width 10
    ttk::button $f.btnclose -text [= "Close"] -command "destroy $w" -underline 0 -width 10
    grid $f.btnRTIN $f.btnclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center } 
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
}

proc ImportRTINWindow { } {   
    set types [list [list [= "RTIN file"] ".dxf"] [list [_ "All files"] ".*"]]
    set defaultextension .dxf
    set filename ""
    set filename [Browser-ramR file read .gid [= "Choose RTIN file"] $filename $types $defaultextension 0]
    
    if { $filename != "" } {
        GiD_Process MEscape files DXFRead $filename escape                
        
        set index [tk_dialogRAM .gid.tmpwin [= "RTIN Imported"] \
                [= "RTIN.dxf was successfully imported. Do you want to collapse the geometry?"] question 0 [= "Yes"] [= "No"]]        
        if {$index == 0} {
            GiD_Process Mescape Utilities Collapse Lines yes escape
        }
    }
}

proc RTINfile { } {
    global RTINPriv    
    set types [list [list [= "Text file"] ".txt .asc"] [list [_ "All files"] ".*"]]
    set defaultextension .txt
    if { ![info exists RTINPriv(fileD)] } {
        set RTINPriv(fileD) ""
    }
    set RTINPriv(fileD) [Browser-ramR file read .gid [= "Choose Original DTM file"] $RTINPriv(fileD) $types $defaultextension 0]       
    set RTINPriv(file) [file tail $RTINPriv(fileD)]
    set RTINPriv(fDir) [file dirname $RTINPriv(fileD)]
}


proc CreateRTIN {w } {
    global RTINPriv
    
    # checks
    set Project [GiD_Info project ModelName]  
    if { $Project == "UNNAMED" } {
        tk_dialogRAM .gid.tmpwin error \
            [= "Before create the RTIN, a project title is needed. Save project to get it"] \
            error 0 OK
        return
    }
    set directory $Project.gid
    if { [file pathtype $directory] == "relative" } {
        set directory [file join [pwd] $directory]
    }
    if { $RTINPriv(file) == " " } {
        tk_dialogRAM .gid.tmpwin error \
            [= "Please, select a DTM file"] \
            error 0 OK
        return
    }
    set max $RTINPriv(max)
    set min $RTINPriv(min)
    set tol $RTINPriv(tol)
    if { [ expr {$max < $min} ] || [ expr {$max < 0} ] || [ expr {$min < 0} ] || [ expr {$tol < 0} ] } {
        tk_dialogRAM .gid.tmpwin error \
            [= "Wrong RTIN options. Please, check it"] \
            error 0 OK
        return
    }
    
    # parameter file
    set fdata [file join $directory RTIN.dat]
    set fd [open $fdata w]
    
    puts $fd $RTINPriv(file) 
    puts $fd $RTINPriv(tol)  
    puts $fd $RTINPriv(max) 
    puts $fd $RTINPriv(min)
    
    close $fd 
    
    if { $RTINPriv(type) == "1" } {  
        set program [Iber::GetFullPathExe rtin.exe]
        set f [open |\"$program\" r+]
        puts $f "\"$RTINPriv(fDir)\" \"$directory\""
        flush $f
        gets $f fin
        
        destroy $w
        
        if {[file exists [file join $directory RTIN_ERROR.log]]==1} {
            tk_dialogRAM .gid.tmpwin [= "RTIN Error"] \
                [= "There is not enough memory. New RTIN file was not created."] \
                error 0 OK
        } else {
            GidUtils::SetWarnLine [= "New RTIN file was created. Now it will be imported."]
            
            # Import directly
            set filename [file join $directory RTIN.dxf]
            GiD_Process MEscape files DXFRead $filename escape
            set index [tk_dialogRAM .gid.tmpwin [= "RTIN Imported"] \
                    [= "RTIN.dxf was successfully imported. Do you want to collapse the geometry?"] question 0 [= "Yes"] [= "No"]]
            
            if {$index == 0} {
                GiD_Process Mescape Utilities Collapse Lines yes escape
                set index [tk_dialogRAM .gid.tmpwin [= "Geometry meshing"] \
                        [= "Do you want to mesh the geometry?"] question 0 [= "Yes"] [= "No"]]   
                if {$index == 0} {
                    OneElementMesh
                }
            }
        }
    } elseif {$RTINPriv(type)== "2"} {
        set program [Iber::GetFullPathExe rtin_mesh.exe]
        set f [open |\"$program\" r+]
        puts $f "\"$RTINPriv(fDir)\" \"$directory\""
        flush $f
        gets $f fin
        
        destroy $w
        
        if {[file exists [file join $directory RTIN_ERROR.log]]==1} {
            tk_dialogRAM .gid.tmpwin [= "RTIN Error"] \
                [= "There is not enough memory. New RTIN mesh file was not created."] \
                error 0 OK
        } else {
            GidUtils::SetWarnLine [= "New RTIN mesh file was created. Now it will be imported."]
            
            # Importing directly
            set filename [file join $directory RTIN.msh]
            GiD_Process Mescape Files MeshRead $filename escape
            tk_dialogRAM .gid.tmpwin [= "RTIN Mesh Imported"] \
                [= "RTIN mesh file was successfully imported."] info 0 OK
        }
    }
    unset RTINPriv   
}
