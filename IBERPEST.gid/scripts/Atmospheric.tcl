
namespace eval Atmospheric {
    variable data ;#array of values
    variable atmospherics ;#list of names of data
    variable current_atmospheric ;#name of current selected discharge
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {visible location table_value}
    variable fields_defaults {1 {} {0.0 0.0}}  
    variable drawopengl
    variable table ;#tktable widget
}

proc Atmospheric::UnsetVariables { } {
    variable data
    variable atmospherics      
    unset -nocomplain data
    unset -nocomplain atmospherics
}

proc Atmospheric::SetDefaultValues { } {    
    variable data
    variable atmospherics
    variable current_atmospheric
    variable current_value
    variable fields
    variable fields_defaults
    array unset data
    set atmospherics {}
    foreach field $fields  {
        set current_value($field) ""
    }
    set current_value(table_value) {0 0.0 0.0 0.0 0.0}
    set current_atmospheric {}
}

#store the discharge information in a hidden field of the problem data
#then this data is saved with the model without do nothing more
proc Atmospheric::FillTclDataFromProblemData { } {
    variable data
    variable atmospherics
    if { [catch {set x [GiD_AccessValue get gendata DischargeData]} msg] } {
        #reading a model created by an old version of the problemtype this gendata doesn't exists
        return
    }    
    array set data $x
    
    set atmospherics [list]
    foreach item [array names data *,location] {
        lappend atmospherics [string range $item 0 end-15]
    }
    set atmospherics [lsort -dictionary $atmospherics]
    Atmospheric::SetCurrentDischarge [lindex $atmospherics 0]
}

proc Atmospheric::FillProblemDataFromTclData { } {
    variable data
    variable atmospherics
    set x [array get data]
    GiD_AccessValue set gendata DischargeData $x
}

proc Atmospheric::WriteCalculationFile { } {
    variable data
    set res ""       
    
    set i 0
    append res "Atmospheric"  \n
    append res [llength [Atmospheric::GetDischarge]] \n
    foreach discharge [Atmospheric::GetDischarge] {
        incr i        
        
        append res [lindex $data($discharge,location) 0]
        append res " "
        append res [lindex $data($discharge,location) 1] 
        set lines [expr [llength $data($discharge,table_value)]/18]
        append res "  $lines"
        append res " "
        append res "1" \n
        
        set l [expr $lines*18]
        for {set ii 0} {$ii < $l} {set ii [expr $ii+18]} {
            append res "[lrange $data($discharge,table_value) $ii [expr $ii+17]]" \n              
        }        
        
    }
    append res "End" 
    return $res
}

proc Atmospheric::GetDischarge { } {
    variable atmospherics
    if { ![info exists atmospherics] } {
        return ""
    }
    return $atmospherics
}

proc Atmospheric::GetCurrentDischarge { } {
    variable current_atmospheric
    return $current_atmospheric
}

proc Atmospheric::OnChangeSelectedDischarge { cb } {   
    Atmospheric::SetCurrentDischarge [Atmospheric::GetCurrentDischarge]
}

proc Atmospheric::SetCurrentDischarge { discharge } {
    variable data
    variable current_atmospheric
    variable current_value
    variable fields   
    variable table
    #fill in the current_value variables with data
    if { $discharge != "" } {
        foreach field $fields {
            if { ![info exists data($discharge,$field)] } {
                set data($discharge,$field) 0
            }
            set current_value($field) $data($discharge,$field)           
        }                
    } else {
        foreach field $fields {
            set current_value($field) ""
        }
    }
    
    #the other widgets have a linked variable that automatically update it but the table not
    if { [info exists table] && [winfo exists $table] } {
        GridData::SetData $table $current_value(table_value)
    }
    set current_atmospheric $discharge  
}

proc Atmospheric::GetIndex { discharge } {
    variable atmospherics    
    return [lsearch $atmospherics $discharge]
}

proc Atmospheric::Exists { discharge } {    
    if { [Atmospheric::GetIndex $discharge] != -1 } {
        set exists 1
    } else {
        set exists 0
    }
    return $exists
}

proc Atmospheric::GetDischargeAutomaticName { } {
    set basename [_ "discharge"]
    set i 1
    set discharge $basename-$i
    while { [Atmospheric::Exists $discharge] } {        
        incr i
        set discharge $basename-$i        
    }
    return $discharge
}

proc Atmospheric::NewDischarge { cb } {
    variable atmospherics
    variable data
    variable fields 
    variable fields_defaults
    set discharge [Atmospheric::GetDischargeAutomaticName]     
    
    if { [Atmospheric::Exists $discharge] } {
        #already exists
        return 1
    }
    foreach field $fields value $fields_defaults {
        set data($discharge,$field) $value
    }
    lappend atmospherics $discharge   
    if { [winfo exists $cb] } {
        $cb configure -values [Atmospheric::GetDischarge]
    }
    Atmospheric::SetCurrentDischarge $discharge
    return 0
}

proc Atmospheric::DeleteDischarge { cb } {
    variable atmospherics
    variable data
    variable fields 
    variable fields_defaults
    set discharge [Atmospheric::GetCurrentDischarge] 
    if { ![Atmospheric::Exists $discharge] } {
        #not exists
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete discharge '%s'" $discharge] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {     
        set i [Atmospheric::GetIndex $discharge] 
        array unset data $discharge,*        
        set atmospherics [lreplace $atmospherics $i $i]       
        if { [winfo exists $cb] } {
            $cb configure -values [Atmospheric::GetDischarge]
        }
        Atmospheric::SetCurrentDischarge [lindex $atmospherics 0]  
        GiD_Redraw     
    }
    return 0
}

proc Atmospheric::RenameDischarge { cb } {
    variable data
    variable atmospherics
    variable fields
    set discharge [Atmospheric::GetCurrentDischarge]   
    if { ![Atmospheric::Exists $discharge] } {
        #not exists
        return 1
    } 
    set new_name [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $discharge] \
            [= "Enter new name of %s '%s'" [= "discharge"] $discharge] gidquestionhead "any" ""]
    if { $new_name != "--CANCEL--" } {
        foreach field $fields {
            set data($new_name,$field) $data($discharge,$field)
        }
        array unset data $discharge,*
        set i [Atmospheric::GetIndex $discharge] 
        lset atmospherics $i $new_name
        if { [winfo exists $cb] } {
            $cb configure -values [Atmospheric::GetDischarge]           
        }
        Atmospheric::SetCurrentDischarge $new_name       
        GiD_Redraw 
    }
    return 0
}

#apply the values of the current window
proc Atmospheric::Apply { T } {
    variable data
    variable current_value
    variable fields
    
    set discharge [Atmospheric::GetCurrentDischarge]    
    foreach field $fields {
        set data($discharge,$field) $current_value($field)
    }
    
    set data($discharge,table_value) [GridData::GetDataAllItems $T]
    GiD_Redraw        
}

#not Atmospheric::StartDraw and Atmospheric::EndDraw are not used, the Draw button is hidden (because unregister fail in GiD 11.1.5d)
#and the redraw procedure is registered when loading the problemtype, and each discharge is draw depending on its 'visible' variable value
proc Atmospheric::StartDraw { bdraw} {
    variable drawopengl
    set drawopengl [GiD_OpenGL register Atmospheric::ReDraw]
    GiD_Redraw
    $bdraw configure -text [= "Finish draw"] -command [list Atmospheric::EndDraw $bdraw]
}

proc Atmospheric::EndDraw { bdraw } {
    variable drawopengl
    if { [info exists drawopengl] } { 
        #unregister the callback tcl procedure to be called by GiD when redrawing
        GiD_OpenGL unregister $drawopengl
        catch {unset drawopengl}
        GiD_Redraw
        if { $bdraw != "" } {
            $bdraw configure -text [= "Draw"] -command [list Atmospheric::StartDraw $bdraw]
        }
    }
}

proc Atmospheric::ReDraw { } {
    foreach discharge [Atmospheric::GetDischarge] {
        Atmospheric::Draw $discharge
    }
}

proc Atmospheric::Draw { discharge } {
    variable data
    if { $discharge == "" || ![info exists data($discharge,visible)] || !$data($discharge,visible) } {
        return 1
    }
    foreach field {location} {
        if { [llength $data($discharge,$field)] != 3 } {
            return 1
        }
        foreach v $data($discharge,$field) {
            if { ![string is double $v] } {
                return 1
            }
        }
    }    
    set blue {0 0 1}
    GiD_OpenGL draw -color $blue
    GiD_OpenGL draw -pointsize 5
    GiD_OpenGL draw -begin points
    GiD_OpenGL draw -vertex $data($discharge,location)    
    GiD_OpenGL draw -end
    
    #show label
    #increase a little z to try to draw the label over the mesh
    set p [MathUtils::VectorSum $data($discharge,location) {0.0 0.0 0.0001}]
    GiD_OpenGL draw -rasterpos $p
    
    set txt [concat $discharge ([= "discharge"])]
    
    GiD_OpenGL drawtext $txt
    return 0
}

proc Atmospheric::DestroyDischargeWindow { W w } {   
    if { $W == $w } {
        Atmospheric::EndDraw ""
        Atmospheric::FillProblemDataFromTclData             
    }
}

proc Atmospheric::OnChangeType { f } {
    variable current_value
    
}


proc Atmospheric::PickPointInSurfaceOrElementCmd { entry } {
    set view_mode [GiD_Info Project ViewMode]
    if { $view_mode == "GEOMETRYUSE" } {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an surface"] PointInSurface GEOMETRYUSE]
        #set surface_id $::GidPriv(selection)  
    } elseif { $view_mode == "MESHUSE"} {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an element"] PointInElement MESHUSE]
        #set element_id $::GidPriv(selection)    
    } else {
        set xyz ""
    }        
    if { $xyz != "" } {                    
        set res [format "%g %g %g" {*}$xyz]
        $entry delete 0 end
        $entry insert end $res        
    }
}

proc Atmospheric::Window { } {
    variable data
    variable atmospherics
    variable current_atmospheric
    variable current_value
    variable table
    
    if { ![info exists atmospherics] } {
        Atmospheric::SetDefaultValues
    }
    
    Atmospheric::FillTclDataFromProblemData    
    
    set w .gid.discharge
    InitWindow $w [= "Atmospheric"] PreDischargeWindowGeom Atmospheric::Window
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    set f [ttk::frame $w.fdischarges]
    
    ttk::combobox $f.cb1 -textvariable Atmospheric::current_atmospheric -values [Atmospheric::GetDischarge] -state readonly
    bind $f.cb1 <<ComboboxSelected>> [list Atmospheric::OnChangeSelectedDischarge %W]
    ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list Atmospheric::NewDischarge $f.cb1]
    GidHelp $f.bnew  [= "Create a new discharge"]
    ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list Atmospheric::DeleteDischarge $f.cb1]
    GidHelp $f.bdel  [= "Delete a discharge"]
    ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list Atmospheric::RenameDischarge $f.cb1]
    GidHelp $w.f.bren  [= "Rename a discharge"]   
    grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
    grid $f -sticky new
    grid rowconfigure $f 0 -weight 1
    
    set f [ttk::frame $w.fdischarge]        
    # set field visible
    # ttk::checkbutton $f.ch$field -text [= "Visible"] -variable Atmospheric::current_value($field)
    # grid $f.ch$field -sticky w
    
    # foreach field {location} text [list [= "Location"]] {
        # ttk::label $f.l$field -text $text
        # ttk::entry $f.e$field -textvariable Atmospheric::current_value($field)
        # ttk::button $f.b$field -image [gid_themes::GetImage "point.png" small_icons] -command [list Atmospheric::PickPointInSurfaceOrElementCmd $f.e$field]
        # grid $f.l$field $f.e$field $f.b$field -sticky ew
        # }
    
    grid $f -sticky new
    grid columnconfigure $f {1} -weight 1
    
    package require fulltktree
    set columns ""
    foreach name [list [= "Time (s)"] [= "Radiation (W/m2)"] [= "Wind_10 (m/s)"] [= "Air T (Celsius)"] [= "Hr (-)"]] {
        lappend columns [list 4 $name left text 1]
    }
    set T [fulltktree $f.t -columns $columns -showlines 0 -showheader 1 -expand 1]    
    set table $T
    $T configure -editbeginhandler GridData::EditBegin
    $T configure -editaccepthandler [list GridData::EditAccept]
    $T configure -deletehandler GridData::RemoveRows
    $T configure -contextualhandler_menu GridData::ContextualMenu    
    $T column configure all -button 0
    
    ttk::frame $f.fbuttons
    ttk::button $f.fbuttons.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $T 1]
    GidHelp $f.fbuttons.b1 [_ "Pressing this button, one new line will be added to this window"]    
    ttk::button $f.fbuttons.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $T 1]
    GidHelp $f.fbuttons.b2 [_ "Pressing this button, one line is deleted from this window"]        
    
    ttk::button $f.fbuttons.b3 -image [gid_themes::GetImage graphs.png small_icons] -text [_ "Graph"] \
        -command [list GridData::PlotCurveWin $T [= "Atmospheric"] ""]
    GidHelp $f.fbuttons.b3 [_ "Pressing this button, a XY graph will be drawn"]
    
    grid $T -sticky nsew -columnspan 2
    grid $f.fbuttons.b1 $f.fbuttons.b2 $f.fbuttons.b3 -sticky w    
    grid $f.fbuttons -sticky ew
    
    
    
    #    grid columnconfigure $f {0} -weight 1
    grid rowconfigure $f {1 4} -weight 1
    grid $f -sticky nsew -padx 2 -pady 2
    GidHelp $f [= "Set Atmospheric parameters"]
    
    bind $T <<Paste>> [list ::GridData::Paste $T CLIPBOARD]
    bind $T <<PasteSelection>> [list ::GridData::Paste $T PRIMARY]
    bind $T <<Cut>> [list GridData::Cut $T]
    bind $T <<Copy>> [list GridData::Copy $T]
    bind [$T givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header
    
    GridData::SetData $T $current_value(table_value)
    
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.bapply -text [= "Apply"] -command [list Atmospheric::Apply $T] -style BottomFrame.TButton
    
    ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    
    grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }        
    
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    bind $w <Alt-c> "$f.bclose invoke"
    bind $w <Escape> "$f.bclose invoke"   
    bind $w <Destroy> [list +Atmospheric::DestroyDischargeWindow %W $w] ;# + to add to previous script  
}



proc Atmospheric::CloseWindow { } {
    set w .gid.discharge
    if { [winfo exists $w] } {
        close $w
    }
}
