# Iber Post-process tool: ENCROACHMENT button and tool for pre-define ENC criteria

proc ENCcriteria {} {
    
    package require objarray 
    
    #Define de result to show: Max. Specific Discharge
#     set step [lindex [GiD_Info postprocess get all_steps "Hydraulic"] end]
#     
#     GiD_Process Mescape DisplayStyle bodybound Mescape \
#         Results AnalysisSel {Maps of Maximums} $step Geometry Original \
#         escape escape escape escape View Label Off escape escape escape escape \
#         results Geometry NoResults escape escape escape escape escape \
#         Results ContourFill {Specific Discharge (m2/s)}
    
    #Define the threshold value & show filtrated results
    set w .gid.vidresults

    
    InitWindow $w [= "Encroachment definition"] PostSelectResultsWindowGeom ENCcriteria
    
    set value 0
    set ::GidPriv(ZIP) 0
    
    ttk::labelframe $w.frmType -text [= " Encroachment options "]
    ttk::button $w.frmType.ziphelp -image [gid_themes::GetImage help.png small_icons] ;#-command [list Culvert::DeleteCulvert $f.cb1]
    ttk::radiobutton $w.frmType.zipbut -text [= "RD 9/2008"] -value 0 -variable ::GidPriv(ZIP)
    ttk::label $w.frmType.ziptext1 -text [= " "]
    ttk::label $w.frmType.ziptext2 -text [= " "]
    ttk::button $w.frmType.zipapply -text [= "Apply"] -command [list showthres $w]
    GidHelp $w.frmType.ziphelp  [= "Define the encroachment area according to the Severe Hazard zone of RD9/2008"]
    
    ttk::button $w.frmType.ctmhelp -image [gid_themes::GetImage help.png small_icons] ;#-command [list Culvert::DeleteCulvert $f.cb1]
    ttk::radiobutton $w.frmType.ctmbut -text [= "Custom"] -value 1 -variable ::GidPriv(ZIP)
    ttk::label $w.frmType.ctmtext -text [= "Value"]
    ttk::entry $w.frmType.ctmval -textvariable value
    ttk::button $w.frmType.ctmapply -text [= "Apply"] -command [list showthres $w]
    GidHelp $w.frmType.ctmhelp  [= "Define the encroachment area function of the maximum values of the hydraulic variables (h, v and q)"]
    
    grid $w.frmType.ziphelp $w.frmType.zipbut $w.frmType.ziptext1 $w.frmType.ziptext2 $w.frmType.zipapply -sticky ew
    grid $w.frmType.ctmhelp $w.frmType.ctmbut $w.frmType.ctmtext $w.frmType.ctmval $w.frmType.ctmapply -sticky ew
    grid columnconfigure $w.frmType {0 1 2 3} -weight 1
    grid $w.frmType -sticky new -padx 5 -pady 5
    
    ttk::frame $w.frmButton -style BottomFrame.TFrame
    ttk::button $w.frmButton.accept -text [= "Accept"] -command [list vid $w] -style BottomFrame.TButton
    ttk::button $w.frmButton.close -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton    
    
    grid $w.frmButton -sticky nsew
    grid $w.frmButton.accept $w.frmButton.close -padx 5 -pady 6
    if { $::tcl_version >= 8.5 } { grid anchor $w.frmButton center }
    
    grid columnconfigure $w "0" -weight 1
    grid rowconfigure $w "0" -weight 1
    
    bind $w <Alt-c> "$w.frmButton.close invoke"
    bind $w <Escape> "$w.frmButton.close invoke"
    bind $w <Return> "$w.frmButton.accept invoke"
    
    focus $w.frmButton.accept   
    
}

proc showthres {w} {
    global value
    
    set ::GidPriv(hydrovar) 0
    
    GiD_Process MEscape Results ContOptions SetMinOptions OutMinColor Black escape escape escape Utilities Redisplay
        
    if { $::GidPriv(ZIP) == 0 } {
        #RD9/2008
        set maxhazardanals [lsearch [GiD_Info postprocess get all_analysis] *Hazard*]
        if { $maxhazardanals != -1 } {
            set current_anal "Maps of Hazard"
            set maxsteps [lindex [GiD_Info postprocess get all_steps $current_anal] end]
        } else {
            tk_dialogRAM .gid.tmpwin error \
                [= "Results of hazard (RD9/2008) have not been calculated. Please, active it and re-run the simulation" ]\
                error 0 OK
            return
        }
       
        set severehazard [lsearch [GiD_Info postprocess get results_list contour_fill $current_anal $maxsteps] *Severe*]
        
        if { $severehazard != -1 } {
            GiD_Process MEscape Results AnalysisSel $current_anal $maxsteps Geometry Original escape results Geometry NoResults escape Results ContourFill {Maximum Severe Hazard RD9/2008}
            set ::GidPriv(hydrovar) "Maximum Severe Hazard RD9/2008"
        } else {
            tk_dialogRAM .gid.tmpwin error \
                [= "Results of hazard (RD9/2008) have not been calculated. Please, active it and re-run the simulation" ]\
                error 0 OK
            return
        }
        GidUtils::SetWarnLine [= "The coloured area defined according to the RD9/2008 will be used for the encroachment calculation" ]
    } elseif { $::GidPriv(ZIP) == 1 } {
        #RD9/2008
        set maxhydroanals [lsearch [GiD_Info postprocess get all_analysis] *Maximums*]
         if { $maxhydroanals != -1 } {
            set current_anal "Maps of Maximums"
            set maxsteps [lindex [GiD_Info postprocess get all_steps $current_anal] end]
        } else {
            tk_dialogRAM .gid.tmpwin error \
                [= "Results of maximums have not been calculated. Please, active it and re-run the simulation" ]\
                error 0 OK
            return
        }
       
#         set max_h [lsearch [GiD_Info postprocess get results_list contour_fill $current_anal $maxsteps] *Depth*]
#         set max_v [lsearch [GiD_Info postprocess get results_list contour_fill $current_anal $maxsteps] *Velocity*]
#         set max_q [lsearch [GiD_Info postprocess get results_list contour_fill $current_anal $maxsteps] *Discharge*]
        set curr_res [GiD_Info postprocess get cur_result]
        if { $curr_res == "" } {
            tk_dialogRAM .gid.tmpwin info \
                [= "Please" ]\
                info 0 OK
            return
        }       
        
        if { $curr_res == "Depth (m)" } {
            GiD_Process MEscape Results AnalysisSel $current_anal $maxsteps Geometry Original escape results Geometry NoResults escape Results ContourFill {Depth (m)}
            set ::GidPriv(hydrovar) "Depth (m)"
        } elseif { $curr_res == "Velocity (m/s)" } {
            GiD_Process MEscape Results AnalysisSel $current_anal $maxsteps Geometry Original escape results Geometry NoResults escape Results ContourFill {Velocity (m/s)}
            set ::GidPriv(hydrovar) "Velocity (m/s)"
        } elseif { $curr_res == "Specific Discharge (m2/s)" } {
            GiD_Process MEscape Results AnalysisSel $current_anal $maxsteps Geometry Original escape results Geometry NoResults escape Results ContourFill {Specific Discharge (m2/s)}
            set ::GidPriv(hydrovar) "Specific Discharge (m2/s)"
        } else {
            tk_dialogRAM .gid.tmpwin error \
                [= "The selected hydraulic maximum variable have not been calculated or is not available for this purpose. Please, active it and re-run the simulation or select maximum depth, velocity or specific discharge" ]\
                error 0 OK
            return
        }

        GiD_Process Mescape results contoptions setminoptions setvalue $value Mescape Utilities Redisplay escape escape
        GidUtils::SetWarnLine [= "The coloured area defined according user's criteria will be used for the encroachment calculation" ]
    }    
}

proc vid {w} {
    global value
    
    #Show & define list of Nodes/Elements BELOW the filtration value
    set vidcriteria [list $::GidPriv(hydrovar)]
	if { $vidcriteria == "{Maximum Severe Hazard RD9/2008}" } {
        set value 0.99
    } 
    set sel [GetSelectionResultQuality $vidcriteria $value]
#     if { $::GidPriv(ZIP) == 0 } {
# 
#         
#     } elseif { $::GidPriv(ZIP) == 1 } {
#         set sel [GetSelectionResultQuality $vidcriteria $value]
#     }
    
    
#     set sel [GetSelectionResultQuality {{Specific Discharge (m2/s)} 0} $value]
    #W $sel
    
    set type [lsearch -exact $sel "Nodes"]
    if {$type == 0} {
        set entity 1
        set sel [lreplace $sel $type $type]
    }
    set type [lsearch -exact $sel "Elements"]
    if {$type == 0} {
        set entity 2
        set sel [lreplace $sel $type $type]
    }
    
    #Returns uncompacted list
    set obj [GidUtils::UnCompactNumberList $sel]
    #W $obj
    
    #Determine the length of the new list
    set num [llength $obj]    
    
    #Define de folder
    set Project [GiD_Info project ModelName] 
    if { $Project == "UNNAMED" } {
        tk_dialogRAM .gid.tmpwin error \
            [= "Before writting results, a project title is needed. Save project to get it" ]\
            error 0 OK
        return
    }
    set directory $Project.gid
    if { [file pathtype $directory] == "relative" } {
        set directory [file join [pwd] $directory]
    }                        
    
    #Create a file with the list of nodes/elements
    set filename "Iber_Encroachment_q"        
    append filename ".dat"        
    set fitxer [file join $directory $filename] 
    set fitxeraux [open $fitxer w]
    set title $entity
    append title " "
    append title $value
    append title " "
    append title $num
    puts $fitxeraux $title
    for {set ent 0} {$ent <= $num} {incr ent} {
        puts $fitxeraux [lindex $obj $ent]        
    }
    close $fitxeraux
    
#     destroy $w
}
