namespace eval SoilErosion {
    variable data ;#array of values
    variable zones ;#list of names of data
    variable current_zone ;#name of current selected zone
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {type table_value}
    variable fields_defaults {{2} {1 0.0 0.0}}  
    variable drawopengl
    variable table ;#tktable widget
}

proc SoilErosion::UnsetVariables { } {
    variable data
    variable zones      
    unset -nocomplain data
    unset -nocomplain zones
}

proc SoilErosion::SetDefaultValues { } {    
    variable data
    variable zones
    variable current_zone
    variable current_value
    variable fields
    variable fields_defaults
    array unset data
    set zones {}
    foreach field $fields  {
        set current_value($field) ""
    }
    set current_value(table_value) {1 0.0 0.0}
    set current_zone {}
}

#store the zones information in a hidden field of the problem data
#then this data is saved with the model without do nothing more
proc SoilErosion::FillTclDataFromProblemData { } {
    variable data
    variable zones
    if { [catch {set x [GiD_AccessValue get gendata SoilErosionData]} msg] } {
        #reading a model created by an old version of the problemtype this gendata doesn't exists
        return
    }    
    array set data $x
    
    set zones [list]
    foreach item [array names data *,table_value] {
        lappend zones [string range $item 0 end-12]
    }
    set zones [lsort -dictionary $zones]
    SoilErosion::SetCurrentZone [lindex $zones 0]
}

proc SoilErosion::FillProblemDataFromTclData { } {
    variable data
    variable zones
    set x [array get data]
    GiD_AccessValue set gendata SoilErosionData $x
}

proc SoilErosion::WriteCalculationFile { } {
    variable data
    set res ""       
    
    set i 0
    append res "Erodible Zones"  \n
    append res [llength [SoilErosion::GetZones]] \n
    foreach zone [SoilErosion::GetZones] {
        incr i
        append res $zone \n        
        append res $i  \n
        append res [lindex $data($zone,type)] \n
        set lines [expr [llength $data($zone,table_value)]/3]
        append res $lines \n
        set l [expr $lines*3]
        for {set ii 0} {$ii < $l} {set ii [expr $ii+3]} {
            append res "[lrange $data($zone,table_value) $ii [expr $ii+2]]" \n
        }                
    }
    
    return $res
}

proc SoilErosion::WriteZoneNumber { name } {
    variable number
    
    set number [expr [lsearch [SoilErosion::GetZones] $name]+1]
    
    return $number    
}

proc SoilErosion::GetZones { } {
    variable zones
    if { ![info exists zones] } {
        return ""
    }
    return $zones
}

proc SoilErosion::GetCurrentZone { } {
    variable current_zone
    return $current_zone
}

proc SoilErosion::OnChangeSelectedZone { cb } {   
    SoilErosion::SetCurrentZone [SoilErosion::GetCurrentZone]
}

proc SoilErosion::SetCurrentZone { zone } {
    variable data
    variable current_zone
    variable current_value
    variable fields   
    variable table
    #fill in the current_value variables with data
    if { $zone != "" } {
        foreach field $fields {
            if { ![info exists data($zone,$field)] } {
                set data($zone,$field) 0
            }
            set current_value($field) $data($zone,$field)           
        }                
    } else {
        foreach field $fields {
            set current_value($field) ""
        }
    }
    
    #the other widgets have a linked variable that automatically update it but the table not
    if { [info exists table] && [winfo exists $table] } {
        GridData::SetData $table $current_value(table_value)
    }
    set current_zone $zone  
}

proc SoilErosion::GetIndex { zone } {
    variable zones    
    return [lsearch $zones $zone]
}

proc SoilErosion::Exists { zone } {    
    if { [SoilErosion::GetIndex $zone] != -1 } {
        set exists 1
    } else {
        set exists 0
    }
    return $exists
}

proc SoilErosion::GetZonesAutomaticName { } {
    set basename [_ "zone"]
    set i 1
    set zone $basename-$i
    while { [SoilErosion::Exists $zone] } {        
        incr i
        set zone $basename-$i        
    }
    return $zone
}

proc SoilErosion::NewZone { cb } {
    variable zones
    variable data
    variable fields 
    variable fields_defaults
    set zone [SoilErosion::GetZonesAutomaticName]     
    
    if { [SoilErosion::Exists $zone] } {
        #already exists
        return 1
    }
    foreach field $fields value $fields_defaults {
        set data($zone,$field) $value
    }
    lappend zones $zone   
    if { [winfo exists $cb] } {
        $cb configure -values [SoilErosion::GetZones]
    }
    SoilErosion::SetCurrentZone $zone
    return 0
}

proc SoilErosion::DeleteZone { cb } {
    variable zones
    variable data
    variable fields 
    variable fields_defaults
    set zone [SoilErosion::GetCurrentZone] 
    if { ![SoilErosion::Exists $zone] } {
        #not exists
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete zone '%s'" $zone] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {     
        set i [SoilErosion::GetIndex $zone] 
        array unset data $zone,*        
        set zones [lreplace $zones $i $i]       
        if { [winfo exists $cb] } {
            $cb configure -values [SoilErosion::GetZones]
        }
        SoilErosion::SetCurrentZone [lindex $zones 0]  
        GiD_Redraw     
    }
    return 0
}

proc SoilErosion::RenameZone { cb } {
    variable data
    variable zones
    variable fields
    set zone [SoilErosion::GetCurrentZone] 
    set num [SoilErosion::WriteZoneNumber $zone]        
    if { ![SoilErosion::Exists $zone] } {
        #not exists
        return 1
    } 
    set new_text [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $zone] \
            [= "Enter new name of %s '%s'" [= "zone"] $zone] gidquestionhead "any" ""]
    set new_name "ID$num - $new_text"
    if { $new_name != "--CANCEL--" } {
        foreach field $fields {
            set data($new_name,$field) $data($zone,$field)
        }
        array unset data $zone,*
        set i [SoilErosion::GetIndex $zone] 
        lset zones $i $new_name
        if { [winfo exists $cb] } {
            $cb configure -values [SoilErosion::GetZones]           
        }
        SoilErosion::SetCurrentZone $new_name       
        GiD_Redraw 
    }
    return 0
}

#apply the values of the current window
proc SoilErosion::Apply { T } {
    variable data
    variable current_value
    variable fields
    
    set zone [SoilErosion::GetCurrentZone]    
    foreach field $fields {
        set data($zone,$field) $current_value($field)
    }
    
    set data($zone,table_value) [GridData::GetDataAllItems $T]
    GiD_Redraw        
}

#not SoilErosion::StartDraw and SoilErosion::EndDraw are not used, the Draw button is hidden (because unregister fail in GiD 11.1.5d)
#and the redraw procedure is registered when loading the problemtype, and each zone is draw depending on its 'visible' variable value

proc SoilErosion::StartDraw { bdraw} {
    variable drawopengl
    set drawopengl [GiD_OpenGL register SoilErosion::ReDraw]
    GiD_Redraw
    $bdraw configure -text [= "Finish draw"] -command [list SoilErosion::EndDraw $bdraw]
}

proc SoilErosion::EndDraw { bdraw } {
    variable drawopengl
    if { [info exists drawopengl] } { 
        #unregister the callback tcl procedure to be called by GiD when redrawing
        GiD_OpenGL unregister $drawopengl
        catch {unset drawopengl}
        GiD_Redraw
        if { $bdraw != "" } {
            $bdraw configure -text [= "Draw"] -command [list SoilErosion::StartDraw $bdraw]
        }
    }
}

proc SoilErosion::ReDraw { } {
    foreach zone [SoilErosion::GetZones] {
        SoilErosion::Draw $zone
    }
}


proc SoilErosion::DestroyZoneWindow { W w } {   
    if { $W == $w } {
        SoilErosion::EndDraw ""
        SoilErosion::FillProblemDataFromTclData             
    }
}

proc SoilErosion::OnChangeType { f } {
    variable current_value
    
}

proc SoilErosion::PickPointInSurfaceOrElementCmd { entry } {
    set view_mode [GiD_Info Project ViewMode]
    if { $view_mode == "GEOMETRYUSE" } {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an surface"] PointInSurface GEOMETRYUSE]
        #set surface_id $::GidPriv(selection)  
    } elseif { $view_mode == "MESHUSE"} {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an element"] PointInElement MESHUSE]
        #set element_id $::GidPriv(selection)    
    } else {
        set xyz ""
    }        
    if { $xyz != "" } {                    
        set res [format "%g %g %g" {*}$xyz]
        $entry delete 0 end
        $entry insert end $res        
    }
}



proc SoilErosion::Window { } {
    variable data
    variable zones
    variable current_zone
    variable current_value
    variable table
    
    if { ![info exists zones] } {
        SoilErosion::SetDefaultValues
    }
    
    SoilErosion::FillTclDataFromProblemData    
    
    set w .gid.zone
    InitWindow $w [= "Erodible Zones"] PreZoneWindowGeom SoilErosion::Window
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    set f [ttk::frame $w.fzones]
    
    ttk::combobox $f.cb1 -textvariable SoilErosion::current_zone -values [SoilErosion::GetZones] -state readonly
    bind $f.cb1 <<ComboboxSelected>> [list SoilErosion::OnChangeSelectedZone %W]
    ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list SoilErosion::NewZone $f.cb1]
    GidHelp $f.bnew  [= "Create a new erodible zone"]
    ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list SoilErosion::DeleteZone $f.cb1]
    GidHelp $f.bdel  [= "Delete an erodible zone"]
    ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list SoilErosion::RenameZone $f.cb1]
    GidHelp $w.f.bren  [= "Rename an erodible zone"]   
    grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
    grid $f -sticky new
    grid rowconfigure $f 0 -weight 1
    
    set f [ttk::frame $w.fzone]
    
    foreach field {type} text [list [= "Location of erodible material"]] values {{1 2}} labels [list [list [= "Over DEM"] [= "Below DEM"]]] {
        ttk::label $f.l$field -text $text
        #ttk::combobox $f.c$field -values $values -state readonly -textvariable SoilErosion::current_value($field)
        TTKComboBox $f.c$field -values $values -labels $labels -state readonly -textvariable SoilErosion::current_value($field) \
            -modifycmd [list SoilErosion::OnChangeType $f]
        grid $f.l$field $f.c$field -sticky ew
    }
    
    grid $f -sticky new
    grid columnconfigure $f {1} -weight 1
    
    package require fulltktree
    set columns ""
    foreach name [list [= "Soil ID"] [= "Erodible material (kg/m2)"] [= "SS (kg/m3)"]] {
        lappend columns [list 4 $name left text 1]
    }
    set T [fulltktree $f.t -columns $columns -showlines 0 -showheader 1 -expand 1]    
    set table $T
    $T configure -editbeginhandler GridData::EditBegin
    $T configure -editaccepthandler [list GridData::EditAccept]
    $T configure -deletehandler GridData::RemoveRows
    $T configure -contextualhandler_menu GridData::ContextualMenu    
    $T column configure all -button 0
    
    ttk::frame $f.fbuttons
    ttk::button $f.fbuttons.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $T 1]
    GidHelp $f.fbuttons.b1 [_ "Pressing this button, one new line will be added to this window"]    
    ttk::button $f.fbuttons.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $T 1]
    GidHelp $f.fbuttons.b2 [_ "Pressing this button, one line is deleted from this window"]        
    
    grid $T -sticky nsew -columnspan 2
    grid $f.fbuttons.b1 $f.fbuttons.b2 -sticky w    
    grid $f.fbuttons -sticky ew
    
    
    
    grid columnconfigure $f {0} -weight 1
    grid rowconfigure $f {1 4} -weight 1
    grid $f -sticky nsew -padx 2 -pady 2
    GidHelp $f [= "Set Zone parameters"]
    
    bind $T <<Paste>> [list ::GridData::Paste $T CLIPBOARD]
    bind $T <<PasteSelection>> [list ::GridData::Paste $T PRIMARY]
    bind $T <<Cut>> [list GridData::Cut $T]
    bind $T <<Copy>> [list GridData::Copy $T]
    bind [$T givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header
    
    GridData::SetData $T $current_value(table_value)
    
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.bapply -text [= "Apply"] -command [list SoilErosion::Apply $T] -style BottomFrame.TButton
    #ttk::button $f.bdraw -text [= "Draw"] -command [list SoilErosion::StartDraw $f.bdraw] -style BottomFrame.TButton
    ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    #grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
    grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }        
    
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    bind $w <Alt-c> "$f.bclose invoke"
    bind $w <Escape> "$f.bclose invoke"   
    bind $w <Destroy> [list +SoilErosion::DestroyZoneWindow %W $w] ;# + to add to previous script  
}

proc SoilErosion::CloseWindow { } {
    set w .gid.zone
    if { [winfo exists $w] } {
        close $w
    }
}



proc Iber::AssignErosionZoneWin { questions } {
    set filenames_and_questions [list]
    foreach question $questions {
        set filename_raster [Browser-ramR file read .gid [= "Read ASCII RASTER file to assign Erosion Zone ID"] \
                {} {{{} {.txt}} {{} {.asc}} {{All files} {.*}}} ]
        if {$filename_raster != "" } {
            lappend filenames_and_questions $filename_raster $question
        } else {
            return 1
        }
    }
    
    if { [llength $filenames_and_questions] } {        
        GidUtils::WaitState
        set tstart [clock milliseconds]
        set show_advance_bar 1
        set fail [Iber::AssignZoneDo $filenames_and_questions $show_advance_bar]
        set tend [clock milliseconds]
        GidUtils::EndWaitState        
        GidUtils::SetWarnLine [= "Erosion Zone assigned to elements. time %s seconds" [expr ($tend-$tstart)/1000.0]]
        #ask the user if want to draw it
        set answer [MessageBoxOptionsButtons [= "Erosion Zone assigned"] [= "Erosion Zone was succesfully assigned to mesh elements. Do you want to draw it?"] [list yes no] [list [_ "Yes"] [_ "No"]] question ""]
        if {$answer == "yes"} {
            #ensure mesh view mode
            MeshView 0
            set i 0
            set n [llength $questions]
            GiD_Process Mescape Data Conditions DrawCond -ByColor- Zone_definition $question
            if { $i < [expr $n-1] } {
                set answer [MessageBoxOptionsButtons [= "Draw erosion zones" $question] [= "Press continue to draw next Erosion Zone"] "" [list continue cancel] [list [_ "Continue"] [_ "Cancel"]] question]
                if { $answer == "cancel" } {
                    after 1000 GiD_Process escape escape escape
                    break
                }
            }
            incr i
            
        }
    }
    return 0
}


#to assign to mesh elements the Erosion Zone (GiD condition Zone_definition with values from a list of ARC/Info .asc files, one by question to be set)
# e.g.
# 
proc Iber::AssignZoneDo { filenames_and_questions {show_advance_bar 1} } {
    set fail 0
    variable percent 0
    variable stop 0
    
    #assign percent of four steps to 10 10 40 40
    set condition_name "Zone_definition"
    set default_values [GidUtils::GetConditionDefaultValues $condition_name] ;#default values 
    if { $show_advance_bar } {
        GidUtils::CreateAdvanceBar [_ "Assign Erosion Zone ID"] [_ "Percentage"]: ::Iber::percent 100  {set ::Iber::stop 1}
    }
    set percent 0
    set nodata_value -9999 ;#assumed same value for all files 
    set element_ids [Iber::GetElementIdsLayersNoFrozen]
    set num_elements [objarray length $element_ids]
    set xy_element_centers [Iber::GetElementCenters $element_ids]    
    set percent 10
    #initialize default values (except fixed model) for all elements
    for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
        set element_id [objarray get $element_ids $i_element]
        set element_values($element_id) $default_values        
    }    
    set n_raster [expr [llength $filenames_and_questions]/2]
    set n_refresh 1
    #fill a field by each raster
    set i_raster 0
    foreach {filename_raster condition_question} $filenames_and_questions {
        if { $show_advance_bar } {
            if { ![expr {$i_raster%$n_refresh}] } {
                set percent [expr int(double($i_raster)/$n_raster*100*0.4+20)]
            }
        }
        if { $stop } {
            set fail -1
            break
        }  
        set condition_question_index [GidUtils::GetConditionQuestionIndex $condition_name "zone_id"]
        set values $default_values
        set raster_interpolation [GDAL::ReadRaster $filename_raster 0]
        set nodata_value [GDAL::GetNoDataValue $filename_raster]
        #set interpolated_values [GiD_Raster interpolate $raster_interpolation [list nodes $xy_element_centers]]
        #-closest in this case also, because GiD_AssignData become a bottleneck in case of a each element a different value (and big strings), better use only a few discrete values
        set interpolated_values [GiD_Raster interpolate -closest $raster_interpolation [list nodes $xy_element_centers]]        
        for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
            set element_id [objarray get $element_ids $i_element]
            set value [objarray get $interpolated_values $i_element]
            lset element_values($element_id) $condition_question_index $value
        }
        incr i_raster
    }    
    #assign the final values
    #GiD_AssignData condition of a condition canrepeat=no an big mesh is a huge bottleneck 
    #(because it is moving every call the whole container to a temporary container sorted by it)
    #in future versions (>14.1.8d) we want to enhance the code storing entityvalue ordered by entity id and avoid the bottleneck
    #
    #to minimize GiD_AssignData calls join in a single assign the elements with same values
    for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
        set element_id [objarray get $element_ids $i_element]
        set values $element_values($element_id)
        lappend elements_same_value($values) $element_id
    }
    unset element_ids
    array unset element_values
    set list_values [array names elements_same_value]
    set num_different_values [llength $list_values]
    set n_refresh_different_values [expr int($num_different_values/4)]
    if { $n_refresh_different_values<1 } {
        set n_refresh_different_values 1
    }
    set i_different_values 0
    foreach values $list_values {
        if { $values != $nodata_value } {
            if { $show_advance_bar } {
                if { ![expr {$i_different_values%$n_refresh_different_values}] } {
                    set percent [expr int(double($i_different_values)/$num_different_values*100*0.4+60)]
                }
            }
            GiD_AssignData condition $condition_name body_elements $values $elements_same_value($values)
            incr i_different_values
        }
    }        
    if { $show_advance_bar } {
        GidUtils::DeleteAdvanceBar
    }
    return $fail
}

