
namespace eval Culvert {
    variable data ;#array of values
    variable culverts ;#list of names of data
    variable current_culvert ;#name of current selected culvert
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {visible calculate start end manning type width height}
    variable fields_defaults {1 1 {} {} 0.011 1 0.0 0.0}   
    variable drawopengl
    variable importculverts
}

proc Culvert::UnsetVariables { } {
    variable data
    variable culverts      
    unset -nocomplain data
    unset -nocomplain culverts
}

proc Culvert::SetDefaultValues { } {    
    variable data
    variable culverts
    variable current_culvert
    variable current_value
    variable fields
    variable fields_defaults
    array unset data
    set culverts {}
    foreach field $fields  {
        set current_value($field) ""
    }
    set current_culvert {}
}

#store the culverts information in a hiddend field of the problem data
#then this data is saved with the model without do nothing more
proc Culvert::FillTclDataFromProblemData { } {
    variable data
    variable culverts
    
    if { [catch {set x [GiD_AccessValue get gendata CulvertsData]} msg] } {
        #reading a model created by an old version of the problemtype this gendata doesn't exists
        return
    }
    array set data $x
    
    set culverts [list]
    foreach item [array names data *,start] {
        lappend culverts [string range $item 0 end-6]
    }
    set culverts [lsort -dictionary $culverts]
    Culvert::SetCurrentCulvert [lindex $culverts 0]
}

proc Culvert::FillProblemDataFromTclData { } {
    variable data
    variable culverts
    set x [array get data]
    GiD_AccessValue set gendata CulvertsData $x
}

proc Culvert::WriteCalculationFile { } {
    variable data
    set res ""       
    
    set i 0
    foreach culvert [Culvert::GetCulverts] {
        incr i
        
        if { $data($culvert,calculate) == 1 } {        
            set calc_culvert "1"
        } else {
            set calc_culvert "0"
        }
        
        set item [list]
        lappend item $i
        lappend item $calc_culvert
        lappend item {*}[lrange $data($culvert,start) 0 1]
        lappend item {*}[lrange $data($culvert,end) 0 1]
        lappend item [lindex $data($culvert,start) 2]
        lappend item [lindex $data($culvert,end) 2]
        if { $data($culvert,type) == 1 } {
            lappend item 1
        } else {
            lappend item 2
        }
        lappend item $data($culvert,width)
        if { $data($culvert,type) == 1 } {
            lappend item $data($culvert,height)
        } else {
            lappend item 0
        }
        lappend item $data($culvert,manning)
        lappend item $culvert        
        append res [join $item " "]\n
    }
    return $res
}

proc Culvert::GetCulverts { } {
    variable culverts
    if { ![info exists culverts] } {
        return ""
    }
    return $culverts
}

proc Culvert::GetCurrentCulvert { } {
    variable current_culvert
    return $current_culvert
}

proc Culvert::OnChangeSelectedCulvert { cb } {   
    Culvert::SetCurrentCulvert [Culvert::GetCurrentCulvert]
}

proc Culvert::SetCurrentCulvert { culvert } {
    variable data
    variable current_culvert
    variable current_value
    variable fields   
    #fill in the current_value variables with data
    if { $culvert != "" } {
        foreach field $fields {
            if { ![info exists data($culvert,$field)] } {
                set data($culvert,$field) 0
            }
            set current_value($field) $data($culvert,$field)           
        }
    } else {
        foreach field $fields {
            set current_value($field) ""
        }
    }
    set current_culvert $culvert  
}

proc Culvert::GetIndex { culvert } {
    variable culverts    
    return [lsearch $culverts $culvert]
}

proc Culvert::Exists { culvert } {    
    if { [Culvert::GetIndex $culvert] != -1 } {
        set exists 1
    } else {
        set exists 0
    }
    return $exists
}

proc Culvert::GetCulvertAutomaticName { } {
    set basename [_ "culvert"]
    set i 1
    set culvert $basename-$i
    while { [Culvert::Exists $culvert] } {        
        incr i
        set culvert $basename-$i        
    }
    return $culvert
}

proc Culvert::NewCulvert { cb } {
    variable culverts
    variable data
    variable fields 
    variable fields_defaults
    set culvert [Culvert::GetCulvertAutomaticName]     
    
    if { [Culvert::Exists $culvert] } {
        #already exists
        return 1
    }
    foreach field $fields value $fields_defaults {
        set data($culvert,$field) $value
    }
    lappend culverts $culvert   
    if { [winfo exists $cb] } {
        $cb configure -values [Culvert::GetCulverts]
    }
    Culvert::SetCurrentCulvert $culvert
    return 0
}

proc Culvert::DeleteCulvert { cb } {
    variable culverts
    variable data
    variable fields 
    variable fields_defaults
    set culvert [Culvert::GetCurrentCulvert] 
    if { ![Culvert::Exists $culvert] } {
        #not exists
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete culvert '%s'" $culvert] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {     
        set i [Culvert::GetIndex $culvert] 
        array unset data $culvert,*        
        set culverts [lreplace $culverts $i $i]       
        if { [winfo exists $cb] } {
            $cb configure -values [Culvert::GetCulverts]
        }
        Culvert::SetCurrentCulvert [lindex $culverts 0]  
        GiD_Redraw     
    }
    return 0
}

proc Culvert::DeleteAllCulverts { cb } {
    variable culverts
    variable data
    variable fields 
    variable fields_defaults
    
    set culvert [Culvert::GetCurrentCulvert] 
    if { ![Culvert::Exists $culvert] } {
        MessageBoxOk [= "Deleting process of culverts"] [= "There is no culverts to delete"] error
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete all culverts?"] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]

    if { $ret == 0 } {
        set i 1
        foreach culvert $culverts { 
            set i [Culvert::GetIndex $culvert] 
            array unset data $culvert,*        
            set culverts [lreplace $culverts $i $i]       
            if { [winfo exists $cb] } {
                $cb configure -values [Culvert::GetCulverts]
            }
            Culvert::SetCurrentCulvert [lindex $culverts 0]  
            incr i
        }
    }
    GiD_Redraw
    GidUtils::SetWarnLine [= "All culverts have been deleted"]
    return 0
}

proc Culvert::DeleteRepeatedCulvert { cb } {
    variable culverts
    variable data
    variable fields 
    variable fields_defaults
    
    set culvert [Culvert::GetCurrentCulvert] 
    
    set i [Culvert::GetIndex $culvert] 
    array unset data $culvert,*        
    set culverts [lreplace $culverts $i $i]       
    if { [winfo exists $cb] } {
        $cb configure -values [Culvert::GetCulverts]
    }
    Culvert::SetCurrentCulvert [lindex $culverts 0]  
    GiD_Redraw     

    return 0
}

proc Culvert::RenameCulvert { cb } {
    variable data
    variable culverts
    variable fields
    set culvert [Culvert::GetCurrentCulvert]   
    if { ![Culvert::Exists $culvert] } {
        #not exists
        return 1
    } 
    set new_name [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $culvert] \
            [= "Enter new name of %s '%s'" [= "culvert"] $culvert] gidquestionhead "any" ""]
    if { $new_name != "--CANCEL--" } {
        foreach field $fields {
            set data($new_name,$field) $data($culvert,$field)
        }
        array unset data $culvert,*
        set i [Culvert::GetIndex $culvert] 
        lset culverts $i $new_name
        if { [winfo exists $cb] } {
            $cb configure -values [Culvert::GetCulverts]           
        }
        Culvert::SetCurrentCulvert $new_name       
        GiD_Redraw 
    }
    set dsd 1
    return 0
}

proc Culvert::RenameCulvertImported { culvertname } {
    variable data
    variable culverts
    variable fields
    
    set culvert [Culvert::GetCurrentCulvert]
        
    if { ![Culvert::Exists $culvertname] } {
        set new_name $culvertname
        
        foreach field $fields {
            set data($new_name,$field) $data($culvert,$field)
        }
        array unset data $culvert,*
        set i [Culvert::GetIndex $culvert] 
        lset culverts $i $new_name

        Culvert::SetCurrentCulvert $new_name       
        GiD_Redraw
    }
    
    return 0
}

#apply the values of the current window
proc Culvert::Apply { } {
    variable data
    variable current_value
    variable fields
    
    set culvert [Culvert::GetCurrentCulvert]    
    foreach field $fields {
        set data($culvert,$field) $current_value($field)
    }
    
    GiD_Redraw        
}

#now Culvert::StartDraw and Culvert::EndDraw are not used, the Draw button is hidden (because unregister fail in GiD 11.1.5d)
#and the redraw procedure is registered when loading the problemtype, and each culvert is draw depending on its 'visible' variable value
proc Culvert::StartDraw { bdraw} {
    variable drawopengl
    set drawopengl [GiD_OpenGL register Culvert::ReDraw]
    GiD_Redraw
    $bdraw configure -text [= "Finish draw"] -command [list Culvert::EndDraw $bdraw]
}

proc Culvert::EndDraw { bdraw } {
    variable drawopengl
    if { [info exists drawopengl] } { 
        #unregister the callback tcl procedure to be called by GiD when redrawing
        GiD_OpenGL unregister $drawopengl
        catch {unset drawopengl}
        GiD_Redraw
        if { $bdraw != "" } {
            $bdraw configure -text [= "Draw"] -command [list Culvert::StartDraw $bdraw]
        }
    }
}

proc Culvert::ReDraw { } {
    foreach culvert [Culvert::GetCulverts] {
        Culvert::Draw $culvert
    }
}

proc Culvert::Draw { culvert } {
    variable data
    if { $culvert == "" || ![info exists data($culvert,visible)] || !$data($culvert,visible) } {
        return 1
    }
    foreach field {start end} {
        if { [llength $data($culvert,$field)] != 3 } {
            return 1
        }
        foreach v $data($culvert,$field) {
            if { ![string is double $v] } {
                return 1
            }
        }
    }
    # Draw in "blue" when the culvert is calculated, and in "grey" when the culvert is not calculated
    if { $data($culvert,calculate) == 1 } {
        set color {0 0 1}
    } else {
        set color {0.75 0.75 0.75}
    }
    GiD_OpenGL draw -color $color
    GiD_OpenGL draw -pointsize 3
    GiD_OpenGL draw -begin points
    GiD_OpenGL draw -vertex $data($culvert,start)
    GiD_OpenGL draw -vertex $data($culvert,end)
    GiD_OpenGL draw -end
    GiD_OpenGL draw -begin lines
    GiD_OpenGL draw -vertex $data($culvert,start)
    GiD_OpenGL draw -vertex $data($culvert,end)
    GiD_OpenGL draw -end
    #show label
    set p [MathUtils::VectorSum $data($culvert,start) $data($culvert,end)]
    set p [MathUtils::ScalarByVectorProd 0.5 $p]
    GiD_OpenGL draw -rasterpos $p
    GiD_OpenGL drawtext $culvert
    return 0
}

proc Culvert::DestroyCulvertWindow { W w } {   
    if { $W == $w } {
        Culvert::EndDraw ""
        Culvert::FillProblemDataFromTclData             
    }
}

proc Culvert::OnChangeType { f } {
    variable current_value
    if { $current_value(type) == 1 } {
        #Rectangular
        $f.lwidth configure -text [= "Width (m)"]        
        grid $f.lheight $f.eheight
    } else {
        #Circular
        $f.lwidth configure -text [= "Diameter (m)"]
        grid remove $f.lheight $f.eheight
    }
}

proc Culvert::PickPointInSurfaceOrElementCmd { entry } {
    set view_mode [GiD_Info Project ViewMode]
    if { $view_mode == "GEOMETRYUSE" } {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an surface"] PointInSurface GEOMETRYUSE]
        #set surface_id $::GidPriv(selection)  
    } elseif { $view_mode == "MESHUSE"} {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an element"] PointInElement MESHUSE]
        #set element_id $::GidPriv(selection)    
    } else {
        set xyz ""
    }        
    if { $xyz != "" } {                    
        set res [format "%g %g %g" {*}$xyz]
        $entry delete 0 end
        $entry insert end $res        
    }
}

proc Culvert::Window { } {
    variable data
    variable culverts
    variable current_culvert
    variable current_value
    
    if { ![info exists culverts] } {
        Culvert::SetDefaultValues
    }
    
    Culvert::FillTclDataFromProblemData    
    
    set w .gid.culvert
    InitWindow $w [= "Culvert"] PreCulvertWindowGeom Culvert::Window
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    set f [ttk::frame $w.fculverts]
    
    ttk::combobox $f.cb1 -textvariable Culvert::current_culvert -values [Culvert::GetCulverts] -state readonly
    bind $f.cb1 <<ComboboxSelected>> [list Culvert::OnChangeSelectedCulvert %W]
    ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list Culvert::NewCulvert $f.cb1]
    GidHelp $f.bnew  [= "Create a new culvert"]
    ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list Culvert::DeleteCulvert $f.cb1]
    GidHelp $f.bdel  [= "Delete a culvert"]
    ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list Culvert::RenameCulvert $f.cb1]
    GidHelp $w.f.bren  [= "Rename a culvert"]
    ttk::button $f.bdelall -image [gid_themes::GetImage erase.png small_icons] -command [list Culvert::DeleteAllCulverts $f.cb1]
    GidHelp $f.bdelall  [= "Delete all culverts"]
    grid $f.cb1 $f.bnew $f.bdel $f.bren $f.bdelall -sticky w
    grid $f -sticky new
    grid rowconfigure $f 0 -weight 1
    
    set f [ttk::frame $w.fculvert]        
    set field visible
    ttk::checkbutton $f.ch$field -text [= "Visible"] -variable Culvert::current_value($field)
    grid $f.ch$field -sticky w
    
    set field calculate
    ttk::checkbutton $f.ch$field -text [= "Calculate"] -variable Culvert::current_value($field)
    grid $f.ch$field -sticky w
    
    foreach field {start end} text [list [= "Start"] [= "End"]] {
        ttk::label $f.l$field -text $text
        ttk::entry $f.e$field -textvariable Culvert::current_value($field)
        ttk::button $f.b$field -image [gid_themes::GetImage "point.png" small_icons] -command [list Culvert::PickPointInSurfaceOrElementCmd $f.e$field]
        grid $f.l$field $f.e$field $f.b$field -sticky ew
    }
    foreach field {manning} text [list [= "Manning"]] {
        ttk::label $f.l$field -text $text
        ttk::entry $f.e$field -textvariable Culvert::current_value($field)
        grid $f.l$field $f.e$field -sticky ew
    }
    foreach field {type} text [list [= "Type"]] values {{1 2}} labels [list [list [= "Rectangular"] [= "Circular"]]] {
        ttk::label $f.l$field -text $text
        #ttk::combobox $f.c$field -values $values -state readonly -textvariable Culvert::current_value($field)
        TTKComboBox $f.c$field -values $values -labels $labels -state readonly -textvariable Culvert::current_value($field) \
            -modifycmd [list Culvert::OnChangeType $f]
        grid $f.l$field $f.c$field -sticky ew
    }
    foreach field {width height} text [list [= "Width (m)"] [= "Height (m)"]] {
        ttk::label $f.l$field -text $text
        ttk::entry $f.e$field -textvariable Culvert::current_value($field)
        grid $f.l$field $f.e$field -sticky ew
    }
    
    grid $f -sticky new
    grid columnconfigure $f {1} -weight 1
    
    
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.bapply -text [= "Apply"] -command [list Culvert::Apply] -style BottomFrame.TButton
    ttk::button $f.bimport -text [= "Import"] -command [list Culvert::Import] -style BottomFrame.TButton
    GidHelp $f.bimport  [= "Import the culvert file from other model"]
    ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    grid $f.bapply $f.bimport $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }        
    
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    bind $w <Alt-c> "$f.bclose invoke"
    bind $w <Escape> "$f.bclose invoke"   
    bind $w <Destroy> [list +Culvert::DestroyCulvertWindow %W $w] ;# + to add to previous script  
}

proc Culvert::CloseWindow { } {
    set w .gid.culvert    
    if { [winfo exists $w] } {
        destroy $w
    }
}

proc Culvert::Import { } { 
    variable importculverts
    
    set types [list [list [= "File format"] {.dat}] [list [_ "All files"] {.*}]]
    set defaultextension .dat
    set importculverts(file) ""
    
    set importculverts(file) [Browser-ramR file read .gid [= "Choose Culverts file"] $importculverts(file) $types $defaultextension 0]      
    set importculverts(filename) [file tail importculverts(file)]
    set importculverts(filedir) [file dirname importculverts(file)]
    
    if { $importculverts(file) == "" } {
        GidUtils::SetWarnLine [= "No culvert file has been selected" ]
        return
    }
    
    set culvertfile [open $importculverts(file) r]
    set alldata [read $culvertfile [file size $importculverts(file)]]
    set lineCount [llength [split $alldata "\n"]]
    set importculverts(numculverts) [expr ($lineCount-2)]
    close $culvertfile
    Culvert::ReadCulvertFile
}


proc Culvert::ReadCulvertFile { } {
    variable data
    variable culverts
    variable current_culvert
    variable current_value
    variable importculverts
    variable vars {id calculate s1 s2 e1 e2 s3 e3 type width height manning culvertname}
    
    GidUtils::WaitState .gid
    set tstart [clock milliseconds]
    set numculimp 0
    
    set culvertfile [open $importculverts(file) r]
    
    if { [file exists $importculverts(file)] } {
        for {set i 1} {$i <= $importculverts(numculverts)} {incr i} {
            set culvertdata ""
            set culvertdata [gets $culvertfile]
            set llarg [llength $culvertdata]
            
            Culvert::GetCulverts
            set current_value(culvertname) [lrange $culvertdata 12 end]
            set check [Culvert::Exists $current_value(culvertname)]
            if { $check == 0 } {
                set cb ""
                Culvert::NewCulvert $cb
                set culvert [Culvert::GetCurrentCulvert]
                
                set count 0
                foreach var $vars {
                    set var [lrange $vars $count $count]
                    set current_value($var) [lrange $culvertdata $count $count]
                    incr count
                }
                set current_value(start) [list $current_value(s1) $current_value(s2) $current_value(s3)]
                set current_value(end) [list $current_value(e1) $current_value(e2) $current_value(e3)]
                set current_value(visible) 1
                
                Culvert::Apply
                Culvert::RenameCulvertImported $current_value(culvertname)
                Culvert::FillProblemDataFromTclData
                incr numculimp
            } else {
                GidUtils::SetWarnLine [= "Culvert named '%s' already exists. Not imported" $current_value(culvertname)]
            }
            
        }
    } else {
        MessageBoxOk [= "Import process of culverts"] [= "Culvert file does not exists"] error
        return
    }
    
    Culvert::CloseWindow
    
    set tend [clock milliseconds]
    set elapsedtime [expr ($tend-$tstart)/1000.0]
    GidUtils::EndWaitState .gid
    
    Culvert::Window
    
    GidUtils::SetWarnLine [= "%s culverts imported successfully in %s seconds!" $numculimp $elapsedtime]
    close $culvertfile
}

