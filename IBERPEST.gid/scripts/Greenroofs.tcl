namespace eval GreenRoofs {
    variable data ;#array of values
    variable greenroofs ;#list of names of data
    variable current_greenroof ;#name of current selected greenroof
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {Start GASuctionGR GATotPorosGR GAInitialSatGR GAKsGR GAInitialLossGR GASoilDepthGR PoreidpercolGR KspercolGR sudsGR}
    variable fields_defaults { {} 20 0.5 0.1 10 0.0 1 0.0 0.2 0.0 }  
    variable drawopengl
    variable table ;#tktable widget
}

proc GreenRoofs::UnsetVariables { } {
    variable data
    variable greenroofs      
    unset -nocomplain data
    unset -nocomplain greenroofs
}

proc GreenRoofs::SetDefaultValues { } {    
    variable data
    variable greenroofs
    variable current_greenroof
    variable current_value
    variable fields
    variable fields_defaults
    array unset data
    set greenroofs {}
    foreach field $fields  {
	set current_value($field) ""
    }
    set current_greenroof {}
}

#store the greenroofs information in a hidden field of the problem data
#then this data is saved with the model without do nothing more
proc GreenRoofs::FillTclDataFromProblemData { } {
    variable data
    variable greenroofs
    if { [catch {set x [GiD_AccessValue get gendata GreenRoofsData]} msg] } {
	#reading a model created by an old version of the problemtype this gendata doesn't exists
	return
    }    
    array set data $x
    
    set greenroofs [list]
    foreach item [array names data *,Start] {
	lappend greenroofs [string range $item 0 end-6]
    }
    set greenroofs [lsort -dictionary $greenroofs]
    GreenRoofs::SetCurrentGreenRoof [lindex $greenroofs 0]
}

proc GreenRoofs::FillProblemDataFromTclData { } {
    variable data
    variable greenroofs
    set x [array get data]
    GiD_AccessValue set gendata GreenRoofsData $x
}

proc GreenRoofs::ExpandGIDSelection { sellist } {
    set result [list]
    foreach sel $sellist {
	if {[llength [set range [split $sel :]]]==1} {
	    lappend result $range
	} else {
	    foreach {from to} $range break
	    while {$from<$to} {
		lappend result $from
		incr from
	    }
	}
    }
    # you can also use: return $result
    set result
}

proc GreenRoofs::WriteCalculationFile { } {
    variable data
	variable sudsGR
	set res "" 
	
	append res [llength [GreenRoofs::GetGreenRoof]] \n
    set i 0
    foreach greenroof [GreenRoofs::GetGreenRoof] {
		incr i
		append res "Green roof $i: parameters" \n
		append res $i \n
		append res $data($greenroof,GASuctionGR)  
		append res " "
		append res $data($greenroof,GATotPorosGR)
		append res " "
		append res $data($greenroof,GAInitialSatGR)
		append res " "
		append res $data($greenroof,GAKsGR)
		append res " "
		append res $data($greenroof,GAInitialLossGR)
		append res " "
		append res $data($greenroof,GASoilDepthGR)
		append res " "
		append res $data($greenroof,PoreidpercolGR)
		append res " "
		append res $data($greenroof,KspercolGR)\n	 
    }
	append res "Number green of roofs"
	
	return $res
}

proc GreenRoofs::WriteNumberOfRoofs { } {
	set num_entities [GiD_Info conditions Assign_green_roof geometry -count] 
	append num_entities
	return $num_entities
}

proc GreenRoofs::WriteGreenRoofNumber { name } {
    variable number
    
    set number [expr [lsearch [GreenRoofs::GetGreenRoof] $name]+1]
    
    return $number    
}


proc GreenRoofs::GetGreenRoof { } {
    variable greenroofs
    if { ![info exists greenroofs] } {
	return ""
    }
    return $greenroofs
}

proc GreenRoofs::GetCurrentGreenRoof { } {
    variable current_greenroof
    return $current_greenroof
}

proc GreenRoofs::OnChangeSelectedGreenRoof { cb } {   
    GreenRoofs::SetCurrentGreenRoof [GreenRoofs::GetCurrentGreenRoof]
}

proc GreenRoofs::SetCurrentGreenRoof { greenroof } {
    variable data
    variable current_greenroof
    variable current_value
    variable fields   
    variable table
    #fill in the current_value variables with data
    if { $greenroof != "" } {
	foreach field $fields {
	    if { ![info exists data($greenroof,$field)] } {
		set data($greenroof,$field) 0
	    }
	    set current_value($field) $data($greenroof,$field)           
	}                
    } else {
	foreach field $fields {
	    set current_value($field) ""
	}
    }
    
    #the other widgets have a linked variable that automatically update it but the table not
    if { [info exists table] && [winfo exists $table] } {
	GridData::SetData $table $current_value(table_value)
    }
    set current_greenroof $greenroof  
}

proc GreenRoofs::GetIndex { greenroof } {
    variable greenroofs    
    return [lsearch $greenroofs $greenroof]
}

proc GreenRoofs::Exists { greenroof } {    
    if { [GreenRoofs::GetIndex $greenroof] != -1 } {
	set exists 1
    } else {
	set exists 0
    }
    return $exists
}

proc GreenRoofs::GetGreenRoofAutomaticName { } {
    set basename [_ "GreenRoof"]
    set i 1
    set greenroof $basename-$i
    while { [GreenRoofs::Exists $greenroof] } {        
	incr i
	set greenroof $basename-$i        
    }
    return $greenroof
}

proc GreenRoofs::NewGreenRoof { cb } {
    variable greenroofs
    variable data
    variable fields 
    variable fields_defaults
    set greenroof [GreenRoofs::GetGreenRoofAutomaticName]     
    
    if { [GreenRoofs::Exists $greenroof] } {
	#already exists
	return 1
    }
    foreach field $fields value $fields_defaults {
	set data($greenroof,$field) $value
    }
    lappend greenroofs $greenroof   
    if { [winfo exists $cb] } {
	$cb configure -values [GreenRoofs::GetGreenRoof]
    }
    GreenRoofs::SetCurrentGreenRoof $greenroof
    return 0
}

proc GreenRoofs::DeleteGreenRoof { cb } {
    variable greenroofs
    variable data
    variable fields 
    variable fields_defaults
    set greenroof [GreenRoofs::GetCurrentGreenRoof] 
    if { ![GreenRoofs::Exists $greenroof] } {
	#not exists
	return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete greenroof '%s'" $greenroof] \
	    gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {     
	set i [GreenRoofs::GetIndex $greenroof] 
	array unset data $greenroof,*        
	set greenroofs [lreplace $greenroofs $i $i]       
	if { [winfo exists $cb] } {
	    $cb configure -values [GreenRoofs::GetGreenRoof]
	}
	GreenRoofs::SetCurrentGreenRoof [lindex $greenroofs 0]  
	GiD_Redraw     
    }
    return 0
}

proc GreenRoofs::RenameGreenRoof { cb } {
    variable data
    variable greenroofs
    variable fields
    set greenroof [GreenRoofs::GetCurrentGreenRoof]   
    if { ![GreenRoofs::Exists $greenroof] } {
	#not exists
	return 1
    } 
    set new_name [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $greenroof] \
	    [= "Enter new name of %s '%s'" [= "greenroof"] $greenroof] gidquestionhead "any" ""]
    if { $new_name != "--CANCEL--" } {
	foreach field $fields {
	    set data($new_name,$field) $data($greenroof,$field)
	}
	array unset data $greenroof,*
	set i [GreenRoofs::GetIndex $greenroof] 
	lset greenroofs $i $new_name
	if { [winfo exists $cb] } {
	    $cb configure -values [GreenRoofs::GetGreenRoof]           
	}
	GreenRoofs::SetCurrentGreenRoof $new_name       
	GiD_Redraw 
    }
    return 0
}

#apply the values of the current window
proc GreenRoofs::Apply { } {
    variable data
    variable current_value
    variable fields
    
    set greenroof [GreenRoofs::GetCurrentGreenRoof]    
    foreach field $fields {
	set data($greenroof,$field) $current_value($field)
    }
    GiD_Redraw     
}

#not GreenRoofs::StartDraw and GreenRoofs::EndDraw are not used, the Draw button is hidden (because unregister fail in GiD 11.1.5d)
#and the redraw procedure is registered when loading the problemtype, and each greenroof is draw depending on its 'visible' variable value
proc GreenRoofs::StartDraw { bdraw} {
    variable drawopengl
    set drawopengl [GiD_OpenGL register GreenRoofs::ReDraw]
    GiD_Redraw
    $bdraw configure -text [= "Finish draw"] -command [list GreenRoofs::EndDraw $bdraw]
}

proc GreenRoofs::EndDraw { bdraw } {
    variable drawopengl
    if { [info exists drawopengl] } { 
	#unregister the callback tcl procedure to be called by GiD when redrawing
	GiD_OpenGL unregister $drawopengl
	catch {unset drawopengl}
	GiD_Redraw
	if { $bdraw != "" } {
	    $bdraw configure -text [= "Draw"] -command [list GreenRoofs::StartDraw $bdraw]
	}
    }
}

proc GreenRoofs::ReDraw { } {
    foreach greenroof [GreenRoofs::GetGreenRoof] {
	GreenRoofs::Draw $greenroof
    }
}

proc GreenRoofs::Draw { greenroof } {
    variable data
    
    set greenroof [GreenRoofs::GetCurrentGreenRoof]
    set view_mode [GiD_Info Project ViewMode]
    if { $view_mode == "GEOMETRYUSE" } {
	set Exf_surf [list]
	foreach elem [GiD_Info conditions Assign_exfiltration_area geometry] {
	    lappend Exf_surf [lindex $elem 1]
	    GiD_OpenGL drawentity -mode  filled surface $Exf_surf
	}
    } elseif { $view_mode == "MESHUSE"} {        
	set Exf_elems [list]
	foreach elem [GiD_Info conditions Assign_exfiltration_area mesh] {
	    lappend Exf_elems [lindex $elem 1]
	    GiD_OpenGL drawentity -mode filled element $Exf_elems        
	}
    }
    
    return 0
}



proc GreenRoofs::DestroyGreenRoof { W w } {   
    if { $W == $w } {
	GreenRoofs::EndDraw ""
	GreenRoofs::FillProblemDataFromTclData             
    }
}

proc GreenRoofs::OnChangeType { f } {
    variable current_value
    
}

proc GreenRoofs::PickSurfaceOrElementCmd { entry } {
    variable data
    variable current_greenroof
    
    set greenroof [GreenRoofs::GetCurrentGreenRoof]
    set view_mode [GiD_Info Project ViewMode]
    if { $view_mode == "GEOMETRYUSE" } {
	set sudsGR [GidUtils::PickEntities Surfaces multiple]
	GiD_AssignData condition Assign_exfiltration_area surfaces [GreenRoofs::WriteGreenRoofNumber $greenroof] $swmmroof
	#set surface_id $::GidPriv(selection)  
    } elseif { $view_mode == "MESHUSE"} {
	set sudsGR [GidUtils::PickEntities elements multiple]
	GiD_AssignData condition Assign_exfiltration_area elements [GreenRoofs::WriteGreenRoofNumber $greenroof] $swmmroof
	#set element_id $::GidPriv(selection)    
    } else {
	set sudsGR ""
    }
    
    
    if { $swmmroof != "" } {                    
	set res $swmmroof
	$entry delete 0 end
	$entry insert end $res        
    }
}


proc GreenRoofs::Window { } {
    variable data
    variable greenroofs
    variable current_greenroof
    variable current_value
    variable table
    
    if { ![info exists greenroofs] } {
	GreenRoofs::SetDefaultValues
    }
    
    GreenRoofs::FillTclDataFromProblemData    
    
    set w .gid.greenroof
    InitWindow $w [= "Green Roof definition"] PreGreenRoofWindowGeom GreenRoofs::Window
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    set f [ttk::frame $w.fgreenroofs]
    
    ttk::combobox $f.cb1 -textvariable GreenRoofs::current_greenroof -values [GreenRoofs::GetGreenRoof] -state readonly
    bind $f.cb1 <<ComboboxSelected>> [list GreenRoofs::OnChangeSelectedGreenRoof %W]
    ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list GreenRoofs::NewGreenRoof $f.cb1]
    GidHelp $f.bnew  [= "Create a new greenroof"]
    ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list GreenRoofs::DeleteGreenRoof $f.cb1]
    GidHelp $f.bdel  [= "Delete a greenroof"]
    ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list GreenRoofs::RenameGreenRoof $f.cb1]
    GidHelp $w.f.bren  [= "Rename a greenroof"]   
    grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
    grid $f -sticky new
	grid rowconfigure $f 0 -weight 1
    
	set f [ttk::frame $w.fgreenroof]        
    
	ttk::labelframe $w.fsoil -text [= "Soil layer - Infiltration"]
    foreach field {GASuctionGR GATotPorosGR GAInitialSatGR GAKsGR GAInitialLossGR GASoilDepthGR} text [list [= "Suction (mm)"] [= "Total porosity"] [= "Initial saturation"] [= "Ks (mm/h)"] [= "Initial loss (mm)"] [= "Soil depth (m)"]] {
		ttk::label $w.fsoil.l$field -text $text
		ttk::entry $w.fsoil.e$field -textvariable GreenRoofs::current_value($field)
		grid $w.fsoil.l$field $w.fsoil.e$field -sticky new
		GidHelp $w.fsoil.eGASuctionGR  [= "Suction"]
		GidHelp $w.fsoil.eGATotPorosGR [= "Total porosity"]
		GidHelp $w.fsoil.eGAInitialLossGR [= "Initial saturation"]
		GidHelp $w.fsoil.eGAKsGR [= "Ks (mm/h)"]
		GidHelp $w.fsoil.eGAInitialLossGR [= "Initial loss (mm)"]
		GidHelp $w.fsoil.eGASoilDepthGR [= "Soil depth (m)"]
		grid $w.fsoil.l$field $w.fsoil.e$field -sticky ew
	}
	grid $w.fsoil -sticky new
	grid columnconfigure $w.fsoil {1} -weight 1 

	ttk::labelframe $w.fstore -text [= "Storage layer - Percolation"]
	    foreach field {PoreidpercolGR KspercolGR} text [list [= "Pore distribution index"] [= "Ks percolation (mm/h"]] {
		ttk::label $w.fstore.l$field -text $text
		ttk::entry $w.fstore.e$field -textvariable GreenRoofs::current_value($field)
		grid $w.fstore.l$field $w.fstore.e$field -sticky new
		GidHelp $w.fstore.PoreidpercolGR [= "Pore distribution index"]
		GidHelp $w.fstore.KspercolGR [= "Ks percolation (mm/h)"]
    } 
	grid $w.fstore -sticky new
    grid columnconfigure $w.fstore {1} -weight 1
    
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.bapply -text [= "Apply"] -command [list GreenRoofs::Apply] -style BottomFrame.TButton
    #ttk::button $f.bdraw -text [= "Draw"] -command [list GreenRoofs::StartDraw $f.bdraw] -style BottomFrame.TButton
    ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    #grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
    grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }      
    
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    bind $w <Alt-c> "$f.bclose invoke"
    bind $w <Escape> "$f.bclose invoke"   
    bind $w <Destroy> [list +GreenRoofs::DestroyGreenRoof %W $w] ;# + to add to previous script  
}

proc GreenRoofs::CloseWindow { } {
    set w .gid.greenroof
    if { [winfo exists $w] } {
	close $w
    }
}
