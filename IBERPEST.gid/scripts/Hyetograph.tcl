namespace eval Hyetograph {
    variable data ;#array of values
    variable hyetographs ;#list of names of data
    variable current_hyetograph ;#name of current selected hyetograph
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {visible location table_value}
    variable fields_defaults {1 {0 0 0} {0.0 0.0}}  
    variable drawopengl
    variable table ;#tktable widget
}

proc Hyetograph::UnsetVariables { } {
    variable data
    variable hyetographs      
    unset -nocomplain data
    unset -nocomplain hyetographs
}

proc Hyetograph::SetDefaultValues { } {    
    variable data
    variable hyetographs
    variable current_hyetograph
    variable current_value
    variable fields
    variable fields_defaults
    array unset data
    set hyetographs {}
    foreach field $fields  {
        set current_value($field) ""
    }
    set current_value(table_value) {0.0 0.0}
    set current_hyetograph {}
}

#store the hyetographs information in a hidden field of the problem data
#then this data is saved with the model without do nothing more
proc Hyetograph::FillTclDataFromProblemData { } {
    variable data
    variable hyetographs
    if { [catch {set x [GiD_AccessValue get gendata HyetographsData]} msg] } {
        #reading a model created by an old version of the problemtype this gendata doesn't exists
        return
    }    
    array set data $x
    
    set hyetographs [list]
    foreach item [array names data *,table_value] {
        lappend hyetographs [string range $item 0 end-12]
    }
    set hyetographs [lsort -dictionary $hyetographs]
    Hyetograph::SetCurrentHyetograph [lindex $hyetographs 0]
}

proc Hyetograph::FillProblemDataFromTclData { } {
    variable data
    variable hyetographs
    set x [array get data]
    GiD_AccessValue set gendata HyetographsData $x
}

proc Hyetograph::WriteCalculationFile { } {
    variable data
    set res ""       
    
    set i 0
    append res "Hyetographs"  \n
    append res [llength [Hyetograph::GetHyetograph]] \n
    foreach hyetograph [Hyetograph::GetHyetograph] {
        incr i   
        append res $i  \n                
        append res [lindex $data($hyetograph,location) 0]
        append res " "
        append res [lindex $data($hyetograph,location) 1] \n
        set lines [expr [llength $data($hyetograph,table_value)]/2]
        append res $lines  \n
        set l [expr $lines*2]
        for {set ii 0} {$ii < $l} {set ii [expr $ii+2]} {
            append res "[lrange $data($hyetograph,table_value) $ii [expr $ii+1]] " \n        
        }       
    }
    append res "End"    
    return $res
}

proc Hyetograph::WriteHyetographNumber { name } {
    variable number
    
    set number [expr [lsearch [Hyetograph::GetHyetograph] $name]+1]
    
    return $number    
}

proc Hyetograph::GetHyetograph { } {
    variable hyetographs
    if { ![info exists hyetographs] } {
        return ""
    }
    return $hyetographs
}

proc Hyetograph::GetCurrentHyetograph { } {
    variable current_hyetograph
    return $current_hyetograph
}

proc Hyetograph::OnChangeSelectedHyetograph { cb } {   
    Hyetograph::SetCurrentHyetograph [Hyetograph::GetCurrentHyetograph]
}

proc Hyetograph::SetCurrentHyetograph { hyetograph } {
    variable data
    variable current_hyetograph
    variable current_value
    variable fields   
    variable table
    #fill in the current_value variables with data
    if { $hyetograph != "" } {
        foreach field $fields {
            if { ![info exists data($hyetograph,$field)] } {
                set data($hyetograph,$field) 0
            }
            set current_value($field) $data($hyetograph,$field)           
        }                
    } else {
        foreach field $fields {
            set current_value($field) ""
        }
    }
    
    #the other widgets have a linked variable that automatically update it but the table not
    if { [info exists table] && [winfo exists $table] } {
        GridData::SetData $table $current_value(table_value)
    }
    set current_hyetograph $hyetograph  
}

proc Hyetograph::GetIndex { hyetograph } {
    variable hyetographs    
    return [lsearch $hyetographs $hyetograph]
}

proc Hyetograph::Exists { hyetograph } {    
    if { [Hyetograph::GetIndex $hyetograph] != -1 } {
        set exists 1
    } else {
        set exists 0
    }
    return $exists
}

proc Hyetograph::GetHyetographAutomaticName { } {
    set basename [_ "hyetograph"]
    set i 1
    set hyetograph $basename-$i
    while { [Hyetograph::Exists $hyetograph] } {        
        incr i
        set hyetograph $basename-$i        
    }
    return $hyetograph
}

proc Hyetograph::NewHyetograph { cb } {
    variable hyetographs
    variable data
    variable fields 
    variable fields_defaults
    set hyetograph [Hyetograph::GetHyetographAutomaticName]     
    
    if { [Hyetograph::Exists $hyetograph] } {
        #already exists
        return 1
    }
    foreach field $fields value $fields_defaults {
        set data($hyetograph,$field) $value
    }
    lappend hyetographs $hyetograph   
    if { [winfo exists $cb] } {
        $cb configure -values [Hyetograph::GetHyetograph]
    }
    Hyetograph::SetCurrentHyetograph $hyetograph
    return 0
}

proc Hyetograph::DeleteHyetograph { cb } {
    variable hyetographs
    variable data
    variable fields 
    variable fields_defaults
    set hyetograph [Hyetograph::GetCurrentHyetograph] 
    if { ![Hyetograph::Exists $hyetograph] } {
        #not exists
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete hyetograph '%s'" $hyetograph] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {     
        set i [Hyetograph::GetIndex $hyetograph] 
        array unset data $hyetograph,*        
        set hyetographs [lreplace $hyetographs $i $i]       
        if { [winfo exists $cb] } {
            $cb configure -values [Hyetograph::GetHyetograph]
        }
        Hyetograph::SetCurrentHyetograph [lindex $hyetographs 0]  
        GiD_Redraw     
    }
    return 0
}

proc Hyetograph::RenameHyetograph { cb } {
    variable data
    variable hyetographs
    variable fields
    set hyetograph [Hyetograph::GetCurrentHyetograph]   
    if { ![Hyetograph::Exists $hyetograph] } {
        #not exists
        return 1
    } 
    set new_name [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $hyetograph] \
            [= "Enter new name of %s '%s'" [= "hyetograph"] $hyetograph] gidquestionhead "any" ""]
    if { $new_name != "--CANCEL--" } {
        foreach field $fields {
            set data($new_name,$field) $data($hyetograph,$field)
        }
        array unset data $hyetograph,*
        set i [Hyetograph::GetIndex $hyetograph] 
        lset hyetographs $i $new_name
        if { [winfo exists $cb] } {
            $cb configure -values [Hyetograph::GetHyetograph]           
        }
        Hyetograph::SetCurrentHyetograph $new_name       
        GiD_Redraw 
    }
    return 0
}

#apply the values of the current window
proc Hyetograph::Apply { T } {
    variable data
    variable current_value
    variable fields
    
    set hyetograph [Hyetograph::GetCurrentHyetograph]    
    foreach field $fields {
        set data($hyetograph,$field) $current_value($field)
    }
    
    set data($hyetograph,table_value) [GridData::GetDataAllItems $T]
    GiD_Redraw     
}

#not Hyetograph::StartDraw and Hyetograph::EndDraw are not used, the Draw button is hidden (because unregister fail in GiD 11.1.5d)
#and the redraw procedure is registered when loading the problemtype, and each hyetograph is draw depending on its 'visible' variable value
proc Hyetograph::StartDraw { bdraw} {
    variable drawopengl
    set drawopengl [GiD_OpenGL register Hyetograph::ReDraw]
    GiD_Redraw
    $bdraw configure -text [= "Finish draw"] -command [list Hyetograph::EndDraw $bdraw]
}

proc Hyetograph::EndDraw { bdraw } {
    variable drawopengl
    if { [info exists drawopengl] } { 
        #unregister the callback tcl procedure to be called by GiD when redrawing
        GiD_OpenGL unregister $drawopengl
        catch {unset drawopengl}
        GiD_Redraw
        if { $bdraw != "" } {
            $bdraw configure -text [= "Draw"] -command [list Hyetograph::StartDraw $bdraw]
        }
    }
}

proc Hyetograph::ReDraw { } {
    foreach hyetograph [Hyetograph::GetHyetograph] {
        Hyetograph::Draw $hyetograph
    }
}

proc Hyetograph::Draw { hyetograph } {
    variable data
    if { $hyetograph == "" || ![info exists data($hyetograph,visible)] || !$data($hyetograph,visible) } {
        return 1
    }
    foreach field {location} {
        if { [llength $data($hyetograph,$field)] != 3 } {
            return 1
        }
        foreach v $data($hyetograph,$field) {
            if { ![string is double $v] } {
                return 1
            }
        }
    }    
    set blue {0 0 1}
    GiD_OpenGL draw -color $blue
    GiD_OpenGL draw -pointsize 5
    GiD_OpenGL draw -begin points
    GiD_OpenGL draw -vertex $data($hyetograph,location)    
    GiD_OpenGL draw -end
    
    #show label
    #increase a little z to try to draw the label over the mesh
    set p [MathUtils::VectorSum $data($hyetograph,location) {0.0 0.0 0.0001}]
    GiD_OpenGL draw -rasterpos $p
    
    set txt [concat $hyetograph ([= "hyetograph"])]
    
    GiD_OpenGL drawtext $txt
    return 0
}

proc Hyetograph::DestroyHyetographWindow { W w } {   
    if { $W == $w } {
        Hyetograph::EndDraw ""
        Hyetograph::FillProblemDataFromTclData             
    }
}

proc Hyetograph::OnChangeType { f } {
    variable current_value
    
}

proc Hyetograph::PickPointInSurfaceOrElementCmd { entry } {
    set view_mode [GiD_Info Project ViewMode]
    if { $view_mode == "GEOMETRYUSE" } {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an surface"] PointInSurface GEOMETRYUSE]
        #set surface_id $::GidPriv(selection)  
    } elseif { $view_mode == "MESHUSE"} {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an element"] PointInElement MESHUSE]
        #set element_id $::GidPriv(selection)    
    } else {
        set xyz ""
    }        
    if { $xyz != "" } {                    
        set res [format "%g %g %g" {*}$xyz]
        $entry delete 0 end
        $entry insert end $res        
    }
}

proc Hyetograph::Window { } {
    variable data
    variable hyetographs
    variable current_hyetograph
    variable current_value
    variable table
    
    if { ![info exists hyetographs] } {
        Hyetograph::SetDefaultValues
    }
    
    Hyetograph::FillTclDataFromProblemData    
    
    set w .gid.hyetograph
    InitWindow $w [= "Hyetograph"] PreHyetographWindowGeom Hyetograph::Window
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    set f [ttk::frame $w.fhyetographs]
    
    ttk::combobox $f.cb1 -textvariable Hyetograph::current_hyetograph -values [Hyetograph::GetHyetograph] -state readonly
    bind $f.cb1 <<ComboboxSelected>> [list Hyetograph::OnChangeSelectedHyetograph %W]
    ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list Hyetograph::NewHyetograph $f.cb1]
    GidHelp $f.bnew  [= "Create a new hyetograph"]
    ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list Hyetograph::DeleteHyetograph $f.cb1]
    GidHelp $f.bdel  [= "Delete a hyetograph"]
    ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list Hyetograph::RenameHyetograph $f.cb1]
    GidHelp $w.f.bren  [= "Rename a hyetograph"]   
    grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
    grid $f -sticky new
    grid rowconfigure $f 0 -weight 1
    
    set f [ttk::frame $w.fhyetograph]        
    set field visible
    ttk::checkbutton $f.ch$field -text [= "Visible"] -variable Hyetograph::current_value($field)
    grid $f.ch$field -sticky w
    
    foreach field {location} text [list [= "Location"]] {
        ttk::label $f.l$field -text $text
        ttk::entry $f.e$field -textvariable Hyetograph::current_value($field)
        ttk::button $f.b$field -image [gid_themes::GetImage "point.png" small_icons] -command [list Hyetograph::PickPointInSurfaceOrElementCmd $f.e$field]
        grid $f.l$field $f.e$field $f.b$field -sticky ew
    }
    
    
    grid $f -sticky new
    grid columnconfigure $f {1} -weight 1
    
    package require fulltktree
    set columns ""
    foreach name [list [= "Time (s)"] [= "I (mm/h)"]] {
        lappend columns [list 4 $name left text 1]
    }
    set T [fulltktree $f.t -columns $columns -showlines 0 -showheader 1 -expand 1]    
    set table $T
    $T configure -editbeginhandler GridData::EditBegin
    $T configure -editaccepthandler [list GridData::EditAccept]
    $T configure -deletehandler GridData::RemoveRows
    $T configure -contextualhandler_menu GridData::ContextualMenu    
    $T column configure all -button 0
    
    ttk::frame $f.fbuttons
    ttk::button $f.fbuttons.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $T 1]
    GidHelp $f.fbuttons.b1 [_ "Pressing this button, one new line will be added to this window"]    
    ttk::button $f.fbuttons.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $T 1]
    GidHelp $f.fbuttons.b2 [_ "Pressing this button, one line is deleted from this window"]        
    
    ttk::button $f.fbuttons.b3 -image [gid_themes::GetImage graphs.png small_icons] -text [_ "Graph"] \
        -command [list GridData::PlotCurveWin $T [= "Hyetograph"] ""]
    GidHelp $f.fbuttons.b3 [_ "Pressing this button, a XY graph will be drawn"]
    
    grid $T -sticky nsew -columnspan 2
    grid $f.fbuttons.b1 $f.fbuttons.b2 $f.fbuttons.b3 -sticky w    
    grid $f.fbuttons -sticky ew
    
    
    
    grid columnconfigure $f {0} -weight 1
    grid rowconfigure $f {1 4} -weight 1
    grid $f -sticky nsew -padx 2 -pady 2
    GidHelp $f [= "Set hyetograph parameters"]
    
    bind $T <<Paste>> [list ::GridData::Paste $T CLIPBOARD]
    bind $T <<PasteSelection>> [list ::GridData::Paste $T PRIMARY]
    bind $T <<Cut>> [list GridData::Cut $T]
    bind $T <<Copy>> [list GridData::Copy $T]
    bind [$T givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header
    
    GridData::SetData $T $current_value(table_value)
    
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.bapply -text [= "Apply"] -command [list Hyetograph::Apply $T] -style BottomFrame.TButton
    #ttk::button $f.bdraw -text [= "Draw"] -command [list Hyetograph::StartDraw $f.bdraw] -style BottomFrame.TButton
    ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    #grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
    grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }        
    
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    bind $w <Alt-c> "$f.bclose invoke"
    bind $w <Escape> "$f.bclose invoke"   
    bind $w <Destroy> [list +Hyetograph::DestroyHyetographWindow %W $w] ;# + to add to previous script  
}

proc Hyetograph::CloseWindow { } {
    set w .gid.hyetograph
    if { [winfo exists $w] } {
        close $w
    }
}
