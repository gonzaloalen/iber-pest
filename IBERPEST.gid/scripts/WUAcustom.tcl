# Iber Pre-process tool: WUA custom (curves defined by the user)
# --- Marcos 2018-11-9 ---

#                                                                                                        #
#                ##        ########        #########        ########                #
#                ##        ##                #        ##                        ##                #                #
#                ##        ##                #        ##                        ##                #                #
#                ##        ##                #        ##                        ##                #                #
#                ##        ##           #        #####                ##           #                #
#                ##        ##                #        ##                        ##          #                        #
#                ##        ##                #        ##                        ##          #                        #
#                ##        ##                #        ##                        ##           #                #
#                ##        ########        #########        ##                #                #
#                                                                                                        #                                                                                                  

namespace eval WUAcustom {
    variable data ;#array of values
    variable curves ;#list of names of data
    variable current_curve ;#name of current selected curve
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {table_value1 table_value2}
    variable fields_defaults {{0.0 0.0} {0.0 0.0}}  
    variable drawopengl
    variable table1 ;#tktable widget
    variable table2 ;#tktable widget
}

proc WUAcustom::UnsetVariables { } {
    variable data
    variable curves      
    unset -nocomplain data
    unset -nocomplain curves
}

proc WUAcustom::SetDefaultValues { } {    
    variable data
    variable curves
    variable current_curve
    variable current_value
    variable fields
    variable fields_defaults
    array unset data
    set curves {}
    foreach field $fields field_default $fields_defaults {
        set current_value($field) ""
    }
    set current_curve {}
}

#store the curves information in a hidden field of the problem data
#then this data is saved with the model without do nothing more
proc WUAcustom::FillTclDataFromProblemData { } {
    variable data
    variable curves
    
    if { [catch {set x [GiD_AccessValue get gendata SuitabilityData]} msg] } {
        #reading a model created by an old version of the problemtype this gendata doesn't exists
        return
    }    
    array set data $x
    
    set curves [list]
    foreach item [array names data *,table_value1] {
        lappend curves [string range $item 0 end-13]
    } 
    set curves [lsort -dictionary $curves]
    WUAcustom::SetCurrentCurve [lindex $curves 0]
}

proc WUAcustom::FillProblemDataFromTclData { } {
    variable data
    set x [array get data]
    GiD_AccessValue set gendata SuitabilityData $x
}

proc WUAcustom::WriteCalculationFile { } {
    variable data
    set res ""       
    
    set i 0
    append res "Custom suitability curves" \n
    append res [llength [WUAcustom::GetCurves]] \n
    foreach curve [WUAcustom::GetCurves] {
        incr i        
        append res $i  \n
        append res $curve \n
        set lines [expr [llength $data($curve,table_value1)]/2]
        append res $lines  \n
        set l [expr $lines*2]
        for {set ii 0} {$ii < $l} {set ii [expr $ii+2]} {
            append res "[lrange $data($curve,table_value1) $ii [expr $ii+1]] " \n        
        }       
        set lines [expr [llength $data($curve,table_value2)]/2]
        append res $lines  \n
        set l [expr $lines*2]
        for {set ii 0} {$ii < $l} {set ii [expr $ii+2]} {
            append res "[lrange $data($curve,table_value2) $ii [expr $ii+1]] " \n        
        }       
    }
    
    return $res
}

proc WUAcustom::WriteCurveNumber { name } {
    variable number
    
    set number [expr [lsearch [WUAcustom::GetCurves] $name]+1]
    
    return $number    
}


proc WUAcustom::GetCurves { } {
    variable curves
    if { ![info exists curves] } {
        return ""
    }
    return $curves
}

proc WUAcustom::GetCurrentCurve { } {
    variable current_curve
    return $current_curve
}

proc WUAcustom::OnChangeSelectedCurve { cb } {   
    WUAcustom::SetCurrentCurve [WUAcustom::GetCurrentCurve]
}

proc WUAcustom::SetCurrentCurve { curve } {
    variable data
    variable current_curve
    variable current_value
    variable fields   
    variable table1
    variable table2
    #fill in the current_value variables with data
    if { $curve != "" } {
        foreach field $fields {
            if { ![info exists data($curve,$field)] } {
                set data($curve,$field) 0
            }
            set current_value($field) $data($curve,$field)           
        }                
    } else {
        foreach field $fields {
            set current_value($field) ""
        }
    }
    
    #the other widgets have a linked variable that automatically update it but the table not
    if { [info exists table1] && [winfo exists $table1] } {
        GridData::SetData $table1 $current_value(table_value1)
    }
    if { [info exists table2] && [winfo exists $table2] } {
        GridData::SetData $table2 $current_value(table_value2)
    }
    set current_curve $curve  
}

proc WUAcustom::GetIndex { curve } {
    variable curves    
    return [lsearch $curves $curve]
}

proc WUAcustom::Exists { curve } {    
    if { [WUAcustom::GetIndex $curve] != -1 } {
        set exists 1
    } else {
        set exists 0
    }
    return $exists
}

proc WUAcustom::GetCurveAutomaticName { } {
    set basename [_ "Fish and stage"]
    set i 1
    set curve $basename-$i
    while { [WUAcustom::Exists $curve] } {        
        incr i
        set curve $basename-$i        
    }
    return $curve
}

proc WUAcustom::NewCurve { cb } {
    variable curves
    variable data
    variable fields 
    variable fields_defaults
    set curve [WUAcustom::GetCurveAutomaticName]     
    
    if { [WUAcustom::Exists $curve] } {
        #already exists
        return 1
    }
    foreach field $fields value $fields_defaults {
        set data($curve,$field) $value
    }
    lappend curves $curve   
    if { [winfo exists $cb] } {
        $cb configure -values [WUAcustom::GetCurves]
    }
    WUAcustom::SetCurrentCurve $curve
    return 0
}

proc WUAcustom::DeleteCurve { cb } {
    variable curves
    variable data
    variable fields 
    variable fields_defaults
    set curve [WUAcustom::GetCurrentCurve] 
    if { ![WUAcustom::Exists $curve] } {
        #not exists
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete curve '%s'" $curve] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {     
        set i [WUAcustom::GetIndex $curve] 
        array unset data $curve,*        
        set curves [lreplace $curves $i $i]       
        if { [winfo exists $cb] } {
            $cb configure -values [WUAcustom::GetCurves]
        }
        WUAcustom::SetCurrentCurve [lindex $curves 0]  
        GiD_Redraw     
    }
    return 0
}

proc WUAcustom::RenameCurve { cb } {
    variable data
    variable curves
    variable fields
    set curve [WUAcustom::GetCurrentCurve]   
    if { ![WUAcustom::Exists $curve] } {
        #not exists
        return 1
    } 
    set new_name [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $curve] \
            [= "Enter new name of %s '%s'" [= "curve"] $curve] gidquestionhead "any" ""]
    if { $new_name != "--CANCEL--" } {
        foreach field $fields {
            set data($new_name,$field) $data($curve,$field)
        }
        array unset data $curve,*
        set i [WUAcustom::GetIndex $curve] 
        lset curves $i $new_name
        if { [winfo exists $cb] } {
            $cb configure -values [WUAcustom::GetCurves]           
        }
        WUAcustom::SetCurrentCurve $new_name       
        GiD_Redraw 
    }
    return 0
}

#apply the values of the current window
proc WUAcustom::Apply { T1 T2 } {
    variable data
    set curve [WUAcustom::GetCurrentCurve]
    set data($curve,table_value1) [GridData::GetDataAllItems $T1]
    set data($curve,table_value2) [GridData::GetDataAllItems $T2]
}

#not WUAcustom::StartDraw and WUAcustom::EndDraw are not used, the Draw button is hidden (because unregister fail in GiD 11.1.5d)
#and the redraw procedure is registered when loading the problemtype, and each curve is draw depending on its 'visible' variable value
proc WUAcustom::StartDraw { bdraw} {
    variable drawopengl
    set drawopengl [GiD_OpenGL register WUAcustom::ReDraw]
    GiD_Redraw
    $bdraw configure -text [= "Finish draw"] -command [list WUAcustom::EndDraw $bdraw]
}

proc WUAcustom::EndDraw { bdraw } {
    variable drawopengl
    if { [info exists drawopengl] } { 
        #unregister the callback tcl procedure to be called by GiD when redrawing
        GiD_OpenGL unregister $drawopengl
        catch {unset drawopengl}
        GiD_Redraw
        if { $bdraw != "" } {
            $bdraw configure -text [= "Draw"] -command [list WUAcustom::StartDraw $bdraw]
        }
    }
}

proc WUAcustom::DestroyCurveWindow { W w } {   
    if { $W == $w } {
        WUAcustom::EndDraw ""
        WUAcustom::FillProblemDataFromTclData             
    }
}

proc WUAcustom::Window { } {
    variable data
    variable curves
    variable current_curve
    variable current_value
    variable table1
    variable table2
    
    package require fulltktree
    
    if { ![info exists curves] } {
        WUAcustom::SetDefaultValues
    }
    
    WUAcustom::FillTclDataFromProblemData    
    
    set w .gid.curve
    InitWindow $w [= "Fish suitability"] PreSuitabilityWindowGeom WUAcustom::Window
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    set f [ttk::frame $w.fcurves]
    
    ttk::combobox $f.cb1 -textvariable WUAcustom::current_curve -values [WUAcustom::GetCurves] -state readonly
    bind $f.cb1 <<ComboboxSelected>> [list WUAcustom::OnChangeSelectedCurve %W]
    ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list WUAcustom::NewCurve $f.cb1]
    GidHelp $f.bnew  [= "Create a new curve (use '_' as words separator)"]
    ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list WUAcustom::DeleteCurve $f.cb1]
    GidHelp $f.bdel  [= "Delete a curve"]
    ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list WUAcustom::RenameCurve $f.cb1]
    GidHelp $w.f.bren  [= "Rename a curve"]   
    grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
    grid $f -sticky new
    grid rowconfigure $f 0 -weight 1
    
    set f [ttk::frame $w.fcurve]  
    
    #         Tabla 1: velocidad
    
    set columns ""
    foreach name [list [= "Velocity (m/s)"] [= "Suitability"]] {
        lappend columns [list 4 $name left text 1]
    }
    set T1 [fulltktree $f.t1 -columns $columns -showlines 0 -showheader 1 -expand 1]    
    set table1 $T1
    $T1 configure -editbeginhandler GridData::EditBegin
    $T1 configure -editaccepthandler [list GridData::EditAccept]
    $T1 configure -deletehandler GridData::RemoveRows
    $T1 configure -contextualhandler_menu GridData::ContextualMenu    
    $T1 column configure all -button 0
    
    ttk::frame $f.fbuttons1
    ttk::button $f.fbuttons1.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $T1 1]
    GidHelp $f.fbuttons1.b1 [_ "Pressing this button, one new line will be added to this window"]    
    ttk::button $f.fbuttons1.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $T1 1]
    GidHelp $f.fbuttons1.b2 [_ "Pressing this button, one line is deleted from this window"]        
    
    ttk::button $f.fbuttons1.b3 -image [gid_themes::GetImage graphs.png small_icons] -text [_ "Graph"] \
        -command [list GridData::PlotCurveWin $T1 [= "Curve"] ""]
    GidHelp $f.fbuttons1.b3 [_ "Pressing this button, a XY graph will be drawn"]
    
    grid $T1 -sticky nsew -columnspan 2
    grid $f.fbuttons1.b1 $f.fbuttons1.b2 $f.fbuttons1.b3 -sticky w    
    grid $f.fbuttons1 -sticky ew
    #   grid columnconfigure $f {0} -weight 1
    grid rowconfigure $f {1 4} -weight 1
    grid $f -sticky nsew -padx 2 -pady 2
    GidHelp $f [= "Set curve parameters"]
    
    bind $T1 <<Paste>> [list ::GridData::Paste $T1 CLIPBOARD]
    bind $T1 <<PasteSelection>> [list ::GridData::Paste $T1 PRIMARY]
    bind $T1 <<Cut>> [list GridData::Cut $T1]
    bind $T1 <<Copy>> [list GridData::Copy $T1]
    bind [$T1 givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header
    
    GridData::SetData $T1 $current_value(table_value1)
    
    #         Tabla 2: calado
    set columns ""
    foreach name [list [= "Depth (m)"] [= "Suitability"]] {
        lappend columns [list 4 $name left text 1]
    }
    set T2 [fulltktree $f.t2 -columns $columns -showlines 0 -showheader 1 -expand 1]    
    set table2 $T2
    $T2 configure -editbeginhandler GridData::EditBegin
    $T2 configure -editaccepthandler [list GridData::EditAccept]
    $T2 configure -deletehandler GridData::RemoveRows
    $T2 configure -contextualhandler_menu GridData::ContextualMenu    
    $T2 column configure all -button 0
    
    ttk::frame $f.fbuttons2
    ttk::button $f.fbuttons2.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $T2 1]
    GidHelp $f.fbuttons2.b1 [_ "Pressing this button, one new line will be added to this window"]    
    ttk::button $f.fbuttons2.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $T2 1]
    GidHelp $f.fbuttons2.b2 [_ "Pressing this button, one line is deleted from this window"]        
    
    ttk::button $f.fbuttons2.b3 -image [gid_themes::GetImage graphs.png small_icons] -text [_ "Graph"] \
        -command [list GridData::PlotCurveWin $T2 [= "Curve"] ""]
    GidHelp $f.fbuttons2.b3 [_ "Pressing this button, a XY graph will be drawn"]
    
    grid $T2 -sticky nsew -columnspan 2
    grid $f.fbuttons2.b1 $f.fbuttons2.b2 $f.fbuttons2.b3 -sticky w    
    grid $f.fbuttons2 -sticky ew
    #   grid columnconfigure $f {0} -weight 1
    grid rowconfigure $f {1 4} -weight 1
    grid $f -sticky nsew -padx 2 -pady 2
    GidHelp $f [= "Set curve parameters"]
    
    bind $T2 <<Paste>> [list ::GridData::Paste $T2 CLIPBOARD]
    bind $T2 <<PasteSelection>> [list ::GridData::Paste $T2 PRIMARY]
    bind $T2 <<Cut>> [list GridData::Cut $T2]
    bind $T2 <<Copy>> [list GridData::Copy $T2]
    bind [$T2 givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header
    
    GridData::SetData $T2 $current_value(table_value2)
    
    grid $f -sticky new
    grid columnconfigure $f {2} -weight 1
    
    
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.bapply -text [= "Apply"] -command [list WUAcustom::Apply $T1 $T2] -style BottomFrame.TButton
    #ttk::button $f.bdraw -text [= "Draw"] -command [list WUAcustom::StartDraw $f.bdraw] -style BottomFrame.TButton
    ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    #grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
    grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }        
    
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    bind $w <Alt-c> "$f.bclose invoke"
    bind $w <Escape> "$f.bclose invoke"   
    bind $w <Destroy> [list +WUAcustom::DestroyCurveWindow %W $w] ;# + to add to previous script  
}


proc WUAcustom::CloseWindow { } {
    set w .gid.curve
    if { [winfo exists $w] } {
        close $w
    }
}
