namespace eval SoilWaterStress {
    variable data ;#array of values
    variable crops ;#list of names of data
    variable current_crop ;#name of current selected crop
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {field_cap wilting_p depletion_f table_value}
    variable fields_defaults {0.0 0.0 0.0 {0.0 0.0}}  
    variable drawopengl
    variable table ;#tktable widget
}

proc SoilWaterStress::UnsetVariables { } {
    variable data
    variable crops      
    unset -nocomplain data
    unset -nocomplain crops
}

proc SoilWaterStress::SetDefaultValues { } {    
    variable data
    variable crops
    variable current_crop
    variable current_value
    variable fields
    variable fields_defaults
    array unset data
    set crops {}
    foreach field $fields  {
        set current_value($field) ""
    }
    set current_value(table_value) {0.0 0.0}
    set current_crop {}
}

#store the zones information in a hidden field of the problem data
#then this data is saved with the model without do nothing more
proc SoilWaterStress::FillTclDataFromProblemData { } {
    variable data
    variable crops
    if { [catch {set x [GiD_AccessValue get gendata SoilWaterStressData]} msg] } {
        #reading a model created by an old version of the problemtype this gendata doesn't exists
        return
    }    
    array set data $x
    
    set crops [list]
    foreach item [array names data *,table_value] {
        lappend crops [string range $item 0 end-12]
    }
    set crops [lsort -dictionary $crops]
    SoilWaterStress::SetCurrentCrop [lindex $crops 0]
}

proc SoilWaterStress::FillProblemDataFromTclData { } {
    variable data
    variable crops
    set x [array get data]
    GiD_AccessValue set gendata SoilWaterStressData $x
}

proc SoilWaterStress::WriteCalculationFile { } {
    variable data
    set res ""       
    
    set i 0
    append res "Define Crops"  \n
    append res [llength [SoilWaterStress::GetCrops]] \n
    foreach crop [SoilWaterStress::GetCrops] {
        incr i       
        append res $i  \n
        append res $data($crop,field_cap)
        append res " "
        append res $data($crop,wilting_p)        
        append res " "
        append res $data($crop,depletion_f) \n        
        set lines [expr [llength $data($crop,table_value)]/2]
        append res $lines  \n
        set l [expr $lines*2]
        for {set ii 0} {$ii < $l} {set ii [expr $ii+2]} {
            append res "[lrange $data($crop,table_value) $ii [expr $ii+1]] " \n        
        }   
    }
    append res "Assign crops"
    return $res
}

proc SoilWaterStress::WriteCropNumber { name } {
    variable number
    
    set number [expr [lsearch [SoilWaterStress::GetCrops] $name]+1]
    
    return $number    
}

proc SoilWaterStress::GetCrops { } {
    variable crops
    if { ![info exists crops] } {
        return ""
    }
    return $crops
}

proc SoilWaterStress::GetCurrentCrop { } {
    variable current_crop
    return $current_crop
}

proc SoilWaterStress::OnChangeSelectedCrop { cb } {   
    SoilWaterStress::SetCurrentCrop [SoilWaterStress::GetCurrentCrop]
}

proc SoilWaterStress::SetCurrentCrop { crop } {
    variable data
    variable current_crop
    variable current_value
    variable fields   
    variable table
    #fill in the current_value variables with data
    if { $crop != "" } {
        foreach field $fields {
            if { ![info exists data($crop,$field)] } {
                set data($crop,$field) 0
            }
            set current_value($field) $data($crop,$field)           
        }                
    } else {
        foreach field $fields {
            set current_value($field) ""
        }
    }
    
    #the other widgets have a linked variable that automatically update it but the table not
    if { [info exists table] && [winfo exists $table] } {
        GridData::SetData $table $current_value(table_value)
    }
    set current_crop $crop  
}

proc SoilWaterStress::GetIndex { crop } {
    variable crops    
    return [lsearch $crops $crop]
}

proc SoilWaterStress::Exists { crop } {    
    if { [SoilWaterStress::GetIndex $crop] != -1 } {
        set exists 1
    } else {
        set exists 0
    }
    return $exists
}

proc SoilWaterStress::GetCropsAutomaticName { } {
    set basename [_ "crop"]
    set i 1
    set crop $basename-$i
    while { [SoilWaterStress::Exists $crop] } {        
        incr i
        set crop $basename-$i        
    }
    return $crop
}

proc SoilWaterStress::NewCrop { cb } {
    variable crops
    variable data
    variable fields 
    variable fields_defaults
    set crop [SoilWaterStress::GetCropsAutomaticName]     
    
    if { [SoilWaterStress::Exists $crop] } {
        #already exists
        return 1
    }
    foreach field $fields value $fields_defaults {
        set data($crop,$field) $value
    }
    lappend crops $crop   
    if { [winfo exists $cb] } {
        $cb configure -values [SoilWaterStress::GetCrops]
    }
    SoilWaterStress::SetCurrentCrop $crop
    return 0
}

proc SoilWaterStress::DeleteCrop { cb } {
    variable crops
    variable data
    variable fields 
    variable fields_defaults
    set crop [SoilWaterStress::GetCurrentCrop] 
    if { ![SoilWaterStress::Exists $crop] } {
        #not exists
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete crop '%s'" $crop] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {     
        set i [SoilWaterStress::GetIndex $crop] 
        array unset data $crop,*        
        set crops [lreplace $crops $i $i]       
        if { [winfo exists $cb] } {
            $cb configure -values [SoilWaterStress::GetCrops]
        }
        SoilWaterStress::SetCurrentCrop [lindex $crops 0]  
        GiD_Redraw     
    }
    return 0
}

proc SoilWaterStress::RenameCrop { cb } {
    variable data
    variable crops
    variable fields
    set crop [SoilWaterStress::GetCurrentCrop] 
    set num [SoilWaterStress::WriteCropNumber $crop]        
    if { ![SoilWaterStress::Exists $crop] } {
        #not exists
        return 1
    } 
    set new_text [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $crop] \
            [= "Enter new name of %s '%s'" [= "crop"] $crop] gidquestionhead "any" ""]
    set new_name "ID$num - $new_text"
    if { $new_name != "--CANCEL--" } {
        foreach field $fields {
            set data($new_name,$field) $data($crop,$field)
        }
        array unset data $crop,*
        set i [SoilWaterStress::GetIndex $crop] 
        lset crops $i $new_name
        if { [winfo exists $cb] } {
            $cb configure -values [SoilWaterStress::GetCrops]           
        }
        SoilWaterStress::SetCurrentCrop $new_name       
        GiD_Redraw 
    }
    return 0
}

#apply the values of the current window
proc SoilWaterStress::Apply { T } {
    variable data
    variable current_value
    variable fields
    
    set crop [SoilWaterStress::GetCurrentCrop]    
    foreach field $fields {
        set data($crop,$field) $current_value($field)
    }
    
    set data($crop,table_value) [GridData::GetDataAllItems $T]
    GiD_Redraw        
}

#not SoilWaterStress::StartDraw and SoilWaterStress::EndDraw are not used, the Draw button is hidden (because unregister fail in GiD 11.1.5d)
#and the redraw procedure is registered when loading the problemtype, and each crop is draw depending on its 'visible' variable value

proc SoilWaterStress::StartDraw { bdraw} {
    variable drawopengl
    set drawopengl [GiD_OpenGL register SoilWaterStress::ReDraw]
    GiD_Redraw
    $bdraw configure -text [= "Finish draw"] -command [list SoilWaterStress::EndDraw $bdraw]
}

proc SoilWaterStress::EndDraw { bdraw } {
    variable drawopengl
    if { [info exists drawopengl] } { 
        #unregister the callback tcl procedure to be called by GiD when redrawing
        GiD_OpenGL unregister $drawopengl
        catch {unset drawopengl}
        GiD_Redraw
        if { $bdraw != "" } {
            $bdraw configure -text [= "Draw"] -command [list SoilWaterStress::StartDraw $bdraw]
        }
    }
}

proc SoilWaterStress::ReDraw { } {
    foreach crop [SoilWaterStress::GetCrops] {
        SoilWaterStress::Draw $crop
    }
}


proc SoilWaterStress::DestroyCropWindow { W w } {   
    if { $W == $w } {
        SoilWaterStress::EndDraw ""
        SoilWaterStress::FillProblemDataFromTclData             
    }
}

proc SoilWaterStress::OnChangeType { f } {
    variable current_value
    
}

proc SoilWaterStress::PickPointInSurfaceOrElementCmd { entry } {
    set view_mode [GiD_Info Project ViewMode]
    if { $view_mode == "GEOMETRYUSE" } {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an surface"] PointInSurface GEOMETRYUSE]
        #set surface_id $::GidPriv(selection)  
    } elseif { $view_mode == "MESHUSE"} {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an element"] PointInElement MESHUSE]
        #set element_id $::GidPriv(selection)    
    } else {
        set xyz ""
    }        
    if { $xyz != "" } {                    
        set res [format "%g %g %g" {*}$xyz]
        $entry delete 0 end
        $entry insert end $res        
    }
}



proc SoilWaterStress::Window { } {
    variable data
    variable crops
    variable current_crop
    variable current_value
    variable table
    
    if { ![info exists crops] } {
        SoilWaterStress::SetDefaultValues
    }
    
    SoilWaterStress::FillTclDataFromProblemData    
    
    set w .gid.crop
    InitWindow $w [= "Crop definition"] PreCropWindowGeom SoilWaterStress::Window
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    set f [ttk::frame $w.fcrops]
    
    ttk::combobox $f.cb1 -textvariable SoilWaterStress::current_crop -values [SoilWaterStress::GetCrops] -state readonly
    bind $f.cb1 <<ComboboxSelected>> [list SoilWaterStress::OnChangeSelectedCrop %W]
    ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list SoilWaterStress::NewCrop $f.cb1]
    GidHelp $f.bnew  [= "Create a new erodible zone"]
    ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list SoilWaterStress::DeleteCrop $f.cb1]
    GidHelp $f.bdel  [= "Delete an erodible zone"]
    ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list SoilWaterStress::RenameCrop $f.cb1]
    GidHelp $w.f.bren  [= "Rename an erodible zone"]   
    grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
    grid $f -sticky new
    grid rowconfigure $f 0 -weight 1
    
    set f [ttk::frame $w.fcrop]
    
    foreach field {field_cap wilting_p depletion_f} text [list [= "Field capacity"] [= "Wilting point"] [= "Depletion factor"]] {
        ttk::label $f.l$field -text $text
        ttk::entry $f.e$field -textvariable SoilWaterStress::current_value($field)
        grid $f.l$field $f.e$field -sticky ew
        GidHelp $f.efield_cap [= "Field capacity"]
        GidHelp $f.ewilting_p [= "Wilting point of the crop"]
        GidHelp $f.edepletion_f [= "Depletion factor (0-1)"]
    }
    
    
    grid $f -sticky new
    grid columnconfigure $f {1} -weight 1
    
    package require fulltktree
    set columns ""
    foreach name [list [= "Time (s)"] [= "Kc"]] {
        lappend columns [list 4 $name left text 1]
    }
    set T [fulltktree $f.t -columns $columns -showlines 0 -showheader 1 -expand 1]    
    set table $T
    $T configure -editbeginhandler GridData::EditBegin
    $T configure -editaccepthandler [list GridData::EditAccept]
    $T configure -deletehandler GridData::RemoveRows
    $T configure -contextualhandler_menu GridData::ContextualMenu    
    $T column configure all -button 0
    
    ttk::frame $f.fbuttons
    ttk::button $f.fbuttons.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $T 1]
    GidHelp $f.fbuttons.b1 [_ "Pressing this button, one new line will be added to this window"]    
    ttk::button $f.fbuttons.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $T 1]
    GidHelp $f.fbuttons.b2 [_ "Pressing this button, one line is deleted from this window"]        
    
    grid $T -sticky nsew -columnspan 2
    grid $f.fbuttons.b1 $f.fbuttons.b2 -sticky w    
    grid $f.fbuttons -sticky ew
    
    
    
    grid columnconfigure $f {0} -weight 1
    grid rowconfigure $f {1 4} -weight 1
    grid $f -sticky nsew -padx 2 -pady 2
    GidHelp $f [= "Set Crop parameters"]
    
    bind $T <<Paste>> [list ::GridData::Paste $T CLIPBOARD]
    bind $T <<PasteSelection>> [list ::GridData::Paste $T PRIMARY]
    bind $T <<Cut>> [list GridData::Cut $T]
    bind $T <<Copy>> [list GridData::Copy $T]
    bind [$T givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header
    
    GridData::SetData $T $current_value(table_value)
    
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.bapply -text [= "Apply"] -command [list SoilWaterStress::Apply $T] -style BottomFrame.TButton
    #ttk::button $f.bdraw -text [= "Draw"] -command [list SoilWaterStress::StartDraw $f.bdraw] -style BottomFrame.TButton
    ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    #grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
    grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }        
    
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    bind $w <Alt-c> "$f.bclose invoke"
    bind $w <Escape> "$f.bclose invoke"   
    bind $w <Destroy> [list +SoilWaterStress::DestroyCropWindow %W $w] ;# + to add to previous script  
}

proc SoilWaterStress::CloseWindow { } {
    set w .gid.crop
    if { [winfo exists $w] } {
        close $w
    }
}
