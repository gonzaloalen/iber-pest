namespace eval Groundwater {
    variable data ;#array of values
    variable catchments ;#list of names of data
    variable current_catchment ;#name of current selected catchment
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {Start Aparam Bparam MaxDepth IniSat reservoir}
    variable fields_defaults { {} 0.0 0.0 0.0 0.0 0.0 }
    variable drawopengl
    variable table ;#tktable widget
}

proc Groundwater::UnsetVariables { } {
    variable data
    variable catchments
    unset -nocomplain data
    unset -nocomplain catchments
}

proc Groundwater::SetDefaultValues { } {
    variable data
    variable catchments
    variable current_catchment
    variable current_value
    variable fields
    variable fields_defaults
    array unset data
    set catchments {}
    foreach field $fields  {
        set current_value($field) ""
    }
    set current_catchment {}
}

#store the catchments information in a hidden field of the problem data
#then this data is saved with the model without do nothing more
proc Groundwater::FillTclDataFromProblemData { } {
    variable data
    variable catchments
    if { [catch {set x [GiD_AccessValue get gendata CatchmentsData]} msg] } {
        #reading a model created by an old version of the problemtype this gendata doesn't exists
        return
    }
    array set data $x

    set catchments [list]
    foreach item [array names data *,Start] {
        lappend catchments [string range $item 0 end-6]
    }
    set catchments [lsort -dictionary $catchments]
    Groundwater::SetCurrentCatchment [lindex $catchments 0]
}

proc Groundwater::FillProblemDataFromTclData { } {
    variable data
    variable catchments
    set x [array get data]
    GiD_AccessValue set gendata CatchmentsData $x
}

proc Groundwater::ExpandGIDSelection { sellist } {
    set result [list]
    foreach sel $sellist {
        if {[llength [set range [split $sel :]]]==1} {
            lappend result $range
        } else {
            foreach {from to} $range break
            while {$from<$to} {
                lappend result $from
                incr from
            }
        }
    }
    # you can also use: return $result
    set result
}

proc Groundwater::WriteCalculationFile { } {
    variable data
    variable reservoir
    set res ""
    append res "Begin lumped param" \n
    append res [llength [Groundwater::GetCatchment]] \n
    set i 0
    foreach catchment [Groundwater::GetCatchment] {
        incr i
        append res "Reservoir $i: parameters" \n
        append res $i \n
        append res $data($catchment,Aparam)
        append res " "
        append res $data($catchment,Bparam)
        append res " "
        append res $data($catchment,MaxDepth)
        append res " "
        append res $data($catchment,IniSat) \n
    }
    append res "Reservoir exfiltration elements"
    return $res
}

proc Groundwater::WriteCatchmentNumber { name } {
    variable number

    set number [expr [lsearch [Groundwater::GetCatchment] $name]+1]

    return $number
}


proc Groundwater::GetCatchment { } {
    variable catchments
    if { ![info exists catchments] } {
        return ""
    }
    return $catchments
}

proc Groundwater::GetCurrentCatchment { } {
    variable current_catchment
    return $current_catchment
}

proc Groundwater::OnChangeSelectedCatchment { cb } {
    Groundwater::SetCurrentCatchment [Groundwater::GetCurrentCatchment]
}

proc Groundwater::SetCurrentCatchment { catchment } {
    variable data
    variable current_catchment
    variable current_value
    variable fields
    variable table
    #fill in the current_value variables with data
    if { $catchment != "" } {
        foreach field $fields {
            if { ![info exists data($catchment,$field)] } {
                set data($catchment,$field) 0
            }
            set current_value($field) $data($catchment,$field)
        }
    } else {
        foreach field $fields {
            set current_value($field) ""
        }
    }

    #the other widgets have a linked variable that automatically update it but the table not
    if { [info exists table] && [winfo exists $table] } {
        GridData::SetData $table $current_value(table_value)
    }
    set current_catchment $catchment
}

proc Groundwater::GetIndex { catchment } {
    variable catchments
    return [lsearch $catchments $catchment]
}

proc Groundwater::Exists { catchment } {
    if { [Groundwater::GetIndex $catchment] != -1 } {
        set exists 1
    } else {
        set exists 0
    }
    return $exists
}

proc Groundwater::GetCatchmentAutomaticName { } {
    set basename [_ "Reservoir"]
    set i 1
    set catchment $basename-$i
    while { [Groundwater::Exists $catchment] } {
        incr i
        set catchment $basename-$i
    }
    return $catchment
}

proc Groundwater::NewCatchment { cb } {
    variable catchments
    variable data
    variable fields
    variable fields_defaults
    set catchment [Groundwater::GetCatchmentAutomaticName]

    if { [Groundwater::Exists $catchment] } {
        #already exists
        return 1
    }
    foreach field $fields value $fields_defaults {
        set data($catchment,$field) $value
    }
    lappend catchments $catchment
    if { [winfo exists $cb] } {
        $cb configure -values [Groundwater::GetCatchment]
    }
    Groundwater::SetCurrentCatchment $catchment
    return 0
}

proc Groundwater::DeleteCatchment { cb } {
    variable catchments
    variable data
    variable fields
    variable fields_defaults
    set catchment [Groundwater::GetCurrentCatchment]
    if { ![Groundwater::Exists $catchment] } {
        #not exists
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete catchment '%s'" $catchment] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {
        set i [Groundwater::GetIndex $catchment]
        array unset data $catchment,*
        set catchments [lreplace $catchments $i $i]
        if { [winfo exists $cb] } {
            $cb configure -values [Groundwater::GetCatchment]
        }
        Groundwater::SetCurrentCatchment [lindex $catchments 0]
        GiD_Redraw
    }
    return 0
}

proc Groundwater::RenameCatchment { cb } {
    variable data
    variable catchments
    variable fields
    set catchment [Groundwater::GetCurrentCatchment]
    if { ![Groundwater::Exists $catchment] } {
        #not exists
        return 1
    }
    set new_name [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $catchment] \
            [= "Enter new name of %s '%s'" [= "catchment"] $catchment] gidquestionhead "any" ""]
    if { $new_name != "--CANCEL--" } {
        foreach field $fields {
            set data($new_name,$field) $data($catchment,$field)
        }
        array unset data $catchment,*
        set i [Groundwater::GetIndex $catchment]
        lset catchments $i $new_name
        if { [winfo exists $cb] } {
            $cb configure -values [Groundwater::GetCatchment]
        }
        Groundwater::SetCurrentCatchment $new_name
        GiD_Redraw
    }
    return 0
}

#apply the values of the current window
proc Groundwater::Apply { } {
    variable data
    variable current_value
    variable fields

    set catchment [Groundwater::GetCurrentCatchment]
    foreach field $fields {
        set data($catchment,$field) $current_value($field)
    }

    GiD_Redraw
}

#not Groundwater::StartDraw and Groundwater::EndDraw are not used, the Draw button is hidden (because unregister fail in GiD 11.1.5d)
#and the redraw procedure is registered when loading the problemtype, and each catchment is draw depending on its 'visible' variable value
proc Groundwater::StartDraw { bdraw} {
    variable drawopengl
    set drawopengl [GiD_OpenGL register Groundwater::ReDraw]
    GiD_Redraw
    $bdraw configure -text [= "Finish draw"] -command [list Groundwater::EndDraw $bdraw]
}

proc Groundwater::EndDraw { bdraw } {
    variable drawopengl
    if { [info exists drawopengl] } {
        #unregister the callback tcl procedure to be called by GiD when redrawing
        GiD_OpenGL unregister $drawopengl
        catch {unset drawopengl}
        GiD_Redraw
        if { $bdraw != "" } {
            $bdraw configure -text [= "Draw"] -command [list Groundwater::StartDraw $bdraw]
        }
    }
}

proc Groundwater::ReDraw { } {
    foreach catchment [Groundwater::GetCatchment] {
        Groundwater::Draw $catchment
    }
}

proc Groundwater::Draw { catchment } {

}



proc Groundwater::DestroyCatchment { W w } {
    if { $W == $w } {
        Groundwater::EndDraw ""
        Groundwater::FillProblemDataFromTclData
    }
}

proc Groundwater::OnChangeType { f } {
    variable current_value

}


proc Groundwater::Window { } {
    variable data
    variable catchments
    variable current_catchment
    variable current_value
    variable table

    if { ![info exists catchments] } {
        Groundwater::SetDefaultValues
    }

    Groundwater::FillTclDataFromProblemData

    set w .gid.catchment
    InitWindow $w [= "Reservoir definition"] PreCatchmentWindowGeom Groundwater::Window
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    set f [ttk::frame $w.fcatchments]

    ttk::combobox $f.cb1 -textvariable Groundwater::current_catchment -values [Groundwater::GetCatchment] -state readonly
    bind $f.cb1 <<ComboboxSelected>> [list Groundwater::OnChangeSelectedCatchment %W]
    ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list Groundwater::NewCatchment $f.cb1]
    GidHelp $f.bnew  [= "Create a new catchment"]
    ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list Groundwater::DeleteCatchment $f.cb1]
    GidHelp $f.bdel  [= "Delete a catchment"]
    ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list Groundwater::RenameCatchment $f.cb1]
    GidHelp $w.f.bren  [= "Rename a catchment"]
    grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
    grid $f -sticky new
    grid rowconfigure $f 0 -weight 1

    set f [ttk::frame $w.fcatchment]

    foreach field {Aparam Bparam MaxDepth IniSat} text [list [= "A (1/h)"] [= "B (mm)"] [= "Max depth (mm)"] [= "Initial saturation (0-1)"]] {
        ttk::label $f.l$field -text $text
        ttk::entry $f.e$field -textvariable Groundwater::current_value($field)
        grid $f.l$field $f.e$field -sticky ew
        GidHelp $f.eAparam  [= "A reservoir parameter"]
        GidHelp $f.eBparam  [= "B reservoir parameter"]
        GidHelp $f.eMaxDepth [= "Total thickness of the soil layer"]
        GidHelp $f.eIniSat [= "Initial soil moisture"]
    }


    grid $f -sticky new
    grid columnconfigure $f {1} -weight 1

    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.bapply -text [= "Apply"] -command [list Groundwater::Apply] -style BottomFrame.TButton
    #ttk::button $f.bdraw -text [= "Draw"] -command [list Groundwater::StartDraw $f.bdraw] -style BottomFrame.TButton
    ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    #grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
    grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }

    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    bind $w <Alt-c> "$f.bclose invoke"
    bind $w <Escape> "$f.bclose invoke"
    bind $w <Destroy> [list +Groundwater::DestroyCatchment %W $w] ;# + to add to previous script
}

proc Groundwater::CloseWindow { } {
    set w .gid.catchment
    if { [winfo exists $w] } {
        close $w
    }
}
