
namespace eval Mixtures {
    variable datamezclas ;#array of values
    variable mixtures ;#list of names of data
    variable current_mixture ;#name of current selected mixture
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {initialerosion table_value}
    variable fields_defaults {0.0 {0 0 0 0 0 0 0 0 0 0 0 0 0}}  
    variable drawopengl
    variable table ;#tktable widget
    variable current_mixturelc
    variable nstrat
    variable nclass
    variable name
}

proc Mixtures::UnsetVariables { } {
    variable datamezclas
    variable mixtures      
    unset -nocomplain datamezclas
    unset -nocomplain mixtures
}

proc Mixtures::SetDefaultValues { } {    
    variable datamezclas
    variable mixtures
    variable current_mixture
    variable current_value
    variable fields
    variable fields_defaults
    variable mixtureslc
    variable nstrat
    variable nclass
    
    #Recuperamos las variables
    #MixturesLC::FillTclDataFromProblemData
    
    #Recuperamos el n�mero de Stratums (width == Stratums) y de Classes (height == Classes)
    #set nstrat MixturesLC::GetNstrats
    #set numcolumns MixturesLC::GetNumcols
    
    #array unset datamezclas
    set fields_defaults {}
    foreach field $fields  {
        set current_value($field) ""
    }
    set structabledim [expr $nstrat*($nclass+2)]
    set item [list]   
    for {set i 1} {$i <= $structabledim} {incr i}  {
        lappend item 0.0
    }
    set current_value(initialerosion) 0.0
    set current_value(table_value) $item
    lappend fields_defaults $current_value(initialerosion)
    lappend fields_defaults $current_value(table_value)
    set aaaa 1
    #set current_mixture {}
}

#store the mixtures information in a hidden field of the problem datamezclas
#then this datamezclas is saved with the model without do nothing more
proc Mixtures::FillTclDataFromProblemData { } {
    variable datamezclas
    variable mixtures
    if { [catch {set x [GiD_AccessValue get gendata MixturesData]} msg] } {
        #reading a model created by an old version of the problemtype this gendata doesn't exists
        return
    }    
    array set datamezclas $x
    
    set mixtures [list]
    foreach item [array names datamezclas *,table_value] {
        lappend mixtures [string range $item 0 end-12]
    }
    set mixtures [lsort -dictionary $mixtures]
    Mixtures::SetCurrentMixture [lindex $mixtures 0]
}

proc Mixtures::FillProblemDataFromTclData { } {
    variable datamezclas
    variable mixtures
    set x [array get datamezclas]
    GiD_AccessValue set gendata MixturesData $x
}

proc Mixtures::WriteCalculationFile { } {
    variable datamezclas
    set res ""       
    
    set i 0
    append res "Mixtures"  \n
    append res [llength [Mixtures::GetMixtures]] \n
    foreach mixture [Mixtures::GetMixtures] {
        incr i        
        set item [list]   
        lappend item $i                  
        lappend item $data($mixture,initialerosion)                 
        append res [join $item " "]
        set lines [expr [llength $datamezclas($mixture,table_value)]/2]
        append res $lines  \n
        set l [expr $lines*2]
        for {set ii 0} {$ii < $l} {set ii [expr $ii+2]} {
            append res "[lrange $datamezclas($mixture,table_value) $ii [expr $ii+1]] " \n        
        }       
    }
    
    return $res
}

proc Mixtures::WriteMixtureNumber { name } {
    variable number
    
    set number [expr [lsearch [Mixtures::GetMixtures] $name]+1]
    
    return $number    
}


proc Mixtures::GetMixtures { } {
    variable mixtures
    if { ![info exists mixtures] } {
        return ""
    }
    return $mixtures
}

proc Mixtures::GetCurrentMixture { } {
    variable current_mixture
    return $current_mixture
}

proc Mixtures::OnChangeSelectedMixture { cb } {   
    Mixtures::SetCurrentMixture [Mixtures::GetCurrentMixture]
}

proc Mixtures::SetCurrentMixture { mixture } {
    variable datamezclas
    variable current_mixture
    variable current_value
    variable fields   
    variable table
    
    #fill in the current_value variables with datamezclas
    if { $mixture != "" } {
        foreach field $fields {
            if { ![info exists datamezclas($mixture,$field)] } {
                set datamezclas($mixture,$field) 0
            }
            set current_value($field) $datamezclas($mixture,$field)           
        }                
    } else {
        foreach field $fields {
            set current_value($field) ""
        }
    }
    
    #the other widgets have a linked variable that automatically update it but the table not
    if { [info exists table] && [winfo exists $table] } {
        GridData::SetData $table $current_value(table_value)
    }
    set current_mixture $mixture  
}

proc Mixtures::GetIndex { mixture } {
    variable mixtures    
    return [lsearch $mixtures $mixture]
}

proc Mixtures::Exists { mixture } {    
    if { [Mixtures::GetIndex $mixture] != -1 } {
        set exists 1
    } else {
        set exists 0
    }
    return $exists
}

proc Mixtures::GetMixtureAutomaticName { } {
    set basename [_ "Soil Structure"]
    #set mixture $basename
    
    #This lines are commented waiting for future versions. The last one substitutes 'set mixture $basename-$i' (previous)    
    set i 1
    set mixture $basename-$i
    
    #if { [Mixtures::Exists $mixture] } {
        #tk_dialogRAM .gid.tmpwin [= "Warning"] [= "The mixture table has been already defined"] warning 0 OK
        ##already exists
        ##return $mixture
        #}
    
    #This lines are commented waiting for future versions. The last one substitutes 'set mixture $basename-$i' (previous). See culverts.tlc
    while { [Mixtures::Exists $mixture] } {        
        incr i
        set mixture $basename-$i        
    }
    return $mixture
}

proc Mixtures::NewMixture { cb } {
    variable mixtures
    variable datamezclas
    variable fields 
    variable current_value
    variable table
    variable fields_defaults
    
    set mixture [Mixtures::GetMixtureAutomaticName]  
    
    if { [Mixtures::Exists $mixture] } {
        #already exists
        return 1
    }
    Mixtures::SetDefaultValues
    foreach field $fields value $fields_defaults {
        set datamezclas($mixture,$field) $value
    }
    lappend mixtures $mixture   
    if { [winfo exists $cb] } {
        $cb configure -values [Mixtures::GetMixtures]
    }
    Mixtures::SetCurrentMixture $mixture
    GridData::SetData $table $current_value(table_value)
    return 0
}

proc Mixtures::DeleteMixture { cb } {
    variable mixtures
    variable datamezclas
    variable fields 
    variable fields_defaults
    set mixture [Mixtures::GetCurrentMixture] 
    if { ![Mixtures::Exists $mixture] } {
        #not exists
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete mixture '%s'" $mixture] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {     
        set i [Mixtures::GetIndex $mixture] 
        array unset datamezclas $mixture,*        
        set mixtures [lreplace $mixtures $i $i]       
        if { [winfo exists $cb] } {
            $cb configure -values [Mixtures::GetMixtures]
        }
        Mixtures::SetCurrentMixture [lindex $mixtures 0]  
        GiD_Redraw     
    }
    return 0
}

proc Mixtures::RenameMixture { cb } {
    variable datamezclas
    variable mixtures
    variable fields
    set mixture [Mixtures::GetCurrentMixture]   
    if { ![Mixtures::Exists $mixture] } {
        #not exists
        return 1
    } 
    set new_name [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $mixture] \
            [= "Enter new name of %s '%s'" [= "Soil Estructure"] $mixture] gidquestionhead "any" ""]
    if { $new_name != "--CANCEL--" } {
        foreach field $fields {
            set datamezclas($new_name,$field) $datamezclas($mixture,$field)
        }
        array unset datamezclas $mixture,*
        set i [Mixtures::GetIndex $mixture] 
        lset mixtures $i $new_name
        if { [winfo exists $cb] } {
            $cb configure -values [Mixtures::GetMixtures]           
        }
        Mixtures::SetCurrentMixture $new_name       
        GiD_Redraw 
    }
    return 0
}

#apply the values of the current window
proc Mixtures::Apply { T } {
    variable datamezclas
    variable current_value
    variable fields
    set mixture [Mixtures::GetCurrentMixture]     
    
    if { ![Mixtures::Exists $mixture] } {
        #already exists
        return 1
    }
    
    set mixture [Mixtures::GetCurrentMixture]    
    foreach field $fields {
        set datamezclas($mixture,$field) $current_value($field)
    }
    
    set datamezclas($mixture,table_value) [GridData::GetDataAllItems $T]
    GiD_Redraw        
}

#not Mixtures::StartDraw and Mixtures::EndDraw are not used, the Draw button is hidden (because unregister fail in GiD 11.1.5d)
#and the redraw procedure is registered when loading the problemtype, and each mixture is draw depending on its 'visible' variable value
proc Mixtures::StartDraw { bdraw} {
    variable drawopengl
    set drawopengl [GiD_OpenGL register Mixtures::ReDraw]
    GiD_Redraw
    $bdraw configure -text [= "Finish draw"] -command [list Mixtures::EndDraw $bdraw]
}

proc Mixtures::EndDraw { bdraw } {
    variable drawopengl
    if { [info exists drawopengl] } { 
        #unregister the callback tcl procedure to be called by GiD when redrawing
        GiD_OpenGL unregister $drawopengl
        catch {unset drawopengl}
        GiD_Redraw
        if { $bdraw != "" } {
            $bdraw configure -text [= "Draw"] -command [list Mixtures::StartDraw $bdraw]
        }
    }
}

proc Mixtures::DestroyMixtureWindow { W w } {   
    if { $W == $w } {
        Mixtures::EndDraw ""
        Mixtures::FillProblemDataFromTclData             
    }
}

proc Mixtures::AutomaticColumnsNames { } {
    variable nstrat
    variable nclass
    variable name
    
    set name "Stratum"
    lappend name "Thickness (m)"
    set i 1
    for {set i 1} {$i <= $nclass} {incr i}  {
        set columnname [_ "%-Class"]
        lappend name $columnname-$i
    }
    return $name
}

proc Mixtures::Window { } {
    variable datamezclas
    variable mixtures
    variable current_mixture
    variable current_value
    variable table
    variable nstrat
    variable nclass
    variable name
    
    #Verificamos que se hayan definido el num. Stratums&Classes
    set mixturelc [MixturesLC::GetCurrentMixturelc]
    
    if { $mixturelc == "" } {
        tk_dialogRAM .gid.tmpwin [= "Warning"] [= "Please, define Number of Stratums&Classes"] warning 0 OK
        Mixtures::CloseWindow
    } else { 

        #Recuperamos el n�mero de Stratums (width == Stratums) y de Classes (height == Classes)
        set nstrat [MixturesLC::GetNstrats]
        set nclass [MixturesLC::GetNclasses]
        set nclasstot [expr 2 + $nclass]
        
        #Definimos valores por defecto la primera vez que entramos aqu�
        if { ![info exists mixtures] } {
            Mixtures::SetDefaultValues
        }
        
        #Generamos una tabla de "nstrat" filas y "nclass+2" columnas (Stratums,Thickness,Class-1,Class-2...Class-nclass)
        Mixtures::AutomaticColumnsNames
        
        Mixtures::FillTclDataFromProblemData    
        
        set w .gid.mixture
        InitWindow $w [= "Soil structure definition"] PreMixtureWindowGeom Mixtures::Window
        if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
        
        set f [ttk::frame $w.fmixtures]
        
        ttk::combobox $f.cb1 -textvariable Mixtures::current_mixture -values [Mixtures::GetMixtures] -state readonly
        bind $f.cb1 <<ComboboxSelected>> [list Mixtures::OnChangeSelectedMixture %W]
        ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list Mixtures::NewMixture $f.cb1]
        GidHelp $f.bnew  [= "Create a new mixture"]
        ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list Mixtures::DeleteMixture $f.cb1]
        GidHelp $f.bdel  [= "Delete a mixture"]
        ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list Mixtures::RenameMixture $f.cb1]
        GidHelp $f.f.bren  [= "Rename a mixture"]   
        grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
        grid $f -sticky new
        grid rowconfigure $f 0 -weight 1
        
        #ttk::label $f.tinitialerosion -text "Tolerance (m) "
        #ttk::entry $f.vinitialerosion -textvariable Mixtures::current_value(initialerosion)
        #grid $f.tinitialerosion $f.evinitialerosion -sticky ew

        
        set f [ttk::frame $w.fstructure] 
 
        set f [ttk::frame $w.fstratdist]
        foreach field {initialerosion} text [list [= "Upper stratum top to bed distance"]] {
            ttk::label $f.l$field -text $text
            ttk::entry $f.e$field -textvariable Mixtures::current_value($field)
            grid $f.l$field $f.e$field -sticky ew
        }       
        
        grid $f -sticky new
        grid columnconfigure $f {1} -weight 1
        
        package require fulltktree
        set columns ""
        for {set i 1} {$i <= $nclasstot} {incr i} {
            set colid $i
            set colname ""
            set colname [lindex $name [expr $i-1]]
            lappend columns [list 4 $colname left text 1]
        }
        
        set T [fulltktree $f.t -columns $columns -showlines 0 -showheader 1 -expand 1]    
        set table $T
        $T configure -editbeginhandler GridData::EditBegin
        $T configure -editaccepthandler [list GridData::EditAccept]
        $T configure -deletehandler GridData::RemoveRows
        $T configure -contextualhandler_menu GridData::ContextualMenu    
        $T column configure all -button 0
        
        #ttk::frame $f.fbuttons
        #ttk::button $f.fbuttons.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $T 1]
        #GidHelp $f.fbuttons.b1 [_ "Pressing this button, one new line will be added to this window"]    
        #ttk::button $f.fbuttons.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $T 1]
        #GidHelp $f.fbuttons.b2 [_ "Pressing this button, one line is deleted from this window"]        
        
        #ttk::button $f.fbuttons.b3 -image [gid_themes::GetImage graphs.png small_icons] -text [_ "Graph"] \
            #-command [list GridData::PlotCurveWin $T [= "Mixture"] ""]
        #GidHelp $f.fbuttons.b3 [_ "Pressing this button, a XY graph will be drawn"]
        
        grid $T -sticky nsew -columnspan 2
        #grid $f.fbuttons.b1 $f.fbuttons.b2 -sticky w    
        #grid $f.fbuttons -sticky ew
        
        
        
        grid columnconfigure $f {0} -weight 1
        grid rowconfigure $f {1 4} -weight 1
        grid $f -sticky nsew -padx 2 -pady 2
        GidHelp $f [= "Set mixture parameters"]
        
        bind $T <<Paste>> [list ::GridData::Paste $T CLIPBOARD]
        bind $T <<PasteSelection>> [list ::GridData::Paste $T PRIMARY]
        bind $T <<Cut>> [list GridData::Cut $T]
        bind $T <<Copy>> [list GridData::Copy $T]
        bind [$T givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header
        
        
        for {set i 1} {$i <= $nstrat} {incr i} {
                GridData::AppendRows $T 1
        }
        
        set mixturenum [llength $mixtures]
                
        if { $mixtures == "" } {
            #Mixtures::SetDefaultValues
            #Creamos tantas filas como Stratums
        } else {
             GridData::SetData $T $current_value(table_value)
        }        
        
        
        
        set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
        ttk::button $f.bapply -text [= "Apply"] -command [list Mixtures::Apply $T] -style BottomFrame.TButton
        #ttk::button $f.bdraw -text [= "Draw"] -command [list Mixtures::StartDraw $f.bdraw] -style BottomFrame.TButton
        ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
        #grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
        grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
        grid $f -sticky sew
        if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }        
        
        grid columnconfigure $w 0 -weight 1
        grid rowconfigure $w 1 -weight 1
        bind $w <Alt-c> "$f.bclose invoke"
        bind $w <Escape> "$f.bclose invoke"   
        bind $w <Destroy> [list +Mixtures::DestroyMixtureWindow %W $w] ;# + to add to previous script  
    }
}

proc Mixtures::CloseWindow { } {
    set w .gid.mixture
    if { [winfo exists $w] } {
        close $w
    }
}
