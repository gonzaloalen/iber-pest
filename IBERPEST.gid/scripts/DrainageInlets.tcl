
namespace eval DrainageInlet {
    variable data ;#array of values
    variable drainageinlets ;#list of names of data
    variable current_drainageinlet ;#name of current selected drainageinlet
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {visible start conntype end type A B Length Width Effective_area Weir_Cd Orifice_Cd Inlet_Discharge table_value}
    variable fields_defaults {1 {} 1 {} 1 0.47 0.77 1.0 0.5 100 1.7 0.6 0 {0.0 0.0}}   
    variable drawopengl
    variable table ;#tktable widget
}

proc DrainageInlet::UnsetVariables { } {
    variable data
    variable drainageinlets      
    unset -nocomplain data
    unset -nocomplain drainageinlets
}

proc DrainageInlet::SetDefaultValues { } {    
    variable data
    variable drainageinlets
    variable current_drainageinlet
    variable current_value
    variable fields
    variable fields_defaults
    array unset data
    set drainageinlets {}
    foreach field $fields  {
        set current_value($field) ""
    }
    set current_value(table_value) {"0.0 0.0"}
    set current_drainageinlet {}
}

#store the drainageinlets information in a hiddend field of the problem data
#then this data is saved with the model without do nothing more
proc DrainageInlet::FillTclDataFromProblemData { } {
    variable data
    variable drainageinlets
    
    if { [catch {set x [GiD_AccessValue get gendata DrainageInletsData]} msg] } {
        #reading a model created by an old version of the problemtype this gendata doesn't exists
        return
    }
    array set data $x
    
    set drainageinlets [list]
    foreach item [array names data *,start] {
        lappend drainageinlets [string range $item 0 end-6]
    }
    set drainageinlets [lsort -dictionary $drainageinlets]
    DrainageInlet::SetCurrentDrainageInlet [lindex $drainageinlets 0]
}

proc DrainageInlet::FillProblemDataFromTclData { } {
    variable data
    variable drainageinlets
    set x [array get data]
    GiD_AccessValue set gendata DrainageInletsData $x
}

proc DrainageInlet::WriteCalculationFile { } {
    variable data
    set res ""       
    
    set i 0
    foreach drainageinlet [DrainageInlet::GetDrainageInlets] { 
        incr i        
        set item [list]
        lappend item $i
        lappend item $data($drainageinlet,visible)
        lappend item {*}[lrange $data($drainageinlet,start) 0 2]
        lappend item $data($drainageinlet,conntype)
        if { $data($drainageinlet,conntype) == 1 } {
            lappend item {*}[lrange $data($drainageinlet,end) 0 2]
        } elseif { $data($drainageinlet,conntype) == 2 } {
            set sink "-9999 -9999 -9999"
            lappend item $sink
        }
        #lappend item {*}[lrange $data($drainageinlet,end) 0 2]
        if { $data($drainageinlet,type) == 1 } {
            lappend item 1
        } elseif { $data($drainageinlet,type) == 2 } {
            lappend item 2
        } else {
            lappend item 3
        }
        lappend item $data($drainageinlet,A)
        lappend item $data($drainageinlet,B)
        lappend item $data($drainageinlet,Length)
        lappend item $data($drainageinlet,Width)
        lappend item $data($drainageinlet,Effective_area)
        lappend item $data($drainageinlet,Weir_Cd)
        lappend item $data($drainageinlet,Orifice_Cd)
        lappend item $data($drainageinlet,Inlet_Discharge)
        lappend item $drainageinlet   
        append res [join $item " "]
        
        set lines [expr [llength $data($drainageinlet,table_value)]/2]
        append res " $lines" \n                        
        set l [expr $lines*2]        
        
        if { $data($drainageinlet,Inlet_Discharge) == 1 } {
            for {set ii 0} {$ii < $l} {set ii [expr $ii+2]} {
                append res "[lrange $data($drainageinlet,table_value) $ii [expr $ii+1]] " \n        
            }
        }
        
    }
    return $res
}

proc DrainageInlet::GetDrainageInlets { } {
    variable drainageinlets
    if { ![info exists drainageinlets] } {
        return ""
    }
    return $drainageinlets
}

proc DrainageInlet::GetCurrentDrainageInlet { } {
    variable current_drainageinlet
    return $current_drainageinlet
}

proc DrainageInlet::OnChangeSelectedDrainageInlet { cb } {   
    DrainageInlet::SetCurrentDrainageInlet [DrainageInlet::GetCurrentDrainageInlet]
}

proc DrainageInlet::SetCurrentDrainageInlet { drainageinlet } {
    variable data
    variable current_drainageinlet
    variable current_value
    variable fields   
    variable table
    #fill in the current_value variables with data
    if { $drainageinlet != "" } {
        foreach field $fields {
            if { ![info exists data($drainageinlet,$field)] } {
                set data($drainageinlet,$field) 0
            }
            set current_value($field) $data($drainageinlet,$field)           
        }
    } else {
        foreach field $fields {
            set current_value($field) ""
        }
    }  
    
    #the other widgets have a linked variable that automatically update it but the table not
    if { [info exists table] && [winfo exists $table] } {
        GridData::SetData $table $current_value(table_value)
    }
    set current_drainageinlet $drainageinlet  
}

proc DrainageInlet::GetIndex { drainageinlet } {
    variable drainageinlets    
    return [lsearch $drainageinlets $drainageinlet]
}

proc DrainageInlet::Exists { drainageinlet } {    
    if { [DrainageInlet::GetIndex $drainageinlet] != -1 } {
        set exists 1
    } else {
        set exists 0
    }
    return $exists
}

proc DrainageInlet::GetDrainageInletAutomaticName { } {
    set basename [_ "Inlet"]
    set i 1
    set drainageinlet $basename-$i
    while { [DrainageInlet::Exists $drainageinlet] } {        
        incr i
        set drainageinlet $basename-$i        
    }
    return $drainageinlet
}

proc DrainageInlet::NewDrainageInlet { cb } {
    variable drainageinlets
    variable data
    variable fields 
    variable fields_defaults
    set drainageinlet [DrainageInlet::GetDrainageInletAutomaticName]     
    
    if { [DrainageInlet::Exists $drainageinlet] } {
        #already exists
        return 1
    }
    foreach field $fields value $fields_defaults {
        set data($drainageinlet,$field) $value
    }
    lappend drainageinlets $drainageinlet   
    if { [winfo exists $cb] } {
        $cb configure -values [DrainageInlet::GetDrainageInlets]
    }
    DrainageInlet::SetCurrentDrainageInlet $drainageinlet
    return 0
}


proc DrainageInlet::NewDrainageInletfromfile { } {
    variable drainageinlets
    variable data
    variable fields 
    variable fields_defaults
    set drainageinlet [DrainageInlet::GetDrainageInletAutomaticName]     
    
    if { [DrainageInlet::Exists $drainageinlet] } {
        #already exists
        WarnWinText "Inlet" $drainageinlet "already exists. Not imported."
        return 1
    }
    foreach field $fields value $fields_defaults {
        set data($drainageinlet,$field) $value
    }
    lappend drainageinlets $drainageinlet   
    # if { [winfo exists $cb] } {
        # $cb configure -values [DrainageInlet::GetDrainageInlets]
        # }
    DrainageInlet::SetCurrentDrainageInlet $drainageinlet
    return 0
}



proc DrainageInlet::DeleteDrainageInlet { cb } {
    variable drainageinlets
    variable data
    variable fields 
    variable fields_defaults
    set drainageinlet [DrainageInlet::GetCurrentDrainageInlet] 
    if { ![DrainageInlet::Exists $drainageinlet] } {
        #not exists
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete drainageinlet '%s'" $drainageinlet] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {     
        set i [DrainageInlet::GetIndex $drainageinlet] 
        array unset data $drainageinlet,*        
        set drainageinlets [lreplace $drainageinlets $i $i]       
        if { [winfo exists $cb] } {
            $cb configure -values [DrainageInlet::GetDrainageInlets]
        }
        DrainageInlet::SetCurrentDrainageInlet [lindex $drainageinlets 0]  
        GiD_Redraw     
    }
    return 0
}

proc DrainageInlet::RenameDrainageInlet { cb } {
    variable data
    variable drainageinlets
    variable fields
    set drainageinlet [DrainageInlet::GetCurrentDrainageInlet]   
    if { ![DrainageInlet::Exists $drainageinlet] } {
        #not exists
        return 1
    } 
    set new_name [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $drainageinlet] \
            [= "Enter new name of %s '%s'" [= "Drainage_Inlet"] $drainageinlet] gidquestionhead "any" ""]
    if { $new_name != "--CANCEL--" } {
        foreach field $fields {
            set data($new_name,$field) $data($drainageinlet,$field)
        }
        array unset data $drainageinlet,*
        set i [DrainageInlet::GetIndex $drainageinlet] 
        lset drainageinlets $i $new_name
        if { [winfo exists $cb] } {
            $cb configure -values [DrainageInlet::GetDrainageInlets]           
        }
        DrainageInlet::SetCurrentDrainageInlet $new_name       
        GiD_Redraw 
    }
    return 0
}


proc DrainageInlet::RenameDrainageInletfromfile { new_name } {
    variable data
    variable drainageinlets
    variable fields
    set drainageinlet [DrainageInlet::GetCurrentDrainageInlet]   
    if { ![DrainageInlet::Exists $drainageinlet] } {
        #not exists
        return 1
    } 
    
    foreach field $fields {
        set data($new_name,$field) $data($drainageinlet,$field)
    }
    array unset data $drainageinlet,*
    set i [DrainageInlet::GetIndex $drainageinlet] 
    lset drainageinlets $i $new_name
    #if { [winfo exists $cb] } {
        #    $cb configure -values [DrainageInlet::GetDrainageInlets]           
        #}
    DrainageInlet::SetCurrentDrainageInlet $new_name       
    GiD_Redraw 
    
    return 0
}


proc DrainageInlet::ApplyInletsfromfile { table } {
    variable data
    variable current_value
    variable fields
    
    set drainageinlet [DrainageInlet::GetCurrentDrainageInlet]    
    foreach field $fields {
        set data($drainageinlet,$field) $current_value($field) 
        set kk ""
    }
    set data($drainageinlet,table_value) $table 
    GiD_Redraw        
}


#apply the values of the current window
proc DrainageInlet::Apply { T } {
    variable data
    variable current_value
    variable fields
    
    set drainageinlet [DrainageInlet::GetCurrentDrainageInlet]    
    foreach field $fields {
        set data($drainageinlet,$field) $current_value($field) 
        if {$current_value(conntype) == 2} {
            set current_value(end) $current_value(start)
        }
        
        set kk ""
    }
    set data($drainageinlet,table_value) [GridData::GetDataAllItems $T]  
    set jj ""  
    GiD_Redraw        
}

#not DrainageInlet::StartDraw and DrainageInlet::EndDraw are not used, the Draw button is hidden (because unregister fail in GiD 11.1.5d)
#and the redraw procedure is registered when loading the problemtype, and each drainageinlet is draw depending on its 'visible' variable value
proc DrainageInlet::StartDraw { bdraw} {
    variable drawopengl
    set drawopengl [GiD_OpenGL register DrainageInlet::ReDraw]
    GiD_Redraw
    $bdraw configure -text [= "Finish draw"] -command [list DrainageInlet::EndDraw $bdraw]
}

proc DrainageInlet::EndDraw { bdraw } {
    variable drawopengl
    if { [info exists drawopengl] } { 
        #unregister the callback tcl procedure to be called by GiD when redrawing
        GiD_OpenGL unregister $drawopengl
        catch {unset drawopengl}
        GiD_Redraw
        if { $bdraw != "" } {
            $bdraw configure -text [= "Draw"] -command [list DrainageInlet::StartDraw $bdraw]
        }
    }
}

proc DrainageInlet::ReDraw { } {
    foreach drainageinlet [DrainageInlet::GetDrainageInlets] {
        DrainageInlet::Draw $drainageinlet
    }
}

proc DrainageInlet::Draw { drainageinlet } {
    variable data
    if { $drainageinlet == "" || ![info exists data($drainageinlet,visible)] || !$data($drainageinlet,visible) } {
        return 1
    }
    foreach field {start end} {
        if { [llength $data($drainageinlet,$field)] != 3 } {
            return 1
        }
        foreach v $data($drainageinlet,$field) {
            if { ![string is double $v] } {
                return 1
            }
        }
    }    
    set blue {0 0 1}
    GiD_OpenGL draw -color $blue
    GiD_OpenGL draw -pointsize 3
    GiD_OpenGL draw -begin points
    GiD_OpenGL draw -vertex $data($drainageinlet,start)
    GiD_OpenGL draw -vertex $data($drainageinlet,end)
    GiD_OpenGL draw -end
    GiD_OpenGL draw -begin lines
    GiD_OpenGL draw -vertex $data($drainageinlet,start)
    GiD_OpenGL draw -vertex $data($drainageinlet,end)
    GiD_OpenGL draw -end
    #show label
    set p [MathUtils::VectorSum $data($drainageinlet,start) $data($drainageinlet,end)]
    set p [MathUtils::ScalarByVectorProd 0.5 $p]
    GiD_OpenGL draw -rasterpos $p
    GiD_OpenGL drawtext $drainageinlet
    return 0
}

proc DrainageInlet::DestroyDrainageInletWindow { W w } {   
    if { $W == $w } {
        DrainageInlet::EndDraw ""
        DrainageInlet::FillProblemDataFromTclData             
    }
}

proc DrainageInlet::OnChangeType { f } {
    variable current_value
    if { $current_value(type) == 1 } {
        #Longitudinal 
        grid $f.lA $f.eA   
        grid $f.lB $f.eB
        $f.lLength configure -text [= "Length"]        
        grid $f.lWidth $f.eWidth
    } elseif { $current_value(type) == 2 } {
        #Transversal
        grid $f.lA $f.eA   
        grid $f.lB $f.eB
        $f.lLength configure -text [= "Length (m)"]        
        grid $f.lWidth $f.eWidth
    } else {
        #Manhole
        grid remove $f.lA $f.eA   
        grid remove $f.lB $f.eB
        $f.lLength configure -text [= "Diameter (m)"]
        grid remove $f.lWidth $f.eWidth
    }
}

proc DrainageInlet::OnChangeConnType { f } {
    variable current_value
    if { $current_value(conntype) == 1 } {
        #To network
        grid $f.lend $f.eend $f.bend 
        # grid $f.lA $f.eA   
        # grid $f.lB $f.eB
        # $f.lLength configure -text [= "Length"]        
        # grid $f.lWidth $f.eWidth
    } elseif { $current_value(conntype) == 2 } {
        #Sink
        set f.eend $f.estart
        grid remove $f.lend $f.eend $f.bend   
        # grid $f.lB $f.eB
        # $f.lLength configure -text [= "Length (m)"]        
        # grid $f.lWidth $f.eWidth
        # } else {
        # Manhole
        # grid remove $f.lA $f.eA   
        # grid remove $f.lB $f.eB
        # $f.lLength configure -text [= "Diameter (m)"]
        # grid remove $f.lWidth $f.eWidth
    }
}

proc DrainageInlet::PickPoint { entry } {
    set view_mode [GiD_Info Project ViewMode]
    if { $view_mode == "GEOMETRYUSE" } {
        set xyz [GidUtils::GetCoordinates [_ "Pick point on a surface"] PointInSurface GEOMETRYUSE]
        #set surface_id $::GidPriv(selection)  
    } elseif { $view_mode == "MESHUSE"} {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an element"] PointInElement MESHUSE]
        #set element_id $::GidPriv(selection)    
    } else {
        set xyz ""
    }        
    if { $xyz != "" } {                    
        set res [format "%g %g %g" {*}$xyz]
        $entry delete 0 end
        $entry insert end $res        
    }
}
proc DrainageInlet::PickPointInSurfaceOrElementCmd { entry } {
    set view_mode [GiD_Info Project ViewMode]
    if { $view_mode == "GEOMETRYUSE" } {
        set xyz [GidUtils::GetCoordinates [_ "Pick point on a surface"] PointInSurface GEOMETRYUSE]
        #set surface_id $::GidPriv(selection)  
    } elseif { $view_mode == "MESHUSE"} {
        set xyz [GidUtils::GetCoordinates [_ "Pick point on an element"] PointInElement MESHUSE]
        #set element_id $::GidPriv(selection)    
    } else {
        set xyz ""
    }        
    if { $xyz != "" } {                    
        set res [format "%g %g %g" {*}$xyz]
        $entry delete 0 end
        $entry insert end $res        
    }
}

proc DrainageInlet::PickPointInLineOrElementCmd { entry } {
    set view_mode [GiD_Info Project ViewMode]
    if { $view_mode == "GEOMETRYUSE" } {
        set xyz [GidUtils::GetCoordinates [_ "Pick point on a line"] PointInLine GEOMETRYUSE]
        #set surface_id $::GidPriv(selection)  
    } elseif { $view_mode == "MESHUSE"} {
        set xyz [GidUtils::GetCoordinates [_ "Pick point on an element"] PointInElement MESHUSE]
        #set element_id $::GidPriv(selection)    
    } else {
        set xyz ""
    }        
    if { $xyz != "" } {                    
        set res [format "%g %g %g" {*}$xyz]
        $entry delete 0 end
        $entry insert end $res        
    }
}

proc DrainageInlet::Inletsfromfile {vv ss ct ee tt AA BB LL WiWi EfEf WW OO II nn nt taula} {
    variable data
    variable drainageinlets
    variable current_drainageinlet
    variable current_value
    variable table
    variable fields {visible start conntype end type A B Length Width Effective_area Weir_Cd Orifice_Cd Inlet_Discharge table_value}
    
    DrainageInlet::FillTclDataFromProblemData
    set w .gid.drainageinlet    
    InitWindow $w [= "Grates and Manholes"] PreDrainageInletWindowGeom DrainageInlet::Window
    set f [ttk::frame $w.fdrainageinlets]
    package require fulltktree
    set columns ""
    foreach name [list [= "Time (s)"] [= "Q (m3/s)"]] {
        lappend columns [list 4 $name left text 1]
    }
    set T [fulltktree $f.t -columns $columns -showlines 0 -showheader 1 -expand 1]
    
    set current_drainageinlet $nn
    set DrainageInlet::current_value(visible) $vv
    set DrainageInlet::current_value(start) $ss
    set DrainageInlet::current_value(conntype) $ct
    if { $ct == 2 } { set ee $ss }
    set DrainageInlet::current_value(end) $ee
    set DrainageInlet::current_value(type) $tt
    set DrainageInlet::current_value(A) $AA
    set DrainageInlet::current_value(B) $BB
    set DrainageInlet::current_value(Length) $LL
    set DrainageInlet::current_value(Width) $WiWi
    set DrainageInlet::current_value(Effective_area) $EfEf
    set DrainageInlet::current_value(Weir_Cd) $WW
    set DrainageInlet::current_value(Orifice_Cd) $OO
    set DrainageInlet::current_value(Inlet_Discharge) $II
    set DrainageInlet::current_value(table_value) $taula
    # TT ha de ser el doble de nombre de files
    
    
    if { ![info exists drainageinlets] } {
        DrainageInlet::SetDefaultValues
    }       
    
    DrainageInlet::ApplyInletsfromfile $taula
    DrainageInlet::Apply T    
    destroy $w        
}

proc DrainageInlet::Window { } {
    variable data
    variable drainageinlets
    variable current_drainageinlet
    variable current_value
    variable table
    
    if { ![info exists drainageinlets] } {
        DrainageInlet::SetDefaultValues
    }
    
    DrainageInlet::FillTclDataFromProblemData    
    
    set w .gid.drainageinlet
    InitWindow $w [= "Grates and Manholes"] PreDrainageInletWindowGeom DrainageInlet::Window
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    set f [ttk::frame $w.fdrainageinlets]
    
    ttk::combobox $f.cb1 -textvariable DrainageInlet::current_drainageinlet -values [DrainageInlet::GetDrainageInlets] -state readonly
    bind $f.cb1 <<ComboboxSelected>> [list DrainageInlet::OnChangeSelectedDrainageInlet %W]
    ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list DrainageInlet::NewDrainageInlet $f.cb1]
    GidHelp $f.bnew  [= "Create a new drainage inlet"]
    ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list DrainageInlet::DeleteDrainageInlet $f.cb1]
    GidHelp $f.bdel  [= "Delete a drainage inlet"]
    ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list DrainageInlet::RenameDrainageInlet $f.cb1]
    GidHelp $w.f.bren  [= "Rename a drainage inlet"]   
    grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
    grid $f -sticky new
    grid rowconfigure $f 0 -weight 1
    
    set f [ttk::frame $w.fdrainageinlet]        
    set field visible
    ttk::checkbutton $f.ch$field -text [= "Visible"] -variable DrainageInlet::current_value($field)
    grid $f.ch$field -sticky w
    
    foreach field {start} text [list [= "Surface location"]] {
        ttk::label $f.l$field -text $text
        ttk::entry $f.e$field -textvariable DrainageInlet::current_value($field)
        ttk::button $f.b$field -image [gid_themes::GetImage "point.png" small_icons] -command [list DrainageInlet::PickPointInSurfaceOrElementCmd $f.e$field]
        grid $f.l$field $f.e$field $f.b$field -sticky ew
    }
    foreach field {conntype} text [list [= "Connection type"]] values {{1 2}} labels [list [list [= "To network"] [= "Sink"]]] {
        ttk::label $f.l$field -text $text
        #ttk::combobox $f.c$field -values $values -state readonly -textvariable DrainageInlet::current_value($field)
        TTKComboBox $f.c$field -values $values -labels $labels -state readonly -textvariable DrainageInlet::current_value($field) \
            -modifycmd [list DrainageInlet::OnChangeConnType $f]
        grid $f.l$field $f.c$field -sticky ew
    }
    foreach field {end} text [list [= "Network location"]] {
        ttk::label $f.l$field -text $text
        ttk::entry $f.e$field -textvariable DrainageInlet::current_value($field)
        ttk::button $f.b$field -image [gid_themes::GetImage "point.png" small_icons] -command [list DrainageInlet::PickPointInLineOrElementCmd $f.e$field]
        grid $f.l$field $f.e$field $f.b$field -sticky ew
    }
    foreach field {type} text [list [= "Type"]] values {{1 2 3}} labels [list [list [= "Longitudinal Grate"] [= "Transversal Grate"] [= "Manhole"]]] {
        ttk::label $f.l$field -text $text
        #ttk::combobox $f.c$field -values $values -state readonly -textvariable DrainageInlet::current_value($field)
        TTKComboBox $f.c$field -values $values -labels $labels -state readonly -textvariable DrainageInlet::current_value($field) \
            -modifycmd [list DrainageInlet::OnChangeType $f]
        grid $f.l$field $f.c$field -sticky ew
    }
    foreach field {A B Length Width Effective_area Weir_Cd Orifice_Cd} text [list [= "A"] [= "B"] [= "Length (m)"] [= "Width (m)"] [= "Effective area (%)"] [= "Weir Cd"] [= "Orifice Cd"]] {
        ttk::label $f.l$field -text $text
        ttk::entry $f.e$field -textvariable DrainageInlet::current_value($field)
        grid $f.l$field $f.e$field -sticky ew
    }
    
    set field Inlet_Discharge
    ttk::checkbutton $f.ch$field -text [= "Inlet Discharge"] -variable DrainageInlet::current_value($field)
    grid $f.ch$field -sticky w
    
    grid $f -sticky new
    grid columnconfigure $f {1} -weight 1
    
    
    package require fulltktree
    set columns ""
    foreach name [list [= "Time (s)"] [= "Q (m3/s)"]] {
        lappend columns [list 4 $name left text 1]
    }
    set T [fulltktree $f.t -columns $columns -showlines 0 -showheader 1 -expand 1]    
    set table $T
    $T configure -editbeginhandler GridData::EditBegin
    $T configure -editaccepthandler [list GridData::EditAccept]
    $T configure -deletehandler GridData::RemoveRows
    $T configure -contextualhandler_menu GridData::ContextualMenu    
    $T column configure all -button 0
    
    ttk::frame $f.fbuttons
    ttk::button $f.fbuttons.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $T 1]
    GidHelp $f.fbuttons.b1 [_ "Pressing this button, one new line will be added to this window"]    
    ttk::button $f.fbuttons.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $T 1]
    GidHelp $f.fbuttons.b2 [_ "Pressing this button, one line is deleted from this window"]        
    
    ttk::button $f.fbuttons.b3 -image [gid_themes::GetImage graphs.png small_icons] -text [_ "Graph"] \
        -command [list GridData::PlotCurveWin $T [= "Drainage Inlet"] ""]
    GidHelp $f.fbuttons.b3 [_ "Pressing this button, a XY graph will be drawn"]
    
    grid $T -sticky nsew -columnspan 2
    grid $f.fbuttons.b1 $f.fbuttons.b2 $f.fbuttons.b3 -sticky w    
    grid $f.fbuttons -sticky ew
    
    
    
    #    grid columnconfigure $f {0} -weight 1
    grid rowconfigure $f {1 4} -weight 1
    grid $f -sticky nsew -padx 2 -pady 2
    GidHelp $f [= "Set Drainage Inlet parameters"]
    
    bind $T <<Paste>> [list ::GridData::Paste $T CLIPBOARD]
    bind $T <<PasteSelection>> [list ::GridData::Paste $T PRIMARY]
    bind $T <<Cut>> [list GridData::Cut $T]
    bind $T <<Copy>> [list GridData::Copy $T]
    bind [$T givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header
    
    GridData::SetData $T $current_value(table_value)
    
    
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.bapply -text [= "Apply"] -command [list DrainageInlet::Apply $T] -style BottomFrame.TButton
    #ttk::button $f.bdraw -text [= "Draw"] -command [list DrainageInlet::StartDraw $f.bdraw] -style BottomFrame.TButton
    ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    #grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
    grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }        
    
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    bind $w <Alt-c> "$f.bclose invoke"
    bind $w <Escape> "$f.bclose invoke"   
    bind $w <Destroy> [list +DrainageInlet::DestroyDrainageInletWindow %W $w] ;# + to add to previous script  
}

proc DrainageInlet::CloseWindow { } {
    set w .gid.drainageinlet    
    if { [winfo exists $w] } {
        destroy $w
    }
}


