#### created by GONZALOALEN Enero/2023
###################################################################################
namespace eval Pest {
    variable data ; # array of values
    variable settings ; # list of names of data
    variable current_varset ; # name of current selected varset
    variable current_value ; # array values linked to widgets that represent the current one
    variable fields {gauge_num start first end noptmax phiredstp nphistp nphinored \
      relparstp nrelpar phiratsuf phiredlam numlam model manning_mul CN_mul \
      Ia_mul Suct_mul Por_mul Sat_mul Ks_mul Loss_mul Soil_mul table_value_Tq \
      table_wei visible1 visible2 table_manning_sin path_PEST qorh \
      withmulti table_manning_con table_value_Th table_value_Ts}
    variable fields_defaults {1 1 1 1 30 0.005 4 4 0.005 4 0.3 0.03 10 1 \
      {0.0 0.0 0.0} {0.0 0.0 0.0} {0.0 0.0 0.0} {0.0 0.0 0.0} {0.0 0.0 0.0} \
      {0.0 0.0 0.0} {0.0 0.0 0.0} {0.0 0.0 0.0} {0.0 0.0 0.0} {0.0 0.0 0.0} 0.0 0 0 \
      {0.0 0.0 0.0} "...\\pest17" 0 0 {0.0} {0.0 0.0 0.0} {0.0 0.0 0.0} }
    variable table ; # tktable widget
    variable Tpar_sinmulti
    variable Tpar_conmulti
    variable Twei
    variable Tq
    variable Th
    variable Ts
}

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# Funciones de escritura de los *.bas
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
proc Pest::WriteOutputTemplateFile { } { ; # Iber_PEST_output.ins
    variable data
    set res ""
    set i 0

    foreach varset [Pest::Getsettings] {
      if {$data($varset,qorh) == 0} { ; # Caudal en basin outlet (Qout)
        append res "@time(s);Qout(m3/s)@" \n
        incr i
        set lines [expr [llength $data($varset,table_value_Tq)]/3]
        for {set i 1} {$i < $lines + 1} { incr i } {
          if {$i == 1} {
            append res "l$data($varset,first)" \t
          } else {
            append res "l1"  \t
          }
          append res "\[obs$i]26:41"  \n
        }
      } elseif {$data($varset,qorh) == 1} { ; # Caudal en stream gauge (Qin,suf1)
        append res "@time(s);Qout(m3/s)@" \n
        incr i
        set lines [expr [llength $data($varset,table_value_Ts)]/3]
        for {set i 1} {$i < $lines + 1} { incr i } {
          if {$i == 1} {
            append res "l$data($varset,first)" \t
          } else {
            append res "l1"  \t
          }
          append res "@;@ @;@ @;@ @;@ "
          for {set j 1} {$j < $data($varset,gauge_num)} { incr j } {
            append res "@;@ "
          }
          append res "!obs$i! @;@"  \n
        }
      } else { ; # Water depth en gauge
        append res "@time(s);zb(m);h(m);wse(m);qx(m2/s);qy(m2/s)@" \n
        incr i
        set lines [expr [llength $data($varset,table_value_Th)]/3]
        for {set i 1} {$i < $lines + 1} { incr i } {
          if {$i == 1} {
            append res "l$data($varset,first)" \t
          } else {
            append res "l1"  \t
          }
          append res "@;@ @;@ @;@ !obs$i! @;@"  \n
        }

      }
    }
    return $res
}

proc Pest::WriteInput1CN { CN_val Ia_val } { ; # Iber_PEST_input1.tpl
    variable data
    set res ""

    foreach varset [Pest::Getsettings] {
      if {$data($varset,visible1) == 1 && $data($varset,model) == 1} { ; # Si hay calibracion de parametros hidrologicos y el modelo es CN
        append res "2 "
        if { [lindex $data($varset,CN_mul) 1] != [lindex $data($varset,CN_mul) 2] } {
            append res "#mcn  # "
        } else {
          append res $CN_val
          append res " "
        }

        if { [lindex $data($varset,Ia_mul) 1] != [lindex $data($varset,Ia_mul) 2] } {
          append res "#cia  #"
        } else {
          append res $Ia_val
        }
      } else {
        append res "2 $CN_val $Ia_val"
      }
    }
    return $res
}

proc Pest::WriteInput1GA { Suc_val Por_val Sat_val Ks_val \
                           Loss_val Soil_val } { ; # Iber_PEST_input1.tpl
    variable data
    set res ""

    foreach varset [Pest::Getsettings] {
      if {$data($varset,visible1) == 1 && $data($varset,model) == 2} { ; # Si hay que calibrar parametros hidrologicos y el modelo es Green&Ampt
        append res "4 "
        if { [lindex $data($varset,Suct_mul) 1] != [lindex $data($varset,Suct_mul) 2] } { ; # Si se calibra succion
            append res "#msu  # "
        } else {
          append res $Suc_val
          append res " "
        }
        if { [lindex $data($varset,Por_mul) 1] != [lindex $data($varset,Por_mul) 2] } { ; # Si se calibra porosidad
            append res "#mpo  # "
        } else {
          append res $Por_val
          append res " "
        }
        if { [lindex $data($varset,Sat_mul) 1] != [lindex $data($varset,Sat_mul) 2] } { ; # Si se calibra saturacion
            append res "#msa  # "
        } else {
          append res $Sat_val
          append res " "
        }
        if { [lindex $data($varset,Ks_mul) 1] != [lindex $data($varset,Ks_mul) 2] } { ; # Si se calibra ks
            append res "#mks  # "
        } else {
          append res $Ks_val
          append res " "
        }
        if { [lindex $data($varset,Loss_mul) 1] != [lindex $data($varset,Loss_mul) 2] } { ; # Si se calibra initial losses
            append res "#mlo  # "
        } else {
          append res $Loss_val
          append res " "
        }
        if { [lindex $data($varset,Soil_mul) 1] != [lindex $data($varset,Soil_mul) 2] } { ; # Si se calibra soil depth
            append res "#msd  # "
        } else {
          append res $Soil_val
          append res " "
        }
      } else {
        append res "4 $Suc_val $Por_val $Sat_val $Ks_val $Loss_val $Soil_val"
      }
    }
    return $res
}

proc Pest::WritePar2Par { } { ; # Iber_PEST_par2par.tpl
    variable data
    set res ""

    append res "* parameter data" \n

    foreach varset [Pest::Getsettings] {

      # Variables del hidrológico
      if {$data($varset,visible1) == 1} {
        if { $data($varset,model) == 1 } { ; # CN
          # Por si no queremos calibrar CN (hay que poner los rangos de variacion iguales)
          if { [lindex $data($varset,CN_mul) 1] != [lindex $data($varset,CN_mul) 2] } {
            append res "mcn = \$   cnm  $" \n
          }
          # Por si no queremos calibrar Ia
          if { [lindex $data($varset,Ia_mul) 1] != [lindex $data($varset,Ia_mul) 2] } {
            append res "cia = \$   cia  $" \n
          }

        } elseif { $data($varset,model) == 2 } { ; # Green&Ampt
          # Por si no queremos calibrar succion
          if { [lindex $data($varset,Suct_mul) 1] != [lindex $data($varset,Suct_mul) 2] } {
            append res "msu = \$   sum  $" \n
          }
          # Por si no queremos calibrar porosidad
          if { [lindex $data($varset,Por_mul) 1] != [lindex $data($varset,Por_mul) 2] } {
            append res "mpo = \$   pom  $" \n
          }
          # Por si no queremos calibrar saturacion
          if { [lindex $data($varset,Sat_mul) 1] != [lindex $data($varset,Sat_mul) 2] } {
            append res "msa = \$   sam  $" \n
          }
          # Por si no queremos calibrar Ks
          if { [lindex $data($varset,Ks_mul) 1] != [lindex $data($varset,Ks_mul) 2] } {
            append res "mks = \$   ksm  $" \n
          }
          # Por si no queremos calibrar Initial losses
          if { [lindex $data($varset,Loss_mul) 1] != [lindex $data($varset,Loss_mul) 2] } {
            append res "mlo = \$   lom  $" \n
          }
          # Por si no queremos calibrar soil depth
          if { [lindex $data($varset,Soil_mul) 1] != [lindex $data($varset,Soil_mul) 2] } {
            append res "msd = \$   sdm  $" \n
          }
        }
      }

      # Variables del Manning
      if {$data($varset,visible2) == 1} {
        set lc [llength $data($varset,table_manning_con)]
        set ls [llength $data($varset,table_manning_sin)]
        set iaux 0
        if {$data($varset,withmulti) == 0} { ; # Calibra con el multiplicador del manning
          for {set ii 0} {$ii < $lc} {set ii [expr $ii+1]} {
              set iaux [expr $iaux+1]
              set i [lrange $data($varset,table_manning_con) $ii $ii]
              append res "n$iaux = $i * \$   mam  $"  \n
          }
        } else {
          for {set ii 0} {$ii < $ls} {set ii [expr $ii+3]} {
              set iaux [expr $iaux+1]
              set i [lrange $data($varset,table_manning_sin) $ii $ii]
              append res "n$iaux = \$   ma$iaux  $"  \n
          }
        }
      }
      append res "* template and model input files" \n
      if {$data($varset,visible1) == 1} {
        # Se calibran los parámetros hidrológicos
        append res "Iber_PEST_input1.tpl	Iber_losses.dat" \n
      }
      if {$data($varset,visible2) == 1 } {
        # Se calibra la rugosidad
        append res "Iber_PEST_input2.tpl	Iber2D.dat" \n
      }

      append res "* control data" \n
      append res "single point" \n
    }
    return $res
}

proc Pest::WritePaths { path_ref } { ; # Iber_PEST_batchfile.bat
    variable data
    set res ""

    foreach varset [Pest::Getsettings] {
      if { $path_ref == 1 } { ; # Escribe el path de PEST
        append res $data($varset,path_PEST)
        append res "\\par2par"
      } elseif { $path_ref == 2 } { ; # Escribe el path de los ejecutables de Iber
        append res "if exist Iber_CPUSolver.dat "
        append res [Iber::GetFullPathExe Iber.exe]
        append res "\n"
        append res "if exist Iber_GPUSolver.dat "
        append res [Iber::GetFullPathExe IberPlus.exe]
        append res "\n"
        append res "if exist Iber_RIberSolver.dat "
        append res [Iber::GetFullPathExe RIber.exe]
      }
    }
    return $res
}

proc Pest::DelFiles { } { ; # Iber_PEST_batchfile.bat
    variable data
    set res ""

    foreach varset [Pest::Getsettings] {
      if {$data($varset,visible1) == 1} { ; # Si se calibran los parámetros hidrológicos
        append res "del Iber_losses.dat"
        if {$data($varset,visible2) == 1} {
          append res \n
        }
      }
      if {$data($varset,visible2) == 1} { ; # Si se calibran la rugosidad
        append res "del Iber2D.dat"
      }
    }
    return $res
}

proc Pest::WriteRunPest { } { ; # Iber_PEST_runPEST.bat
    variable data
    set res ""

    foreach varset [Pest::Getsettings] {
      if {$data($varset,end) == 1} {
        append res $data($varset,path_PEST)
        append res "\\pest "
        append res "iber_PEST_case" \n
      } elseif {$data($varset,end) == 2} {
        append res "rem First it is created the new control file (regularisation)" \n
        append res $data($varset,path_PEST)
        append res "\\addreg1 "
        append res "iber_PEST_case "
        append res "iber_PEST_case_regularisation" \n
        append res "rem Then it is runned the new control file" \n
        append res $data($varset,path_PEST)
        append res "\\pest "
        append res "iber_PEST_case_regularisation" \n
      }
    }
    return $res
}

proc Pest::WriteRunPestchek { } { ; # Iber_PEST_runPEST.bat
    variable data
    set res ""
    foreach varset [Pest::Getsettings] {
      append res $data($varset,path_PEST)
      append res "\\pestchek "
      append res "iber_PEST_case" \n
    }
    return $res
}

proc Pest::CheckVar { } { ; # Iber_PEST_runPEST.bat
    variable data
    set res ""

    foreach varset [Pest::Getsettings] {
      if {($data($varset,visible1) == 0) && ($data($varset,visible2) == 0)} {
        append res "rem check if there are variable to calibrate" \n
        append res "echo ERROR: There are not variable to calibrate." \n
        append res "pause" \n
        append res "exit" \n
      }
    }
    return $res
}

proc Pest::WriteControlFile { } { ; # iber_PEST_case.pst
    variable data
    set res ""
    set npar 0

    append res "* control data"  \n

    foreach varset [Pest::Getsettings] {
      if {$data($varset,start) == 1} {
        append res "restart "
      } elseif {$data($varset,start) == 2} {
        append res "norestart "
      }
      if {$data($varset,end) == 1} {
        append res "estimation"  \n
      } elseif {$data($varset,end) == 2} {
        append res "estimation"  \n ; # Se cambia luego el modo con addreg1
      }

      # Numero total de parámetros
      if {$data($varset,visible1) == 1} {
        # Hay variables hidrologias
        if { $data($varset,model) == 1} {
          # Modelo CN
          # Si se calibran ambos parametros de CN:
          if { ([lindex $data($varset,CN_mul) 1] != [lindex $data($varset,CN_mul) 2]) && ([lindex $data($varset,Ia_mul) 1] != [lindex $data($varset,Ia_mul) 2]) } {
            set npar [expr {$npar + 2}]  ; #CN y coeficiente Ia
          } else { ; # Si se calibra solo uno de los dos
            set npar [expr {$npar +1}] ; # CN o coeficiente Ia
          }
        } elseif {$data($varset,model) == 2} {
          # Modelo Green&Ampt
          # Vamos incrementando el valor de npar segun el numero de parametros que se calibren
          if { [lindex $data($varset,Suct_mul) 1] != [lindex $data($varset,Suct_mul) 2] } { ; # Si se calibra succion
            incr npar
          }
          if { [lindex $data($varset,Por_mul) 1] != [lindex $data($varset,Por_mul) 2] } { ; # Si se calibra porosidad
            incr npar
          }
          if { [lindex $data($varset,Sat_mul) 1] != [lindex $data($varset,Sat_mul) 2] } { ; # Si se calibra saturacion
            incr npar
          }
          if { [lindex $data($varset,Ks_mul) 1] != [lindex $data($varset,Ks_mul) 2] } { ; # Si se calibra ks
            incr npar
          }
          if { [lindex $data($varset,Loss_mul) 1] != [lindex $data($varset,Loss_mul) 2] } { ; # Si se calibra initial losses
            incr npar
          }
          if { [lindex $data($varset,Soil_mul) 1] != [lindex $data($varset,Soil_mul) 2] } { ; # Si se calibra soil depth
            incr npar
          }
        }
      }
      if {$data($varset,visible2) == 1} { ; # Hay que calibrar multiplicador manning
        if {$data($varset,withmulti) == 0} { ; # Calibra con el multiplicador del manning
          set npar [expr {$npar + 1}]
        } else {
          set ls [llength $data($varset,table_manning_sin)]
          set npar [expr {$npar + $ls/3}]
        }
      }
      # Numero de gurpos de parametros igual al numero de parametros
      set npargp $npar


      # Numero total de observaciones
      if {$data($varset,qorh) == 0} { ; # Caudal en basin outlet (Qout)
        set nobs [expr [llength $data($varset,table_value_Tq)]/3]
      } elseif {$data($varset,qorh) == 1} { ; # Caudal en stream gauge (Qin,surf)
        set nobs [expr [llength $data($varset,table_value_Ts)]/3]
      } else { ; # Water depth en gauge
        set nobs [expr [llength $data($varset,table_value_Th)]/3]
      }

      # Numero de archivos de entrada
      set fil_in 1 ; # Es siempre 1 porque utilizo el puente par2par

      # Escribo en el archivo
      append res "$npar $nobs $npargp	0	1"  \n
      append res "$fil_in	1	single	point	1	0	0"  \n
      append res "10.0	2.0	$data($varset,phiratsuf)	$data($varset,phiredlam)	$data($varset,numlam)"  \n
      append res "10.0	10.0	0.001"  \n
      append res "0.1"  \n
      append res "$data($varset,noptmax)	$data($varset,phiredstp)	$data($varset,nphistp)	$data($varset,nphinored)	$data($varset,relparstp)	$data($varset,nrelpar)"  \n
      append res "1	1	1"  \n
      append res "* singular value decomposition" \n
      append res "1" \n
      append res "$npar 5.0e-7" \n
      append res "0" \n
      append res "* parameter groups"  \n
      set j $data($varset,visible1)
      set i $data($varset,model)
      if { $data($varset,visible1) == 1 } { ; # Hay variables del hidrologico
        if { $data($varset,model) == 1 } {
        # SCS
          if {$data($varset,visible2) == 1} { ; # Tenemos tambien que calibrar manning
            if { [lindex $data($varset,CN_mul) 1] != [lindex $data($varset,CN_mul) 2] } { ; # Si se calibra CN
              append res "curnum	relative	0.001  0.001 switch	2.0	parabolic"  \n
            }
            if { [lindex $data($varset,Ia_mul) 1] != [lindex $data($varset,Ia_mul) 2] } { ; # Si se calibra Ia
              append res "Iacoef	relative	0.005  0.001 switch  2.0 parabolic"  \n
            }
            if {$data($varset,withmulti) == 0} { ; # Calibra con el multiplicador del manning
              append res "Mannin	relative	0.001  0.001 switch  2.0 parabolic"  \n
            } else {
              set ls [llength $data($varset,table_manning_sin)]
              set iiaux 0
              for {set ii 0} {$ii < $ls} {set ii [expr $ii+3]} {
                set iiaux [expr $iiaux + 1]
                append res "Mannin$iiaux	relative	0.01	0.0005	switch	2.0	parabolic"  \n
              }
            }
            append res "* parameter data"  \n
            if { [lindex $data($varset,CN_mul) 1] != [lindex $data($varset,CN_mul) 2] } { ; # Si se calibra CN
              set CN_mul_mod1 [expr [lrange $data($varset,CN_mul) 0 0] * 1 + 0]
              set CN_mul_mod2 [expr [lrange $data($varset,CN_mul) 1 1] * 1 + 0]
              set CN_mul_mod3 [expr [lrange $data($varset,CN_mul) 2 2] * 1 + 0]
              foreach var_aux {CN_mul_mod1 CN_mul_mod2 CN_mul_mod3} {
                if {[set $var_aux] == 0} {
                  set $var_aux 1e-10
                }
              }
              append res "cnm	log	factor	$CN_mul_mod1	$CN_mul_mod2	$CN_mul_mod3	curnum	1.0	0.0	1"  \n
            }
            if { [lindex $data($varset,Ia_mul) 1] != [lindex $data($varset,Ia_mul) 2] } { ; # Si se calibra Ia
              set Ia_mul_mod1 [expr [lrange $data($varset,Ia_mul) 0 0 ] * 1 + 0]
              set Ia_mul_mod2 [expr [lrange $data($varset,Ia_mul) 1 1 ] * 1 + 0]
              set Ia_mul_mod3 [expr [lrange $data($varset,Ia_mul) 2 2 ] * 1 + 0]
              foreach var_aux {Ia_mul_mod1 Ia_mul_mod2 Ia_mul_mod3} {
                if {[set $var_aux] == 0} {
                  set $var_aux 1e-10
                }
              }
              append res "cia	log	factor	$Ia_mul_mod1	$Ia_mul_mod2	$Ia_mul_mod3	Iacoef	1.0	0.0	1"  \n
            }
            if {$data($varset,withmulti) == 0} { ; # Calibra con el multiplicador del manning
              set Man1 [expr [lrange $data($varset,manning_mul) 0 0 ] * 1 + 0]
              set Man2 [expr [lrange $data($varset,manning_mul) 1 1 ] * 1 + 0]
              set Man3 [expr [lrange $data($varset,manning_mul) 2 2 ] * 1 + 0]
              foreach var_aux {Man1 Man2 Man3} {
                if {[set $var_aux] == 0} {
                  set $var_aux 1e-10
                }
              }
              append res "mam	log	factor	$Man1	$Man2	$Man3	Mannin	1.0	0.0	1"  \n
            } else { ; # Calibra el Manning segun los limites impuestos por el modelador
              set ls [llength $data($varset,table_manning_sin)]
              set iip 0
              for {set ii 0} {$ii < $ls} {set ii [expr $ii+3]} {
                  set iip [expr $iip+1]
                  append res "ma$iip log factor "
                  for {set i 0} {$i < 3} {set i [expr $i+1]} {
                    set Manningp [expr {[lrange $data($varset,table_manning_sin) [expr $ii+$i] [expr $ii+$i]] * 1 + 0}]
                    if {$i == 1} {
                      if {$Manningp == 0} {
                        set Manningp 1e-10
                      }
                    }
                    append res "$Manningp "
                  }
                  append res "Mannin$iip	1.0	0.0	1" \n
              }
            }
          } else {
            if { [lindex $data($varset,CN_mul) 1] != [lindex $data($varset,CN_mul) 2] } { ; # Si se calibra CN
              append res "curnum	relative	0.001  0.001 switch	2.0	parabolic"  \n
            }
            if { [lindex $data($varset,Ia_mul) 1] != [lindex $data($varset,Ia_mul) 2] } { ; # Si se calibra Ia
              append res "Iacoef	relative	0.005	0.001	switch	2.0	parabolic"  \n
            }
            append res "* parameter data"  \n
            if { [lindex $data($varset,CN_mul) 1] != [lindex $data($varset,CN_mul) 2] } { ; # Si se calibra CN
              set CN_mul_mod1 [expr [lrange $data($varset,CN_mul) 0 0] * 1 + 0]
              set CN_mul_mod2 [expr [lrange $data($varset,CN_mul) 1 1] * 1 + 0]
              set CN_mul_mod3 [expr [lrange $data($varset,CN_mul) 2 2] * 1 + 0]
              foreach var_aux {CN_mul_mod1 CN_mul_mod2 CN_mul_mod3} {
                if {[set $var_aux] == 0} {
                  set $var_aux 1e-10
                }
              }
              append res "cnm	log	factor	$CN_mul_mod1	$CN_mul_mod2	$CN_mul_mod3	curnum	1.0	0.0	1"  \n
            }
            if { [lindex $data($varset,Ia_mul) 1] != [lindex $data($varset,Ia_mul) 2] } { ; # Si se calibra Ia
              set Ia_mul_mod1 [expr [lrange $data($varset,Ia_mul) 0 0 ] * 1 + 0]
              set Ia_mul_mod2 [expr [lrange $data($varset,Ia_mul) 1 1 ] * 1 + 0]
              set Ia_mul_mod3 [expr [lrange $data($varset,Ia_mul) 2 2 ] * 1 + 0]
              foreach var_aux {Ia_mul_mod1 Ia_mul_mod2 Ia_mul_mod3} {
                if {[set $var_aux] == 0} {
                  set $var_aux 1e-10
                }
              }
              append res "cia	log	factor	$Ia_mul_mod1	$Ia_mul_mod2	$Ia_mul_mod3	Iacoef	1.0	0.0	1"  \n
            }
          }

        } elseif { $data($varset,model) == 2 } {
          # Green&Ampt
          if {$data($varset,visible2) == 1} { ; #Tenemos tambien que calibrar manning
            if { [lindex $data($varset,Suct_mul) 1] != [lindex $data($varset,Suct_mul) 2] } { ; # Si se calibra succion
              append res "Sucmul	relative	0.01	0.01	switch	2.0	parabolic"  \n
            }
            if { [lindex $data($varset,Por_mul) 1] != [lindex $data($varset,Por_mul) 2] } { ; # Si se calibra porosidad
              append res "Pormul	relative	0.01	0.01	switch	2.0	parabolic"  \n
            }
            if { [lindex $data($varset,Sat_mul) 1] != [lindex $data($varset,Sat_mul) 2] } { ; # Si se calibra saturacion
              append res "Satmul	relative	0.01	0.01	switch	2.0	parabolic"  \n
            }
            if { [lindex $data($varset,Ks_mul) 1] != [lindex $data($varset,Ks_mul) 2] } { ; # Si se calibra ks
              append res "Ksmul	relative	0.01	0.01	switch	2.0	parabolic"  \n
            }
            if { [lindex $data($varset,Loss_mul) 1] != [lindex $data($varset,Loss_mul) 2] } { ; # Si se calibra initial losses
              append res "Losmul	relative	0.01	0.01	switch	2.0	parabolic"  \n
            }
            if { [lindex $data($varset,Soil_mul) 1] != [lindex $data($varset,Soil_mul) 2] } { ; # Si se calibra soil depth
              append res "Sodmul	relative	0.01	0.01	switch	2.0	parabolic"  \n
            }
            if {$data($varset,withmulti) == 0} { ; # Calibra con el multiplicador del manning
              append res "Mannin	relative	0.01  0.01	switch	2.0	parabolic"  \n
            } else {
              set ls [llength $data($varset,table_manning_sin)]
              set iiaux 0
              for {set ii 0} {$ii < $ls} {set ii [expr $ii+3]} {
                set iiaux [expr $iiaux + 1]
                append res "Mannin$iiaux	relative	0.01	0.0005	switch	2.0	parabolic"  \n
              }
            }
            append res "* parameter data"  \n
            if { [lindex $data($varset,Suct_mul) 1] != [lindex $data($varset,Suct_mul) 2] } { ; # Si se calibra succion
              set Suc1 [expr [lrange $data($varset,Suct_mul) 0 0 ] * 1 + 0]
              set Suc2 [expr [lrange $data($varset,Suct_mul) 1 1 ] * 1 + 0]
              set Suc3 [expr [lrange $data($varset,Suct_mul) 2 2 ] * 1 + 0]
              foreach var {Suc1 Suc2 Suc3} {
                if {[set $var] == 0} {
                  set $var 1e-10
                }
              }
              append res "sum	log	factor	$Suc1 $Suc2 $Suc3	Sucmul	1.0	0.0	1"  \n
            }
            if { [lindex $data($varset,Por_mul) 1] != [lindex $data($varset,Por_mul) 2] } { ; # Si se calibra porosidad
              set Por1 [expr [lrange $data($varset,Por_mul) 0 0 ] * 1 + 0]
              set Por2 [expr [lrange $data($varset,Por_mul) 1 1 ] * 1 + 0]
              set Por3 [expr [lrange $data($varset,Por_mul) 2 2 ] * 1 + 0]
              foreach var {Por1 Por2 Por3} {
                if {[set $var] == 0} {
                  set $var 1e-10
                }
              }
              append res "pom	log	factor	$Por1 $Por2 $Por3	Pormul	1.0	0.0	1"  \n
            }
            if { [lindex $data($varset,Sat_mul) 1] != [lindex $data($varset,Sat_mul) 2] } { ; # Si se calibra saturacion
              set Sat1 [expr [lrange $data($varset,Sat_mul) 0 0 ] * 1 + 0]
              set Sat2 [expr [lrange $data($varset,Sat_mul) 1 1 ] * 1 + 0]
              set Sat3 [expr [lrange $data($varset,Sat_mul) 2 2 ] * 1 + 0]
              foreach var {Sat1 Sat2 Sat3} {
                if {[set $var] == 0} {
                  set $var 1e-10
                }
              }
              append res "sam	log	factor	$Sat1 $Sat2 $Sat3	Satmul	1.0	0.0	1"  \n
            }
            if { [lindex $data($varset,Ks_mul) 1] != [lindex $data($varset,Ks_mul) 2] } { ; # Si se calibra ks
              set Ks1 [expr [lrange $data($varset,Ks_mul) 0 0 ] * 1 + 0]
              set Ks2 [expr [lrange $data($varset,Ks_mul) 1 1 ] * 1 + 0]
              set Ks3 [expr [lrange $data($varset,Ks_mul) 2 2 ] * 1 + 0]
              foreach var {Ks1 Ks2 Ks3} {
                if {[set $var] == 0} {
                  set $var 1e-10
                }
              }
              append res "ksm	log	factor	$Ks1 $Ks2 $Ks3	Ksmul	1.0	0.0	1"  \n
            }
            if { [lindex $data($varset,Loss_mul) 1] != [lindex $data($varset,Loss_mul) 2] } { ; # Si se calibra initial losses
              set Los1 [expr [lrange $data($varset,Loss_mul) 0 0 ] * 1 + 0]
              set Los2 [expr [lrange $data($varset,Loss_mul) 1 1 ] * 1 + 0]
              set Los3 [expr [lrange $data($varset,Loss_mul) 2 2 ] * 1 + 0]
              foreach var {Los1 Los2 Los3} {
                if {[set $var] == 0} {
                  set $var 1e-10
                }
              }
              append res "lom	log	factor	$Los1 $Los2 $Los3	Losmul	1.0	0.0	1"  \n
            }
            if { [lindex $data($varset,Soil_mul) 1] != [lindex $data($varset,Soil_mul) 2] } { ; # Si se calibra soil depth
              set Sod1 [expr [lrange $data($varset,Soil_mul) 0 0 ] * 1 + 0]
              set Sod2 [expr [lrange $data($varset,Soil_mul) 1 1 ] * 1 + 0]
              set Sod3 [expr [lrange $data($varset,Soil_mul) 2 2 ] * 1 + 0]
              foreach var {Sod1 Sod2 Sod3} {
                if {[set $var] == 0} {
                  set $var 1e-10
                }
              }
              append res "sdm	log	factor	$Sod1 $Sod2 $Sod3	Sodmul	1.0	0.0	1"  \n
            }
            if {$data($varset,withmulti) == 0} { ; # Calibra con el multiplicador del manning
              set Man1 [expr [lrange $data($varset,manning_mul) 0 0 ] * 1 + 0]
              set Man2 [expr [lrange $data($varset,manning_mul) 1 1 ] * 1 + 0]
              set Man3 [expr [lrange $data($varset,manning_mul) 2 2 ] * 1 + 0]
              foreach var {Man1 Man2 Man3} {
                if {[set $var] == 0} {
                  set $var 1e-10
                }
              }
              append res "mam log factor  $Man1 $Man2 $Man3 Mannin  1.0 0.0 1"  \n
            } else { ; # Calibra el Manning segun los limites impuestos por el modelador
              set ls [llength $data($varset,table_manning_sin)]
              set iip 0
              for {set ii 0} {$ii < $ls} {set ii [expr $ii+3]} {
                  set iip [expr $iip+1]
                  append res "ma$iip	log	factor "
                  for {set i 0} {$i < 3} {set i [expr $i+1]} {
                    set Manningp [expr {[lrange $data($varset,table_manning_sin) [expr $ii+$i] [expr $ii+$i]] * 1 + 0}]
                    if {$i == 1} {
                      if {$Manningp == 0} {
                        set Manningp 1e-10
                      }
                    }
                    append res "$Manningp "
                  }
                  append res "Mannin$iip	1.0	0.0	1" \n
              }
            }

          } else { ; # Si no hay que calibrar Manning
            if { [lindex $data($varset,Suct_mul) 1] != [lindex $data($varset,Suct_mul) 2] } { ; # Si se calibra succion
              append res "Sucmul	relative	0.01	0.01	switch	2.0	parabolic"  \n
            }
            if { [lindex $data($varset,Por_mul) 1] != [lindex $data($varset,Por_mul) 2] } { ; # Si se calibra porosidad
              append res "Pormul	relative	0.01	0.01	switch	2.0	parabolic"  \n
            }
            if { [lindex $data($varset,Sat_mul) 1] != [lindex $data($varset,Sat_mul) 2] } { ; # Si se calibra saturacion
              append res "Satmul	relative	0.01	0.01	switch	2.0	parabolic"  \n
            }
            if { [lindex $data($varset,Ks_mul) 1] != [lindex $data($varset,Ks_mul) 2] } { ; # Si se calibra ks
              append res "Ksmul	relative	0.01	0.01	switch	2.0	parabolic"  \n
            }
            if { [lindex $data($varset,Loss_mul) 1] != [lindex $data($varset,Loss_mul) 2] } { ; # Si se calibra initial losses
              append res "Losmul	relative	0.01	0.01	switch	2.0	parabolic"  \n
            }
            if { [lindex $data($varset,Soil_mul) 1] != [lindex $data($varset,Soil_mul) 2] } { ; # Si se calibra soil depth
              append res "Sodmul	relative	0.01	0.01	switch	2.0	parabolic"  \n
            }
            append res "* parameter data"  \n
            if { [lindex $data($varset,Suct_mul) 1] != [lindex $data($varset,Suct_mul) 2] } { ; # Si se calibra succion
              set Suc1 [expr [lrange $data($varset,Suct_mul) 0 0 ] * 1 + 0]
              set Suc2 [expr [lrange $data($varset,Suct_mul) 1 1 ] * 1 + 0]
              set Suc3 [expr [lrange $data($varset,Suct_mul) 2 2 ] * 1 + 0]
              foreach var {Suc1 Suc2 Suc3} {
                if {[set $var] == 0} {
                  set $var 1e-10
                }
              }
              append res "sum	log	factor	$Suc1 $Suc2 $Suc3	Sucmul	1.0	0.0	1"  \n
            }
            if { [lindex $data($varset,Por_mul) 1] != [lindex $data($varset,Por_mul) 2] } { ; # Si se calibra porosidad
              set Por1 [expr [lrange $data($varset,Por_mul) 0 0 ] * 1 + 0]
              set Por2 [expr [lrange $data($varset,Por_mul) 1 1 ] * 1 + 0]
              set Por3 [expr [lrange $data($varset,Por_mul) 2 2 ] * 1 + 0]
              foreach var {Por1 Por2 Por3} {
                if {[set $var] == 0} {
                  set $var 1e-10
                }
              }
              append res "pom	log	factor	$Por1 $Por2 $Por3	Pormul	1.0	0.0	1"  \n
            }
            if { [lindex $data($varset,Sat_mul) 1] != [lindex $data($varset,Sat_mul) 2] } { ; # Si se calibra saturacion
              set Sat1 [expr [lrange $data($varset,Sat_mul) 0 0 ] * 1 + 0]
              set Sat2 [expr [lrange $data($varset,Sat_mul) 1 1 ] * 1 + 0]
              set Sat3 [expr [lrange $data($varset,Sat_mul) 2 2 ] * 1 + 0]
              foreach var {Sat1 Sat2 Sat3} {
                if {[set $var] == 0} {
                  set $var 1e-10
                }
              }
              append res "sam	log	factor	$Sat1 $Sat2 $Sat3	Satmul	1.0	0.0	1"  \n
            }
            if { [lindex $data($varset,Ks_mul) 1] != [lindex $data($varset,Ks_mul) 2] } { ; # Si se calibra ks
              set Ks1 [expr [lrange $data($varset,Ks_mul) 0 0 ] * 1 + 0]
              set Ks2 [expr [lrange $data($varset,Ks_mul) 1 1 ] * 1 + 0]
              set Ks3 [expr [lrange $data($varset,Ks_mul) 2 2 ] * 1 + 0]
              foreach var {Ks1 Ks2 Ks3} {
                if {[set $var] == 0} {
                  set $var 1e-10
                }
              }
              append res "ksm	log	factor	$Ks1 $Ks2 $Ks3	Ksmul	1.0	0.0	1"  \n
            }
            if { [lindex $data($varset,Loss_mul) 1] != [lindex $data($varset,Loss_mul) 2] } { ; # Si se calibra initial losses
              set Los1 [expr [lrange $data($varset,Loss_mul) 0 0 ] * 1 + 0]
              set Los2 [expr [lrange $data($varset,Loss_mul) 1 1 ] * 1 + 0]
              set Los3 [expr [lrange $data($varset,Loss_mul) 2 2 ] * 1 + 0]
              foreach var {Los1 Los2 Los3} {
                if {[set $var] == 0} {
                  set $var 1e-10
                }
              }
              append res "lom	log	factor	$Los1 $Los2 $Los3	Losmul	1.0	0.0	1"  \n
            }
            if { [lindex $data($varset,Soil_mul) 1] != [lindex $data($varset,Soil_mul) 2] } { ; # Si se calibra soil depth
              set Sod1 [expr [lrange $data($varset,Soil_mul) 0 0 ] * 1 + 0]
              set Sod2 [expr [lrange $data($varset,Soil_mul) 1 1 ] * 1 + 0]
              set Sod3 [expr [lrange $data($varset,Soil_mul) 2 2 ] * 1 + 0]
              foreach var {Sod1 Sod2 Sod3} {
                if {[set $var] == 0} {
                  set $var 1e-10
                }
              }
              append res "sdm	log	factor	$Sod1 $Sod2 $Sod3	Sodmul	1.0	0.0	1"  \n
            }
          }
        }
      }
      if {($data($varset,visible1) == 0) && ($data($varset,visible2) == 1)} {
        if {$data($varset,withmulti) == 0} { ; # Calibra con el multiplicador del manning
          append res "Mannin	relative	0.01  0.01	switch	2.0	parabolic"  \n
        } else {
          set ls [llength $data($varset,table_manning_sin)]
          set iiaux 0
          for {set ii 0} {$ii < $ls} {set ii [expr $ii+3]} {
            set iiaux [expr $iiaux + 1]
            append res "Mannin$iiaux	relative	0.01	0.0005	switch	2.0	parabolic"  \n
          }
        }
        append res "* parameter data"  \n
        if {$data($varset,withmulti) == 0} { ; # Calibra con el multiplicador del manning
          set Man1 [expr [lrange $data($varset,manning_mul) 0 0 ] * 1 + 0]
          set Man2 [expr [lrange $data($varset,manning_mul) 1 1 ] * 1 + 0]
          set Man3 [expr [lrange $data($varset,manning_mul) 2 2 ] * 1 + 0]
          foreach var {Man1 Man2 Man3} {
            if {[set $var] == 0} {
              set $var 1e-10
            }
          }
          append res "mam log factor	$Man1	$Man2	$Man3	Mannin	1.0	0.0	1"  \n
        } else { ; # Calibra el Manning segun los limites impuestos por el modelador
          set ls [llength $data($varset,table_manning_sin)]
          set iip 0
          for {set ii 0} {$ii < $ls} {set ii [expr $ii+3]} {
              set iip [expr $iip+1]
              append res "ma$iip log factor "
              for {set i 0} {$i < 3} {set i [expr $i+1]} {
                set Manningp [expr {[lrange $data($varset,table_manning_sin) [expr $ii+$i] [expr $ii+$i]] * 1 + 0}]
                if {$i == 1} {
                  if {$Manningp == 0} {
                    set Manningp 1e-10
                  }
                }
                append res "$Manningp "
              }
              append res "Mannin$iip	1.0	0.0	1" \n
          }
        }
      }

      append res "* observation groups"  \n
      append res "Obs"  \n
      append res "* observation data"  \n

      if {$data($varset,qorh) == 0} { ; # Caudal en basin outlet (Qout)
        set lines [expr [llength $data($varset,table_value_Tq)]/3]
      } elseif {$data($varset,qorh) == 1} { ; # Caudal en stream gauge (Qin,surf)
        set lines [expr [llength $data($varset,table_value_Ts)]/3]
      } else { ; # Water depth en gauge
        set lines [expr [llength $data($varset,table_value_Th)]/3]
      }

      set item [list]
      set i 0
      for {set ii 1} {$ii < [expr $lines*3] } { set ii [expr $ii+3]} {
        incr i
        append res "obs$i"  \t
        if {$data($varset,qorh) == 0} { ; # Caudal en basin outlet (Qout)
          append res "[lrange $data($varset,table_value_Tq) $ii [expr $ii+1]] " \t
        } elseif {$data($varset,qorh) == 1} { ; # Caudal en stream gauge (Qin,surf)
          append res "[lrange $data($varset,table_value_Ts) $ii [expr $ii+1]] " \t
        } else { ; # Water depth en gauge
          append res "[lrange $data($varset,table_value_Th) $ii [expr $ii+1]] " \t
        }
        append res "Obs"  \n
      }
      append res "* model command line"  \n
      append res "Iber_PEST_batchfile.bat"  \n
      append res "* model input/output"  \n
      append res "Iber_PEST_par2par.tpl	Iber_PEST_par2par.in"  \n
      if {$data($varset,qorh) == 0} { ; # Caudal en basin outlet (Qout)
        append res "Iber_PEST_output.ins	.\\TimeSeries\\Hydrology_Hydrographs.csv"  \n
      } elseif {$data($varset,qorh) == 1} { ; # Caudal en stream gauge (Qin,surf)
        append res "Iber_PEST_output.ins	\.\\TimeSeries\\Hydrology_Hydrographs.csv"  \n
      } else {
        if {$data($varset,gauge_num) < 10} {
          append res "Iber_PEST_output.ins	\.\\TimeSeries\\Gauges\\res_gauge_0$data($varset,gauge_num).csv"  \n
        } else {
          append res "Iber_PEST_output.ins	\.\\TimeSeries\\Gauges\\res_gauge_$data($varset,gauge_num).csv"  \n
        }
      }

      return $res
    }
}

proc Pest::WriteIber2DPest { elemnsmat } { ; # Iber_PEST_input2.tpl
    variable data
    set res ""
    set counters {}
    set j 0

    foreach varset [Pest::Getsettings] {
      if {$data($varset,withmulti) == 0} {
        set lc [llength $data($varset,table_manning_con)]
        set iaux 0
        set iaux2 0
        for {set ii 0} {$ii < $lc} {set ii [expr $ii+1]} {
            set iaux [expr $iaux+1]
            set i [lrange $data($varset,table_manning_con) $ii $ii]
            if { $elemnsmat == $i && $iaux2 == 0 }  {
              append res "    #n$iaux   #"  \t
              incr iaux2
            }
        }
        if {$iaux2 == 0} {
            append res "    $elemnsmat"  \t
        }
      } else {
        set ls [llength $data($varset,table_manning_sin)]
        set iaux 0
        set iaux2 0
        for {set ii 0} {$ii < $ls} {set ii [expr $ii+3]} {
            set iaux [expr $iaux+1]
            set i [lrange $data($varset,table_manning_sin) $ii $ii]
            if { $elemnsmat == $i && $iaux2 == 0 } {
              append res "    #n$iaux   #"  \t
              incr iaux2
            }
        }
        if {$iaux2 == 0} {
            append res "    $elemnsmat"  \t
        }
      }


      return $res
    }
}

proc Pest::WriteTimestep { } { ; # Iber_PEST_runPEST.bat
    variable data
    set res ""

    foreach varset [Pest::Getsettings] {
      if {$data($varset,qorh) == 0} { ; # Caudal en basin outlet (Qout)
        append res "[expr int([lrange $data($varset,table_value_Tq) 3 3] - [lrange $data($varset,table_value_Tq) 0 0]) ]"
      } elseif {$data($varset,qorh) == 1} { ; # Caudal en stream gauge (Qin,surf)
        append res "[expr int([lrange $data($varset,table_value_Ts) 3 3] - [lrange $data($varset,table_value_Tq) 0 0]) ]"
      } else { ; # Water depth en gauge
        append res "[expr int([lrange $data($varset,table_value_Th) 3 3] - [lrange $data($varset,table_value_Tq) 0 0]) ]"
      }
    }
    return $res
}

proc Pest::WriteObsbatchfile { } { ; # Iber_PEST_runPEST.bat
    variable data
    set res ""

    foreach varset [Pest::Getsettings] {
      if {$data($varset,qorh) == 0} { ; # Caudal en basin outlet (Qout)
        append res "[expr [llength $data($varset,table_value_Tq)]/3]"
      } elseif {$data($varset,qorh) == 1} { ; # Caudal en stream gauge (Qin,surf)
        append res "[expr [llength $data($varset,table_value_Ts)]/3]"
      } else { ; # Water depth en gauge
        append res "[expr [llength $data($varset,table_value_Th)]/3]"
      }
    }

    return $res
}

proc Pest::WriteIberRowsbatchfile { Max_simulation_time Timeseries_time_interval } { ; # Iber_PEST_runPEST.bat
    variable data
    set res ""

    foreach varset [Pest::Getsettings] {
      append res "[expr $Max_simulation_time/$Timeseries_time_interval + 1 - ($data($varset,first) - 1) ]"
    }

    return $res
}

proc Pest::CheckHydrologicalModels { var1 } { ; # Iber_PEST_runPEST.bat
    variable data
    set res ""

    foreach varset [Pest::Getsettings] {
      if {$data($varset,visible1) == 1} { ; # Si se calibran parámetros hidrológicos
        if {$var1 == 0} { ; # No está activado el cálculo de pérdidas por infiltración
            append res "rem it was found an error with the hydrological model selected" \n
            append res "echo ERROR: Infiltration losses are not activated and PEST is ordered to calibrate hydrological parameters" \n
            append res "echo Revise: Data - Problem data - Hydrological processes" \n
            append res "pause" \n
            append res "exit" \n
            return $res
        }
        if {$var1 == 1} { ; # G&A
          if {$data($varset,model) == 1} { ; # El modelo es CN
              append res "rem it was found an error with the hydrological model selected" \n
              append res "echo ERROR: Hydrological model selected in Problem Data (GreenAmpt) is different to the hydrological model selected in PEST (SCS)" \n
              append res "pause" \n
              append res "exit" \n
              return $res
            }
        } elseif {$var1 == 2} { ; # Horton
            append res "rem it was found an error with the hydrological model selected" \n
            append res "echo ERROR: Hydrological model selected in Problem Data (Horton) is different to the hydrological model selected in PEST" \n
            append res "pause" \n
            append res "exit" \n
            return $res
        } elseif {$var1 == 3} { ; # SCS
          if {$data($varset,model) == 2} { ; # El modelo es G&A
              append res "rem it was found an error with the hydrological model selected" \n
              append res "echo ERROR: Hydrological model selected in Problem Data (SCS) is different to the hydrological model selected in PEST (GreenAmpt)" \n
              append res "pause" \n
              append res "exit" \n
              return $res
            }
        } elseif {$var1 == 4} { ; # SCS_continuo
            append res "rem it was found an error with the hydrological model selected" \n
            append res "echo ERROR: Hydrological model selected in Problem Data (SCS continuo) is different to the hydrological model selected in PEST" \n
            append res "pause" \n
            append res "exit" \n
            return $res
        } elseif {$var1 == 5} { ; # Lineal
            append res "rem it was found an error with the hydrological model selected" \n
            append res "echo ERROR: Hydrological model selected in Problem Data (Lineal) is different to the hydrological model selected in PEST" \n
            append res "pause" \n
            append res "exit" \n
            return $res
        } elseif {$var1 == 6} { ; # Depth dependent
            append res "rem it was found an error with the hydrological model selected" \n
            append res "echo ERROR: Hydrological model selected in Problem Data (Depth dependent) is different to the hydrological model selected in PEST" \n
            append res "pause" \n
            append res "exit" \n
            return $res
        }
      }
    }
}
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Funciones de la ventanas de Pest
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
proc Pest::UnsetVariables { } {
  variable data
  variable settings
  unset -nocomplain data
  unset -nocomplain settings
}

proc Pest::browseFolder {field} {
    variable data
    set folder [tk_chooseDirectory]
    set Pest::current_value($field) $folder
}

proc Pest::SetDefaultValues { } {
    variable data
    variable settings
    variable current_varset
    variable current_value
    variable fields
    variable fields_defaults
    array unset data
    set settings {}
    foreach field $fields  {
        set current_value($field) ""
    }
    set current_varset {}
}

# store the settings information in a hiddend field of the problem data
# then this data is saved with the model without do nothing more
proc Pest::FillTclDataFromProblemData { } {
    variable data
    variable settings

    if { [catch {set x [GiD_AccessValue get gendata PestData]} msg] } {
        # reading a model created by an old version of the problemtype this gendata doesn't exists
        return
    }
    array set data $x

    set settings [list]
    foreach item [array names data *,start] {
        lappend settings [string range $item 0 end-6]
    }
    set settings [lsort -dictionary $settings]
    Pest::SetCurrentPest [lindex $settings 0]
}

proc Pest::FillProblemDataFromTclData { } {
    variable data
    variable settings
    set x [array get data]
    GiD_AccessValue set gendata PestData $x
}

proc Pest::CloseWindow { } {
    set w .gid.varset
    if { [winfo exists $w] } {
        destroy $w
    }
}

proc Pest::Getsettings { } {
    variable settings
    if { ![info exists settings] } {
        return ""
    }
    return $settings
}

proc Pest::GetCurrentPest { } {
    variable current_varset
    return $current_varset
}

proc Pest::OnChangeSelectedPest { cb } {
    Pest::SetCurrentPest [Pest::GetCurrentPest]
}

proc Pest::SetCurrentPest { varset } {
    variable data
    variable current_varset
    variable current_value
    variable fields
    variable Twei
    variable table

    #fill in the current_value variables with data
    if { $varset != "" } {
        foreach field $fields {
            if { ![info exists data($varset,$field)] } {
                set data($varset,$field) 0
            }
            set current_value($field) $data($varset,$field)
        }
    } else {
        foreach field $fields {
            set current_value($field) ""
        }
    }
    #the other widgets have a linked variable that automatically update it but the table not
    if { [info exists table] && [winfo exists $table] } {
      if {$data($varset,qorh) == 0} { ; # Caudal en basin outlet (Qout)
        GridData::SetData $table $current_value(table_value_Tq)
      } elseif {$data($varset,qorh) == 1} { ; # Caudal en stream gauge (Qin,surf)
        GridData::SetData $table $current_value(table_value_Ts)
      } else { ; # Water depth en gauge
        GridData::SetData $table $current_value(table_value_Th)
      }

    }

    if { [info exists Twei] && [winfo exists $Twei] } {
        GridData::SetData $Twei $current_value(table_wei)
    }
    #if { [info exists Tpar_conmulti] && [winfo exists $Tpar] } {
    #    GridData::SetData $Tpar $current_value(table_manning_con)
    #}
    set current_varset $varset
}

proc Pest::GetIndex { varset } {
    variable settings
    return [lsearch $settings $varset]
}

proc Pest::Exists { varset } {
    if { [Pest::GetIndex $varset] != -1 } {
        set exists 1
    } else {
        set exists 0
    }
    return $exists
}

proc Pest::GetPestAutomaticName { } {
    set basename [_ "varset"]
    set i 1
    set varset $basename-$i
    while { [Pest::Exists $varset] } {
        incr i
        set varset $basename-$i
    }
    return $varset
}

proc Pest::NewPest { cb } {
    variable settings
    variable data
    variable fields
    variable fields_defaults
    set varset [Pest::GetPestAutomaticName]

    if { [Pest::Exists $varset] } {
        #already exists
        return 1
    }
    foreach field $fields value $fields_defaults {
        set data($varset,$field) $value
    }
    lappend settings $varset
    if { [winfo exists $cb] } {
        $cb configure -values [Pest::Getsettings]
    }
    Pest::SetCurrentPest $varset
    return 0
}

proc Pest::DeletePest { cb } {
    variable settings
    variable data
    variable fields
    variable fields_defaults
    set varset [Pest::GetCurrentPest]
    if { ![Pest::Exists $varset] } {
        #not exists
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete varset '%s'" $varset] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {
        set i [Pest::GetIndex $varset]
        array unset data $varset,*
        set settings [lreplace $settings $i $i]
        if { [winfo exists $cb] } {
            $cb configure -values [Pest::Getsettings]
        }
        Pest::SetCurrentPest [lindex $settings 0]
        GiD_Redraw
    }
    return 0
}

proc Pest::RenamePest { cb } {
    variable data
    variable settings
    variable fields
    set Pest [Pest::GetCurrentPest]
    if { ![Pest::Exists $varset] } {
        #not exists
        return 1
    }
    set new_name [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $varset] \
            [= "Enter new name of %s '%s'" [= "varset"] $varset] gidquestionhead "any" ""]
    if { $new_name != "--CANCEL--" } {
        foreach field $fields {
            set data($new_name,$field) $data($varset,$field)
        }
        array unset data $varset,*
        set i [Pest::GetIndex $varset]
        lset settings $i $new_name
        if { [winfo exists $cb] } {
            $cb configure -values [Pest::Getsettings]
        }
        Pest::SetCurrentPest $new_name
        GiD_Redraw
    }
    return 0
}

# apply the values of the current window
proc Pest::ApplyWin1 { } {
    variable data
    variable current_value
    variable fields
    set varset [Pest::GetCurrentPest]
    foreach field $fields {
        set data($varset,$field) $current_value($field)
    }
    GiD_Redraw
}

proc Pest::ApplyWin2 { Tq Ts Th } {
    variable data
    variable current_value
    variable fields
    set varset [Pest::GetCurrentPest]
    foreach field $fields {
        set data($varset,$field) $current_value($field)
    }
    set data($varset,table_value_Tq) [GridData::GetDataAllItems $Tq]
    set data($varset,table_value_Ts) [GridData::GetDataAllItems $Ts]
    set data($varset,table_value_Th) [GridData::GetDataAllItems $Th]

    GiD_Redraw
}

proc Pest::ApplyWin3 { Twei } {
    variable data
    variable current_value
    variable fields
    set varset [Pest::GetCurrentPest]
    foreach field $fields {
        set data($varset,$field) $current_value($field)
    }
    set data($varset,table_wei) [GridData::GetDataAllItems $Twei]
    GiD_Redraw
}

proc Pest::ApplyWin4 { Tpar_conmulti Tpar_sinmulti } {
  variable data
  variable current_value
  variable fields

  set varset [Pest::GetCurrentPest]
  foreach field $fields {
      set data($varset,$field) $current_value($field)
  }

  set data($varset,table_manning_con) [GridData::GetDataAllItems $Tpar_conmulti]
  set data($varset,table_manning_sin) [GridData::GetDataAllItems $Tpar_sinmulti]
  GiD_Redraw
}

proc Pest::DestroyPestWindow { W w } {
    if { $W == $w } {
        Pest::FillProblemDataFromTclData
    }
}

proc Pest::UpdateManningWindow {w} {
    variable current_value
    variable Tpar_conmulti
    variable Tpar_sinmulti

    if {$current_value(withmulti) == 0 } { ; # Se utiliza multiplicador
      grid $w.lmanning_mul
      grid $w.emanning_mul
      grid $Tpar_conmulti -sticky nsew -columnspan 2
      grid $w.fbuttonsc.b1 $w.fbuttonsc.b2 -sticky w
      grid $w.fbuttonsc -sticky ew
      grid remove $Tpar_sinmulti
      grid remove $w.fbuttonss
    } else {
      grid remove $w.lmanning_mul
      grid remove $w.emanning_mul
      grid $Tpar_sinmulti -sticky nsew -columnspan 2
      grid $w.fbuttonss.b1 $w.fbuttonss.b2 -sticky w
      grid $w.fbuttonss -sticky ew
      grid remove $Tpar_conmulti
      grid remove $w.fbuttonsc
    }
}

proc Pest::UpdateObservationWindow {w} {
  variable current_value
  variable Tq
  variable Th
  variable Ts

  if {$current_value(qorh) == 0 } { ; # Qobs
      grid $Tq -sticky nsew -columnspan 2
      grid $w.fbuttonsq.b1 $w.fbuttonsq.b2 -sticky w
      grid $w.fbuttonsq -sticky ew
      grid remove $Th
      grid remove $Ts
      grid remove $w.fbuttonsh
      grid remove $w.fbuttonss
      grid remove $w.lgauge_num
      grid remove $w.egauge_num
  } elseif { $current_value(qorh) == 1 } { ; # Stream gauge
      grid $Ts -sticky nsew -columnspan 2
      grid $w.fbuttonss.b1 $w.fbuttonss.b2 -sticky w
      grid $w.fbuttonss -sticky ew
      grid $w.lgauge_num
      grid $w.egauge_num
      grid remove $Th
      grid remove $Tq
      grid remove $w.fbuttonsh
      grid remove $w.fbuttonsq
  } else {
      grid $Th -sticky nsew -columnspan 2
      grid $w.fbuttonsh.b1 $w.fbuttonsh.b2 -sticky w
      grid $w.fbuttonsh -sticky ew
      grid $w.lgauge_num
      grid $w.egauge_num
      grid remove $Tq
      grid remove $Ts
      grid remove $w.fbuttonsq
      grid remove $w.fbuttonss
  }
}

proc Pest::OnChangeModel { w } {
    variable current_value
    if { $current_value(model) == 1 } {
        # Curve Number (SCS)
        grid $w.lCN_mul $w.eCN_mul
        grid $w.lIa_mul $w.eIa_mul
        grid remove $w.lSuct_mul $w.eSuct_mul
        grid remove $w.lPor_mul $w.ePor_mul
        grid remove $w.lSat_mul $w.eSat_mul
        grid remove $w.lKs_mul $w.eKs_mul
        grid remove $w.lLoss_mul $w.eLoss_mul
        grid remove $w.lSoil_mul $w.eSoil_mul
    } elseif { $current_value(model) == 2 } {
        # Green&Ampt
        grid remove $w.lCN_mul $w.eCN_mul
        grid remove $w.lIa_mul $w.eIa_mul
        grid $w.lSuct_mul $w.eSuct_mul
        grid $w.lPor_mul $w.ePor_mul
        grid $w.lSat_mul $w.eSat_mul
        grid $w.lKs_mul $w.eKs_mul
        grid $w.lLoss_mul $w.eLoss_mul
        grid $w.lSoil_mul $w.eSoil_mul
    }
}
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# ##############################################################################
# Ventana de la interfaz gráfica de PEST
# ##############################################################################
proc Pest::Window { type } {
    variable data
    variable settings
    variable current_varset
    variable current_value
    variable Tpar_conmulti
    variable Tpar_sinmulti
    variable Twei
    variable Tq
    variable Ts
    variable Th
    variable varset

    if { ![info exists settings] } {
        Pest::SetDefaultValues
    }

    Pest::FillTclDataFromProblemData

    if { $type == "set1"} {
      set w .gid.varset
      InitWindow $w [= "PEST path"] PestWindowGeom [list Pest::Window $type]
      if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
      set f [ttk::frame $w.fsettings]

      ttk::combobox $f.cb1 -textvariable Pest::current_varset -values [Pest::Getsettings] -state readonly
      # Con estas lineas se genera un varset por defecto sin mostrar lo de generar varsets
      if { $settings == "" } {
        Pest::NewPest $f.cb1
      }
      # Con estas lineas aparecen los botones de generar los varsets a mano
      #bind $f.cb1 <<ComboboxSelected>> [list Pest::OnChangeSelectedPest %W]
      #ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list Pest::NewPest $f.cb1]
      #GidHelp $f.bnew  [= "Create a new varset"]
      #ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list Pest::DeletePest $f.cb1]
      #GidHelp $f.bdel  [= "Delete a varset"]
      #ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list Pest::RenamePest $f.cb1]
      #GidHelp $w.f.bren  [= "Rename a varset"]
      #grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
      #grid $f -sticky new
      #grid rowconfigure $f 0 -weight 1

      set f [ttk::frame $w.fvarset]

      foreach field {path_PEST} text [list [= "PEST exes"]] {
          ttk::label $f.l$field -text $text
          ttk::entry $f.e$field -textvariable Pest::current_value($field)
          ttk::button $f.b$field -text [= "Retrieve"] -command [list Pest::browseFolder $field]
          grid $f.l$field $f.e$field $f.b$field -sticky ew -pady 2
      }

      grid columnconfigure $f {0 1} -weight 1
      grid $f -sticky new -padx 2 -pady 2
      GidHelp $f.epath_PEST [= "Location of the PEST executables"]


      set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
      ttk::button $f.bapply -text [= "Apply"] -command [list Pest::ApplyWin1] -style BottomFrame.TButton
      ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
      #grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
      grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
      grid $f -sticky sew
      if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }

      grid columnconfigure $w 0 -weight 1
      grid rowconfigure $w 1 -weight 1
      bind $w <Alt-c> "$f.bclose invoke"
      bind $w <Escape> "$f.bclose invoke"
      bind $w <Destroy> [list +Pest::DestroyPestWindow %W $w] ;# + to add to previous script

    } elseif { $type == "set2"} {
      set w .gid.varset
      InitWindow $w [= "PEST Settings"] PestWindowGeom [list Pest::Window $type]
      if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
      set f [ttk::frame $w.fsettings]

      ttk::combobox $f.cb1 -textvariable Pest::current_varset -values [Pest::Getsettings] -state readonly
      # Con estas lineas se genera un varset por defecto sin mostrar lo de generar varsets
      if { $settings == "" } {
        Pest::NewPest $f.cb1
      }
      # Con estas lineas aparecen los botones de generar los varsets a mano
      #bind $f.cb1 <<ComboboxSelected>> [list Pest::OnChangeSelectedPest %W]
      #ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list Pest::NewPest $f.cb1]
      #GidHelp $f.bnew  [= "Create a new varset"]
      #ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list Pest::DeletePest $f.cb1]
      #GidHelp $f.bdel  [= "Delete a varset"]
      #ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list Pest::RenamePest $f.cb1]
      #GidHelp $w.f.bren  [= "Rename a varset"]
      #grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
      #grid $f -sticky new
      #grid rowconfigure $f 0 -weight 1

      set f [ttk::frame $w.fvarset]

      ttk::labelframe $w.fvariables -text [= "PEST parameters"]
      #foreach field {start} text [list [= "Rstfle"]] values {{1 2}} labels [list [list [= "Restart"] [= "Norestart"]]] {
      #    ttk::label $w.fvariables.l$field -text $text
      #    # ttk::combobox $w.fvariables.c$field -values $values -state readonly -textvariable Pest::current_value($field)
      #    TTKComboBox $w.fvariables.c$field -values $values -labels $labels -state readonly -textvariable Pest::current_value($field)
      #    grid $w.fvariables.l$field $w.fvariables.c$field -sticky ew
      #}
      #foreach field {end} text [list [= "Pestmode"]] values {{1 2}} labels [list [list [= "Estimation"] [= "Regularisation"]]] {
      #    ttk::label $w.fvariables.l$field -text $text
      #    # ttk::combobox $w.fvariables.c$field -values $values -state readonly -textvariable Pest::current_value($field)
      #    TTKComboBox $w.fvariables.c$field -values $values -labels $labels -state readonly -textvariable Pest::current_value($field)
      #    grid $w.fvariables.l$field $w.fvariables.c$field -sticky ew
      #}
      foreach field {noptmax} text [list [= "NOPTMAX"]] {
          ttk::label $w.fvariables.l$field -text $text
          ttk::entry $w.fvariables.e$field -textvariable Pest::current_value($field)
          grid $w.fvariables.l$field $w.fvariables.e$field -sticky ew
      }
      foreach field {phiredstp} text [list [= "PHIREDSTP"]] {
          ttk::label $w.fvariables.l$field -text $text
          ttk::entry $w.fvariables.e$field -textvariable Pest::current_value($field)
          grid $w.fvariables.l$field $w.fvariables.e$field -sticky ew
      }
      foreach field {nphistp} text [list [= "NPHISTP"]] {
          ttk::label $w.fvariables.l$field -text $text
          ttk::entry $w.fvariables.e$field -textvariable Pest::current_value($field)
          grid $w.fvariables.l$field $w.fvariables.e$field -sticky ew
      }
      foreach field {nphinored} text [list [= "NPHINORED"]] {
          ttk::label $w.fvariables.l$field -text $text
          ttk::entry $w.fvariables.e$field -textvariable Pest::current_value($field)
          grid $w.fvariables.l$field $w.fvariables.e$field -sticky ew
      }
      foreach field {relparstp} text [list [= "RELPARSTP"]] {
          ttk::label $w.fvariables.l$field -text $text
          ttk::entry $w.fvariables.e$field -textvariable Pest::current_value($field)
          grid $w.fvariables.l$field $w.fvariables.e$field -sticky ew
      }
      foreach field {nrelpar} text [list [= "NRELPAR"]] {
          ttk::label $w.fvariables.l$field -text $text
          ttk::entry $w.fvariables.e$field -textvariable Pest::current_value($field)
          grid $w.fvariables.l$field $w.fvariables.e$field -sticky ew
      }
      foreach field {phiratsuf} text [list [= "PHIRATSUF"]] {
          ttk::label $w.fvariables.l$field -text $text
          ttk::entry $w.fvariables.e$field -textvariable Pest::current_value($field)
          grid $w.fvariables.l$field $w.fvariables.e$field -sticky ew
      }
      foreach field {phiredlam} text [list [= "PHIREDLAM"]] {
          ttk::label $w.fvariables.l$field -text $text
          ttk::entry $w.fvariables.e$field -textvariable Pest::current_value($field)
          grid $w.fvariables.l$field $w.fvariables.e$field -sticky ew
      }
      foreach field {numlam} text [list [= "NUMLAM"]] {
          ttk::label $w.fvariables.l$field -text $text
          ttk::entry $w.fvariables.e$field -textvariable Pest::current_value($field)
          grid $w.fvariables.l$field $w.fvariables.e$field -sticky ew
      }
      grid columnconfigure $w.fvariables {0 1} -weight 1
      grid $w.fvariables -sticky new -padx 2 -pady 2
      GidHelp $w.fvariables.enoptmax [= "Set maximum number of iterations"]



      set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
      ttk::button $f.bapply -text [= "Apply"] -command [list Pest::ApplyWin1] -style BottomFrame.TButton
      ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
      #grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
      grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
      grid $f -sticky sew
      if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }

      grid columnconfigure $w 0 -weight 1
      grid rowconfigure $w 1 -weight 1
      bind $w <Alt-c> "$f.bclose invoke"
      bind $w <Escape> "$f.bclose invoke"
      bind $w <Destroy> [list +Pest::DestroyPestWindow %W $w] ;# + to add to previous script

    } elseif { $type == "par"} {
      set w .gid.varset
      InitWindow $w [= "Parameters to calibrate"] PestWindowGeom [list Pest::Window $type]
      if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
      set f [ttk::frame $w.fsettings]
      ttk::combobox $f.cb1 -textvariable Pest::current_varset -values [Pest::Getsettings] -state readonly
      # Con estas lineas se genera un varset por defecto sin mostrar lo de generar varsets
      if { $settings == "" } {
        Pest::NewPest $f.cb1
      }
      # Con estas lineas aparecen los botones de generar los varsets a mano
      #bind $f.cb1 <<ComboboxSelected>> [list Pest::OnChangeSelectedPest %W]
      #ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list Pest::NewPest $f.cb1]
      #GidHelp $f.bnew  [= "Create a new varset"]
      #ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list Pest::DeletePest $f.cb1]
      #GidHelp $f.bdel  [= "Delete a varset"]
      #ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list Pest::RenamePest $f.cb1]
      #GidHelp $w.f.bren  [= "Rename a varset"]
      #grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
      #grid $f -sticky new
      #grid rowconfigure $f 0 -weight 1

      ttk::labelframe $w.fmanning -text [= "Roughness"]
      set field visible2
      ttk::checkbutton $w.fmanning.ch$field -text [= "Calibrate"] -variable Pest::current_value($field)
      grid $w.fmanning.ch$field -sticky w
      set field withmulti
    	ttk::radiobutton $w.fmanning.fwithmulti0 -text [= "With multiplier"] -variable Pest::current_value($field) -value 0 \
        -command [list Pest::UpdateManningWindow $w.fmanning]
      grid $w.fmanning.fwithmulti0 -sticky w
    	ttk::radiobutton $w.fmanning.fwithmulti1 -text [= "With limits"] -variable Pest::current_value($field) -value 1 \
        -command [list Pest::UpdateManningWindow $w.fmanning]
      grid $w.fmanning.fwithmulti1 -sticky w
      package require fulltktree
      foreach field {manning_mul} text [list [= "Manning multiplier"]] {
          ttk::label $w.fmanning.l$field -text $text
          ttk::entry $w.fmanning.e$field -textvariable Pest::current_value($field)
          grid $w.fmanning.l$field $w.fmanning.e$field -sticky ew
          GidHelp $w.fmanning.emanning_mul [= "Initial, minimum and maximum values"]
      }

      # Para que se abra de forma correcta si ya tenemos guardados valores
      if {$current_value(withmulti) == 0 } {
        grid $w.fmanning.lmanning_mul
        grid $w.fmanning.emanning_mul
      } else {
        grid remove $w.fmanning.lmanning_mul
        grid remove $w.fmanning.emanning_mul
      }

      # Tabla para cuando sí hay multiplicador
      package require fulltktree
      set columns ""
      foreach name [list [= "Value to calibrate"] ] {
        lappend columns [list 4 $name left text 1]
      }
      set Tpar_conmulti  [fulltktree $w.fmanning.t4 -columns $columns -showlines 0 -showheader 1 -expand 1]
      $Tpar_conmulti configure -editbeginhandler GridData::EditBegin
      $Tpar_conmulti configure -editaccepthandler [list GridData::EditAccept]
      $Tpar_conmulti configure -deletehandler GridData::RemoveRows
      $Tpar_conmulti configure -contextualhandler_menu GridData::ContextualMenu
      $Tpar_conmulti column configure all -button 0
      ttk::frame $w.fmanning.fbuttonsc
      ttk::button $w.fmanning.fbuttonsc.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $Tpar_conmulti 1]
      GidHelp $w.fmanning.fbuttonsc.b1 [_ "Pressing this button, one new line will be added to this window"]
      ttk::button $w.fmanning.fbuttonsc.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $Tpar_conmulti 1]
      GidHelp $w.fmanning.fbuttonsc.b2 [_ "Pressing this button, one line is deleted from this window"]
      GridData::SetData $Tpar_conmulti $current_value(table_manning_con)
      grid columnconfigure $w.fmanning {0 1} -weight 1
      grid $w.fmanning -sticky new -padx 2 -pady 2
      bind $Tpar_conmulti <<Paste>> [list ::GridData::Paste $Tpar_conmulti CLIPBOARD]
      bind $Tpar_conmulti <<PasteSelection>> [list ::GridData::Paste $Tpar_conmulti PRIMARY]
      bind $Tpar_conmulti <<Cut>> [list GridData::Cut $Tpar_conmulti]
      bind $Tpar_conmulti <<Copy>> [list GridData::Copy $Tpar_conmulti]
      bind [$Tpar_conmulti givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header


      # Tabla para cuando no hay multiplicador
      package require fulltktree
      set columns ""
      foreach name [list [= "Value to calibrate"] [= "lower limit"] [= "upper limit"]] {
        lappend columns [list 4 $name left text 1]
      }
      set Tpar_sinmulti [fulltktree $w.fmanning.t5 -columns $columns -showlines 0 -showheader 1 -expand 1]
      $Tpar_sinmulti configure -editbeginhandler GridData::EditBegin
      $Tpar_sinmulti configure -editaccepthandler [list GridData::EditAccept]
      $Tpar_sinmulti configure -deletehandler GridData::RemoveRows
      $Tpar_sinmulti configure -contextualhandler_menu GridData::ContextualMenu
      $Tpar_sinmulti column configure all -button 0
      ttk::frame $w.fmanning.fbuttonss
      ttk::button $w.fmanning.fbuttonss.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $Tpar_sinmulti 1]
      GidHelp $w.fmanning.fbuttonss.b1 [_ "Pressing this button, one new line will be added to this window"]
      ttk::button $w.fmanning.fbuttonss.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $Tpar_sinmulti 1]
      GidHelp $w.fmanning.fbuttonss.b2 [_ "Pressing this button, one line is deleted from this window"]
      GridData::SetData $Tpar_sinmulti $current_value(table_manning_sin)
      grid columnconfigure $w.fmanning {0 1} -weight 1
      grid $w.fmanning -sticky new -padx 2 -pady 2
      bind $Tpar_sinmulti <<Paste>> [list ::GridData::Paste $Tpar_sinmulti CLIPBOARD]
      bind $Tpar_sinmulti <<PasteSelection>> [list ::GridData::Paste $Tpar_sinmulti PRIMARY]
      bind $Tpar_sinmulti <<Cut>> [list GridData::Cut $Tpar_sinmulti]
      bind $Tpar_sinmulti <<Copy>> [list GridData::Copy $Tpar_sinmulti]
      bind [$Tpar_sinmulti givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header

      # Para que se abra de forma correcta si ya tenemos guardados valores
      if {$current_value(withmulti) == 0 } {
        grid $Tpar_conmulti -sticky nsew -columnspan 2
        grid $w.fmanning.fbuttonsc.b1 $w.fmanning.fbuttonsc.b2 -sticky w
        grid $w.fmanning.fbuttonsc -sticky ew
        grid remove $Tpar_sinmulti
        grid remove $w.fmanning.fbuttonss
      } else {
        grid $Tpar_sinmulti -sticky nsew -columnspan 2
        grid $w.fmanning.fbuttonss.b1 $w.fmanning.fbuttonss.b2 -sticky w
        grid $w.fmanning.fbuttonss -sticky ew
        grid remove $Tpar_conmulti
        grid remove $w.fmanning.fbuttonsc
      }

      ttk::labelframe $w.fparinit -text [= "Hydrological parameters"]
      set field visible1
      ttk::checkbutton $w.fparinit.ch$field -text [= "Calibrate"] -variable Pest::current_value($field)
      grid $w.fparinit.ch$field -sticky w
      foreach field {model} text [list [= "Model"]] values {{1 2}} labels [list [list [= "SCS"] [= "Green&Ampt"]]] {
          ttk::label $w.fparinit.l$field -text $text
          #ttk::combobox $f.c$field -values $values -state readonly -textvariable Pest::current_value($field)
          TTKComboBox $w.fparinit.c$field -values $values -labels $labels -state readonly -textvariable Pest::current_value($field) \
              -modifycmd [list Pest::OnChangeModel $w.fparinit]
          grid $w.fparinit.l$field $w.fparinit.c$field -sticky ew
      }
      foreach field {CN_mul Ia_mul Suct_mul Por_mul Sat_mul Ks_mul Loss_mul Soil_mul} text [list [= "CN mutiplier"] [= "Ia coefficient"] [= "Suction mutiplier"] [= "Total porosity mutip."] [= "Init. saturation mutip."] [= "Ks mutiplier"] [= "Initial loss mutip."] [= "Soil depth mutiplier"]] {
          ttk::label $w.fparinit.l$field -text $text
          ttk::entry $w.fparinit.e$field -textvariable Pest::current_value($field)
          grid $w.fparinit.l$field $w.fparinit.e$field -sticky ew
          GidHelp $w.fparinit.eCN_mul  [= "Initial, minimum and maximum values"]
          GidHelp $w.fparinit.eIa_mul  [= "Initial, minimum and maximum values"]
          GidHelp $w.fparinit.eSuct_mul  [= "Initial, minimum and maximum values"]
          GidHelp $w.fparinit.ePor_mul  [= "Initial, minimum and maximum values"]
          GidHelp $w.fparinit.eSat_mul  [= "Initial, minimum and maximum values"]
          GidHelp $w.fparinit.eKs_mul  [= "Initial, minimum and maximum values"]
          GidHelp $w.fparinit.eLoss_mul  [= "Initial, minimum and maximum values"]
          GidHelp $w.fparinit.eSoil_mul  [= "Initial, minimum and maximum values"]
      }

      # Para que se abra de forma correcta si ya tenemos guardados valores
      if { $current_value(model) == 1 } { ; # Curve Number (SCS)
          grid $w.fparinit.lCN_mul $w.fparinit.eCN_mul
          grid $w.fparinit.lIa_mul $w.fparinit.eIa_mul
          grid remove $w.fparinit.lSuct_mul $w.fparinit.eSuct_mul
          grid remove $w.fparinit.lPor_mul $w.fparinit.ePor_mul
          grid remove $w.fparinit.lSat_mul $w.fparinit.eSat_mul
          grid remove $w.fparinit.lKs_mul $w.fparinit.eKs_mul
          grid remove $w.fparinit.lLoss_mul $w.fparinit.eLoss_mul
          grid remove $w.fparinit.lSoil_mul $w.fparinit.eSoil_mul
      } elseif { $current_value(model) == 2 } { ; # Green&Ampt
          grid remove $w.fparinit.lCN_mul $w.fparinit.eCN_mul
          grid remove $w.fparinit.lIa_mul $w.fparinit.eIa_mul
          grid $w.fparinit.lSuct_mul $w.fparinit.eSuct_mul
          grid $w.fparinit.lPor_mul $w.fparinit.ePor_mul
          grid $w.fparinit.lSat_mul $w.fparinit.eSat_mul
          grid $w.fparinit.lKs_mul $w.fparinit.eKs_mul
          grid $w.fparinit.lLoss_mul $w.fparinit.eLoss_mul
          grid $w.fparinit.lSoil_mul $w.fparinit.eSoil_mul
      }

      grid columnconfigure $w.fparinit {0 1} -weight 1
      grid $w.fparinit -sticky new -padx 2 -pady 2

      set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
      ttk::button $f.bapply -text [= "Apply"] -command [list Pest::ApplyWin4 $Tpar_conmulti $Tpar_sinmulti] -style BottomFrame.TButton
      ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
      #grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
      grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
      grid $f -sticky sew
      if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }

      grid columnconfigure $w 0 -weight 1
      grid rowconfigure $w 1 -weight 1
      bind $w <Alt-c> "$f.bclose invoke"
      bind $w <Escape> "$f.bclose invoke"
      bind $w <Destroy> [list +Pest::DestroyPestWindow %W $w] ;# + to add to previous script

    } elseif { $type == "output"} {
      set w .gid.varset
      InitWindow $w [= "Observation data"] PestWindowGeom [list Pest::Window $type]
      if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
      set f [ttk::frame $w.fvarset]
      grid $f -sticky new
      grid rowconfigure $f 0 -weight 1

      ttk::combobox $f.cb1 -textvariable Pest::current_varset -values [Pest::Getsettings] -state readonly
      # Con estas lineas se genera un varset por defecto sin mostrar lo de generar varsets
      if { $settings == "" } {
        Pest::NewPest $f.cb1
      }
      # Con estas lineas aparecen los botones de generar los varsets a mano
      #bind $f.cb1 <<ComboboxSelected>> [list Pest::OnChangeSelectedPest %W]
      #ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list Pest::NewPest $f.cb1]
      #GidHelp $f.bnew  [= "Create a new varset"]
      #ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list Pest::DeletePest $f.cb1]
      #GidHelp $f.bdel  [= "Delete a varset"]
      #ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list Pest::RenamePest $f.cb1]
      #GidHelp $f.bren  [= "Rename a varset"]
      #grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
      #grid $f -sticky new
      #grid rowconfigure $f 0 -weight 1

      ttk::labelframe $w.fsettings -text [= "Observations"]
      set field qorh
    	ttk::radiobutton $w.fsettings.fqorhq -text [= "Discharge at outlet"] -variable Pest::current_value($field) -value 0 \
        -command [list Pest::UpdateObservationWindow $w.fsettings]
      grid $w.fsettings.fqorhq -sticky w
    	ttk::radiobutton $w.fsettings.fqorhs -text [= "Discharge in stream gauge"] -variable Pest::current_value($field) -value 1 \
        -command [list Pest::UpdateObservationWindow $w.fsettings]
      grid $w.fsettings.fqorhs -sticky w
      ttk::radiobutton $w.fsettings.fqorhh -text [= "Water depht in gauge"] -variable Pest::current_value($field) -value 2 \
        -command [list Pest::UpdateObservationWindow $w.fsettings]
    	grid $w.fsettings.fqorhh -sticky w

      foreach field {first} text [list [= "Initial timestep"]] {
          ttk::label $w.fsettings.l$field -text $text
          ttk::entry $w.fsettings.e$field -textvariable Pest::current_value($field)
          grid $w.fsettings.l$field $w.fsettings.e$field -sticky ew
      }
      grid $f -sticky new
      grid columnconfigure $w.fsettings {1} -weight 1

      foreach field {gauge_num} text [list [= "Gauge number"]] {
          ttk::label $w.fsettings.l$field -text $text
          ttk::entry $w.fsettings.e$field -textvariable Pest::current_value($field)
          grid $w.fsettings.l$field $w.fsettings.e$field -sticky ew
          GidHelp $w.fsettings.egauge_num [= "Gauge number"]
      }

      # Tabla para cuando sea Qobs (caudal) en el basin outlet (Qout)
      package require fulltktree
      set columns ""
      foreach name [list [= "Time (s)"] [= "Q obs (m3/s)"] [= "Weight"]] {
        lappend columns [list 4 $name left text 1]
      }
      set Tq [fulltktree $w.fsettings.t1 -columns $columns -showlines 0 -showheader 1 -expand 1]
      $Tq configure -editbeginhandler GridData::EditBegin
      $Tq configure -editaccepthandler [list GridData::EditAccept]
      $Tq configure -deletehandler GridData::RemoveRows
      $Tq configure -contextualhandler_menu GridData::ContextualMenu
      $Tq column configure all -button 0
      ttk::frame $w.fsettings.fbuttonsq
      ttk::button $w.fsettings.fbuttonsq.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $Tq 1]
      GidHelp $w.fsettings.fbuttonsq.b1 [_ "Pressing this button, one new line will be added to this window"]
      ttk::button $w.fsettings.fbuttonsq.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $Tq 1]
      GidHelp $w.fsettings.fbuttonsq.b2 [_ "Pressing this button, one line is deleted from this window"]
      grid $Tq -sticky nsew -columnspan 2
      grid $w.fsettings.fbuttonsq.b1 $w.fsettings.fbuttonsq.b2 -sticky w
      grid $w.fsettings.fbuttonsq -sticky ew
      GridData::SetData $Tq $current_value(table_value_Tq)
      grid columnconfigure $w.fsettings {0 1} -weight 1
      grid $w.fsettings  -sticky new -padx 2 -pady 2
      GidHelp $w.fsettings [= "Set observations"]
      bind $Tq <<Paste>> [list ::GridData::Paste $Tq CLIPBOARD]
      bind $Tq <<PasteSelection>> [list ::GridData::Paste $Tq PRIMARY]
      bind $Tq <<Cut>> [list GridData::Cut $Tq]
      bind $Tq <<Copy>> [list GridData::Copy $Tq]
      bind [$Tq givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header


      # Tabla para cuando sea Qobs (caudal) en el stream gauge (Qb)
      package require fulltktree
      set columns ""
      foreach name [list [= "Time (s)"] [= "Q obs (m3/s)"] [= "Weight"]] {
        lappend columns [list 4 $name left text 1]
      }
      set Ts [fulltktree $w.fsettings.t2 -columns $columns -showlines 0 -showheader 1 -expand 1]
      $Ts configure -editbeginhandler GridData::EditBegin
      $Ts configure -editaccepthandler [list GridData::EditAccept]
      $Ts configure -deletehandler GridData::RemoveRows
      $Ts configure -contextualhandler_menu GridData::ContextualMenu
      $Ts column configure all -button 0
      ttk::frame $w.fsettings.fbuttonss
      ttk::button $w.fsettings.fbuttonss.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $Ts 1]
      GidHelp $w.fsettings.fbuttonss.b1 [_ "Pressing this button, one new line will be added to this window"]
      ttk::button $w.fsettings.fbuttonss.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $Ts 1]
      GidHelp $w.fsettings.fbuttonss.b2 [_ "Pressing this button, one line is deleted from this window"]
      grid $Ts -sticky nsew -columnspan 2
      grid $w.fsettings.fbuttonss.b1 $w.fsettings.fbuttonss.b2 -sticky w
      grid $w.fsettings.fbuttonss -sticky ew
      GridData::SetData $Ts $current_value(table_value_Ts)
      grid columnconfigure $w.fsettings {0 1} -weight 1
      grid $w.fsettings  -sticky new -padx 2 -pady 2
      GidHelp $w.fsettings [= "Set observations"]
      bind $Ts <<Paste>> [list ::GridData::Paste $Ts CLIPBOARD]
      bind $Ts <<PasteSelection>> [list ::GridData::Paste $Ts PRIMARY]
      bind $Ts <<Cut>> [list GridData::Cut $Ts]
      bind $Ts <<Copy>> [list GridData::Copy $Ts]
      bind [$Ts givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header


      # Tabla para cuando sea h obs (calado) en gauge
      package require fulltktree
      set columns ""
      foreach name [list [= "Time (s)"] [= "h obs (m)"] [= "Weight"]] {
        lappend columns [list 4 $name left text 1]
      }
      set Th [fulltktree $w.fsettings.t3 -columns $columns -showlines 0 -showheader 1 -expand 1]
      $Th configure -editbeginhandler GridData::EditBegin
      $Th configure -editaccepthandler [list GridData::EditAccept]
      $Th configure -deletehandler GridData::RemoveRows
      $Th configure -contextualhandler_menu GridData::ContextualMenu
      $Th column configure all -button 0
      ttk::frame $w.fsettings.fbuttonsh
      ttk::button $w.fsettings.fbuttonsh.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $Th 1]
      GidHelp $w.fsettings.fbuttonsh.b1 [_ "Pressing this button, one new line will be added to this window"]
      ttk::button $w.fsettings.fbuttonsh.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $Th 1]
      GidHelp $w.fsettings.fbuttonsh.b2 [_ "Pressing this button, one line is deleted from this window"]
      grid $Th -sticky nsew -columnspan 2
      grid $w.fsettings.fbuttonsh.b1 $w.fsettings.fbuttonsh.b2 -sticky w
      grid $w.fsettings.fbuttonsh -sticky ew
      GridData::SetData $Th $current_value(table_value_Th)
      grid columnconfigure $w.fsettings {0 1} -weight 1
      grid $w.fsettings  -sticky new -padx 2 -pady 2
      GidHelp $w.fsettings [= "Set observations"]
      bind $Th <<Paste>> [list ::GridData::Paste $Th CLIPBOARD]
      bind $Th <<PasteSelection>> [list ::GridData::Paste $Th PRIMARY]
      bind $Th <<Cut>> [list GridData::Cut $Th]
      bind $Th <<Copy>> [list GridData::Copy $Th]
      bind [$Th givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header


      # Para que se abra de forma correcta si ya tenemos guardados valores
      if {$current_value(qorh) == 0 } { ; # Qobs en basin outlet
          grid $Tq -sticky nsew -columnspan 2
          grid $w.fsettings.fbuttonsq.b1 $w.fsettings.fbuttonsq.b2 -sticky w
          grid $w.fsettings.fbuttonsq -sticky ew
          grid remove $Th
          grid remove $Ts
          grid remove $w.fsettings.fbuttonsh
          grid remove $w.fsettings.fbuttonss
          grid remove $w.fsettings.lgauge_num
          grid remove $w.fsettings.egauge_num
      } elseif {$current_value(qorh) == 1 } { ; # Qobs en stream gauge
          grid $Ts -sticky nsew -columnspan 2
          grid $w.fsettings.fbuttonss.b1 $w.fsettings.fbuttonss.b2 -sticky w
          grid $w.fsettings.fbuttonss -sticky ew
          grid $w.fsettings.lgauge_num
          grid $w.fsettings.egauge_num
          grid remove $Tq
          grid remove $Th
          grid remove $w.fsettings.fbuttonsq
          grid remove $w.fsettings.fbuttonsh
      } else { ; # Water depth en gauge
          grid $Th -sticky nsew -columnspan 2
          grid $w.fsettings.fbuttonsh.b1 $w.fsettings.fbuttonsh.b2 -sticky w
          grid $w.fsettings.fbuttonsh -sticky ew
          grid $w.fsettings.lgauge_num
          grid $w.fsettings.egauge_num
          grid remove $Tq
          grid remove $Ts
          grid remove $w.fsettings.fbuttonsq
          grid remove $w.fsettings.fbuttonss
      }



      set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
      ttk::button $f.bapply -text [= "Apply"] -command [list Pest::ApplyWin2 $Tq $Ts $Th] -style BottomFrame.TButton
      ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
      grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
      grid $f -sticky sew
      if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }

      grid columnconfigure $w 0 -weight 1
      grid rowconfigure $w 1 -weight 1
      bind $w <Alt-c> "$f.bclose invoke"
      bind $w <Escape> "$f.bclose invoke"
      bind $w <Destroy> [list +Pest::DestroyPestWindow %W $w] ;# + to add to previous script


    } elseif { $type == "wei"} {
      set w .gid.varset
      InitWindow $w [= "Weight data"] PestWindowGeom [list Pest::Window $type]
      if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
      set f [ttk::frame $w.fsettings]

      ttk::combobox $f.cb1 -textvariable Pest::current_varset -values [Pest::Getsettings] -state readonly
      bind $f.cb1 <<ComboboxSelected>> [list Pest::OnChangeSelectedPest %W]
      ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list Pest::NewPest $f.cb1]
      GidHelp $f.bnew  [= "Create a new varset"]
      ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list Pest::DeletePest $f.cb1]
      GidHelp $f.bdel  [= "Delete a varset"]
      ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list Pest::RenamePest $f.cb1]
      GidHelp $w.f.bren  [= "Rename a varset"]
      grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
      grid $f -sticky new
      grid rowconfigure $f 0 -weight 1

      set f [ttk::frame $w.fvarset]

      package require fulltktree
      set columns ""
      foreach name [list [= "Weight value"]] {
        lappend columns [list 4 $name left text 1]
      }
      set Twei [fulltktree $f.t -columns $columns -showlines 0 -showheader 1 -expand 1]
      $Twei configure -editbeginhandler GridData::EditBegin
      $Twei configure -editaccepthandler [list GridData::EditAccept]
      $Twei configure -deletehandler GridData::RemoveRows
      $Twei configure -contextualhandler_menu GridData::ContextualMenu
      $Twei column configure all -button 0

      ttk::frame $f.fbuttons
      ttk::button $f.fbuttons.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $Twei 1]
      GidHelp $f.fbuttons.b1 [_ "Pressing this button, one new line will be added to this window"]
      ttk::button $f.fbuttons.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $Twei 1]
      GidHelp $f.fbuttons.b2 [_ "Pressing this button, one line is deleted from this window"]

      grid $Twei -sticky nsew -columnspan 2
      grid $f.fbuttons.b1 $f.fbuttons.b2 -sticky w
      grid $f.fbuttons -sticky ew
      grid rowconfigure $f {1 4} -weight 1
      grid $f -sticky nsew -padx 2 -pady 2
      GidHelp $f [= "Set hyetograph parameters"]

      bind $Twei <<Paste>> [list ::GridData::Paste $Twei CLIPBOARD]
      bind $Twei <<PasteSelection>> [list ::GridData::Paste $Twei PRIMARY]
      bind $Twei <<Cut>> [list GridData::Cut $Twei]
      bind $Twei <<Copy>> [list GridData::Copy $Twei]
      bind [$Twei givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header

      GridData::SetData $Twei $current_value(table_wei)

      set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
      ttk::button $f.bapply -text [= "Apply"] -command [list Pest::ApplyWin3 $Twei] -style BottomFrame.TButton
      ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
      grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
      grid $f -sticky sew
      if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }

      grid columnconfigure $w 0 -weight 1
      grid rowconfigure $w 1 -weight 1
      bind $w <Alt-c> "$f.bclose invoke"
      bind $w <Escape> "$f.bclose invoke"
      bind $w <Destroy> [list +Pest::DestroyPestWindow %W $w] ;# + to add to previous script
    } else {
        WarnWinText "Pest::Windows. Unexpected case $type"
        return 1
    }
}
