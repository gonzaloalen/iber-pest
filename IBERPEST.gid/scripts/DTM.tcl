#### create by Georgina 03/2003
#### modified by Ernest 10/2003 - multi DTM
#### modified by Geor 10/2003 - windows DTM import
### modified by Geor 12/03 - create DTM
### modified by Geor 05/2009
### updated by Marcos 09/2022 ###
###################################################################################

proc DTMWindow { ent } {
    global valor Num type
    
    set w .gid.dtmgrid
    set valor 1
    set Num 1
    set type $ent
       
    InitWindow $w "[= "Import DTM File as"] $type" PreDTMWindowGeom [list DTMWindow $ent]
    
    # Label 0
    ttk::labelframe $w.frmPresc -text [= " Grid Spacing "]
    ttk::radiobutton $w.frmPresc.1 -text [= " x1"] -variable valor -value 1 
    ttk::radiobutton $w.frmPresc.2 -text [= " x2"] -variable valor -value 2 
    ttk::radiobutton $w.frmPresc.5 -text [= " x5"] -variable valor -value 5
    ttk::radiobutton $w.frmPresc.10 -text [= " x10"] -variable valor -value 10 
    ttk::radiobutton $w.frmPresc.20 -text [= " x20"] -variable valor -value 20  
    ttk::radiobutton $w.frmPresc.50 -text [= " x50"] -variable valor -value 50
    
    grid $w.frmPresc.1 $w.frmPresc.2 $w.frmPresc.5 -sticky ew
    grid $w.frmPresc.10 $w.frmPresc.20 $w.frmPresc.50 -sticky ew
    grid columnconfigure $w.frmPresc {0 1 2} -weight 1
    grid $w.frmPresc -sticky new -padx 5 -pady 5
    
    # Label 1
    ttk::labelframe $w.frmNum -text [= " Options "]
    ttk::radiobutton $w.frmNum.one -text [= "One File"] -variable Num -value 1
    ttk::radiobutton $w.frmNum.mult -text [= "Multiple Files"] -variable Num -value 2
    
    grid $w.frmNum.one $w.frmNum.mult -sticky ew
    grid columnconfigure $w.frmNum {0 1 2} -weight 1
    grid $w.frmNum -sticky new -padx 5 -pady 5
    
    # Buttons settins
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.btnRTIN -text "Accept" -command "CallReadDTM $w $type" -underline 0 -width 10
    ttk::button $f.btnclose -text "Close" -command "destroy $w" -underline 0 -width 10
    grid $f.btnRTIN $f.btnclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center } 
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    
}



proc NewDTMCellsWindow { } {    
    global DTMPriv
    
    DTMfile
    
    if { $DTMPriv(file) == ""} {
	return
    }
    set Project [GiD_Info project ModelName]        
    
    if { $Project == "UNNAMED" } {
	tk_dialogRAM .gid.tmpwin error \
	    [= "Before create the Cell Surfaces, a project title is needed. Save project to get it"] \
	    error 0 OK
	return
    }
    set directory $Project.gid
    if { [file pathtype $directory] == "relative" } {
	set directory [file join [pwd] $directory]
    }
    
    # archivo de par�metros 
    set fdata [file join $directory reg_mesh.dat]
    set fd [open $fdata w]
    puts $fd $DTMPriv(file)      
    close $fd 
    
    set program [Iber::GetFullPathExe reg_mesh.exe]
    set f [open |\"$program\" r+]
    puts $f "\"$DTMPriv(fDir)\" \"$directory\""
    
    flush $f
    gets $f fin
    #close $f
    tk_dialogRAM .gid.tmpwin [= "Cell Surfaces Created"] \
	[= "New reg_mesh.dxf file was created. Now it will be imported."] \
	info 0 OK
    
    
    #para que importe directamente
    
    set filename [file join $directory REG_MESH.dxf]
    
    GiD_Process MEscape files DXFRead $filename escape
    
    set index [tk_dialogRAM .gid.tmpwin [= "CELLS Surface Imported"] \
	    [= "REG_MESH.dxf was imported. Do you want to collapse the geometry?"] question 0 [= "Yes"] [= "No"]]
    
    if {$index == 0} {
	GiD_Process Mescape Utilities Collapse Lines yes escape
    }
    
}

proc DTMfile { } {
    global DTMPriv    
    #use of tk_getOpenFile crash on Windows 8 x64!!
    #set types {        {{Text Files} {.txt} } {{Asc Files} {.asc} } {{All Files} * } }
    #set DTMPriv(fileD) [tk_getOpenFile -filetypes $types -title [= "Choose Original DTM file"]]
    set types [list [list [= "Text file"] ".txt .asc"] [list [_ "All files"] ".*"]]
    set defaultextension .txt
    if { ![info exists $DTMPriv(fileD)] } {
	set DTMPriv(fileD) ""
    }
    set DTMPriv(fileD) [Browser-ramR file read .gid [= "Choose Original DTM file"] $DTMPriv(fileD) $types $defaultextension 0]    
    
    set DTMPriv(file) [file tail $DTMPriv(fileD)]
    set DTMPriv(fDir) [file dirname $DTMPriv(fileD)]    
}


proc CreateDTMWin { } {
    global DTMPriv 
    
    set w .gid.createdtm
    
    set DTMPriv(dir) ""
    set DTMPriv(nom) "dtm"
    set DTMPriv(row) 300
    set DTMPriv(col) 300
    
    InitWindow $w [= "Create DTM Files"] PreCreateDTMWindowGeom CreateDTMWin
    
    
    set f [ttk::frame $w.frmDTMParams]
    # Label 0
    ttk::label $f.workdirtext -text [= "DTM Directory: "]
    ttk::entry $f.workdir -textvariable DTMPriv(dir)
    ttk::button $f.workdirret -text [= "Select"] -command DTMdir
    grid $f.workdirtext $f.workdir $f.workdirret -sticky w

    grid columnconfigure $w.frmDTMParams {0 1 2} -weight 1
    grid $w.frmDTMParams -sticky new -padx 2 -pady 2
    
    # Label 1
    ttk::labelframe $w.foptions -text [= " DTM options "]
    ttk::label $w.foptions.tolerancetext -text [= "Resultant Files Name "]
    ttk::entry $w.foptions.tolerancevalue -textvariable DTMPriv(nom)
    ttk::label $w.foptions.maxsidetext -text [= "Max. Number of Rows "]
    ttk::entry $w.foptions.maxsidevalue -textvariable DTMPriv(row)
    ttk::label $w.foptions.minsidetext -text [= "Max. Number of Columns "]
    ttk::entry $w.foptions.minsidevalue -textvariable DTMPriv(col)
    grid $w.foptions.tolerancetext $w.foptions.tolerancevalue -sticky ew
    grid $w.foptions.maxsidetext $w.foptions.maxsidevalue -sticky ew
    grid $w.foptions.minsidetext $w.foptions.minsidevalue -sticky ew
    grid columnconfigure $w.foptions {0 1 2} -weight 1
    grid $w.foptions -sticky new -padx 5 -pady 5
    
    # Label2
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.btnDTM -text "Accept" -command "CreateDTM $w" -underline 0 -width 10
    ttk::button $f.btnclose -text "Close" -command "destroy $w" -underline 0 -width 10
    grid $f.btnDTM $f.btnclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center } 
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    
}

proc DTMdir { } {
    global DTMPriv
    set DTMPriv(dir) [tk_chooseDirectory -title [= "Choose DTM's directory"]]
}

proc CreateDTM {w } {
    global DTMPriv
    
    set program [Iber::GetFullPathExe DTM_maker.exe]
    set f [open |\"$program\" r+]
    
    if { $f != "" } {
	#warning: the progam require in Windows separator \, no valid / and between "" if there are spaces
	set output_dir \"[file native $DTMPriv(dir)]\"        
	foreach value [list $output_dir $DTMPriv(nom) $DTMPriv(row) $DTMPriv(col)] {            
	    if { [string compare $value ""] == 0} {
		tk_dialogRAM .gid.tmpwin error [= "Some entry(ies) are empty, please fill them"] error 0 OK
		close $f
		return
	    } else {
		puts $f $value
	    }
	}     
	close $f
	tk_dialogRAM .gid.tmpwin [= "DTM Created"] [= "New DTM files was succesfully created"] info 0 OK
    }
    
    destroy $w
}

proc DTMGrid { } {
    global njumpx njumpy valor
    
    set njumpx [expr int(1)]
    set njumpy [expr int(1)]
    
    if {$valor == 1 } {
	set njumpx [expr int(1)]
	set njumpy [expr int(1)]
    } elseif {$valor == 2 } {
	set njumpx [expr int(2)]
	set njumpy [expr int(2)]
    } elseif {$valor == 5 } {
	set njumpx [expr int(5)]
	set njumpy [expr int(5)]
    } elseif {$valor == 10 } {
	set njumpx [expr int(10)]
	set njumpy [expr int(10)]
    } elseif {$valor == 20 } {
	set njumpx [expr int(20)]
	set njumpy [expr int(20)]
    } elseif {$valor == 50 } {
	set njumpx [expr int(50)]
	set njumpy [expr int(50)]
    }
}

proc CallReadDTM {w ent} {
    global GidPriv  
    global njumpx njumpy valor Num     
    
    DTMGrid 
    if {$Num == 1} {
	set filename [Browser-ramR file read .gid [= "Read DTM file"] \
		{} {{{DTM grid} {.txt }} {{All files} {.*}}} ]
	if {$filename == ""} {
	    return
	}
	ReadDTM $w $filename $ent
	
    } elseif {$Num == 2} {
	set filesfile [Browser-ramR file read .gid [= "Read DTM file"] \
		{} {{{DTM grid} {.dat}} {{All files} {.*}}} ]
	if {$filesfile == ""} {
	    return
	}
	set fins [open $filesfile r]
	gets $fins bb
	set nn $bb
	for {set ii 1} {$ii <= $bb} {incr ii} { 
	    gets $fins aa 
	    set filename $aa
	    ReadDTM $w $filename $ent
	}
	close $fins
    }
    GiD_Process Mescape Utilities Collapse Model yes escape
    GiD_Process Mescape View Zoom Frame escape    
}

proc ReadDTM {w filename ent } {
    global GidPriv  
    global njumpx njumpy 
    
    #no se si es valido asumir que el fichero siemple cumple con este formato
    #habria que tener la especificacion de lo que es valido, asumo siempre:
    #NCOLS       <n�entero> - n� de columnas
    #NROWS       <n�entero> - n� de filas 
    #XLLCENTER   <n�real>   - coord x del v�rtice inferior izquierdo
    #YLLCENTER   <n�real>   - coord y del v�rtice inferior izquierdo
    #CELLSIZE    <n�real>   - paso
    #NODATA_VALUE <n�real>  - valor que indica la ausencia de datos
    #cada fila representa puntos con igual Y
    #cada columna representa puntos con igual X
    #<n�real Z 0> <n�real Z 2> ... <n�real Z NCOLS-1> 
    # ...
    #<n�real Z (NROWS-1)*NCOLS+0> ... <n�real ZNCOLS*NROWS-1>
    
    destroy $w
    
    set fin [open $filename r]
    gets $fin aa
    set DTM(ncols) [lindex $aa 1]
    gets $fin aa
    set DTM(nrows) [lindex $aa 1]
    gets $fin aa
    set DTM(xllcenter) [lindex $aa 1]
    gets $fin aa
    set DTM(yllcenter) [lindex $aa 1]
    gets $fin aa
    set DTM(cellsize) [lindex $aa 1]
    gets $fin aa
    set DTM(nondatavalue) [lindex $aa 1]
    
    set OldCreateAlwaysNewPoint [GiD_Set CreateAlwaysNewPoint]
    GidUtils::DisableGraphics
    GidUtils::DisableWarnLine
    GidUtils::WaitState .gid
    
    set GidPriv(StopLoading) 0
    AdvanceBar 0 1 0 [= "Reading DTM"]  [= "Percentage"]:
    
    # cambios para que se dibuje en las coordenadas reales
    set xini [expr ($DTM(xllcenter))]                                
    set xfin [expr ($DTM(xllcenter)+(($DTM(ncols)-1)*$DTM(cellsize)))]        
    set yini [expr ($DTM(yllcenter))]                                
    set yfin [expr ($DTM(yllcenter)+(($DTM(nrows)-1)*$DTM(cellsize)))]            
    
    set pasoi [expr ($DTM(cellsize)*-1)]
    set ikont 0
    # he afegit un igual:
    set contlocaly 0
    set i [expr int(0)]
    for {set yi $yfin} {$yi >= $yini} {set yi [expr {$yi+$pasoi}]} {                      
	incr i          
	incr ikont
	set percent [expr int($ikont/($DTM(nrows)-1.0)*100)]
	AdvanceBar $percent 100 $percent
	if { $GidPriv(StopLoading) } break
	
	
	gets $fin aa        
	#not jump the first and last curves
	if { $i != [expr int(1)] &&  $i != [expr int($DTM(nrows))] } {                                
	    incr contlocaly
	    #jump njumpy internal curves
	    if { $contlocaly < $njumpy } continue
	}
	set contlocaly 0
	
	set y $yi    
	
	if { $ent == [= "Points"] } {
	    GiD_Process Mescape Geometry Create Point
	} else {
	    GiD_Process Mescape Geometry Create Nurbsline
	}
	
	set pasoj [expr ($DTM(cellsize))]
	set jkont 0
	#he afegit un igual:
	
	set j [expr int(0)]
	for {set xj $xini} {$xj <= $xfin} {set xj [expr {$xj+$pasoj}]} {
	    incr j 
	    
	    #he canviat la seg�ent l�nia de lloc:                
	    set z [lindex $aa $jkont]
	    
	    incr jkont
	    #not jump the first and last points
	    if { $j != [expr int(1)] &&  $j != [expr int($DTM(ncols))] } {
		incr contlocalx
		#jump njumpx internal points
		if { $contlocalx < $njumpx } continue
	    }
	    set contlocalx 0
	    
	    if { $z == $DTM(nondatavalue) } continue
	    set x $xj
	    GiD_Process $x,$y,$z
	}
	GiD_Process escape
    }
    #         ::GidUtils::EnableGraphics
    #         ::GidUtils::EnableWarnLine
    #         ::GidUtils::EndWaitState .gid
    
    if { $GidPriv(StopLoading) } {
	GiD_Process Mescape
	GidUtils::EnableGraphics
	GidUtils::EnableWarnLine
	GidUtils::EndWaitState .gid
	GidUtils::SetWarnLine [_ "Stop at user demand"]
	WarnWin [_ "Stop at user demand"]
    } elseif { $ent == [= "Points"] } {
	GiD_Set CreateAlwaysNewPoint 1
	GiD_Process Mescape View Zoom Frame escape
	GidUtils::EnableGraphics
	GidUtils::EnableWarnLine
	GidUtils::EndWaitState .gid
	GidUtils::SetWarnLine [_ "File '%s' Read" $filename]
    } elseif {$ent == [= "Lines"] } {
	GiD_Set CreateAlwaysNewPoint 1
	GiD_Process Mescape Geometry Delete Point 1: escape
	GiD_Process Mescape View Zoom Frame escape
	GidUtils::EnableGraphics
	GidUtils::EnableWarnLine
	GidUtils::EndWaitState .gid
	GidUtils::SetWarnLine [_ "File '%s' Read" $filename]
    } elseif {$ent == [= "Surface"] } {
	GiD_Set CreateAlwaysNewPoint 1
	GiD_Process Mescape Geometry Create NurbsSurface ParallelLines SelWindow AddToSelection filter:HigherEntity=0 1:end escape
	destroy .gid.filter_win
	GiD_Process Mescape Geometry Delete Line 1: escape
	GiD_Process Mescape Geometry Delete Point 1: escape
	GidUtils::EnableGraphics
	GiD_Process Mescape View Zoom Frame escape
	GidUtils::EnableWarnLine
	GidUtils::EndWaitState .gid
	GidUtils::SetWarnLine [_ "File '%s' Read" $filename]
    }
    AdvanceBar 100 100 100
    close $fin    
}


proc NewCallReadDTMCells { } {
    global DTMPriv
    #use of tk_getOpenFile crash on Windows 8 x64!!
    #set types { { {Cell Files} {.dxf} } { {All Files} * } }
    #set DTMPriv(rcell) [tk_getOpenFile -filetypes $types -title [= "Choose Cells Surfaces file"]]
    set types [list [list [= "Cell file"] ".dxf"] [list [_ "All files"] ".*"]]
    set defaultextension .dxf
    if { ![info exists $DTMPriv(rcell)] } {
	set DTMPriv(rcell) ""
    }
    set DTMPriv(rcell) [Browser-ramR file read .gid [= "Choose Cells Surfaces file"] $DTMPriv(rcell) $types $defaultextension 0]    
    if { $DTMPriv(rcell) != "" } {
	GiD_Process MEscape files DXFRead $DTMPriv(rcell) escape      
	set index [tk_dialogRAM .gid.tmpwin [= "Surfaces Imported"] \
		[= "dxf file was imported. Do you want to collapse the geometry?"] question 0 [= "Yes"] [= "No"]]        
	if {$index == 0} {
	    GiD_Process Mescape Utilities Collapse Lines Yes escape
	} else {
	    return
	}
    }    
}




##################################################################
#cosas en desuso
##################################################################

proc DTMCellsWindow { } {
    global valor Num
    
    set w .gid.dtmgrid
    set valor 1
    set Num 1
    # ventana principal
    InitWindow $w [= "Import DTM as Cells Surface"] PreDTMCellsWindowGeom DTMCellsWindow
    
    # creaci�n de los widgets
    frame $w.frmPresc -bd 2 -relief groove
    radiobutton $w.frmPresc.1 -text [= "Grid Spacing x 1"] -variable valor -value "1"  
    
    frame $w.frmNum -bd 2 -relief groove
    radiobutton $w.frmNum.one -text [= "One File"] -variable Num -value 1
    radiobutton $w.frmNum.mult -text [= "Multiple Files"] -variable Num -value 2
    
    set def_back [$w cget -background]
    frame $w.frmButt -bg [CCColorActivo $def_back] 
    button $w.frmButt.apply -text [= "Apply"] -command "CallReadDTMCells $w" -width 6
    button $w.frmButt.close -text [= "Close"] -command "destroy $w" -width 6
    
    # empaquetamiento widgets
    grid $w.frmPresc -padx 5 -pady 5 -sticky ew
    grid $w.frmPresc.1 -sticky w
    
    grid $w.frmNum -padx 5 -pady 5 -sticky ews
    grid $w.frmNum.one $w.frmNum.mult -padx 5 -pady 5 
    
    grid $w.frmButt -sticky news -columnspan 5 
    grid $w.frmButt.apply $w.frmButt.close -padx 5 -pady 6
    
    grid columnconf $w "0" -weight 1
    grid rowconfigure $w "0" -weight 1
}


proc CallReadDTMCells {w} {
    global GidPriv  
    global njumpx njumpy valor Num
    
    DTMGrid 
    if {$Num == 1} {
	set filename [Browser-ramR file read .gid [= "Read DTM file"] \
		{} {{{DTM grid} {.txt }} {{All files} {.*}}} ]
	if {$filename == ""} {
	    return
	}
	ReadDTMCells $w $filename
	
    } elseif {$Num == 2} {
	set filesfile [Browser-ramR file read .gid [= "Read DTM file"] \
		{} {{{DTM grid} {.dat}} {{All files} {.*}}} ]
	if {$filesfile == ""} {
	    return
	}
	set fins [open $filesfile r]
	gets $fins bb
	set nn $bb
	for {set ii 1} {$ii <= $bb} {incr ii} { 
	    gets $fins aa 
	    set filename $aa
	    ReadDTMCells $w $filename
	}
	close $fins
    }
    GiD_Process Mescape
}


proc ReadDTMCells {w filename } {
    
    global GidPriv  
    global njumpx njumpy 
    
    #no se si es valido asumir que el fichero siemple cumple con este formato
    #habria que tener la especificacion de lo que es valido, asumo siempre:
    #NCOLS       <n�entero> - n� de columnas
    #NROWS       <n�entero> - n� de filas 
    #XLLCENTER   <n�real>   - coord x del v�rtice inferior izquierdo
    #YLLCENTER   <n�real>   - coord y del v�rtice inferior izquierdo
    #CELLSIZE    <n�real>   - paso
    #NODATA_VALUE <n�real>  - valor que indica la ausencia de datos
    #cada fila representa puntos con igual Y
    #cada columna representa puntos con igual X
    #<n�real Z 0> <n�real Z 2> ... <n�real Z NCOLS-1> 
    # ...
    #<n�real Z (NROWS-1)*NCOLS+0> ... <n�real ZNCOLS*NROWS-1>
    
    destroy $w
    
    Iber::RenumberMeshIfNecessary
    set npoints [expr int([GiD_Info Geometry NumPoints])]
    
    set fin [open $filename r]
    gets $fin aa
    set DTM(ncols) [lindex $aa 1]
    gets $fin aa
    set DTM(nrows) [lindex $aa 1]
    gets $fin aa
    set DTM(xllcenter) [lindex $aa 1]
    gets $fin aa
    set DTM(yllcenter) [lindex $aa 1]
    gets $fin aa
    set DTM(cellsize) [lindex $aa 1]
    gets $fin aa
    set DTM(nondatavalue) [lindex $aa 1]
    
    set OldCreateAlwaysNewPoint [GiD_Set CreateAlwaysNewPoint]
    ::GidUtils::DisableGraphics
    ::GidUtils::DisableWarnLine
    ::GidUtils::WaitState .gid
    
    set GidPriv(StopLoading) 0
    AdvanceBar 0 1 0 [= "Reading DTM"]  [= "Percentage"]:
    
    # cambios para que se dibuje en las coordenadas reales
    set xini [expr ($DTM(xllcenter))]                                
    set xfin [expr ($DTM(xllcenter)+(($DTM(ncols)-1)*$DTM(cellsize)))]        
    set yini [expr ($DTM(yllcenter))]                                
    set yfin [expr ($DTM(yllcenter)+(($DTM(nrows)-1)*$DTM(cellsize)))]            
    
    set pasoi [expr ($DTM(cellsize)*-1)]
    set ikont 0
    
    # he afegit un igual:
    set contlocaly 0
    set i [expr int(0)]
    for {set yi $yfin} {$yi >= $yini} {set yi [expr {$yi+$pasoi}]} {                
	incr i 
	
	incr ikont
	set percent [expr int($ikont/($DTM(nrows)-1.0)*100)]
	AdvanceBar $percent 100 $percent
	if { $GidPriv(StopLoading) } break
	
	
	gets $fin aa        
	#not jump the first and last curves
	if { $i != [expr int(1)] &&  $i != [expr int($DTM(nrows))] } {                                
	    incr contlocaly
	    #jump njumpy internal curves
	    if { $contlocaly < $njumpy } continue
	}
	set contlocaly 0
	
	set y $yi     
	GiD_Process Mescape Geometry Create Line            
	
	set pasoj [expr ($DTM(cellsize))]
	set jkont 0
	#he afegit un igual:
	
	set j [expr int(0)]
	for {set xj $xini} {$xj <= $xfin} {set xj [expr {$xj+$pasoj}]} {
	    incr j 
	    
	    #he canviat la seg�ent l�nia de lloc:                
	    set z [lindex $aa $jkont]
	    
	    incr jkont
	    #not jump the first and last points
	    if { $j != [expr int(1)] &&  $j != [expr int($DTM(ncols))] } {
		incr contlocalx
		#jump njumpx internal points
		if { $contlocalx < $njumpx } continue
	    }
	    set contlocalx 0
	    
	    if { $z == $DTM(nondatavalue) } continue
	    set x $xj
	    GiD_Process $x,$y,$z
	}
	GiD_Process escape
    }
    set ikont 0
    for {set ncol 1} {$ncol<= $DTM(ncols)} {incr ncol} {
	GiD_Process Mescape Geometry Create Line Join
	for {set nrow 1} {$nrow<= $DTM(nrows)} {incr nrow} {                
	    set npunt [expr ($nrow-1)*$DTM(ncols)+$ncol+$npoints]
	    GiD_Process $npunt
	}
	GiD_Process escape
    }
    
    ::GidUtils::EnableGraphics
    ::GidUtils::EnableWarnLine
    ::GidUtils::EndWaitState .gid
    
    if { $GidPriv(StopLoading) } {
	GiD_Process Mescape
	::GidUtils::EnableGraphics
	::GidUtils::EnableWarnLine
	::GidUtils::EndWaitState .gid
	::GidUtils::SetWarnLine [_ "Stop at user demand"]
	WarnWin [_ "Stop at user demand"]
    } else {
	GiD_Set CreateAlwaysNewPoint 1
	GiD_Process Mescape View Zoom Frame escape
	::GidUtils::EnableGraphics
	::GidUtils::EnableWarnLine
	::GidUtils::EndWaitState .gid
	::GidUtils::SetWarnLine [_ "File '%s' Read" $filename]
    }
    AdvanceBar 100 100 100
    close $fin
}
