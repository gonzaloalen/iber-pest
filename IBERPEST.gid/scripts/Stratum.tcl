# to use it to show granulometric curves by step and stratum, 
# in postprocess with this kind of "Sediments" analysis results must call the proc
# -np- Stratum::Window

namespace eval Stratum {
    variable StratumPriv
    variable DiameterEncoded ;#to recover the original encoded string from the real number stored
    set StratumPriv(logarithmic) 1 ;#1 0r 0
    #do not use ::GidPriv(PostResultsAnal) to allow draw other results independently of this window
    set StratumPriv(analysis) "Sediments" ;#hardcoded, this tool is only for this kind of analysis
    set StratumPriv(steps) [list]
    set StratumPriv(stratums) [list]
    set StratumPriv(diameters) [list]
    set StratumPriv(element_ids) ""
    set StratumPriv(step) ""
    set StratumPriv(stratum) ""
    
    #Stratum::Init
}

proc Stratum::GetStratumNamesAndDiametersFromResultNames { result_names } {
    variable DiameterEncoded
    set stratum_names [list]
    set diameters [list]
    #result_name of intereste is something like: "Stratum//Active layer// D = 0.0010m (%)"
    foreach result_name $result_names {
        lassign [GidUtils::Split $result_name //] main_name stratum_name diameter_encoded
        if { $main_name == "Stratum" } {            
            lappend stratum_names $stratum_name
            #diameter_encoded is like "D = 0.0010m (%)" ;#if 
            #set diameter [string range [lindex $diameter_encoded 2] 0 end-1]
            if { [regexp {.*=[ ]*(.*)m} $diameter_encoded dummy diameter] } {                                
                if { ![string is double $diameter] } {
                    W "Warning, the diameter '$diameter' is not a valid real number"
                } else {
                    lappend diameters_by_stratum($stratum_name) $diameter
                    set DiameterEncoded($diameter) $diameter_encoded
                }                
            } else {
                W "Warning, the diameter cannot be extracted from the string '$diameter_encoded'"
            }            
        }
    }
    #remove repetitions and sort
    set stratum_names [lsort -unique -dictionary $stratum_names]
    #sort diameters_by_stratum to check that all are equal
    foreach stratum_name $stratum_names {
        set diameters_by_stratum($stratum_name) [lsort -real $diameters_by_stratum($stratum_name)]
    }
    set first_stratum_name [lindex $stratum_names 0]
    if { $first_stratum_name != "" && [info exists diameters_by_stratum($first_stratum_name)] } {
        set diameters $diameters_by_stratum($first_stratum_name)
        foreach stratum_name [lrange $stratum_names 1 end] {
            if { $diameters_by_stratum($stratum_name) != $diameters } {
                W "Warning, the diameters are not equal for Stratum [lindex $stratum_names 0] and $stratum_name"
                break
            }
        }
    }
    
    
    
    return [list $stratum_names $diameters]
}

proc Stratum::Init { } {
    variable StratumPriv   
    set StratumPriv(steps) [GiD_Info postprocess get all_steps $StratumPriv(analysis)]
    set result_names [GiD_Info postprocess get results_list contour_fill $StratumPriv(analysis) [lindex $StratumPriv(steps) end]]
    lassign [Stratum::GetStratumNamesAndDiametersFromResultNames $result_names] StratumPriv(stratums) StratumPriv(diameters)
    set StratumPriv(element_ids) ""
    set StratumPriv(step) ""
    set StratumPriv(stratum) ""
}

proc Stratum::Window { } {    
    variable StratumPriv
    
    set sedtype [GiD_AccessValue get gendata Sediment_type]    
    set StratumPriv(steps) [GiD_Info postprocess get all_steps $StratumPriv(analysis)]
    set result_names [GiD_Info postprocess get results_list contour_fill $StratumPriv(analysis) [lindex $StratumPriv(steps) end]]
    
    if { [lindex $result_names 0] != "Active stratus" } {
        # Do nothing. Warning is shown after
        tk_dialogRAM .gid.tmpwin [= "Warning"] [= "This option is not available for 'Uniform' sediment"] warning 0 OK
    } else {
        Stratum::Init
        
        set w .gid.stratum
        
        set title [= "Stratum"]    
        InitWindow $w $title PostStratumWindowGeom Stratum::Window
        if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
        
        ttk::labelframe $w.foptions -text [= "Options"]
        GidHelp $w.foptions [= "Set some additional options"]
        
        ttk::checkbutton $w.foptions.logarithmic -text [= "Logarithmic graph"] -variable Stratum::StratumPriv(logarithmic) -command [list Stratum::OnChangeLogarithmic]
        
        GidHelp $w.foptions.logarithmic [= "To show diameter x axis in log 10 scale"]
        
        grid $w.foptions.logarithmic -sticky w -padx 2 -columnspan 2
        
        ttk::label $w.foptions.lstep -text [= "Step"]:
        ttk::combobox $w.foptions.cbstep -textvariable Stratum::StratumPriv(step) -state readonly -values $StratumPriv(steps)
        bind $w.foptions.cbstep <<ComboboxSelected>> [list Stratum::OnChangeStep]
        GidHelp [list $w.foptions.lstep $w.foptions.cbstep] [= "Time step"]
        grid $w.foptions.lstep $w.foptions.cbstep -sticky w -padx 2
        grid configure $w.foptions.cbstep -sticky ew
        
        set StratumPriv(step) [GiD_Info postprocess get cur_step $StratumPriv(analysis)] ;#to update the value to current step
        
        ttk::label $w.foptions.lstratum -text [= "Stratum"]:
        ttk::combobox $w.foptions.cbstratum -textvariable Stratum::StratumPriv(stratum) -state readonly -values $StratumPriv(stratums)
        bind $w.foptions.cbstratum <<ComboboxSelected>> [list Stratum::OnChangeStratum]     
        GidHelp [list $w.foptions.lstratum  $w.foptions.cbstratum ] [= "Stratum to see its granulometry"]
        grid $w.foptions.lstratum  $w.foptions.cbstratum  -sticky w -padx 2
        grid configure $w.foptions.cbstratum -sticky ew
        
        if { $StratumPriv(stratum) == "" } {
            set StratumPriv(stratum) [lindex $StratumPriv(stratums) 0] ;#to update the value
        }
        
        ttk::button $w.foptions.belement -image [gid_themes::GetImage element.png small_icons] -command [list Stratum::PickElements]
        ttk::entry $w.foptions.eelement -textvariable Stratum::StratumPriv(element_ids) -width 8
        GidHelp [list $w.foptions.belement $w.foptions.eelement] [= "Select the elements to draw its granulometric curve"]
        grid $w.foptions.belement $w.foptions.eelement -sticky w -padx 2
        grid configure $w.foptions.eelement -sticky ew
        
        ttk::frame $w.fgranulometry
        
        grid columnconfigure $w.foptions 1 -weight 1
        grid $w.foptions -sticky new -padx 2 -pady 2
        grid $w.fgranulometry -sticky nsew -padx 2 -pady 2
        grid columnconfigure $w.fgranulometry 0 -weight 1
        grid rowconfigure $w.fgranulometry 0 -weight 1
        
        ttk::frame $w.frmButtons -style BottomFrame.TFrame -padding 5    
        ttk::button $w.frmButtons.bclose -text [= "Close"] -command [list Stratum::DestroyWindow $w]
        grid $w.frmButtons.bclose -sticky ew
        grid anchor $w.frmButtons center
        
        grid $w.frmButtons -sticky sew -row 5 -padx 2 -pady 2
        grid rowconfigure $w 1 -weight 1
        grid columnconfigure $w 0 -weight 1
        
        bind $w <Alt-c> [list $w.frmButtons.bclose invoke]
        bind $w <Escape> [list $w.frmButtons.bclose invoke]
    }
}

proc Stratum::PickElements {} {
    variable StratumPriv
    if { $StratumPriv(element_ids) != "" } {    
        GiD_Process 'Label SetToOff Select Elements {*}$StratumPriv(element_ids) escape
    }
    set StratumPriv(element_ids) [GidUtils::UnCompactNumberList [GidUtils::PickEntities Elements multiple [= "Select the elements to draw its granulometric curve"]]]
    #GiD_Redraw ;#to now show in red the selection, GiD 15.0 is not refreshing by itself    
    if { $StratumPriv(element_ids) != "" && $StratumPriv(step) != "" && $StratumPriv(stratum) != "" } {
        GiD_Process 'Label SetToOn Select Elements {*}$StratumPriv(element_ids) escape
        Stratum::DrawGraph
    }
    return
}

proc Stratum::OnChangeLogarithmic { } {
    variable StratumPriv
    if { $StratumPriv(element_ids) != "" && $StratumPriv(step) != "" && $StratumPriv(stratum) != "" } {
        Stratum::DrawGraph
    }
}

proc Stratum::OnChangeStep { } {
    variable StratumPriv
    if { $StratumPriv(element_ids) != "" && $StratumPriv(step) != "" && $StratumPriv(stratum) != "" } {
        Stratum::DrawGraph
    }
}

proc Stratum::OnChangeStratum { } {
    variable StratumPriv
    if { $StratumPriv(element_ids) != "" && $StratumPriv(step) != "" && $StratumPriv(stratum) != "" } {
        Stratum::DrawGraph
    }
}

proc Stratum::GetElementGranulometry { element_ids step stratum_name } {
    variable StratumPriv
    variable DiameterEncoded
    set points [list]
    set num_graphs [llength $element_ids]
    set percents_accumulated_prev [objarray new floatarray $num_graphs 0.0]
    set some_result_not_defined 0
    foreach diameter $StratumPriv(diameters) {
        set diameter_encoded $DiameterEncoded($diameter)        
        set result_name [join [list "Stratum" $stratum_name $diameter_encoded] //]
        set result_data [GiD_Result get -selection $element_ids -array [list $result_name $StratumPriv(analysis) $step]]
        lassign [lindex $result_data 3] ids values
        set percents [lindex $values 0] ;#floatarray
        set percents_accumulated [list]
        foreach id $ids percent $percents percent_accumulated_prev $percents_accumulated_prev {
            if { [IsResultNotDefined $percent] } {
                set some_result_not_defined 1
                break
            }
            set percent_accumulated [expr {$percent+$percent_accumulated_prev}]
            if { $percent_accumulated > 100.0 } {
                set percent_accumulated 100.0
            }
            lappend percents_accumulated $percent_accumulated
        }
        if { [llength $percents_accumulated] == $num_graphs } {
            lappend points [list $diameter {*}$percents_accumulated]  
            set percents_accumulated_prev $percents_accumulated
        }
    }
    if { $some_result_not_defined } {
        W "Some result is not defined. step $step stratum_name"
    }
    return $points
}

proc Stratum::DrawGraph { } {
    variable StratumPriv
    package require gid_graph
    
    set w .gid.stratum
    set f $w.fgranulometry
    if { [winfo exists $f.c] } {
        ::Plotchart::ClearPlot $f.c
        destroy $f.c
    }
    
    set points [Stratum::GetElementGranulometry $StratumPriv(element_ids) $StratumPriv(step) $StratumPriv(stratum)]
    set title [= "Granulometric curve (step %s %s)" $StratumPriv(step) $StratumPriv(stratum)]
    set x_text [= "Diameter (m)"]
    set y_text  [list]
    foreach element_id $StratumPriv(element_ids) {
        lappend y_text  [= "(%%) passing element %d" $element_id]
    }    
    set series_options {}
    set type "normal"
    if { $StratumPriv(logarithmic) } {
        set type "logx"
    }
    set xyplot [GidGraph::InFrame $f $points $title $x_text $y_text $series_options $type]    
    #$xyplot yconfig -scale [list 0.0 100.0 10.0]
    #$xyplot legendconfig -position bottom-right
    
}

proc Stratum::DestroyWindow { w } {
    variable StratumPriv
    if { $StratumPriv(element_ids) != "" } {    
        GiD_Process 'Label SetToOff Select Elements {*}$StratumPriv(element_ids) escape
    }
    if { [winfo exists $w] } {
        destroy $w
    }    
}


