# Iber Post-process tool: WUA button for obtaining WUA graphs
# --- Marcos 2018-11-8 ---

#                                                                                                        #
#                ##        ########        #########        ########                #
#                ##        ##                #        ##                        ##                #                #
#                ##        ##                #        ##                        ##                #                #
#                ##        ##                #        ##                        ##                #                #
#                ##        ##           #        #####                ##           #                #
#                ##        ##                #        ##                        ##          #                        #
#                ##        ##                #        ##                        ##          #                        #
#                ##        ##                #        ##                        ##           #                #
#                ##        ########        #########        ##                #                #
#                                                                                                        #

proc Wua {} {
    global directory
    global Project
    
    set Project [GiD_Info project ModelName] 
    if { $Project == "UNNAMED" } {
        tk_dialogRAM .gid.tmpwin error \
            [= "Before writting results raster, a project title is needed. Save project to get it" ]\
            error 0 OK
        return
    }
    
    # WarnWinText $Project
    
    set directory $Project.gid
    if { [file pathtype $directory] == "relative" } {
        set directory [file join [pwd] $directory]
    }    
    # WarnWinText $directory
    
    set wuadir ""
    append wuadir $directory "/WUA"
    
    # WarnWinText $wuadir
    
    GidUtils::DisableGraphics
    GidUtils::DisableWarnLine
    
    foreach grffile [glob -directory $wuadir *.grf] {
        # WarnWinText $grffile
        
        GiD_Process Mescape results graphs readgraph $grffile Mescape
        GiD_Process Mescape Results Graphs OptionsGraph Y_axis SetMin 0.0 'Zoom Frame Escape 'Zoom Frame Escape
        GiD_Process Mescape Options OpaqueLegends Yes 'Zoom Frame Escape 'Zoom Frame Escape                
        
    }        
    
    GidUtils::EnableGraphics
    GidUtils::EnableWarnLine
    GidUtils::OpenWindow GRAPHS
    
}
