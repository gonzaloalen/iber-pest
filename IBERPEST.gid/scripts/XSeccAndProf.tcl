namespace eval XsAndProf {
    
}

proc XsAndProf::CreateProfiles { } {  
    
    # set step [lindex [GiD_Info postprocess get all_steps "Hydraulic"] end]
    
    # GiD_Process Mescape DisplayStyle bodybound Mescape \
        # Results AnalysisSel {Maps of Maximums} $step Geometry Original \
        # escape escape escape escape View Label Off escape escape escape escape \
        # results Geometry NoResults escape escape escape escape escape \
        # Results ContourFill {Water Elevation (m)} 
    
    GiD_Process Mescape DoCut CutWire    
    
}  

#auxiliary procedure, to temporary implement the GiD event AfterCreateCutSet to integrate the created cut
proc XsAndProf::AfterCreateCutSet { cut_name } {   
    
    rename ::AfterCreateCutSet "" ;#undefine the GiD_event
    
    set graphset_name [GiD_GraphSet create]               
    
    GiD_GraphSet current $graphset_name
    
    #define the GiD-event temporary only, for this case that want integrate the new cut
    
    proc ::AfterCreateGraph { name } {
        
        return [::XsAndProf::AfterCreateGraph $name]
    }   
    set required_analysis Hydraulic
    set required_result "Specific Discharge"
    if { [GiD_Info postprocess get cur_analysis] != $required_analysis } {
        set step [lindex [GiD_Info postprocess get all_steps $required_analysis] end]
        GiD_Process Mescape Results AnalysisSel $required_analysis $step escape
    }
    GiD_Process Mescape Results Graphs Integrate 2D AllSteps NormalIntegration VectorResult $required_result CutSets $cut_name escape
    
}


#auxiliary procedure, to temporary implement the GiD event AfterCreateGraph to modify the graph
proc XsAndProf::AfterCreateGraph { graph_name } {
    
    rename ::AfterCreateGraph "" ;#undefine the GiD_event
    
    set graph_data [GiD_Graph get $graph_name]
    
    #modify titles and units
    lset graph_data 0 [= "Time"]
    lset graph_data 1 [= "Discharge"]
    lset graph_data 4 "s"
    lset graph_data 5 "m3/s"
    GiD_Graph delete $graph_name
    GiD_Graph create $graph_name {*}$graph_data
    
}


#crete a graph with the hydrogram (integrating specific discharge) on a new create cut
proc XsAndProf::ResultHydrograph { } {  
    
    #set step [lindex [GiD_Info postprocess get all_steps "Maps of Maximums"] end]
    GiD_Process Mescape DisplayStyle bodybound escape
    #GiD_Process Mescape Results AnalysisSel {Maps of Maximums} $step Geometry Original escape
    GiD_Process Mescape View Label Off escape
    #GiD_Process Mescape Results Geometry NoResults escape
    #GiD_Process Mescape Results ContourFill {Water Elevation (m)}
    
    #define the GiD-event temporary only, for this case that want integrate the new cut
    proc ::AfterCreateCutSet { name } {
        
        return [::XsAndProf::AfterCreateCutSet $name]
        
    }
    
    #this process command will interactively ask the user to select where to cut....
    
    GiD_Process Mescape DoCut CutWire
    
}


proc XsAndProf::ViewProfileResWin { } {
    global XsAndProfPriv 
    
    set w .gid.xsection    
    set XsAndProfPriv(selC) ""
    set XsAndProfPriv(selT) ""
    
    set XsAndProfPriv(time) [GiD_Info postprocess get all_steps "Hydraulic"]
    set XsAndProfPriv(cuts) [GiD_Info postprocess get all_cuts]
    
    
    InitWindow $w [= "Select graph values"] PostCreateGraphWindowGeom XsAndProf::ViewProfileResWin
    
    ttk::frame $w.frmData    
    ttk::label $w.frmData.lnom -text [= "Section"]
    set e1 [ttk::combobox $w.frmData.seclist -state readonly -values $::XsAndProfPriv(cuts) -textvariable ::XsAndProfPriv(selC)]    
    ttk::label $w.frmData.lrow -text [= "Instant"]
    set e2 [ttk::combobox $w.frmData.timelist -state readonly -values $::XsAndProfPriv(time) -textvariable ::XsAndProfPriv(selT)]
    
    
    set XsAndProfPriv(e) [list $e1 $e2] ;#to check its values
    
    ttk::frame $w.frmButton -style BottomFrame.TFrame
    ttk::button $w.frmButton.accept -text [= "Accept"] -command [list XsAndProf::ViewProfileInstRes $w]  -style BottomFrame.TButton
    ttk::button $w.frmButton.close -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    
    grid $w.frmData.lnom $w.frmData.seclist -sticky ew
    grid $w.frmData.lrow $w.frmData.timelist -sticky ew
    
    grid configure $w.frmData.lnom $w.frmData.lrow -sticky w
    grid $w.frmData -padx 5 -pady 5 -sticky nsew
    grid columnconfigure $w.frmData 1 -weight 1
    
    grid $w.frmButton -sticky nsew
    grid $w.frmButton.accept $w.frmButton.close -padx 5 -pady 6
    if { $::tcl_version >= 8.5 } { grid anchor $w.frmButton center }
    
    grid columnconfigure $w "0" -weight 1
    grid rowconfigure $w "0" -weight 1
    
    bind $w <Alt-c> "$w.frmButton.close invoke"
    bind $w <Escape> "$w.frmButton.close invoke"
    bind $w <Return> "$w.frmButton.accept invoke"
    
    focus $w.frmButton.accept         
}

proc XsAndProf::Selection { e type } {
    global XsAndProfPriv    
    set idx [$e curselection]
    if {$type=="Cut"} {
        if {[llength $idx]==1} {
            set XsAndProfPriv(selC) [lindex $XsAndProfPriv(cuts) $idx]
        }
    } elseif {$type=="Time"} {
        if {[llength $idx]==1} {
            set XsAndProfPriv(selT) [lindex $XsAndProfPriv(time) $idx]
        }
    }
    
}


# value: all or the time
# selection: the cut name
proc XsAndProf::ViewProfileInstRes { w} {
    global XsAndProfPriv    
    set value $XsAndProfPriv(selT)
    set selection $XsAndProfPriv(selC)
    set analysis_name_1 Topography
    set result_name_1 Elevation   
    set analysis_name_2 Hydraulic
    set result_name_2 {Water Elevation (m)}
    GidUtils::DisableGraphics
    GidUtils::DisableWarnLine
    GidUtils::WaitState .gid        
    GiD_Process Mescape results analysissel $analysis_name_1 0.0 escape escape escape escape \
        results Geometry NoResults Geometry NoResults Geometry Original escape escape escape escape \
        View Label Off escape escape escape escape escape
    GiD_Process Mescape Results Graphs BorderGraph \
        LineVariation $result_name_1 $result_name_1 $selection 'Zoom Frame escape
    GiD_Process Mescape results analysissel $analysis_name_2 $value escape escape escape escape \
        results Geometry NoResults Geometry NoResults Geometry Original escape escape \
        escape escape View Label Off escape escape escape escape escape
    GiD_Process Mescape Results Graphs BorderGraph LineVariation $result_name_2 $result_name_2 $selection escape 'Zoom Frame escape
    GidUtils::EnableGraphics
    GidUtils::EnableWarnLine
    GidUtils::EndWaitState .gid
    GidUtils::SetWarnLine [_ "Results Graphs Drawn"]    
    GidUtils::OpenWindow GRAPHS    
}

# for each existing cut create a graph with the water level (topo+water depth) at last step
proc XsAndProf::ViewProfileMaxRes { } {    
    set CutList [GiD_Info postprocess get all_cuts]
    set numC [llength $CutList]    
    if { $numC == 0 } {       
        tk_dialogRAM .gid.tmpwin [= "Result Graph"] [= "There are no Profiles or XSecctions."] warning 0 OK
        return        
    } else {
        
        set step [lindex [GiD_Info postprocess get all_steps "Maps of Maximums"] end]
        
        GidUtils::DisableGraphics
        GidUtils::DisableWarnLine
        GidUtils::WaitState .gid        
        foreach item $CutList {       
            set name [GiD_GraphSet create]
            GiD_GraphSet current $name   
            
            # GiD_Process Mescape results \
                # analysissel "Topography" 0.0 escape escape escape escape results Geometry \
                # NoResults Geometry NoResults Geometry Original escape escape escape escape View \
                # Label Off escape escape escape escape escape 
            # GiD_Process Mescape Results Graphs BorderGraph \
                # LineVariation "Elevation (m)" "Elevation (m)" $item \
                # 'Zoom Frame escape 
            
            # GiD_Process Mescape results analysissel "Maps of Maximums" $step escape escape escape escape \
                # results Geometry NoResults Geometry NoResults Geometry Original escape escape \
                # escape escape View Label Off escape escape escape escape escape 
            # GiD_Process Mescape Results Graphs \
                # BorderGraph LineVariation "Water Elevation (m)" "Water_Elevation (m)" $item \
                # escape 'Zoom Frame escape
            
            foreach analysis_name {{Elevation (m)} {Topography}} result_name {{Water Elevation (m)} {Maps of Maximums}} {
                GiD_Process MEscape Results AnalysisSel {Topography} 0.0 \
                    Graphs BorderGraph LineVariation $analysis_name $analysis_name $item Mescape
                
                GiD_Process Mescape Results AnalysisSel {Maps of Maximums} $step \
                    Graphs BorderGraph LineVariation $result_name $result_name $item escape Mescape MEscape \
                    Results AnalysisSel {Maps of Maximums} $step
            }            
        }
        GidUtils::EnableGraphics
        GidUtils::EnableWarnLine
        GidUtils::EndWaitState .gid
        GidUtils::SetWarnLine [_ "Maximums Water Elevation Results Graphs Drawn"]            
        GidUtils::OpenWindow GRAPHS
    }    
}
