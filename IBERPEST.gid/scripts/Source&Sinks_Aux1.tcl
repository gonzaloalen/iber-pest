
#***********Impone condicones en rejas a partir de un archivo, hay que activarlo desde carpa.tcl**********************************
proc rejasWin {} {
    global PropPriv    
    
    set PropPriv(idata) ""
    set w .gid.properties
    set PropPriv(ImportPref) 1
    
    PropSelec1 $w
    
    # # ventana princ
    # InitWindow $w "Drainage inlets" PrePropertiesWindowGeom rejasWin
    
    # # widget
    # frame $w.frmSelection -bd 2 -relief groove
    # radiobutton $w.frmSelection.def -text "TXT File" -variable PropPriv(ImportPref) -value 1
    # #   radiobutton $w.frmSelection.ram -text "DAT File" -variable PropPriv(ImportPref) -value 2
    
    # set def_back [$w cget -background]
    # frame $w.frmButtons -bg [CCColorActivo $def_back]
    # button $w.frmButtons.btnAccept -text "Accept" -command "PropSelec1 $w" -width 6
    # button $w.frmButtons.btnClose -text "Close" -command "destroy $w" -width 6
    
    # # empaquetamiento
    # grid $w.frmSelection -padx 5 -pady 5 -sticky news
    # grid $w.frmSelection.def 
    # grid columnconf $w.frmSelection 1 -weight 1
    # # grid $w.frmSelection.ram 
    # # grid columnconf $w.frmSelection 2 -weight 1
    
    # grid $w.frmButtons -sticky ews -columnspan 7
    # grid $w.frmButtons.btnAccept $w.frmButtons.btnClose -padx 5 -pady 6
    
    # grid columnconf $w "0" -weight 1
    # grid rowconfigure $w "0" -weight 1
    
    # focus $w.frmButtons.btnAccept
}

proc PropSelec1 {w } {
    global PropPriv
    
    if {$PropPriv(ImportPref) == 1} {
        rejas1 $w
    } elseif {$PropPriv(ImportPref) == 2} {
        rejas2 $w
    }
    destroy $w
}

proc  rejas1  { w } {
    #global PropPriv GidPriv ProblemTypePriv
    global PropPriv GidPriv
    destroy $w  
    
    # directorio de trabajo
    set aa [.central.s info Project]
    set Project [lindex $aa 1]
    set Project [file normalize $Project]    
    set directory $Project.gid
    
    # ubicaci�n de la base de los datos
    set filename [Browser-ramR file read .gid [_ "Read Properties file"] \
            {} {{{} {.csv}} {{} {.dat}} {{} {.txt}} {{All files} {.*}}} ]
    if {$filename == "" } {
        return
    }   
    # lee la base datos 
    CreaRejas $filename
}

proc rejas2 { } {
    #do nothing (for the moment)
}

#Pone condicioones de rejas a partir de un archivo de datos:rejas.txt o fuysums.dat
proc CreaRejas { namefile } {
    #variable percent
    #variable userstop
    
    #set im 1
    #set percent 0
    #set userstop 0
    
    set file [open $namefile]
      
    #::GidUtils::CreateAdvanceBar [= "Reading Grate inlets"]... [= "Percentage"] \
        #::percent 100.0000001 [list set UrbanDrainage::userstop 1]
    
    #set numrejas [gets $file]
#     DrainageInlet::Window
    set numrejas 0
       
    while {![eof $file] } {
        set datrejas [gets $file]
        set llarg [llength $datrejas]
        #DrainageInlet::NewDrainageInletfromfile
        
        if {$llarg>0} { 
            set nn [lrange $datrejas 0 0]
            set vv [lrange $datrejas 1 1]
            set ss [lrange $datrejas 2 2]
            set s2 [lrange $datrejas 3 3]
            set s3 [lrange $datrejas 4 4]
            append ss " "
            append ss $s2
            append ss " "
            append ss $s3
            set ct [lrange $datrejas 5 5]
            set ee [lrange $datrejas 6 6]
            set e2 [lrange $datrejas 7 7]
            set e3 [lrange $datrejas 8 8]
            append ee " "
            append ee $e2
            append ee " "
            append ee $e3
            set tt [lrange $datrejas 9 9]
            set AA [lrange $datrejas 10 10]
            set BB [lrange $datrejas 11 11]
            set LL [lrange $datrejas 12 12]
            set WiWi [lrange $datrejas 13 13]
            set EfEf [lrange $datrejas 14 14]
            set WW [lrange $datrejas 15 15]
            set OO [lrange $datrejas 16 16]
            set II [lrange $datrejas 17 17]
            set nn [lrange $datrejas 18 18]
            #DrainageInlet::RenameDrainageInletfromfile ($nn)
            
            set nt [lrange $datrejas 19 19]
            # afegir aqu� la lectura de TT
            
            set taula ""
            
            if { $II == 1 } {
                for {set ii 1} {$ii<=$nt} {incr ii 1} {
                    set datrejas [gets $file]
                    append taula $datrejas " "
                }
            }                            
            DrainageInlet::Inletsfromfile $vv $ss $ct $ee $tt $AA $BB $LL $WiWi $EfEf $WW $OO $II $nn $nt $taula 
            incr numrejas
        }
    }
    
    # for {set ireja 1} {$ireja<=$numrejas} {incr ireja 1} {
        # set nr [expr double($numrejas)]
        # set p1 [expr ($ireja/$nr)*100]
        # set percent [format "%5.2f" $p1]
        # if { $userstop != 0 } break 
        # } 
    close $file
    
    DrainageInlet::Window
    DrainageInlet::Apply T
    DrainageInlet::DestroyDrainageInletWindow %W w
    # ::GidUtils::DeleteAdvanceBar
    #tk_dialogRAM .gid.tmpwin "Info" \
        #"The process -Import Drainage Inlets by File- has finished" \
        #info 0 OK
#     DrainageInlet::DestroyDrainageInletWindow
    GidUtils::SetWarnLine [= "%s inlets have been improted" $numrejas]
}

proc sameside {isame p11 p12 p21 p22 a1 a2 b1 b2} {
    set cp1 [expr ($b1-$a1)*($p12-$a2)-($b2-$a2)*($p11-$a1)]
    set cp2 [expr ($b1-$a1)*($p22-$a2)-($b2-$a2)*($p21-$a1)]
    set isame 0
    set ix [expr $cp1*$cp2]
    if {$ix>=0} { set isame 1}
    return $isame
}

#coordenadas del centro de gravedad del elemento donde se ubica la reja
proc celems {ielem } {
    set datos [GiD_Info ListMassProperties Elements $ielem]
    set xc1 [lrange $datos 11 11]
    set xc [string range $xc1 9 end]
    set yc [lrange $datos 12 12]
    set zc [lrange $datos 13 13]
    set res [format "%12.3f %12.3f %12.3f" $xc $yc $zc]
    return $res
}
