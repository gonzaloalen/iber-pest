# Iber Pre-process Menu ### Marcos 2018-05-24 ###

#
#                ##        ########        #########       #######
#                ##        ##      #       ##              ##     #
#                ##        ##      #       ##              ##     #
#                ##        ##      #       ##              ##    # 
#                ##        ########        #####           ######
#                ##        ##      #       ##              ##  #  
#                ##        ##      #       ##              ##   # 
#                ##        ##      #       ##              ##    #
#                ##        ########        #########       ##     #
#

namespace eval SelRes {
    
}

#POST-process view results on elements
proc SelRes::ResOnElem {} {
    GiD_Process Mescape View Label Select ResultOnView
}


#POST-process view results on elements
proc SelRes::ResOnElemOff {} {
    GiD_Process Mescape View Label Off
}
