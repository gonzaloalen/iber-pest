
namespace eval MixturesLC {
    variable data ;#array of values
    variable mixtureslc ;#list of names of data
    variable current_mixturelc ;#name of current selected mixture
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {width height actlaythick} ;#widht == Stratums; height == Classes; actlaythick == Active Layer Thickness
    variable fields_defaults {1 1 0.1}   
    variable drawopengl
    variable nstrat
    variable nclass
    variable actlaythick
}

proc MixturesLC::UnsetVariables { } {
    variable data
    variable mixtureslc      
    unset -nocomplain data
    unset -nocomplain mixtureslc
	unset -nocomplain current_mixturelc
}

proc MixturesLC::SetDefaultValues { } {    
    variable data
    variable mixtureslc
    variable current_mixturelc
    variable current_value
    variable fields
    variable fields_defaults
    array unset data
    set mixtureslc {}
    foreach field $fields  {
        set current_value($field) ""
    }
    set current_mixturelc {}
}

#store the mixtureslc information in a hiddend field of the problem data
#then this data is saved with the model without do nothing more
proc MixturesLC::FillTclDataFromProblemData { } {
    variable data
    variable mixtureslc
    
    if { [catch {set x [GiD_AccessValue get gendata MixturesLCData]} msg] } {
        #reading a model created by an old version of the problemtype this gendata doesn't exists
        return
    }
    array set data $x
    
    set mixtureslc [list]
    foreach item [array names data *,width] {
        lappend mixtureslc [string range $item 0 end-6]
    }
    set mixtureslc [lsort -dictionary $mixtureslc]
    MixturesLC::SetCurrentMixturelc [lindex $mixtureslc 0]
}

proc MixturesLC::FillProblemDataFromTclData { } {
    variable data
    variable mixtureslc
    set x [array get data]
    GiD_AccessValue set gendata MixturesLCData $x
}

#proc MixturesLC::WriteCalculationFile { } {
    #    variable data
    #    set res ""       
    
    #    set i 0
    #    foreach mixturelc [MixturesLC::GetMixtureslc] {
        #        incr i        
        #        set item [list]
        #        lappend item $i
        #        lappend item {*}[lrange $data($mixturelc,start) 0 1]
        #        lappend item {*}[lrange $data($mixturelc,end) 0 1]
        #        lappend item [lindex $data($mixturelc,start) 2]
        #        lappend item [lindex $data($mixturelc,end) 2]
        #        if { $data($mixturelc,type) == 1 } {
            #            lappend item 1
            #        } else {
            #            lappend item 2
            #        }
        #        lappend item $data($mixturelc,width)
        #        if { $data($mixturelc,type) == 1 } {
            #            lappend item $data($mixturelc,height)
            #        } else {
            #            lappend item 0
            #        }
        #        lappend item $data($mixturelc,manning)
        #        lappend item $mixturelc        
        #        append res [join $item ,]\n
        #    }
    #    return $res
    #}

proc MixturesLC::GetMixtureslc { } {
    variable mixtureslc
    if { ![info exists mixtureslc] } {
        return ""
    }
    return $mixtureslc
}

proc MixturesLC::GetCurrentMixturelc { } {
    variable current_mixturelc
    return $current_mixturelc
}

proc MixturesLC::OnChangeSelectedMixturelc { cb } {   
    MixturesLC::SetCurrentMixturelc [MixturesLC::GetCurrentMixturelc]
}

proc MixturesLC::SetCurrentMixturelc { mixturelc } {
    variable data
    variable current_mixturelc
    variable current_value
    variable fields   
    #fill in the current_value variables with data
    if { $mixturelc != "" } {
        foreach field $fields {
            if { ![info exists data($mixturelc,$field)] } {
                set data($mixturelc,$field) 0
            }
            set current_value($field) $data($mixturelc,$field)           
        }
    } else {
        foreach field $fields {
            set current_value($field) ""
        }
    }
    set current_mixturelc $mixturelc  
}

proc MixturesLC::GetIndex { mixturelc } {
    variable mixtureslc    
    return [lsearch $mixtureslc $mixturelc]
}

proc MixturesLC::Exists { mixturelc } {    
    if { [MixturesLC::GetIndex $mixturelc] != -1 } {
        set exists 1
    } else {
        set exists 0
    }
    return $exists
}

proc MixturesLC::GetMixturelcAutomaticName { } {
    set basename [_ "stratums-classes"]
    set mixturelc $basename
    
    #This lines are commented waiting for future versions. The last one substitutes 'set mixturelc $basename-$i' (previous)
    #set i 1
    #set mixturelc $basename-$i
    
    if { [MixturesLC::Exists $mixturelc] } {
        tk_dialogRAM .gid.tmpwin [= "Warning"] [= "The number of Stratums and Classes have been already defined"] warning 0 OK
        #already exists
        #return $mixturelc
    }
    
    #This lines are commented waiting for future versions. The last one substitutes 'set mixtureLC $basename-$i' (previous). See culverts.tlc
    #while { [MixturesLC::Exists $mixturelc] } {        
        #    incr i
        #    set mixturelc $basename-$i        
        #}
    return $mixturelc
}

proc MixturesLC::NewMixturelc { cb } {
    variable mixtureslc
    variable data
    variable fields 
    variable fields_defaults
    set mixturelc [MixturesLC::GetMixturelcAutomaticName]     
    
    if { [MixturesLC::Exists $mixturelc] } {
        #already exists
        return 1
    }
    foreach field $fields value $fields_defaults {
        set data($mixturelc,$field) $value
    }
    lappend mixtureslc $mixturelc   
    if { [winfo exists $cb] } {
        $cb configure -values [MixturesLC::GetMixtureslc]
    }
    MixturesLC::SetCurrentMixturelc $mixturelc
    return 0
}

proc MixturesLC::DeleteMixturelc { cb } {
    variable mixtureslc
    variable data
    variable fields 
    variable fields_defaults
    set mixturelc [MixturesLC::GetCurrentMixturelc] 
    if { ![MixturesLC::Exists $mixturelc] } {
        #not exists
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete mixturelc '%s'" $mixturelc] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {     
        set i [MixturesLC::GetIndex $mixturelc] 
        array unset data $mixturelc,*        
        set mixtureslc [lreplace $mixtureslc $i $i]       
        if { [winfo exists $cb] } {
            $cb configure -values [MixturesLC::GetMixtureslc]
        }
        MixturesLC::SetCurrentMixturelc [lindex $mixtureslc 0]  
        GiD_Redraw     
    }
    return 0
}

proc MixturesLC::RenameMixturelc { cb } {
    variable data
    variable mixtureslc
    variable fields
    set mixturelc [MixturesLC::GetCurrentMixturelc]   
    if { ![MixturesLC::Exists $mixturelc] } {
        #not exists
        return 1
    } 
    set new_name [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $mixturelc] \
            [= "Enter new name of %s '%s'" [= "mixturelc"] $mixturelc] gidquestionhead "any" ""]
    if { $new_name != "--CANCEL--" } {
        foreach field $fields {
            set data($new_name,$field) $data($mixturelc,$field)
        }
        array unset data $mixturelc,*
        set i [MixturesLC::GetIndex $mixturelc] 
        lset mixtureslc $i $new_name
        if { [winfo exists $cb] } {
            $cb configure -values [MixturesLC::GetMixtureslc]           
        }
        MixturesLC::SetCurrentMixturelc $new_name       
        GiD_Redraw 
    }
    return 0
}

#apply the values of the current window
proc MixturesLC::Apply { } {
    variable data
    variable current_value
    variable fields
    variable nstrat
    variable nclass
    variable actlaythick
    
    set mixturelc [MixturesLC::GetCurrentMixturelc]    
    foreach field $fields {
        if {$field == "width"} {
            MixturesLC::GetNstrats
        } elseif {$field == "height"} {
            MixturesLC::GetNclasses
        } elseif {$field == "actlaythick"} {
            MixturesLC::GetActlaythick
        }
    }
}


#Get number of Stratums (nstrat in F)
proc MixturesLC::GetNstrats { } {
    variable data
    variable current_value
    variable fields
    variable nstrat
    variable nclass
    variable actlaythick
    
    set mixturelc [MixturesLC::GetCurrentMixturelc]    
    foreach field $fields {
        if { $field == "width" } {
            if { $current_value($field) < 1 || $current_value($field) > 10 } {
                tk_dialogRAM .gid.tmpwin [= "Warning"] [= "The number of Stratums must be an integer between 1 and 10"] warning 0 OK
            } else {
                set nstrat $current_value($field)
                set data($mixturelc,$field) $current_value($field)
            } 
        }
        
        #if {$field == "width"} {
            #        set nstrat $current_value($field)
            #} elseif {$field == "height"} {
            #        set nclass $current_value($field)
            #} elseif {$field == "actlaythick"} {
            #        set actlaythick $current_value($field)
            #}
    }
    
    return $nstrat
}

#Get number of Classes (nclass in F)
proc MixturesLC::GetNclasses { } {
    variable data
    variable current_value
    variable fields
    variable nstrat
    variable nclass
    variable actlaythick
    
    set mixturelc [MixturesLC::GetCurrentMixturelc]    
    foreach field $fields {
        if { $field == "height" } {
            if { $current_value($field) < 1 || $current_value($field) > 10 } {
                tk_dialogRAM .gid.tmpwin [= "Warning"] [= "The number of Classes must be an integer between 1 and 10"] warning 0 OK
            } else {
                set nclass $current_value($field)
                set data($mixturelc,$field) $current_value($field)
            } 
        }
        
        #if {$field == "width"} {
            #        set nstrat $current_value($field)
            #} elseif {$field == "height"} {
            #        set nclass $current_value($field)
            #} elseif {$field == "actlaythick"} {
            #        set actlaythick $current_value($field)
            #}
    }
    
    return $nclass
}

#Get Active Layer Thickness (d90 in F)
proc MixturesLC::GetActlaythick { } {
    variable data
    variable current_value
    variable fields
    variable nstrat
    variable nclass
    variable actlaythick
    
    set mixturelc [MixturesLC::GetCurrentMixturelc]    
    foreach field $fields {
        
        if {$field == "width"} {
            set nstrat $current_value($field)
            set data($mixturelc,$field) $current_value($field)
        } elseif {$field == "height"} {
            set nclass $current_value($field)
            set data($mixturelc,$field) $current_value($field)
        } elseif {$field == "actlaythick"} {
            set actlaythick $current_value($field)
            set data($mixturelc,$field) $current_value($field)
        }
    }
    
    return $actlaythick
}

#not MixturesLC::StartDraw and MixturesLC::EndDraw are not used, the Draw button is hidden (because unregister fail in GiD 11.1.5d)
#and the redraw procedure is registered when loading the problemtype, and each mixturelc is draw depending on its 'visible' variable value
#proc MixturesLC::StartDraw { bdraw} {
    #    variable drawopengl
    #    set drawopengl [GiD_OpenGL register MixturesLC::ReDraw]
    #    GiD_Redraw
    #    $bdraw configure -text [= "Finish draw"] -command [list MixturesLC::EndDraw $bdraw]
    #}

#proc MixturesLC::EndDraw { bdraw } {
    #    variable drawopengl
    #    if { [info exists drawopengl] } { 
        #        #unregister the callback tcl procedure to be called by GiD when redrawing
        #        GiD_OpenGL unregister $drawopengl
        #        catch {unset drawopengl}
        #        GiD_Redraw
        #        if { $bdraw != "" } {
            #            $bdraw configure -text [= "Draw"] -command [list MixturesLC::StartDraw $bdraw]
            #        }
        #    }
    #}

#proc MixturesLC::ReDraw { } {
    #    foreach mixturelc [MixturesLC::GetMixtureslc] {
        #        MixturesLC::Draw $mixturelc
        #    }
    #}

# proc MixturesLC::Draw { mixturelc } {
    #    variable data
    #    if { $mixturelc == "" || ![info exists data($mixturelc,visible)] || !$data($mixturelc,visible) } {
        #        return 1
        #    }
    #    foreach field {start end} {
        #        if { [llength $data($mixturelc,$field)] != 3 } {
            #            return 1
            #        }
        #        foreach v $data($mixturelc,$field) {
            #            if { ![string is double $v] } {
                #                return 1
                #            }
            #        }
        #    }    
    #    set blue {0 0 1}
    #    GiD_OpenGL draw -color $blue
    #    GiD_OpenGL draw -pointsize 3
    #    GiD_OpenGL draw -begin points
    #    GiD_OpenGL draw -vertex $data($mixturelc,start)
    #    GiD_OpenGL draw -vertex $data($mixturelc,end)
    #    GiD_OpenGL draw -end
    #    GiD_OpenGL draw -begin lines
    #    GiD_OpenGL draw -vertex $data($mixturelc,start)
    #    GiD_OpenGL draw -vertex $data($mixturelc,end)
    #    GiD_OpenGL draw -end
    #    #show label
    #    set p [MathUtils::VectorSum $data($mixturelc,start) $data($mixturelc,end)]
    #    set p [MathUtils::ScalarByVectorProd 0.5 $p]
    #    GiD_OpenGL draw -rasterpos $p
    #    GiD_OpenGL drawtext $mixturelc
    #    return 0
    #}

proc MixturesLC::DestroyMixturelcWindow { W w } {   
    if { $W == $w } {
        #        MixturesLC::EndDraw ""
        MixturesLC::FillProblemDataFromTclData             
    }
}

#proc MixturesLC::OnChangeType { f } {
    #    variable current_value
    #    if { $current_value(type) == 1 } {
        #        #Rectangular
        #        $f.lwidth configure -text [= "Width"]        
        #        grid $f.lheight $f.eheight
        #    } else {
        #        #Circular
        #        $f.lwidth configure -text [= "Diameter"]
        #        grid remove $f.lheight $f.eheight
        #    }
    #}

#proc MixturesLC::PickPointInSurfaceOrElementCmd { entry } {
    #    set view_mode [GiD_Info Project ViewMode]
    #    if { $view_mode == "GEOMETRYUSE" } {
        #        set xyz [GidUtils::GetCoordinates [_ "Pick point into an surface"] PointInSurface GEOMETRYUSE]
        #        #set surface_id $::GidPriv(selection)  
        #    } elseif { $view_mode == "MESHUSE"} {
        #        set xyz [GidUtils::GetCoordinates [_ "Pick point into an element"] PointInElement MESHUSE]
        #        #set element_id $::GidPriv(selection)    
        #    } else {
        #        set xyz ""
        #    }        
    #    if { $xyz != "" } {                    
        #        set res [format "%g %g %g" {*}$xyz]
        #        $entry delete 0 end
        #        $entry insert end $res        
        #    }
    #}

proc MixturesLC::Window { } {
    variable data
    variable mixtureslc
    variable current_mixturelc
    variable current_value
    
    if { $mixtureslc != "stratums-classes" } {
        MixturesLC::SetDefaultValues
        MixturesLC::NewMixturelc cb
    }
    
    MixturesLC::FillTclDataFromProblemData    
    
    set w .gid.mixturelc
    InitWindow $w [= "Number of Stratums&Classes"] MixturelcWindowGeom MixturesLC::Window
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    set f [ttk::frame $w.fmixtureslc]
    
    ttk::combobox $f.cb1 -textvariable MixturesLC::current_mixturelc -values [MixturesLC::GetMixtureslc] -state readonly
    bind $f.cb1 <<ComboboxSelected>> [list MixturesLC::OnChangeSelectedMixturelc %W]
    #ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list MixturesLC::NewMixturelc $f.cb1]
    #GidHelp $f.bnew  [= "Create a new mixturelc"]
    #ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list MixturesLC::DeleteMixturelc $f.cb1]
    #GidHelp $f.bdel  [= "Delete a mixturelc"]
    #ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list MixturesLC::RenameMixturelc $f.cb1]
    #GidHelp $w.f.bren  [= "Rename a mixturelc"]   
    #grid $f.cb1 $f.bnew $f.bdel -sticky w
    grid $f -sticky new
    grid rowconfigure $f 0 -weight 1
    
    set f [ttk::frame $w.fmixturelc]        
    #set field visible
    #ttk::checkbutton $f.ch$field -text [= "Visible"] -variable MixturesLC::current_value($field)
    #grid $f.ch$field -sticky w
    
    
    #widht == Stratums; height == Classes; actlaythick == Active Layer Thickness
    foreach field {width height actlaythick} text [list [= "Stratums"] [= "Classes"] [= "Active layer thick. (m)"]] {
        ttk::label $f.l$field -text $text
        ttk::entry $f.e$field -textvariable MixturesLC::current_value($field)
        grid $f.l$field $f.e$field -sticky ew
    }
    
    grid $f -sticky new
    grid columnconfigure $f {1} -weight 1    
    
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.bapply -text [= "Apply"] -command [list MixturesLC::Apply] -style BottomFrame.TButton
    #ttk::button $f.bdraw -text [= "Draw"] -command [list MixturesLC::StartDraw $f.bdraw] -style BottomFrame.TButton
    ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    #grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
    grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }        
    
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    bind $w <Alt-c> "$f.bclose invoke"
    bind $w <Escape> "$f.bclose invoke"   
    bind $w <Destroy> [list +MixturesLC::DestroyMixturelcWindow %W $w] ;# + to add to previous script  
}

proc MixturesLC::CloseWindow { } {
    set w .gid.mixturelc    
    if { [winfo exists $w] } {
        destroy $w
    }
}
