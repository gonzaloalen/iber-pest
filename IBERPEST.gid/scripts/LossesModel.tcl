# escolano: this file will replace (when GiD 14.1.8d is available) HydraLosses.tcl
namespace eval LossesModel {
    variable data ;#array of values
    variable zones ;#list of names of data
    variable current_zone ;#name of current selected zone
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {start type Hf0 Hfc HK SCSCN LIa LFi GASuction GATotPoros GAInitialSat GAKs GAInitialLoss GASoilDepth}
    variable fields_defaults { {} 1 0.0 0.0 2.9957 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0}   
    variable drawopengl
}

proc LossesModel::UnsetVariables { } {
    variable data
    variable zones      
    unset -nocomplain data
    unset -nocomplain zones
}

proc LossesModel::SetDefaultValues { } {    
    variable data
    variable zones
    variable current_zone
    variable current_value
    variable fields
    variable fields_defaults
    array unset data
    set zones {}
    foreach field $fields  {
        set current_value($field) ""
    }
    set current_zone {}
}

#store the zones information in a hiddend field of the problem data
#then this data is saved with the model without do nothing more
proc LossesModel::FillTclDataFromProblemData { } {
    variable data
    variable zones
    
    if { [catch {set x [GiD_AccessValue get gendata LossesData]} msg] } {
        #reading a model created by an old version of the problemtype this gendata doesn't exists
        return
    }
    array set data $x
    
    set zones [list]
    foreach item [array names data *,start] {
        lappend zones [string range $item 0 end-6]
    }
    set zones [lsort -dictionary $zones]
    LossesModel::SetCurrentZone [lindex $zones 0]
}

proc LossesModel::FillProblemDataFromTclData { } {
    variable data
    variable zones
    set x [array get data]
    GiD_AccessValue set gendata LossesData $x
}


proc LossesModel::WriteCalculationFile { } {
    variable data
    set res ""       
    
    set lossmodel [GiD_AccessValue get gendata Zonal_losses_model]
    set i 0
    append res "Infiltration Zones - Zonal losses assign"  \n
    if { $lossmodel == "Lineal_" } {
        append res "1" \n
    } elseif { $lossmodel == "SCS_" }  {
        append res "2"
        append res " "
        append res [GiD_AccessValue get gendata Ia_coefficient] \n
    } elseif { $lossmodel == "SCS_continuous" }  {
        append res "5"
        append res " "
        append res [GiD_AccessValue get gendata Ia_coefficient]
        append res " "
        append res [GiD_AccessValue get gendata B_depletion]
        append res " "
        append res [GiD_AccessValue get gendata Initial_S_coef] \n
    } elseif { $lossmodel == "Horton_" }  {
        append res "3" \n
    } elseif { $lossmodel == "Green_Ampt_" }  {
        append res "4" \n
    }  
    append res [llength [LossesModel::GetZones]] \n
    foreach zone [LossesModel::GetZones] {
        incr i
        append res $zone \n        
        append res $i  \n
        if { $lossmodel == "Lineal_" } {
            append res [lindex $data($zone,LIa)]
            append res " "
            append res [lindex $data($zone,LFi)] \n   
        } elseif { $lossmodel == "SCS_" }  {
            append res [lindex $data($zone,SCSCN)] \n
        } elseif { $lossmodel == "SCS_continuous" }  {
            append res [lindex $data($zone,SCSCN)] \n
        } elseif { $lossmodel == "Horton_" }  {
            append res [lindex $data($zone,Hf0)]
            append res " "
            append res [lindex $data($zone,Hfc)]
            append res " "
            append res [lindex $data($zone,HK)] \n
        } elseif { $lossmodel == "Green_Ampt_" }  {
            append res [lindex $data($zone,GASuction)]
            append res " "
            append res [lindex $data($zone,GATotPoros)]
            append res " "
            append res [lindex $data($zone,GAKs)]
            append res " "
            append res [lindex $data($zone,GAInitialLoss)]
            append res " "
            append res [lindex $data($zone,GASoilDepth)] \n
        }      
    }
    # Hf0 Hfc HK SCSCN LIa LFi GASuction GATotPoros GAInitialSat GAKs GAInitialLoss GASoilDepth
    append res "Element asignation"
    return $res   
}

proc LossesModel::WriteInfZoneNumber { name } {
    variable number
    
    set number [expr [lsearch [LossesModel::GetZones] $name]+1]
    
    return $number    
}

proc LossesModel::GetZones { } {
    variable zones
    if { ![info exists zones] } {
        return ""
    }
    return $zones
}

proc LossesModel::GetCurrentZone { } {
    variable current_zone
    return $current_zone
}

proc LossesModel::OnChangeSelectedZone { cb } {   
    LossesModel::SetCurrentZone [LossesModel::GetCurrentZone]
}

proc LossesModel::SetCurrentZone { zone } {
    variable data
    variable current_zone
    variable current_value
    variable fields   
    #fill in the current_value variables with data
    if { $zone != "" } {
        foreach field $fields {
            if { ![info exists data($zone,$field)] } {
                set data($zone,$field) 0
            }
            set current_value($field) $data($zone,$field)           
        }
    } else {
        foreach field $fields {
            set current_value($field) ""
        }
    }
    set current_zone $zone  
}

proc LossesModel::GetIndex { zone } {
    variable zones    
    return [lsearch $zones $zone]
}

proc LossesModel::Exists { zone } {    
    if { [LossesModel::GetIndex $zone] != -1 } {
        set exists 1
    } else {
        set exists 0
    }
    return $exists
}

proc LossesModel::GetZoneAutomaticName { } {
    set basename [_ "Infiltration zone"]
    set i 1
    set zone $basename-$i
    while { [LossesModel::Exists $zone] } {        
        incr i
        set zone $basename-$i        
    }
    return $zone
}

proc LossesModel::NewZone { cb } {
    variable zones
    variable data
    variable fields 
    variable fields_defaults
    set zone [LossesModel::GetZoneAutomaticName]     
    
    if { [LossesModel::Exists $zone] } {
        #already exists
        return 1
    }
    foreach field $fields value $fields_defaults {
        set data($zone,$field) $value
    }
    lappend zones $zone   
    if { [winfo exists $cb] } {
        $cb configure -values [LossesModel::GetZones]
    }
    LossesModel::SetCurrentZone $zone
    return 0
}

proc LossesModel::DeleteZone { cb } {
    variable zones
    variable data
    variable fields 
    variable fields_defaults
    set zone [LossesModel::GetCurrentZone] 
    if { ![LossesModel::Exists $zone] } {
        #not exists
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete '%s'" $zone] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {     
        set i [LossesModel::GetIndex $zone] 
        array unset data $zone,*        
        set zones [lreplace $zones $i $i]       
        if { [winfo exists $cb] } {
            $cb configure -values [LossesModel::GetZones]
        }
        LossesModel::SetCurrentZone [lindex $zones 0]  
        GiD_Redraw     
    }
    return 0
}

proc LossesModel::RenameZone { cb } {
    variable data
    variable zones
    variable fields
    set zone [LossesModel::GetCurrentZone]
    set num [LossesModel::WriteInfZoneNumber $zone]        
    if { ![LossesModel::Exists $zone] } {
        #not exists
        return 1
    } 
    set new_text [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $zone] \
            [= "Enter new name of %s '%s'" [= "zone"] $zone] gidquestionhead "any" ""]
    set new_name "ID$num - $new_text"
    if { $new_name != "--CANCEL--" } {
        foreach field $fields {
            set data($new_name,$field) $data($zone,$field)
        }
        array unset data $zone,*
        set i [LossesModel::GetIndex $zone] 
        lset zones $i $new_name
        if { [winfo exists $cb] } {
            $cb configure -values [LossesModel::GetZones]           
        }
        LossesModel::SetCurrentZone $new_name       
        GiD_Redraw 
    }
    return 0
}

#apply the values of the current window
proc LossesModel::Apply { } {
    variable data
    variable current_value
    variable fields
    
    set zone [LossesModel::GetCurrentZone]    
    foreach field $fields {
        set data($zone,$field) $current_value($field)
    }
    
    GiD_Redraw        
}

#not LossesModel::StartDraw and LossesModel::EndDraw are not used, the Draw button is hidden (because unregister fail in GiD 11.1.5d)
#and the redraw procedure is registered when loading the problemtype, and each zone is draw depending on its 'visible' variable value
proc LossesModel::StartDraw { bdraw} {
    variable drawopengl
    set drawopengl [GiD_OpenGL register LossesModel::ReDraw]
    GiD_Redraw
    $bdraw configure -text [= "Finish draw"] -command [list LossesModel::EndDraw $bdraw]
}

proc LossesModel::EndDraw { bdraw } {
    variable drawopengl
    if { [info exists drawopengl] } { 
        #unregister the callback tcl procedure to be called by GiD when redrawing
        GiD_OpenGL unregister $drawopengl
        catch {unset drawopengl}
        GiD_Redraw
        if { $bdraw != "" } {
            $bdraw configure -text [= "Draw"] -command [list LossesModel::StartDraw $bdraw]
        }
    }
}

proc LossesModel::ReDraw { } {
    foreach zone [LossesModel::GetZones] {
        LossesModel::Draw $zone
    }
}

proc LossesModel::Draw { zone } {
    
}

proc LossesModel::DestroyZoneWindow { W w } {   
    if { $W == $w } {
        LossesModel::EndDraw ""
        LossesModel::FillProblemDataFromTclData             
    }
}

proc LossesModel::OnChangeType { f } {
    variable current_value
    if { $current_value(type) == 3 } {       
        grid $f.lHf0 $f.eHf0
        grid $f.lHfc $f.eHfc
        grid $f.lHK $f.eHK
        grid remove $f.lSCSCN $f.eSCSCN
        grid remove $f.lLIa $f.eLIa
        grid remove $f.lLFi $f.eLFi
        grid remove $f.lGASuction $f.eGASuction
        grid remove $f.lGATotPoros $f.eGATotPoros
        grid remove $f.lGAInitialSat $f.eGAInitialSat
        grid remove $f.lGAKs $f.eGAKs 
        grid remove $f.lGAInitialLoss $f.eGAInitialLoss
        grid remove $f.lGASoilDepth $f.eGASoilDepth        
    } elseif { $current_value(type) == 2 } {
        grid remove $f.lHf0 $f.eHf0
        grid remove $f.lHfc $f.eHfc
        grid remove $f.lHK $f.eHK
        grid $f.lSCSCN $f.eSCSCN
        grid remove $f.lLIa $f.eLIa
        grid remove $f.lLFi $f.eLFi
        grid remove $f.lGASuction $f.eGASuction
        grid remove $f.lGATotPoros $f.eGATotPoros
        grid remove $f.lGAInitialSat $f.eGAInitialSat
        grid remove $f.lGAKs $f.eGAKs 
        grid remove $f.lGAInitialLoss $f.eGAInitialLoss
        grid remove $f.lGASoilDepth $f.eGASoilDepth        
    } elseif { $current_value(type) == 1 } {
        grid remove $f.lHf0 $f.eHf0
        grid remove $f.lHfc $f.eHfc
        grid remove $f.lHK $f.eHK
        grid remove $f.lSCSCN $f.eSCSCN
        grid $f.lLIa $f.eLIa
        grid $f.lLFi $f.eLFi
        grid remove $f.lGASuction $f.eGASuction
        grid remove $f.lGATotPoros $f.eGATotPoros
        grid remove $f.lGAInitialSat $f.eGAInitialSat
        grid remove $f.lGAKs $f.eGAKs 
        grid remove $f.lGAInitialLoss $f.eGAInitialLoss
        grid remove $f.lGASoilDepth $f.eGASoilDepth        
    } elseif { $current_value(type) == 4 } {
        grid remove $f.lHf0 $f.eHf0
        grid remove $f.lHfc $f.eHfc
        grid remove $f.lHK $f.eHK
        grid remove $f.lSCSCN $f.eSCSCN
        grid remove $f.lLIa $f.eLIa
        grid remove $f.lLFi $f.eLFi
        grid $f.lGASuction $f.eGASuction
        grid $f.lGATotPoros $f.eGATotPoros
        grid $f.lGAInitialSat $f.eGAInitialSat
        grid $f.lGAKs $f.eGAKs 
        grid $f.lGAInitialLoss $f.eGAInitialLoss
        grid $f.lGASoilDepth $f.eGASoilDepth        
    }
    # Hf0 Hfc HK SCSCN LIa LFi GASuction GATotPoros GAInitialSat GAKs GAInitialLoss GASoilDepth
}

proc LossesModel::PickPointInSurfaceOrElementCmd { entry } {
    set view_mode [GiD_Info Project ViewMode]
    if { $view_mode == "GEOMETRYUSE" } {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an surface"] PointInSurface GEOMETRYUSE]
        #set surface_id $::GidPriv(selection)  
    } elseif { $view_mode == "MESHUSE"} {
        set xyz [GidUtils::GetCoordinates [_ "Pick point into an element"] PointInElement MESHUSE]
        #set element_id $::GidPriv(selection)    
    } else {
        set xyz ""
    }        
    if { $xyz != "" } {                    
        set res [format "%g %g %g" {*}$xyz]
        $entry delete 0 end
        $entry insert end $res        
    }
}

proc LossesModel::ZonaDistModelWindow { } {
    variable data
    variable zones
    variable current_zone
    variable current_value
    
    if { ![info exists zones] } {
        LossesModel::SetDefaultValues
    }
    
    LossesModel::FillTclDataFromProblemData    
    
    set w .gid.zone
    InitWindow $w [= "Infiltration zones definition"] PreZoneWindowGeom LossesModel::ZonaDistModelWindow
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    set f [ttk::frame $w.fzones]
    
    ttk::combobox $f.cb1 -textvariable LossesModel::current_zone -values [LossesModel::GetZones] -state readonly
    bind $f.cb1 <<ComboboxSelected>> [list LossesModel::OnChangeSelectedZone %W]
    ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list LossesModel::NewZone $f.cb1]
    GidHelp $f.bnew  [= "Create a new zone"]
    ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list LossesModel::DeleteZone $f.cb1]
    GidHelp $f.bdel  [= "Delete a zone"]
    ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list LossesModel::RenameZone $f.cb1]
    GidHelp $w.f.bren  [= "Rename a zone"]   
    grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
    grid $f -sticky new
    grid rowconfigure $f 0 -weight 1
    
    set f [ttk::frame $w.fzone]        
    #set field visible
    #ttk::checkbutton $f.ch$field -text [= "Visible"] -variable LossesModel::current_value($field)
    #grid $f.ch$field -sticky w
    
    ttk::label $w.fzone.title_text -text [= "Choose infiltration model"]
    grid $w.fzone.title_text -sticky w -ipady 5
    
    foreach field {type} text [list [= "Infiltration model"]] values {{1 2 3 4}} labels [list [list [= "Lineal"] [= "SCS"] [= "Horton"] [= "Green&Ampt"]]] {
        ttk::label $f.l$field -text $text
        #ttk::combobox $f.c$field -values $values -state readonly -textvariable LossesModel::current_value($field)
        TTKComboBox $f.c$field -values $values -labels $labels -state readonly -textvariable LossesModel::current_value($field) \
            -modifycmd [list LossesModel::OnChangeType $f]
        grid $f.l$field $f.c$field -sticky e -ipadx 0
    }
    
    ttk::label $w.fzone.mid_text -text [= "Introduce model parameters"]
    grid $w.fzone.mid_text -sticky w -ipady 5
    
    foreach field {Hf0 Hfc HK SCSCN LIa LFi GASuction GATotPoros GAInitialSat GAKs GAInitialLoss GASoilDepth} text [list [= "f0 (mm/h)"] [= "fc (mm/h)"] [= "K"] [= "CN"] [= "Ia (mm)"] [= "Fi (mm/h)"] [= "Suction (mm)"] [= "Total porosity"] [= "Initial saturation"] [= "Ks (mm/h)"] [= "Initial loss (mm)"] [= "Soil depth (m)"]] {
        ttk::label $f.l$field -text $text
        ttk::entry $f.e$field -textvariable LossesModel::current_value($field)
        grid $f.l$field $f.e$field -sticky w 
        GidHelp $f.eHf0  [= "Maximum initial infiltration losses index."] 
        GidHelp $f.eHfc  [= "Minimum infiltration losses index, applied once the soil is fully saturated."] 
        GidHelp $f.eHK  [= "Decay coefficient that defines the temporal variation of the infiltration rate."] 
        GidHelp $f.eSCSCN  [= "Curve number, depending on the type of soil, land uses and terrain slopes."] 
        GidHelp $f.eLIa  [= "Initial losses, they can be satisfied with the initial rain or with the water draft on the ground surface."] 
        GidHelp $f.eLFi  [= "Potential infiltration, assumed constant over time."] 
        GidHelp $f.eGASuction  [= "Suction in unsaturated soil layer."] 
        GidHelp $f.eGATotPoros  [= "Total soil porosity."] 
        GidHelp $f.eGAInitialSat  [= "Initial saturation of soil layer (0-1)."] 
        GidHelp $f.eGAKs  [= "Saturated soil permeability in the vertical direction."] 
        GidHelp $f.eGAInitialLoss  [= "Initial infiltration losses, can be satisfied by initial precipitation or by surface runoff from the ground before precipitation begins."]
        GidHelp $f.eGASoilDepth [= "Depth of the soil layer."]
    }
    
    grid $f -sticky new
    grid columnconfigure $f {1} -weight 1
    
    
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.bapply -text [= "Apply"] -command [list LossesModel::Apply] -style BottomFrame.TButton
    #ttk::button $f.bdraw -text [= "Draw"] -command [list LossesModel::StartDraw $f.bdraw] -style BottomFrame.TButton
    ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    #grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
    grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }        
    
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    bind $w <Alt-c> "$f.bclose invoke"
    bind $w <Escape> "$f.bclose invoke"   
    bind $w <Destroy> [list +LossesModel::DestroyZoneWindow %W $w] ;# + to add to previous script  
}

proc LossesModel::CloseWindow { } {
    set w .gid.zone    
    if { [winfo exists $w] } {
        destroy $w
    }
}




proc Iber::AssignInfiltrationZoneWin { questions } {
    set filenames_and_questions [list]
    foreach question $questions {
        set filename_raster [Browser-ramR file read .gid [= "Read ASCII RASTER file to assign Infiltration Zone ID"] \
                {} {{{} {.txt}} {{} {.asc}} {{All files} {.*}}} ]
        if {$filename_raster != "" } {
            lappend filenames_and_questions $filename_raster $question
        } else {
            return 1
        }
    }
    
    if { [llength $filenames_and_questions] } {        
        GidUtils::WaitState
        set tstart [clock milliseconds]
        set show_advance_bar 1
        set fail [Iber::AssignInfiltrationZoneDo $filenames_and_questions $show_advance_bar]
        set tend [clock milliseconds]
        GidUtils::EndWaitState        
        GidUtils::SetWarnLine [= "Infiltration Zone assigned to elements. time %s seconds" [expr ($tend-$tstart)/1000.0]]
        #ask the user if want to draw it
        set answer [MessageBoxOptionsButtons [= "Infiltration Zone assigned"] [= "Infiltration Zone was succesfully assigned to mesh elements. Do you want to draw it?"] [list yes no] [list [_ "Yes"] [_ "No"]] question ""]
        if {$answer == "yes"} {
            #ensure mesh view mode
            MeshView 0
            set i 0
            set n [llength $questions]
            GiD_Process Mescape Data Conditions DrawCond -ByColor- Infiltration_Zones $question
            if { $i < [expr $n-1] } {
                set answer [MessageBoxOptionsButtons [= "Draw erosion zones" $question] [= "Press continue to draw next Erosion Zone"] "" [list continue cancel] [list [_ "Continue"] [_ "Cancel"]] question]
                if { $answer == "cancel" } {
                    after 1000 GiD_Process escape escape escape
                    break
                }
            }
            incr i
            
        }
    }
    return 0
}


#to assign to mesh elements the Infiltration Zone (GiD condition Infiltration_Zones with values from a list of ARC/Info .asc files, one by question to be set)
# e.g.
# 
proc Iber::AssignInfiltrationZoneDo { filenames_and_questions {show_advance_bar 1} } {
    set fail 0
    variable percent 0
    variable stop 0
    
    #assign percent of four steps to 10 10 40 40
    set condition_name "Infiltration_Zones"
    set default_values [GidUtils::GetConditionDefaultValues $condition_name] ;#default values 
    if { $show_advance_bar } {
        GidUtils::CreateAdvanceBar [_ "Assign Erosion Zone values"] [_ "Percentage"]: ::Iber::percent 100  {set ::Iber::stop 1}
    }
    set percent 0
    set nodata_value -9999 ;#assumed same value for all files
    set element_ids [Iber::GetElementIdsLayersNoFrozen]
    set num_elements [objarray length $element_ids]
    set xy_element_centers [Iber::GetElementCenters $element_ids]    
    set percent 10
    #initialize default values (except fixed model) for all elements
    for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
        set element_id [objarray get $element_ids $i_element]
        set element_values($element_id) $default_values        
    }    
    set n_raster [expr [llength $filenames_and_questions]/2]
    set n_refresh 1
    #fill a field by each raster
    set i_raster 0
    foreach {filename_raster condition_question} $filenames_and_questions {
        if { $show_advance_bar } {
            if { ![expr {$i_raster%$n_refresh}] } {
                set percent [expr int(double($i_raster)/$n_raster*100*0.4+20)]
            }
        }
        if { $stop } {
            set fail -1
            break
        }  
        set condition_question_index [GidUtils::GetConditionQuestionIndex $condition_name "infzone_id"]
        set values $default_values
        set raster_interpolation [GDAL::ReadRaster $filename_raster 0]
        set nodata_value [GDAL::GetNoDataValue $filename_raster]
        #set interpolated_values [GiD_Raster interpolate $raster_interpolation [list nodes $xy_element_centers]]
        #-closest in this case also, because GiD_AssignData become a bottleneck in case of a each element a different value (and big strings), better use only a few discrete values
        set interpolated_values [GiD_Raster interpolate -closest $raster_interpolation [list nodes $xy_element_centers]]        
        for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
            set element_id [objarray get $element_ids $i_element]
            set value [objarray get $interpolated_values $i_element]
            lset element_values($element_id) $condition_question_index $value
        }
        incr i_raster
    }    
    #assign the final values
    #GiD_AssignData condition of a condition canrepeat=no an big mesh is a huge bottleneck 
    #(because it is moving every call the whole container to a temporary container sorted by it)
    #in future versions (>14.1.8d) we want to enhance the code storing entityvalue ordered by entity id and avoid the bottleneck
    #
    #to minimize GiD_AssignData calls join in a single assign the elements with same values
    for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
        set element_id [objarray get $element_ids $i_element]
        set values $element_values($element_id)
        lappend elements_same_value($values) $element_id
    }
    unset element_ids
    array unset element_values
    set list_values [array names elements_same_value]
    set num_different_values [llength $list_values]
    set n_refresh_different_values [expr int($num_different_values/4)]
    if { $n_refresh_different_values<1 } {
        set n_refresh_different_values 1
    }
    set i_different_values 0
    foreach values $list_values {
        if { $values != $nodata_value } {
            if { $show_advance_bar } {
                if { ![expr {$i_different_values%$n_refresh_different_values}] } {
                    set percent [expr int(double($i_different_values)/$num_different_values*100*0.4+60)]
                }
            }
            GiD_AssignData condition $condition_name body_elements $values $elements_same_value($values)
            incr i_different_values
        }
    }        
    if { $show_advance_bar } {
        GidUtils::DeleteAdvanceBar
    }
    return $fail
}




# for model_value and questions see the definitio of the condition Losses_Model in IBER.cnd
# e.g.
# Iber::AssignLossesModelWin SCS [list CN]
# Iber::AssignLossesModelWin Lineal_model [list Ia Fi]
#
proc Iber::AssignLossesModelWin { model_value questions } {
    set filenames_and_questions [list]
    foreach question $questions {
        set filename_raster [Browser-ramR file read .gid [= "Read ASCII RASTER file to assign losses model %s parameter %s" $model_value $question] \
                {} {{{} {.txt}} {{} {.asc}} {{All files} {.*}}} ]
        if {$filename_raster != "" } {
            lappend filenames_and_questions $filename_raster $question
        } else {
            return 1
        }
    }
    
    if { [llength $filenames_and_questions] } {        
        GidUtils::WaitState
        set tstart [clock milliseconds]
        set show_advance_bar 1
        set fail [Iber::AssignLossesModelDo $model_value $filenames_and_questions $show_advance_bar]
        set tend [clock milliseconds]
        GidUtils::EndWaitState        
        GidUtils::SetWarnLine [= "Losses model %s assigned to elements. time %s seconds" $model_value [expr ($tend-$tstart)/1000.0]]
        #ask the user if want to draw it
        set answer [MessageBoxOptionsButtons [= "Losses model assigned"] [= "Losses model was succesfully assigned to mesh elements. Do you want to draw it?"] [list yes no] [list [_ "Yes"] [_ "No"]] question ""]
        if {$answer == "yes"} {
            #ensure mesh view mode
            MeshView 0
            set i 0
            set n [llength $questions]
            foreach question $questions {
                GiD_Process Mescape Data Conditions DrawCond -ByColor- $model_value $question
                if { $i < [expr $n-1] } {
                    set answer [MessageBoxOptionsButtons [= "Draw losses model %s" $question] [= "Press continue to draw next parameter"] [list continue cancel] [list [_ "Continue"] [_ "Cancel"]] question ""]
                    if { $answer == "cancel" } {
                        after 1000 GiD_Process escape escape escape
                        break
                    }
                }
                incr i
            }
        }
    }
    return 0
}


#to assign to mesh elements the losses (GiD condition SCS/Lineal_model/G&A with values from a list of ARC/Info .asc files, one by question to be set)
# e.g.
# Iber::AssignLossesModelDo SCS [list $filename_raster_cn CN]
# Iber::AssignLossesModelDo Lineal_model [list $filename_raster_ia Ia $filename_raster_fi Fi]
# 
proc Iber::AssignLossesModelDo { model_value filenames_and_questions {show_advance_bar 1} } {
    set fail 0
    variable percent 0
    variable stop 0
    
    #assign percent of four steps to 10 10 40 40
    
    set condition_name $model_value
    set default_values [GidUtils::GetConditionDefaultValues $condition_name] ;#default values    
    if { $show_advance_bar } {
        GidUtils::CreateAdvanceBar [_ "Assign losses model values"] [_ "Percentage"]: ::Iber::percent 100  {set ::Iber::stop 1}
    }
    set percent 0    
    set nodata_value -9999 ;#assumed same value for all files
    set element_ids [Iber::GetElementIdsLayersNoFrozen]
    set num_elements [objarray length $element_ids]
    set xy_element_centers [Iber::GetElementCenters $element_ids]    
    set percent 10
    #initialize default values (except fixed model) for all elements
    for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
        set element_id [objarray get $element_ids $i_element]
        set element_values($element_id) $default_values        
    }    
    set n_raster [expr [llength $filenames_and_questions]/2]
    set n_refresh 1
    #fill a field by each raster
    set i_raster 0
    foreach {filename_raster condition_question} $filenames_and_questions {
        if { $show_advance_bar } {
            if { ![expr {$i_raster%$n_refresh}] } {
                set percent [expr int(double($i_raster)/$n_raster*100*0.4+20)]
            }
        }
        if { $stop } {
            set fail -1
            break
        }  
        set condition_question_index [GidUtils::GetConditionQuestionIndex $condition_name $condition_question]
        set values $default_values
        set raster_interpolation [GDAL::ReadRaster $filename_raster 0]
        set nodata_value [GDAL::GetNoDataValue $filename_raster]
        #set interpolated_values [GiD_Raster interpolate $raster_interpolation [list nodes $xy_element_centers]]
        #-closest in this case also, because GiD_AssignData become a bottleneck in case of a each element a different value (and big strings), better use only a few discrete values
        set interpolated_values [GiD_Raster interpolate -closest $raster_interpolation [list nodes $xy_element_centers]]        
        for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
            set element_id [objarray get $element_ids $i_element]
            set value [objarray get $interpolated_values $i_element]
            lset element_values($element_id) $condition_question_index $value
        }
        incr i_raster
    }    
    #assign the final values
    #GiD_AssignData condition of a condition canrepeat=no an big mesh is a huge bottleneck 
    #(because it is moving every call the whole container to a temporary container sorted by it)
    #in future versions (>14.1.8d) we want to enhance the code storing entityvalue ordered by entity id and avoid the bottleneck
    #
    #to minimize GiD_AssignData calls join in a single assign the elements with same values
    for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
        set element_id [objarray get $element_ids $i_element]
        set values $element_values($element_id)
        lappend elements_same_value($values) $element_id
    }
    unset element_ids
    array unset element_values
    set list_values [array names elements_same_value]
    set num_different_values [llength $list_values]
    set n_refresh_different_values [expr int($num_different_values/4)]
    if { $n_refresh_different_values<1 } {
        set n_refresh_different_values 1
    }
    set i_different_values 0
    foreach values $list_values {
        if { $values != $nodata_value } {
            if { $show_advance_bar } {
                if { ![expr {$i_different_values%$n_refresh_different_values}] } {
                    set percent [expr int(double($i_different_values)/$num_different_values*100*0.4+60)]
                }
            }
            GiD_AssignData condition $condition_name body_elements $values $elements_same_value($values)
            incr i_different_values
        }
    }
    if { $show_advance_bar } {
        GidUtils::DeleteAdvanceBar
    }
    return $fail
}
