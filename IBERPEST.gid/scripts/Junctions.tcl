# Devuelve los puntos con codici�n de nodo
#jaragonh 10/2016

proc Nodos1 {} {
    set nodos [GiD_Info conditions Junctions mesh]
    set long [llength $nodos]  
    set numnodos $long
    
    return $numnodos
}

# Propiedades de los nodos (nodos.dat)
proc Nodos2 {cont} {    
    set nodos [GiD_Info conditions Junctions mesh]
    set prueba [GiD_Info conditions Grate_inlets mesh]
    set numnodos [llength $nodos] 
    
    set nodo [lindex $nodos $cont]
    set idnodo [lrange $nodo 1 1]
    set telev [lrange $nodo 3 3]
    set belev [lrange $nodo 4 4]
    set hnodo [lrange $nodo 5 5]
    set diam [lrange $nodo 6 6] 
    set imet1 [lrange $nodo 7 7]  
    set imet 1
    if {$imet1=="2D"} {set imet 2}  
    set npd 0  
    set arehid [lrange $nodo 9 9]
    if {$arehid=="1"} {
        set dhid [lrange $nodo 10 10]
        set hid [lindex $dhid 0]
        set nd [lrange $hid 1 1]
        set npd [expr $nd/2] 
    }
    
    set lista [format "%-s %12.3f %12.3f %12.3f %12.3f %8s %8s" $idnodo $telev $belev $hnodo $diam $npd $imet]
    return $lista
}

#Id de nodo y no de lineas del hidrograma
proc Nodos3 {cont ifila} {    
    set nodos [GiD_Info conditions Junctions mesh]
    set nodo [lindex $nodos $cont]
    if {$ifila==0} {
        set idnodo [lrange $nodo 1 1]        
        set lista [format "%-s" $idnodo]
    } else {
        set dhid [lrange $nodo 10 10]
        set hid [lindex $dhid 0]
        set nd [lrange $hid 1 1]
        set npd [expr $nd/2]        
        set lista [format "%-s" $npd]
    }      
    return $lista
}

#hidrograma en los nodos
proc Nodos4 {cont ipd} {    
    set nodos [GiD_Info conditions Junctions mesh]
    set nodo [lindex $nodos $cont]
    set dhid [lrange $nodo 10 10]
    set hid [lindex $dhid 0]
    set i [expr 2*$ipd]       
    set t [lrange $hid $i $i]
    set q [lrange $hid $i+1 $i+1]     
    set lista [format "%12.3f %12.3f" $t $q]
    return $lista
}
