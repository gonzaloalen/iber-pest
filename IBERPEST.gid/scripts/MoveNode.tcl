#################################
### updated by Marcos 10/2021 ###
#################################

namespace eval MoveNode {
}

proc LIDARimport { fileDTMname } {
    global MOVENODEfileD
    global MOVENODEfileDaux
    global MOVENODEfile  
    global MOVENODEfileDir
    
    set MOVENODEfileDaux $fileDTMname
    MoveNode::LeeGrid
}

proc MoveNode::LeeGrid { } {
    global MOVENODEfileD
    global MOVENODEfileDaux
    variable gridfilename
    
    if { ![info exists gridfilename] } {
        set gridfilename ""
    }
    if { [GiD_Info Mesh] == 0 } {
        tk_dialogRAM .gid.tmpwin [_ "Set elevation info"] \
            [_ "There is no mesh. Please generate a mesh before editing it."] \
            info 0 [_ "OK"]
        return
    }
    
    set title [= "Read raster file"]    
    set types [list [list [= "Raster formats"] {.adf .tif .png .gif .jpg .bmp .txt .dat .asc .N1 .sid}] [list [_ "All files"] {.*}]]
    set defaultextension ""
    
    if { ![info exists MOVENODEfileDaux] || $MOVENODEfileDaux == "" } {
        MoveNodeWin
    } else { 
        MOVENODEselfile
        MoveNodeWin
    }
}

proc CreateMOVENODE { w } {
    global MOVENODEfileD
    global gridfilename        
    
    #destroy $w                                
    
    set gridfilename $MOVENODEfileD        
    if { $gridfilename != "" } {
        GidUtils::DisableGraphics            
        GidUtils::WaitState .gid
        if { [catch { set fail [MoveNode::SetElevationFromFile $gridfilename $::GidPriv(MoveNodesToZRasterPreserveZ)] } err] } {
            set fail 1            
            WarnWinText "Error $err"
        }
        GidUtils::EnableGraphics
        GidUtils::EndWaitState .gid
        if { $fail } {
            if { $fail == -1 } {
                GidUtils::SetWarnLine [= "User stop while setting elevation from file '%s'" $gridfilename]
            } else {
                GidUtils::SetWarnLine [= "Error setting elevation from file '%s'" $gridfilename]
            }
        } else {
            GidUtils::SetWarnLine [= "Elevation set from file '%s'" $gridfilename]
            # GiD_Process 'Zoom Frame
            unset gridfilename
            #unset MOVENODEfileD
            GiD_Redraw
        }
    }        
}


proc MoveNodeWin { } {
    global MOVENODEfileD
    global MOVENODEfile
        
    set w .gid.createmovenode
    if { ![info exists MOVENODEfileD] } {
        set MOVENODEfile " "
    }
    set ::GidPriv(MoveNodesToZRasterPreserveZ) 0
        
    InitWindow $w [= "Set elevation from file"] PreMoveNodeWindowGeom MoveNodeWin
    
    #creacion de los witgets
    set f [ttk::frame $w.frmParams]
    
    #Label 0
    ttk::label $f.ldir -text [= "Raster File: "]
    ttk::entry $f.edir -textvariable MOVENODEfile
    ttk::button $f.bdir -text [= "Retrieve"] -command MOVENODEselfile
    grid $f.ldir $f.edir $f.bdir -sticky w
    grid columnconfigure $w.frmParams {0 1 2} -weight 1
    grid $w.frmParams -sticky new -padx 2 -pady 2
    
    #Label 1
    ttk::labelframe $w.frmType -text [= " Elevation options "]
    ttk::radiobutton $w.frmType.eext -text [= "All nodes"] -value 0 -variable ::GidPriv(MoveNodesToZRasterPreserveZ)
    ttk::radiobutton $w.frmType.erai -text [= "Only Nodes below"] -value 1 -variable ::GidPriv(MoveNodesToZRasterPreserveZ)
    ttk::radiobutton $w.frmType.elow -text [= "Only Nodes above"] -value 2 -variable ::GidPriv(MoveNodesToZRasterPreserveZ)
    grid $w.frmType.eext -sticky ew
    grid $w.frmType.erai -sticky ew
    grid $w.frmType.elow -sticky ew
    grid columnconfigure $w.frmType {0 1 2} -weight 1
    grid $w.frmType -sticky new -padx 5 -pady 5
    
    #Buttons settings
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.baccept -text [= "Execute"] -command "CreateMOVENODE $w" -underline 0 -width 10
    ttk::button $f.bclose -text [= "Close"] -command "destroy $w" -underline 0 -width 10
    grid $f.baccept $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center } 
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
}


proc MOVENODEselfile { } {
    global MOVENODEfileD  
    global MOVENODEfileDaux
    global MOVENODEfile  
    global MOVENODEfileDir
    
    if { ![info exists MOVENODEfileDaux] || $MOVENODEfileDaux == "" } {
        set types [list [list [= "Raster formats"] {.adf .tif .png .gif .jpg .bmp .txt .dat .asc .N1 .sid}] [list [_ "All files"] {.*}]]
        set defaultextension .txt
        if { ![info exists MOVENODEfileD] } {
            set MOVENODEfileD ""
        }
        set MOVENODEfileD [Browser-ramR file read .gid [= "Choose Original DTM file"] $MOVENODEfileD $types $defaultextension 0]      
        set MOVENODEfile [file tail $MOVENODEfileD]
        set MOVENODEfileDir [file dirname $MOVENODEfileD]
    } else {
        set MOVENODEfileD $MOVENODEfileDaux
        set MOVENODEfile [file tail $MOVENODEfileD]
        set MOVENODEfileDir [file dirname $MOVENODEfileD]
        set MOVENODEfileDaux ""
    }
}


proc MoveNode::SetElevationConstantWin { } {
    variable elevation
    if { ![info exists elevation] } {
        set elevation 0.0
    }
    set z [tk_dialogEntryRAM .gid.elevation [= "Set elevation"] [= "Enter new elevation"] gidquestionhead real $elevation ""]
    if { $z == "--CANCEL--" } {
        return 1
    } else {
        set elevation $z 
        set selection [GidUtils::PickEntities Nodes multiple]
        if { $selection != "" } {
            set selection [GidUtils::UnCompactNumberList $selection]
            MoveNode::SetElevationConstant $z $selection
            GidUtils::SetWarnLine [= "Elevation z=%s set to %s nodes" $elevation [llength $selection]]
            GiD_Redraw
        }
        MoveNode::SetElevationConstantWin
    }    
}

proc MoveNode::SetElevationConstant { z selection } {
    foreach node_id $selection {
        set coordinates [lrange [GiD_Mesh get node $node_id] 1 end]
        lset coordinates 2 $z
        GiD_Mesh edit node $node_id $coordinates
    }
}

proc MoveNode::SetHeightConstantWin { } {
    variable elevation
    if { ![info exists elevation] } {
        set elevation 0.0
    }
    set z [tk_dialogEntryRAM .gid.elevation [= "Set offset"] [= "Enter new offset"] gidquestionhead real $elevation ""]
    if { $z == "--CANCEL--" } {
        return 1
    } else {
        set elevation $z 
        #GidUtils::SetWarnLine [= "Select nodes to set its elevation to %s" $elevation]
        set selection [GidUtils::PickEntities Nodes multiple]
        if { $selection != "" } {
            set selection [GidUtils::UnCompactNumberList $selection]
            MoveNode::SetHeightConstant $z $selection
            GidUtils::SetWarnLine [= "height h=%s set to %s nodes" $elevation [llength $selection]]
            GiD_Redraw
        }
        MoveNode::SetHeightConstantWin
    }
}

#MoveNode by height
proc MoveNode::SetHeightConstant { z selection } {
    foreach node_id $selection {
        set coordinates [lrange [GiD_Mesh get node $node_id] 1 end]
        set h [lindex $coordinates end]
        set zz [expr $z + $h]
        lset coordinates 2 $zz
        GiD_Mesh edit node $node_id $coordinates
    }
}

proc MoveNode::SetElevationFromFile { filename_raster preserve_z } {
    set node_ids [Iber::GetNodeIdsLayersNoFrozen]
    GIS::MoveNodesToZFilenameRaster $filename_raster $node_ids $preserve_z
}
