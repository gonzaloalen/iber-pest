namespace eval IberswmmRoofs {
    variable data ;#array of values
    variable tejados ;#list of names of data
    variable current_tejado ;#name of current selected tejado
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {Start Slope Width Manning State swmmroof}
    variable fields_defaults { {} 0.2 1.0 0.02 1 0.0 }  
    variable drawopengl
    variable table ;#tktable widget
}

proc IberswmmRoofs::UnsetVariables { } {
    variable data
    variable tejados      
    unset -nocomplain data
    unset -nocomplain tejados
}

proc IberswmmRoofs::SetDefaultValues { } {    
    variable data
    variable tejados
    variable current_tejado
    variable current_value
    variable fields
    variable fields_defaults
    array unset data
    set tejados {}
    foreach field $fields  {
	set current_value($field) ""
    }
    set current_tejado {}
}

#store the tejados information in a hidden field of the problem data
#then this data is saved with the model without do nothing more
proc IberswmmRoofs::FillTclDataFromProblemData { } {
    variable data
    variable tejados
    if { [catch {set x [GiD_AccessValue get gendata TejadosData]} msg] } {
	#reading a model created by an old version of the problemtype this gendata doesn't exists
	return
    }    
    array set data $x
    
    set tejados [list]
    foreach item [array names data *,Start] {
	lappend tejados [string range $item 0 end-6]
    }
    set tejados [lsort -dictionary $tejados]
    IberswmmRoofs::SetCurrentTejado [lindex $tejados 0]
}

proc IberswmmRoofs::FillProblemDataFromTclData { } {
    variable data
    variable tejados
    set x [array get data]
    GiD_AccessValue set gendata TejadosData $x
}

proc IberswmmRoofs::ExpandGIDSelection { sellist } {
    set result [list]
    foreach sel $sellist {
	if {[llength [set range [split $sel :]]]==1} {
	    lappend result $range
	} else {
	    foreach {from to} $range break
	    while {$from<$to} {
		lappend result $from
		incr from
	    }
	}
    }
    # you can also use: return $result
    set result
}

proc IberswmmRoofs::WriteCalculationFile { } {
    variable data
	variable swmmroof
	set res "" 
	
	append res [llength [IberswmmRoofs::GetTejado]] \n
    set i 0
    foreach tejado [IberswmmRoofs::GetTejado] {
		incr i
		append res "Roof $i: parameters" \n
		append res $i \n
		append res $data($tejado,Slope)  
		append res " "
		append res $data($tejado,Width)
		append res " "
		append res $data($tejado,Manning)
		append res " "
		append res $data($tejado,State)\n
    }
	append res "Number of roofs"
	
	return $res
}

proc IberswmmRoofs::WriteCalculationFile2 { } {
	
	set res [list]
	foreach item [GiD_Info conditions Roofs_properties geometry] {
		append res [lindex $item 3]
		append res " "
		append res [lindex $item 1]
		append res " "
		append res [lindex $item 4]
		append res " "
		append res [lindex $item 5]
		append res " "
		append res [lindex $item 6]
		append res " "
		if {[lindex $item 7] == "100%_connected"} {
			append res "1"
			append res " "
			append res [lindex $item 8]
			append res " "
			append res "100"
			append res " "
			append res "0"
			append res " "
			append res "0"
		} elseif {[lindex $item 7] == "Partially_connected"} {
			append res "2"
			append res " "
			append res [lindex $item 8]
			append res " "
			append res [lindex $item 9]
			append res " "
			append res [lindex $item 10]
			append res " "
			append res [lindex $item 11]
		} elseif {[lindex $item 7] == "Disconnected"} {
			append res "3"
			append res " "
			append res "Disc"
			append res " "
			append res "0"
			append res " "
			append res [lindex $item 10]
			append res " "
			append res [lindex $item 11]
		}
		append res \n
	}
	return $res
}

proc IberswmmRoofs::WriteNumberOfRoofs { } {
	set num_entities [GiD_Info conditions Roofs_properties geometry -count] 
	append num_entities
	return $num_entities
}

proc IberswmmRoofs::WriteTejadoNumber { name } {
    variable number
    
    set number [expr [lsearch [IberswmmRoofs::GetTejado] $name]+1]
    
    return $number    
}


proc IberswmmRoofs::GetTejado { } {
    variable tejados
    if { ![info exists tejados] } {
	return ""
    }
    return $tejados
}

proc IberswmmRoofs::GetCurrentTejado { } {
    variable current_tejado
    return $current_tejado
}

proc IberswmmRoofs::OnChangeSelectedTejado { cb } {   
    IberswmmRoofs::SetCurrentTejado [IberswmmRoofs::GetCurrentTejado]
}

proc IberswmmRoofs::SetCurrentTejado { tejado } {
    variable data
    variable current_tejado
    variable current_value
    variable fields   
    variable table
    #fill in the current_value variables with data
    if { $tejado != "" } {
	foreach field $fields {
	    if { ![info exists data($tejado,$field)] } {
		set data($tejado,$field) 0
	    }
	    set current_value($field) $data($tejado,$field)           
	}                
    } else {
	foreach field $fields {
	    set current_value($field) ""
	}
    }
    
    #the other widgets have a linked variable that automatically update it but the table not
    if { [info exists table] && [winfo exists $table] } {
	GridData::SetData $table $current_value(table_value)
    }
    set current_tejado $tejado  
}

proc IberswmmRoofs::GetIndex { tejado } {
    variable tejados    
    return [lsearch $tejados $tejado]
}

proc IberswmmRoofs::Exists { tejado } {    
    if { [IberswmmRoofs::GetIndex $tejado] != -1 } {
	set exists 1
    } else {
	set exists 0
    }
    return $exists
}

proc IberswmmRoofs::GetTejadoAutomaticName { } {
    set basename [_ "Tejado"]
    set i 1
    set tejado $basename-$i
    while { [IberswmmRoofs::Exists $tejado] } {        
	incr i
	set tejado $basename-$i        
    }
    return $tejado
}

proc IberswmmRoofs::NewTejado { cb } {
    variable tejados
    variable data
    variable fields 
    variable fields_defaults
    set tejado [IberswmmRoofs::GetTejadoAutomaticName]     
    
    if { [IberswmmRoofs::Exists $tejado] } {
	#already exists
	return 1
    }
    foreach field $fields value $fields_defaults {
	set data($tejado,$field) $value
    }
    lappend tejados $tejado   
    if { [winfo exists $cb] } {
	$cb configure -values [IberswmmRoofs::GetTejado]
    }
    IberswmmRoofs::SetCurrentTejado $tejado
    return 0
}

proc IberswmmRoofs::DeleteTejado { cb } {
    variable tejados
    variable data
    variable fields 
    variable fields_defaults
    set tejado [IberswmmRoofs::GetCurrentTejado] 
    if { ![IberswmmRoofs::Exists $tejado] } {
	#not exists
	return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete tejado '%s'" $tejado] \
	    gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {     
	set i [IberswmmRoofs::GetIndex $tejado] 
	array unset data $tejado,*        
	set tejados [lreplace $tejados $i $i]       
	if { [winfo exists $cb] } {
	    $cb configure -values [IberswmmRoofs::GetTejado]
	}
	IberswmmRoofs::SetCurrentTejado [lindex $tejados 0]  
	GiD_Redraw     
    }
    return 0
}

proc IberswmmRoofs::RenameTejado { cb } {
    variable data
    variable tejados
    variable fields
    set tejado [IberswmmRoofs::GetCurrentTejado]   
    if { ![IberswmmRoofs::Exists $tejado] } {
	#not exists
	return 1
    } 
    set new_name [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $tejado] \
	    [= "Enter new name of %s '%s'" [= "tejado"] $tejado] gidquestionhead "any" ""]
    if { $new_name != "--CANCEL--" } {
	foreach field $fields {
	    set data($new_name,$field) $data($tejado,$field)
	}
	array unset data $tejado,*
	set i [IberswmmRoofs::GetIndex $tejado] 
	lset tejados $i $new_name
	if { [winfo exists $cb] } {
	    $cb configure -values [IberswmmRoofs::GetTejado]           
	}
	IberswmmRoofs::SetCurrentTejado $new_name       
	GiD_Redraw 
    }
    return 0
}

#apply the values of the current window
proc IberswmmRoofs::Apply { } {
    variable data
    variable current_value
    variable fields
    
    set tejado [IberswmmRoofs::GetCurrentTejado]    
    foreach field $fields {
	set data($tejado,$field) $current_value($field)
    }
    GiD_Redraw     
}

#not IberswmmRoofs::StartDraw and IberswmmRoofs::EndDraw are not used, the Draw button is hidden (because unregister fail in GiD 11.1.5d)
#and the redraw procedure is registered when loading the problemtype, and each tejado is draw depending on its 'visible' variable value
proc IberswmmRoofs::StartDraw { bdraw} {
    variable drawopengl
    set drawopengl [GiD_OpenGL register IberswmmRoofs::ReDraw]
    GiD_Redraw
    $bdraw configure -text [= "Finish draw"] -command [list IberswmmRoofs::EndDraw $bdraw]
}

proc IberswmmRoofs::EndDraw { bdraw } {
    variable drawopengl
    if { [info exists drawopengl] } { 
	#unregister the callback tcl procedure to be called by GiD when redrawing
	GiD_OpenGL unregister $drawopengl
	catch {unset drawopengl}
	GiD_Redraw
	if { $bdraw != "" } {
	    $bdraw configure -text [= "Draw"] -command [list IberswmmRoofs::StartDraw $bdraw]
	}
    }
}

proc IberswmmRoofs::ReDraw { } {
    foreach tejado [IberswmmRoofs::GetTejado] {
	IberswmmRoofs::Draw $tejado
    }
}

proc IberswmmRoofs::Draw { tejado } {
    variable data
    
    set tejado [IberswmmRoofs::GetCurrentTejado]
    set view_mode [GiD_Info Project ViewMode]
    if { $view_mode == "GEOMETRYUSE" } {
	set Exf_surf [list]
	foreach elem [GiD_Info conditions Assign_exfiltration_area geometry] {
	    lappend Exf_surf [lindex $elem 1]
	    GiD_OpenGL drawentity -mode  filled surface $Exf_surf
	}
    } elseif { $view_mode == "MESHUSE"} {        
	set Exf_elems [list]
	foreach elem [GiD_Info conditions Assign_exfiltration_area mesh] {
	    lappend Exf_elems [lindex $elem 1]
	    GiD_OpenGL drawentity -mode filled element $Exf_elems        
	}
    }
    
    return 0
}



proc IberswmmRoofs::DestroyTejado { W w } {   
    if { $W == $w } {
	IberswmmRoofs::EndDraw ""
	IberswmmRoofs::FillProblemDataFromTclData             
    }
}

proc IberswmmRoofs::OnChangeType { f } {
    variable current_value
    
}

proc IberswmmRoofs::PickSurfaceOrElementCmd { entry } {
    variable data
    variable current_tejado
    
    set tejado [IberswmmRoofs::GetCurrentTejado]
    set view_mode [GiD_Info Project ViewMode]
    if { $view_mode == "GEOMETRYUSE" } {
	set swmmroof [GidUtils::PickEntities Surfaces multiple]
	GiD_AssignData condition Assign_exfiltration_area surfaces [IberswmmRoofs::WriteTejadoNumber $tejado] $swmmroof
	#set surface_id $::GidPriv(selection)  
    } elseif { $view_mode == "MESHUSE"} {
	set swmmroof [GidUtils::PickEntities elements multiple]
	GiD_AssignData condition Assign_exfiltration_area elements [IberswmmRoofs::WriteTejadoNumber $tejado] $swmmroof
	#set element_id $::GidPriv(selection)    
    } else {
	set swmmroof ""
    }
    
    
    if { $swmmroof != "" } {                    
	set res $swmmroof
	$entry delete 0 end
	$entry insert end $res        
    }
}


proc IberswmmRoofs::Window { } {
    variable data
    variable tejados
    variable current_tejado
    variable current_value
    variable table
    
    if { ![info exists tejados] } {
	IberswmmRoofs::SetDefaultValues
    }
    
    IberswmmRoofs::FillTclDataFromProblemData    
    
    set w .gid.tejado
    InitWindow $w [= "Roof definition"] PreTejadoWindowGeom IberswmmRoofs::Window
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    set f [ttk::frame $w.ftejados]
    
    ttk::combobox $f.cb1 -textvariable IberswmmRoofs::current_tejado -values [IberswmmRoofs::GetTejado] -state readonly
    bind $f.cb1 <<ComboboxSelected>> [list IberswmmRoofs::OnChangeSelectedTejado %W]
    ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list IberswmmRoofs::NewTejado $f.cb1]
    GidHelp $f.bnew  [= "Create a new tejado"]
    ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list IberswmmRoofs::DeleteTejado $f.cb1]
    GidHelp $f.bdel  [= "Delete a tejado"]
    ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list IberswmmRoofs::RenameTejado $f.cb1]
    GidHelp $w.f.bren  [= "Rename a tejado"]   
    grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
    grid $f -sticky new
    grid rowconfigure $f 0 -weight 1
    
    set f [ttk::frame $w.ftejado]        
    
    foreach field {Slope Width Manning State} text [list [= "Slope"] [= "Width"] [= "Manning"] [= "State"]] {
	ttk::label $f.l$field -text $text
	ttk::entry $f.e$field -textvariable IberswmmRoofs::current_value($field)
	grid $f.l$field $f.e$field -sticky ew
	GidHelp $f.eSlope  [= "Slope"]
	GidHelp $f.eWidth [= "Width"]
	GidHelp $f.eManning [= "Manning"]
	GidHelp $f.eState [= "State"]
    }
    
    
    grid $f -sticky new
    grid columnconfigure $f {1} -weight 1
    
    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.bapply -text [= "Apply"] -command [list IberswmmRoofs::Apply] -style BottomFrame.TButton
    #ttk::button $f.bdraw -text [= "Draw"] -command [list IberswmmRoofs::StartDraw $f.bdraw] -style BottomFrame.TButton
    ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    #grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
    grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }        
    
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    bind $w <Alt-c> "$f.bclose invoke"
    bind $w <Escape> "$f.bclose invoke"   
    bind $w <Destroy> [list +IberswmmRoofs::DestroyTejado %W $w] ;# + to add to previous script  
}

proc IberswmmRoofs::CloseWindow { } {
    set w .gid.tejado
    if { [winfo exists $w] } {
	close $w
    }
}

