#### created by GONZALOALEN Feb/2023
###################################################################################
namespace eval Damage {
    variable data ;#array of values
    variable damage_curves
    variable current_damage_curve
    variable table
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {raster_value table_value}
    variable fields_defaults {0 {0.0 0.0}}
}


# Funciones relacionadas con la asignacion automatica de los valores de Maximum damage
# ------------------------------------------------------------------------------
proc Damage::AssignMaxDaRasterWin { questions } {
    set filenames_and_questions [list]
    foreach question $questions {
        set filename_raster [Browser-ramR file read .gid [= "Read ASCII RASTER file to assign maximum damage"] \
                {} {{{} {.tif}} {{} {.txt}} {{} {.asc}} {{All files} {.*}}} ]
        if {$filename_raster != "" } {
            lappend filenames_and_questions $filename_raster $question
        } else {
            return 1
        }
    }

    if { [llength $filenames_and_questions] } {
        GidUtils::WaitState
        set tstart [clock milliseconds]
        set show_advance_bar 1
        set fail [Damage::AssignMaxDaRasterDo $filenames_and_questions $show_advance_bar]
        set tend [clock milliseconds]
        GidUtils::EndWaitState
        GidUtils::SetWarnLine [= "Maximum damage assigned to elements. time %s seconds" [expr ($tend-$tstart)/1000.0]]
        #ask the user if want to draw it
        set answer [MessageBoxOptionsButtons [= "Maximum damage assigned"] [= "Maximum damage was succesfully assigned to mesh elements. Do you want to draw it?"] [list yes no] [list [_ "Yes"] [_ "No"]] question ""]
        if {$answer == "yes"} {
            #ensure mesh view mode
            MeshView 0
            set i 0
            set n [llength $questions]
            foreach question $questions {
                GiD_Process Mescape Data Conditions DrawCond -ByColor- "Max_Damage" $question
            }
        }
    }
    return 0
}

proc Damage::AssignDamageCurveRasterWin { questions } {
    set filenames_and_questions [list]
    foreach question $questions {
        set filename_raster [Browser-ramR file read .gid [= "Read ASCII RASTER file to assign damage curves"] \
                {} {{{} {.tif}} {{} {.txt}} {{} {.asc}} {{All files} {.*}}} ]
        if {$filename_raster != "" } {
            lappend filenames_and_questions $filename_raster $question
        } else {
            return 1
        }
    }

    if { [llength $filenames_and_questions] } {
        GidUtils::WaitState
        set tstart [clock milliseconds]
        set show_advance_bar 1
        set fail [Damage::AssignDamageCurveRasterDo $filenames_and_questions $show_advance_bar]
        set tend [clock milliseconds]
        GidUtils::EndWaitState
        GidUtils::SetWarnLine [= "Damage curve values assigned to elements. time %s seconds" [expr ($tend-$tstart)/1000.0]]
        #ask the user if want to draw it
        set answer [MessageBoxOptionsButtons [= "Damage curve values assigned"] [= "Damage curve values were succesfully assigned to mesh elements. Do you want to draw it?"] [list yes no] [list [_ "Yes"] [_ "No"]] question ""]
        if {$answer == "yes"} {
            #ensure mesh view mode
            MeshView 0
            set i 0
            set n [llength $questions]
            foreach question $questions {
                GiD_Process Mescape Data Conditions DrawCond -ByColor- "Damage_curves" $question
            }
        }
    }
    return 0
}

proc Damage::AssignMaxDaRasterDo { filenames_and_questions {show_advance_bar 1} } {
    set fail 0
    variable percent 0
    variable stop 0

    #assign percent of four steps to 10 10 40 40

    set condition_name "Max_Damage"
    set default_values [GidUtils::GetConditionDefaultValues $condition_name] ;#default values
    if { $show_advance_bar } {
        GidUtils::CreateAdvanceBar [_ "Assign maximum damage values"] [_ "Percentage"]: ::Iber::percent 100  {set ::Iber::stop 1}
    }
    set percent 0
    set nodata_value -9999 ;#assumed same value for all files
    set element_ids [Iber::GetElementIdsLayersNoFrozen]
    set num_elements [objarray length $element_ids]
    set xy_element_centers [Iber::GetElementCenters $element_ids]
    set percent 10
    #initialize default values (except fixed model) for all elements
    for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
        set element_id [objarray get $element_ids $i_element]
        set element_values($element_id) $default_values
    }
    set n_raster [expr [llength $filenames_and_questions]/2]
    set n_refresh 1
    #fill a field by each raster
    set i_raster 0
    foreach {filename_raster condition_question} $filenames_and_questions {
        if { $show_advance_bar } {
            if { ![expr {$i_raster%$n_refresh}] } {
                set percent [expr int(double($i_raster)/$n_raster*100*0.4+20)]
            }
        }
        if { $stop } {
            set fail -1
            break
        }
        set condition_question_index [GidUtils::GetConditionQuestionIndex $condition_name $condition_question]
        set values $default_values
        set raster_interpolation [GDAL::ReadRaster $filename_raster 0]
        set nodata_value [GDAL::GetNoDataValue $filename_raster]
        #set interpolated_values [GiD_Raster interpolate $raster_interpolation [list nodes $xy_element_centers]]
        #-closest in this case also, because GiD_AssignData become a bottleneck in case of a each element a different value (and big strings), better use only a few discrete values
        set interpolated_values [GiD_Raster interpolate -closest $raster_interpolation [list nodes $xy_element_centers]]
        for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
            set element_id [objarray get $element_ids $i_element]
            set value [objarray get $interpolated_values $i_element]
            lset element_values($element_id) $condition_question_index $value
        }
        incr i_raster
    }
    #assign the final values
    #GiD_AssignData condition of a condition canrepeat=no an big mesh is a huge bottleneck
    #(because it is moving every call the whole container to a temporary container sorted by it)
    #in future versions (>14.1.8d) we want to enhance the code storing entityvalue ordered by entity id and avoid the bottleneck
    #
    #to minimize GiD_AssignData calls join in a single assign the elements with same values
    for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
        set element_id [objarray get $element_ids $i_element]
        set values $element_values($element_id)
        lappend elements_same_value($values) $element_id
    }
    unset element_ids
    array unset element_values
    set list_values [array names elements_same_value]
    set num_different_values [llength $list_values]
    set n_refresh_different_values [expr int($num_different_values/4)]
    if { $n_refresh_different_values<1 } {
        set n_refresh_different_values 1
    }
    set i_different_values 0
    foreach values $list_values {
        if { $values != $nodata_value } {
            if { $show_advance_bar } {
                if { ![expr {$i_different_values%$n_refresh_different_values}] } {
                    set percent [expr int(double($i_different_values)/$num_different_values*100*0.4+60)]
                }
            }
            GiD_AssignData condition $condition_name body_elements $values $elements_same_value($values)
            incr i_different_values
        }
    }
    if { $show_advance_bar } {
        GidUtils::DeleteAdvanceBar
    }
    return $fail
}

proc Damage::AssignDamageCurveRasterDo { filenames_and_questions {show_advance_bar 1} } {
    set fail 0
    variable percent 0
    variable stop 0

    #assign percent of four steps to 10 10 40 40

    set condition_name "Damage_curves"
    set default_values [GidUtils::GetConditionDefaultValues $condition_name] ;#default values
    if { $show_advance_bar } {
        GidUtils::CreateAdvanceBar [_ "Assign damage curve numbers"] [_ "Percentage"]: ::Iber::percent 100  {set ::Iber::stop 1}
    }
    set percent 0
    set nodata_value -9999 ;#assumed same value for all files
    set element_ids [Iber::GetElementIdsLayersNoFrozen]
    set num_elements [objarray length $element_ids]
    set xy_element_centers [Iber::GetElementCenters $element_ids]
    set percent 10
    #initialize default values (except fixed model) for all elements
    for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
        set element_id [objarray get $element_ids $i_element]
        set element_values($element_id) $default_values
    }
    set n_raster [expr [llength $filenames_and_questions]/2]
    set n_refresh 1
    #fill a field by each raster
    set i_raster 0
    foreach {filename_raster condition_question} $filenames_and_questions {
        if { $show_advance_bar } {
            if { ![expr {$i_raster%$n_refresh}] } {
                set percent [expr int(double($i_raster)/$n_raster*100*0.4+20)]
            }
        }
        if { $stop } {
            set fail -1
            break
        }
        set condition_question_index [GidUtils::GetConditionQuestionIndex $condition_name $condition_question]
        set values $default_values
        set raster_interpolation [GDAL::ReadRaster $filename_raster 0]
        set nodata_value [GDAL::GetNoDataValue $filename_raster]
        #set interpolated_values [GiD_Raster interpolate $raster_interpolation [list nodes $xy_element_centers]]
        #-closest in this case also, because GiD_AssignData become a bottleneck in case of a each element a different value (and big strings), better use only a few discrete values
        set interpolated_values [GiD_Raster interpolate -closest $raster_interpolation [list nodes $xy_element_centers]]
        for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
            set element_id [objarray get $element_ids $i_element]
            set value [expr int([objarray get $interpolated_values $i_element])]
            lset element_values($element_id) $condition_question_index $value
        }
        incr i_raster
    }
    #assign the final values
    #GiD_AssignData condition of a condition canrepeat=no an big mesh is a huge bottleneck
    #(because it is moving every call the whole container to a temporary container sorted by it)
    #in future versions (>14.1.8d) we want to enhance the code storing entityvalue ordered by entity id and avoid the bottleneck
    #
    #to minimize GiD_AssignData calls join in a single assign the elements with same values
    for {set i_element 0} {$i_element < $num_elements} {incr i_element} {
        set element_id [objarray get $element_ids $i_element]
        set values $element_values($element_id)
        lappend elements_same_value($values) $element_id
    }
    unset element_ids
    array unset element_values
    set list_values [array names elements_same_value]
    set num_different_values [llength $list_values]
    set n_refresh_different_values [expr int($num_different_values/4)]
    if { $n_refresh_different_values<1 } {
        set n_refresh_different_values 1
    }
    set i_different_values 0
    foreach values $list_values {
        if { $values != $nodata_value } {
            if { $show_advance_bar } {
                if { ![expr {$i_different_values%$n_refresh_different_values}] } {
                    set percent [expr int(double($i_different_values)/$num_different_values*100*0.4+60)]
                }
            }
            GiD_AssignData condition $condition_name body_elements $values $elements_same_value($values)
            incr i_different_values
        }
    }
    if { $show_advance_bar } {
        GidUtils::DeleteAdvanceBar
    }
    return $fail
}
# ------------------------------------------------------------------------------

# Funciones relacionadas con la definicion y asignacion de las Damage curves
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
proc Damage::Window { } {
    variable data
    variable damage_curves
    variable current_damage_curve
    variable current_value
    variable table

    if { ![info exists damage_curves] } {
        Damage::SetDefaultValues
    }

    Damage::FillTclDataFromProblemData

    set w .gid.damage_curve
    InitWindow $w [= "Damage curve"] PreDamageCurveWindowGeom Damage::Window
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    set f [ttk::frame $w.fdamage_curves]

    ttk::combobox $f.cb1 -textvariable Damage::current_damage_curve -values [Damage::GetDamageCurve] -state readonly
    bind $f.cb1 <<ComboboxSelected>> [list Damage::OnChangeSelectedDamageCurve %W]
    ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list Damage::NewDamageCurve $f.cb1]
    GidHelp $f.bnew  [= "Create a new damage curve"]
    ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list Damage::DeleteDamageCurve $f.cb1]
    GidHelp $f.bdel  [= "Delete a damage curve"]
    ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list Damage::RenameDamageCurve $f.cb1]
    GidHelp $w.f.bren  [= "Rename a damage curve"]
    grid $f.cb1 $f.bnew $f.bdel $f.bren -sticky w
    grid $f -sticky new
    grid rowconfigure $f 0 -weight 1

    set f [ttk::frame $w.fdamage_curve]

    foreach field {raster_value} text [list [= "Damage curve number"]] {
          ttk::label $f.l$field -text $text
          ttk::entry $f.e$field -textvariable Damage::current_value($field)
          grid $f.l$field $f.e$field -sticky ew
      }

    grid $f -sticky new
    grid columnconfigure $f {1} -weight 1

    package require fulltktree
    set columns ""
    foreach name [list [= "Water depth (m)"] [= "% (0-1)"]] {
        lappend columns [list 4 $name left text 1]
    }
    set T [fulltktree $f.t -columns $columns -showlines 0 -showheader 1 -expand 1]
    set table $T
    $T configure -editbeginhandler GridData::EditBegin
    $T configure -editaccepthandler [list GridData::EditAccept]
    $T configure -deletehandler GridData::RemoveRows
    $T configure -contextualhandler_menu GridData::ContextualMenu
    $T column configure all -button 0

    ttk::frame $f.fbuttons
    ttk::button $f.fbuttons.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $T 1]
    GidHelp $f.fbuttons.b1 [_ "Pressing this button, one new line will be added to this window"]
    ttk::button $f.fbuttons.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $T 1]
    GidHelp $f.fbuttons.b2 [_ "Pressing this button, one line is deleted from this window"]

    ttk::button $f.fbuttons.b3 -image [gid_themes::GetImage graphs.png small_icons] -text [_ "Graph"] \
        -command [list GridData::PlotCurveWin $T [= "Damage curve"] ""]
    GidHelp $f.fbuttons.b3 [_ "Pressing this button, a XY graph will be drawn"]

    grid $T -sticky nsew -columnspan 2
    grid $f.fbuttons.b1 $f.fbuttons.b2 $f.fbuttons.b3 -sticky w
    grid $f.fbuttons -sticky ew



    grid columnconfigure $f {0} -weight 1
    grid rowconfigure $f {1 4} -weight 1
    grid $f -sticky nsew -padx 2 -pady 2
    GidHelp $f [= "Set damage curve"]

    bind $T <<Paste>> [list ::GridData::Paste $T CLIPBOARD]
    bind $T <<PasteSelection>> [list ::GridData::Paste $T PRIMARY]
    bind $T <<Cut>> [list GridData::Cut $T]
    bind $T <<Copy>> [list GridData::Copy $T]
    bind [$T givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header

    GridData::SetData $T $current_value(table_value)

    set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
    ttk::button $f.bapply -text [= "Apply"] -command [list Damage::Apply $T] -style BottomFrame.TButton
    ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
    #grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
    grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
    grid $f -sticky sew
    if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }

    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1
    bind $w <Alt-c> "$f.bclose invoke"
    bind $w <Escape> "$f.bclose invoke"
    bind $w <Destroy> [list +Damage::DestroyDamageWindow %W $w] ;# + to add to previous script
}

proc Damage::SetDefaultValues { } {
    variable data
    variable damage_curves
    variable current_damage_curve
    variable current_value
    variable fields
    variable fields_defaults
    array unset data
    set damage_curves {}
    foreach field $fields  {
        set current_value($field) ""
    }
    set current_value(table_value) {0.0 0.0}
    set current_damage_curve {}
}

proc Damage::FillTclDataFromProblemData { } {
    variable data
    variable damage_curves
    if { [catch {set x [GiD_AccessValue get gendata DamageCurvesData]} msg] } {
        #reading a model created by an old version of the problemtype this gendata doesn't exists
        return
    }
    array set data $x

    set damage_curves [list]
    foreach item [array names data *,table_value] {
        lappend damage_curves [string range $item 0 end-12]
    }
    set damage_curves [lsort -dictionary $damage_curves]
    Damage::SetCurrentDamageCurve [lindex $damage_curves 0]
}

proc Damage::SetCurrentDamageCurve { damage_curve } {
    variable data
    variable current_damage_curve
    variable current_value
    variable fields
    variable table
    #fill in the current_value variables with data
    if { $damage_curve != "" } {
        foreach field $fields {
            if { ![info exists data($damage_curve,$field)] } {
                set data($damage_curve,$field) 0
            }
            set current_value($field) $data($damage_curve,$field)
        }
    } else {
        foreach field $fields {
            set current_value($field) ""
        }
    }

    #the other widgets have a linked variable that automatically update it but the table not
    if { [info exists table] && [winfo exists $table] } {
        GridData::SetData $table $current_value(table_value)
    }
    set current_damage_curve $damage_curve
}

proc Damage::GetDamageCurve { } {
    variable damage_curves
    if { ![info exists damage_curves] } {
        return ""
    }
    return $damage_curves
}

proc Damage::OnChangeSelectedDamageCurve { cb } {
    Damage::SetCurrentDamageCurve [Damage::GetCurrentDamageCurve]
}

proc Damage::GetDamageCurveAutomaticName { } {
    set basename [_ "damage_curve"]
    set i 1
    set damage_curve $basename-$i
    while { [Damage::Exists $damage_curve] } {
        incr i
        set damage_curve $basename-$i
    }
    return $damage_curve
}

proc Damage::GetDamageCurveAutomaticNumber { } {
    set i 1
    set damage_curve_number_value $i
    set basename [_ "damage_curve"]
    set damage_curve $basename-$i
    while { [Damage::Exists $damage_curve] } {
        incr i
        set damage_curve $basename-$i
        set damage_curve_number_value $i
    }
    return $damage_curve_number_value
}

proc Damage::Exists { damage_curve } {
    if { [Damage::GetIndex $damage_curve] != -1 } {
        set exists 1
    } else {
        set exists 0
    }
    return $exists
}

proc Damage::GetIndex { damage_curve } {
    variable damage_curves
    return [lsearch $damage_curves $damage_curve]
}

proc Damage::NewDamageCurve { cb } {
    variable damage_curves
    variable data
    variable fields
    variable fields_defaults
    set damage_curve [Damage::GetDamageCurveAutomaticName]
    set raster_value_curve [Damage::GetDamageCurveAutomaticNumber]
    if { [Damage::Exists $damage_curve] } {
        #already exists
        return 1
    }
    foreach field $fields value $fields_defaults {
        set data($damage_curve,$field) $value
    }
    set data($damage_curve,raster_value) $raster_value_curve
    lappend damage_curves $damage_curve
    if { [winfo exists $cb] } {
        $cb configure -values [Damage::GetDamageCurve]
    }
    Damage::SetCurrentDamageCurve $damage_curve
    return 0
}

proc Damage::GetDamageCurveNumber { damage_curve } {
    variable data

    append res $data($damage_curve,raster_value)

    return $res
}

proc Damage::GetCurrentDamageCurve { } {
    variable current_damage_curve
    return $current_damage_curve
}

proc Damage::DeleteDamageCurve { cb } {
    variable damage_curves
    variable data
    variable fields
    variable fields_defaults
    set damage_curve [Damage::GetCurrentDamageCurve]
    if { ![Damage::Exists $damage_curve] } {
        #not exists
        return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete curve '%s'" $damage_curve] \
            gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {
        set i [Damage::GetIndex $damage_curve]
        array unset data $damage_curve,*
        set damage_curves [lreplace $damage_curves $i $i]
        if { [winfo exists $cb] } {
            $cb configure -values [Damage::GetDamageCurve]
        }
        Damage::SetCurrentDamageCurve [lindex $damage_curves 0]
        GiD_Redraw
    }
    return 0
}

proc Damage::RenameDamageCurve { cb } {
    variable data
    variable damage_curves
    variable fields
    set damage_curve [Damage::GetCurrentDamageCurve]
    if { ![Damage::Exists $damage_curve] } {
        #not exists
        return 1
    }
    set new_name [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $damage_curve] \
            [= "Enter new name of %s '%s'" [= "damage_curve"] $damage_curve] gidquestionhead "any" ""]
    if { $new_name != "--CANCEL--" } {
        foreach field $fields {
            set data($new_name,$field) $data($damage_curve,$field)
        }
        array unset data $damage_curve,*
        set i [Damage::GetIndex $damage_curve]
        lset damage_curves $i $new_name
        if { [winfo exists $cb] } {
            $cb configure -values [Damage::GetDamageCurve]
        }
        Damage::SetCurrentDamageCurve $new_name
        GiD_Redraw
    }
    return 0
}

proc Damage::Apply { T } {
    variable data
    variable current_value
    variable fields

    set damage_curve [Damage::GetCurrentDamageCurve]
    foreach field $fields {
        set data($damage_curve,$field) $current_value($field)
    }

    set data($damage_curve,table_value) [GridData::GetDataAllItems $T]
    GiD_Redraw
}

proc Damage::DestroyDamageWindow { W w } {
    if { $W == $w } {
        Damage::FillProblemDataFromTclData
    }
}

proc Damage::FillProblemDataFromTclData { } {
    variable data
    variable current_values
    set x [array get data]
    GiD_AccessValue set gendata DamageCurvesData $x
}

proc Damage::CloseWindow { } {
    set w .gid.damage_curve
    if { [winfo exists $w] } {
        close $w
    }
}

proc Damage::UnsetVariables { } {
    variable data
    variable current_values
    unset -nocomplain data
    unset -nocomplain current_values
}
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# Funciones para escribir el Iber_FloodDamageCurves.dat
proc Damage::WriteDamageCurvesFile { } {
    variable data
    set res ""

    set i 0
    append res "Damage curves definition"  \n
    append res [llength [Damage::GetDamageCurve]]
    append res "  --> number of damage curves"  \n
    foreach damage_curve [Damage::GetDamageCurve] {
        incr i
        append res [Damage::WriteDamageCurveName [expr $i-1]]  \n
        append res $data($damage_curve,raster_value)
        append res  "  --> damage curve number"  \n
        set lines [expr [llength $data($damage_curve,table_value)]/2]
        append res $lines  \n
        set l [expr $lines*2]
        for {set ii 0} {$ii < $l} {set ii [expr $ii+2]} {
            append res "[lrange $data($damage_curve,table_value) $ii [expr $ii+1]] " \n
        }
    }
    append res "Damage curves assignation"

    return $res
}

proc Damage::WriteDamageCurveName { arg } {
    variable name_damage_curve

    set name_damage_curve [lindex [Damage::GetDamageCurve] $arg]

    return $name_damage_curve
}

proc Damage::WriteDamageCurveNumber { name } {
    variable number

    set number [lindex [lsearch [Damage::GetDamageCurve] $name] 3]

    return $number
}
