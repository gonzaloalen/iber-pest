# Iber Pre-process Menu ### Marcos 2018-05-24 ###

#                                                                                                        #
#                ##        ########        #########        ########                #
#                ##        ##                #        ##                        ##                #                #
#                ##        ##                #        ##                        ##                #                #
#                ##        ##                #        ##                        ##                #                #
#                ##        ##           #        #####                ##           #                #
#                ##        ##                #        ##                        ##          #                        #
#                ##        ##                #        ##                        ##          #                        #
#                ##        ##                #        ##                        ##           #                #
#                ##        ########        #########        ##                #                #
#                                                                                                        #

namespace eval IberPreMenu {
    
}

#LOAD Background Image
proc IberPreMenu::BackgroundImage {} {
    GiD_Process Mescape Utilities BackgroundImg RealSizeImg
    # this expects interaction with the user to ask filename and possible screen location
    # cannot ask its result or do zoom frame here because the command is not finished !!
    # can implement it at GiD_Event_AfterChangeBackground that will be invoked with the action is done
}

#Background Image SHOW/HIDE
proc IberPreMenu::SwapShowBackgroundImage {} {
    if { [GiD_BackgroundImage get show] } {
        set new_state 0
        set message [= "Background image set hidden"]
    } else {
        set new_state 1
        set message [= "Background image set visible"]
    }
    GiD_BackgroundImage set show $new_state
    #GidUtils::SetWarnLine $message
}

#Create mesh
proc IberPreMenu::CreateMesh {} {
    GiD_Process Mescape Meshing Generate
}


#Calculate
proc IberPreMenu::CalculateIber {} {
    GiD_Process Mescape Utilities Calculate
    PWViewOutput
}


#Process Info
# what can be: current or window
proc IberPreMenu::PWViewOutput { { what current } } {
    global GidProcWin RunProcInfo
    
    if { ![info exists RunProcInfo] } {
        set RunProcInfo ""
    }
    
    if { ![info exists GidProcWin(w)] || \
        ![winfo exists $GidProcWin(w).listbox#1] } {
        set wbase .gid
        set w ""
    } else {
        set wbase $GidProcWin(w)
        set w $GidProcWin(w).listbox#1
    }
    
    if { $what == "current" } {
        set ProjectName [GiD_Info Project ModelName]
        if { [file extension $ProjectName] == ".gid" } {
            set ProjectName [file root $ProjectName]
        }
        set basename [file tail $ProjectName]
        set uid ""
        
        foreach i $RunProcInfo {
            set name [file tail [lindex $i 0]]
            if { $name == $basename } {
                set filenames [lindex $i 3]
                set uid [lindex $i 2]
                set time [lindex $i 1]
                break
            }
        }
        if { $uid == "" } {
            set filenames [PWFIndOutputFilenameCurrent]
        }
        if { $filenames == "" } {
            tk_dialogRAM $wbase.tmpwin [_ "error"] \
                [_ "There is no information to visualize for current project '%s'." $basename] \
                error 0 [_ "OK"]
            return
        }
        set index 0
        foreach filename $filenames {
            if { $uid == "" && ![file exists $filename] } {
                tk_dialogRAM $wbase.tmpwin [_ "error"] \
                    [_ "There is no information to visualize for current project."] \
                    error 0 [_ "OK"]
                return
            }
            set name current
            if {$index > 0} {
                set name "$name-$index"
            }
            if { $uid == "" } {
                PWViewOutputWin $filename $name ""
            } elseif { $uid == "remote" } {
                PWViewOutputWin $filename $name \
                    [lrange [clock format [list $time]] 0 3] $basename
            } else {
                PWViewOutputWin $filename $name [lrange [clock format [list $time]] \
                        0 3] ""
            }
            incr index
        }
        return
    }
    
    if { [llength $RunProcInfo] == 0 } {
        #tk_dialogRAM $wbase.tempwin [_ "Error window"] [_ "There are no processes running."] gidquestionhead 0 [_ "OK"]
        PWViewOutput "current"
        return
    }
    
    set inum 0
    if { [$w size] == 1 } { $w sel set 0}
    for { set i 0 } { $i < [$w size] } { incr i } {
        if { [$w sel includes $i] } {
            incr inum
            set aa [$w get $i]
            set name [string trim [string range $aa 0 19]]
            set ii 20
            while { [string index $aa $ii] != " " } {
                append name [string index $aa $ii]
                incr ii
            }
            set filenames ""
            foreach j $RunProcInfo {
                set namein [file tail [lindex $j 0]]
                if { [string comp $namein $name] == 0 } {
                    set filenames [lindex $j 3]
                    set uid [lindex $j 2]
                    set time [lindex $j 1]
                    break
                }
            }
            if { $filenames == "" } {
                tk_dialogRAM $wbase.tmpwin [_ "error"] \
                    [_ "There is no information to visualize for project '%s'." $name] \
                    error 0 [_ "OK"]
                continue
            }
            set index 0
            foreach filename $filenames {
                if {$index > 0} {
                    set name "$name-$index"
                }
                if { $uid == "remote" } {
                    PWViewOutputWin $filename $name [lrange [clock format \
                                [list $time]] 0 3] $name
                } else {
                    PWViewOutputWin $filename $name [lrange [clock format \
                                [list $time]] 0 3] ""
                }
                incr index
            }
        }
    }
    if { $inum == 0 } {
        tk_dialogRAM $wbase.tmpwin [_ "error"] [_ "Please, select a process first."] \
            error 0 [_ "OK"]
        return
    }
}


#CANCEL Calculate
proc IberPreMenu::CancelCalculateIber {} {
    GiD_Process Mescape Utilities CancelProcess
}

# POST-process

#POST-process view results on elements
proc IberPreMenu::ResOnElem {} {
    GiD_Process Mescape View Label Select ResultOnView
}

#POST-process view results on elements
proc IberPreMenu::ResOnElemOff {} {
    GiD_Process Mescape View Label Off
}
