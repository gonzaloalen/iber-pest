
proc InitGIDProject {dir} {

	GiDMenu::Create "Ponds" PRE
    GiDMenu::InsertOption "Ponds" [list "Automatic geometry"] 0 PRE "InputData" "" ""
    GiDMenu::InsertOption "Ponds" [list "Multiple calculation"] 1 PRE "CalculateMultipleGeom" "" ""
		
	#GiDMenu::InsertOption "Ponds" [list "---"] 1 PRE "" "" ""
	GiDMenu::UpdateMenus
	# Custom Menu
}

proc InputData { } {
	global GeometryPriv
	set w .gid.geometry
    
	set GeometryPriv(workingdir) ""
	#creación de la ventana principal
	InitWindow $w "Automatic model generation" PreGeometryLateralWindowGeom InputData
	if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
	#frame $w.frmParams
	
	#creación de los witgets
	set f [ttk::frame $w.frmParams]
	
	#Label 0
	ttk::label $f.workdirtext -text "Working directory: "
	ttk::entry $f.workdir -textvariable GeometryPriv(workingdir)
	ttk::button $f.workdirret -text [= "Select"] -command WorkDirPonds
	grid $f.workdirtext $f.workdir $f.workdirret -sticky w
	#Label 1
	ttk::label $f.modelstext -text "Number of models: "
	ttk::entry $f.gmodelsnum -textvariable GeometryPriv(modelsnum)
	grid $f.modelstext $f.gmodelsnum -sticky w
	#Label 2
	set GeometryPriv(calculate) 1
	ttk::label $f.calctext -text "Calculate all models"
	ttk::checkbutton $f.calc -variable GeometryPriv(calculate)
	grid $f.calctext $f.calc -sticky w
	#Label 3
	set GeometryPriv(nogeometry) 1
	ttk::label $f.nogeotext -text "Delete geometry at end"
	ttk::checkbutton $f.nogeo -variable GeometryPriv(nogeometry)
	grid $f.nogeotext $f.nogeo -sticky w
	grid $f -sticky new
	grid columnconfigure $f {1} -weight 1
	$f.workdir configure -state disable
	
	#Buttons settins
	set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
	#frame $f.frmButtons
	button $f.btnGeom -text "Execute" -command "CreateGeometry $w" -underline 0 -width 10
	button $f.btnclose -text "Close" -command "destroy $w" -underline 0 -width 10
	grid $f.btnGeom $f.btnclose -sticky ew -padx 5 -pady 5
	grid $f -sticky sew
	if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center } 
	grid columnconfigure $w 0 -weight 1
	grid rowconfigure $w 1 -weight 1
}

proc WorkDirPonds { } {
    global GeometryPriv
    set GeometryPriv(workingdir) [tk_chooseDirectory -title [= "Choose working directory"]]
}

#Create geometry
proc CreateGeometry { w } {
	global GeometryPriv
	set n $GeometryPriv(modelsnum)

	#Working directories
	set dir $GeometryPriv(workingdir)
	set dirgeo [file join $dir Geometries]
	set GeometryPriv(dirgeo) $dirgeo
	set checkdirgeo [file exists $dirgeo]
	set dirbre [file join $dir Breachs]
	set GeometryPriv(dirbre) $dirbre
	set checkdirbre [file exists $dirbre]
	set dirgau [file join $dir Gauges]
	set GeometryPriv(dirgau) $dirgau
	set checkdirgau [file exists $dirgau]

	if { $checkdirgeo == 0 } {
		tk_dialogRAM .gid.tmpwin [= "Error in working directory"] \
			[= "Folder of 'Geometries' does not exist in the current working directoty"] \
			info 0 OK
		return
	} elseif { $checkdirbre == 0 } {
		tk_dialogRAM .gid.tmpwin [= "Error in working directory"] \
			[= "Folder of 'Breachs' does not exist in the current working directoty"] \
			info 0 OK
		return
	}

	set dirmod [file join $dir Models]
	set GeometryPriv(dirmod) $dirmod
	file mkdir $dirmod
	
	GidUtils::DisableGraphics
	GidUtils::DisableWarnLine
	GidUtils::WaitState .gid

	set initime [clock seconds]

	set GidPriv(StopLoading) 0
	AdvanceBar 0 1 0 [= "Generating models: 1 of $n" ] [= "Percentage"]:
	set dn $n
	if { $dn < 1 } { set dn 1 }
		
	for {set i 1} {$i <= $n} {incr i} {
		set filemane ""
		set filename [file join $dirgeo terrenobalsa$i.txt]
		#set filename [file join {C:/00/GEO/} terrenobalsa$i.txt]

		set GeometryPriv(control,$i) [file exists $filename]

		if { $GeometryPriv(control,$i) == 1 } {
			#Geometry
			GiD_Process Mescape Files XYZRead ToPoints -connect:0 -collapse:0 -autotol:0 -tolerance:0.00025 -ignorelayer:1 -collapse_allow_more_tasks:0 -- $filename
			GiD_Process 'Layers ChangeName Layer0 InitialCond1 escape 
			GiD_Process Mescape Meshing ConnectPoints CreateSurfaces 1:200 escape
			GiD_Process 'Layers New Layer0 escape 'Layers ChangeName Layer0 InitialCond2 escape 
			GiD_Process Mescape Meshing ConnectPoints CreateSurfaces 161:240 escape
			GiD_Process 'Layers New Layer0 escape 'Layers ChangeName Layer0 Intermedio escape 
			GiD_Process Mescape Meshing ConnectPoints CreateSurfaces 201:320 escape
			GiD_Process 'Layers New Layer0 escape 'Layers ChangeName Layer0 Terrain1 escape 
			GiD_Process Mescape Meshing ConnectPoints CreateSurfaces 281:1400 escape
			GiD_Process 'Layers New Layer0 escape 'Layers ChangeName Layer0 Terrain2 escape 
			GiD_Process Mescape Meshing ConnectPoints CreateSurfaces 1361:3480 escape
			GiD_Process Mescape Utilities Collapse model Yes
	
			set maxcota 504.98
			#Initial and outlet condition to geometry
			set surfaces_ci [GiD_EntitiesLayers get InitialCond1 surfaces]
			set aux2 [GiD_EntitiesLayers get InitialCond2 surfaces]
			append surfaces_ci [GiD_EntitiesLayers get InitialCond2 surfaces]
			set lines_terr [GiD_EntitiesLayers get Terrain2 lines]
			GiD_Process Mescape Data Conditions AssignCond 2D_Initial_Condition Change Elevation $maxcota 0.0 {*}$surfaces_ci escape escape
			GiD_Process Mescape Data Conditions AssignCond 2D_Outlet Change 0 {0 0 0} SUPERCRITICAL/CRITICAL Weir #N# 2 0.0 0.0 1.6 Height 0.0 0.0 1 #N# 2 0.0 0.0 1 #N# 2 0.0 0.0 1 {*}$lines_terr SwapSelection SwapSelection InvertSelection RemoveFromSel filter: filter:HIGHERENTITY=2 1:end escape RemoveFromSel Mescape escape escape escape escape
	
			#Roughness
			GiD_Process Mescape Data Materials AssignMaterial river Surfaces 1:End escape Mescape escape escape escape escape

			GiD_Process 'Layers Entities Intermedio LowerEntities HigherEntities Surfaces SelWindow SwapSelection AddToSelection filter: filter:LAYER=InitialCond2 1:end escape
			
			#Mesh
			#GiD_Process Mescape Meshing Structured Surfaces 1:End escape {*}$lines_ci {*}$lines_terr escape 1 escape
			#GiD_Process Mescape Meshing Generate 10 escape
			OneElementMesh

			#Breach - Importation
			Breach::ReadBreachFile $i $dirbre
			#Breach::ImportBreaches

			#Get farest point
			set xmaxcoor 0
			set xmaxcoorant 0
			set maxnumpoints [GiD_Info Geometry NumPoints]
			for {set p 1} {$p <= $maxnumpoints} {incr p} {
				set coor [GiD_Info Coordinates $p]
				set xmaxcoor [lindex [lindex $coor 0] 0]
				if { $xmaxcoor > $xmaxcoorant } {
					set xmaxcoorant $xmaxcoor
				} else {
					#Keep the previous value
				}
			}

			#Erase geometry
			if { $GeometryPriv(nogeometry) == 1 } {
				GiD_Process Mescape Geometry Delete Surfaces LowerEntities 1:End escape
			}
			#Finer mesh
			for {set m 1} {$m <= 2} {incr m} {
				GiD_Process Mescape Meshing EditMesh SplitElems TriaToTria SelWindow SwapSelection AddToSelection filter: filter:LAYER=Intermedio 1:end escape
			}
			#GiD_Process Mescape Meshing MeshView 

			#Time parameters
			#Assimung routing velocity of 0.25m/s
			set maxsimtime [expr {$xmaxcoor/0.25}]
			GiD_AccessValue set gendata Max_simulation_time_ $maxsimtime
			GiD_AccessValue set gendata Results_time_interval_ 300
			GiD_AccessValue set gendata Number_of_Threads 2

			#Results
			GiD_AccessValue set gendata Specific_Discharge 0
			GiD_AccessValue set gendata Froude 0
			GiD_AccessValue set gendata Maximum_Spec_Discharge 0

			#Gauges
			if { $checkdirgau == 0 } {
				GiD_AccessValue set gendata Gauges Off
			} else {
				GiD_AccessValue set gendata Gauges On
				GiD_AccessValue set gendata Gauge_time_interval_ 60
				set gaugecoor ""
				set gaugecoor [ReadingGauges $i $dirgau]
				GiD_AccessValue set gendata Gauge_coordinates $gaugecoor
			}
	
			#Save
			set filename2 [file join $dirmod geometry_$i]
			#set filename2 [file join {C:/00/Modelos/} geometry_$i]
			GiD_Process Mescape Files SaveAs $filename2 escape

			#Generate calculation files
			#GiD_Process Mescape Files WriteCalcFile
			#set batfile [file join {D:/IBER/MASTER/IBER.gid} IBER.win.bat]
			#exec $batfile

			#Run simulation
			if { $GeometryPriv(calculate) == 1 } {
				GiD_Process Mescape Utilities Calculate
			}
	
			GiD_Process escape escape escape escape escape Mescape Files New

		} else {
			set GeometryPriv(control,$i) 0
		}

		set percent [expr int(100.0*$i/$n)]
		AdvanceBar $percent 100 $percent [= "Generating models: $i of $n" ] [= "Percentage"]:
		if { $GidPriv(StopLoading) == 1 } {
			break
			unset percent
		}
	}
	
	AdvanceBar 100 100 100 [= "Generating models: $n of $n" ] [= "Percentage"]:
	GidUtils::EnableGraphics
	GidUtils::EnableWarnLine
	GidUtils::EndWaitState .gid
	
	for {set i 1} {$i <= $n} {incr i} {
		if { $GeometryPriv(control,$i) == 1} {
			#Do nothing
		} else {
			WarnWinText [= "File ID %s does not exists" $i]
		}
	}

	if { $checkdirgau == 0 } {
		GidUtils::SetWarnLine [= "'Gauges' folder does not exist. Thus, any gauge will be assessed"]
		#tk_dialogRAM .gid.tmpwin [= "Error in working directory"] \
		#	[= "Folder of 'Gauges' does not exist in the current working directoty"] \
		#	info 0 OK
		#return
	}

	set endtime [clock seconds]
	set elapsedtime [expr {$endtime-$initime}]
	GidUtils::SetWarnLine [= "Elapsed time: %s seconds" $elapsedtime]

	#set index [tk_dialogRAM .gid.tmpwin [= "Geometries created"] \
	#[= "Do you want to calculate each geometry?"] question 0 [= "Yes"] [= "No"]]
    
	#if {$index == 0} {
	#	CalculateMultipleGeom
	#}	
}

proc ReadingGauges { i dirgau } {
	global GeometryPriv
	
	set filegau ""
	set filegau [file join $dirgau gauges_$i.txt]
	set check [file exists $filegau]
	
	if { $check == 1 } {
		set gaugefile [open $filegau]
		set gaugedata ""
		set gaugedata [gets $gaugefile]

		set gaugecoor $gaugedata
	} else {
		GidUtils::SetWarnLine [= "Wrong gauge filename format in model %s" $i]
		#tk_dialogRAM .gid.tmpwin "Wrong gauge filename format" info 0 OK
		GiD_AccessValue set gendata Gauges Off
		set gaugecoor {#N# 2 0 0}
	}
	
	return $gaugecoor
}


#To calculate the models in cascade, e.g. one after the other or per blocks of 4 models
proc CalculateMultipleGeom {} {
	global GeometryPriv
	set n $GeometryPriv(modelsnum)
	set dir $GeometryPriv(workingdir)
	
	#set directory "C:/00/Modelos/"
	set directory [file join $dir Models]
	file mkdir $directory
	set files [glob [file join $directory *]]
	set numsim [llength $files]

	GidUtils::DisableGraphics
	GidUtils::DisableWarnLine
	GidUtils::WaitState .gid

	set GidPriv(StopLoading) 0
	AdvanceBar 0 1 0 [= "Generating models: 1 of $numsim" ] [= "Percentage"]:
	set dn $numsim
	if { $dn < 1 } { set dn 1 }

	set countruns 1

	for {set j 1} {$j <= $numsim} {incr j} {
		set name ""
		set name [file join $dir geometry_$j]
		
		Optimization::AddToCalculationQueue $name
		
		set xsecs 60
		after [expr {1000*$xsecs}]
		
		#if { $countruns <= 4 } {
		#	Optimization::AddToCalculationQueue $name
		#} else {
		#	set xsecs 20
		#	after [exrp {1000*$xsecs}]
		#	set countruns [expr {$countruns-1}]
		#}

		set percent [expr int(100.0*$j/$numsim)]
		AdvanceBar $percent 100 $percent [= "Generating models: $j of $numsim" ] [= "Percentage"]:
		if { $GidPriv(StopLoading) == 1 } {
			break
			unset percent
		}
		incr countruns
	}

	AdvanceBar 100 100 100 [= "Generating models: $numsim of $numsim" ] [= "Percentage"]:
	GidUtils::EnableGraphics
	GidUtils::EnableWarnLine
	GidUtils::EndWaitState .gid
}

proc void {} {
	#set filename3 [file join $filename2.gid proceso.rep]
	#set status [file lstat $filename3 mtime]

	set test [GiD_AccessValue get gendata Gauge_coordinates]
	set test1 1
}