
namespace eval MixturesC {
    variable dataclases ;#array of values
    variable mixturesc ;#list of names of dataclases
    variable current_mixturec ;#name of current selected mixturec
    variable current_value ;#array values linked to widgets that represent the current one
    variable fields {table_value}
    variable fields_defaults {{0 0}}  
    variable drawopengl
    variable table ;#tktable widget
    variable current_mixturelc
    variable nstrat
    variable nclass
    variable actlaythick
    variable name
    variable datamezclas
}

proc MixturesC::UnsetVariables { } {
    variable dataclases
    variable mixturesc      
    unset -nocomplain dataclases
    unset -nocomplain mixturesc
}

proc MixturesC::SetDefaultValues { } {    
    variable dataclases
    variable mixturesc
    variable current_mixturec
    variable current_value
    variable fields
    variable fields_defaults
    variable mixtureslc
    
    #Recuperamos las variables
    #MixturesC::FillTclDataFromProblemData
    
    #Recuperamos el n�mero de Stratums (width == Stratums) y de Classes (height == Classes)
    #set nstrat MixturesLC::GetNstrats
    #set nclass MixturesLC::GetNclasses
    
    array unset dataclases
    set mixturesc {}
    foreach field $fields  {
	set current_value($field) ""
    }
    set current_value(table_value) {0 0}
    set current_mixturec {}
}

#store the mixturesc information in a hidden field of the problem data
#then this dataclases is saved with the model without do nothing more
proc MixturesC::FillTclDataFromProblemData { } {
    variable dataclases
    variable mixturesc
    if { [catch {set x [GiD_AccessValue get gendata MixturesCData]} msg] } {
	#reading a model created by an old version of the problemtype this gendata doesn't exists
	return
    }    
    array set dataclases $x
    
    set mixturesc [list]
    foreach item [array names dataclases *,table_value] {
	lappend mixturesc [string range $item 0 end-12]
    }
    set mixturesc [lsort -dictionary $mixturesc]
    MixturesC::SetCurrentMixturec [lindex $mixturesc 0]
}

proc MixturesC::FillProblemDataFromTclData { } {
    variable dataclases
    variable mixturesc
    set x [array get dataclases]
    GiD_AccessValue set gendata MixturesCData $x
}

proc MixturesC::WriteCalculationFile { } {
    variable dataclases
    variable datamezclas
    variable mixturesc
    
    set nstrat [MixturesLC::GetNstrats]
    set nclass [MixturesLC::GetNclasses]
    set actlaythick [MixturesLC::GetActlaythick]
    
    set res ""       
    
    #Stratums&Classes
    set i 0
    append res $nstrat  \n
    append res $nclass \n
    #append res1 [llength [MixturesC::GetMixturesc]] \n
    
    #Classes
    #if { [catch {set x [GiD_AccessValue get gendata MixturesCData]} msg] } {
	#reading a model created by an old version of the problemtype this gendata doesn't exists
	#        return
	#}    
    #array set dataclases $x
    
    foreach mixturec [MixturesC::GetMixturesc] {
	incr i        
	#append res $i  \n
	set lines [expr [llength $dataclases($mixturec,table_value)]/2]
	#append res $lines  \n
	set l [expr $lines*2]
	for {set ii 0} {$ii < $l} {set ii [expr $ii+2]} {
	    append res "[list [lindex $dataclases($mixturec,table_value) [expr $ii+1]]] " 
	    #append res "[lrange $dataclases($mixturec,table_value) $ii [expr $ii-1]] " \n       
	}  
	#Esto es para d90
	set res1 ""
	append res $res1 \n
	append res $actlaythick \n 
    }
    
    #Mixtures
    if { [catch {set y [GiD_AccessValue get gendata MixturesData]} msg] } {
	#reading a model created by an old version of the problemtype this gendata doesn't exists
	return
    }    
    array set datamezclas $y
    set i 0
    append res "Mixtures"  \n
    append res [llength [Mixtures::GetMixtures]] \n
    foreach mixture [Mixtures::GetMixtures] {
	incr i        
	append res $i  \n
	set lines [expr [llength $datamezclas($mixture,table_value)]/$nstrat]
	append res $nstrat 
	append res  "     " 
	append res  $datamezclas($mixture,initialerosion)  \n
	set l [expr $lines*$nstrat]
	for {set ii 0} {$ii < $l} {set ii [expr $ii+$nclass+2]} {
	    append res "[lrange $datamezclas($mixture,table_value) $ii [expr $ii+$nclass+1]] " \n        
	}       
    }
    
    
    return $res
}

proc MixturesC::WriteMixturecNumber { name } {
    variable number
    
    set number [expr [lsearch [MixturesC::GetMixturesc] $name]+1]
    
    return $number    
}


proc MixturesC::GetMixturesc { } {
    variable dataclases
    variable mixturesc
    if { ![info exists mixturesc] } {
	return ""
    }
    return $mixturesc
}

proc MixturesC::GetCurrentMixturec { } {
    variable current_mixturec
    return $current_mixturec
}

proc MixturesC::OnChangeSelectedMixturec { cb } {   
    MixturesC::SetCurrentMixturec [MixturesC::GetCurrentMixturec]
}

proc MixturesC::SetCurrentMixturec { mixturec } {
    variable dataclases
    variable current_mixturec
    variable current_value
    variable fields   
    variable table
    #fill in the current_value variables with dataclases
    if { $mixturec != "" } {
	foreach field $fields {
	    if { ![info exists dataclases($mixturec,$field)] } {
		set dataclases($mixturec,$field) 0
	    }
	    set current_value($field) $dataclases($mixturec,$field)           
	}                
    } else {
	foreach field $fields {
	    set current_value($field) ""
	}
    }
    
    #the other widgets have a linked variable that automatically update it but the table not
    if { [info exists table] && [winfo exists $table] } {
	GridData::SetData $table $current_value(table_value)
    }
    set current_mixturec $mixturec  
}

proc MixturesC::GetIndex { mixturec } {
    variable mixturesc    
    return [lsearch $mixturesc $mixturec]
}

proc MixturesC::Exists { mixturec } {    
    if { [MixturesC::GetIndex $mixturec] != -1 } {
	set exists 1
    } else {
	set exists 0
    }
    return $exists
}

proc MixturesC::GetMixturecAutomaticName { } {
    set basename [_ "class"]
    set mixturec $basename
    
    #This lines are commented waiting for future versions. The last one substitutes 'set mixturelc $basename-$i' (previous)
    #set i 1
    #set mixturec $basename-$i
    #while { [MixturesC::Exists $mixturec] } {        
	#        incr i
	#        set mixturec $basename-$i        
	#}
    
    if { [MixturesC::Exists $mixturec] } {
	tk_dialogRAM .gid.tmpwin [= "Warning"] [= "The number of Classes have been already defined"] warning 0 OK
	#already exists
	#return $mixturec
    }
    return $mixturec
}

proc MixturesC::NewMixturec { cb } {
    variable mixturesc
    variable dataclases
    variable fields 
    variable fields_defaults
    set mixturec [MixturesC::GetMixturecAutomaticName]     
    
    if { [MixturesC::Exists $mixturec] } {
	#already exists
	return 1
    }
    foreach field $fields value $fields_defaults {
	set dataclases($mixturec,$field) $value
    }
    lappend mixturesc $mixturec   
    if { [winfo exists $cb] } {
	$cb configure -values [MixturesC::GetMixturesc]
    }
    MixturesC::SetCurrentMixturec $mixturec
    return 0
}

proc MixturesC::DeleteMixturec { cb } {
    variable mixturesc
    variable dataclases
    variable fields 
    variable fields_defaults
    set mixturec [MixturesC::GetCurrentMixturec] 
    if { ![MixturesC::Exists $mixturec] } {
	#not exists
	return 1
    }
    set ret [tk_dialogRAM .gid.askyn [= "Confirm"] [= "Are you sure to delete mixturec '%s'" $mixturec] \
	    gidquestionhead 1 [= "Yes"] [= "No#C#I don't want to do that"]]
    if { $ret == 0 } {     
	set i [MixturesC::GetIndex $mixturec] 
	array unset dataclases $mixturec,*        
	set mixturesc [lreplace $mixturesc $i $i]       
	if { [winfo exists $cb] } {
	    $cb configure -values [MixturesC::GetMixturesc]
	}
	MixturesC::SetCurrentMixturec [lindex $mixturesc 0]  
	GiD_Redraw     
    }
    return 0
}

proc MixturesC::RenameMixturec { cb } {
    variable dataclases
    variable mixturesc
    variable fields
    set mixturec [MixturesC::GetCurrentMixturec]   
    if { ![MixturesC::Exists $mixturec] } {
	#not exists
	return 1
    } 
    set new_name [tk_dialogEntryRAM .gid.askyn [= "Rename %s" $mixturec] \
	    [= "Enter new name of %s '%s'" [= "mixturec"] $mixturec] gidquestionhead "any" ""]
    if { $new_name != "--CANCEL--" } {
	foreach field $fields {
	    set dataclases($new_name,$field) $dataclases($mixturec,$field)
	}
	array unset dataclases $mixturec,*
	set i [MixturesC::GetIndex $mixturec] 
	lset mixturesc $i $new_name
	if { [winfo exists $cb] } {
	    $cb configure -values [MixturesC::GetMixturesc]           
	}
	MixturesC::SetCurrentMixturec $new_name       
	GiD_Redraw 
    }
    return 0
}

#apply the values of the current window
proc MixturesC::Apply { T } {
    variable dataclases
    variable current_value
    variable fields
    
    set mixturec [MixturesC::GetCurrentMixturec]    
    foreach field $fields {
	set dataclases($mixturec,$field) $current_value($field)
    }
    
    set dataclases($mixturec,table_value) [GridData::GetDataAllItems $T]
    GiD_Redraw        
}

#not MixturesC::StartDraw and MixturesC::EndDraw are not used, the Draw button is hidden (because unregister fail in GiD 11.1.5d)
#and the redraw procedure is registered when loading the problemtype, and each mixturec is draw depending on its 'visible' variable value
proc MixturesC::StartDraw { bdraw} {
    variable drawopengl
    set drawopengl [GiD_OpenGL register MixturesC::ReDraw]
    GiD_Redraw
    $bdraw configure -text [= "Finish draw"] -command [list MixturesC::EndDraw $bdraw]
}

proc MixturesC::EndDraw { bdraw } {
    variable drawopengl
    if { [info exists drawopengl] } { 
	#unregister the callback tcl procedure to be called by GiD when redrawing
	GiD_OpenGL unregister $drawopengl
	catch {unset drawopengl}
	GiD_Redraw
	if { $bdraw != "" } {
	    $bdraw configure -text [= "Draw"] -command [list MixturesC::StartDraw $bdraw]
	}
    }
}

proc MixturesC::DestroyMixturecWindow { W w } {   
    if { $W == $w } {
	MixturesC::EndDraw ""
	MixturesC::FillProblemDataFromTclData             
    }
}

proc MixturesC::Window { } {
    variable dataclases
    variable mixturesc
    variable current_mixturec
    variable current_value
    variable table
    variable nstrat
    variable nclass
    variable name
    
    #Verificamos que se hayan definido el num. Stratums&Classes
    set mixturelc [MixturesLC::GetCurrentMixturelc]
    
    if { $mixturelc == "" } {
	tk_dialogRAM .gid.tmpwin [= "Warning"] [= "Please, define Number of Stratums&Classes"] warning 0 OK
	Mixtures::CloseWindow
    } else {
	
	if { $mixturesc != "class" } {
	    MixturesC::SetDefaultValues
	    MixturesC::NewMixturec cb
	}
	
	#Recuperamos el n�mero de Classes (height == Classes)
	set nclass [MixturesLC::GetNclasses]
	
	MixturesC::FillTclDataFromProblemData    
	
	set w .gid.mixturec
	InitWindow $w [= "Classes definition"] PreMixturecWindowGeom MixturesC::Window
	if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
	set f [ttk::frame $w.fmixturesc]
	
	ttk::combobox $f.cb1 -textvariable MixturesC::current_mixturec -values [MixturesC::GetMixturesc] -state readonly
	bind $f.cb1 <<ComboboxSelected>> [list MixturesC::OnChangeSelectedMixturec %W]
	#ttk::button $f.bnew -image [gid_themes::GetImage new.png small_icons] -command [list MixturesC::NewMixturec $f.cb1]
	#GidHelp $f.bnew  [= "Create a new class"]
	#ttk::button $f.bdel -image [gid_themes::GetImage erase-cross.png small_icons] -command [list MixturesC::DeleteMixturec $f.cb1]
	#GidHelp $f.bdel  [= "Delete a class"]
	#ttk::button $f.bren -image [gid_themes::GetImage rename.png small_icons] -command [list MixturesC::RenameMixturec $f.cb1]
	#GidHelp $w.f.bren  [= "Rename a class"]   
	#grid $f.cb1 $f.bnew $f.bdel -sticky w
	grid $f -sticky new
	grid rowconfigure $f 0 -weight 1
	
	set f [ttk::frame $w.fmixturec]        
	
	package require fulltktree
	set columns ""
	foreach name [list [= "Class"] [= "D (m)"]] {
	    lappend columns [list 4 $name left text 1]
	}
	set T [fulltktree $f.t -columns $columns -showlines 0 -showheader 1 -expand 1]    
	set table $T
	$T configure -editbeginhandler GridData::EditBegin
	$T configure -editaccepthandler [list GridData::EditAccept]
	$T configure -deletehandler GridData::RemoveRows
	$T configure -contextualhandler_menu GridData::ContextualMenu    
	$T column configure all -button 0
	
	#ttk::frame $f.fbuttons
	#ttk::button $f.fbuttons.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $T 1]
	#GidHelp $f.fbuttons.b1 [_ "Pressing this button, one new line will be added to this window"]    
	#ttk::button $f.fbuttons.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $T 1]
	#GidHelp $f.fbuttons.b2 [_ "Pressing this button, one line is deleted from this window"]        
	
	#ttk::button $f.fbuttons.b3 -image [gid_themes::GetImage graphs.png small_icons] -text [_ "Graph"] \
	    #-command [list GridData::PlotCurveWin $T [= "Mixture"] ""]
	#GidHelp $f.fbuttons.b3 [_ "Pressing this button, a XY graph will be drawn"]
	
	grid $T -sticky nsew -columnspan 2
	#grid $f.fbuttons.b1 $f.fbuttons.b2 -sticky w    
	#grid $f.fbuttons -sticky ew
	
	
	
	grid columnconfigure $f {0} -weight 1
	grid rowconfigure $f {1 4} -weight 1
	grid $f -sticky nsew -padx 2 -pady 2
	GidHelp $f [= "Set mixturec parameters"]
	
	bind $T <<Paste>> [list ::GridData::Paste $T CLIPBOARD]
	bind $T <<PasteSelection>> [list ::GridData::Paste $T PRIMARY]
	bind $T <<Cut>> [list GridData::Cut $T]
	bind $T <<Copy>> [list GridData::Copy $T]
	bind [$T givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header
	
	#GridData::SetData $T $current_value(table_value)
	
	if { $mixturesc == "" } {
	    #Mixturesc::SetDefaultValues
	    #Creamos tantas filas como Stratums
	    for {set i 1} {$i <= $nclass} {incr i} {
		GridData::AppendRows $T 1
	    }
	} else {
	    set aux [llength $current_value(table_value)]
	    set nstratcurr [expr $aux/2]
	    #set nstratcurr [ llength $current_value ]
	    if { $nclass != $nstratcurr } {
		for {set i 1} {$i <= $nclass} {incr i} {
		    GridData::AppendRows $T 1
		}
	    } else {
		GridData::SetData $T $current_value(table_value)
	    }
	}      
	
	set f [ttk::frame $w.fbuttons -style BottomFrame.TFrame]
	ttk::button $f.bapply -text [= "Apply"] -command [list MixturesC::Apply $T] -style BottomFrame.TButton
	#ttk::button $f.bdraw -text [= "Draw"] -command [list MixturesC::StartDraw $f.bdraw] -style BottomFrame.TButton
	ttk::button $f.bclose -text [= "Close"] -command [list destroy $w] -style BottomFrame.TButton
	#grid $f.bapply $f.bdraw $f.bclose -sticky ew -padx 5 -pady 5
	grid $f.bapply $f.bclose -sticky ew -padx 5 -pady 5
	grid $f -sticky sew
	if { $::tcl_version >= 8.5 } { grid anchor $w.fbuttons center }        
	
	grid columnconfigure $w 0 -weight 1
	grid rowconfigure $w 1 -weight 1
	bind $w <Alt-c> "$f.bclose invoke"
	bind $w <Escape> "$f.bclose invoke"   
	bind $w <Destroy> [list +MixturesC::DestroyMixturecWindow %W $w] ;# + to add to previous script  
    }
}

proc MixturesC::CloseWindow { } {
    set w .gid.mixturec
    if { [winfo exists $w] } {
	close $w
    }
}
