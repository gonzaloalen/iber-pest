# Section.tcl  -*- TCL -*-
# Geor 3
#---------------------------------------------------------------------------
#This file is written in TCL lenguage 
#For more information about TCL look at: http://www.sunlabs.com/research/tcl/
#
#For more information about GID internals, check the program scripts.
#---------------------------------------------------------------------------

proc SectionWin { } {       
}

proc SectionMake { } {
    global GidPriv
    global SecPriv
    
    set list [GiD_Info conditions Line_Section geometry]
    set seclist [lsort -integer -index 3 [lsort -integer -index 4 $list]]
    
    #n� tramo
    set i 1 
    #n� secc tramo
    set j 1 
    
    foreach k $seclist {
        set aa [lindex $k 3]
        if {$aa != $i} {
            set i $aa
            set j 1
        }
        if {$aa == $i} {
            set SecPriv(nsecc,$aa) $j
            incr j
            lappend SecPriv(list,$aa) [lindex $k 1]
        }
        set SecPriv(tram) $aa
    }
    #   WindowMessage "$SecPriv(nsecc,1) $SecPriv(nsecc,2) $SecPriv(nsecc,3)"
    #   WindowMessage "$SecPriv(list,1) \b $SecPriv(list,2) \b $SecPriv(list,3)" 
    
    
    for {set i 1} {$i <= $SecPriv(tram)} {incr i} {
        foreach e $SecPriv(list,$i) {
            set p1 [GiD_Info parametric line $e coord 0]
            lappend SecPriv(punts,$i) $p1
            set p2 [GiD_Info parametric line $e coord 1]
            lappend SecPriv(vector,$i) [MathUtils::VectorDiff $p1 $p2] 
            
            lappend SecPriv(secp,$i) $p1 $p2
        }
    }
    
    # ubicaci�n de *.bas
    set basfile [file join $::IberPriv(dir) Templates CorMesh.bas]
    
    # directorio de trabajo
    
    set Project [GiD_Info project ModelName]        
    set Project [file normalize $Project]
    
    if { [file extension $Project] == ".gid" } {
        set Project [file root $Project]
    }
    if { $Project == "UNNAMED" } {
        tk_dialogRAM .gid.tmpwin error \
            "Before create section, a project title is needed. Save project to get it" \
            error 0 OK
        return
    }
    set directory $Project.gid
    if { [file pathtype $directory] == "relative" } {
        set directory [file join [pwd] $directory]
    }
    set adress [file join $directory MESHCor.msh]
    
    # escribe el archivo con la informaci�n de la malla        
    set meshfile [GiD_Process MEscape Files WriteForBAS \"$basfile\" \"$adress\" escape]
    
    #llama al c�lculo de las distancias
    
    SectionDistance 
    
    # escribe las secciones en el archivo de informaci�n
    set f [open $adress a]
    puts $f $SecPriv(tram)
    
    for {set k 1} {$k <= $SecPriv(tram)} {incr k} {
        puts $f $SecPriv(nsecc,$k)
        # WindowMessage "$SecPriv(nsecc,$k)"      
        set npp [llength $SecPriv(vector,$k)] 
        #WindowMessage "$SecPriv(vector,$k)"    
        set i 1
        set iii 1
        for {set i 1} {$i <= $npp} {incr i} {
            set a [lindex $SecPriv(secp,$k) [expr int($iii-1)]]
            set b [lindex $SecPriv(secp,$k) $iii]
            set g [lindex $SecPriv(vector,$k) [expr int($i-1)]]
            puts $f "$a $b $g"
            incr iii 2
        }
        
        set ndist [llength $SecPriv(dist,$k)]
        #WindowMessage "$ndist" 
        #WindowMessage "$SecPriv(nsecc,$k)" 
        for {set j 1} {$j <= $ndist } {incr j 3} {
            set c [lindex $SecPriv(dist,$k) [expr int($j-1)]]
            set d [lindex $SecPriv(dist,$k) $j]
            set e [lindex $SecPriv(dist,$k) [expr int($j+1)]]
            puts $f "$c $d $e"   
        }
    }
    close $f
    
    #llama al programa que hace los cortes   
    SectionCutsInvoke $directory
    
    #llama al dibujo de la seccion    
    SectionDraw $directory
    
    #limpia las variables
    unset SecPriv
}

proc SectionCutsInvoke {dir} {
    global SecPriv
    
    set program [Iber::GetFullPathExe Cortes.exe]
    set fil [open |\"$program\" r+]
    puts $fil \"$dir\"
    
    flush $fil
    
    tk_dialogRAM .gid.tmpwin "Make Section" \
        "1D Section Created" \
        info 0 OK
}

proc SectionDraw {dir } {
    global SecPriv
    
    set secc [file join $dir secc.txt]
    set fefe [open $secc r]
    set meshf [file join $dir MeshSec.txt]
    set mefe [open $meshf w]
    set meshfe [file join $dir MeshElem.txt]
    set mefefe [open $meshfe w]
    GiD_Process MEscape Utilities Variables CreateAlwaysNewPoint 1
    GiD_Process MEscape Utilities Layer New seccions escape
    set nnodes [GiD_Info Mesh MaxNumNodes]
    set nelems [GiD_Info Mesh MaxNumElements]
    set npoint [GiD_Info Geometry MaxNumPoints]
    #    WindowMessage "$nelems $nnodes $npoint"
    
    while { ![eof $fefe] } {
        gets $fefe gg
        set np [lindex $gg 3]
        set nsec [lindex $gg 1]
        set ntram [lindex $gg 0]
        puts $mefe "$nsec $np"
        #           set nsec [expr int([lindex $gg 0]-1)]
        #           set p1 [lindex $SecPriv(secp) $nsec]
        set nlines [expr int([GiD_Info Geometry MaxNumLines]+1)]
        
        GiD_Process Mescape Geometry Create Line Number $nlines
        for {set i 1} {$i <= $np} {incr i} { 
            gets $fefe gg
            set x [lindex $gg 0]
            set y [lindex $gg 1]
            set z [lindex $gg 2]
            
            GiD_Process $x,$y,$z
            GiD_AssignData condition 1D_Section points "$ntram $nsec" [expr int($npoint+$i)]
            GiD_Mesh create node [expr int($nnodes+$i)] "$x $y $z" 
            GiD_AssignData condition 1D_Section nodes "$ntram $nsec" [expr int($nnodes+$i)]
            set nodel [expr int($nnodes+$i)]
            puts $mefe "$nodel $nsec $z"
        }
        GiD_Process MEscape 
        
        set nl [expr int($np-1)]
        for {set k 1} {$k < $np } {incr k} {
            GiD_AssignData condition Line_Section lines "$ntram $nsec" $nlines
            set nlines [expr int($nlines+1)]
        }
        
        for {set j 1} {$j < $np } {incr j} {
            set nelems [expr int($nelems+1)]
            set nnodes [expr int($nnodes+1)]
            GiD_Mesh create element $nelems linear 2 \
                "$nnodes [expr int($nnodes+1)]" 
            puts $mefe "$nelems $nsec"
        }
        set nelems [expr int($nelems+1)]
        set nnodes [expr int($nnodes+1)]
        
        set npoint [expr int($npoint+$i-1)]
        #       WindowMessage "$npoint"      
        GiD_Process MEscape 
    }
    
    #WindowMessage "$nelems $nnodes"
    puts $mefefe "$nelems $nnodes"
    close $fefe
    close $mefe
    close $mefefe
    
    GiD_Process MEscape Utilities Variables CreateAlwaysNewPoint 0
    GiD_Process MEscape
}

proc SectionDistance { } {
    global SecPriv
    
    set plist [GiD_Info conditions 1D_Section geometry]
    set pplist [lsort -integer -index 3 [lsort -integer -index 4 $plist]]
    
    set i 1   
    foreach k $pplist {
        set aa [lindex $k 3]
        if {$aa != $i} { set i $aa }
        if {$aa == $i} { lappend ppplist($aa) [lindex $k 1] }      
    }    
    set i 1
    for {set i 1} {$i <= $SecPriv(tram)} {incr i} {
        set ppp 0
        set h1 1
        foreach ep $ppplist($i) {    
            #            set pp [lindex $ep 1]
            set aa [GiD_Geometry get point $ep]         
            set bb [list [lindex $aa 1] [lindex $aa 2] [lindex $aa 3]]            
            lappend pppplist($i) $bb 
            set ppp [expr int($ppp+1)]
        }
        set h1 1
        for {set h 1} {$h <= $ppp} {incr h} {
            lappend clist [lindex $SecPriv(secp,$i) [expr ($h1-1)]]
            lappend clist [lindex $pppplist($i) [expr ($h-1)]]
            lappend clist [lindex $SecPriv(secp,$i) $h1]
            set h1 [expr $h1+2]
        }        
        set numsec [llength $clist]
        set numsec [expr int($numsec-3)]
        #      set numsec $SecPriv(nsecc,$i)
        
        for {set k 1} {$k <= $numsec} {incr k} { 
            set ii [expr int($k-1)]
            set p1 [lindex $clist $ii]
            
            set jj [expr int($k+2)]
            set p2 [lindex $clist $jj]
            
            lappend SecPriv(dist,$i) [MathUtils::VectorDistance $p1 $p2] 
        }
        
        lappend SecPriv(dist,$i) 0.0 0.0 0.0
        unset clist
    }
}

proc 1DSection { } {
    
    set Project [GiD_Info project ModelName]
    set Project [file normalize $Project]
    
    if { [file extension $Project] == ".gid" } {
        set Project [file root $Project]
    }
    if { $Project == "UNNAMED" } {
        tk_dialogRAM .gid.tmpwin error \
            "Before create section, a project title is needed. Save project to get it" \
            error 0 OK
        return
    }
    set directory $Project.gid
    if { [file pathtype $directory] == "relative" } {
        set directory [file join [pwd] $directory]
    }
    
    set secc [file join $directory varios.txt]
    set fefe [open $secc r]
    
    set buffer [read $fefe]
    
    close $fefe
    
    set buffer
}
