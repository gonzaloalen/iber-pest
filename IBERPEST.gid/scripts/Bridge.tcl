#require GiD version 11.1.5d or higher, because of some changes of GridData (DataWindows.tcl) to edit bridge table and draw its graphs

namespace eval Bridge {
    variable p0
    variable p1
    variable width
    variable mode ;#0 insert segment, 1 insert rectangle
    variable interpolation ;#0 interpolate nodes from mesh, 1interpolate nodes from segment ends
    variable apply_condition ;#1 to apply Weir-Gate 2D_Internal_Condition
    variable Free_Pressured_Flow_Cd ;#Free_Gate_Cd
    variable Submerged_Pressured_Flow_Cd ;#Submerged_Gate_Cd
    variable Deck_Cd ;#Weir_Cd
    variable Gauge_Number ;#Gauge_Number
    variable bridge_top_bottom_values
    variable pile_values
    variable draw_terrain ;#to calculate or not drawing the bridge curves
    variable force_bridge_points ;#to force the bridge points to be in the inserted mesh
    variable force_pile_points ;#to force the pile points to be in the inserted mesh
    variable BridgeData ;#to save the imported bridges
    variable BridgesImplemented ;#number of implemented bridges
    variable refpoints 0
    variable Ref_Point_1
    variable Ref_Point_2

    set BridgesImplemented 0
}

proc Bridge::Init { } {
    variable local_edges_triangle
    variable local_edges_quadrilateral
    set local_edges_triangle(0,0) 0
    set local_edges_triangle(0,1) 1
    set local_edges_triangle(1,0) 1
    set local_edges_triangle(1,1) 2
    set local_edges_triangle(2,0) 2
    set local_edges_triangle(2,1) 0

    set local_edges_quadrilateral(0,0) 0
    set local_edges_quadrilateral(0,1) 1
    set local_edges_quadrilateral(1,0) 1
    set local_edges_quadrilateral(1,1) 2
    set local_edges_quadrilateral(2,0) 2
    set local_edges_quadrilateral(2,1) 3
    set local_edges_quadrilateral(3,0) 3
    set local_edges_quadrilateral(3,1) 0
}

proc Bridge::SetDefaultValues { } {
    variable mode
    variable interpolation
    variable apply_condition
    variable Free_Pressured_Flow_Cd ;#Free_Gate_Cd
    variable Submerged_Pressured_Flow_Cd ;#Submerged_Gate_Cd
    variable Deck_Cd ;#Weir_Cd
    variable Gauge_Number ;#Gauge_Number
    variable bridge_top_bottom_values
    variable pile_values
    variable draw_terrain
    variable force_bridge_points
    variable force_pile_points
    variable refpoints
    variable Ref_Point_1
    variable Ref_Point_2

    set mode 0 ;#0 insert segment, 1 insert rectangle
    set interpolation 0 ;#0 interpolate nodes from mesh, 1interpolate nodes from segment ends
    set apply_condition 1;#1 to apply Weir-Gate 2D_Internal_Condition
    set Free_Pressured_Flow_Cd 0.6
    set Submerged_Pressured_Flow_Cd 0.8
    set Deck_Cd 1.7
    set Gauge_Number 1
    set bridge_top_bottom_values {0.0 0.0 0.0 1.0 0.0 0.0}
    set pile_values {0.0 0.0}
    set draw_terrain 1
    set force_bridge_points 0
    set force_pile_points 0
    set refpoints 0
    set Ref_Point_1 "0 0 0"
    set Ref_Point_2 "0 0 0"

}

proc Bridge::UpdateBridgeOptionsState { frm } {
    variable apply_condition
    if { $apply_condition } {
	grid $frm
    } else {
	grid remove $frm
    }
} 

#type: bridge or levee
proc Bridge::Window { type } {
    variable p0
    variable p1
    variable width
    variable mode
    variable interpolation
    variable apply_condition
    variable Free_Pressured_Flow_Cd ;#Free_Gate_Cd
    variable Submerged_Pressured_Flow_Cd ;#Submerged_Gate_Cd
    variable Deck_Cd ;#Weir_Cd
    variable Gauge_Number ;#Gauge_Number
    variable bridge_top_bottom_values
    variable pile_values
    variable force_bridge_points
    variable force_pile_points
    variable refpoints
    variable Ref_Point_1
    variable Ref_Point_2

    if { ![info exists mode] } {
	Bridge::SetDefaultValues
    }

    set w .gid.bridge
    if { $type == "levee" } {
	set title [= "Levee"]
	set Bridge::apply_condition 0
	set Bridge::interpolation 1;#z from segment
    } elseif { $type == "bridge" } {
	set title [= "Bridge"]
	set Bridge::mode 0 ;#insert segment
	#mode==1 insert rectangle is not finished now for bridges:
	#then the pile sizes perpendicular to bridge axis must be entered
	#options:
	# a) Constraint the mesh to represent the piles as rectangles (or polygonal circles)
	#    and must apply different condition values to piles (Bridge_Opening_Percent=0%) and rest of bridge triangles (100%)
	# b) Not constraint the mesh to the piles, and take in account the effect of these piles
	#    in the values of the applied conditions:
	#    Bridge_Opening_Percent: for each bridge element must calculate the percent of area substracting the 'virtually embedded' piles
	#    this effective area is relatively difficult to be calculated
	# In both cases must be used a 'surfacic condition' of type
	# CONDITION: Surface_Bridge
	# CONDTYPE: over surfaces
	# CONDMESHTYPE: over body elements
	# and require also calculate these fields:
	# Lower_deck_elevation (e.g elevation of the center of each triangle projected in the bridge axis)
	# Deck_elevation       (similar)
	# Deck_Area_Percent (hidden and not used, always 100% now)
	set Bridge::apply_condition 1
	set Bridge::interpolation 0;#z from mesh
    } else {
	WarnWinText "Bridge::Window. Unexpected case $type"
	return 1
    }
    InitWindow $w $title Pre${type}WindowGeom [list Bridge::Window $type]
    if { ![winfo exists $w] } return ;# windows disabled || usemorewindows == 0
    set f [ttk::frame $w.fpoints]
    foreach i {0 1} text [list [= "Start"] [= "End"]] {
	ttk::label $f.lp${i} -text $text
	foreach axis {0 1 2} {
	    ttk::entry $f.ep${i}${axis} -textvariable ::Bridge::p${i}($axis)
	}
	ttk::button $f.bp${i} -image [gid_themes::GetImage "point.png" small_icons] -command [list Bridge::GetCoordinates $i $text]
	grid $f.lp${i} $f.ep${i}0 $f.ep${i}1 $f.ep${i}2 $f.bp${i} -sticky ew
    }
    grid columnconfigure $w.fpoints {1 2 3} -weight 1
    grid $w.fpoints -sticky new -padx 2 -pady 2
    GidHelp $w.fpoints [= "Set the start and end coordinates of the bridge axis."]

    ttk::frame $w.fwidth
    ttk::label $w.fwidth.lwidh -text [= "Width (m)"]
    ttk::entry $w.fwidth.ewidth -textvariable Bridge::width
    grid $w.fwidth.lwidh $w.fwidth.ewidth -sticky ew

    grid columnconfigure $w.fwidth {1} -weight 1
    grid $w.fwidth -sticky new -padx 2 -pady 2
    GidHelp $w.fwidth [= "Set the width of the bridge, in meters (used as rectangle width, and also in case of segment to find the band of elements to be replaced)"]

    ttk::labelframe $w.foptions -text [= "Options"]
    GidHelp $w.foptions [= "Set some additional options"]
    if { $type == "levee" } {
	ttk::radiobutton $w.foptions.rmode0 -text [= "Insert segment"] -variable Bridge::mode -value 0
	ttk::radiobutton $w.foptions.rmode1 -text [= "Insert rectangle"] -variable Bridge::mode -value 1
	GidHelp $w.foptions.rmode0 [= "Insert mesh edges to represent the bridge axis"]
	GidHelp $w.foptions.rmode1 [= "Insert mesh elements to represent the bridge rectangle"]
	grid $w.foptions.rmode0 $w.foptions.rmode1 -sticky ew
    }

    #ttk::radiobutton $w.foptions.rinterpolation0 -text [= "Interpolate Z from mesh"] -variable Bridge::interpolation -value 0
    #ttk::radiobutton $w.foptions.rinterpolation1 -text [= "Interpolate Z from segment"] -variable Bridge::interpolation -value 1
    #GidHelp $w.foptions.rinterpolation0 [= "set the elevation of inserted nodes projected on the terrain"]
    #GidHelp $w.foptions.rinterpolation1 [= "set the elevation of inserted nodes projected on the start-end segment"]
    #grid $w.foptions.rinterpolation0 $w.foptions.rinterpolation1 -sticky ew
    #ttk::checkbutton $w.foptions.capply_condition -text [= "Apply bridge conditions"] -variable Bridge::apply_condition \
	-command [list Bridge::UpdateBridgeOptionsState $w.fbridge]
    #GidHelp $w.foptions.capply_condition [= "Apply Hidrodynamic a Conditions to represent the bridge as a combination of weir-gate expressions"]
    #grid $w.foptions.capply_condition -sticky w -columnspan 2

    if { $type == "bridge" } {
	ttk::checkbutton $w.foptions.cforce_bridge_points -text [= "Force bridge points"] -variable Bridge::force_bridge_points
	ttk::checkbutton $w.foptions.cforce_pile_points -text [= "Force pile points"] -variable Bridge::force_pile_points
	GidHelp $w.foptions.cforce_bridge_points [= "Force the points that define the bridge shape to be inserted in the mesh"]
	GidHelp $w.foptions.cforce_pile_points  [= "Force the points that define the start and end of each pile shape to be inserted in the mesh"]
	grid $w.foptions.cforce_bridge_points  $w.foptions.cforce_pile_points -sticky w
	
	ttk::frame $w.foptions.frefpointsdef
	ttk::label $w.foptions.frefpointsdeftext -text [= "Ref. Points Definition:"]
	ttk::radiobutton $w.foptions.frefpointsstandard -text [= "Standard"] -variable Bridge::refpoints -value 0
	ttk::radiobutton $w.foptions.frefpointsuser -text [= "User defined"] -variable Bridge::refpoints -value 1
	ttk::label $w.foptions.frefpointscoordtext -text [= "Select upstream and downstream points (User Defined only):"]
	ttk::entry $w.foptions.frefpoint1 -textvariable Bridge::Ref_Point_1
	ttk::button $w.foptions.brefpoint1 -image [gid_themes::GetImage "point.png" small_icons] -command [list Culvert::PickPointInSurfaceOrElementCmd $w.foptions.frefpoint1]
	ttk::entry $w.foptions.frefpoint2 -textvariable Bridge::Ref_Point_2
	ttk::button $w.foptions.brefpoint2 -image [gid_themes::GetImage "point.png" small_icons] -command [list Culvert::PickPointInSurfaceOrElementCmd $w.foptions.frefpoint2]
	
	grid $w.foptions.frefpointsdeftext $w.foptions.frefpointscoordtext -sticky w
	grid $w.foptions.frefpointsstandard  $w.foptions.frefpoint1 $w.foptions.brefpoint1 -sticky w
	grid $w.foptions.frefpointsuser $w.foptions.frefpoint2 $w.foptions.brefpoint2 -sticky w
	
    }

    grid columnconfigure $w.foptions {0 1 2} -weight 1
    grid $w.foptions -sticky new -padx 2 -pady 2

    if { $type == "bridge" } {
	ttk::labelframe $w.fbridge -text [= "Bridge definition. Relative distance from 0.0 to 1.0. Bridge top and bottom are the elevations in meters"]
	ttk::frame $w.fbridge.fcoefficients
	foreach item {Free_Pressured_Flow_Cd Submerged_Pressured_Flow_Cd Deck_Cd Gauge_Number} {
	    set txt [string map {_ { }} $item]
	    ttk::label $w.fbridge.fcoefficients.l$item -text [= $txt]
	    ttk::entry $w.fbridge.fcoefficients.e$item -textvariable Bridge::$item
	}

	foreach item {Free_Pressured_Flow_Cd Submerged_Pressured_Flow_Cd Deck_Cd Gauge_Number} {
	    grid $w.fbridge.fcoefficients.l$item $w.fbridge.fcoefficients.e$item -sticky ew
	}
	grid $w.fbridge.fcoefficients -sticky ew
	grid columnconfigure $w.fbridge.fcoefficients {1} -weight 1

	package require fulltktree
	set columns ""
	foreach name [list [= "Relative distance"] [= "Bridge top (m)"] [= "Bridge bottom (m)"]] {
	    lappend columns [list 9 $name left text 1]
	}
	set T [fulltktree $w.fbridge.t -columns $columns -showlines 0 -showheader 1 -expand 1]
	$T configure -editbeginhandler GridData::EditBegin
	$T configure -editaccepthandler [list GridData::EditAccept]
	$T configure -deletehandler GridData::RemoveRows
	$T configure -contextualhandler_menu GridData::ContextualMenu
	$T column configure all -button 0

	ttk::frame $w.fbridge.fbuttons
	ttk::button $w.fbridge.fbuttons.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $T 1]
	GidHelp $w.fbridge.fbuttons.b1 [_ "Pressing this button, one new line will be added to this window"]
	ttk::button $w.fbridge.fbuttons.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $T 1]
	GidHelp $w.fbridge.fbuttons.b2 [_ "Pressing this button, one line is deleted from this window"]

	grid $T -sticky nsew -columnspan 2
	grid $w.fbridge.fbuttons.b1 $w.fbridge.fbuttons.b2 -sticky w
	grid $w.fbridge.fbuttons -sticky ew

	ttk::labelframe $w.fbridge.fpiles -text [= "Bridge piles"]
	set columns ""
	foreach name [list [= "Relative distance"] [= "Width (m)"]] {
	    lappend columns [list 9 $name left text 1]
	}
	set Tpiles [fulltktree $w.fbridge.fpiles.t -columns $columns -showlines 0 -showheader 1 -expand 1]
	$Tpiles configure -editbeginhandler GridData::EditBegin
	$Tpiles configure -editaccepthandler [list GridData::EditAccept]
	$Tpiles configure -deletehandler GridData::RemoveRows
	$Tpiles configure -contextualhandler_menu GridData::ContextualMenu
	$Tpiles column configure all -button 0

	ttk::frame $w.fbridge.fpiles.fbuttons
	ttk::button $w.fbridge.fpiles.fbuttons.b1 -image DownImage -text [_ "Add row"] -command [list GridData::AppendRows $Tpiles 1]
	GidHelp $w.fbridge.fpiles.fbuttons.b1 [_ "Pressing this button, one new line will be added to this window"]
	ttk::button $w.fbridge.fpiles.fbuttons.b2 -image UpImage -text [_ "Remove row"] -command [list GridData::RemoveLastRows $Tpiles 1]
	GidHelp $w.fbridge.fpiles.fbuttons.b2 [_ "Pressing this button, one line is deleted from this window"]

	grid $Tpiles -sticky nsew
	grid $w.fbridge.fpiles.fbuttons.b1 $w.fbridge.fpiles.fbuttons.b2 -sticky w
	grid $w.fbridge.fpiles.fbuttons -sticky ew
	grid $w.fbridge.fpiles -sticky ew -columnspan 2
	grid columnconfigure $w.fbridge.fpiles {0} -weight 1
	grid rowconfigure $w.fbridge.fpiles {0} -weight 1

	ttk::frame $w.fbridge.fdraw
	ttk::button $w.fbridge.fdraw.bdraw -image [gid_themes::GetImage graphs.png small_icons] -text [_ "Graph"] \
	    -command [list Bridge::PressDrawGraph $T $Tpiles]
	ttk::checkbutton $w.fbridge.fdraw.cdraw_terrain -text [= "Draw terrain"] -variable Bridge::draw_terrain
	GidHelp $w.fbridge.fdraw.bdraw [_ "Pressing this button, a XY graph will be drawn"]

	grid $w.fbridge.fdraw.bdraw $w.fbridge.fdraw.cdraw_terrain -sticky ew
	grid $w.fbridge.fdraw -sticky ew
	#grid columnconfigure $w.fbridge.fdraw {0 1} -weight 1
	if { $::tcl_version >= 8.5 } { grid anchor $w.fbridge.fdraw center }


	grid columnconfigure $w.fbridge {0} -weight 1
	grid rowconfigure $w.fbridge {1 3} -weight 1
	grid $w.fbridge -sticky nsew -padx 2 -pady 2
	GidHelp $w.fbridge [= "Set bridge parameters"]

	bind $T <<Paste>> [list ::GridData::Paste $T CLIPBOARD]
	bind $T <<PasteSelection>> [list ::GridData::Paste $T PRIMARY]
	bind $T <<Cut>> [list GridData::Cut $T]
	bind $T <<Copy>> [list GridData::Copy $T]
	bind [$T givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header

	bind $Tpiles <<Paste>> [list ::GridData::Paste $Tpiles CLIPBOARD]
	bind $Tpiles <<PasteSelection>> [list ::GridData::Paste $Tpiles PRIMARY]
	bind $Tpiles <<Cut>> [list GridData::Cut $Tpiles]
	bind $Tpiles <<Copy>> [list GridData::Copy $Tpiles]
	bind [$Tpiles givetreectrl] <Control-c> "" ;#to remove the default binding of fulltktree that capture also the header

	GridData::SetData $T $bridge_top_bottom_values
	GridData::SetData $Tpiles $pile_values
    } else {
	set T ""
	set Tpiles ""
    }

    ttk::frame $w.frmButtons -style BottomFrame.TFrame -padding 5
    ttk::button $w.frmButtons.bapply -text [_ "Apply"] -command [list Bridge::Apply $T $Tpiles]
    ttk::button $w.frmButtons.bclose -text [_ "Close"] -command [list Bridge::DestroyWindow $w $T]
    grid $w.frmButtons.bapply $w.frmButtons.bclose -sticky ew
    if { $::tcl_version >= 8.5 } { grid anchor $w.frmButtons center }

    grid $w.frmButtons -sticky sew -row 5 -padx 2 -pady 2
    grid rowconfigure $w 3 -weight 1
    grid columnconfigure $w 0 -weight 1

    bind $w <Alt-c> [list $w.frmButtons.bclose invoke]
    bind $w <Escape> [list $w.frmButtons.bclose invoke]
    bind $w <Return> [list $w.frmButtons.bapply invoke]
}

proc Bridge::PlotTerrainGraph { xyplot points } {
    set serie 2
    $xyplot dataconfig $serie -colour blue -type both -symbol plus
    $xyplot legend $serie [= "Z terrain"]
    foreach {x y} $points {
	$xyplot plot $serie $x $y
	$xyplot bindlast $serie <Enter> [list GridData::ShowAnnotation $xyplot %W]
    }
}
proc Bridge::PlotPiles { xyplot points } {
    set serie 3
    $xyplot dataconfig $serie -colour gray -type both -symbol plus
    $xyplot legend $serie [= "Piles"]
    foreach {x ymin ymax} $points {
	$xyplot interval $serie $x $ymin $ymax
	#$xyplot yband $xmin $xmax
    }
}

proc Bridge::PressDrawGraph { T Tpiles } {
    variable bridge_top_bottom_values
    variable pile_values

    variable draw_terrain
    set title [= "Bridge shape"]
    set series_options {{-filled up -fillcolour white} {-filled down -fillcolour white}}
    set xyplot [GridData::PlotCurveWin $T $title $series_options]
    if { $draw_terrain } {
	#calculate and draw also terrain graph
	variable p0
	variable p1
	if { [info exists p0(0)] && [info exists p1(0)] } {
	    set c0 [list $p0(0) $p0(1) 0.0]
	    set c1 [list $p1(0) $p1(1) 0.0]
	    set relative_ts [list]
	    foreach {t ztop zbottom} [GridData::GetDataAllItems $T] {
		lappend relative_ts $t
	    }
	    lassign [Bridge::CalculateLineExtraEdgesAndNodes $c0 $c1 $relative_ts] extra_edges extra_nodes
	    set selected_elements [Bridge::SelectLineTrianglesOrQuadrilaterals $c0 $c1 1e-5]
	    set extra_nodes [Bridge::MapExtraNodesToMesh $selected_elements $extra_nodes]
	    set points [list]
	    foreach t $relative_ts node $extra_nodes {
		lappend points $t [lindex $node 3]
	    }
	    set c [$xyplot canvas]
	    if { [string match {[0-9]*} $c] } {
		set c [string range $c 2 end]
	    }
	    if { [winfo exists $c] } {
		set w [winfo toplevel $c]
		Bridge::PlotTerrainGraph $xyplot $points
		bind $w <Configure> +[list Bridge::PlotTerrainGraph $xyplot $points]
	    }
	}
    }
    set draw_piles 1
    if { $draw_piles } {
	set bridge_top_bottom_values [GridData::GetDataAllItems $T]
	set pile_values [GridData::GetDataAllItems $Tpiles]

	package require math::interpolate
	#set table_name [math::interpolate::defineTable auxname {distance ztop zbottom} $bridge_top_bottom_values]
	set bridge_bottom [list]
	foreach {t z_bridge_top z_bridge_bottom} $bridge_top_bottom_values {
	    lappend bridge_bottom $t $z_bridge_bottom
	}

	set points [list]
	foreach {t width} $pile_values {
	    #lassign [Bridge::GetEdgeBridgeTopAndBottomFromRelativeT $table_name $t]  z_bridge_top z_bridge_bottom
	    if { 0 } {
		variable p0
		variable p1
		set c0 [list $p0(0) $p0(1) 0.0]
		set c1 [list $p1(0) $p1(1) 0.0]
		set incre [MathUtils::VectorDiff $c0 $c1]
		set modulus [MathUtils::VectorModulus $incre]
		set relative_half_width [expr {0.5*$width/$modulus}]
		set t0 [expr {$t-$relative_half_width}]
		set z_bridge_bottom [::math::interpolate::interp-linear $bridge_bottom $t0]
		lappend points $t0 0.0 $z_bridge_bottom
		set t1 [expr {$t+$relative_half_width}]
		set z_bridge_bottom [::math::interpolate::interp-linear $bridge_bottom $t1]
		lappend points $t1 0.0 $z_bridge_bottom
	    } else {
		set z_bridge_bottom [::math::interpolate::interp-linear $bridge_bottom $t]
		lappend points $t 0.0 $z_bridge_bottom
	    }
	}

	set c [$xyplot canvas]
	if { [string match {[0-9]*} $c] } {
	    set c [string range $c 2 end]
	}
	if { [winfo exists $c] } {
	    set w [winfo toplevel $c]
	    Bridge::PlotPiles $xyplot $points
	    bind $w <Configure> +[list Bridge::PlotPiles $xyplot $points]
	}
    }
}

proc Bridge::GetCoordinates { i text } {
    variable p${i}
    set p [GidUtils::GetCoordinates $text PointInElement MESHUSE]
    if { $p != "" } {
	foreach axis {0 1 2} {
	    set p${i}($axis) [lindex $p $axis]
	}
    }
}

proc Bridge::Apply { T Tpiles } {
    variable p0
    variable p1
    variable width
    variable mode
    variable interpolation
    variable apply_condition
    variable Free_Pressured_Flow_Cd ;#Free_Gate_Cd
    variable Submerged_Pressured_Flow_Cd ;#Submerged_Gate_Cd
    variable Deck_Cd ;#Weir_Cd
    variable Gauge_Number
    variable bridge_top_bottom_values
    variable force_bridge_points
    variable force_pile_points
    variable refpoints
    variable Ref_Point_1
    variable Ref_Point_2

    if { ![info exists p0(0)] || ![info exists p1(0)] } {
	WarnWinText [= "Set the start and end coordinates of the bridge axis."]
	return 1
    }
    if { ![info exists width] || ![string is double $width] || $width < 0 } {
	WarnWinText [= "Set the width of the bridge, in meters"]
	return 1
    }
    set bridge_top_bottom_values [GridData::GetDataAllItems $T]
    if { $apply_condition } {
	set xprev -1.0
	set i 0
	foreach {x ztop zbottom} $bridge_top_bottom_values {
	    incr i
	    if { $ztop < $zbottom } {
		WarnWinText [= "Wrong bridge z. row %s" $i]
		return 1
	    }
	    if { $x<0.0 || $x > 1.0 || $x<=$xprev} {
		WarnWinText [= "Wrong bridge x, relative distance must an increasing number from 0.0 to 1.0. row %s" $i]
		return 1
	    }
	    set xprev $x
	}
    }

    set c0 [list $p0(0) $p0(1) $p0(2)]
    set c1 [list $p1(0) $p1(1) $p1(2)]
    set bridge_coefficients [list $Free_Pressured_Flow_Cd $Submerged_Pressured_Flow_Cd $Deck_Cd $Gauge_Number]
    Bridge::InsertStructure $refpoints $Ref_Point_1 $Ref_Point_2 $c0 $c1 $width $mode $interpolation $apply_condition $bridge_top_bottom_values $bridge_coefficients \
	$force_bridge_points $T $force_pile_points $Tpiles
    return 0
}

proc Bridge::DestroyWindow { w T } {
    variable bridge_top_bottom_values
    set bridge_top_bottom_values [GridData::GetDataAllItems $T]
    destroy $w
}

proc Bridge::GetTsFromNumDivisions { num_divisions } {
    set relative_ts [list]
    for {set i 0} {$i<= $num_divisions} {incr i} {
	lappend relative_ts [expr {double($i)/$num_divisions}]
    }
    return $relative_ts
}

proc Bridge::GetTsFromBoundaryEdgesAndNodes { c0 c1 boundary_edges boundary_nodes } {
    set ts [list]
    set node_min -1
    set incre [MathUtils::VectorDiff $c0 $c1]
    set direction [MathUtils::VectorNormalized $incre]
    foreach node $boundary_nodes {
	set node_id [lindex $node 0]
	set node_coordinates($node_id) [lrange $node 1 3]
	set relative_position [MathUtils::VectorDiff $c0 $node_coordinates($node_id)]
	set projection [MathUtils::VectorDotProd $direction $relative_position]
	if { $node_min == -1 } {
	    set node_min $node_id
	    set projection_min $projection
	    set node_max $node_id
	    set projection_max $projection
	} else {
	    if { $projection_min > $projection } {
		set node_min $node_id
		set projection_min $projection
	    } elseif { $projection_max < $projection } {
		set node_max $node_id
		set projection_max $projection
	    }
	}
    }
    if { $node_min != -1 } {
	set pos_first [lsearch -integer -index 0 $boundary_edges $node_min]
	if { $pos_first != -1 } {
	    set boundary_edges_reordered [concat [lrange $boundary_edges $pos_first end] [lrange $boundary_edges 0 [expr {$pos_first-1}]]]
	    #calculate ts with half edges (arbitrary startig in $node_min and ending in $node_max, ignore the other half!!)
	    set lengths [list]
	    set total_length 0.0
	    foreach edge $boundary_edges_reordered {
		lassign $edge n0 n1
		set length [MathUtils::VectorDistance $node_coordinates($n0) $node_coordinates($n1)]
		lappend lengths $length
		set total_length [expr {$total_length+$length}]
		if { $n1 == $node_max } {
		    break
		}
	    }
	    set t 0.0
	    set ts [list $t]
	    foreach length $lengths {
		set t [expr {$t+$length/$total_length}]
		lappend ts $t
	    }
	    lset ts end 1.0
	} else {
	    set ts [list $t]
	}
    } else {
	set ts [list $t]
    }
    return $ts
}

proc Bridge::GetTsOfBridge { T } {
    set relative_ts [list]
    foreach {t ztop zbottom} [GridData::GetDataAllItems $T] {
	lappend relative_ts $t
    }
    return $relative_ts
}

proc Bridge::GetTsOfPiles { Tpiles } {
    variable p0
    variable p1

    set c0 [list $p0(0) $p0(1) 0.0]
    set c1 [list $p1(0) $p1(1) 0.0]
    set incre [MathUtils::VectorDiff $c0 $c1]
    set modulus [MathUtils::VectorModulus $incre]
    foreach {t width} [GridData::GetDataAllItems $Tpiles] {
	set relative_half_width [expr {0.5*$width/$modulus}]
	set t0 [expr {$t-$relative_half_width}]
	if { $t0 < 0.0 } {
	    set t0 0.0
	}
	lappend relative_ts $t0
	set t1 [expr {$t+$relative_half_width}]
	if { $t1 > 1.0 } {
	    set t1 1.0
	}
	lappend relative_ts $t1
    }

    return $relative_ts
}

#insert exactly the bridge points (not closer than min_relative_dt)
#and subdivide spans greater than max_relative_dt
proc Bridge::EnhanceTs { ts min_relative_dt max_relative_dt} {
    set relative_ts [list]
    set t_prev ""
    foreach t $ts {
	if { $t_prev!="" } {
	    set relative_dt [expr {$t-$t_prev}]
	    if { $relative_dt < $min_relative_dt } {
		#avoid too short intervals
		continue
	    } elseif { $relative_dt > $max_relative_dt } {
		#insert equi-spaced ts in this interval
		set num_divisions [expr {ceil($relative_dt/$max_relative_dt)}]
		set increment [expr {$relative_dt/double($num_divisions)}]
		for {set i 1} {$i< $num_divisions} {incr i} {
		    lappend relative_ts [expr {$t_prev+double($i)*$increment}]
		}
	    }
	}
	lappend relative_ts $t
	set t_prev $t
    }
    set relative_ts [lsort -unique -real $relative_ts]
    return $relative_ts
}

proc Bridge::InsertTs { ts relative_ts } {
    lappend relative_ts {*}$ts
    set relative_ts [lsort -unique -real $relative_ts] ;#sort and remove repeated values
    return $relative_ts
}

proc Bridge::InsertStructure { refpoints Ref_Point_1 Ref_Point_2 c0 c1 width mode interpolation apply_condition bridge_top_bottom_values bridge_coefficients \
    force_bridge_points T force_pile_points Tpiles} {
    variable BridgesImplemented

    set selected_elements [Bridge::SelectLineTrianglesOrQuadrilaterals $c0 $c1 $width]
    if { ![llength $selected_elements] } {
	WarnWinText [= "The shape can't be inserted"].-SelectLineTrianglesOrQuadrilaterals
    } else {
	#GiD_Process 'Label Select Elements {*}$selected_elements escape
	set boundary_edges [Bridge::GetBoundaryEdgesTrianglesOrQuadrilaterals $selected_elements]
	if { ![llength $boundary_edges] } {
	    WarnWinText [= "The shape can't be inserted"].-GetBoundaryEdgesTrianglesOrQuadrilaterals
	} else {
	    set boundary_nodes [Bridge::GetBoundaryNodes $boundary_edges]
	    set inner_nodes [Bridge::GetInnerNodes $selected_elements $boundary_nodes]
	    #set mean_edge_size [Bridge::GetMeanEdgeSize $boundary_edges $boundary_nodes]
	    #set num_divisions [expr {max(1,int([MathUtils::VectorDistance $c0 $c1]/$mean_edge_size))}]
	    #set minimum_edge_size [Bridge::GetMinimumEdgeSize $boundary_edges $boundary_nodes]
	    #set num_divisions [expr {max(1,int([MathUtils::VectorDistance $c0 $c1]/$minimum_edge_size))}]
	    set num_boundary_edges [llength $boundary_edges]
	    if { [expr {$num_boundary_edges%2}] } {
		#to be multiple of 2
		incr num_boundary_edges
	    }
	    set num_divisions [expr {$num_boundary_edges/2}]
	    #set minimum the same amount of divisions that the amount of spans that define the bridge profile
	    set num_divisions_bridge [expr {[llength [Bridge::GetTsOfBridge $T]]-1}]
	    if { $num_divisions < $num_divisions_bridge } {
		set num_divisions $num_divisions_bridge
	    }
	    set min_relative_dt 1.0e-2
	    set max_relative_dt [expr {1.0/double($num_divisions)}]
	    if { $force_bridge_points } {
		set relative_ts [Bridge::EnhanceTs [Bridge::GetTsOfBridge $T] $min_relative_dt $max_relative_dt]
		if { $force_pile_points } {
		    #insert a posteriori the pile points
		    set relative_ts [Bridge::InsertTs [Bridge::GetTsOfPiles $Tpiles] $relative_ts]
		}
	    } else {
		if { $force_pile_points } {
		    set ts [list 0.0 {*}[Bridge::GetTsOfPiles $Tpiles] 1.0]
		    set relative_ts [Bridge::EnhanceTs $ts $min_relative_dt $max_relative_dt]
		} else {
		    #set relative_ts [Bridge::GetTsFromBoundaryEdgesAndNodes $c0 $c1 $boundary_edges $boundary_nodes]
		    set relative_ts [Bridge::GetTsFromNumDivisions $num_divisions]
		}
	    }


	    if { $mode == 0 } {
		lassign [Bridge::CalculateLineExtraEdgesAndNodes $c0 $c1 $relative_ts] extra_edges extra_nodes
	    } else {
		lassign [Bridge::CalculateRectangleExtraEdgesAndNodes $c0 $c1 $width $relative_ts] extra_edges extra_nodes
	    }
	    if { $interpolation == 0 } {
		#map extra_nodes z into mesh
		set extra_nodes [Bridge::MapExtraNodesToMesh $selected_elements $extra_nodes]
	    }
	    set created_elements [Bridge::CalculateInteriorTriangles $boundary_edges $boundary_nodes $extra_edges $extra_nodes]
	    if { ![llength $created_elements] } {
		WarnWinText [= "The shape can't be inserted"].-CalculateInteriorTriangles
	    } else {
		set previous_layer_to_use [GiD_Info project LayerToUse]
		set node_layer [Bridge::GetNodeLayerName [lindex [lindex $boundary_nodes 0] 0]]
		if { $node_layer != [GiD_Info project LayerToUse] } {
		    GiD_Process 'Layers ToUse $node_layer
		}
		set created_nodes_ids [Bridge::CreateExtraNodes $extra_nodes]
		set node_groups [Bridge::GetNodeGroups [lindex [lindex $boundary_nodes 0] 0]]
		foreach group $node_groups {
		    GiD_EntitiesGroups assign $group nodes $created_nodes_ids
		}
		set element_layer [Bridge::GetElementLayerName [lindex $selected_elements 0]]
		if { $element_layer != [GiD_Info project LayerToUse] } {
		    GiD_Process 'Layers ToUse $element_layer
		}
		set material_name [Bridge::GetElementMaterialName [lindex $selected_elements 0]]
		set created_elements_ids [Bridge::CreateInteriorTriangles $created_elements $material_name]
		if { $previous_layer_to_use != [GiD_Info project LayerToUse] } {
		    GiD_Process 'Layers ToUse $previous_layer_to_use
		}
		set element_groups [Bridge::GetElementGroups [lindex $selected_elements 0]]
		foreach group $element_groups {
		    GiD_EntitiesGroups assign $group elements $created_elements_ids
		}

		set applied_conditions [Bridge::GetElementsConditions $selected_elements]
		Bridge::AssignConditions elements $created_elements_ids $applied_conditions
		set boundary_nodes_ids [list]
		foreach node $boundary_nodes {
		    lappend boundary_nodes_ids [lindex $node 0]
		}
		set applied_conditions [Bridge::GetNodesConditions $boundary_nodes_ids]
		Bridge::AssignConditions nodes $created_nodes_ids $applied_conditions

		if { $apply_condition } {
		    set element_faces_of_extra_edges [Bridge::GetElementFacesOfExtraEdges $extra_edges $created_elements $created_elements_ids]
		    Bridge::ApplyBridgeConditions $refpoints $Ref_Point_1 $Ref_Point_2 $c0 $c1 $element_faces_of_extra_edges $bridge_top_bottom_values $bridge_coefficients $Tpiles
		}
		Bridge::DeleteInteriorElementsAndInnerNodes $selected_elements $inner_nodes
		GiD_RaiseEvent GiD_Event_AfterChangeMesh [GiD_Info Mesh NumNodes] [GiD_Info Mesh NumElements]
		GiD_Redraw
	    }
	}
    }
    incr BridgesImplemented
}

proc Bridge::SelectLineTrianglesOrQuadrilaterals { c0 c1 width } {
    #select the triangles 'near' the segment c0-c1

    #rotate to use bounding box aligned to the segment
    set c(0) [list [lindex $c0 0] [lindex $c0 1] 0.0]
    set c(1) [list [lindex $c1 0] [lindex $c1 1] 0.0]
    set incre [MathUtils::VectorDiff $c(0) $c(1)]
    set modulus [MathUtils::VectorModulus $incre]
    set sin_angle [expr {[lindex $incre 1]/$modulus}]
    set cos_angle [expr {[lindex $incre 0]/$modulus}]
    foreach i_node {0 1} {
	lassign $c($i_node) x y z
	set x_rotated [expr {$x*$cos_angle+$y*$sin_angle}]
	set y_rotated [expr {$y*$cos_angle-$x*$sin_angle}]
	set c_rotated($i_node) [list $x_rotated $y_rotated]
    }

    set selected_elements [list]
    foreach axis {0 1} {
	set v [lindex $c_rotated(0) $axis]
	set bbox($axis,min) $v
	set bbox($axis,max) $v
    }
    foreach axis {0 1} {
	set v [lindex $c_rotated(1) $axis]
	if { $bbox($axis,min) > $v  } {
	    set bbox($axis,min) $v
	}
	if { $bbox($axis,max) < $v } {
	    set bbox($axis,max) $v
	}
    }
    #add the width size
    set half_width [expr {$width*0.5}]
    set bbox(1,min) [expr {$bbox(1,min)-$half_width}]
    set bbox(1,max) [expr {$bbox(1,max)+$half_width}]
    #add also an extra size to the bbox in all directions
    set extra [expr {$width*0.25}]
    foreach axis { 0 1 } {
	set bbox($axis,min) [expr {$bbox($axis,min)-$extra}]
	set bbox($axis,max) [expr {$bbox($axis,max)+$extra}]
    }

    foreach {node_id x y z} [GiD_Info Mesh Nodes] {
	set x_rotated [expr {$x*$cos_angle+$y*$sin_angle}]
	set y_rotated [expr {$y*$cos_angle-$x*$sin_angle}]
	set coordinates_rotated($node_id) [list $x_rotated $y_rotated]
    }

    foreach {element_id n(0) n(1) n(2) mat} [GiD_Info Mesh Elements Triangle] {
	#calculate bbox of the element
	foreach axis {0 1} {
	    set v [lindex $coordinates_rotated($n(0)) $axis]
	    set ebbox($axis,min) $v
	    set ebbox($axis,max) $v
	}
	foreach i_node {1 2} {
	    foreach axis {0 1} {
		set v [lindex $coordinates_rotated($n($i_node)) $axis]
		if { $ebbox($axis,min) > $v } {
		    set ebbox($axis,min) $v
		}
		if { $ebbox($axis,max) < $v } {
		    set ebbox($axis,max) $v
		}
	    }
	}
	if { $bbox(0,min) > $ebbox(0,max) } continue
	if { $bbox(0,max) < $ebbox(0,min) } continue
	if { $bbox(1,min) > $ebbox(1,max) } continue
	if { $bbox(1,max) < $ebbox(1,min) } continue
	lappend selected_elements $element_id
    }

    foreach {element_id n(0) n(1) n(2) n(3) mat} [GiD_Info Mesh Elements Quadrilateral] {
	#calculate bbox of the element
	foreach axis {0 1} {
	    set v [lindex $coordinates_rotated($n(0)) $axis]
	    set ebbox($axis,min) $v
	    set ebbox($axis,max) $v
	}
	foreach i_node {1 2 3} {
	    foreach axis {0 1} {
		set v [lindex $coordinates_rotated($n($i_node)) $axis]
		if { $ebbox($axis,min) > $v } {
		    set ebbox($axis,min) $v
		}
		if { $ebbox($axis,max) < $v } {
		    set ebbox($axis,max) $v
		}
	    }
	}
	if { $bbox(0,min) > $ebbox(0,max) } continue
	if { $bbox(0,max) < $ebbox(0,min) } continue
	if { $bbox(1,min) > $ebbox(1,max) } continue
	if { $bbox(1,max) < $ebbox(1,min) } continue
	lappend selected_elements $element_id
    }

    return $selected_elements
}

proc Bridge::GetBoundaryEdgesTrianglesOrQuadrilaterals { selected_elements } {
    variable local_edges_triangle
    variable local_edges_quadrilateral

    if { ![array exists local_edges_triangle] } {
	Bridge::Init
    }


    foreach element_id $selected_elements {
	set element_data [GiD_Mesh get element $element_id]
	set element_type [lindex $element_data 1]
	if { $element_type == "Triangle" } {
	    set connectivities [lrange $element_data 3 5]
	    foreach i_edge {0 1 2} {
		set n0 [lindex $connectivities $local_edges_triangle($i_edge,0)]
		set n1 [lindex $connectivities $local_edges_triangle($i_edge,1)]
		set edge [list $n0 $n1]
		set opposite_edge [list $n1 $n0]
		if { [info exists edges($opposite_edge)] } {
		    unset edges($opposite_edge)
		} else {
		    set edges($edge) 1
		}
	    }
	} elseif { $element_type == "Quadrilateral" } {
	    set connectivities [lrange $element_data 3 6]
	    foreach i_edge {0 1 2 3} {
		set n0 [lindex $connectivities $local_edges_quadrilateral($i_edge,0)]
		set n1 [lindex $connectivities $local_edges_quadrilateral($i_edge,1)]
		set edge [list $n0 $n1]
		set opposite_edge [list $n1 $n0]
		if { [info exists edges($opposite_edge)] } {
		    unset edges($opposite_edge)
		} else {
		    set edges($edge) 1
		}
	    }
	} else {
	    WarnWinText "Bridge::GetBoundaryEdgesTrianglesOrQuadrilaterals. Unexpected element type $element_type"
	}
    }
    #return [array names edges]
    #reorder the edges to be consecutive, althought it is not necessary to use triangle
    set boundary_edges_ordered_by_first_index [lsort -integer -index 0 [array names edges]]
    array unset edges
    set edge [lindex $boundary_edges_ordered_by_first_index 0]
    set connect [lindex $edge 1]
    set boundary_edges [list $edge]
    set num_boundary_edges [llength $boundary_edges_ordered_by_first_index] ;#to avoid infinite loops
    set i_boundary_edges 0 ;#to avoid infinite loops, e.g. in case of laces
    set start $connect
    while { $connect != "" } {
	set i_edge [lsearch -sorted -integer -index 0 $boundary_edges_ordered_by_first_index $connect]
	if { $i_edge != -1 } {
	    set edge [lindex $boundary_edges_ordered_by_first_index $i_edge]
	    set connect [lindex $edge 1]
	    if  { $connect == $start } {
		#assumed a single boundary without holes
		set connect ""
	    } else {
		lappend boundary_edges $edge
		incr i_boundary_edges
		if { $i_boundary_edges > $num_boundary_edges } {
		    set boundary_edges [list]
		    break
		}
	    }
	} else {
	    set connect ""
	}
    }
    if { [llength $boundary_edges] != $num_boundary_edges } {
	set boundary_edges [list] ;#error
    }
    return $boundary_edges
}

proc Bridge::GetBoundaryNodes { boundary_edges } {
    foreach edge $boundary_edges {
	lassign $edge n0 n1
	set edge_nodes($n0) 1
	set edge_nodes($n1) 1
    }
    set boundary_nodes [list]
    foreach node_id [lsort -integer [array names edge_nodes]] {
	set coordinates [lrange [GiD_Mesh get node $node_id] 1 end]
	lappend boundary_nodes [list $node_id {*}$coordinates]
    }
    return $boundary_nodes
}

proc Bridge::GetInnerNodes { selected_elements boundary_nodes } {
    #add triangle's nodes without repetition
    foreach element_id $selected_elements {
	set element_data [GiD_Mesh get element $element_id]
	set element_type [lindex $element_data 1]
	if { $element_type == "Triangle" } {
	    set connectivities [lrange $element_data 3 5]
	} elseif { $element_type == "Quadrilateral" } {
	    set connectivities [lrange [GiD_Mesh get element $element_id] 3 6]
	} else {
	    WarnWinText "Bridge::GetInnerNodes. Unexpected element type=$element_data"
	    continue
	}
	foreach node_id $connectivities {
	    set elements_nodes($node_id) 1
	}
    }
    #remove boundary nodes
    foreach item $boundary_nodes {
	unset elements_nodes([lindex $item 0])
    }
    set inner_nodes [lsort -integer [array names elements_nodes]]
    return $inner_nodes
}

proc Bridge::GetMeanEdgeSize { boundary_edges boundary_nodes } {
    foreach item $boundary_nodes {
	set boundary_node([lindex $item 0]) [lrange $item 1 3]
    }
    set dsum 0.0
    set i 0
    foreach edge $boundary_edges {
	incr i
	lassign $edge n0 n1
	set d [MathUtils::VectorDistance $boundary_node($n0) $boundary_node($n1)]
	set dsum [expr {$dsum+$d}]
    }
    set mean_edge_size [expr {$dsum/double($i)}]
    return $mean_edge_size
}

proc Bridge::GetMinimumEdgeSize { boundary_edges boundary_nodes } {
    foreach item $boundary_nodes {
	set boundary_node([lindex $item 0]) [lrange $item 1 3]
    }
    set dmin 1e20
    set i 0
    foreach edge $boundary_edges {
	incr i
	lassign $edge n0 n1
	set d [MathUtils::VectorDistance $boundary_node($n0) $boundary_node($n1)]
	if { $d < $dmin } {
	    set dmin $d
	}
    }
    return $dmin
}

proc Bridge::CalculateLineExtraEdgesAndNodes { c0 c1 relative_ts } {
    set node_id [GiD_Info Mesh MaxNumNodes]
    set extra_nodes [list]
    set incre [MathUtils::VectorDiff $c0 $c1]
    set prev_node_id 0
    foreach t $relative_ts {
	set p [MathUtils::VectorSum $c0 [MathUtils::ScalarByVectorProd $t $incre]]
	incr node_id
	lappend extra_nodes "$node_id $p"
	if { $prev_node_id } {
	    lappend extra_edges [list $prev_node_id $node_id]
	}
	set prev_node_id $node_id
    }

    return [list $extra_edges $extra_nodes]
}

proc Bridge::CalculateRectangleExtraEdgesAndNodes { c0 c1 width relative_ts } {
    set n [expr {[llength $relative_ts]-1}]

    set node_id [GiD_Info Mesh MaxNumNodes]
    set prev_node_id [expr $node_id+2*($n+1)]
    set extra_nodes [list]
    set incre [MathUtils::VectorDiff $c0 $c1]
    set dir_rotated_90 [MathUtils::VectorNormalized [list [lindex $incre 1] [expr {-[lindex $incre 0]}] 0.0]]
    set p0 [MathUtils::VectorSum $c0 [MathUtils::ScalarByVectorProd [expr {$width*0.5}] $dir_rotated_90]]
    foreach t $relative_ts {
	set p [MathUtils::VectorSum $p0 [MathUtils::ScalarByVectorProd $t $incre]]
	incr node_id
	lappend extra_nodes "$node_id $p"
	lappend extra_edges [list $prev_node_id $node_id]
	set prev_node_id $node_id
    }
    set incre [MathUtils::VectorDiff $c1 $c0]
    set p0 [MathUtils::VectorSum $c1 [MathUtils::ScalarByVectorProd [expr {-$width*0.5}] $dir_rotated_90]]
    foreach t [lreverse $relative_ts] {
	set t [expr {1.0-$t}]
	set p [MathUtils::VectorSum $p0 [MathUtils::ScalarByVectorProd $t $incre]]
	incr node_id
	lappend extra_nodes "$node_id $p"
	lappend extra_edges [list $prev_node_id $node_id]
	set prev_node_id $node_id
    }
    return [list $extra_edges $extra_nodes]
}

proc Bridge::InsideTriangle2D { p1 p2 p3 x y } {
    lassign $p1 x1 y1 z1
    lassign $p2 x2 y2 z2
    lassign $p3 x3 y3 z3
    # The point must lie on the same side of each edge spanned by two vertices as the third vertex
    foreach {xn1 yn1 xn2 yn2 xn3 yn3} [list $x1 $y1 $x2 $y2 $x3 $y3 $x2 $y2 $x3 $y3 $x1 $y1 $x3 $y3 $x1 $y1 $x2 $y2] {
	set inpp  [expr {($x  -$xn1)*($yn2-$yn1)-($y  -$yn1)*($xn2-$xn1)}]
	set inp3  [expr {($xn3-$xn1)*($yn2-$yn1)-($yn3-$yn1)*($xn2-$xn1)}]
	if { $inpp < 0.0 && $inp3 > 0.0 } {
	    return 0
	}
	if { $inpp > 0.0 && $inp3 < 0.0 } {
	    return 0
	}
    }
    return 1
}

#
# Parametrisation of the plane:
#
#  /x\   /x1\     /x12\     /x13\
    #  |y| = |y1| + L |y12| + M |y13|
#  \z/   \z1/     \z12/     \z13/
#
# Now: find the values of L and M for (x,y) and fill in
#
#   L (x12*y13-x13*y12) = (x-x1)*y13 - (y-y1)*x13
#   M (x13*y12-x12*y13) = (x-x1)*y12 - (y-y1)*x12
#
proc Bridge::InterpolatePlane {p1 p2 p3 x y} {
    lassign $p1 x1 y1 z1
    lassign $p2 x2 y2 z2
    lassign $p3 x3 y3 z3
    set x12  [expr {$x2-$x1}]
    set y12  [expr {$y2-$y1}]
    set z12  [expr {$z2-$z1}]
    set x13  [expr {$x3-$x1}]
    set y13  [expr {$y3-$y1}]
    set z13  [expr {$z3-$z1}]
    set dx   [expr {$x-$x1}]
    set dy   [expr {$y-$y1}]
    set det [expr {$x12*$y13-$x13*$y12}]
    set L   [expr {($dx*$y13-$dy*$x13)/$det}]
    set M   [expr {-($dx*$y12-$dy*$x12)/$det}]
    set z   [expr {$z1+$z12*$L+$z13*$M}]
    return $z
}


proc Bridge::MapExtraNodesToMesh { selected_elements extra_nodes } {
    #     mean_edge_size should be provided as parameter
    #     package require math::interpolate
    #     set max_search_radius [expr {$mean_edge_size*2.0}]
    #     ::math::interpolate::interp-spatial-params $max_search_radius 2
    #
    #     set elements_nodes [list]
    #     foreach element_id $selected_elements {
	#         foreach node_id [lrange [GiD_Mesh get element $element_id] 3 5] {
	    #             lappend elements_nodes $node_id
	    #         }
	#     }
    #     set elements_nodes [lsort -integer -unique $elements_nodes]
    #     set elements_coordinates [list]
    #     foreach node_id $elements_nodes {
	#         lappend elements_coordinates [lrange [GiD_Mesh get node $node_id] 1 3]
	#     }

    foreach element_id $selected_elements {
	set connectivities [lrange [GiD_Mesh get element $element_id] 3 5]
	foreach node_id $connectivities i_node {0 1 2} {
	    set coordinates($i_node) [lrange [GiD_Mesh get node $node_id] 1 3]
	}
	#calculate bbox of the element
	foreach axis {0 1} {
	    set v [lindex $coordinates(0) $axis]
	    set ebbox($axis,min) $v
	    set ebbox($axis,max) $v
	}
	foreach i_node {1 2} {
	    foreach axis {0 1} {
		set v [lindex $coordinates($i_node) $axis]
		if { $ebbox($axis,min) > $v } {
		    set ebbox($axis,min) $v
		}
		if { $ebbox($axis,max) < $v } {
		    set ebbox($axis,max) $v
		}
	    }
	}
	lappend selected_elements_bbox [list $ebbox(0,min) $ebbox(0,max) $ebbox(1,min) $ebbox(1,max)]
    }

    set modified_extra_nodes [list]
    foreach node $extra_nodes {
	lassign $node extra_node_id x y z
	set found 0
	foreach element_id $selected_elements ebbox_list $selected_elements_bbox {
	    lassign $ebbox_list xmin xmax ymin ymax
	    if { $x > $xmax } continue
	    if { $x < $xmin } continue
	    if { $y > $ymax } continue
	    if { $y < $ymin } continue
	    #point lies in triangle bbox, must check if lies in the triangle and interpolate from its corners
	    set connectivities [lrange [GiD_Mesh get element $element_id] 3 5]
	    foreach node_id $connectivities i_node {0 1 2} {
		set coordinates($i_node) [lrange [GiD_Mesh get node $node_id] 1 3]
	    }
	    if { [Bridge::InsideTriangle2D $coordinates(0) $coordinates(1) $coordinates(2) $x $y] } {
		set new_z [Bridge::InterpolatePlane $coordinates(0) $coordinates(1) $coordinates(2) $x $y]
		set found 1
		break
	    }
	}
	if { !$found } {
	    set new_z $z
	}
	#         set new_z [::math::interpolate::interp-spatial $elements_coordinates [list $x $y]]
	lappend modified_extra_nodes [list $extra_node_id $x $y $new_z]
    }
    return $modified_extra_nodes
}

proc Bridge::CalculateInteriorTriangles { boundary_edges boundary_nodes extra_edges extra_nodes } {
    set created_elements [list]
    set exe [Iber::GetFullPathExe triangle.exe]
    if { ![file exists $exe] } {
	WarnWinText [= "File '%s' not found" $exe]
	return ""
    }
    set dir [GidUtils::CreateTmpFolderName]

    set filename [file join $dir boundary.poly]
    set fp [open $filename w]
    if { $fp != "" } {
	set num_nodes [expr {[llength $boundary_nodes]+[llength $extra_nodes]}]
	set num_edges [expr {[llength $boundary_edges]+[llength $extra_edges]}]
	puts $fp "$num_nodes 2 1 0"
	set i 0 ;#must number noder from 1 to n without jumps
	foreach node $boundary_nodes {
	    incr i
	    lassign $node i_node x y z
	    set new_node_num($i_node) $i
	    set old_node_num($i) $i_node
	    puts $fp "$i $x $y $z"
	}
	foreach node $extra_nodes {
	    incr i
	    lassign $node i_node x y z
	    set new_node_num($i_node) $i
	    set old_node_num($i) $i_node
	    puts $fp "$i $x $y $z"
	}
	puts $fp "$num_edges 0"
	set i 0
	foreach edge $boundary_edges {
	    incr i
	    lassign $edge n0 n1
	    puts $fp "$i $new_node_num($n0) $new_node_num($n1)"
	}
	foreach edge $extra_edges {
	    incr i
	    lassign $edge n0 n1
	    puts $fp "$i $new_node_num($n0) $new_node_num($n1)"
	}
	puts $fp "0"
	close $fp
	#flag g for .off output
	catch { exec "$exe" -pBPNQ "$filename" } err

	set outfilename [file rootname $filename].1.ele
	set fp [open $outfilename r]
	if { $fp != "" } {
	    gets $fp line
	    lassign $line num_elements n_node n_markers
	    for { set i 0 } {$i<$num_elements} {incr i } {
		gets $fp line
		lassign $line element_id n0 n1 n2
		if { [info exists old_node_num($n0)] && [info exists old_node_num($n1)] && [info exists old_node_num($n2)] } {
		    lappend created_elements [list $old_node_num($n0) $old_node_num($n1) $old_node_num($n2)]
		} else {
		    set created_elements [list]
		    break;
		}
	    }
	    close $fp
	    file delete $outfilename
	}
	file delete $filename
    }
    file delete $dir
    return $created_elements
}

proc Bridge::CreateExtraNodes { extra_nodes } {
    set node_ids [list]
    foreach node $extra_nodes {
	set p [lrange $node 1 3]
	lappend node_ids [GiD_Mesh create node append $p]
    }
    return $node_ids
}

proc Bridge::GetNodeGroups { node_id } {
    return [GiD_EntitiesGroups entity_groups nodes $node_id]
}

proc Bridge::GetElementGroups { element_id } {
    return [GiD_EntitiesGroups entity_groups elements $element_id]
}

#return the conditions over surface that are applied over all selected_elements
proc Bridge::GetElementsConditions { selected_elements } {
    set applied_conditions [list]
    set element_id [lindex $selected_elements 0]
    set num_conditions [lindex [lindex [split [GiD_Info list_entities elements $element_id] \n] 1] 5]
    if { $num_conditions } {
	set icond 0
	foreach condition_name [GiD_Info Conditions ovsurf] {
	    set values [lrange [lindex [GiD_Info Conditions $condition_name mesh $element_id] 0] 3 end]
	    if { $values != "" } {
		lappend applied_conditions $condition_name $values
		incr icond
		if { $icond == $num_conditions } {
		    break
		}
	    }
	}
	if { [llength $applied_conditions] } {
	    foreach element_id [lrange $selected_elements 1 end] {
		foreach {condition_name reference_values} $applied_conditions {
		    set values [lrange [lindex [GiD_Info Conditions $condition_name mesh $element_id] 0] 3 end]
		    if { $values != $reference_values } {
		        #not equal for all
		        set applied_conditions ""
		        break
		    }
		}
	    }
	}
    }
    return $applied_conditions
}

#return the conditions over ovsurf that are applied over all selected_nodes
proc Bridge::GetNodesConditions { selected_nodes } {
    set applied_conditions [list]
    set node_id [lindex $selected_nodes 0]
    set num_conditions [lindex [lindex [split [GiD_Info list_entities nodes $node_id] \n] 1] 5]
    if { $num_conditions } {
	set icond 0
	foreach condition_name [GiD_Info Conditions ovsurf] {
	    set values [lrange [lindex [GiD_Info Conditions $condition_name mesh $node_id] 0] 3 end]
	    if { $values != "" } {
		lappend applied_conditions $condition_name $values
		incr icond
		if { $icond == $num_conditions } {
		    break
		}
	    }
	}
	if { [llength $applied_conditions] } {
	    foreach node_id [lrange $selected_nodes 1 end] {
		foreach {condition_name reference_values} $applied_conditions {
		    set values [lrange [lindex [GiD_Info Conditions $condition_name mesh $node_id] 0] 3 end]
		    if { $values != $reference_values } {
		        #not equal for all
		        set applied_conditions ""
		        break
		    }
		}
	    }
	}
    }
    return $applied_conditions
}

proc Bridge::GetNodeLayerName { node_id } {
    set layer_name [lindex [GiD_Mesh get node $node_id] 0]
    return $layer_name
}

proc Bridge::GetElementLayerName { element_id } {
    set layer_name [lindex [GiD_Mesh get element $element_id] 0]
    return $layer_name
}

proc Bridge::GetElementMaterialName { element_id } {
    set material_id [lindex [GiD_Info Mesh Elements Any $element_id] end]
    if { $material_id } {
	set material_name [lindex [GiD_Info Materials] $material_id-1]
    } else {
	set material_name ""
    }
    return $material_name
}

proc Bridge::AssignConditions { over created_elements_ids applied_conditions } {
    foreach {condition_name values} $applied_conditions {
	GiD_AssignData condition $condition_name $over $values $created_elements_ids
    }
    return 0
}

proc Bridge::CreateInteriorTriangles { created_elements material_name } {
    set created_elements_ids [list]
    if { $material_name != "" } {
	foreach element $created_elements {
	    lappend created_elements_ids [GiD_Mesh create element append triangle 3 $element $material_name]
	}
    } else {
	foreach element $created_elements {
	    lappend created_elements_ids [GiD_Mesh create element append triangle 3 $element]
	}
    }
    return $created_elements_ids
}

proc Bridge::GetElementFacesOfExtraEdges { extra_edges created_elements created_elements_ids } {
    set element_faces_of_extra_edges [list]
    foreach edge $extra_edges {
	set edges($edge) 1
    }
    set local_edges(0,0) 0
    set local_edges(0,1) 1
    set local_edges(1,0) 1
    set local_edges(1,1) 2
    set local_edges(2,0) 2
    set local_edges(2,1) 0
    foreach connectivities $created_elements element_id $created_elements_ids {
	foreach i_edge {0 1 2} {
	    set n0 [lindex $connectivities $local_edges($i_edge,0)]
	    set n1 [lindex $connectivities $local_edges($i_edge,1)]
	    set edge [list $n0 $n1]
	    set opposite_edge [list $n1 $n0]
	    if { [info exists edges($edge)] || [info exists edges($opposite_edge)] } {
		lappend element_faces_of_extra_edges [list $element_id [expr {$i_edge+1}]]
	    }
	}
    }
    return $element_faces_of_extra_edges
}

proc Bridge::GetEdgeCoordinates { element_id i_edge } {
    set connectivities [lrange [GiD_Mesh get element $element_id] 3 5]
    set local_edges(0,0) 0
    set local_edges(0,1) 1
    set local_edges(1,0) 1
    set local_edges(1,1) 2
    set local_edges(2,0) 2
    set local_edges(2,1) 0
    incr i_edge -1
    set n0 [lindex $connectivities $local_edges($i_edge,0)]
    set n1 [lindex $connectivities $local_edges($i_edge,1)]
    set c0 [lrange [GiD_Mesh get node $n0] 1 end]
    set c1 [lrange [GiD_Mesh get node $n1] 1 end]
    return [list $c0 $c1]
}

proc Bridge::GetEdgeBridgeTopAndBottomFromRelativeT { table_name t } {
    lassign [math::interpolate::interp-1d-table $table_name $t] xout z_bridge_top z_bridge_bottom
    return [list $z_bridge_top $z_bridge_bottom]
}

#t of x,y relative to the line c0->c1 (in 2D projection)
proc Bridge::GetRelativeT { c0 c1 x y } {
    set c0 [lrange $c0 0 1]
    set c1 [lrange $c1 0 1]
    set p [list $x $y]
    set distance_01 [MathUtils::VectorDistance $c0 $c1]
    set distance_0p [MathUtils::VectorDistance $c0 $p]
    set t [expr {$distance_0p/$distance_01}]
    return $t
}

proc Bridge::GetEdgeBridgeTopAndBottom { table_name c0 c1 x y } {
    set t [Bridge::GetRelativeT $c0 $c1 $x $y]
    return [Bridge::GetEdgeBridgeTopAndBottomFromRelativeT $table_name $t]
}

proc Bridge::GetLengthIntersectionIntervals { a b intervals } {
    set length_intersection 0.0
    foreach {a_i b_i} $intervals {
	set insersection_min [::math::max $a_i $a]
	set insersection_max [::math::min $b_i $b]
	set d [expr {$insersection_max-$insersection_min}]
	if { $d > 0 } {
	    set length_intersection [expr {$length_intersection+$d}]
	}
    }
    return $length_intersection
}

proc Bridge::GetPercentSubstractingPileIntervals { a b piles_intervals } {
    set length_intersection [Bridge::GetLengthIntersectionIntervals $a $b $piles_intervals]
    if { $length_intersection > 0.0 } {
	set d [expr {$b-$a}]
	set percent [expr {($d-$length_intersection)/$d*100.0}]
	if { $percent < 1e-5 } {
	    set percent 0.0
	}
    } else {
	set percent 100.0
    }
    return [format %g $percent]
}


proc Bridge::ApplyBridgeConditions { refpoints Ref_Point_1 Ref_Point_2 c0 c1 element_faces_of_extra_edges bridge_top_bottom_values bridge_coefficients Tpiles } {
    package require math
    package require math::interpolate
    catch {::math::interpolate::__auxname destroy}
	set dummy_row {- - -} ;#after package math::interpolate 1.0.3 the definition of 1d-table for math::interpolate::interpolate-1d-table has changed and now require a first dummy row!!
    set table_name [math::interpolate::defineTable auxname {distance ztop zbottom} [concat $dummy_row $bridge_top_bottom_values]]
    
    set piles_intervals [Bridge::GetTsOfPiles $Tpiles]

    set real_format %g
    set condition_name Line_Bridge ;#instead of similar 2D_Internal_Condition
    lassign $bridge_coefficients Free_Pressured_Flow_Cd Submerged_Pressured_Flow_Cd Deck_Cd Gauge_Number

    set Deck_Length_Percent 100.0 ;#hidden, this is always 100 because piles not cross the top of the bridge
    foreach element_face $element_faces_of_extra_edges {
	lassign [Bridge::GetEdgeCoordinates {*}$element_face] p(0) p(1)
	foreach i {0 1} {
	    lassign $p($i) x y z
	    set t_face($i) [Bridge::GetRelativeT $c0 $c1 $x $y]
	}
	if { $t_face(0) > $t_face(1) } {
	    #reorder interval increasing Lower_deck_elevation
	    set t_face_tmp $t_face(0)
	    set t_face(0) $t_face(1)
	    set t_face(1) $t_face_tmp
	}
	#must check if the interval t_edge(0) t_edge(1) collides with the interval of some piles,
	#and then decrease the Bridge_opening_Percent accordly (substracting the percent of piles of the edge)
	set Bridge_Opening_Percent [Bridge::GetPercentSubstractingPileIntervals $t_face(0) $t_face(1) $piles_intervals]
	set edge_midpoint [MathUtils::ScalarByVectorProd 0.5 [MathUtils::VectorSum $p(0) $p(1)]]
	lassign $edge_midpoint x_midpoint y_midpoint z_midpoint
	lassign [Bridge::GetEdgeBridgeTopAndBottom $table_name $c0 $c1 $x_midpoint $y_midpoint] z_bridge_top z_bridge_bottom
	#set Gate_Bottom_Elev [format $real_format $z_midpoint] ;#unused now
	set Deck_Elevation [format $real_format $z_bridge_top]
	set Lower_Deck_Elevation [format $real_format $z_bridge_bottom]
	if { $refpoints == 0 } {
	set refpointsdef "Standard"
	} elseif { $refpoints == 1 } {
	set refpointsdef "User_defined"
	}
	set voidquestion ""
	set values [list $refpointsdef $Ref_Point_1 $Ref_Point_2 $Deck_Elevation $Lower_Deck_Elevation $Bridge_Opening_Percent $Deck_Length_Percent $Deck_Cd $Free_Pressured_Flow_Cd $Submerged_Pressured_Flow_Cd $Gauge_Number]
	GiD_AssignData condition $condition_name face_elements $values $element_face
    }
    $table_name destroy
}

proc Bridge::DeleteInteriorElementsAndInnerNodes { selected_elements inner_nodes } {
    GiD_Mesh delete element $selected_elements
    if { [catch {GiD_Mesh delete node $inner_nodes} msg] } {
	WarnWinText $msg
    }
    return 0
}

proc Bridge::DefineElementType {} {
    global PropPriv

    set PropPriv(idata) ""
    set d .gid.properties
    set PropPriv(ImportPref) 1

    Bridge::FileType $d
}

proc Bridge::FileType { d } {
    global PropPriv

    if {$PropPriv(ImportPref) == 1} {
	Bridge::ImportFile $d
    } elseif {$PropPriv(ImportPref) == 2} {
	# Not used yet. Will allow to import other IC
	Bridge::ImportFile $d
    }
    destroy $d
}

proc Bridge::ImportFile { d } {
    global PropPriv GidPriv
    destroy $d

    # Working directory
    set aa [.central.s info Project]
    set Project [lindex $aa 1]
    set Project [file normalize $Project]
    set directory $Project.gid

    # Folder of the database
    set filename [Browser-ramR file read .gid [_ "Read Bridges file"] \
	    {} {{{} {.txt}} {{All files} {.*}}} ]
    if {$filename == "" } {
	return
    }

    # Read database
    set w ""
    set T .gid.bridge.fbridge.t
    set Tpiles .gid.bridge.fbridge.fpiles.t
    Bridge::ReadBridgeData $filename $w $T $Tpiles
}

proc Bridge::ReadBridgeData { filename w T Tpiles } {
    variable p0
    variable p1
    variable width
    variable mode ;#0 insert segment, 1 insert rectangle
    variable interpolation ;#0 interpolate nodes from mesh, 1interpolate nodes from segment ends
    variable apply_condition ;#1 to apply Weir-Gate 2D_Internal_Condition
    variable Free_Pressured_Flow_Cd ;#Free_Gate_Cd
    variable Submerged_Pressured_Flow_Cd ;#Submerged_Gate_Cd
    variable Deck_Cd ;#Weir_Cd
    variable Gauge_Number ;#Gauge_Number
    variable bridge_top_bottom_values
    variable pile_values
    variable draw_terrain ;#to calculate or not drawing the bridge curves
    variable force_bridge_points ;#to force the bridge points to be in the inserted mesh
    variable force_pile_points ;#to force the pile points to be in the inserted mesh
    #Still not working
    variable refpoints
    variable Ref_Point_1
    variable Ref_Point_2

    set file [open $filename]
    Bridge::SetDefaultValues

    #Trick: open Bridge window for creating $T and $Tpiles
    #GidUtils::DisableGraphics
    #GidUtils::WaitState .gid

    set type bridge
    Bridge::Window $type
    set count 0
    set BridgeData ""
    #GidUtils::EnableGraphics
    #GidUtils::EndWaitState .gid


    while {![eof $file] } {
	set bridgedata [gets $file]
	set llarg [llength $bridgedata]

	if { $llarg == 16 } {
	    # Reading variables
	    set id [lrange $bridgedata 0 0]                         ;# bridge id
	    set p0(0) [lrange $bridgedata 1 1]                                ;# coordenate X start
	    set p0(1) [lrange $bridgedata 2 2]                                ;# coordenate Y start
	    set p0(2) [lrange $bridgedata 3 3]                                ;# coordenate Z start
	    set p1(0) [lrange $bridgedata 4 4]                                ;# coordenate X end
	    set p1(1) [lrange $bridgedata 5 5]                                ;# coordenate Y end
	    set p1(2) [lrange $bridgedata 6 6]                                ;# coordenate Z end
	    set width [lrange $bridgedata 7 7]                                ;# width
	    set force_bridge_points [lrange $bridgedata 8 8]                  ;# force bridge points
	    set force_pile_points [lrange $bridgedata 9 9]                    ;# force pile points
	    set Free_Pressured_Flow_Cd [lrange $bridgedata 10 10]             ;# Cd free
	    set Submerged_Pressured_Flow_Cd [lrange $bridgedata 11 11]        ;# Cd submerged
	    set Deck_Cd [lrange $bridgedata 12 12]                            ;# Cd deck
	    set Gauge_Number [lrange $bridgedata 13 13]                       ;# bridge gauge number
	    #set bt [lrange $bridgedata 14 14]                                ;# bridge table
	    set rbt [lrange $bridgedata 14 14]                                ;# rows of the bridge table
	    #set pt [lrange $bridgedata 15 15]                                ;# piles table
	    set rpt [lrange $bridgedata 15 15]                                ;# rows of the piles table

	    # Tables reading
	    set bridge_top_bottom_values ""
	    set pile_values ""

	    # bridge table
	    if { $rbt >= 2 } {
		for {set ii 1} {$ii<=$rbt} {incr ii 1} {
		    set bridgedata [gets $file]
		    append bridge_top_bottom_values $bridgedata " "
		}
	    } else {
		tk_dialogRAM .gid.tmpwin "Brigde importation" \
		    "The bridge definition requires at least the the top and bottom elevation of the deck" \
		    info 0 OK
	    }
	    # piles table
	    if { $rpt >= 1 } {
		for {set ii 1} {$ii<=$rpt} {incr ii 1} {
		    set bridgedata [gets $file]
		    append pile_values $bridgedata " "
		}
		set check 0
	    } else {
		set pile_values "0.0 0.0"
		set check 1
		}

	    if { $check == 1 } {
		GidUtils::SetWarnLine [= "No piles were implemented for bridge number %s" $id ]
	    }

	    # Adapting variables
	    set c0 [list $p0(0) $p0(1) $p0(2)]
	    set c1 [list $p1(0) $p1(1) $p1(2)]

	    set bridge_coefficients [list $Free_Pressured_Flow_Cd $Submerged_Pressured_Flow_Cd $Deck_Cd $Gauge_Number]

	    GridData::SetData $T $bridge_top_bottom_values
	    GridData::SetData $Tpiles $pile_values


	    # OJO!!
	    # No solo hay que comprobar que exista la malla, sino que las coordenadas de inicio y fin de cada puentes est�n dentro del dominio del modelo
	    if { [GidUtils::ExistsMesh] } {
		set apply_condition 1
		set mode 0
		set interpolation 0

		#Bridge::Apply $T $Tpiles
		Bridge::InsertStructure $c0 $c1 $width $mode $interpolation $apply_condition $bridge_top_bottom_values $bridge_coefficients \
		    $force_bridge_points $T $force_pile_points $Tpiles

		Bridge::SaveImportedBridges $id $c0 $c1 $width $force_bridge_points $force_pile_points \
		    $bridge_coefficients $rbt $rpt $bridge_top_bottom_values $pile_values
		incr count
	    } else {
		tk_dialogRAM .gid.tmpwin "Brigde importation" \
		    "Don't forget to generate the mesh" \
		    info 0 OK
	    }
	} else {
	    # Do nothing
	    #tk_dialogRAM .gid.tmpwin "Wrong bridge file format" info 0 OK
	}
    }

    close $file

    if { [info exists id] } {
	if { $w == "" } {
	    set w .gid.bridge
	    Bridge::DestroyWindow $w $T
	} else {
	    Bridge::DestroyWindow $w $T
	}
    }

    set ImpBri $count
    GidUtils::SetWarnLine [= "%s Bridges have been imported and implemented into the mesh" $ImpBri ]

}

proc Bridge::SaveImportedBridges { id c0 c1 width force_bridge_points force_pile_points \
		                   bridge_coefficients rbt rpt bridge_top_bottom_values pile_values } {
    variable BridgesData
    variable count
#     variable VariablesNames { c0 c1 width force_bridge_points force_pile_points bridge_coefficients rbt rpt bridge_top_bottom_values pile_values }
#     variable VariablesValues [list]
#
#     set test $c0
#     set long [llength $VariablesNames]
#     set VariablesValues ""
#     for { set ii 0 } { $ii < $long } {incr ii 1} {
#         append VariablesValues [lindex $VariablesNames $ii]
#     }
#     foreach item $VariablesNames {
#         set name ${item}
#         lappend VariablesValues $name
#     }
#
#     # Saving varibales for each bridge
#     foreach item $VariablesNames {
#         set test $($item)
#         set BridgesData($id,$item) ${item}
#     }
    set BridgesData($id,c0) $c0
    set BridgesData($id,c1) $c0
    set BridgesData($id,width) $width
    set BridgesData($id,force_bridge_points) $force_bridge_points
    set BridgesData($id,force_pile_points) $force_pile_points
    set BridgesData($id,bridge_coefficients) $bridge_coefficients
    set BridgesData($id,rbt) $rbt
    set BridgesData($id,rpt) $rpt
    set BridgesData($id,bridge_top_bottom_values) $bridge_top_bottom_values
    set BridgesData($id,pile_values) $pile_values

}

proc Bridge:WriteBridgeData {} {
    variable BridgesData
    variable BridgesImplemented

    Bridge::GetNumImpBridges
    set numbrid $BridgesImplemented
    set res ""


    for { set ii 0 } { $ii < $numbrid } {incr ii 1} {
	#do nothing
    }



}

proc Bridge::GetNumImpBridges {} {
    variable BridgesImplemented

    if { ![info exists BridgesImplemented] } {
	return ""
    }
    return $BridgesImplemented
}
