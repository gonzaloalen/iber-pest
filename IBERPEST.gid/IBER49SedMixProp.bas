*#EMPIEZA CHRISTIAN
*#----------------Soil_Erosion---------------------
*if(strcmp(GenData(Soil_Erosion),"Off")!=0)
!!!!!!!!!!!!!!!!!!!!General options!!!!!!!!!!!!!!!!!!
*#ACTIVACION/DESACTIVACION DEL MODULO DE CALCULO EN SUSPENSION(ACORDARSE QUE HAY 2 SOLVE SUSPENDED LAYER)
*if(strcasecmp(GenData(Mixture_model),"One_NON-Cohesive_layer")==0 && strcasecmp(GenData(Solve__Suspended__Load),"Off")==0 )
0                             ---> Solve suspended sediment mixture. isolcsmix. 0-No 1-Yes
*elseif(strcasecmp(GenData(Mixture_model),"One_NON-Cohesive_layer")==0 && strcasecmp(GenData(Solve__Suspended__Load),"On")==0)
1                             ---> Solve suspended sediment mixture. isolcsmix. 0-No 1-Yes
*elseif(strcasecmp(GenData(Mixture_model),"One_NON-Cohesive_layer_over_one_cohesive_layer")==0 && strcasecmp(GenData(Solve_suspended_load),"Off")==0)
0                             ---> Solve suspended sediment mixture. isolcsmix. 0-No 1-Yes
*elseif(strcasecmp(GenData(Mixture_model),"One_NON-Cohesive_layer_over_one_cohesive_layer")==0 && strcasecmp(GenData(Solve_suspended_load),"On")==0)
1                             ---> Solve suspended sediment mixture. isolcsmix. 0-No 1-Yes
*endif
*#ACTIVACION/DESACTIVACION DEL MODULO DE CALCULO EN EL LECHO
*if(strcmp(GenData(Solve_bed_load),"Off")==0)
0                             ---> Solve bed load sediment mixture. isolqsbmix. 0-No 1-Yes
*else
1                             ---> Solve bed load sediment mixture. isolqsbmix. 0-No 1-Yes
*endif
*#NUMERO DE CLASES DE SEDIMENTO
*if(strcmp(GenData(Mixture_model),"One_NON-Cohesive_layer")==0)
*Operation(GenData(Sediment_classes_1_layer,int)/5)                           ---> Number of sediment classes. ndsed
*elseif(strcmp(GenData(Mixture_model),"One_NON-Cohesive_layer_over_one_cohesive_layer")==0)
*Operation(GenData(Sediment_classes_2_layers,int)/7)  
*endif
*GenData(Start_time_Soil_Erosion)                         ---> start computing soil erosion. tstartsoilerosion
!!!!!!!!!!!!!!!!!!!!!!!!!!Soil layer structure!!!!!!!!!!!!!!!!!!!!
*#ANCHO DE LA CAPA CRITICA SUPERFICIAL EROSIONADA (OJO EN EL ORIGINAL CREO QUE ESTA EN METROS)
*GenData(Shield_length_of_eroded_layer_[mm])                          ---> Critical shield length of eroded layer. dltotsedcrit. <0-dynamic(n multiplica a D50) >0-length(mm)
*#ojo variables no introducidas por usuario
2                         ---> izrockdef. 0-Elevation_File 1.Elevation_cte 2.Depth_cte
1000.                     ---> rock elevation. zrockcte (solo si izrockdef=1,2)
zrock.dat                 ---> Fichero con zrock (solo si izrockdef=0)
*#TIPO DE MODELO DE MEZCLA
*if(strcmp(GenData(Mixture_model),"One_NON-Cohesive_layer")==0)
2                             ---> mixture model. imixmodel. 1-MultiNonCohesiveLayers 2-OneNonCohesiveOverOneCohesive
*elseif(strcmp(GenData(Mixture_model),"One_NON-Cohesive_layer_over_one_cohesive_layer")==0)
2                             ---> mixture model. imixmodel. 1-MultiNonCohesiveLayers 2-OneNonCohesiveOverOneCohesive
*endif
*#ACTUALIZAR LA DISTRIBUCION DE LAS CLASES DE SEDIMENTO
*if(strcmp(GenData(Update_sediment_classes_distribution),"Off")==0)
0                             ---> Update sediment classes distribution. iupdatemix. 0-No 1-Yes
*elseif(strcmp(GenData(Update_sediment_classes_distribution),"On")==0)
1                             ---> Update sediment classes distribution. iupdatemix. 0-No 1-Yes
*endif
*#ojo variables no introducidas por usuario
0.05                      ---> Active layer thickness (m). dlased.              (solo si imixmodel=1)
1                         ---> Number of substrate layers sup. nksedsup. (1-5)  (solo si imixmodel=1)
1                         ---> Number of substrate layers inf. nksedinf. (1-5)  (solo si imixmodel=1)
0.10                      ---> Substrate layers thickness (m). dlksed           (solo si imixmodel=1)
!!!!!!!!!!!!!!!!!!!Sediment properties!!!!!!!!!!!!!!!!!!!
*#OJO APARECE UN d0 QUE ES ESO????
*GenData(Sediment_density_[kg/m3])                         ---> sediment density (kg/m3). rhosed
*GenData(Sediment_porosity)                         ---> sediment porosity. dporosed
*GenData(Angle_of_friction_[rad])                         ---> sediment friction angle (rad). fricangle
!!!!!!!!!!!!!!!!!!!Bed load models!!!!!!!!!!!!!!!!!!!
*if(strcmp(GenData(BL_critical_shear_stress),"Constant_stress")==0)
0                             ---> Critical Shear Stress. itaucritbed. 0-Constant 1-Shields 2-Parker
*elseif(strcmp(GenData(BL_critical_shear_stress),"Shields")==0)
1                             ---> Critical Shear Stress. itaucritbed. 0-Constant 1-Shields 2-Parker
*else
2                             ---> Critical Shear Stress. itaucritbed. 0-Constant 1-Shields 2-Parker
*endif
*GenData(NON-dimensional_critical_stress)                          ---> constant non-dimensional critical stress. taustarctebed. (solo si itaucrit=0)
*if(strcmp(GenData(Bed_load_model),"Generic")==0)
0                             ---> bed load model. ibedloadmod. 0.Generic 1.MeyerPeter 2.WongParker(MPM modified) 3.Einstein&Brown 4.vanRijn 5.EngelundHansen 6.Yalin 7.AshidaMichiue
*elseif(strcmp(GenData(Bed_load_model),"Meyer_Peter_Muller")==0)
1                             ---> bed load model. ibedloadmod. 0.Generic 1.MeyerPeter 2.WongParker(MPM modified) 3.Einstein&Brown 4.vanRijn 5.EngelundHansen 6.Yalin 7.AshidaMichiue
*elseif(strcmp(GenData(Bed_load_model),"Wong_Parker_MPM_modified")==0)
2                             ---> bed load model. ibedloadmod. 0.Generic 1.MeyerPeter 2.WongParker(MPM modified) 3.Einstein&Brown 4.vanRijn 5.EngelundHansen 6.Yalin 7.AshidaMichiue
*elseif(strcmp(GenData(Bed_load_model),"Einstein_Brown")==0)
3                             ---> bed load model. ibedloadmod. 0.Generic 1.MeyerPeter 2.WongParker(MPM modified) 3.Einstein&Brown 4.vanRijn 5.EngelundHansen 6.Yalin 7.AshidaMichiue
*elseif(strcmp(GenData(Bed_load_model),"Van_Rijn_BL")==0)
4                             ---> bed load model. ibedloadmod. 0.Generic 1.MeyerPeter 2.WongParker(MPM modified) 3.Einstein&Brown 4.vanRijn 5.EngelundHansen 6.Yalin 7.AshidaMichiue
*elseif(strcmp(GenData(Bed_load_model),"Engelund_Hansen")==0)
5                             ---> bed load model. ibedloadmod. 0.Generic 1.MeyerPeter 2.WongParker(MPM modified) 3.Einstein&Brown 4.vanRijn 5.EngelundHansen 6.Yalin 7.AshidaMichiue
*elseif(strcmp(GenData(Bed_load_model),"Yalin")==0)
6                             ---> bed load model. ibedloadmod. 0.Generic 1.MeyerPeter 2.WongParker(MPM modified) 3.Einstein&Brown 4.vanRijn 5.EngelundHansen 6.Yalin 7.AshidaMichiue
*elseif(strcmp(GenData(Bed_load_model),"Ashida_Michiue")==0)
7                             ---> bed load model. ibedloadmod. 0.Generic 1.MeyerPeter 2.WongParker(MPM modified) 3.Einstein&Brown 4.vanRijn 5.EngelundHansen 6.Yalin 7.AshidaMichiue
*endif
*GenData(A_parameter)  *GenData(B_parameter)                      ---> Param A and B para formula generica (si ibedloadmod=0). qsbpar(2) Tc=A.(tb-tc)^B
*GenData(Hiding_factor_BL)                           ---> Hiding factor entre fracciones para taucrit. dmixhidebed. 0-min; 1-max
!!!!!!!!!!!!!!!!!!!Suspended Load Models!!!!!!!!!!!!!!!!!!!
*#ojo variables no introducidas por usuario
0.d0                      ---> diflcs. Laminar diffusivity (m2/s)
1.7                       ---> schtcs. Schmidt number
1                         ---> Rainfall production of sediment. icsrain. 0-No 1-Yes
*#TIPO DE FORMULACION PARA EL RAINFALL
1                             ---> Rainfall erodibility formulation. ierodrain. 1-Intensity(m/s) 2-Energy(J/m2/s)
*#CALADO LIMITE A PARTIR DEL QUE LAS GOTAS DE LLUVIA EROSIONAN (ojo hay dos valores (raindamp,braindamp siempre 0.8???)
*GenData(Rain_damping_depth_[mm]) 0.8                      ---> Rainfall damping critical depth. (draindamp [mm], braindamp). <0-Dynamic >0-Cte
*#ojo variables no introducidas por usuario
1                         ---> Flow driven detachment from cohesive layer (Dfdd). icssoil. 0-No 1-Yes
*GenData(Taucrit_Cohesive_Layer_[N/m2])                         ---> Critical shear stress of cohesive soil (Dfdd). taucritsoil. [N/m2]   (solo si icssoil=1)
*#Exchange model (ojo no esta metido discontinuous
*if(strcmp(GenData(Resuspension_model),"Van_Rijn")==0)
1                             ---> Exchange model between eroded layer and Cs (Dfdrd+Ddep). icserod. 1-VanRijn 2-Hairsine 3-Discontinuo
*else
2                             ---> Exchange model between eroded layer and Cs (Dfdrd+Ddep). icserod. 1-VanRijn 2-Hairsine 3-Discontinuo
*endif
*#Cs equilibbrium
*if(strcmp(GenData(Equilibrium_Cs),"VanRijn1984")==0)
1                             ---> Equilibrium Cs (Dfdrd). icastar. 1-VanRijn1984 2-Smith1977 3-Garcia1991 4-Zyserman1994 (solo si icserod=1)
*elseif(strcmp(GenData(Equilibrium_Cs),"Smith1977")==0)
2                             ---> Equilibrium Cs (Dfdrd). icastar. 1-VanRijn1984 2-Smith1977 3-Garcia1991 4-Zyserman1994 (solo si icserod=1)
*elseif(strcmp(GenData(Equilibrium_Cs),"Garcia1991")==0)
3                             ---> Equilibrium Cs (Dfdrd). icastar. 1-VanRijn1984 2-Smith1977 3-Garcia1991 4-Zyserman1994 (solo si icserod=1)
*elseif(strcmp(GenData(Equilibrium_Cs),"Zyserman1994")==0)
4                             ---> Equilibrium Cs (Dfdrd). icastar. 1-VanRijn1984 2-Smith1977 3-Garcia1991 4-Zyserman1994 (solo si icserod=1)
*endif
*#ojo variables no introducidas por usuario
1                         ---> Critical Shear Stress para eroded layer (Dfdrd). itaucritsusp. 0-Constant 1-vanRijn     (solo si icserod=1)
0.d0                      ---> Critical shear stress of eroded layer (Dfdrd) (non-dimensional). taustarctesusp.       (solo si icserod=1,3)
0                         ---> Transport capacity para Cs. iqspot. 0-Generic 1-Yalin1963 2-Low1989 3-GoversUSP1992     (solo si icserod=3)
0.04   1.5                ---> Param A and B para generic formula. qspotpar(2) Tc=A.(tb-tc)^B
*#hidding factor
*GenData(Hiding_factor_SL)                            ---> Hiding factor between fractions para taucrit. dmixhidesusp. 0-min; 1-max 
!!!!!!!!!!!!!!!!!!!Model corrections!!!!!!!!!!!!!!!!!!!
*#Stress partition Einstein activado o no
*if(strcmp(GenData(SP_Einstein),"Off")==0)
0                             ---> Stress Partition Einstein. itauskin. 0-No  1-Yes
*else
1                             ---> Stress Partition Einstein. itauskin. 0-No  1-Yes
*endif
*#Grain roughness factor
*GenData(Grain_Roughness_factor)                          ---> Grain roughness factor. dfactgrain (Ksn=dfactgrain multiplica a dsediment) (solo si itauskin=1)
*#Eleccion de metodo para bed slope
*if(strcmp(GenData(Bed_slope_correction),"Off")==0)
0                             ---> Bed slope in bedload. ibedloadslope. 0-No 1-UDC2008 2-Duc2004
*elseif(strcmp(GenData(Bed_slope_correction),"On")==0)
1                             ---> Bed slope in bedload. ibedloadslope. 0-No 1-UDC2008 2-Duc2004
*else
2                             ---> Bed slope in bedload. ibedloadslope. 0-No 1-UDC2008 2-Duc2004
*endif
*#Avalanche correction activado o no
*if(strcmp(GenData(Max_slope_correction),"Off")==0)
0                             ---> Max slope correction. iaval_soilerosion. 0-No  1-Yes
*else
1                             ---> Max slope correction. iaval_soilerosion. 0-No  1-Yes
*endif
*#Apsley smoothing activado o no
*if(strcmp(GenData(Apsley_smoothing),"Off")==0)
0                             ---> Apsley smoothing. apsley_soilerosion. 0-No  1-Yes
*else
1                             ---> Apsley_smoothing. apsley_soilerosion. 0-No  1-Yes
*endif
*#activar o no el equilibrio en el lecho
*if(strcmp(GenData(Non-Equilibrium_bed_load),"Equilibrium")==0)
0                             ---> Non-equilibrium bedload. ibedloadequil. 0-Equilibrium 1-Non-Equilibrium
*else
1                             ---> Non-equilibrium bedload. ibedloadequil. 0-Equilibrium 1-Non-Equilibrium
*endif
*#Flujo secundario en el lecho
*if(strcmp(GenData(Secondary_flow_in_bedload),"Off")==0)
0                             ---> Secondary flow in bedload. isecondbed. 0-No 1-Yes (VER dsecdamp)
*else
1                             ---> Secondary flow in bedload. isecondbed. 0-No 1-Yes (VER dsecdamp)
*endif
*#Tipo de dispersion de Cs
*if(strcmp(GenData(Dispersion_of_Cs),"No")==0)
0                             ---> Dispersion of Cs. idispsed. 0-No 1-Longi 2-LongyTrans  (VER dsecdamp)
*elseif(strcmp(GenData(Dispersion_of_Cs),"Longitudinal")==0)
1                             ---> Dispersion of Cs. idispsed. 0-No 1-Longi 2-LongyTrans  (VER dsecdamp)
*else
2                             ---> Dispersion of Cs. idispsed. 0-No 1-Longi 2-LongyTrans  (VER dsecdamp)
*endif
*GenData(Flow_damping_near_walls)                          ---> Secondary flow damping near walls. dsecdamp. (damp si dwall<dsecdamp multiplicado por h)
*GenData(Secondary_flows_corrections)                         ---> dcurvaturemax. Maximum curvature para secondary flows corrections
!!!!!!!!!!!!!!!!!!!Mixture properties Introduce "ndsed" columns para defininir the properties of each fraction
*if(strcmp(GenData(Mixture_model),"One_NON-Cohesive_layer")==0)
*set var nrows=operation(GenData(Sediment_classes_1_layer,INT)/5)
*#Introduce ndsed columnas para definir las propiedades de cada fraccion number of cols=5, number of rows=*nrows
*#si las variables que introduce el usuario fuesen todas seguidas esta parte del codigo ocuparia mucho menos
*for(i=1;i<=1;i=i+1)
*for(j=0;j<=(nrows-1);j=j+1)
*set var pos=operation(j*5+i) 
*GenData(Sediment_classes_1_layer,*pos,real)*\
*endfor
                       ---> SOIL ID
*endfor
*for(i=2;i<=2;i=i+1)
*for(j=0;j<=(nrows-1);j=j+1)
*set var pos=operation(j*5+i) 
*GenData(Sediment_classes_1_layer,*pos,real)*\
*endfor
                       ---> Size(mm).       djmix(ndsed)
*endfor
*for(k=1;k<=nrows;k=k+1)
           0*\
*endfor
                       ---> Rainfall erodibility coef para cohesive soil (ad). djmixerodrainsoil(ndsed). [kg/m2/m] o [kg/J]
*for(i=3;i<=3;i=i+1)
*for(j=0;j<=(nrows-1);j=j+1)
*set var pos=operation(j*5+i)
*GenData(Sediment_classes_1_layer,*pos,real)*\
*endfor
                       ---> Rainfall erodibility coef para eroded layer (a_rd).  djmixerodrainlay(ndsed).  [kg/m2/m] o [kg/J]
*endfor
*for(k=1;k<=nrows;k=k+1)
           1*\
*endfor
                  ---> Rainfall erodibility exponent. djmixerodrainexp(ndsed)
*for(k=1;k<=nrows;k=k+1)
           0*\
*endfor
                  ---> Rainfall erodibility critical threshold. djmixerodraincrit(ndsed). [m/s] o [J/m2/s]
*for(k=1;k<=nrows;k=k+1)
           0*\
*endfor
                       ---> Flow erodibility coef para cohesive layer (F/J). djmixerodflowsoil(ndsed). [s/m]
*for(k=1;k<=nrows;k=k+1)
           0*\
*endfor
                  ---> Flow erodibility coef para eroded layer.   djmixerodflowlay(ndsed). [s/m]
*for(i=4;i<=4;i=i+1)
*for(j=0;j<=(nrows-1);j=j+1)
*set var pos=operation(j*5+i) 
*GenData(Sediment_classes_1_layer,*pos,real)*\
*endfor
                    ---> Critical stream power para eroded layer (sigma_cr) (si icserod=2). djmixcrspwlay(ndsed). [W/m2]
*endfor
*for(i=5;i<=5;i=i+1)
*for(j=0;j<=(nrows-1);j=j+1)
*set var pos=operation(j*5+i) 
*GenData(Sediment_classes_1_layer,*pos,real)*\
*endfor
                      ---> Fraction of stream power used para entrainment from eroded layer (F) (si icserod=2). djmixfrspwlay(ndsed)
*endfor
*for(k=1;k<=nrows;k=k+1)
           1*\
*endfor
                  ---> Rouse factor. djmixrouse(ndsed) <0-Computed N points  >1-cte (1:full mixed; inf:full strat)
*elseif(strcmp(GenData(Mixture_model),"One_NON-Cohesive_layer_over_one_cohesive_layer")==0)
*set var nrows=operation(GenData(Sediment_classes_2_layers,INT)/7)
*#Introduce ndsed columnas para definir las propiedades de cada fraccion number of cols=5, number of rows=*nrows
*#si las variables que introduce el usuario fuesen todas seguidas esta parte del codigo ocuparia mucho menos
*for(i=1;i<=1;i=i+1)
*for(j=0;j<=(nrows-1);j=j+1)
*set var pos=operation(j*7+i) 
*GenData(Sediment_classes_2_layers,*pos,real)*\
*endfor
                       ---> SOIL ID
*endfor
*for(i=2;i<=2;i=i+1)
*for(j=0;j<=(nrows-1);j=j+1)
*set var pos=operation(j*7+i) 
*GenData(Sediment_classes_2_layers,*pos,real)*\
*endfor
                       ---> Size(mm).       djmix(ndsed)
*endfor
*for(i=6;i<=6;i=i+1)
*for(j=0;j<=(nrows-1);j=j+1)
*set var pos=operation(j*7+i)
*GenData(Sediment_classes_2_layers,*pos,real)*\
*endfor
                       ---> Rainfall erodibility coef para cohesive soil (ad). djmixerodrainsoil(ndsed). [kg/m2/m] o [kg/J]
*endfor
*for(i=3;i<=3;i=i+1)
*for(j=0;j<=(nrows-1);j=j+1)
*set var pos=operation(j*7+i)
*GenData(Sediment_classes_2_layers,*pos,real)*\
*endfor
                       ---> Rainfall erodibility coef para eroded layer (a_rd).  djmixerodrainlay(ndsed).  [kg/m2/m] o [kg/J]
*endfor
*for(k=1;k<=nrows;k=k+1)
           1*\
*endfor
                  ---> Rainfall erodibility exponent. djmixerodrainexp(ndsed)
*for(k=1;k<=nrows;k=k+1)
           0*\
*endfor
                  ---> Rainfall erodibility critical threshold. djmixerodraincrit(ndsed). [m/s] o [J/m2/s]
*for(i=7;i<=7;i=i+1)
*for(j=0;j<=(nrows-1);j=j+1)
*set var pos=operation(j*7+i)
*GenData(Sediment_classes_2_layers,*pos,real)*\
*endfor
                       ---> Flow erodibility coef para cohesive layer (F/J). djmixerodflowsoil(ndsed). [s/m]
*endfor
*for(k=1;k<=nrows;k=k+1)
           0*\
*endfor
                  ---> Flow erodibility coef para eroded layer.   djmixerodflowlay(ndsed). [s/m]
*for(i=4;i<=4;i=i+1)
*for(j=0;j<=(nrows-1);j=j+1)
*set var pos=operation(j*7+i) 
*GenData(Sediment_classes_2_layers,*pos,real)*\
*endfor
                    ---> Critical stream power para eroded layer (sigma_cr) (si icserod=2). djmixcrspwlay(ndsed). [W/m2]
*endfor
*for(i=5;i<=5;i=i+1)
*for(j=0;j<=(nrows-1);j=j+1)
*set var pos=operation(j*7+i) 
*GenData(Sediment_classes_2_layers,*pos,real)*\
*endfor
                      ---> Fraction of stream power used para entrainment from eroded layer (F) (si icserod=2). djmixfrspwlay(ndsed)
*endfor
*for(k=1;k<=nrows;k=k+1)
           1*\
*endfor
                  ---> Rouse factor. djmixrouse(ndsed) <0-Computed N points  >1-cte (1:full mixed; inf:full strat)
*endif
!!!!!!!!!!!!!!!!!!!Numerical parameters!!!!!!!!!!!!!!!!!!!
*#ojo variables no introducidas por usuario
11                        ---> ischemeqsb. Numerical scheme para bedload eq. 11-upwind 20-centred 22-MUSCL
1.d-6                     ---> epswdqsb.Wet-Dry tolerance para bed load transport
11                        ---> ischemecs. Numerical scheme para suspension eq. 11-upwind 20-centred 21-gamma 22-MUSCL
0.5                       ---> betamcs. betam para suspension eq. solo si ischemecs=21 [0.1-0.5] [0.5-more stable]
0.                        ---> Diffusive stabilisation factor in Exner equation. dvtzbed
1                         ---> Bed Load escala de tiempo. ntimesed
!!!!!!!!!!!!!!!!!!!Outpt. options!!!!!!!!!!!!!!!!!!!
*#SS_concentration
*GenData(SS_concentration)                             ---> isalcsmix. Suspended sediment concentration mixture outpt. 0-No 1-Yes
*#bed load outpt
*GenData(Bed_load_discharge)                             ---> isalqsbmix. Bed load outpt. 0-No 1-Yes
*#Salida de E-D
*GenData(Suspended_load_discharge)                             ---> isalqssmix. Suspended load discharge. 0-No 1-Yes
*#Depth of eroded layer
*GenData(Depth_of_NON-cohesive_layer)                             ---> isaldlsed. Depth of eroded layer. 0-No 1-Yes
*#ojo variables no introducidas por usuario
0                         ---> isalrouse. Rousefactor. 0-No 1-Yes
0                             ---> isaldmix. sediment mixture distr in active layer. 0-No 1-Yes
0                             ---> isaldmixall. sediment mixture distr in every  layer. 0-No 1-Yes
*else

Soil erosion module disabled
*endif
*#FIN CHRISTIAN