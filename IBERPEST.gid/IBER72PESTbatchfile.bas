*#GONZALOALEN Nov/2022
*#------ Escribe el archivo batchfile intermedio que utiliza PEST ----
@echo off
rem Model input files written by PAR2PAR are deleted
*tcl(Pest::DelFiles)
rem PAR2PAR is run
*tcl(Pest::WritePaths 1) Iber_PEST_par2par.in
rem The model is run
*tcl(Pest::WritePaths 2)
