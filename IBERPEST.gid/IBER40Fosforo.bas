*# WQ 2.0 (Luis-Marcos 08/2019)
*#----------------Phosphorus---------------------
*if(Strcmp(GenData(Phosphorus),"On")==0)
*#------ Initial Condition --------------------
Initial condition Phosphorus 
*set cond Phosphorus_Initial_Cond *elems 
*if(CondNumEntities==0)
*messagebox - An element without Phosphorus Initial Condition was found, please check and run again - Process finished with error -
*endif
*loop elems *OnlyInCond
*ElemsNum *cond(P-org_[mg/l]) *cond(P-inorg_[mg/l])
*end elems
End Initial condition Phosphorus 
*#------ Boundary Condition --------------------
Boundary condition Phosphorus 
*set cond Phosphorus_Bound_Cond *elems 
*loop elems *OnlyInCond
*set var INS(int)=cond(Phosphorus_inlet,int)
*for(i=1;i<=INS(int);i=i+3)
*ElemsNum *CondElemFace *cond(Phosphorus_inlet,*i,int) *cond(Phosphorus_inlet,*Operation(i+1),real) *cond(Phosphorus_inlet,*Operation(i+2),real)
*end for
*end elems
End condition Phosphorus 
*endif

