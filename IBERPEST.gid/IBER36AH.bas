*# MSR 08/2018 - Updated 03/2023
*if(strcmp(GenData(IberHabitatPlugin),"Enabled")==0)
*if(strcmp(GenData(Habitat),"On")==0)
*if(strcmp(GenData(Habitat_criterion),"Multiplication")==0)
1
*elseif(strcmp(GenData(Habitat_criterion),"Addition")==0)
2
*elseif(strcmp(GenData(Habitat_criterion),"Arithmetic_mean")==0)
3
*elseif(strcmp(GenData(Habitat_criterion),"Geometric_mean")==0)
4
*endif
*format "%8i"
*GenData(Martinez-Capel_2006)    Martinez-Capel_2006
*format "%8i"
*GenData(Raleigh_1986)    Raleigh_1986 
*format "%8i"
*GenData(Waddle_2006)    Waddle_2006
*format "%8i"
*GenData(Luciobarbus_bocagei)    Luciobarbus_bocagei
*format "%8i"
*GenData(Pseudochondrostoma_polylepis)    Pseudochondrostoma_polylepis
*format "%8i"
*GenData(Squalius_pyrenaicus)    Squalius_pyrenaicus
*format "%8i"
*GenData(Salmo_Trutta_[Bovee])    Spanish_database_[Bovee]
*format "%8i"
*GenData(Salmo_Trutta_[Raleigh])    Spanish_database_[Raleigh]
*format "%8i"
*GenData(Custom_Suitability)    Custom_Suitability
*else
0
*endif
*else
0
*endif